<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ログイン前の場合、強制的にログイン画面に飛ばす */
Route::group(["before" => "guest"], function()
{
	Route::any("/", [
		//Laravel VerUp From 4.2 To 5.5
		// "as"   => "user/login",
		"as"   => "user.login",
		"uses" => "AA1010LoginController@LoginAction"
	]);
});


/* ログイン後の場合 */
Route::group(["before" => "auth"], function()
{
	Route::any("user/list", [
		"as"   => "user.list",
		"uses" => "BA1010ListController@ListAction"
	]);
	
	Route::any("user/preentry", [
		"as"   => "user.preentry",
		"uses" => "BA2020PreEntryController@EntryAction"
	]);
	
	Route::any("user/entry", [
		"as"   => "user.entry",
		"uses" => "BA2010EntryController@EntryAction"
	]);
	
	Route::any("user/master", [
		"as"   => "user.master",
		"uses" => "ZA1010MasterController@MasterAction"
	]);

	Route::any("user/picture", [
		"as"   => "user.picture",
		"uses" => "ZA2010PictureController@MasterAction"
	]);

	Route::any("user/usermaster", [
		"as"   => "user.usermaster",
		"uses" => "ZA2020UserMasterController@MasterAction"
	]);

	Route::any("user/machinemaster", [
		"as"   => "user.machinemaster",
		"uses" => "ZA2030MachineMasterController@MasterAction"
	]);

	Route::any("user/customermaster", [
		"as"   => "user.customermaster",
		"uses" => "ZA2040CustomerMasterController@MasterAction"
	]);

	Route::any("user/equipmentmaster", [
		"as"   => "user.equipmentmaster",
		"uses" => "ZA2050EquipmentMasterController@MasterAction"
	]);

	Route::any("user/ngreasonmaster", [
		"as"   => "user.ngreasonmaster",
		"uses" => "ZA2060NgreasonMasterController@MasterAction"
	]);

	Route::any("user/ngreasonbunruimaster", [
		"as"   => "user.ngreasonbunruimaster",
		"uses" => "ZA2070NgreasonBunruiMasterController@MasterAction"
	]);

	Route::any("user/actionmdetailmaster", [
		"as"   => "user.actionmdetailmaster",
		"uses" => "ZA2080ActionDetailMasterController@MasterAction"
	]);

	Route::any("user/trpersonmaster", [
		"as"   => "user.trpersonmaster",
		"uses" => "ZA2090TechnicianRequestPersonMasterController@MasterAction"
	]);

	Route::any("user/monitoring", [
		"as"   => "user.monitoring",
		"uses" => "BA3010MonitoringController@MasterAction"
	]);
	
	Route::any("user/graph", [
		"as"   => "user.graph",
		"uses" => "BA4010GraphController@MasterAction"
	]);

	Route::any("user/logout", [
		"as"   => "user.logout",
		"uses" => "AA1010LoginController@LogoutAction"
	]);

});

