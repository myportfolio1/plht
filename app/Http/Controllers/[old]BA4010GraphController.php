<?php

use Illuminate\Support\MessageBag;
use Carbon\Carbon;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');

include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';
include_once 'PHPExcel/Writer/Excel2007.php';
//**************************************************************************
// display          :Graph
// overview         :
// author           :
// date             :
// record of updates:
//         
//**************************************************************************
class BA4010GraphController
extends Controller
{
	//data count per 1 page
	CONST NUMBER_PER_PAGE    = 10;

	CONST SHONINEXCEL_HEADER_STARTROW = 2;
	CONST NGSHONINEXCEL_MEISAI_STARTROW = 7;

	//**************************************************************************
	// process:  MasterAction
	// overview: display screen
	// argument: nothing
	// return value: nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData						= []; //Array for transportion to screen

		$SearchResultData               = [];
		$lTblSearchResultData    		= [];
		$lPagenation             		= []; //For Paging（Return to screen）

		//Value to make Combo (Set value from session when transit to screen)
		$lCmbValMasterNoForGraph		= "";
		$lCmbValProcessForGraph 		= "";
		$lCmbValCustomerForGraph 		= "";
		$lCmbValCheckSheetNoForGraph 	= "";

		$arrCategories  = [];
        $arrAverage		= [];
        $arrMax   	    = [];
        $arrMin   	    = [];
        $arrRange       = [];

		//store value of entering to screen
		Input::flash();

		//Issue parameters from login display through Session to Array for transportion
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnGraph'))
		{
			//log
			Log::write('info', 'BA4010 Graph Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForGraph'		   	,''),
					"Process"				=> Input::get('cmbProcessForGraph'		   		,''),
					"Customer/Supplier"		=> Input::get('cmbCustomerForGraph'				,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForGraph'       	,''),
					"Revision No."			=> Input::get('cmbRevisionNoForGraph'			,''),
					"Anal Grp(Insp No)"		=> Input::get('cmbAnalyticalGroupForGraph'			,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForGraph'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForGraph'       	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForGraph' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForGraph'   	,''),
					"Condition"				=> Input::get('cmbConditionForGraph'			,''),
					"Select Type"			=> Input::get('cmbSelectTypeForGraph'			,''),
				]
			);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//clear in case search requirment remain in session
				Session::forget('BA4010MasterNoForGraph');
				Session::forget('BA4010ProcessForGraph');
				Session::forget('BA4010CustomerForGraph');
				Session::forget('BA4010CheckSheetNoForGraph');
				Session::forget('BA4010RevisionNoForGraph');
				//↓InspectionNo=AnalyticalGroup
				Session::forget('BA4010InspectionNoForGraph');
				Session::forget('BA4010MaterialNameForGraph');
				Session::forget('BA4010MaterialSizeForGraph');
				Session::forget('BA4010InspectionDateFromForGraph');
				Session::forget('BA4010InspectionDateToForGraph');
				Session::forget('BA4010ConditionForGraph');
            
                //Nawaphat make Average Graph
				$SelectGraphType = Input::get('cmbSelectTypeForGraph','');
				$ProcessForGraph = Input::get('cmbProcessForGraph','');
				$GRP = Input::get('cmbAnalyticalGroupForGraph','');
				$Condition = Input::get('cmbConditionForGraph','');
				$SearchResultAllGraph = $this->getResultAllGraph();
			   	if (count($SearchResultAllGraph) == 0)
				{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "No have Inspection Result."
						]);

				}else{
			    	foreach ($SearchResultAllGraph as $key => $value) 
					{
						if($value->CPK_VALUE != 0.0000) {
									if($SelectGraphType == 04){
										$arrMax[] = $value->MAX_VALUE;
									    $arrMin[] = $value->MIN_VALUE;
									    $arrRange[] = $value->RANGE_VALUE;
										$arrAverage[] = $value->AVERAGE_VALUE;
									}else{
										$arrMax[] = array($value->MAX_VALUE);
									    $arrMin[] = array($value->MIN_VALUE);
									    $arrRange[] = array($value->RANGE_VALUE);
										$arrAverage[] = array($value->AVERAGE_VALUE);
									}
									$arrCategories[] = date_format(date_create($value->INSPECTION_YMD),"d/m/Y").' '.$value->INSPECTION_TIME_NAME.' '.$value->CONDITION_CD;
								    $arrMinutOneSigma[] = $value->AVERAGE_VALUE - $value->STDEV_VALUE;
								    $arrMinutTwoSigma[] = $value->AVERAGE_VALUE - ( 2 * $value->STDEV_VALUE);
							 		$arrMinutThreeSigma[] = $value->AVERAGE_VALUE - ( 3 * $value->STDEV_VALUE);
							 		$arrMinutFourSigma[] = $value->AVERAGE_VALUE - ( 4 * $value->STDEV_VALUE);
							 		$arrPlusOneSigma[] = $value->AVERAGE_VALUE + $value->STDEV_VALUE;
								    $arrPlusTwoSigma[] = $value->AVERAGE_VALUE + ( 2 * $value->STDEV_VALUE);
							 		$arrPlusThreeSigma[] = $value->AVERAGE_VALUE + ( 3 * $value->STDEV_VALUE);
							 		$arrPlusFourSigma[] = $value->AVERAGE_VALUE + ( 4 * $value->STDEV_VALUE);
							 		$arrUCL[] = $value->UPPER_CONTROL_LIMIT;
							 		$arrLCL[] = $value->LOWER_CONTROL_LIMIT;
							 		$arrAnalyticalGroup[] = $value->ANALYTICAL_GRP;
							 		$arrCPK[] = $value->CPK_VALUE;
							 		$arrDataSimpleDistribution[] = $this->simpleDistribution($value->INSPECTION_RESULT_NO, $value->ANALYTICAL_GRP ,$value->CONDITION_CD);
							 		if($value->STDEV_VALUE == 0.000){
							 			$arrBellCurve[] = 0;
							 		}else{
										$arrBellCurve[] = $this->getBellCurve($value->INSPECTION_RESULT_NO, $value->ANALYTICAL_GRP,$value->STDEV_VALUE,$value->AVERAGE_VALUE);
							 		}
							 		$arrSTDEV[] = $value->STDEV_VALUE;
							 		
						}
					}

					if((empty($GRP) and empty($Condition)) or (empty($GRP) and !empty($Condition)))
					{
							if($SelectGraphType == 04)
							{
	                            $flag = 1;
								$lViewData += [
								"SelectTypeData" 		=> $SelectGraphType,
								"arrCategories"         => $arrCategories,
								"arrAverage"            => $arrAverage,
								"arrMax"                => $arrMax,
								"arrMin"                => $arrMin,
								"arrRange"              => $arrRange,
								"arrMinutOneSigma"      => $arrMinutOneSigma,
								"arrMinutTwoSigma"      => $arrMinutTwoSigma,
								"arrMinutThreeSigma"    => $arrMinutThreeSigma,
								"arrMinutFourSigma"     => $arrMinutFourSigma,
								"arrPlusOneSigma"       => $arrPlusOneSigma,
								"arrPlusTwoSigma"       => $arrPlusTwoSigma,
								"arrPlusThreeSigma"     => $arrPlusThreeSigma,
								"arrPlusFourSigma"      => $arrPlusFourSigma,
								"arrUCL"                => $arrUCL,
								"arrLCL"                => $arrLCL,
								"arrDataSimpleDistribution" => $arrDataSimpleDistribution,
								"arrBellCurve"           => $arrBellCurve,
								"arrAnalyticalGroup"     => $arrAnalyticalGroup,
								"arrCPK"                 => $arrCPK,
								"flag"                   => $flag,
								"arrSTDEV"               => $arrSTDEV
								];
								
							}else{

		                            $countArrAnalyticalGroup = array_count_values($arrAnalyticalGroup);

		                            foreach ($countArrAnalyticalGroup as $key => $value) {
		                            	$AnalyGrp[] = $value;
		                            	$Group[] = $key;
									}
									
		                            $result = [];
		                            for($i = 0; $i < sizeof($AnalyGrp) ; $i++){
			                           	if( $i == 0){
				  							$sum = current($AnalyGrp); 
				                        }else{
				                        	$sum = end($result) + next($AnalyGrp); 
				                        }
				                        array_push($result, $sum);
		                            }
		  								 
		 						
		                            foreach ($SearchResultAllGraph as $k => $val) {
		                            	if($val->CPK_VALUE != 0.0000) {
		                            		$arrAnalyticalEachGroup[$val->ANALYTICAL_GRP][] = $val->ANALYTICAL_GRP;
		                            		$arrCategoriesEachGroup[$val->ANALYTICAL_GRP][] = array(date_format(date_create($val->INSPECTION_YMD),"d/m/Y").' '.$val->INSPECTION_TIME_NAME.' '.$val->CONDITION_CD);
		                            		$arrAverageEachGroup[$val->ANALYTICAL_GRP][] = array($val->AVERAGE_VALUE); 
		                            		$arrMaxEachGroup[$val->ANALYTICAL_GRP][] = array($val->MAX_VALUE);
		                            		$arrMinEachGroup[$val->ANALYTICAL_GRP][] = array($val->MIN_VALUE);
		                            		$arrUCLEachGroup[$val->ANALYTICAL_GRP][] = array($val->UPPER_CONTROL_LIMIT);
		                            		$arrLCLEachGroup[$val->ANALYTICAL_GRP][] = array($val->LOWER_CONTROL_LIMIT);
		                            		$arrRangeEachGroup[$val->ANALYTICAL_GRP][] = array($val->RANGE_VALUE);
		                            	} 	  
		                            }
		                              $flag = 3;
										$lViewData += [
										"SelectTypeData" 		=> $SelectGraphType,
										"arrCategories"         => $arrCategories,
										"arrAverage"            => $arrAverage,
										"arrMax"                => $arrMax,
										"arrMin"                => $arrMin,
										"arrRange"              => $arrRange,
										"arrMinutOneSigma"      => $arrMinutOneSigma,
										"arrMinutTwoSigma"      => $arrMinutTwoSigma,
										"arrMinutThreeSigma"    => $arrMinutThreeSigma,
										"arrMinutFourSigma"     => $arrMinutFourSigma,
										"arrPlusOneSigma"       => $arrPlusOneSigma,
										"arrPlusTwoSigma"       => $arrPlusTwoSigma,
										"arrPlusThreeSigma"     => $arrPlusThreeSigma,
										"arrPlusFourSigma"      => $arrPlusFourSigma,
										"arrUCL"                => $arrUCL,
										"arrLCL"                => $arrLCL,
										"arrDataSimpleDistribution" => $arrDataSimpleDistribution,
										"arrBellCurve"           => $arrBellCurve,
										"arrAnalyticalGroup"     => $arrAnalyticalGroup,
										"arrCPK"                 => $arrCPK,
										"flag"                   => $flag,
										"AllGroup" 				  => $Group,
										"arrAnalyticalEachGroup"  => $arrAnalyticalEachGroup,
										"arrCategoriesEachGroup"  => $arrCategoriesEachGroup,
										"arrAverageEachGroup"     => $arrAverageEachGroup,
										"arrMaxEachGroup"         => $arrMaxEachGroup,
										"arrMinEachGroup"         => $arrMinEachGroup,
										"arrUCLEachGroup"         => $arrUCLEachGroup,
										"arrLCLEachGroup"         => $arrLCLEachGroup,
										"arrRangeEachGroup"       => $arrRangeEachGroup,
										"arrSTDEV"                => $arrSTDEV
										]; 
						    }
	                }
					elseif((!empty($GRP) and empty($Condition)) or (!empty($GRP) and !empty($Condition)))
					{
							if($SelectGraphType == 01 or $SelectGraphType == 02 or $SelectGraphType == 03 or $SelectGraphType == 04)
								{
	                                $flag = 1;
									$lViewData += [
									"SelectTypeData" 		=> $SelectGraphType,
									"arrCategories"         => $arrCategories,
									"arrAverage"            => $arrAverage,
									"arrMax"                => $arrMax,
									"arrMin"                => $arrMin,
									"arrRange"              => $arrRange,
									"arrMinutOneSigma"      => $arrMinutOneSigma,
									"arrMinutTwoSigma"      => $arrMinutTwoSigma,
									"arrMinutThreeSigma"    => $arrMinutThreeSigma,
									"arrMinutFourSigma"     => $arrMinutFourSigma,
									"arrPlusOneSigma"       => $arrPlusOneSigma,
									"arrPlusTwoSigma"       => $arrPlusTwoSigma,
									"arrPlusThreeSigma"     => $arrPlusThreeSigma,
									"arrPlusFourSigma"      => $arrPlusFourSigma,
									"arrUCL"                => $arrUCL,
									"arrLCL"                => $arrLCL,
									"arrDataSimpleDistribution" => $arrDataSimpleDistribution,
									"arrBellCurve"           => $arrBellCurve,
									"arrAnalyticalGroup"     => $arrAnalyticalGroup,
									"arrCPK"                 => $arrCPK,
									"flag"                   => $flag,
									"arrSTDEV"               => $arrSTDEV
									];
								}
					}else{
						$lViewData["errors"] = new MessageBag([
								"error" => "E997 : Target data does not exist."
							]);
					}
			    }
 

				//sessionにstore
				Session::put('BA4010InspectionResultData', $SearchResultData);
			}
		}elseif(Input::has('btnExcel')){

			//log
			Log::write('info', 'BA4010 Graph Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForGraph'		   	,''),
					"Process"				=> Input::get('cmbProcessForGraph'		   		,''),
					"Customer/Supplier"		=> Input::get('cmbCustomerForGraph'				,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForGraph'       	,''),
					"Revision No."			=> Input::get('cmbRevisionNoForGraph'			,''),
					"Anal Grp(Insp No)"		=> Input::get('cmbAnalyticalGroupForGraph'			,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForGraph'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForGraph'       	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForGraph' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForGraph'   	,''),
					"Condition"				=> Input::get('cmbConditionForGraph'			,''),
					"Select Type"			=> Input::get('cmbSelectTypeForGraph'			,''),
				]
			);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				$allData = $this->getDataforExcelSheet();
				if (count($allData ) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E036 : Not Exists Target Data."
					]);
				}
				else  //data exists
				{
					// $inputFileName = public_path()."/excel/DataOnGraphPage_template.xlsx";
					$inputFileName = "excel/DataOnGraphPage_template.xlsx";

				 	try 
				    {
				        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
				        $objPHPExcel = $objReader -> load($inputFileName);

						 // Add some data
				        foreach ($allData as $key => $value) {
				        	$cell = $key + 2;
				        	$objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('A'.$cell, $value->MASTER_NO);
						    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$cell, $value->PROCESS_ID, PHPExcel_Cell_DataType::TYPE_STRING);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('C'.$cell, $value->CODEORDER_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('D'.$cell, $value->CUSTOMER_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('E'.$cell, $value->INSPECTION_SHEET_NO);
						    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$cell, $value->REV_NO, PHPExcel_Cell_DataType::TYPE_STRING);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('G'.$cell, $value->MATERIAL_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('H'.$cell, $value->MATERIAL_SIZE);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('I'.$cell, $value->MACHINE_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('J'.$cell, $value->MACHINE_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('K'.$cell, $value->LAST_UPDATE_USER_ID);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('L'.$cell, $value->USER_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('M'.$cell, $value->PART_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('N'.$cell, $value->PART_NAME);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('O'.$cell, $value->INSERT_DATE);	
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('P'.$cell, $value->INSERT_TIME);					
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('Q'.$cell, $value->CONDITION_CD);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('R'.$cell, $value->INSPECTION_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('S'.$cell, $value->INSPECTION_RESULT_VALUE);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('T'.$cell, $value->AVERAGE_VALUE);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('U'.$cell, $value->MIN_VALUE);	
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('V'.$cell, $value->MAX_VALUE);					
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('W'.$cell, $value->RANGE_VALUE);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('X'.$cell, $value->CPK_VALUE);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('Y'.$cell, $value->LOWER_CONTROL_LIMIT);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('Z'.$cell, $value->UPPER_CONTROL_LIMIT);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AA'.$cell, $value->ANALYTICAL_GRP);	
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AB'.$cell, $value->ANALYTICAL_GRP_POINT);					
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AC'.$cell, $value->ANALYTICAL_GRP_HOWMANY);	
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AD'.$cell, $value->LOT_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AE'.$cell, $value->MATERIAL_LOT_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AF'.$cell, $value->CERTIFICATE_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AG'.$cell, $value->PO_NO);	
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AH'.$cell, $value->INVOICE_NO);
						    $objPHPExcel->setActiveSheetIndex(0)
						            ->setCellValue('AI'.$cell, $value->UPDATE_DATE);   
				        }

						$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

						// $filename = "DataOnGraphPage_template.xlsx";
						$filename = str_replace("template", date("Ymd")."_".date("His"), "DataOnGraphPage_template.xlsx");
						$objWriter->save($filename);
						header('Content-Disposition: attachment; filename="' . $filename . '"');
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Length: ' . filesize($filename));
						header('Content-Transfer-Encoding: binary');
						header('Cache-Control: must-revalidate');
						header('Pragma: public');
						readfile($filename);
						    
				    }
				    catch(Exception $e) 
				    {
				        die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				    }
				}
			}	
		}
		else
		{

			//clear information except for entry in screen in session（delete search result and restore before state
			//in case index.php/user/list do not include in URL,(＝transition from other screen）＝False
			//don't be False
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/graph');

				if($lPrevURL == false)
				{
					//delete information of search（reset）
					$this->initializeSessionData();
					
					//if transition from other screen,get value from session
					$lCmbValMasterNoForGraph = Session::get('BA4010MasterNoForGraph');
					$lCmbValProcessForGraph = Session::get('BA4010ProcessForGraph');
					$lCmbValCustomerForGraph = Session::get('BA4010CustomerForGraph');
					$lCmbValCheckSheetNoForGraph = Session::get('BA4010CheckSheetNoForGraph');
					
					//上に場所移動
					//delete information of search（reset）
					//$this->initializeSessionData();
				}
			}
		}

		//-----------------------------
		//set combo Condition
		//-----------------------------
		$lViewData = $this->setConditionList($lViewData);

		//make pagenation
		$lTblSearchResultData = $this->getResultAllGraph();
		// $lTblSearchResultData = [];
	
		$lPagenation = Paginator::make($lTblSearchResultData, Count($lTblSearchResultData), self::NUMBER_PER_PAGE);    //data,total number of issue,number per 1page

		//add to array for transportion to screen(written in +=))
		$lViewData += [
			"Pagenator"       => $lPagenation,
		];
          
		//画面入力項目の保持と再設定
		//store and set again entry in screen
		$lViewData = $this->setListForms($lViewData);

		//-----------------------------
		//set combo process
		//-----------------------------
		$lViewData = $this->setProcessList($lViewData);
		
		//in case process is posted from screen
		if (Input::has('cmbProcessForGraph'))
		{
			$lViewData += [
				"ProcessForGraph" => (String)Input::get('cmbProcessForGraph'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData += [
				"ProcessForGraph" => "",
			];
		}

		//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■Add Hoshina 04-Aug-2017 KOHBYO
		//-----------------------------
		//set combo ①etc (for Entry)
		//-----------------------------
		// if (Input::has('txtMasterNoForEntry') && Input::has('cmbProcessForEntry'))
		if ((Input::has('txtMasterNoForGraph') && Input::has('cmbProcessForGraph')) || Input::has('cmbRevisionNoForGraph'))
		{  
			//in case value exist in customer infomation for search
			$lViewData = $this->setCustomerNoListEntry($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), "arrDataListCustomerList");
          
			if (count($lViewData['arrDataListCustomerList']) == 1)
			{
				$lTblCustomerID = $this->getCustomerID(Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'));
				
				if($lTblCustomerID != null)
				{
					$lRowCustomerID = (Array)$lTblCustomerID[0];
					//in case value exist in customer infomation for search
					//$pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey
					$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], "arrCheckSheetNoForGraphList");

						if (Input::has('cmbCheckSheetNoForGraph'))
						{
							// in case value exist in customer infomation for search
							
							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], Input::get('cmbCheckSheetNoForGraph'), "arrMaterialNameForGraphList");
							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], Input::get('cmbCheckSheetNoForGraph'), "arrMaterialSizeForGraphList");
							$lViewData = $this->setRevisionNoList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], Input::get('cmbCheckSheetNoForGraph'), "arrRevNoForGraphList");
						}
						else
						{
						
							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForGraph, "arrMaterialNameForGraphList");
							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForGraph, "arrMaterialSizeForGraphList");
							$lViewData = $this->setRevisionNoList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForGraph, "arrRevNoForGraphList");
						}
				}
			}
		}

		//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

		//-----------------------------
		//set combo customer
		//-----------------------------
		$lViewData = $this->setCustomerList($lViewData);
		
		//in case Customer is posted from screen
		if (Input::has('cmbCustomerForGraph'))
		{
			$lViewData = $this->setAnaGrp($lViewData, "arrDataListAnalyticalGroup");
			$lViewData += [
				"CustomerForGraph" => (String)Input::get('cmbCustomerForGraph'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData = $this->setAnaGrp($lViewData, "arrDataListAnalyticalGroup");
			$lViewData += [
				"CustomerForGraph" => "",
			];
		}
		
		//-----------------------------
		//set combo ①Inspection Sheet No
		//-----------------------------
		if (Input::has('txtMasterNoForGraph') && Input::has('cmbProcessForGraph') && Input::has('cmbCustomerForGraph'))
		{
			//in case value exist
			$lViewData = $this->setCheckSheetNoList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), "arrCheckSheetNoForGraphList");
			$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), $lCmbValCheckSheetNoForGraph, "arrMaterialNameForGraphList");
			$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), $lCmbValCheckSheetNoForGraph, "arrMaterialSizeForGraphList");
			$lViewData = $this->setRevisionNoList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), "arrRevNoForGraphList");
		}
		else
		{
			//in case value does not exist
			$lViewData = $this->setCheckSheetNoList($lViewData, $lCmbValMasterNoForGraph, $lCmbValProcessForGraph, $lCmbValCustomerForGraph, "arrCheckSheetNoForGraphList");
		}

		//-----------------------------
		//set combo ①Revision No ②MaterialName ③MaterialSize
		//-----------------------------
		if ((Input::has('cmbProcessForGraph') && Input::has('cmbCustomerForGraph') && Input::has('cmbCheckSheetNoForGraph')) || (Input::has('cmbCustomerForGraph') && Input::has('cmbCheckSheetNoForGraph')))
		{
			//in case value exits in both Inspection No. infomation
			$lViewData = $this->setRevNoList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'));
			$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), "arrMaterialNameForGraphList");
			$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForGraph'), Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), "arrMaterialSizeForGraphList");
		}
		else
		{
			//in case value does not exit in Inspection No.
			$lViewData = $this->setRevNoList($lViewData, $lCmbValMasterNoForGraph, $lCmbValProcessForGraph, $lCmbValCustomerForGraph, $lCmbValCheckSheetNoForGraph);
			$lViewData = $this->setMaterialNameList($lViewData, $lCmbValMasterNoForGraph, $lCmbValProcessForGraph, $lCmbValCustomerForGraph, $lCmbValCheckSheetNoForGraph, "arrMaterialNameForGraphList");
			$lViewData = $this->setMaterialSizeList($lViewData, $lCmbValMasterNoForGraph, $lCmbValProcessForGraph, $lCmbValCustomerForGraph, $lCmbValCheckSheetNoForGraph, "arrMaterialSizeForGraphList");
		}

		//-----------------------------
		//set combo Analytical Group(Inspection No)
		//-----------------------------
		if(Input::has('cmbProcessForGraph') && Input::has('cmbCheckSheetNoForGraph'))
		{
			$lViewData = $this->setAnalyticalGroupList($lViewData, Input::get('cmbCheckSheetNoForGraph'));
		}
		else
		{
			$lViewData = $this->setAnalyticalGroupList($lViewData, $lCmbValCheckSheetNoForGraph);
		}

		if(Input::has('cmbSelectTypeForGraph'))
		{
			$lViewData += [
				"SelectTypeData" 		=> Session::get('cmbSelectTypeForGraph'),
				"arrCategories"			=> $arrCategories,
			];
		}
		else
		{
			$lViewData += [
				"SelectTypeData" 		=> Session::get('cmbSelectTypeForGraph'),
				"arrCategories"			=> $arrCategories,
			];
		}

		return View::make("user/graph", $lViewData);
	}




	//**************************************************************************
	// process    isErrorForSearch★★
	// overview      check type in search
	// argument      Array to return to screen
	// return value    Array to return to screen
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForSearch($pViewData)
	{
		//must check Master No.
		$lValidator = Validator::make(
			array('txtMasterNoForGraph' => Input::get('txtMasterNoForGraph')),
			array('txtMasterNoForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E042 : Enter Master No. ."
			]);
			
			return $pViewData;
		}
		
		//check Master No.
		$lValidator = Validator::make(
			array('txtMasterNoForGraph'	=> Input::get('txtMasterNoForGraph')),
			array('txtMasterNoForGraph'	=> array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E043 : Master No. is invalid."
			]);
			
			return $pViewData;
		}


		//must check Process data
		$lValidator = Validator::make(
			array('cmbProcessForGraph' => Input::get('cmbProcessForGraph')),
			array('cmbProcessForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E044 : Select Process."
			]);
			
			return $pViewData;
		}

		//check Process
		$lValidator = Validator::make(
			array('cmbProcessForGraph'	=> Input::get('cmbProcessForGraph')),
			array('cmbProcessForGraph'	=> array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E045 : Process is invalid."
			]);
			
			return $pViewData;
		}


		//must check customer
		$lValidator = Validator::make(
			array('cmbCustomerForGraph' => Input::get('cmbCustomerForGraph')),
			array('cmbCustomerForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E049 : Select Customer."
			]);
			
			return $pViewData;
		}


		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForGraph' => Input::get('cmbCheckSheetNoForGraph')),
			array('cmbCheckSheetNoForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E006 : Enter Check Sheet No. ."
			]);
			
				return $pViewData;
		}

		//check check sheet No. type
		//Validation check after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForGraph'));

		$lValidator = Validator::make(
			array('cmbCheckSheetNoForGraph' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForGraph' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E007 : Check Sheet No. is invalid."
			]);

			return $pViewData;
		}

		//must Revision No. type
		$lValidator = Validator::make(
			array('cmbRevisionNoForGraph' => Input::get('cmbRevisionNoForGraph')),
			array('cmbRevisionNoForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E018 : Revision No. ."
			]);
			
			return $pViewData;
		}
		//check Revision No. type
		$lValidator = Validator::make(
			array('cmbRevisionNoForGraph' => Input::get('cmbRevisionNoForGraph')),
			array('cmbRevisionNoForGraph' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E018 : Revision No. is invalid."
			]);

			return $pViewData;
		}


		//check Material Name type
		$lValidator = Validator::make(
			array('cmbMaterialNameForGraph' => Input::get('cmbMaterialNameForGraph')),
			array('cmbMaterialNameForGraph' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E047 : Material Name is invalid."
			]);

			return $pViewData;
		}


		//check inspection date type(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForGraph' => Input::get('txtInspectionDateFromForGraph')),
			array('txtInspectionDateFromForGraph' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);
			
			return $pViewData;
		}

		//check inspection date type(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForGraph' => Input::get('txtInspectionDateToForGraph')),
			array('txtInspectionDateToForGraph' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);
			
			return $pViewData;
		}

		//check inspection from/to date(must check From/To)
		if ((Input::get('txtInspectionDateFromForGraph') != "") and (Input::get('txtInspectionDateToForGraph') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtInspectionDateFromForGraph'));
			$lDateTo   = date_create(Input::get('txtInspectionDateToForGraph'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E019 : The relation of Inspection Date(From/To) is incorrect."
				]);
				
				return $pViewData;
			}
		}


		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbSelectTypeForGraph' => Input::get('cmbSelectTypeForGraph')),
			array('cmbSelectTypeForGraph' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "Select Type of Graph."
			]);
			
			return $pViewData;
		}


		return $pViewData;
	}


	//**************************************************************************
	// process       initializeSessionData★★
	// overview      clear session data（when it return from entry screen and delete）
	// argument      Nothing
	// return value  Nothing
	// author        
	// date          
	//**************************************************************************
	private function initializeSessionData()
	{
		//clear search result data
		Session::forget('BA4010MasterNoForGraph');
		Session::forget('BA4010ProcessForGraph');
		Session::forget('BA4010CustomerForGraph');
		Session::forget('BA4010CheckSheetNoForGraph');
		Session::forget('BA4010RevisionNoForGraph');
		Session::forget('BA4010AnalyticalGroupForGraph');
		Session::forget('BA4010MaterialNameForGraph');
		Session::forget('BA4010MaterialSizeForGraph');
		Session::forget('BA4010ConditionForGraph');
		
		Session::forget('BA4010InspectionResultData');
		
		return null;
	}


	//**************************************************************************
	// process       setListForms★★
	// overview      store and return form input information of search result list screen
	// argument      Arary
	// return value  Array
	// author        
	// date          
	//**************************************************************************
	private function setListForms($pViewData)
	{
		//■①store Master No for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010MasterNoForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtMasterNoForGraph')) {
				Session::put('BA4010MasterNoForGraph', Input::get('txtMasterNoForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010MasterNoForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtMasterNoForGraph')) {
				Session::put('BA4010MasterNoForGraph', Input::get('txtMasterNoForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MasterNoForGraph"  => Session::get('BA4010MasterNoForGraph')
		];

		//■②store Process for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010ProcessForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForGraph')) {
				Session::put('BA4010ProcessForGraph', Input::get('cmbProcessForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010ProcessForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForGraph')) {
				Session::put('BA4010ProcessForGraph', Input::get('cmbProcessForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ProcessForGraph"  => Session::get('BA4010ProcessForGraph')
		];

		//■③store Customer for graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010CustomerForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForGraph')) {
				Session::put('BA4010CustomerForGraph', Input::get('cmbCustomerForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010CustomerForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForGraph')) {
				Session::put('BA4010CustomerForGraph', Input::get('cmbCustomerForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CustomerForGraph"  => Session::get('BA4010CustomerForGraph')
		];

		//■④store Check Sheet No. for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010CheckSheetNoForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForGraph')) {
				Session::put('BA4010CheckSheetNoForGraph', Input::get('cmbCheckSheetNoForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010CheckSheetNoForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForGraph')) {
				Session::put('BA4010CheckSheetNoForGraph', Input::get('cmbCheckSheetNoForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CheckSheetNoForGraph"  => Session::get('BA4010CheckSheetNoForGraph')
		];

		//■⑤store Revision No. for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010RevisionNoForGraph'))) {

			if (Input::has('cmbRevisionNoForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010RevisionNoForGraph', Input::get('cmbRevisionNoForGraph'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA4010RevisionNoForGraph', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbRevisionNoForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010RevisionNoForGraph', Input::get('cmbRevisionNoForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"RevisionNoForGraph"  => Session::get('BA4010RevisionNoForGraph')
		];

		//■⑥store Analytical Group for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010AnalyticalGroupForGraph'))) {

			if (Input::has('cmbAnalyticalGroupForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010AnalyticalGroupForGraph', Input::get('cmbAnalyticalGroupForGraph'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA4010AnalyticalGroupForGraph', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbAnalyticalGroupForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010AnalyticalGroupForGraph', Input::get('cmbAnalyticalGroupForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"AnalyticalGroupForGraph"  => Session::get('BA4010AnalyticalGroupForGraph')
		];

		//■⑦store Material Name for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010MaterialNameForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForGraph')) {
				Session::put('BA4010MaterialNameForGraph', Input::get('cmbMaterialNameForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010MaterialNameForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForGraph')) {
				Session::put('BA4010MaterialNameForGraph', Input::get('cmbMaterialNameForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialNameForGraph"  => Session::get('BA4010MaterialNameForGraph')
		];

		//■⑧store Material Size for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010MaterialSizeForGraph'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForGraph')) {
				Session::put('BA4010MaterialSizeForGraph', Input::get('cmbMaterialSizeForGraph'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA4010MaterialSizeForGraph', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForGraph')) {
				Session::put('BA4010MaterialSizeForGraph', Input::get('cmbMaterialSizeForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialSizeForGraph"  => Session::get('BA4010MaterialSizeForGraph')
		];

		//■⑨store Inspection Date（From） for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010InspectionDateFromForGraph'))) {

			if (Input::has('txtInspectionDateFromForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010InspectionDateFromForGraph', Input::get('txtInspectionDateFromForGraph'));
			}
			else
			{
				//if value of screen,write down system date in session
				//Session::put('BA4010InspectionDateFromForGraph', date("d-m-Y",strtotime("-1 month")));
				Session::put('BA4010InspectionDateFromForGraph', date("d-m-Y",strtotime("-1 week")));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateFromForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010InspectionDateFromForGraph', Input::get('txtInspectionDateFromForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateFromForGraph"  => Session::get('BA4010InspectionDateFromForGraph')
		];

		//■⑩store Inspection Date（To） for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010InspectionDateToForGraph'))) {

			if (Input::has('txtInspectionDateToForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010InspectionDateToForGraph', Input::get('txtInspectionDateToForGraph'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA4010InspectionDateToForGraph', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateToForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010InspectionDateToForGraph', Input::get('txtInspectionDateToForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateToForGraph"  => Session::get('BA4010InspectionDateToForGraph')
		];

		//■⑪store Condition for Graph
		//in case value in session does not exit
		if (is_null(Session::get('BA4010ConditionForGraph'))) {

			if (Input::has('cmbConditionForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010ConditionForGraph', Input::get('cmbConditionForGraph'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA4010ConditionForGraph', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbConditionForGraph')) {
				//if value of screen exists,write down in session
				Session::put('BA4010ConditionForGraph', Input::get('cmbConditionForGraph'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ConditionForGraph"  => Session::get('BA4010ConditionForGraph')
		];

		//if you forgot the return value, there will show error "unsupported operand type"
		return $pViewData;
	}


	//**************************************************************************
	// process       setProcessList★★
	// overview      store and return form input information of search result list screen
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function setProcessList($pViewData)
	{
		$lArrProcessList     = ["" => ""];  //view data to return

		//if cutomer list does not exist in session, search and store in session
		if (is_null(Session::get('BA4010SetProcessDropdownListData'))) {

			//search Inspection Tool Class
			$lArrProcessList = $this->getProcessList();

			//store in session
			Session::put('BA4010SetProcessDropdownListData', $lArrProcessList); //
		}
		else
		{
			//get from session
			$lArrProcessList = Session::get('BA4010SetProcessDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process       getProcessList★★
	// overview      get data for process data combo box with SQL
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function getProcessList()
	{
		$lTblProcessList			= []; //result of getting information of code order name
		$lRowProcessList			= []; //work when it change to array
		$lArrDataProcessList		= []; //List of code order name to return

		$lTblProcessList = DB::select(
		'
			SELECT PROCESS.CODE_ORDER
			     , PROCESS.CODEORDER_NAME
			     , PROCESS.DISPLAY_ORDER
			  FROM TCODEMST AS PROCESS
			 WHERE CODE_NO = "002"
			 ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblProcessList = (array)$lTblProcessList;

		//add blank space
		$lArrDataProcessList += [
			"" => ""
		];

		//data exist
		if ($lTblProcessList != null)
		{
			//store result in Array again
			foreach ($lTblProcessList as $lRowProcessList) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowProcessList = (Array)$lRowProcessList;

				$lArrDataProcessList += [
					$lRowProcessList["CODE_ORDER"] => $lRowProcessList["CODEORDER_NAME"]
				];
			}
		}

		return $lArrDataProcessList;
	}


	//**************************************************************************
	// process       setCustomerList★★
	// overview      store and return form input information of search result list screen
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function setCustomerList($pViewData)
	{
		$lArrCustomerList     = ["" => ""];  //view data to return

		//if cutomer list does not exist in session, search and store in session
		if (is_null(Session::get('BA4010SetCustemorDropdownListData'))) {
			//search
			$lArrCustomerList = $this->getCustomerList();

			//store in session
			Session::put('BA4010SetCustemorDropdownListData', $lArrCustomerList);
		}
		else
		{
			//get from session
			$lArrCustomerList = Session::get('BA4010SetCustemorDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process       getCustomerList★★
	// overview      get data for customer combo box with SQL
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function getCustomerList()
	{
		$lTblCustomerList			= []; //result of getting information of customer
		$lRowCustomerList			= []; //work when it change to array
		$lArrDataCustomerList		= []; //List of customer to return

		$lTblCustomerList = DB::select(
		'
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TCUSTOMM AS CUST
		       WHERE CUST.DELETE_FLG = "0"
		    ORDER BY CUST.DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCustomerList = (array)$lTblCustomerList;

		//add blank space
		$lArrDataCustomerList += [
			"" => ""
		];

		//data exist
		if ($lTblCustomerList != null)
		{
			//store result in Array again
			foreach ($lTblCustomerList as $lRowCustomerList) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCustomerList = (Array)$lRowCustomerList;

				$lArrDataCustomerList += [
					$lRowCustomerList["CUSTOMER_ID"] => $lRowCustomerList["CUSTOMER_NAME"]
				];
			}
		}

		return $lArrDataCustomerList;
	}

	//**************************************************************************
	// process         getCustomerNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************

	private function setCustomerNoListEntry($pViewData, $pMasterNo, $pProcessId, $pViewDataKey)
	{

       $lArrCustomerNoList     = ["" => ""];  //view data to return

	   //search
	   $lArrCustomerNoList = $this->getCustomerNoListEntry($pMasterNo, $pProcessId);

	   $pViewData += [
			$pViewDataKey => $lArrCustomerNoList
		];
 
		return $pViewData;
	}


	//**************************************************************************
	// process         getCustomerNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************
	private function getCustomerNoListEntry($pMasterNo, $pProcessId)
	{
        $lTblCustomerNo			= []; //result of getting infomation of Inspection No.
		$lRowCustomerNo			= []; //work when it change to array
		$lArrCustomerNo			= []; //List of Inspection No. to return
		$lRowCountCustomerNo		= 0;  //

		$lTblCustomerNo = DB::select('
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TISHEETM AS SHET
	 LEFT OUTER JOIN TCUSTOMM CUST
		          ON CUST.CUSTOMER_ID = SHET.CUSTOMER_ID
		        
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		    GROUP BY SHET.CUSTOMER_ID
		    ORDER BY SHET.REV_NO DESC
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCustomerNo = (array)$lTblCustomerNo;

		//data exist
		if ($lTblCustomerNo != null)
		{
			//store result in Array again
			foreach ($lTblCustomerNo as $lRowCustomerNo) {
				$lRowCountCustomerNo = $lRowCountCustomerNo + 1;
			}

			if ($lRowCountCustomerNo != 1)
			{
				//add blank space
				$lArrCustomerNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblCustomerNo as $lRowCustomerNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCustomerNo = (Array)$lRowCustomerNo;

				$lArrCustomerNo += [
					$lRowCustomerNo["CUSTOMER_ID"] => $lRowCustomerNo["CUSTOMER_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrCustomerNo += [
				"" => ""
			];

		}	
       return $lArrCustomerNo;

	}

	//**************************************************************************
	// process         getCustomerID
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************
	private function getCustomerID($pMasterNo, $pProcessId)
	{
		$lTblCustomerID			= [];
		
		$lTblCustomerID = DB::select('
		      SELECT SHET.CUSTOMER_ID
		        FROM TISHEETM AS SHET
		        
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		    GROUP BY SHET.CUSTOMER_ID
		    ORDER BY SHET.REV_NO DESC
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
				]
		);
		
		return $lTblCustomerID;
	}

	//**************************************************************************
	// process         setCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCheckSheetNoListEntry($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoListEntry($pMasterNo, $pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetNoListEntry($pMasterNo, $pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return
		$lRowCountCheckSheetNo		= 0;  //

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				$lRowCountCheckSheetNo = $lRowCountCheckSheetNo + 1;
			}

			if ($lRowCountCheckSheetNo != 1)
			{
				//add blank space
				$lArrCheckSheetNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}

		}
		else
		{
			//add blank space
			$lArrCheckSheetNo += [
				"" => ""
			];
		}

		return $lArrCheckSheetNo;
	}

	//**************************************************************************
	// process       setCheckSheetNoList★★
	// overview      store and return form input information of search result list screen
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function setCheckSheetNoList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoList($pMasterNo, $pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process       getCheckSheetNoList★★
	// overview      get data for Check Sheet No combo box with SQL
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function getCheckSheetNoList($pMasterNo, $pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//add blank space
		$lArrCheckSheetNo += [
			// "" => ""
		];

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}
		}
		return $lArrCheckSheetNo;
	}


	//**************************************************************************
	// process         setRevNoList★★
	// overview        store and return form input information of search result list screen
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function setRevNoList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lArrRevNoList     = ["" => ""];  //view data to return

		//search
		$lArrRevNoList = $this->getRevNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo);

		//add to array for transportion to screen(written in +=)
		$pViewData += [
			"arrRevNoForGraphList" => $lArrRevNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getRevNoList★★
	// overview        get data for Rev No combo box with SQL
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function getRevNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lTblRevNo			= []; //result of getting infomstion of revision No.
		$lRowRevNo			= []; //work when it change to array
		$lArrRevNo			= []; //List of revision No. to return
		$lRowCountRevNo		= 0;  //

		$lTblRevNo = DB::select('
		      SELECT SHET.REV_NO
		            ,SHET.REV_NO AS REV_NO_NAME
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = :InspectionSheetNo
		    ORDER BY SHET.DISPLAY_ORDER
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
					"InspectionSheetNo" => $pInspectionSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblRevNo = (array)$lTblRevNo;

		//data exist
		if ($lTblRevNo != null)
		{
			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				$lRowCountRevNo = $lRowCountRevNo + 1;
			}

			if ($lRowCountRevNo != 1)
			{
				//add blank space
				$lArrRevNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowRevNo = (Array)$lRowRevNo;

				$lArrRevNo += [
					$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrRevNo += [
				"" => ""
			];
		}

		return $lArrRevNo;
	}


	//**************************************************************************
	// process       setMaterialNameList★★
	// overview      store and return form input information of search result list screen
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function setMaterialNameList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialNameList     = ["" => ""];  //view data to return

		//search
		$lArrMaterialNameList = $this->getMaterialNameList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialNameList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process       getMaterialNameList★★
	// overview      get data for Material Name combo box with SQL
	// argument      
	// return value  
	// author        
	// date          
	//**************************************************************************
	private function getMaterialNameList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialName			= []; //result of getting infomation of Material Name
		$lRowMaterialName			= []; //work when it change to array
		$lArrMaterialName			= []; //List of Material Name to return
		$lRowCountMaterialName		= 0;  //

		$lTblMaterialName = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG          = "0"
		         AND SHET.MASTER_NO           = :MasterNo
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)   /* value of input in screen　無ければ条件にしない */
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"       => $pMasterNo,
					"ProcessId"      => $pProcessId,
					"CustomerId"     => $pCustomerId,
					"CheckSheetNo1"  => $pCheckSheetNo,
					"CheckSheetNo2"  => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialName = (array)$lTblMaterialName;

		//data exist
		if ($lTblMaterialName != null)
		{
			//store result in Array again
			foreach ($lTblMaterialName as $lRowMaterialName) {
				$lRowCountMaterialName = $lRowCountMaterialName + 1;
			}

			if ($lRowCountMaterialName != 1)
			{
				//add blank space
				$lArrMaterialName += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialName as $lRowMaterialName) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowMaterialName = (Array)$lRowMaterialName;

				$lArrMaterialName += [
					$lRowMaterialName["MATERIAL_NAME"] => $lRowMaterialName["MATERIAL_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrMaterialName += [
				"" => ""
			];
		}

		return $lArrMaterialName;
	}


	//**************************************************************************
	// process         setMaterialSizeList★★
	// overview        store and return form input information of search result list screen
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function setMaterialSizeList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialSizeList     = ["" => ""];  //view data to return

		//search
		$lArrMaterialSizeList = $this->getMaterialSizeList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialSizeList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getMaterialSizeList★★
	// overview        get data for Material Size combo box with SQL
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function getMaterialSizeList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialSize			= []; //result of getting infomation of Material Size
		$lRowMaterialSize			= []; //work when it change to array
		$lArrMaterialSize			= []; //List of Material Size to return
		$lRowCountMaterialSize		= 0;  //

		$lTblMaterialSize = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG          = "0"
		         AND SHET.MASTER_NO           = :MasterNo
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)   /* value of input in screen　無ければ条件にしない */
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"         => $pMasterNo,
					"ProcessId"        => $pProcessId,
					"CustomerId"       => $pCustomerId,
					"CheckSheetNo1"    => $pCheckSheetNo,
					"CheckSheetNo2"    => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialSize = (array)$lTblMaterialSize;

		//data exist
		if ($lTblMaterialSize != null)
		{
			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize) {
				$lRowCountMaterialSize = $lRowCountMaterialSize + 1;
			}

			if ($lRowCountMaterialSize != 1)
			{
				//add blank space
				$lArrMaterialSize += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowMaterialSize = (Array)$lRowMaterialSize;

				$lArrMaterialSize += [
					$lRowMaterialSize["MATERIAL_SIZE"] => $lRowMaterialSize["MATERIAL_SIZE"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrMaterialSize += [
				"" => ""
			];
		}

		return $lArrMaterialSize;
	}


	//**************************************************************************
	// process         setAnalyticalGroupList★★
	// overview        store and return form input information of search result list screen
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function setAnalyticalGroupList($pViewData, $pCheckSheetNo)
	{
		$lArrDataListAnalyticalGroup     = ["" => ""];  //list of Inspection Time to return to screen

		//if list of Inspection Time does not exist in session, search and store in session
		if (is_null(Session::get('BA4010AnalyticalGroupDropdownListData')))
		{
			//search inspection time
			$lArrDataListAnalyticalGroup = $this->getAnalyticalGroupList($pCheckSheetNo);

			//sessionにstore
			//Session::put('BA4010AnalyticalGroupDropdownListData', $lArrDataListAnalyticalGroup);
		}
		else
		{
			//pop up data in session
			$lArrDataListAnalyticalGroup = Session::get('BA4010AnalyticalGroupDropdownListData');
		}
		
		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListAnalyticalGroup" => $lArrDataListAnalyticalGroup
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getAnalyticalGroupList★★
	// overview        get data combo box with SQL
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function getAnalyticalGroupList($pCheckSheetNo)
	{
		$lTblAnalyticalGroup         = [];         //DataTable（for Inspection No data）
		$lRowAnalyticalGroup         = [];         //DataRow（for Inspection No data）
		$lArrDataListAnalyticalGroup = ["" => ""]; //list of Inspection No to return to screen

		$lTblAnalyticalGroup = DB::select('
			  SELECT ANALYTICAL_GRP
			    FROM TINSPNOM AS IPNO
		  INNER JOIN TINSNTMM AS INTM
		          ON IPNO.MASTER_NO           = INTM.MASTER_NO
		         AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
		         AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
		         AND IPNO.REV_NO              = INTM.REV_NO
		         AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
		         AND INTM.GRAPH_OUTPUT_FLG    = "1"
		         AND INTM.DELETE_FLG          = "0"
			   WHERE IPNO.INSPECTION_SHEET_NO      = :InspectionSheetNo
			     AND IPNO.DELETE_FLG               = "0"
			ORDER BY IPNO.DISPLAY_ORDER
		',
				[
					"InspectionSheetNo" => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblAnalyticalGroup = (array)$lTblAnalyticalGroup;

		//data exist
		if ($lTblAnalyticalGroup != null)
		{
			//store result in Array again
			foreach ($lTblAnalyticalGroup as $lRowAnalyticalGroup)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowAnalyticalGroup = (Array)$lRowAnalyticalGroup;

				$lArrDataListAnalyticalGroup += [
					$lRowAnalyticalGroup["ANALYTICAL_GRP"] => $lRowAnalyticalGroup["ANALYTICAL_GRP"]
				];
			}
		}

		return $lArrDataListAnalyticalGroup;
	}


	//**************************************************************************
	// process         setConditionList★★
	// overview        display list of Condition on screen.control value in session
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function setConditionList($pViewData)
	{

		$lArrDataListCondition     = ["" => ""];  //list of condition data to return to screen

		//if list of Condition in session,search and store in session
		if (is_null(Session::get('BA4010ConditionDropdownListData')))
		{
			//search inspection time
			$lArrDataListCondition = $this->getConditionList();

			//sessionにstore
			Session::put('BA4010ConditionDropdownListData', $lArrDataListCondition);
		}
		else
		{
			//pop up data in session
			$lArrDataListCondition = Session::get('BA4010ConditionDropdownListData');
		}

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListCondition" => $lArrDataListCondition
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getConditionList★★
	// overview        get data for Material Size combo box with SQL
	// argument        
	// return value    
	// date            
	// remarks         
	//**************************************************************************
	private function getConditionList()
	{
		$lTblCondition         = [];         //DataTable（for Condition data）
		$lRowCondition         = [];         //DataRow（for Condition data）
		$lArrDataListCondition = ["" => ""]; //list of condition data to return to screen

		$lTblCondition = DB::select('
			  SELECT CODE_ORDER
			        ,CODEORDER_NAME
			    FROM TCODEMST
			   WHERE CODE_NO = "001"
			ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCondition = (array)$lTblCondition;

		//data exist
		if ($lTblCondition != null)
		{

			//store result in Array again
			foreach ($lTblCondition as $lRowCondition)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCondition = (Array)$lRowCondition;

				$lArrDataListCondition += [
					$lRowCondition["CODE_ORDER"] => $lRowCondition["CODEORDER_NAME"]
				];
			}
		}

		return $lArrDataListCondition;
	}

	//**************************************************************************
	// process    convertDateFormat
	// overview      change data type
	//           
	// argument      date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//           pattern(1:English->Japanese 2:Japanese->English)
	// return value    Nothing
	// author    s-miyamoto
	// date    2014.07.30
	// record of updates  2014.07.30 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{

		//not to entry date
		if ($pTargetDate == "") {
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1") {

			return date_format($lWorkDate, 'Y/m/d');

		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');

	}

	//**************************************************************************
	// process         simpleDistribution()
	// overview        CPK Chart
	// return value    Bar on CPK Chart (Array)
	// author          Nawaphat(Mai^^)
	// date            2017.10.03
	//**************************************************************************
	private function simpleDistribution($INSPECTION_RESULT_NO,$ANALYTICAL_GRP,$CONDITION_CD){
	  
		$masterNo  = TRIM((String)Input::get('txtMasterNoForGraph'));
		$checkSheetNo = TRIM((String)Input::get('cmbCheckSheetNoForGraph'));
		$processId = TRIM((String)Input::get('cmbProcessForGraph'));
		$revisionNo = TRIM((String)Input::get('cmbRevisionNoForGraph'));
		$analyticalGroup  = $ANALYTICAL_GRP;
		$materialName = TRIM((String)Input::get('cmbMaterialNameForGraph'));
		$materialSize = TRIM((String)Input::get('cmbMaterialSizeForGraph'));
		$customerId = TRIM((String)Input::get('cmbCustomerForGraph'));
		$InspectionResultNo = $INSPECTION_RESULT_NO;
		$condition  = TRIM((String)Input::get('cmbConditionForGraph'));

 		$arrInspectionResult          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		if($CONDITION_CD == "Normal"){
			$condition    = "01";
		}else{
			$condition    = "02";
		}
		if (Input::has('cmbRadioChoosePeriod'))
		{
			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);
			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");
		}
		else
		{
			//change English type->Japanese type（because MySQL store as Japanese type）
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
		}
			$arrInspectionResult = DB::select('
					 SELECT ANALYSIS.INSPECTION_RESULT_NO
					 		,TRESD.INSPECTION_NO     
						    ,TRESD.INSPECTION_RESULT_VALUE
							,ANALYSIS.ANALYTICAL_GRP 
							,ANALYSIS.CONDITION_CD
					  FROM 	TRESANAL 			AS ANALYSIS
				INNER JOIN 	TCODEMST			AS PROCESS
				   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
				   	   AND	PROCESS.CODE_NO 				= "002"		
				INNER JOIN  TINSPNOM		    AS NOM 
					    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
					   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
				INNER JOIN  TINSPTIM 			AS TIMEE 
						ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
				INNER JOIN  TRESDETT            AS TRESD
				        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESD.INSPECTION_RESULT_NO
				       AND	ANALYSIS.ANALYTICAL_GRP         = TRESD.ANALYTICAL_GRP
				INNER JOIN  TUSERMST            AS TUSER
				        ON  TRESD.LAST_UPDATE_USER_ID       = TUSER.USER_ID
				INNER JOIN  TRESHEDT            AS TRESH 
				        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
				INNER JOIN  TISHEETM            AS TISH
						ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
					   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
					   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
					   AND	NOM.REV_NO                		= TISH.REV_NO
				INNER JOIN  TCUSTOMM            AS TCUS
						ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
				INNER JOIN  TMACHINM            AS TMAC	
						ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
					 WHERE 	ANALYSIS.MASTER_NO 				LIKE 	:MasterNo
					   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
				   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
					   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
					   AND  ANALYSIS.CONDITION_CD 			LIKE    :Condition
					   AND  ANALYSIS.ANALYTICAL_GRP 		LIKE    :AnalyticalGroup
					   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
					   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
					   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
					   AND  ANALYSIS.INSPECTION_RESULT_NO   LIKE    :InspectionResultNo
					   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
					   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
					   AND 	TIMEE.DELETE_FLG				=		"0"
					   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
				  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD,TRESD.INSPECTION_NO  ASC
				',
					[
						"MasterNo"			=> $masterNo,
						"ProcessId"			=> $processId,
						"CheckSheetNo"  	=> $checkSheetNo,
						"RevisionNo"    	=> $revisionNo,
						"Condition"			=> $condition,
						"AnalyticalGroup"   => $analyticalGroup,
						"CustomerId"        => $customerId,
						"MaterialName"      => $materialName,
						"MaterialSize"      => $materialSize,
						"InspectionResultNo"=> $InspectionResultNo,
						"DateFrom1"      	=> $lInspectionDateFromForGraph,
						"DateFrom2"      	=> $lInspectionDateFromForGraph,
						"DateFrom3"      	=> $lInspectionDateFromForGraph,
						"DateTo1"        	=> $lInspectionDateToForGraph,
						"DateTo2"        	=> $lInspectionDateToForGraph,
						"DateTo3"        	=> $lInspectionDateToForGraph,
						
					]
				);

		foreach ($arrInspectionResult as $key => $value) {
			  $arrInspectionResultValue[] = $value->INSPECTION_RESULT_VALUE;
		}

		sort($arrInspectionResultValue);
		$Interval = 0.05;
		$first = reset($arrInspectionResultValue);
	    $last = end($arrInspectionResultValue);
	    if($first >= $last){
	    	$ClassInterval = ($first - $last) / 0.05;
	    }else{
	    	$ClassInterval = ($last - $first) / 0.05;
	    }

		$lowerBoundary = []; 
		$upperBoundary = [];
        for($i = 0; $i <= ceil($ClassInterval); $i++){
            if($i == 0){
            		$lowerBoundary[] = $first;
            }else{
            		$lowerBoundary[] = $lowerBoundary[$i-1] + 0.05;
            }
        
            $upperBoundary[] = $lowerBoundary[$i] + 0.05;
        }

		$frequency = [];
        for($j = 0; $j < sizeof($arrInspectionResultValue); $j++){
			for ($i = 0; $i <= ceil($ClassInterval); $i++) {   
			    if($arrInspectionResultValue[$j] >= $lowerBoundary[$i] && $arrInspectionResultValue[$j] < $upperBoundary[$i])
			    {
					$frequency[] = $lowerBoundary[$i];
					$MiddlePoint[] = $lowerBoundary[$i] + ((number_format($upperBoundary[$i] - $lowerBoundary[$i], 2, '.', '')/2));
			     } 	
			}
		}

		$frequency = array_replace($frequency,$MiddlePoint);
   		$new_array = array_map('strval', $frequency);
		$frequency = array_count_values($new_array);
  
        foreach ($frequency as $key => $value) {
        	$arrDataSimpleDistribution[] = [$key,$value];
        }
	
	    return $arrDataSimpleDistribution;
	}

	//**************************************************************************
	// process         getBellCurve()
	// overview        CPK Chart
	// return value    Curve on CPK Chart (Array)
	// author          Nawaphat(Mai^^)
	// date            2017.10.03
	//**************************************************************************
	private function getBellCurve($INSPECTION_RESULT_NO,$ANALYTICAL_GRP,$STDEV_VALUE,$AVERAGE_VALUE){
		$Min4Sigma = $AVERAGE_VALUE - (4 * $STDEV_VALUE);
		$Max4Sigma = $AVERAGE_VALUE + (4 * $STDEV_VALUE);
		$Min3Sigma = $AVERAGE_VALUE - (3 * $STDEV_VALUE);
		$Max3Sigma = $AVERAGE_VALUE + (3 * $STDEV_VALUE);
		$Min2Sigma = $AVERAGE_VALUE - (2 * $STDEV_VALUE);
		$Max2Sigma = $AVERAGE_VALUE + (2 * $STDEV_VALUE);
		$MinSigma = $AVERAGE_VALUE - $STDEV_VALUE;
		$MaxSigma = $AVERAGE_VALUE + $STDEV_VALUE;
        
        //-4sigma to < -3sigma
		for($i = $Min4Sigma; $i < $Min3Sigma; $i = $i + 0.001){
			
			$valueX4[] = $i;
			$valueY4[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine4Sigma = [];
		foreach($valueX4 as $key => $value) {
			    $valueLine4Sigma[$key] = [$valueX4[$key],$valueY4[$key]];
		}
        
        //-3sigma to < -2sigma
        for($i = $Min3Sigma; $i < $Min2Sigma; $i = $i + 0.001){
			
			$valueX3[] = $i;
			$valueY3[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine3Sigma = [];
		foreach($valueX3 as $key => $value) {
			    $valueLine3Sigma[$key] = [$valueX3[$key],$valueY3[$key]];
		}

		//-2sigma to < -sigma
 		for($i = $Min2Sigma; $i < $MinSigma; $i = $i + 0.001){
			
			$valueX2[] = $i;
			$valueY2[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine2Sigma = [];
		foreach($valueX2 as $key => $value) {
			    $valueLine2Sigma[$key] = [$valueX2[$key],$valueY2[$key]];
		}

		//-sigma to < average
		for($i = $MinSigma; $i < $AVERAGE_VALUE; $i = $i + 0.001){
			
			$valueX[] = $i;
			$valueY[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}

		$valueLineSigma = [];
		foreach($valueX as $key => $value) {
			    $valueLineSigma[$key] = [$valueX[$key],$valueY[$key]];
		}

		//average
		$valueYAvergage = number_format($this->normal($AVERAGE_VALUE, $AVERAGE_VALUE, $STDEV_VALUE),3);
		$valueLineAvergage = array(
			  array($AVERAGE_VALUE,$valueYAvergage)
			  );

		//>average to +sigma
		for($i = $MaxSigma; $i > $AVERAGE_VALUE; $i -= 0.001){
			
			$valueXRight[] = $i;
			$valueYRight[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlusSigma = [];
		foreach($valueXRight as $key => $value) {
			    $valueLinePlusSigma[$key] = [$valueXRight[$key],$valueYRight[$key]];
		}
		krsort($valueLinePlusSigma);

		//>+sigma to +2sigma
		for($i = $Max2Sigma; $i > $MaxSigma; $i -= 0.001){
			
			$valueX2Right[] = $i;
			$valueY2Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus2Sigma = [];
		foreach($valueX2Right as $key => $value) {
			    $valueLinePlus2Sigma[$key] = [$valueX2Right[$key],$valueY2Right[$key]];
		}
		krsort($valueLinePlus2Sigma);

		//>+2sigma to +3sigma
		for($i = $Max3Sigma; $i > $Max2Sigma; $i -= 0.001){
			
			$valueX3Right[] = $i;
			$valueY3Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus3Sigma = [];
		foreach($valueX3Right as $key => $value) {
			    $valueLinePlus3Sigma[$key] = [$valueX3Right[$key],$valueY3Right[$key]];
		}
		krsort($valueLinePlus3Sigma);

		//>+3sigma to +4sigma
		for($i = $Max4Sigma; $i > $Max3Sigma; $i -= 0.001){
			
			$valueX4Right[] = $i;
			$valueY4Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus4Sigma = [];
		foreach($valueX4Right as $key => $value) {
			    $valueLinePlus4Sigma[$key] = [$valueX4Right[$key],$valueY4Right[$key]];
		}
		krsort($valueLinePlus4Sigma);

		$valueLine = array_merge($valueLine4Sigma, $valueLine3Sigma, $valueLine2Sigma, $valueLineSigma, $valueLineAvergage, $valueLinePlusSigma, $valueLinePlus2Sigma, $valueLinePlus3Sigma, $valueLinePlus4Sigma);

		 return $valueLine;
	}

	//**************************************************************************
	// process         normal()
	// overview        Calculate Axis Y on CPK Chart
	// return value    Axis Y on CPK Chart
	// author          Nawaphat(Mai^^)
	// date            2017.10.03
	//**************************************************************************
	private function normal($x, $mu, $sigma) {
	    return exp(-0.5 * ($x - $mu) * ($x - $mu) / ($sigma*$sigma))
	        / ($sigma * sqrt(2.0 * M_PI));
	}

	//**************************************************************************
	// process         getResultAllGraph()
	// overview        All Analytical group and all condition
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.04
	//**************************************************************************
	private function getResultAllGraph(){

		$masterNo  = TRIM((String)Input::get('txtMasterNoForGraph'));
		$processId = TRIM((String)Input::get('cmbProcessForGraph'));
		$customerId = TRIM((String)Input::get('cmbCustomerForGraph'));
		$revisionNo = TRIM((String)Input::get('cmbRevisionNoForGraph'));
		$checkSheetNo = TRIM((String)Input::get('cmbCheckSheetNoForGraph'));
		$materialName = TRIM((String)Input::get('cmbMaterialNameForGraph'));
		$materialSize = TRIM((String)Input::get('cmbMaterialSizeForGraph'));
		$analyticalGroup  = TRIM((String)Input::get('cmbAnalyticalGroupForGraph'));
		$condition  = TRIM((String)Input::get('cmbConditionForGraph'));

 		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$AnalyticalGroup                = "";

		if (Input::has('cmbRadioChoosePeriod'))
		{
			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);
			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");
		}
		else
		{
			//change English type->Japanese type（because MySQL store as Japanese type）
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
		}
                
		if($condition == "" and $analyticalGroup == ""){

			$lTblSearchResultData = DB::select('
				SELECT DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSPECTION_YMD
		 			,TIMEE.INSPECTION_TIME_NAME							AS INSPECTION_TIME_NAME
		 			,CASE 
		 				WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
		 				WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
						ELSE ""
						END CONDITION_CD
					,ANALYSIS.ANALYTICAL_GRP                        
					,ANALYSIS.CALC_NUM                             
		 			,ANALYSIS.PICTURE_URL
		 			,ANALYSIS.MAX_VALUE								AS MAX_VALUE
		 			,ANALYSIS.MIN_VALUE								AS MIN_VALUE
					,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE
		 			,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
		 			,ANALYSIS.STDEV                                 AS STDEV_VALUE
		 			,ANALYSIS.STD_MAX_SPEC						AS UPPER_CONTROL_LIMIT
		 			,ANALYSIS.STD_MIN_SPEC						AS LOWER_CONTROL_LIMIT
		 			,ANALYSIS.INSPECTION_RESULT_NO              AS INSPECTION_RESULT_NO
		 			,ANALYSIS.CPK                               AS CPK_VALUE
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO 				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);

		}
		else if($condition != "" and $analyticalGroup == "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSPECTION_YMD
		 			,TIMEE.INSPECTION_TIME_NAME							AS INSPECTION_TIME_NAME
		 			,CASE 
		 				WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
		 				WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
						ELSE ""
						END CONDITION_CD
					,ANALYSIS.ANALYTICAL_GRP                        
					,ANALYSIS.CALC_NUM                             
		 			,ANALYSIS.PICTURE_URL
		 			,ANALYSIS.MAX_VALUE								AS MAX_VALUE
		 			,ANALYSIS.MIN_VALUE								AS MIN_VALUE
					,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE
		 			,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
		 			,ANALYSIS.STDEV                                 AS STDEV_VALUE
		 			,ANALYSIS.STD_MAX_SPEC						AS UPPER_CONTROL_LIMIT
		 			,ANALYSIS.STD_MIN_SPEC						AS LOWER_CONTROL_LIMIT
		 			,ANALYSIS.INSPECTION_RESULT_NO              AS INSPECTION_RESULT_NO
		 			,ANALYSIS.CPK                               AS CPK_VALUE
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  ANALYSIS.CONDITION_CD 			LIKE    :Condition
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"Condition"			=> $condition,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}
		else if($condition == "" and $analyticalGroup != "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSPECTION_YMD
		 			,TIMEE.INSPECTION_TIME_NAME							AS INSPECTION_TIME_NAME
		 			,CASE 
		 				WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
		 				WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
						ELSE ""
						END CONDITION_CD
					,ANALYSIS.ANALYTICAL_GRP                        
					,ANALYSIS.CALC_NUM                             
		 			,ANALYSIS.PICTURE_URL
		 			,ANALYSIS.MAX_VALUE								AS MAX_VALUE
		 			,ANALYSIS.MIN_VALUE								AS MIN_VALUE
					,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE
		 			,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
		 			,ANALYSIS.STDEV                                 AS STDEV_VALUE
		 			,ANALYSIS.STD_MAX_SPEC						AS UPPER_CONTROL_LIMIT
		 			,ANALYSIS.STD_MIN_SPEC						AS LOWER_CONTROL_LIMIT
		 			,ANALYSIS.INSPECTION_RESULT_NO              AS INSPECTION_RESULT_NO
		 			,ANALYSIS.CPK                               AS CPK_VALUE
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND  ANALYSIS.ANALYTICAL_GRP 		LIKE    :AnalyticalGroup
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"AnalyticalGroup"   => $analyticalGroup,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}
		else if($condition != "" and $analyticalGroup != "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSPECTION_YMD
		 			,TIMEE.INSPECTION_TIME_NAME							AS INSPECTION_TIME_NAME
		 			,CASE 
		 				WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
		 				WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
						ELSE ""
						END CONDITION_CD
					,ANALYSIS.ANALYTICAL_GRP                        
					,ANALYSIS.CALC_NUM                             
		 			,ANALYSIS.PICTURE_URL
		 			,ANALYSIS.MAX_VALUE								AS MAX_VALUE
		 			,ANALYSIS.MIN_VALUE								AS MIN_VALUE
					,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE
		 			,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
		 			,ANALYSIS.STDEV                                 AS STDEV_VALUE
		 			,ANALYSIS.STD_MAX_SPEC						AS UPPER_CONTROL_LIMIT
		 			,ANALYSIS.STD_MIN_SPEC						AS LOWER_CONTROL_LIMIT
		 			,ANALYSIS.INSPECTION_RESULT_NO              AS INSPECTION_RESULT_NO
		 			,ANALYSIS.CPK                               AS CPK_VALUE
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  ANALYSIS.CONDITION_CD 			LIKE    :Condition
				   AND  ANALYSIS.ANALYTICAL_GRP 		LIKE    :AnalyticalGroup
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"Condition"			=> $condition,
					"AnalyticalGroup"   => $analyticalGroup,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}

		return $lTblSearchResultData;

	}

	//**************************************************************************
	// process         setRevisionNoList()
	// overview        store and return form input information of search result list screen
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.16
	//**************************************************************************
	private function setRevisionNoList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lArrRevNoList     = ["" => ""];  //view data to return

		//search
		$lArrRevNoList = $this->getRevisionNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo);

		//add to array for transportion to screen(written in +=)
		$pViewData += [
			"arrRevNoForGraphList" => $lArrRevNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setRevisionNoList()
	// overview        get data for Rev No combo box with SQL
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.16
	//**************************************************************************
	private function getRevisionNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lTblRevNo			= []; //result of getting infomstion of revision No.
		$lRowRevNo			= []; //work when it change to array
		$lArrRevNo			= []; //List of revision No. to return
		$lRowCountRevNo		= 0;  //

		$lTblRevNo = DB::select('
		      SELECT SHET.REV_NO
		            ,SHET.REV_NO AS REV_NO_NAME
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    ORDER BY SHET.DISPLAY_ORDER
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblRevNo = (array)$lTblRevNo;

		//data exist
		if ($lTblRevNo != null)
		{
			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				$lRowCountRevNo = $lRowCountRevNo + 1;
			}

			if ($lRowCountRevNo != 1)
			{
				//add blank space
				$lArrRevNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowRevNo = (Array)$lRowRevNo;

				$lArrRevNo += [
					$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrRevNo += [
				"" => ""
			];
		}

		return $lArrRevNo;
	}


	//**************************************************************************
	// process         getDataforExcelSheet()
	// overview        get data for excel sheet
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.30
	//**************************************************************************
	private function getDataforExcelSheet()
	{
		$masterNo  = TRIM((String)Input::get('txtMasterNoForGraph'));
		$processId = TRIM((String)Input::get('cmbProcessForGraph'));
		$customerId = TRIM((String)Input::get('cmbCustomerForGraph'));
		$revisionNo = TRIM((String)Input::get('cmbRevisionNoForGraph'));
		$checkSheetNo = TRIM((String)Input::get('cmbCheckSheetNoForGraph'));
		$materialName = TRIM((String)Input::get('cmbMaterialNameForGraph'));
		$materialSize = TRIM((String)Input::get('cmbMaterialSizeForGraph'));
		$analyticalGroup  = TRIM((String)Input::get('cmbAnalyticalGroupForGraph'));
		$condition  = TRIM((String)Input::get('cmbConditionForGraph'));

 		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$AnalyticalGroup                = "";

		if (Input::has('cmbRadioChoosePeriod'))
		{
			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);
			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");
		}
		else
		{
			//change English type->Japanese type（because MySQL store as Japanese type）
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
		}
                
		if($condition == "" and $analyticalGroup == ""){

			$lTblSearchResultData = DB::select('
				SELECT 	ANALYSIS.MASTER_NO
						,ANALYSIS.PROCESS_ID
						,PROCESS.CODEORDER_NAME
						,TCUS.CUSTOMER_NAME
						,ANALYSIS.INSPECTION_SHEET_NO
						,ANALYSIS.REV_NO
						,TISH.MATERIAL_NAME
						,TISH.MATERIAL_SIZE
						,TMAC.MACHINE_NO
						,TMAC.MACHINE_NAME
						,TRESD.LAST_UPDATE_USER_ID
						,TUSER.USER_NAME
						,TISH.ITEM_NO                     AS PART_NO
						,TISH.ITEM_NAME                   AS PART_NAME
						,DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSERT_DATE
						,TIMEE.INSPECTION_TIME_NAME							AS INSERT_TIME
						,CASE 
							WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
							WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
							ELSE ""
							END CONDITION_CD
						,TRESD.INSPECTION_NO              AS INSPECTION_NO
					    ,TRESD.INSPECTION_RESULT_VALUE
						,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE 
						,ANALYSIS.MIN_VALUE								AS MIN_VALUE  
						,ANALYSIS.MAX_VALUE								AS MAX_VALUE 
						,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
						,ANALYSIS.CPK                               	AS CPK_VALUE  
						,ANALYSIS.STD_MIN_SPEC							AS LOWER_CONTROL_LIMIT
						,ANALYSIS.STD_MAX_SPEC							AS UPPER_CONTROL_LIMIT
						,TRESD.ANALYTICAL_GRP
						,TRESD.ANALYTICAL_GRP_POINT
						,TRESD.ANALYTICAL_GRP_HOWMANY
						,TRESH.LOT_NO
						,TRESH.MATERIAL_LOT_NO
						,TRESH.CERTIFICATE_NO
						,TRESH.PO_NO
						,TRESH.INVOICE_NO
						,DATE_FORMAT(TRESD.UPDATE_YMDHMS,"%Y/%m/%d")	    AS UPDATE_DATE
						
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESDETT            AS TRESD
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESD.INSPECTION_RESULT_NO
			       AND	ANALYSIS.ANALYTICAL_GRP         = TRESD.ANALYTICAL_GRP
			INNER JOIN  TUSERMST            AS TUSER
			        ON  TRESD.LAST_UPDATE_USER_ID       = TUSER.USER_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO   				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD,TRESD.INSPECTION_NO  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);

		}
		else if($condition != "" and $analyticalGroup == "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	ANALYSIS.MASTER_NO
						,ANALYSIS.PROCESS_ID
						,PROCESS.CODEORDER_NAME
						,TCUS.CUSTOMER_NAME
						,ANALYSIS.INSPECTION_SHEET_NO
						,ANALYSIS.REV_NO
						,TISH.MATERIAL_NAME
						,TISH.MATERIAL_SIZE
						,TMAC.MACHINE_NO
						,TMAC.MACHINE_NAME
						,TRESD.LAST_UPDATE_USER_ID
						,TUSER.USER_NAME
						,TISH.ITEM_NO                     AS PART_NO
						,TISH.ITEM_NAME                   AS PART_NAME
						,DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	    AS INSERT_DATE
						,TIMEE.INSPECTION_TIME_NAME							AS INSERT_TIME
						,CASE 
							WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
							WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
							ELSE ""
							END CONDITION_CD
						,TRESD.INSPECTION_NO              AS INSPECTION_NO
					    ,TRESD.INSPECTION_RESULT_VALUE
						,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE 
						,ANALYSIS.MIN_VALUE								AS MIN_VALUE  
						,ANALYSIS.MAX_VALUE								AS MAX_VALUE 
						,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
						,ANALYSIS.CPK                               	AS CPK_VALUE  
						,ANALYSIS.STD_MIN_SPEC							AS LOWER_CONTROL_LIMIT
						,ANALYSIS.STD_MAX_SPEC							AS UPPER_CONTROL_LIMIT
						,TRESD.ANALYTICAL_GRP
						,TRESD.ANALYTICAL_GRP_POINT
						,TRESD.ANALYTICAL_GRP_HOWMANY
						,TRESH.LOT_NO
						,TRESH.MATERIAL_LOT_NO
						,TRESH.CERTIFICATE_NO
						,TRESH.PO_NO
						,TRESH.INVOICE_NO
						,DATE_FORMAT(TRESD.UPDATE_YMDHMS,"%Y/%m/%d")	    AS UPDATE_DATE
						
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESDETT            AS TRESD
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESD.INSPECTION_RESULT_NO
			       AND	ANALYSIS.ANALYTICAL_GRP         = TRESD.ANALYTICAL_GRP
			INNER JOIN  TUSERMST            AS TUSER
			        ON  TRESD.LAST_UPDATE_USER_ID       = TUSER.USER_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  ANALYSIS.CONDITION_CD 			LIKE    :Condition
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD,TRESD.INSPECTION_NO  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"Condition"			=> $condition,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}
		else if($condition == "" and $analyticalGroup != "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	ANALYSIS.MASTER_NO
						,ANALYSIS.PROCESS_ID
						,PROCESS.CODEORDER_NAME
						,TCUS.CUSTOMER_NAME
						,ANALYSIS.INSPECTION_SHEET_NO
						,ANALYSIS.REV_NO
						,TISH.MATERIAL_NAME
						,TISH.MATERIAL_SIZE
						,TMAC.MACHINE_NO
						,TMAC.MACHINE_NAME
						,TRESD.LAST_UPDATE_USER_ID
						,TUSER.USER_NAME
						,TISH.ITEM_NO                     AS PART_NO
						,TISH.ITEM_NAME                   AS PART_NAME
						,DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	AS INSERT_DATE
						,TIMEE.INSPECTION_TIME_NAME							AS INSERT_TIME
						,CASE 
							WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
							WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
							ELSE ""
							END CONDITION_CD
						,TRESD.INSPECTION_NO              AS INSPECTION_NO
					    ,TRESD.INSPECTION_RESULT_VALUE
						,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE 
						,ANALYSIS.MIN_VALUE								AS MIN_VALUE  
						,ANALYSIS.MAX_VALUE								AS MAX_VALUE 
						,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
						,ANALYSIS.CPK                               	AS CPK_VALUE  
						,ANALYSIS.STD_MIN_SPEC							AS LOWER_CONTROL_LIMIT
						,ANALYSIS.STD_MAX_SPEC							AS UPPER_CONTROL_LIMIT
						,TRESD.ANALYTICAL_GRP
						,TRESD.ANALYTICAL_GRP_POINT
						,TRESD.ANALYTICAL_GRP_HOWMANY
						,TRESH.LOT_NO
						,TRESH.MATERIAL_LOT_NO
						,TRESH.CERTIFICATE_NO
						,TRESH.PO_NO
						,TRESH.INVOICE_NO
						,DATE_FORMAT(TRESD.UPDATE_YMDHMS,"%Y/%m/%d")	    AS UPDATE_DATE
						
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESDETT            AS TRESD
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESD.INSPECTION_RESULT_NO
			       AND	ANALYSIS.ANALYTICAL_GRP         = TRESD.ANALYTICAL_GRP
			INNER JOIN  TUSERMST            AS TUSER
			        ON  TRESD.LAST_UPDATE_USER_ID       = TUSER.USER_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND  ANALYSIS.ANALYTICAL_GRP 		LIKE    :AnalyticalGroup
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD,TRESD.INSPECTION_NO  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"AnalyticalGroup"   => $analyticalGroup,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}
		else if($condition != "" and $analyticalGroup != "")
		{
			$lTblSearchResultData = DB::select('
				SELECT 	ANALYSIS.MASTER_NO
						,ANALYSIS.PROCESS_ID
						,PROCESS.CODEORDER_NAME
						,TCUS.CUSTOMER_NAME
						,ANALYSIS.INSPECTION_SHEET_NO
						,ANALYSIS.REV_NO
						,TISH.MATERIAL_NAME
						,TISH.MATERIAL_SIZE
						,TMAC.MACHINE_NO
						,TMAC.MACHINE_NAME
						,TRESD.LAST_UPDATE_USER_ID
						,TUSER.USER_NAME
						,TISH.ITEM_NO                     AS PART_NO
						,TISH.ITEM_NAME                   AS PART_NAME
						,DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%Y/%m/%d")	AS INSERT_DATE
						,TIMEE.INSPECTION_TIME_NAME							AS INSERT_TIME
						,CASE 
							WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal"
							WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal"
							ELSE ""
							END CONDITION_CD
						,TRESD.INSPECTION_NO              AS INSPECTION_NO
					    ,TRESD.INSPECTION_RESULT_VALUE
						,ANALYSIS.AVERAGE_VALUE							AS AVERAGE_VALUE 
						,ANALYSIS.MIN_VALUE								AS MIN_VALUE  
						,ANALYSIS.MAX_VALUE								AS MAX_VALUE 
						,ANALYSIS.RANGE_VALUE                           AS RANGE_VALUE
						,ANALYSIS.CPK                               	AS CPK_VALUE  
						,ANALYSIS.STD_MIN_SPEC							AS LOWER_CONTROL_LIMIT
						,ANALYSIS.STD_MAX_SPEC							AS UPPER_CONTROL_LIMIT
						,TRESD.ANALYTICAL_GRP
						,TRESD.ANALYTICAL_GRP_POINT
						,TRESD.ANALYTICAL_GRP_HOWMANY
						,TRESH.LOT_NO
						,TRESH.MATERIAL_LOT_NO
						,TRESH.CERTIFICATE_NO
						,TRESH.PO_NO
						,TRESH.INVOICE_NO
						,DATE_FORMAT(TRESD.UPDATE_YMDHMS,"%Y/%m/%d")	    AS UPDATE_DATE
						
				  FROM 	TRESANAL 			AS ANALYSIS
			INNER JOIN 	TCODEMST			AS PROCESS
			   		ON 	PROCESS.CODE_ORDER 				= ANALYSIS.PROCESS_ID
			   	   AND	PROCESS.CODE_NO 				= "002"		
			INNER JOIN  TINSPNOM		    AS NOM 
				    ON	ANALYSIS.ANALYTICAL_GRP 	    = NOM.ANALYTICAL_GRP  
				   AND  NOM.ANALYTICAL_GRP_POINT        =  "1"
			INNER JOIN  TINSPTIM 			AS TIMEE 
					ON 	ANALYSIS.INSPECTION_TIME_ID 	= TIMEE.INSPECTION_TIME_ID
			INNER JOIN  TRESDETT            AS TRESD
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESD.INSPECTION_RESULT_NO
			       AND	ANALYSIS.ANALYTICAL_GRP         = TRESD.ANALYTICAL_GRP
			INNER JOIN  TUSERMST            AS TUSER
			        ON  TRESD.LAST_UPDATE_USER_ID       = TUSER.USER_ID
			INNER JOIN  TRESHEDT            AS TRESH 
			        ON  ANALYSIS.INSPECTION_RESULT_NO   = TRESH.INSPECTION_RESULT_NO
			INNER JOIN  TISHEETM            AS TISH
					ON  NOM.MASTER_NO                   = TISH.MASTER_NO  
				   AND	NOM.PROCESS_ID                  = TISH.PROCESS_ID
				   AND	NOM.INSPECTION_SHEET_NO         = TISH.INSPECTION_SHEET_NO
				   AND	NOM.REV_NO                		= TISH.REV_NO
			INNER JOIN  TCUSTOMM            AS TCUS
					ON  TISH.CUSTOMER_ID                = TCUS.CUSTOMER_ID
			INNER JOIN  TMACHINM            AS TMAC	
					ON  TRESH.MACHINE_NO                = TMAC.MACHINE_NO	             
				 WHERE 	TISH.MASTER_NO  				LIKE 	:MasterNo
				   AND 	ANALYSIS.INSPECTION_SHEET_NO 	LIKE 	:CheckSheetNo
			   	   AND 	PROCESS.CODE_ORDER 				LIKE    :ProcessId
				   AND 	ANALYSIS.REV_NO 				LIKE 	:RevisionNo
				   AND  ANALYSIS.CONDITION_CD 			LIKE    :Condition
				   AND  ANALYSIS.ANALYTICAL_GRP 		LIKE    :AnalyticalGroup
				   AND  TISH.CUSTOMER_ID                LIKE    :CustomerId
				   AND  TISH.MATERIAL_NAME              LIKE    :MaterialName
				   AND  TISH.MATERIAL_SIZE              LIKE    :MaterialSize
				   AND 	( ANALYSIS.INSPECTION_YMD 		>= 		IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, ANALYSIS.INSPECTION_YMD)
				   AND 	ANALYSIS.INSPECTION_YMD 		<= 		IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, ANALYSIS.INSPECTION_YMD))
				   AND 	TIMEE.DELETE_FLG				=		"0"
				   AND  NOM.INSPECTION_RESULT_INPUT_TYPE =       "1"
			  ORDER BY  ANALYSIS.ANALYTICAL_GRP, ANALYSIS.INSPECTION_YMD, ANALYSIS.INSPECTION_TIME_ID, ANALYSIS.CONDITION_CD,TRESD.INSPECTION_NO  ASC


			',
				[
					"MasterNo"			=> $masterNo,
					"ProcessId"			=> $processId,
					"CheckSheetNo"  	=> $checkSheetNo,
					"RevisionNo"    	=> $revisionNo,
					"Condition"			=> $condition,
					"AnalyticalGroup"   => $analyticalGroup,
					"CustomerId"        => $customerId,
					"MaterialName"      => $materialName,
					"MaterialSize"      => $materialSize,
					"DateFrom1"      	=> $lInspectionDateFromForGraph,
					"DateFrom2"      	=> $lInspectionDateFromForGraph,
					"DateFrom3"      	=> $lInspectionDateFromForGraph,
					"DateTo1"        	=> $lInspectionDateToForGraph,
					"DateTo2"        	=> $lInspectionDateToForGraph,
					"DateTo3"        	=> $lInspectionDateToForGraph,
					
				]
			);
		}

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         setAnaGrp()
	// overview        set Analitycal Group
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.30
	//**************************************************************************
	private function setAnaGrp($pViewData){

		$lArrDataListAnalyticalGroup     = ["" => ""];  //list of Inspection Time to return to screen

		//if list of Inspection Time does not exist in session, search and store in session
		if (is_null(Session::get('BA4010AnalyticalGroupDropdownListData')))
		{
			//search inspection time
			$lArrDataListAnalyticalGroup = $this->getAnalGrpList();
			
			//sessionにstore
			Session::put('BA4010AnalyticalGroupDropdownListData', $lArrDataListAnalyticalGroup);
		}
		else
		{
			//pop up data in session
			$lArrDataListAnalyticalGroup = Session::get('BA4010AnalyticalGroupDropdownListData');
		}
		
		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListAnalyticalGroup" => $lArrDataListAnalyticalGroup
		];

		return $pViewData;
	}


	//**************************************************************************
	// process         getAnalGrpList()
	// overview        get Analitycal Group
	// return value    
	// author          Nawaphat(Mai^^)
	// date            2017.10.30
	//**************************************************************************
	private function getAnalGrpList()
	{
		$lTblAnalyticalGroup         = [];         //DataTable（for Inspection No data）
		$lRowAnalyticalGroup         = [];         //DataRow（for Inspection No data）
		$lArrDataListAnalyticalGroup = ["" => ""]; //list of Inspection No to return to screen

		$lTblAnalyticalGroup = DB::select('
			  SELECT DISTINCT ANALYTICAL_GRP
			    FROM TINSPNOM AS IPNO
		  INNER JOIN TINSNTMM AS INTM
		          ON IPNO.MASTER_NO           = INTM.MASTER_NO
		         AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
		         AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
		         AND IPNO.REV_NO              = INTM.REV_NO
		         AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
		         AND INTM.GRAPH_OUTPUT_FLG    = "1"
		         AND INTM.DELETE_FLG          = "0"
			   WHERE IPNO.DELETE_FLG          = "0"
			ORDER BY IPNO.DISPLAY_ORDER
		');

		//cast search result to array（two dimensions Array）
		$lTblAnalyticalGroup = (array)$lTblAnalyticalGroup;

		//data exist
		if ($lTblAnalyticalGroup != null)
		{
			//store result in Array again
			foreach ($lTblAnalyticalGroup as $lRowAnalyticalGroup)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowAnalyticalGroup = (Array)$lRowAnalyticalGroup;

				$lArrDataListAnalyticalGroup += [
					$lRowAnalyticalGroup["ANALYTICAL_GRP"] => $lRowAnalyticalGroup["ANALYTICAL_GRP"]
				];
			}
		}

		return $lArrDataListAnalyticalGroup;
	}

}