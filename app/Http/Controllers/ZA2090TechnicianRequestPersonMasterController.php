<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Log;
use Session;
use Validator;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    maintenance Technician Request Person master
// over view      maintenance Technician Request Person master
// programer    k-kagawa
// date    2014.12.06
// update  
//           
//**************************************************************************
class ZA2090TechnicianRequestPersonMasterController
extends Controller
{

	//-------------
	//■■define constance
	CONST NUMBER_PER_PAGE = 10;		//number of data per 1 page

	//**************************************************************************
	// processing name    MasterAction
	// over view      display initial screen
	//           separate processing as Entry,Search,Modify button
	//           do processing corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion of data to screen
		
		$lTblSearchResultData		= []; //data table of inspection result list
		$lPagenation				= []; //for paging

		$lTblMasterCheck 			= []; //for master existance check

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //lock mode of screen
		$lPrevMode					= ""; //lock mode of screen before transition

		//store and re-set entry item
		$lViewData = $this->keepFromInputValue($lViewData);

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"Machine Name"  => Input::get('txtTrPersonForSearch'   ,''),
					"M/C No."       => Input::get('txtMachineNoForSearch'      ,''),
				]
			);

			//in case of no data,search
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search
				$lTblSearchResultData = $this->getSearchMasterData();
				
				//in case of no data,error
				if (count($lTblSearchResultData) == 0)
				{
					//set error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				
				//store in session
				Session::put('ZA2090SearchResultData', $lTblSearchResultData);

				//set lock mode in screen
				$lMode = "Search";
				Session::put('ZA2090ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//make value in edit field initial value

			//exchange session to initial value
			Session::put('ZA2090TrPersonIdForEntry', "");
			Session::put('ZA2090TrPersonNameForEntry', "");
			Session::put('ZA2090DisplayOrderForEntry', "");

			//exchange view data to initial value
			$lViewData["TrPersonIdForEntry"] = "";
			$lViewData["TrPersonNameForEntry"] = "";
			$lViewData["DisplayOrderForEntry"] = "";

			//set lock mode in screen
			$lMode = "NewAdd";
			Session::put('ZA2090ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"M/C No."       => Input::get('txtTrPersonIdForEntry'       ,''),
					"M/C Name"      => Input::get('txtTrPersonNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2090ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2090ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate processing corresponding to prevent screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case new entry
					//get data for logic check
					$lTblMasterCheck = $this->getMasterCheckData(Input::get('txtTrPersonIdForEntry'),0);

					//in case data does not exist,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT
						$lSuccessFlg = $this->insertMasterData();

						//in case update successfully, display message and return to initial screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode in screen
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2090ActionMode', "");
						}
						else
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2090ActionMode', $lPrevMode);
						}
					}
					else
					//in case data exists
					{
						//exchange result to array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//in case delete flag is valid,make data valid for re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  TRIM(Input::get('txtTrPersonIdForEntry'))
																	 ,TRIM(Input::get('txtTrPersonNameForEntry'))
																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2090ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2090ActionMode', $lPrevMode);
							}
						}
						else
						//in case delete flag is invalid,key reduplication error
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2090ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result data
					$lTblSearchResultData = Session::get('ZA2090SearchResultData');

					//set list data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//change corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;

						//get data corresponding to machine No. in edit field and start process
						if(TRIM(Input::get('txtTrPersonIdForEntry')) == TRIM((String)$lArrDataRow["TECHNICIAN_USER_ID"]))
						{
							//get data for logic check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["TECHNICIAN_USER_ID"])
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case of getting data,change corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//in case data does not exist or version is not same,error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2090ActionMode', $lPrevMode);
							}
							else
							//in case of no error,start to update
							{
								//update
								$lKohshinCount = $this->updateMasterData(
																		  TRIM(Input::get('txtTrPersonIdForEntry'))
																		 ,TRIM(Input::get('txtTrPersonNameForEntry'))
																		 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																		 ,"0"
																		);

								//in case update successfully, display message and return to initial screen
								if ($lKohshinCount != 0)
								{
									//finishing message
									$lViewData["NormalMessage"] = "I005 : Process has been completed.";

									//set lock mode in screen
									$this->initializeSessionData();
									$lMode = "";
									Session::put('ZA2090ActionMode', "");
								}
								else
								{
									//set error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E999 : System error has occurred. Contact your system manager."
									]);
									//keep the same condition to before update button is push for screen lock mode
									$lMode = $lPrevMode;
									Session::put('ZA2090ActionMode', $lPrevMode);
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"M/C No."       => Input::get('txtTrPersonIdForEntry'       ,''),
					"M/C Name"      => Input::get('txtTrPersonNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2090ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2090ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result data
				$lTblSearchResultData = Session::get('ZA2090SearchResultData');

				//set list data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//change corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to machine No. in edit field and start process
					if(TRIM(Input::get('txtTrPersonIdForEntry')) == TRIM((String)$lArrDataRow["TECHNICIAN_USER_ID"]))
					{
						//get data for logic check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["TECHNICIAN_USER_ID"])
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case of getting data,change corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//in case data does not exist or version is not same,error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2090ActionMode', $lPrevMode);
						}
						else
						//in case of no error,start to update
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["TECHNICIAN_USER_ID"]
																	 ,$lArrDataRow["TECHNICIAN_USER_NAME"]
																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2090ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2090ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidTrPersonId"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key in corresponding line
			$lTrPersonId = Input::get('hidPrimaryKey1');
			//get search result data
			$lTblSearchResultData = Session::get('ZA2090SearchResultData');

			//set list data in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//change corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//in case machin No. exists,write over on lViewData and session to set in edit field
				if(TRIM((String)$lTrPersonId) == TRIM((String)$lArrDataRow["TECHNICIAN_USER_ID"]))
				{
					//write down in session
					Session::put('ZA2090TrPersonIdForEntry', $lArrDataRow["TECHNICIAN_USER_ID"]);
					Session::put('ZA2090TrPersonNameForEntry', $lArrDataRow["TECHNICIAN_USER_NAME"]);
					Session::put('ZA2090DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["TrPersonIdForEntry"] = Session::get('ZA2090TrPersonIdForEntry');
					$lViewData["TrPersonNameForEntry"] = Session::get('ZA2090TrPersonNameForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2090DisplayOrderForEntry');
				}
			}

			//set lock mode in screen
			$lMode = "Edit";
			Session::put('ZA2090ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
		
			//clear all information except entry information in this screen session
			//in case URL in origin of transition does not include"index.php/user/trpersonmaster",False
			//in case origin of transition is other,return string
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/trpersonmaster');

				if($lPrevURL == false)
				{
					//delete all search information
					$this->initializeSessionData();

					//set lock mode in screen
					$lMode = "";
					Session::put('ZA2090ActionMode', "");
				}
				else
				{
					//if search result data does not exist in session,issue  blank
					if (is_null(Session::get('ZA2090SearchResultData')))
					{
						//set lock mode in screen
						$lMode = "";
						Session::put('ZA2090ActionMode', "");
					}
					else
					{
						//set lock mode in screen
						$lMode = "Search";
						Session::put('ZA2090ActionMode', "Search");
					}
				}
			}
		}

		//if search result data does not exist in session,issue  blank
		if (is_null(Session::get('ZA2090SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//get search result data from session
			$lTblSearchResultData = Session::get('ZA2090SearchResultData');
		}

		//make pagenation
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/trpersonmaster')); 
		 //data,total issue, issue per 1 page
		
		//★★★★
		//add to array for transportion to screen
		//$lViewData += [
		//	"Pagenator"       => $lPagenation,
		//];

		//add to array for transportion to screen
		$lViewData += [
			"PersonCount"     => Count($lTblSearchResultData),
			"Pagenator"       => $lPagenation,
		];

		//set lock information in screen
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2090ActionMode'));
		
		return View("user.trpersonmaster", $lViewData);

	}

	//**************************************************************************
	// processing name    lockControls
	// over view      ボタン等の画面の各項目のロック情報を設定する。
	// parameter      ビューデータ・画面のモード
	// returned value    ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
				];
				break;
			case 'NewAdd':
				//新規追加 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "True",
				];
				break;
			case 'Edit':
				//編集 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "Lock",
					"DeleteLock"   => "",
					"EditVisible"  => "True",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getMasterCheckData
	// over view      論理キー重複データ件数取得
	//           検査実績ヘッダが論理キーで重複するデータの件数を返却する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pTrPersonId,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  TEUR.DATA_REV
		           ,TEUR.DELETE_FLG
		      FROM TTECUSRT AS TEUR															/* OFFSET理由/NG理由分類マスタ */
		     WHERE TEUR.TECHNICIAN_USER_ID          = :TrPersonId							/* 画面入力値　無ければ条件にしない */
		       AND TEUR.DATA_REV            = IF(:DataRev1 <> 0, :DataRev2, TEUR.DATA_REV)	/* 画面選択値　無ければ条件にしない */
		',
			[
				"TrPersonId"   => $pTrPersonId,
				"DataRev1"     => $pDataRev,
				"DataRev2"     => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    getSearchMasterData
	// over view      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programer    k-kagawa
	//**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table of inspection result list

		$lTblSearchResultData = DB::select('
		    SELECT TEUR.TECHNICIAN_USER_ID
		          ,TEUR.TECHNICIAN_USER_NAME
		          ,TEUR.DISPLAY_ORDER
		          ,TEUR.DELETE_FLG
		          ,TEUR.DATA_REV
		      FROM TTECUSRT AS TEUR /* OFFSET理由/NG理由分類マスタ */
		     WHERE TEUR.DELETE_FLG          = "0"                                   /* deleteフラグ */
		       AND TEUR.TECHNICIAN_USER_NAME        LIKE :TrPersonName               /* OFFSET理由/NG理由名(部分一致) */
		  ORDER BY TEUR.DISPLAY_ORDER
		          ,TEUR.TECHNICIAN_USER_ID
		',
			[
				"TrPersonName"     => "%".TRIM((String)Input::get('txtTrPersonForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    insertMasterData
	// over view      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lTrPersonName = Input::get('txtTrPersonNameForEntry');
		$lDisplayOrder = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			function() use	(
							  $lTrPersonName
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::insert
				('
						INSERT INTO TTECUSRT
						(
						  TECHNICIAN_USER_ID
						 ,TECHNICIAN_USER_NAME
						 ,DISPLAY_ORDER
						 ,DELETE_FLG
						 ,DATA_REV
						)
						VALUES (
						         NULL
						        ,:TrPersonName
						        ,:DisplayOrder
						        ,"0"
						        ,1
						       )
				', 
					[
						"TrPersonName"	=> $lTrPersonName,
						"DisplayOrder"	=> $lDisplayOrder,
					]
				);
				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	    
	}

	//**************************************************************************
	// processing name    updateMasterData
	// over view      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	//**************************************************************************
	private function updateMasterData
	(
	  $pTrPersonId
	 ,$pTrPersonName
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{

		//更新の成否をセット
		$lKohshinCnt = "";

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			function() use (
							 $pTrPersonId
							,$pTrPersonName
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				$lKohshinCnt = DB::update
				('
						UPDATE TTECUSRT
						   SET  TECHNICIAN_USER_NAME	= :TrPersonName
						       ,DISPLAY_ORDER			= :DisplayOrder
						       ,DELETE_FLG				= :DeleteFlg
						       ,DATA_REV				= DATA_REV + 1
						 WHERE TECHNICIAN_USER_ID		= :TrPersonIdWhere
				', 
					[
						"TrPersonIdWhere"	=> $pTrPersonId,
						"TrPersonName"		=> $pTrPersonName,
						"DisplayOrder"		=> $pDisplayOrder,
						"DeleteFlg"			=> $pDeleteFlg,
					]
				);
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	    
	}

	//**************************************************************************
	// processing name    keepFromInputValue
	// over view      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
		//■登録用・M/C No.の項目保持
		if (Input::has('txtTrPersonIdForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2090TrPersonIdForEntry', Input::get('txtTrPersonIdForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2090TrPersonIdForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"TrPersonIdForEntry"  => Session::get('ZA2090TrPersonIdForEntry')
		];
		
		//■登録用・MachineNameの項目保持
		if (Input::has('txtTrPersonNameForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2090TrPersonNameForEntry', Input::get('txtTrPersonNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2090TrPersonNameForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"TrPersonNameForEntry"  => Session::get('ZA2090TrPersonNameForEntry')
		];
		
		//■登録用・DisplayOrderの項目保持
		if (Input::has('txtDisplayOrderForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2090DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			//画面の値が無ければサーバ日付をwrite down in session。
			Session::put('ZA2090DisplayOrderForEntry', "");
		}

		//セッションには画面値か業務日付が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2090DisplayOrderForEntry')
		];

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"MachineNoForSearch"  => Session::get('ZA2090MachineNoForSearch')
		];
		
		//■検索用・MachineNameの項目保持
		//in case value does not exist in session
		if (Input::has('txtTrPersonForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2090TrPersonForSearch', Input::get('txtTrPersonForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2090TrPersonForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"TrPersonForSearch"  => Session::get('ZA2090TrPersonForSearch')
		];

		return $pViewData;
	}

	//**************************************************************************
	// processing name    initializeSessionData
	// over view      セッションデータのクリア（検索時・編集時の値）
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2090SearchResultData');

		//Entry button時に設定しているものをクリア
		Session::forget('ZA2090TrPersonIdForEntry');
		Session::forget('ZA2090TrPersonNameForEntry');
		Session::forget('ZA2090DisplayOrderForEntry');

		return null;
	}

	//**************************************************************************
	// processing name    isErrorForRegist
	// over view      登録時に実施する必須・型式チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須チェック
		//-------------------------

		//M/C名必須チェック
		$lValidator = Validator::make(
			array('txtTrPersonNameForEntry' => Input::get('txtTrPersonNameForEntry')),
			array('txtTrPersonNameForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Technician Request Person."
			]);
			return $pViewData;
		}

		//表示順必須チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}

		//-------------------------
		//型式チェック
		//-------------------------
		//表示順型チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}

}