<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;

use App\Model\tcodemst;
use App\Model\tmachinm;
use App\Model\tusermst;
use App\Model\tinsptim;
use App\Model\tinsntmm;
use App\Model\ttimedett;

use Auth;
use DB;
use Log;
use Session;
use Validator;
use Redirect;
use Exception;

class BA2020PreEntryController
extends Controller
{
	CONST CONVDATA_ARG     = "1";
	CONST CONDITION_NORMAL = "01";
	CONST CONDCLASS        = "001";
	CONST PROCCLASS        = "002";
	CONST TEAMCLASS        = "004";

	CONST NUM_LENG         = "9999.99";
	CONST DECIMAL_LENG     = 2;
	CONST COMBINING        = ".";
	CONST BLANK            = " ";
	
	//**************************************************************************
	// Processing      EntryAction
	// Overview        Open Screen, etc
	// parameter       
	// returned value  
	// programer       
	// date            
	// history         
	// remark          
	//**************************************************************************
	public function EntryAction()
	{
		$lViewData = [];  

		$lInspectionResultNo = 0;
		$lWorkInspectionNo = "";
		$lOrderCount = 0;


		$this->OutputLog01("BA2020 Start");

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (is_null(Session::get('BA2020FirstOpenJudgeFlg')))
		{
			//session put
			$this->ScreenOpenSessionput();
		}


	try
	{
		$lArrayData = []; 

		$lWorkInspectionDate  = "";
		$lWorkInspectionDate  = $this->convertDateFormat(Session::get('BA1010InspectionDateForEntry'), self::CONVDATA_ARG);

		$lArrayData += [
			"CheckSheetNo"        => Session::get('BA1010CheckSheetNoForEntry'),
			"CustomerId"          => Session::get('BA1010CustomerIdForEntry'),
			"ProcessId"           => Session::get('BA1010ProcessIdForEntry'),
			"MachineNo"           => Session::get('BA1010MachineNoForEntry'),
			"MoldNo"              => Session::get('BA1010MoldNoForEntry'),
			"InspectorCode"       => Session::get('BA1010InspectorCodeForEntry'),
			"InspectionDate"      => $lWorkInspectionDate,
			"ConditionId"         => Session::get('BA1010ConditionIdForEntry'),
			"InspectionTimeId"    => Session::get('BA1010InspectionTimeIdForEntry'),
		];

		$lTblCheckSheetInfo = $this->getCheckSheetData($lArrayData["CheckSheetNo"]);
		$lRowCheckSheetInfo = (array)$lTblCheckSheetInfo[0];

		if ($lRowCheckSheetInfo["INSPECTION_SHEET_NO"] != null)
		{
			//Nothing
		}
		else
		{
			throw new Exception("E008 : Check Sheet No. is not registered in Master. ");
		}

		$lTblProc = [];
		$lRowProc = [];
		$objtcodemst = new tcodemst;
		$lTblProc = $objtcodemst->getCodeInfo(self::PROCCLASS, $lArrayData["ProcessId"]);
		if (count($lTblProc) == 0)
		{
			//Nothing
		}
		else
		{
			$lRowProc = (array)$lTblProc[0];
		}

		$lTblMachine = [];
		$lRowMachine = [];
		$objtmachinm = new tmachinm;
		$lTblMachine = $objtmachinm->getMachineData($lArrayData["MachineNo"]);
		if (count($lTblMachine) == 0)
		{
			throw new Exception("E011 : M/C No. is not registered in Master. ");
		}
		else
		{
			$lRowMachine = (array)$lTblMachine[0];
		}

		$lTblInspector = [];
		$lRowInspector = [];
		$objtusermst = new tusermst;
		$lTblInspector = $objtusermst->getInspectorData($lArrayData["InspectorCode"]);
		if (count($lTblInspector) == 0)
		{
			throw new Exception("E014 : Inspector Code is not registered in Master.");
		}
		else
		{
			$lRowInspector = (array)$lTblInspector[0];
		}

		$lTblTeam = [];
		$lRowTeam = [];
		$objtcodemst = new tcodemst;
		$lTblTeam = $objtcodemst->getCodeInfo(self::TEAMCLASS, $lRowInspector["TEAM_ID"]);
		if (count($lTblTeam) == 0)
		{
			//Nothing
		}
		else
		{
			$lRowTeam = (array)$lTblTeam[0];
		}

		$lTblCond = [];
		$lRowCond = [];
		$objtcodemst = new tcodemst;
		$lTblCond = $objtcodemst->getCodeInfo(self::CONDCLASS, $lArrayData["ConditionId"]);
		if (count($lTblCond) == 0)
		{
			//Nothing
		}
		else
		{
			$lRowCond = (array)$lTblCond[0];
		}

		$lTblTime = [];
		$lRowTime = [];
		$objtinsptim = new tinsptim;
		$lTblTime = $objtinsptim->getTimeInfo($lArrayData["InspectionTimeId"]);
		if (count($lTblTime) == 0)
		{
			//Nothing
		}
		else
		{
			$lRowTime = (array)$lTblTime[0];
		}

		//List screen input → Use session, Other → Use SQL
		$lViewData += [
			"ProcessId"           => Session::get('BA1010ProcessIdForEntry'),
			"CustomerId"          => Session::get('BA1010CustomerIdForEntry'),
			"CheckSheetNo"        => Session::get('BA1010CheckSheetNoForEntry'),
			"MachineNo"           => Session::get('BA1010MachineNoForEntry'),
			"MoldNo"              => Session::get('BA1010MoldNoForEntry'),
			"InspectorCode"       => Session::get('BA1010InspectorCodeForEntry'),
			"InspectionDate"      => Session::get('BA1010InspectionDateForEntry'),
			"ConditionId"         => Session::get('BA1010ConditionIdForEntry'),
			"InspectionTimeId"    => Session::get('BA1010InspectionTimeIdForEntry'),

			"ProcessName"         => $lRowProc["CODE_NAME"],
			"CustomerName"        => $lRowCheckSheetInfo["CUSTOMER_NAME"],
			"CheckSheetName"      => $lRowCheckSheetInfo["INSPECTION_SHEET_NAME"],
			"MachineName"         => $lRowMachine["MACHINE_NAME"],
			"InspectorName"       => $lRowInspector["USER_NAME"],
			"TeamId"              => $lRowInspector["TEAM_ID"],
			"TeamName"            => $lRowTeam["CODE_NAME"],
			"ConditionName"       => $lRowCond["CODE_NAME"],
			"InspectionTimeName"  => $lRowTime["INSPECTION_TIME_NAME"],

			"RevisionNo"          => $lRowCheckSheetInfo["MAX_REV_NO"],
			"ModelName"           => $lRowCheckSheetInfo["MODEL_NAME"],				
			"ItemNo"              => $lRowCheckSheetInfo["ITEM_NO"],
			"ItemName"            => $lRowCheckSheetInfo["ITEM_NAME"],
			"DrawingNo"           => $lRowCheckSheetInfo["DRAWING_NO"],
			"ISOKanriNo"          => $lRowCheckSheetInfo["ISO_KANRI_NO"],
			"MaterialName"        => $lRowCheckSheetInfo['MATERIAL_NAME'],
			"MaterialSize"        => $lRowCheckSheetInfo['MATERIAL_SIZE'],
			"ProductWeight"       => $lRowCheckSheetInfo["PRODUCT_WEIGHT"],
			"HeatTreatmentType"   => $lRowCheckSheetInfo['HEAT_TREATMENT_TYPE'],
			"PlatingKind"         => $lRowCheckSheetInfo["PLATING_KIND"],
			"ColorId"             => $lRowCheckSheetInfo["COLOR_ID"],
			"ColorName"           => $lRowCheckSheetInfo["COLOR_NAME"],
		];

		if (Input::has('btnNext'))  //Next button
		{
			$this->OutputLog01("BA2020 Next Btn Click");

			//error check
			$this->ErrorCheck();

			//session put
			$this->ScreenInputItemSessionput();


			$lTblTimeDetail = [];
			$lRowTimeDetail = [];
			$objtimedett = new ttimedett;
			$lTblTimeDetail = $objtimedett->getTimedetailData($lRowCheckSheetInfo["TINSPTIM_CODE_NO"], $lViewData["InspectionTimeId"]);
			if (count($lTblTimeDetail) == 0)
			{
				throw new Exception("E032 : You mischose the time of Inspection.");
			}
			else
			{
				$lRowTimeDetail = (array)$lTblTimeDetail[0];
			}

			if ($lRowTimeDetail["INPUTNUMBER_JUDGE_FLG"] == 1)
			{
				If ($lViewData["ConditionId"] == self::CONDITION_NORMAL)
				{
					$lDataCount = $this->getLogicalDuplicationDataCount($lViewData);
					if ($lDataCount > 0)
					{
						throw new Exception("E031 : The data of the time is already registered. Search the data and retry the process with Modify Button. ");
					}
				}
			}


			$lTblSheetMasterData = $this->getMasterDataSheetMaster($lViewData);

			if ($lTblSheetMasterData != null)
			{
				$lTblSheetMasterDataRow = (array)$lTblSheetMasterData[0];

				if ($lTblSheetMasterDataRow["PROCESS_ID"] == "")
				{
					$lProcessId =  $lViewData["ProcessId"];
				}
				else
				{
					$lProcessId =  $lTblSheetMasterDataRow["PROCESS_ID"];
				} 


				$lTblInspNoTime = [];
				$objtinsntmm = new tinsntmm;
				$lTblInspNoTime = $objtinsntmm->getInspectionNoTimeData($lViewData["CheckSheetNo"], $lViewData["InspectionTimeId"]);
				if (count($lTblInspNoTime) == 0)
				{
					$lTblNoMasterData = $this->getMasterDataNoMaster2($lTblSheetMasterDataRow["INSPECTION_SHEET_NO"], $lTblSheetMasterDataRow["REV_NO"]);
				}
				else
				{
					$lTblNoMasterData = $this->getMasterDataNoMaster1($lTblSheetMasterDataRow["INSPECTION_SHEET_NO"], $lTblSheetMasterDataRow["REV_NO"], $lViewData["InspectionTimeId"]);
				}


				if ($lTblNoMasterData != null)
				{
					//Start Transaction
					DB::beginTransaction();

					foreach ($lTblNoMasterData As $lRow1)
					{
						$lTblNoMasterDataRow = (Array)$lRow1;
						$lGrp01No     = $lTblNoMasterDataRow["CAVITYNO"];
						$lGrp02Id     = $lTblNoMasterDataRow["WHEREPLACE"];
						$lSampleQty   = $lTblNoMasterDataRow["SAMPLE_QTY"];

						for ($SampleCount = 1 ; $SampleCount <= $lSampleQty; $SampleCount++)
						{
							if ($lGrp02Id == self::BLANK)
							{
								$lWorkInspectionNo = $lTblNoMasterDataRow["INSPECTION_POINT"].self::COMBINING.$lGrp01No.self::COMBINING.$SampleCount;
							}
							else
							{
								$lWorkInspectionNo = $lTblNoMasterDataRow["INSPECTION_POINT"].self::COMBINING.$lGrp01No.self::COMBINING.$lGrp02Id.self::COMBINING.$SampleCount;
							}


							$lOrderCount = $lOrderCount + 1;

							$lInspectionResultNo = $this->insertInspectionResultBasicRecord(
															$lProcessId
															,$lGrp01No
															,$lGrp02Id
															,$SampleCount
															,$lInspectionResultNo
															,$lWorkInspectionNo
															,$lOrderCount
															,$lTblSheetMasterDataRow
															,$lTblNoMasterDataRow
															,$lViewData
															,Input::get('txtMaterialLotNoForEntry')
															,Input::get('txtCertificateNoForEntry')
															,Input::get('txtPoNoForEntry')
															,Input::get('txtInvoiceNoForEntry')
															,Input::get('txtQuantityCoilForEntry')
															,Input::get('txtQuantityWeightForEntry')
															,Input::get('txtLotNoForEntry')
															,Input::get('txtProductionDateForEntry')
														);
						}
					}

					if ($lInspectionResultNo == 0)
					{
						DB::rollback();
					}
					else
					{
						DB::commit();
					}
				}
				else
				{
					throw new Exception("E997 : Target data does not exist.");
				}
			}
			else
			{
				throw new Exception("E997 : Target data does not exist.");
			}


			//reset
			$this->initializeSessionData();

			//Add Result No
			Session::put('BA2020InspectionResultNo'			, $lInspectionResultNo);

			//Laravel VerUp From 4.2 To 5.6
			// return Redirect::route("user/entry");
			return redirect()->route('user.entry');
		}
		elseif (Input::has('btnReturn'))    //Return button
		{
			$this->OutputLog01("BA2020 Rtn Btn Click");

			//session put
			$this->ScreenInputItemSessionput();

			Session::forget('BA2020InspectionResultNo');

			//Laravel VerUp From 4.2 To 5.6
			// return Redirect::route("user/list");
			return redirect()->route('user.list');
		}
		else                                //transition, paging from other screen or menu
		{
			//遷移元のURLに「index.php/user/list含んでいる場合」（＝List画面からの遷移の場合）＝True
			//遷移元がそれ以外の場合は文字列が返ってくる（Trueにはならない）
			//in case index.php/user/list  including in URL,(＝transition from list screen）＝True
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/list');

				if($lPrevURL == true)
				{
					//reset
					$this->initializeSessionData();
				}
			}
		}
	}catch(Exception $e) 
		{
			$lViewData["errors"] = new MessageBag([
				"error" => $e->getMessage()
			]);
		}


		//■must pass
		if (Session::get('BA0000LastScreenInfomation') == "BA2020")
		{
			//session put
			$this->ScreenInputItemSessionput();
		}
		
		//store and set again entry in screen
		$lViewData = $this->setPreEntryForms($lViewData);

		//display screen
		//Laravel VerUp From 4.2 To 5.6
		// return View::make("user/preentry", $lViewData);
		return View("user.preentry", $lViewData);
	}



	//**************************************************************************
	// process         ScreenOpenSessionput
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ScreenOpenSessionput()
	{
		Session::put('BA2020FirstOpenJudgeFlg'	, 1);

		Session::put('BA2020MaterialLotNoForEntry'		, "");
		Session::put('BA2020CertificateNoForEntry'		, "");
		Session::put('BA2020PoNoForEntry'				, "");
		Session::put('BA2020InvoiceNoForEntry'			, "");
		Session::put('BA2020QuantityCoilForEntry'		, "");
		Session::put('BA2020QuantityWeightForEntry'		, "");
		Session::put('BA2020LotNoForEntry'				, "");
		Session::put('BA2020ProductionDateForEntry'		, "");

		return null;
	}

	//**************************************************************************
	// process         ScreenInputItemSessionput
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ScreenInputItemSessionput()
	{
		//Last Screen Infomation
		Session::put('BA0000LastScreenInfomation'		, "BA2020");

		//Entry
		Session::put('BA2020MaterialLotNoForEntry'	, TRIM((String)Input::get('txtMaterialLotNoForEntry')));
		Session::put('BA2020CertificateNoForEntry'	, TRIM((String)Input::get('txtCertificateNoForEntry')));
		Session::put('BA2020PoNoForEntry'			, TRIM((String)Input::get('txtPoNoForEntry')));
		Session::put('BA2020InvoiceNoForEntry'		, TRIM((String)Input::get('txtInvoiceNoForEntry')));
		Session::put('BA2020QuantityCoilForEntry'	, TRIM((String)Input::get('txtQuantityCoilForEntry')));
		Session::put('BA2020QuantityWeightForEntry'	, TRIM((String)Input::get('txtQuantityWeightForEntry')));
		Session::put('BA2020LotNoForEntry'			, TRIM((String)Input::get('txtLotNoForEntry')));
		Session::put('BA2020ProductionDateForEntry'	, TRIM((String)Input::get('txtProductionDateForEntry')));

		return null;
	}

	//**************************************************************************
	// process            initializeSessionData
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function initializeSessionData()
	{
		Session::forget('BA2020MaterialLotNoForEntry');
		Session::forget('BA2020CertificateNoForEntry');
		Session::forget('BA2020PoNoForEntry');
		Session::forget('BA2020InvoiceNoForEntry');
		Session::forget('BA2020QuantityCoilForEntry');
		Session::forget('BA2020QuantityWeightForEntry');
		Session::forget('BA2020LotNoForEntry');
		Session::forget('BA2020ProductionDateForEntry');

		return null;
	}

	//**************************************************************************
	// process         setPreEntryForms
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setPreEntryForms($pViewData)
	{
		$pViewData += [
			"MaterialLotNoForEntry"  => Session::get('BA2020MaterialLotNoForEntry'),
			"CertificateNoForEntry"  => Session::get('BA2020CertificateNoForEntry'),
			"PoNoForEntry"           => Session::get('BA2020PoNoForEntry'),
			"InvoiceNoForEntry"      => Session::get('BA2020InvoiceNoForEntry'),
			"QuantityCoilForEntry"   => Session::get('BA2020QuantityCoilForEntry'),
			"QuantityWeightForEntry" => Session::get('BA2020QuantityWeightForEntry'),
			"LotNoForEntry"          => Session::get('BA2020LotNoForEntry'),
			"ProductionDateForEntry" => Session::get('BA2020ProductionDateForEntry'),
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ErrorCheck
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ErrorCheck()
	{
		if(Input::get('txtLotNoForEntry') != null)
		{
			//nothing
		}
		else
		{
			throw new Exception("E060 : Enter Lot No..");
		}  

		if(Input::get('txtProductionDateForEntry') != null)
		{
			//nothing
		}
		else
		{
			throw new Exception("E062 : Enter Production Date.");
		}  

		if (Input::get('txtQuantityCoilForEntry') != null)
		{
			if (Input::get('txtQuantityCoilForEntry') <= self::NUM_LENG)
			{
				//小数点2桁チェックをしたい
				//preg_match( '/^[0-9]+(.[0-9]{1,' . self::DECIMAL_LENG . '})?$/', $val ) > 0;
				//何もしない
			}
			else
			{
				throw new Exception("E054 : Input Max is '9999.99'.");
			}
		}

		if(Input::get('txtQuantityWeightForEntry') != null)
		{
			if(Input::get('txtQuantityWeightForEntry') <= self::NUM_LENG)
			{
				//nothing
			}
			else
			{
				throw new Exception("E054 : Input Max is '9999.99'.");
			}
		}
	}

	//**************************************************************************
	// process         getCheckSheetData
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetData($pCheckSheetNo)
	{
		$lTblCheckSheetInfo = [];

			$lTblCheckSheetInfo = DB::select('
			          SELECT SHEE.INSPECTION_SHEET_NO
			                ,MAX(SHEE.REV_NO) AS MAX_REV_NO
			                ,SHEE.PROCESS_ID
			                ,SHEE.INSPECTION_SHEET_NAME
			                ,SHEE.CUSTOMER_ID
			                ,CUST.CUSTOMER_NAME
			                ,SHEE.MODEL_NAME
			                ,SHEE.ITEM_NO
			                ,ITEM.ITEM_NAME
			                ,SHEE.DRAWING_NO
			                ,SHEE.ISO_KANRI_NO
			                ,SHEE.TINSPTIM_CODE_NO
			                ,ITEM.PRODUCT_WEIGHT
			                ,ITEM.MATERIAL_NAME
			                ,ITEM.MATERIAL_SIZE
			                ,ITEM.HEAT_TREATMENT_TYPE
			                ,ITEM.PLATING_KIND
			                ,ITEM.COLOR_ID
			                ,COLOR.CODE_NAME AS COLOR_NAME
			            FROM TISHEETM AS SHEE
			 LEFT OUTER JOIN TITEMMST AS ITEM
			              ON ITEM.ITEM_NO             = SHEE.ITEM_NO
			             AND ITEM.DELETE_FLG          = "0"
			 LEFT OUTER JOIN TCODEMST AS COLOR
			              ON COLOR.CODE_CLASS         = "006"
			             AND ITEM.COLOR_ID            = COLOR.CODE_ORDER
			 LEFT OUTER JOIN TCUSTOMM AS CUST
			              ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			             AND CUST.DELETE_FLG          = "0"
			           WHERE SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
			             AND SHEE.DELETE_FLG          = "0"
			',
				[
					"CheckSheetNo" => TRIM((String)$pCheckSheetNo),
				]
			);

		return $lTblCheckSheetInfo;
	}

	//**************************************************************************
	// process         getMasterDataSheetMaster
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function getMasterDataSheetMaster($pViewData)
	{
		$lTblMasterData          = [];

		$lTblMasterData = DB::select('
				SELECT SHEET.INSPECTION_SHEET_NO
				      ,SHEET.REV_NO
				      ,SHEET.PROCESS_ID
				      ,COALESCE(ITEM.MATERIAL_NAME,"") AS MATERIAL_NAME
				      ,COALESCE(ITEM.MATERIAL_SIZE,"") AS MATERIAL_SIZE
				      ,COALESCE(ITEM.HEAT_TREATMENT_TYPE,"") AS HEAT_TREATMENT_TYPE
				      ,COALESCE(ITEM.PLATING_KIND,"") AS PLATING_KIND
				      ,COALESCE(ITEM.COLOR_ID,"") AS COLOR_ID
				  FROM TISHEETM AS SHEET
	   LEFT OUTER JOIN TITEMMST AS ITEM
				    ON ITEM.ITEM_NO              = SHEET.ITEM_NO
				   AND ITEM.DELETE_FLG           = "0"
				 WHERE SHEET.INSPECTION_SHEET_NO = :CheckSheetNo
				   AND SHEET.REV_NO              = :RevisionNo
				   AND SHEET.DELETE_FLG          = "0"
		',
			[
				"CheckSheetNo"  => TRIM((String)$pViewData["CheckSheetNo"]),
				"RevisionNo"    => TRIM((String)$pViewData["RevisionNo"]),
			]
		);

		return $lTblMasterData;
	}

	//**************************************************************************
	// process         getMasterDataNoMaster1
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function getMasterDataNoMaster1($pCheckSheetNo, $pRevNo, $pTimeId)
	{
		$lTblMasterData          = [];

		$lTblMasterData = DB::select('
				SELECT ISNO.INSPECTION_SHEET_NO
				      ,ISNO.REV_NO
				      ,ISNO.INSPECTION_POINT
				      ,ISNO.GRP01_ID
				      ,CODE.CODE_NAME AS CAVITYNO
				      ,ISNO.GRP02_ID
				      ,CODE2.CODE_NAME AS WHEREPLACE
				      ,ISNO.SAMPLE_QTY
				  FROM TINSPNOM AS ISNO
			INNER JOIN TINSNTMM AS INTM
					ON INTM.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
				   AND INTM.INSPECTION_POINT    = ISNO.INSPECTION_POINT
				   AND INTM.INSPECTION_TIME_ID  = :TimeId
				   AND INTM.DELETE_FLG          = "0"
			INNER JOIN TCODEMST AS CODE
					ON CODE.CODE_CLASS          = ISNO.GRP01_ID
			INNER JOIN TCODEMST AS CODE2
					ON CODE2.CODE_CLASS         = ISNO.GRP02_ID
				 WHERE ISNO.INSPECTION_SHEET_NO = :CheckSheetNo
				   AND ISNO.REV_NO              = :RevisionNo
				   AND ISNO.DELETE_FLG          = "0"
			  ORDER BY ISNO.DISPLAY_ORDER
					  ,ISNO.INSPECTION_POINT
					  ,CODE.DISPLAY_ORDER
					  ,CODE2.DISPLAY_ORDER
		',
			[
				"CheckSheetNo"  => TRIM((String)$pCheckSheetNo),
				"RevisionNo"    => TRIM((String)$pRevNo),
				"TimeId"        => TRIM((String)$pTimeId),
			]
		);

		return $lTblMasterData;
	}

	//**************************************************************************
	// process         getMasterDataNoMaster2
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function getMasterDataNoMaster2($pCheckSheetNo, $pRevNo)
	{
		$lTblMasterData          = [];

		$lTblMasterData = DB::select('
				SELECT ISNO.INSPECTION_SHEET_NO
				      ,ISNO.REV_NO
				      ,ISNO.INSPECTION_POINT
				      ,ISNO.GRP01_ID
				      ,CODE.CODE_NAME AS CAVITYNO
				      ,ISNO.GRP02_ID
				      ,CODE2.CODE_NAME AS WHEREPLACE
				      ,ISNO.SAMPLE_QTY
				  FROM TINSPNOM AS ISNO
			INNER JOIN TCODEMST AS CODE
					ON CODE.CODE_CLASS          = ISNO.GRP01_ID
			INNER JOIN TCODEMST AS CODE2
					ON CODE2.CODE_CLASS         = ISNO.GRP02_ID
				 WHERE ISNO.INSPECTION_SHEET_NO = :CheckSheetNo
				   AND ISNO.REV_NO              = :RevisionNo
				   AND ISNO.DELETE_FLG          = "0"
			  ORDER BY ISNO.DISPLAY_ORDER
					  ,ISNO.INSPECTION_POINT
					  ,CODE.DISPLAY_ORDER
					  ,CODE2.DISPLAY_ORDER
		',
			[
				"CheckSheetNo"  => TRIM((String)$pCheckSheetNo),
				"RevisionNo"    => TRIM((String)$pRevNo),
			]
		);

		return $lTblMasterData;
	}

	//**************************************************************************
	// process         getLogicalDuplicationDataCount
	// overview        get count of redundant data of logic key 論理キー重複データ件数取得
	// argument        
	// return value    Int（count of redundant data >=0 or 1）
	// author  　　　　　  
	// date            Ver.01 
	// remarks         Ver.02
	//**************************************************************************
	private function getLogicalDuplicationDataCount($pViewData)
	{
		$lTblLogicalDupulicationDataCount = [];
		$lRowLogicalDupulicationDataCount = [];
		$lDataCount                       = 0;

			$lTblLogicalDupulicationDataCount = DB::select('
					SELECT COUNT("X") AS COUNT
					  FROM TRESHEDT AS HEAD
					 WHERE INSPECTION_SHEET_NO = :InspectionSheetNo
					   AND REV_NO              = :RevisionNo
					   AND PROCESS_ID          = :Process
					   AND MACHINE_NO          = :MachineNo
					   AND MOLD_NO             = :MoldNo
					   AND INSERT_USER_ID      = :InspectorCode
					   AND INSPECTION_YMD      = :InspectionDate
					   AND CONDITION_CD        = :Condition
					   AND INSPECTION_TIME_ID  = :InspectionTime
			',
				[
					"InspectionSheetNo" => TRIM((String)$pViewData["CheckSheetNo"]),
					"RevisionNo"        => TRIM((String)$pViewData["RevisionNo"]),
					"Process"           => TRIM((String)$pViewData["ProcessId"]),
					"MachineNo"         => TRIM((String)$pViewData["MachineNo"]),
					"MoldNo"            => TRIM((String)$pViewData["MoldNo"]),
					"InspectorCode"     => TRIM((String)$pViewData["InspectorCode"]),
					"InspectionDate"    => $this->convertDateFormat((String)$pViewData["InspectionDate"], self::CONVDATA_ARG),
					"Condition"         => TRIM((String)$pViewData["ConditionId"]),
					"InspectionTime"    => TRIM((String)$pViewData["InspectionTimeId"]),
				]
			);

		//read result(1 line)
		$lRowLogicalDupulicationDataCount = $lTblLogicalDupulicationDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowLogicalDupulicationDataCount = (Array)$lRowLogicalDupulicationDataCount;

		//get count
		$lDataCount = $lRowLogicalDupulicationDataCount["COUNT"];

		return $lDataCount;
	}

	//**************************************************************************
	// process         convertDateFormat
	// overview        change data type
	// argument        date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//                 pattern(1:English->Japanese 2:Japanese->English)
	// return value    
	// author          
	// date            Ver.01  
	// remarks         
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}

	//**************************************************************************
	// process         insertInspectionResultBasicRecord
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function insertInspectionResultBasicRecord($pProcessId
														,$pGrp01No
														,$pGrp02Id
														,$pSampleCount
														,$pInspectionResultNo
														,$pWorkInspectionNo
														,$pOrderCount
														,$pTblSheetMasterDataRow
														,$pTblNoMasterDataRow
														,$pViewData
														,$pMaterialLotNo
														,$pCertificateNo
														,$pPoNo
														,$pInvoiceNo
														,$pQuantityCoil
														,$pQuantityWeight
														,$pLotNo
														,$pProductionDate)
	{
		$pInspectionDate = "";
		$pInspectionDate = date_format(date_create($pViewData["InspectionDate"]), 'Y/m/d');
		$pProductionDate = date_format(date_create($pProductionDate), 'Y/m/d');

		//No have TRESHEDT → insert
		if ($pInspectionResultNo == 0)
		{
			// Log::write('info', 'BA2010 Start Insert Header');

			DB::insert('
						INSERT INTO TRESHEDT
							(INSPECTION_SHEET_NO
							,REV_NO
							,ORDER_NO
							,ISSUE_NO
							,PROCESS_ID
							,MACHINE_NO
							,MOLD_NO
							,MATERIAL_NAME
							,MATERIAL_SIZE
							,HEAT_TREATMENT_TYPE
							,PLATING_KIND
							,COLOR_ID
							,INSERT_USER_ID
							,INSPECTION_YMD
							,APPROVE_USER_ID
							,APPROVE_YMD
							,PRODUCT_PASS_QTY
							,PRODUCT_NG_QTY
							,SUPPLIER_NG_QTY
							,MC_NG_QTY
							,QC_CHEK_QTY
							,QC_SAMPLING_QTY
							,QC_PASS_QTY
							,QC_NG_QTY
							,CONDITION_CD
							,INSPECTION_TIME_ID
							,MATERIAL_LOT_NO
							,CERTIFICATE_NO
							,PO_NO
							,INVOICE_NO
							,QUANTITY_COIL
							,QUANTITY_WEIGHT
							,LOT_NO
							,PRODUCTION_YMD
							,REFER_DOCUMENT1
							,REFER_DOCUMENT2
							,INSERT_YMDHMS
							,LAST_UPDATE_USER_ID
							,UPDATE_YMDHMS
							,DATA_REV)
						VALUES (:CheckSheetNo
								,:RevisionNo
								,""
								,""
								,:ProcessId
								,:MachineNo
								,:MoldNo
								,:MaterialName
								,:MaterialSize
								,:HeatTreatType
								,:PlatingKind
								,:ColorId
								,:InspectionUserId
								,:InspectionDate
								,""
								,""
								,"0"
								,"0"
								,"0"
								,"0"
								,"0"
								,"0"
								,"0"
								,"0"
								,:ConditionId
								,:InspectionTimeId
								,:MaterialLotNo
								,:CertificateNo
								,:PoNo
								,:InvoiceNo
								,:QuantityCoil
								,:QuantityWeight
								,:LotNo
								,:ProductionDate
								,""
								,""
								,now()
								,:LastUpdateUserId
								,now()
								,1)
					',
						[
							"ProcessId"        => $pProcessId,
							"CheckSheetNo"     => $pTblSheetMasterDataRow["INSPECTION_SHEET_NO"],
							"RevisionNo"       => $pTblSheetMasterDataRow["REV_NO"],
							"MachineNo"        => $pViewData["MachineNo"],
							"MoldNo"           => $pViewData["MoldNo"],
							"MaterialName"     => $pTblSheetMasterDataRow["MATERIAL_NAME"],
							"MaterialSize"     => $pTblSheetMasterDataRow["MATERIAL_SIZE"],
							"HeatTreatType"    => $pTblSheetMasterDataRow["HEAT_TREATMENT_TYPE"],
							"PlatingKind"      => $pTblSheetMasterDataRow["PLATING_KIND"],
							"ColorId"          => $pTblSheetMasterDataRow["COLOR_ID"],
							"InspectionUserId" => $pViewData["InspectorCode"],
							"InspectionDate"   => $pInspectionDate,
							"ConditionId"      => $pViewData["ConditionId"],
							"InspectionTimeId" => $pViewData["InspectionTimeId"],
							"MaterialLotNo"    => $pMaterialLotNo,
							"CertificateNo"    => $pCertificateNo,
							"PoNo"             => $pPoNo,
							"InvoiceNo"        => $pInvoiceNo,
							"QuantityCoil"     => $pQuantityCoil,
							"QuantityWeight"   => $pQuantityWeight,
							"LotNo"            => $pLotNo,
							"ProductionDate"   => $pProductionDate,
							"LastUpdateUserId" => $pViewData["InspectorCode"],
						]
			);

			//Keep automatic numbering number
			$lTblLastInsertID = DB::select('
					SELECT LAST_INSERT_ID();
			');

			$lRowLastInsertID = (Array)$lTblLastInsertID[0];
			$pInspectionResultNo = $lRowLastInsertID["LAST_INSERT_ID()"];

			// Log::write('info', 'BA2010 Finish Insert Header');
		}

		// Log::write('info', 'BA2010 Start Insert Detail');
		DB::insert('
				INSERT INTO TRESDETT
							(INSPECTION_RESULT_NO
							,INSPECTION_NO
							,INSPECTION_RESULT_VALUE
							,INSPECTION_RESULT_TYPE
							,OK_QTY
							,NG_QTY
							,OFFSET_NG_BUNRUI_ID
							,OFFSET_NG_REASON_ID
							,OFFSET_NG_REASON
							,OFFSET_NG_ACTION
							,OFFSET_NG_ACTION_DTL_INTERNAL
							,OFFSET_NG_ACTION_DTL_REQUEST
							,OFFSET_NG_ACTION_DTL_TSR
							,OFFSET_NG_ACTION_DTL_SORTING
							,OFFSET_NG_ACTION_DTL_NOACTION
							,ACTION_DETAILS_ID
							,ACTION_DETAILS_TR_NO_TSR_NO
							,TR_NO
							,TECHNICIAN_USER_ID
							,TSR_NO
							,INSPECTION_POINT
							,ANALYTICAL_GRP_01
							,ANALYTICAL_GRP_02
							,SAMPLE_NO
							,ANALYTICAL_GRP_01_MEMO
							,INSERT_USER_ID
							,DISPLAY_ORDER
							,INSERT_YMDHMS
							,LAST_UPDATE_USER_ID
							,UPDATE_YMDHMS
							,DATA_REV)
						VALUES (:InspectionResultNo
								,:InspectionNo
								,""
								,""
								,"0"
								,"0"
								,""
								,""
								,""
								,""
								,"0"
								,"0"
								,"0"
								,"0"
								,"0"
								,""
								,""
								,""
								,""
								,""
								,:InspectionPoint
								,:AnalyticalGrp0101
								,:AnalyticalGrp0201
								,:SampleNo
								,:AnalyticalGrp0102
								,:InsertUserId
								,:OrderCount
								,now()
								,:LastUpdateUserId
								,now()
								,1)
			',
				[
					"InspectionResultNo"           => $pInspectionResultNo,
					"InspectionNo"                 => $pWorkInspectionNo,
					"InspectionPoint"              => $pTblNoMasterDataRow["INSPECTION_POINT"],
					"AnalyticalGrp0101"            => $pGrp01No,
					"AnalyticalGrp0102"            => $pGrp01No,
					"AnalyticalGrp0201"            => $pGrp02Id,
					"SampleNo"                     => $pSampleCount,
					"OrderCount"                   => $pOrderCount,
					"InsertUserId"                 => $pViewData["InspectorCode"],
					"LastUpdateUserId"             => $pViewData["InspectorCode"],
				]
		);
		// Log::write('info', 'BA2010 Finish Insert Detail');

		return $pInspectionResultNo;
	}


	//**************************************************************************
	// process         OutputLog01
	//**************************************************************************
	private function OutputLog01($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Process Id"			=> Session::get('BA1010ProcessIdForEntry'),
				"Customer Id"			=> Session::get('BA1010CustomerIdForEntry'),
				"Check Sheet No."		=> Session::get('BA1010CheckSheetNoForEntry'),
				"M/C No."				=> Session::get('BA1010MachineNoForEntry'),
				"Mold No."				=> Session::get('BA1010MoldNoForEntry'),
				"Inspector Code"		=> Session::get('BA1010InspectorCodeForEntry'),
				"Inspection Date"		=> Session::get('BA1010InspectionDateForEntry'),
				"Condition Id"			=> Session::get('BA1010ConditionIdForEntry'),
				"Inspection Time Id"	=> Session::get('BA1010InspectionTimeIdForEntry'),
			]
		);

		return null;
	}




}