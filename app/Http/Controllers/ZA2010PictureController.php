<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Session;
use Log;
use Validator;
use Illuminate\Support\MessageBag;;

class ZA2010PictureController
extends Controller
{

	//**************************************************************************
	// 処理名    MasterAction
	// 概要      画像アップロード・削除画面の初期表示を行う。
	//           また、Delete、Data Upload等のボタンにより処理を分岐し、
	//           それぞれに対応した処理を行う。
	// 引数      無し
	// 戻り値    無し
	// 作成者    k-kagawa
	// 作成日    2014.11.22
	// 更新履歴  2014.11.22 v0.01 初回作成
	//           
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData            = [];                  //画面へのデータ移送用

		//Session経由でログイン画面からのパラメータを受け取り、画面への移送用配列に投入（メニュー制御に必要）
		$lViewData += [
			"AdminFlg"    => Session::get('AA1010AdminFlg'),
			"PictureURL"  => Input::get('cmbPictureData'            ,'img/default.jpg')
		];
        
		if (Input::has('btnDelete'))       //削除ボタン押下
		{
			//ログ
			Log::write('info', 'Delete Button Click.', 
				[
					"Picture Name"           => Input::get('cmbPictureData' ,''),
				]
			);

			//JavaScriptの確認ダイアログでOKが押されている場合だけ処理する
			if ((String)Input::get('hidProcOKFlg') == "OK")
			{
				//削除画像必須チェックを実施
				$lViewData = $this->checkDeletePicture($lViewData);
				
				//エラーが無ければ処理続行
				if (array_key_exists("errors", $lViewData) == false)
				{
					//退避フォルダが存在しない場合はフォルダを作成
					if (file_exists('img/temp') == false)
					{
						mkdir('img/temp');
					}

					//ファイルをtempフォルダに移動(ファイルパスをimg/temp/XXXXにリネーム)
					if (
						rename(Input::get('cmbPictureData' ,'')
						       ,str_replace("img/", "img/temp/", Input::get('cmbPictureData' ,''))
						      )
						)
					{
						//処理完了メッセージ
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";

						//選択する画像をデフォルトに戻す
						$lViewData = array_replace($lViewData,array("PictureURL"  => 'img/default.jpg'));
					}
				}
			
			}
		}
		elseif (Input::has('btnUpload'))     //Uploadボタン押下
		{
			//ログ
			Log::write('info', 'Upload Button Click.', 
				[
					"filUploadFile"           => Input::file('filUploadFile'),
				]
			);
			//JavaScriptの確認ダイアログでOKが押されている場合だけ処理する
			if ((String)Input::get('hidProcOKFlg') == "OK")
			{
				//アップロードファイル名チェックを実施
				$lViewData = $this->checkUploadFile($lViewData);

				//エラーが無い場合
				if (array_key_exists("errors", $lViewData) == false)
				{
					//ファイルを画像フォルダにコピー
					if (
							copy(Input::file('filUploadFile')
							     ,"img/".basename(Input::file('filUploadFile')->getClientOriginalName())
							     )
						)
					{
						//処理完了メッセージ
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";
					}
				}
			}
		}

		//画像リストコンボの一覧取得
		$lViewData = $this->setCmbPictureList($lViewData);

		//画面入力値の保持
		$lViewData += [
			"PictureName"  => Input::get('cmbPictureData','')
		];
		
		return View("user.picture", $lViewData);
		
	}

	//**************************************************************************
	// 処理名    setCmbPictureList
	// 概要      削除対照の画像リストを画面上に表示する。セッションの値管理も行う。
	// 引数      画面表示用配列
	// 戻り値    Array(コンボ情報を追加したもの)
	// 作成者    k-kagawa
	// 作成日    2014.11.22
	// 更新履歴  2014.11.22 v0.01 初回作成
	//           
	//**************************************************************************
	private function setCmbPictureList($pViewData)
	{

		//■変数
		$lArrDataListPictureData = [];  //画像データリストの画面返却用リスト

		//先頭行にデフォルトの空ファイル行を追加
		$lArrDataListPictureData += [
			"img/default.jpg" => "",
		];
            
		//imgフォルダにある画像ファイル(拡張子.jpg)を取得(gifも可能としておく)
		foreach(glob('img/{*.gif,*.jpg,}',GLOB_BRACE) as $file)
		{
			if(is_file($file))
			{
				//リストボックス表示用の配列に格納(値はimg/付きのファイルパス、img/抜きのファイル名)
				$lArrDataListPictureData += [
					$file => str_replace("img/", "", $file)
				];
			}
		}

		//画面への移送用配列に追加
		$pViewData += [
			"arrDataListPictureData" => $lArrDataListPictureData
		];
		
		return $pViewData;
	}

	//**************************************************************************
	// 処理名    checkDeletePicture
	// 概要      Master Nameの必須選択チェック
	// 引数      画面返却用配列
	// 戻り値    画面返却用配列
	// 作成者    k-kagawa
	// 作成日    2014.11.22
	// 更新履歴  2014.11.22 v0.01 初回作成
	//           
	//**************************************************************************
	private function checkDeletePicture($pViewData)
	{

		//ログ
		Log::write('info', 'Delete Button Click.', 
			[
				"Delete File"           => Input::get('cmbPictureData'),
			]
		);

		//Master Name必須選択チェック
		$lValidator = Validator::make(
			array('cmbPictureData' => Input::get('cmbPictureData'),''),
			array('cmbPictureData' => array('required'))
		);

		//エラーの場合
		if ($lValidator->fails() or Input::get('cmbPictureData') == 'img/default.jpg')
		{
				//エラーメッセージ設定
				$pViewData["errors"] = new MessageBag([
					"error" => "E033 : Select delete Picture."
				]);

				return $pViewData;
		}

		return $pViewData;
	
	}
	
	//**************************************************************************
	// 処理名    checkUploadFile
	// 概要      アップロードするファイルの必須チェック、ファイル名チェック
	// 引数      画面返却用配列、テンプレートファイル名
	// 戻り値    画面返却用配列
	// 作成者    k-kagawa
	// 作成日    2014.11.22
	// 更新履歴  2014.11.22 v0.01 初回作成
	//           
	//**************************************************************************
	private function checkUploadFile($pViewData)
	{
		//Upload File必須選択チェック
		$lValidator = Validator::make(
			array('filUploadFile' => Input::file('filUploadFile')),
			array('filUploadFile' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails()) 
		{
				//エラーメッセージ設定
				$pViewData["errors"] = new MessageBag([
					"error" => "E029 : Select Upload File."
				]);
				
				return $pViewData;
		}

		//アップロードするファイル名を取得
		$lUploadFileName = Input::file('filUploadFile')->getClientOriginalName();

		//ログ
		Log::write('info', 'Upload Button Click.', 
			[
				"Upload File"           => $lUploadFileName,
			]
		);

		//Upload File名の末尾が.jpg又はgifであること。
		if (strrchr($lUploadFileName,'.jpg') != ".jpg" and strrchr($lUploadFileName,'.gif') != ".gif") {
			
			//★　拡張子が不正エラー（必要に応じてメッセージを見直す）
			//エラーメッセージ設定
			$pViewData["errors"] = new MessageBag([
				"error" => "E034 : Select Picuture File(.jpg or .gif)."
			]);
			
			return $pViewData;
			
		}
		
		return $pViewData;
	
	}
}