<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Log;
use Session;
use Validator;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    maintenance OFFSET reason/NG reason master
// over view      maintenance OFFSET reason/NG reason master
// programer    k-kagawa
// date    2015.01.12
// update  
//           
//**************************************************************************
class ZA2060NgreasonMasterController
extends Controller
{
	//-------------
	//■■define constance
	CONST NUMBER_PER_PAGE = 10;		//number of data per 1 page

	//**************************************************************************
	// processing name    MasterAction
	// over view      display initial screen
	//           separate processing as Entry,Search,Modify button
	//           do processing corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion of data to screen
		
		$lTblSearchResultData		= []; //data table of inspection result list
		$lPagenation				= []; //for paging

		$lTblMasterCheck 			= []; //for master existance check

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //lock mode of screen
		$lPrevMode					= ""; //lock mode of screen before transition

		//store and re-set entry item
		$lViewData = $this->keepFromInputValue($lViewData);

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		//set OFFSET reason/NG category combo
		$lViewData = $this->setOffsetNGBunrui($lViewData);

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"NGRaason Name"  => Input::get('txtNGRaasonNameForSearch'   ,''),
				]
			);

			//in case of no data,search
			if (array_key_exists("errors", $lViewData) == false)
			{

				//search
				$lTblSearchResultData = $this->getSearchMasterData();

				//in case of no data,error
				if (count($lTblSearchResultData) == 0)
				{
					//set error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				
				//store in session
				Session::put('ZA2060SearchResultData', $lTblSearchResultData);

				//set lock mode in screen
				$lMode = "Search";
				Session::put('ZA2060ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//make value in edit field initial value

			//exchange session to initial value
			Session::put('ZA2060NGRaasonCDForEntry', "");
			Session::put('ZA2060NGRaasonNameForEntry', "");
			Session::put('ZA2060OffsetNGBunruiForEntry', "");
			Session::put('ZA2060DisplayOrderForEntry', "");

			//exchange view data to initial value
			$lViewData["NGRaasonCDForEntry"] = "";
			$lViewData["NGRaasonNameForEntry"] = "";
			$lViewData["OffsetNGBunruiForEntry"] = "";
			$lViewData["DisplayOrderForEntry"] = "";

			//set lock mode in screen
			$lMode = "NewAdd";
			Session::put('ZA2060ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{   
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"NGRaason Cd"   => Input::get('txtNGRaasonIDForEntry'       ,''),
					"NGRaason Name" => Input::get('txtNGRaasonNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2060ActionMode'),
				]
			);
       
			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2060ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate processing corresponding to prevent screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case new entry
					//get data for logic check
					$lTblMasterCheck = $this->getMasterCheckData( 0
					                                             ,Input::get('txtNGRaasonNameForEntry')
					                                             ,0
					                                            );

					//in case data does not exist,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT
						$lSuccessFlg = $this->insertMasterData();

						//in case update successfully, display message and return to initial screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode in screen
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2060ActionMode', "");
						}
						else
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2060ActionMode', $lPrevMode);
						}
					}
					else
					//in case data exists
					{
						//exchange result to array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//in case delete flag is valid,make data valid for re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrCheckMaster["OFFSET_NG_REASON_ID"]
																	 ,TRIM(Input::get('txtNGRaasonNameForEntry'))
																	 ,TRIM(Input::get('cmbOffsetNGBunruiForEntry'))
																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2060ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2060ActionMode', $lPrevMode);
							}
						}
						else
						//in case delete flag is invalid,key reduplication error
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2060ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result data
					$lTblSearchResultData = Session::get('ZA2060SearchResultData');

					//set list data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//change corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;

						//get data corresponding to OFFSET reason/NG reason ID in edit field and start process
						if(TRIM(Input::get('txtNGRaasonIDForEntry')) == TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"]))
						{
							//get data for logic check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"])
							                                             ,""
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case of getting data,change corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//in case data does not exist or version is not same,error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2060ActionMode', $lPrevMode);
							}
							else
							//in case of no error,start to update
							{
								//check OFFSET reason/NG reason reduplication

								//check logic key reduplication
								$lTblMasterCheck = $this->getMasterCheckDataForUpdate(TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"])
								                                             ,TRIM(Input::get('txtNGRaasonNameForEntry'))
								                                            );

								//in case data in OFFSET reason/NG reason is valid
								if ((count($lTblMasterCheck) != 0))
								{
									//set error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E992 : Same data is already registered."
									]);
									//keep the same condition to before update button is push for screen lock mode
									$lMode = $lPrevMode;
									Session::put('ZA2060ActionMode', $lPrevMode);
								}
								else
								//in case data does not exist,update
								{
									//update
									$lKohshinCount = $this->updateMasterData(
																			  TRIM(Input::get('txtNGRaasonIDForEntry'))
																			 ,TRIM(Input::get('txtNGRaasonNameForEntry'))
																			 ,TRIM(Input::get('cmbOffsetNGBunruiForEntry'))
																			 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																			 ,"0"
																			);

									//in case update successfully, display message and return to initial screen
									if ($lKohshinCount != 0)
									{
										//finishing message
										$lViewData["NormalMessage"] = "I005 : Process has been completed.";

										//set lock mode in screen
										$this->initializeSessionData();
										$lMode = "";
										Session::put('ZA2060ActionMode', "");
									}
									else
									{
										//set error message
										$lViewData["errors"] = new MessageBag([
											"error" => "E999 : System error has occurred. Contact your system manager."
										]);
										//keep the same condition to before update button is push for screen lock mode
										$lMode = $lPrevMode;
										Session::put('ZA2060ActionMode', $lPrevMode);
									}
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"NGRaason Cd"       => Input::get('txtNGRaasonIDForEntry'       ,''),
					"NGRaason Name"      => Input::get('txtNGRaasonNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2060ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2060ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result data
				$lTblSearchResultData = Session::get('ZA2060SearchResultData');

				//set list data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//change corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to OFFSET reason/NG reason ID in edit field and start process
					if(TRIM(Input::get('txtNGRaasonIDForEntry')) == TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"]))
					{
						//get data for logic check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"])
						                                             ,""
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case of getting data,change corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//in case data does not exist or version is not same,error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2060ActionMode', $lPrevMode);
						}
						else
						//in case of no error,start to update
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["OFFSET_NG_REASON_ID"]
																	 ,$lArrDataRow["OFFSET_NG_REASON"]
																	 ,$lArrDataRow["OFFSET_NG_BUNRUI_ID"]
																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2060ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2060ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidCustomerID"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key in corresponding line
			$lNGRaasonId = Input::get('hidPrimaryKey1');
			//get search result data
			$lTblSearchResultData = Session::get('ZA2060SearchResultData');

			//set list data in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//change corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//in case OFFSET reason/NG reasonID. exists,write over on lViewData and session to set in edit field
				if(TRIM((String)$lNGRaasonId) == TRIM((String)$lArrDataRow["OFFSET_NG_REASON_ID"]))
				{
					//write down in session
					Session::put('ZA2060NGRaasonCDForEntry', $lArrDataRow["OFFSET_NG_REASON_ID"]);
					Session::put('ZA2060NGRaasonNameForEntry', $lArrDataRow["OFFSET_NG_REASON"]);
					Session::put('ZA2060OffsetNGBunruiForEntry', $lArrDataRow["OFFSET_NG_BUNRUI_ID"]);
					Session::put('ZA2060DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["NGRaasonCDForEntry"] = Session::get('ZA2060NGRaasonCDForEntry');
					$lViewData["NGRaasonNameForEntry"] = Session::get('ZA2060NGRaasonNameForEntry');
					$lViewData["OffsetNGBunruiForEntry"] = Session::get('ZA2060OffsetNGBunruiForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2060DisplayOrderForEntry');
				}
			}

			//set lock mode in screen
			$lMode = "Edit";
			Session::put('ZA2060ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
		
			//clear all information except entry information in this screen session
			//in case URL in origin of transition does not include"index.php/user/ngreasonmaster",False
			//in case origin of transition is other,return string
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/ngreasonmaster');

				if($lPrevURL == false)
				{
					//delete all search information
					$this->initializeSessionData();

					//set lock mode in screen
					$lMode = "";
					Session::put('ZA2060ActionMode', "");
				}
				else
				{
					//if search result data does not exist in session,issue  blank
					if (is_null(Session::get('ZA2060SearchResultData')))
					{
						//set lock mode in screen
						$lMode = "";
						Session::put('ZA2060ActionMode', "");
					}
					else
					{
						//set lock mode in screen
						$lMode = "Search";
						Session::put('ZA2060ActionMode', "Search");
					}
				}
			}
		}

		//if search result data does not exist in session,issue  blank
		if (is_null(Session::get('ZA2060SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//get search result data from session
			$lTblSearchResultData = Session::get('ZA2060SearchResultData');
		}

		//make pagenation
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/ngreasonmaster')); 
		 //data,total issue, issue per 1 page
		
		//add to array for transportion to screen
		$lViewData += [
			"Pagenator"       => $lPagenation,
		];

		//set lock information in screen
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2060ActionMode'));
		return View("user.ngreasonmaster", $lViewData);

	}

	//**************************************************************************
	// processing name    lockControls
	// over view      ボタン等の画面の各項目のロック情報を設定する。
	// parameter      ビューデータ・画面のモード
	// returned value    ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"KeyEditLock" => "",
				];
				break;
			case 'NewAdd':
				//新規追加 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "True",
					"KeyEditLock" => "Lock",
				];
				break;
			case 'Edit':
				//編集 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"    => "Lock",
					"DeleteLock"    => "",
					"EditVisible"   => "True",
					"KeyEditLock" => "Lock",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"KeyEditLock" => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"KeyEditLock" => "Lock",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getMasterCheckData
	// over view      for master existance checkの物理キーに該当するデータを取得する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pNGRaasonId,$pNGRaasonName,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  NGRSN.DATA_REV
		           ,NGRSN.DELETE_FLG
		           ,NGRSN.OFFSET_NG_REASON_ID
		      FROM TNGRESNM AS NGRSN			/* OFFSET理由/NG理由マスタ */
		     WHERE NGRSN.OFFSET_NG_REASON_ID	= IF(:NGRaasonId1 <> 0, :NGRaasonId2, NGRSN.OFFSET_NG_REASON_ID)		/* 画面入力値 */
		       AND NGRSN.OFFSET_NG_REASON		= IF(:NGRaasonName1 <> "", :NGRaasonName2, NGRSN.OFFSET_NG_REASON)		/* 画面入力値 */
		       AND NGRSN.DATA_REV				= IF(:DataRev1 <> 0, :DataRev2, NGRSN.DATA_REV)							/* 無ければ条件にしない */
		',
			[
				"NGRaasonId1"   => $pNGRaasonId,
				"NGRaasonId2"   => $pNGRaasonId,
				"NGRaasonName1" => $pNGRaasonName,
				"NGRaasonName2" => $pNGRaasonName,
				"DataRev1"      => $pDataRev,
				"DataRev2"      => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    getMasterCheckDataForUpdate
	// over view      for master existance checkの論理キーに該当するデータが他に存在しないかチェックする
	// parameter      マスタのキー値
	//           OFFSET理由/NG理由
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckDataForUpdate($pNGRaasonId,$pNGRaasonName)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  NGRSN.DATA_REV
		           ,NGRSN.DELETE_FLG
		           ,NGRSN.OFFSET_NG_REASON_ID
		      FROM TNGRESNM AS NGRSN			/* OFFSET理由/NG理由マスタ */
		     WHERE NGRSN.OFFSET_NG_REASON_ID	<> :NGRaasonId		/* 画面入力値 */
		       AND NGRSN.OFFSET_NG_REASON		=  :NGRaasonName	/* 画面入力値 */
		',
			[
				"NGRaasonId"   => $pNGRaasonId,
				"NGRaasonName" => $pNGRaasonName,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    getSearchMasterData
	// over view      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programer    k-kagawa
	//**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table of inspection result list

		$lTblSearchResultData = DB::select('
		    SELECT  NGRSN.OFFSET_NG_REASON_ID
		           ,NGRSN.OFFSET_NG_REASON
		           ,NGRSN.OFFSET_NG_BUNRUI_ID
		           ,COALESCE(NRBN.OFFSET_NG_BUNRUI_NAME,"") AS OFFSET_NG_BUNRUI_NAME
		           ,NGRSN.DISPLAY_ORDER
		           ,NGRSN.DELETE_FLG
		           ,NGRSN.DATA_REV
		      FROM TNGRESNM AS NGRSN                                                /* OFFSET理由/NG理由マスタ */
		LEFT OUTER JOIN TNGREBNM NRBN												/* 不良理由分類マスタ */
		             ON NGRSN.OFFSET_NG_BUNRUI_ID = NRBN.OFFSET_NG_BUNRUI_ID
		     WHERE NGRSN.DELETE_FLG          = "0"                                  /* deleteフラグ */
		       AND NGRSN.OFFSET_NG_REASON    LIKE :NGRaasonName                     /* OFFSET理由/NG理由名(部分一致) */
		  ORDER BY NGRSN.DISPLAY_ORDER
		          ,NGRSN.OFFSET_NG_REASON_ID
		',
			[
				"NGRaasonName"    => "%".TRIM((String)Input::get('txtNGRaasonNameForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    insertMasterData
	// over view      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lNGRaasonName = Input::get('txtNGRaasonNameForEntry');
		$lNGBunruiId   = Input::get('cmbOffsetNGBunruiForEntry');
		$lDisplayOrder  = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			function() use	(
							  $lNGRaasonName
							 ,$lNGBunruiId
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::insert
				('
						INSERT INTO TNGRESNM
						(
						  OFFSET_NG_REASON_ID
						 ,OFFSET_NG_REASON
						 ,OFFSET_NG_BUNRUI_ID
						 ,DISPLAY_ORDER
						 ,DELETE_FLG
						 ,DATA_REV
						)
						VALUES (
						         NULL
						        ,:NGRaasonName
						        ,:NGBunruiId
						        ,:DisplayOrder
						        ,"0"
						        ,1
						       )
				', 
					[
						"NGRaasonName"	=> $lNGRaasonName,
						"NGBunruiId"	=> $lNGBunruiId,
						"DisplayOrder"	=> $lDisplayOrder,
					]
				);
				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	    
	}

	//**************************************************************************
	// processing name    updateMasterData
	// over view      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	//**************************************************************************
	private function updateMasterData
	(
	  $pNGRaasonId
	 ,$pNGRaasonName
	 ,$pNGBunruiId
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{

		//更新の成否をセット
		$lKohshinCnt = "";

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			function() use (
							 $pNGRaasonId
							,$pNGRaasonName
							,$pNGBunruiId
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				$lKohshinCnt = DB::update
				('
						UPDATE TNGRESNM
						   SET  OFFSET_NG_REASON	= :NGRaasonName
						       ,OFFSET_NG_BUNRUI_ID = :NGBunruiId
						       ,DISPLAY_ORDER		= :DisplayOrder
						       ,DELETE_FLG			= :DeleteFlg
						       ,DATA_REV			= DATA_REV + 1
						 WHERE OFFSET_NG_REASON_ID	= :NGRaasonIdWhere
				', 
					[
						"NGRaasonIdWhere"	=> $pNGRaasonId,
						"NGRaasonName"		=> $pNGRaasonName,
						"NGBunruiId"		=> $pNGBunruiId,
						"DisplayOrder"		=> $pDisplayOrder,
						"DeleteFlg"			=> $pDeleteFlg,
					]
				);
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	    
	}

	//**************************************************************************
	// processing name    keepFromInputValue
	// over view      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
		//■登録用・NGRaason Cdの項目保持
		if (Input::has('txtNGRaasonIDForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2060NGRaasonCDForEntry', Input::get('txtNGRaasonIDForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2060NGRaasonCDForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"NGRaasonCDForEntry"  => Session::get('ZA2060NGRaasonCDForEntry')
		];
		
		//■登録用・不良理由名の項目保持
		if (Input::has('txtNGRaasonNameForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2060NGRaasonNameForEntry', Input::get('txtNGRaasonNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2060NGRaasonNameForEntry', "");
		}

		//■登録用・不良理由分類の項目保持
		if (Input::has('cmbOffsetNGBunruiForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2060OffsetNGBunruiForEntry', Input::get('cmbOffsetNGBunruiForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2060OffsetNGBunruiForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"OffsetNGBunruiForEntry"  => Session::get('ZA2060OffsetNGBunruiForEntry')
		];

		//■登録用・DisplayOrderの項目保持
		if (Input::has('txtDisplayOrderForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2060DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			//画面の値が無ければサーバ日付をwrite down in session。
			Session::put('ZA2060DisplayOrderForEntry', "");
		}

		//セッションには画面値か業務日付が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2060DisplayOrderForEntry')
		];

		//■検索用・NGRaason Cdの項目保持
		//in case value does not exist in session
		if (Input::has('txtNGRaasonNameForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2060NGRaasonNameForSearch', Input::get('txtNGRaasonNameForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2060NGRaasonNameForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"NGRaasonNameForSearch"  => Session::get('ZA2060NGRaasonNameForSearch')
		];
		
		if (Input::has('txtNGRaasonNameForEntry')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2060NGRaasonNameForEntry', Input::get('txtNGRaasonNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2060NGRaasonNameForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"NGRaasonNameForEntry"  => Session::get('ZA2060NGRaasonNameForEntry')
		];

		return $pViewData;
	}

	//**************************************************************************
	// processing name    initializeSessionData
	// over view      セッションデータのクリア（検索時・編集時の値）
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2060SearchResultData');

		//Entry button時に設定しているものをクリア
		Session::forget('ZA2060NGRaasonCDForEntry');
		Session::forget('ZA2060NGRaasonNameForEntry');
		Session::forget('ZA2060DisplayOrderForEntry');
		Session::forget('ZA2060OffsetNGBunruiForEntry');

		return null;
	}

	//**************************************************************************
	// processing name    isErrorForRegist
	// over view      登録時に実施する必須・型式チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須チェック(IDは自動採番するので必須チェックしない)
		//-------------------------
		//NGRaason名必須チェック
		$lValidator = Validator::make(
			array('txtNGRaasonNameForEntry' => Input::get('txtNGRaasonNameForEntry')),
			array('txtNGRaasonNameForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter OFFSET/NG Reason."
			]);
			return $pViewData;
		}
		
		//NGRaason名必須チェック
		$lValidator = Validator::make(
			array('cmbOffsetNGBunruiForEntry' => Input::get('cmbOffsetNGBunruiForEntry')),
			array('cmbOffsetNGBunruiForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter OFFSET/NG Class."
			]);
			return $pViewData;
		}

		//表示順必須チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}

		//-------------------------
		//型式チェック
		//-------------------------
		//表示順型チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}

	//**************************************************************************
	// processing name    setOffsetNGBunrui
	// over view      NG/OFFSet分類ボックス用のデータを取得し、画面表示用の配列にセットする
	// parameter      画面表示用配列
	// returned value    画面表示用配列
	//**************************************************************************
	private function setOffsetNGBunrui($pViewData)
	{
		$lOffsetNGBunruiList     = ["" => ""];  //返却用ビューデータ

		//OFFSET理由/NG理由の検索処理
		$lOffsetNGBunruiList = $this->getOffsetNGBunruiList();
		
		//add to array for transportion to screen
		$pViewData += [
			"arrOffsetNGBunruiList" => $lOffsetNGBunruiList
		];

		return $pViewData;
	}

	//**************************************************************************
	// processing name    getOffsetNGBunruiList
	// over view      NG/OFFSet分類コンボボックス用のデータをSQLで取得
	// parameter      nothing
	// returned value    取得結果の配列
	//**************************************************************************
	private function getOffsetNGBunruiList()
	{
		$lTblOffsetNGBunrui			= []; //NG/OFFSet分類取得結果
		$lRowOffsetNGBunrui			= []; //配列変換時のワーク領域
		$lArrOffsetNGBunrui			= []; //返却するNG/OFFSet分類リスト

		$lTblOffsetNGBunrui = DB::select(
		'
		      SELECT NRBN.OFFSET_NG_BUNRUI_ID
		            ,NRBN.OFFSET_NG_BUNRUI_NAME
		        FROM TNGREBNM AS NRBN
		       WHERE NRBN.DELETE_FLG = "0"
		    ORDER BY NRBN.DISPLAY_ORDER
		'
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblOffsetNGBunrui = (array)$lTblOffsetNGBunrui;

		//空白行を追加
		$lArrOffsetNGBunrui += [
			"" => ""
		];

		//データが存在する
		if ($lTblOffsetNGBunrui != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblOffsetNGBunrui as $lRowOffsetNGBunrui) {

				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowOffsetNGBunrui = (Array)$lRowOffsetNGBunrui;

				$lArrOffsetNGBunrui += [
					$lRowOffsetNGBunrui["OFFSET_NG_BUNRUI_ID"] => $lRowOffsetNGBunrui["OFFSET_NG_BUNRUI_NAME"]
				];
			
			}
			
		}

		return $lArrOffsetNGBunrui;
	}

}