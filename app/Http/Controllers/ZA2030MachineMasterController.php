<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Log;
use Session;
use Validator;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    machine maintenance
// over view      maintain machine master
// programer    k-kagawa
// date    2014.12.06
// update  
//           
//**************************************************************************
class ZA2030MachineMasterController
extends Controller
{

	//-------------
	//■■define constant
	CONST NUMBER_PER_PAGE = 10;		//total number of data per 1 page

	//**************************************************************************
	// process    MasterAction
	// over view      display initial screen
	//           separate process as Entry,Search,Modify button
	//           do process corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion of data to screen
		
		$lTblSearchResultData		= []; //data table of inspection result list
		$lPagenation				= []; //for paging

		$lTblMasterCheck 			= []; //for master existance check

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //lock mode
		$lPrevMode					= ""; //lock mode before transition

		//keep entry item and re-set
		$lViewData = $this->keepFromInputValue($lViewData);

		//receive parameter from login screen through Session and issue to array to transport to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"Machine Name"  => Input::get('txtMachineNameForSearch'   ,''),
					"M/C No."       => Input::get('txtMachineNoForSearch'      ,''),
				]
			);

			//in case of no error ,search
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search process
				$lTblSearchResultData = $this->getSearchMasterData();
				
				//0 issue error
				if (count($lTblSearchResultData) == 0)
				{
					//set error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				
				//store in session
				Session::put('ZA2030SearchResultData', $lTblSearchResultData);

				//set lock mode in screen
				$lMode = "Search";
				Session::put('ZA2030ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//make value in edit field initial value

			//exchange session to initial value
			Session::put('ZA2030MachineNoForEntry', "");
			Session::put('ZA2030MachineNameForEntry', "");
			Session::put('ZA2030DisplayOrderForEntry', "");

			//exchange view data to initial value
			$lViewData["MachineNoForEntry"] = "";
			$lViewData["MachineNameForEntry"] = "";
			$lViewData["DisplayOrderForEntry"] = "";

			//set lock mode in screen
			$lMode = "NewAdd";
			Session::put('ZA2030ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"M/C No."       => Input::get('txtMachineNoForEntry'       ,''),
					"M/C Name"      => Input::get('txtMachineNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2030ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2030ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate process corresponding to previous screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case of new entry
					//get data for logic check
					$lTblMasterCheck = $this->getMasterCheckData(Input::get('txtMachineNoForEntry'),0);

					//in case data exists,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT proccess
						$lSuccessFlg = $this->insertMasterData();

						//if update successfully,display message and return initial screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode in screen
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2030ActionMode', "");
						}
						else
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//maintain the same condition to before update button is push for lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2030ActionMode', $lPrevMode);
						}
					}
					else
					//in case data exists
					{
						//change search result to array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//in case delete flag is valid,make data valid to re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  TRIM(Input::get('txtMachineNoForEntry'))
																	 ,TRIM(Input::get('txtMachineNameForEntry'))
																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//if update successfully,display message and return initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2030ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//maintain the same condition to before update button is push for lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2030ActionMode', $lPrevMode);
							}
						}
						else
						//in case delete flag is invalid,key reduplication error
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//maintain the same condition to before update button is push for lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2030ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result
					$lTblSearchResultData = Session::get('ZA2030SearchResultData');

					//set data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//change corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;

						//get data corresponding to machine No. in edit field and start proccessing
						if(TRIM(Input::get('txtMachineNoForEntry')) == TRIM((String)$lArrDataRow["MACHINE_NO"]))
						{
							//get data for logic check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["MACHINE_NO"])
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case of getting data,change corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//in case data does not exist or version is not same,error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//maintain the same condition to before update button is push for lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2030ActionMode', $lPrevMode);
							}
							else
							//start to update in case of no error 
							{
								//update
								$lKohshinCount = $this->updateMasterData(
																		  TRIM(Input::get('txtMachineNoForEntry'))
																		 ,TRIM(Input::get('txtMachineNameForEntry'))
																		 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																		 ,"0"
																		);

								//if update successfully,display message and return initial screen
								if ($lKohshinCount != 0)
								{
									//finishing message
									$lViewData["NormalMessage"] = "I005 : Process has been completed.";

									//set lock mode in screen
									$this->initializeSessionData();
									$lMode = "";
									Session::put('ZA2030ActionMode', "");
								}
								else
								{
									//set error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E999 : System error has occurred. Contact your system manager."
									]);
									//maintain the same condition to before update button is push for lock mode 
									$lMode = $lPrevMode;
									Session::put('ZA2030ActionMode', $lPrevMode);
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"M/C No."       => Input::get('txtMachineNoForEntry'       ,''),
					"M/C Name"      => Input::get('txtMachineNameForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2030ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2030ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result
				$lTblSearchResultData = Session::get('ZA2030SearchResultData');

				//set data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//change corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to machine No. in edit field and start proccessing
					if(TRIM(Input::get('txtMachineNoForEntry')) == TRIM((String)$lArrDataRow["MACHINE_NO"]))
					{
						//get data for logic check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["MACHINE_NO"])
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case of getting data,change corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//in case data does not exist or version is not same,error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//maintain the same condition to before update button is push for lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2030ActionMode', $lPrevMode);
						}
						else
						//start to update in case of no error
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["MACHINE_NO"]
																	 ,$lArrDataRow["MACHINE_NAME"]
																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//if update successfully,display message and return initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2030ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//maintain the same condition to before update button is push for lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2030ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidMachineNo"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key in selected line
			$lMachineNo = Input::get('hidPrimaryKey1');
			//get search result
			$lTblSearchResultData = Session::get('ZA2030SearchResultData');

			//set list data in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//change corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//if machine No. exists,write over it on lViewData and session to set edit field
				if(TRIM((String)$lMachineNo) == TRIM((String)$lArrDataRow["MACHINE_NO"]))
				{
					//write down in session
					Session::put('ZA2030MachineNoForEntry', $lArrDataRow["MACHINE_NO"]);
					Session::put('ZA2030MachineNameForEntry', $lArrDataRow["MACHINE_NAME"]);
					Session::put('ZA2030DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["MachineNoForEntry"] = Session::get('ZA2030MachineNoForEntry');
					$lViewData["MachineNameForEntry"] = Session::get('ZA2030MachineNameForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2030DisplayOrderForEntry');
				}
			}

			//set lock mode in screen
			$lMode = "Edit";
			Session::put('ZA2030ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
		
			//clear all information except of entry information in this screen session
			//if the origin of transition does not include "index.php/user/machinemaster",False
			//if the origin of transition is other,return string
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/machinemaster');

				if($lPrevURL == false)
				{
					//delete search information(initialization)
					$this->initializeSessionData();

					//set lock mode in screen
					$lMode = "";
					Session::put('ZA2030ActionMode', "");
				}
				else
				{
					//if search result list does not exist in session,issue blank
					if (is_null(Session::get('ZA2030SearchResultData')))
					{
						//set lock mode in screen
						$lMode = "";
						Session::put('ZA2030ActionMode', "");
					}
					else
					{
						//set lock mode in screen
						$lMode = "Search";
						Session::put('ZA2030ActionMode', "Search");
					}
				}
			}
		}

		//if search result list does not exist in session,issue blank
		if (is_null(Session::get('ZA2030SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//if search result exists in session, get it
			$lTblSearchResultData = Session::get('ZA2030SearchResultData');
		}

		//make pagenation
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/machinemaster')); 
		 //データ、総件数、1ページあたり件数
		
		//add to array to transpot to screen
		$lViewData += [
			"Pagenator"       => $lPagenation,
		];

		//set lock information in screen
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2030ActionMode'));

		return View("user.machinemaster", $lViewData);

	}

	//**************************************************************************
	// process    lockControls
	// over view      ボタン等の画面の各項目のロック情報を設定する。
	// parameter      ビューデータ・画面のモード
	// returned value    ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			case 'NewAdd':
				//新規追加 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "",
				];
				break;
			case 'Edit':
				//編集 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "Lock",
					"DeleteLock"   => "",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "Lock",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "Lock",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// process    getMasterCheckData
	// over view      論理キー重複データ件数取得
	//           検査実績ヘッダが論理キーで重複するデータの件数を返却する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pMachineno,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  MACH.DATA_REV
		           ,MACH.DELETE_FLG
		      FROM TMACHINM AS MACH															/* マシンマスタ */
		     WHERE MACH.MACHINE_NO          = :machineno									/* 画面入力値　無ければ条件にしない */
		       AND MACH.DATA_REV            = IF(:DataRev1 <> 0, :DataRev2, MACH.DATA_REV)	/* 画面選択値　無ければ条件にしない */
		',
			[
				"machineno"    => $pMachineno,
				"DataRev1"     => $pDataRev,
				"DataRev2"     => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process    getSearchMasterData
	// over view      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programer    k-kagawa
	//**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table of inspection result list

		$lTblSearchResultData = DB::select('
		    SELECT MACH.MACHINE_NO
		          ,MACH.MACHINE_NAME
		          ,MACH.DISPLAY_ORDER
		          ,MACH.DELETE_FLG
		          ,MACH.DATA_REV
		      FROM TMACHINM AS MACH /* マシンマスタ */
		     WHERE MACH.DELETE_FLG          = "0"                                   /* deleteフラグ */
		       AND MACH.MACHINE_NO          LIKE :machineno                         /* マシンNo(前方一致) */
		       AND MACH.MACHINE_NAME        LIKE :machinename                       /* マシン名(部分一致) */
		  ORDER BY MACH.DISPLAY_ORDER
		          ,MACH.MACHINE_NO
		',
			[
				"machineno"       => TRIM((String)Input::get('txtMachineNoForSearch'))."%",
				"machinename"     => "%".TRIM((String)Input::get('txtMachineNameForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process    insertMasterData
	// over view      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lMachineNo = Input::get('txtMachineNoForEntry');
		$lMachineName = Input::get('txtMachineNameForEntry');
		$lDisplayOrder = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			function() use	(
							  $lMachineNo
							 ,$lMachineName
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::insert
				('
						INSERT INTO TMACHINM
						(
						  MACHINE_NO
						 ,MACHINE_NAME
						 ,DISPLAY_ORDER
						 ,DELETE_FLG
						 ,DATA_REV
						)
						VALUES (
						         :Machineno
						        ,:MachineName
						        ,:DisplayOrder
						        ,"0"
						        ,1
						       )
				', 
					[
						"Machineno"		=> $lMachineNo,
						"MachineName"	=> $lMachineName,
						"DisplayOrder"	=> $lDisplayOrder,
					]
				);
				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	    
	}

	//**************************************************************************
	// process    updateMasterData
	// over view      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	//**************************************************************************
	private function updateMasterData
	(
	  $pMachineNo
	 ,$pMachineName
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{

		//更新の成否をセット
		$lKohshinCnt = "";

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			function() use (
							 $pMachineNo
							,$pMachineName
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				$lKohshinCnt = DB::update
				('
						UPDATE TMACHINM
						   SET  MACHINE_NO		= :Machineno
						       ,MACHINE_NAME	= :MachineName
						       ,DISPLAY_ORDER	= :DisplayOrder
						       ,DELETE_FLG		= :DeleteFlg
						       ,DATA_REV		= DATA_REV + 1
						 WHERE MACHINE_NO	= :MachinenoWhere
				', 
					[
						"Machineno"			=> $pMachineNo,
						"MachinenoWhere"	=> $pMachineNo,
						"MachineName"		=> $pMachineName,
						"DisplayOrder"		=> $pDisplayOrder,
						"DeleteFlg"			=> $pDeleteFlg,
					]
				);
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	    
	}

	//**************************************************************************
	// process    keepFromInputValue
	// over view      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
		//■登録用・M/C No.の項目保持
		if (Input::has('txtMachineNoForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2030MachineNoForEntry', Input::get('txtMachineNoForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2030MachineNoForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"MachineNoForEntry"  => Session::get('ZA2030MachineNoForEntry')
		];
		
		//■登録用・MachineNameの項目保持
		if (Input::has('txtMachineNameForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2030MachineNameForEntry', Input::get('txtMachineNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2030MachineNameForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"MachineNameForEntry"  => Session::get('ZA2030MachineNameForEntry')
		];
		
		//■登録用・DisplayOrderの項目保持
		if (Input::has('txtDisplayOrderForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2030DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			//画面の値が無ければサーバ日付をwrite down in session。
			Session::put('ZA2030DisplayOrderForEntry', "");
		}

		//セッションには画面値か業務日付が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2030DisplayOrderForEntry')
		];

		//■検索用・M/C No.の項目保持
		//セッションに値が無い場合
		if (Input::has('txtMachineNoForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2030MachineNoForSearch', Input::get('txtMachineNoForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2030MachineNoForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"MachineNoForSearch"  => Session::get('ZA2030MachineNoForSearch')
		];
		
		//■検索用・MachineNameの項目保持
		//セッションに値が無い場合
		if (Input::has('txtMachineNameForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2030MachineNameForSearch', Input::get('txtMachineNameForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2030MachineNameForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"MachineNameForSearch"  => Session::get('ZA2030MachineNameForSearch')
		];

		return $pViewData;
	}

	//**************************************************************************
	// process    initializeSessionData
	// over view      セッションデータのクリア（検索時・編集時の値）
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2030SearchResultData');

		//Entry button時に設定しているものをクリア
		Session::forget('ZA2030MachineNoForEntry');
		Session::forget('ZA2030MachineNameForEntry');
		Session::forget('ZA2030DisplayOrderForEntry');

		return null;
	}

	//**************************************************************************
	// process    isErrorForRegist
	// over view      登録時に実施する必須・型式チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須チェック
		//-------------------------
		//M/C No.必須チェック
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E009 : Enter M/C No. ."
			]);
			return $pViewData;
		}

		//M/C名必須チェック
		$lValidator = Validator::make(
			array('txtMachineNameForEntry' => Input::get('txtMachineNameForEntry')),
			array('txtMachineNameForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Machine Name."
			]);
			return $pViewData;
		}

		//表示順必須チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}

		//-------------------------
		//型式チェック
		//-------------------------
		//M/C No.型チェック
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('alpha_dash'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E010 : M/C No. is invalid."
			]);
			return $pViewData;
		}

		//表示順型チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}

}