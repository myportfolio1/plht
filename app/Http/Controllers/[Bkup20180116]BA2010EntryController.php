<?php

use Illuminate\Support\MessageBag;

//■■15-08-2016 Motooka added for SUNLIT■■
set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';

class BA2010EntryController
extends Controller
{
	//Result Class
	CONST RESULT_CLASS_OK            = "1"; //OK
	CONST RESULT_CLASS_OFFSET        = "2"; //OFFSET
	CONST RESULT_CLASS_NG            = "3"; //NG
	CONST RESULT_CLASS_NO_CHECK      = "4"; //NO CHECK
	CONST RESULT_CLASS_LINE_STOP     = "5"; //LINE STOP

	//Result Input Type
	CONST RESULT_INPUT_TYPE_NUMBER   = "1"; //NUMBER(TEXT)
	CONST RESULT_INPUT_TYPE_OKNG     = "2"; //OK/NG(RadioButton)

	//OFFSET NG Action
	CONST OFFSET_NG_ACTION_RUNNING   = "1"; //RUNNING
	CONST OFFSET_NG_ACTION_STOP_LINE = "2"; //STOP LINE

	//アクション明細の改行する数字
	CONST ACTION_DETAILS_KAIGYO      = 30;

	//Planing Inspection (Normal)
	CONST CONDITION_NORMAL           = "01";
	//No Planing Inspection (Abnormal)
	CONST INSPECTION_TIME_ALL        = "ZZ";
	
	//■■10-03-2017 Motooka added for Kohbyo■■
	//XBAR (UCL&LCL)
	CONST COEFFICIENT_XBAR           = "A2";
	//RCHAT_UCL
	CONST COEFFICIENT_RCHAT_UCL      = "D4";
	//RCHAT_LCL
	CONST COEFFICIENT_RCHAT_LCL      = "D3";

	//**************************************************************************
	// 処理名    EntryAction
	// 概要      検査結果登録画面の初期表示を行う。
	//           また、Entry等のボタンにより処理を分岐し、
	//           それぞれに対応した処理を行う。
	// 引数      無し
	// 戻り値    無し
	// 作成者    s-miyamoto
	// 作成日    2014.05.23
	// 更新履歴  2014.05.23 v0.01 初回作成
	//           2014.08.26 v1.00 納品で一旦FIX
	//           2017.03.31 v1.01 added For Kohbyo
	//**************************************************************************
	public function EntryAction()
	{
		$lViewData = [];                     //画面へのデータ移送用

		$lTblInspectionNo           = [];    //検査番号マスタのDataTable
		$lRowInspectionNo           = [];    //検査番号マスタのDataRow

		$lInspectionNo              = "";    //検査番号（これは文字型も入る可能性がある。例：1、2、3a、4、5a、6a、7…）

		$lDataRowNo                 = 0;     //現在処理中の検査番号に該当するマスタ行番（0開始）
		$lAdjustmentNo              = 0;     //前後の項目に移動するための調整用番号

		$lEntryFlg                  = false; //Entryで遷移した場合のみTrueになるフラグ

		$lInspectionResultNo        = 0;     //検査実績番号　変更時はパラメータの値を格納
		                                     //新規登録時は1件目の時初期値（ゼロ）、2件目以降は1件目の登録で採番された値

		$lTblInspectionResultDetail = [];    //検査実績明細DataTable
		$lRowInspectionResultDetail = [];    //検査実績明細DataRow

		$lOnchangeFlg               = "0";   //flag to change combo
		
		//■■15-08-2016 Motooka added for SUNLIT■■
		$EquipmentCode              = "";
		
		//Motooka added for Kohbyo
		$lCurrentlRowInspectionNo                = [];
		$lTblBeforeCommitInspectionResultDetail1 = [];
		$lRowBeforeCommitInspectionResultDetail1 = [];
		$lTblBeforeCommitInspectionResultDetail2 = [];
		$lRowBeforeCommitInspectionResultDetail2 = [];
		$lTblBeforeCommitInspectionResultDetailValueData = [];
		$lRowBeforeCommitInspectionResultDetailValueData = [];
		$lTblBeforeCommitInspectionResultAnalyze = [];
		$lRowBeforeCommitInspectionResultAnalyze = [];
		$lTblCoefficientXbar                     = [];
		$lRowCoefficientXbar                     = [];
		$lTblCoefficientRChartUCL                = [];
		$lRowCoefficientRChartUCL                = [];
		$lTblCoefficientRChartLCL                = [];
		$lRowCoefficientRChartLCL                = [];
		$lTblInspectionResultAnalyze             = [];  //TRESANAL DataTable
		$lRowInspectionResultAnalyze             = [];  //TRESANAL DataRow
		
		$lWorkMaxValue                           = 0;
		$lWorkMinValue                           = 0;
		$lAnalyticalGroup                        = 0;
		$lAnalyticalGroupPoint                   = 0;
		$lAnalyticalGroupHowmany                 = 0;
		$lStdSpecUnder                           = 0;
		$lStdSpecOver                            = 0;
		$lPictureURL                             = "";
		$lCalcNam                                = 0;
		$lMaxValue                               = 0;
		$lMinValue                               = 0;
		$lAverageValue                           = 0;
		$lRangeValue                             = 0;
		$lStdev                                  = 0;
		$lWorkCpkStdSpecOver                     = 0;
		$lWorkCpkStdSpecUnder                    = 0;
		$lCpk                                    = 0;
		$lXbarUcl                                = 0;
		$lXbarLcl                                = 0;
		$lRchartUcl                              = 0;
		$lRchartLcl                              = 0;
		$lJudgeReason                            = "";
		$lJudgeMin                               = 0;
		$lJudgeMax                               = 0;
		$lStdSpecCenter                          = 0;
		$lCodeNum                                = 0;

		//get parameter from login screen through Session and enter to Array to transport to screen
		$lViewData += [
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		//set combo of Judge reason
		$lViewData = $this->setJudgeReason($lViewData);
		
		//set combo of OFFSET reason/NG category
		$lViewData = $this->setOffsetNGBunrui($lViewData);

		//set combo of Action
		$lViewData = $this->setOffsetNGActionDetail($lViewData);

		//set combo of technitian master
		$lViewData = $this->setTecReqPerson($lViewData);

		//get parameter from search result list through Session and enter to Array to transport to screen
		if (Session::has('BA1010InspectionResultNo'))
		{
			//in case screen is transported from Modify
			$lViewData += [
				"MasterNo"               => Session::get('BA1010MasterNo'),
				"Process"                => Session::get('BA1010ProcessForModify'),
				"ProcessName"            => Session::get('BA1010ProcessForModifyName'),
				"CustomerName"           => Session::get('BA1010CustomerName'),
				"CheckSheetNo"           => Session::get('BA1010CheckSheetNoForModify'),
				"CheckSheetName"         => Session::get('BA1010CheckSheetName'),
				"RevisionNo"             => Session::get('BA1010RevisionNo'),
				"ItemNo"                 => Session::get('BA1010ItemNo'),
				"ItemName"               => Session::get('BA1010ItemName'),
				"MaterialName"           => Session::get('BA1010MaterialNameForModify'),
				"MaterialSize"           => Session::get('BA1010MaterialSizeForModify'),
				"MaterialLotNoForEntry"  => Session::get('BA2020MaterialLotNoForEntry'),
				"MachineNo"              => Session::get('BA1010MachineNoForModify'),
				"MachineNoName"          => Session::get('BA1010MachineNoForModifyName'),
				"InspectorCode"          => Session::get('BA1010InspectorCodeForModify'),
				"InspectorName"          => Session::get('BA1010InspectorName'),
				"InspectionDate"         => Session::get('BA1010InspectionDateForModify'),
				"Condition"              => Session::get('BA1010ConditionForModify'),
				"ConditionName"          => Session::get('BA1010ConditionForModifyName'),
				"InspectionTime"         => Session::get('BA1010InspectionTimeForModify'),
				"InspectionTimeName"     => Session::get('BA1010InspectionTimeForModifyName'),

				"ModelName"              => Session::get('BA1010ModelName'),
				"PONoForEntry"           => Session::get('BA2020PONoForEntry'),
				"CertificateNoForEntry"  => Session::get('BA2020CertificateNoForEntry'),
				"InvoiceNoForEntry"      => Session::get('BA2020InvoiceNoForEntry'),
				"QuantityCoilForEntry"   => Session::get('BA2020QuantityCoilForEntry'),
				"QuantityWeightForEntry" => Session::get('BA2020QuantityWeightForEntry'),
				"LotNoForEntry"          => Session::get('BA2020LotNoForEntry'),
				
				"DrawingNo"              => Session::get('BA1010DrawingNo'),
				"ISOKanriNo"             => Session::get('BA1010ISOKanriNo'),
				"ProductWeight"          => Session::get('BA1010ProductWeight'),

				"HeatTreatmentType"      => Session::get('BA1010HeatTreatmentType'),
				"PlatingKind"            => Session::get('BA1010PlatingKind'),
				
//★★画面上部気になる
				
			];

			//store Inspection Result No. transported by parameter in other prameter
			$lInspectionResultNo = Session::get('BA1010InspectionResultNo');
		}
		else
		{
			//in case screen is transported from Entry
			$lViewData += [
				"MasterNo"               => Session::get('BA1010MasterNoForEntry'),
				"Process"                => Session::get('BA1010ProcessForEntry'),
				"ProcessName"            => Session::get('BA1010ProcessForEntryName'),
				"CustomerName"           => Session::get('BA1010CustomerName'),
				"CheckSheetNo"           => Session::get('BA1010CheckSheetNoForEntry'),
				"CheckSheetName"         => Session::get('BA1010CheckSheetName'),
				"RevisionNo"             => Session::get('BA1010RevisionNo'),
				"ItemNo"                 => Session::get('BA1010ItemNo'),
				"ItemName"               => Session::get('BA1010ItemName'),
				"MaterialName"           => Session::get('BA1010MaterialNameForEntry'),
				"MaterialSize"           => Session::get('BA1010MaterialSizeForEntry'),
				"MaterialLotNoForEntry"  => Session::get('BA2020MaterialLotNoForEntry'),
				"MachineNo"              => Session::get('BA1010MachineNoForEntry'),
				"MachineNoName"          => Session::get('BA1010MachineNoForEntryName'),
				"InspectorCode"          => Session::get('BA1010InspectorCodeForEntry'),
				"InspectorName"          => Session::get('BA1010InspectorName'),
				"InspectionDate"         => Session::get('BA1010InspectionDateForEntry'),
				"Condition"              => Session::get('BA1010ConditionForEntry'),
				"ConditionName"          => Session::get('BA1010ConditionForEntryName'),
				"InspectionTime"         => Session::get('BA1010InspectionTimeForEntry'),
				"InspectionTimeName"     => Session::get('BA1010InspectionTimeForEntryName'),

				"ModelName"              => Session::get('BA1010ModelName'),
				"PONoForEntry"           => Session::get('BA2020PONoForEntry'),
				"CertificateNoForEntry"  => Session::get('BA2020CertificateNoForEntry'),
				"InvoiceNoForEntry"      => Session::get('BA2020InvoiceNoForEntry'),
				"QuantityCoilForEntry"   => Session::get('BA2020QuantityCoilForEntry'),
				"QuantityWeightForEntry" => Session::get('BA2020QuantityWeightForEntry'),
				"LotNoForEntry"          => Session::get('BA2020LotNoForEntry'),
				
				"DrawingNo"              => Session::get('BA1010DrawingNo'),
				"ISOKanriNo"             => Session::get('BA1010ISOKanriNo'),
				"ProductWeight"          => Session::get('BA1010ProductWeight'),
				
				"HeatTreatmentType"      => Session::get('BA1010HeatTreatmentType'),
				"PlatingKind"            => Session::get('BA1010PlatingKind'),
				
				
			];
		}

		//search master data,if it does not exist
		if (is_null(Session::get('BA2010InspectionNoData')))
		{
			//in case Condition=nomal
			If ($lViewData["Condition"] == self::CONDITION_NORMAL)
			{
				//read data in Inspection No master(TINSPNOM) in spite of entry and update
				$lTblInspectionNo = $this->getInspectionNoList($lViewData["MasterNo"], $lViewData["Process"], $lViewData["CheckSheetNo"], $lViewData["RevisionNo"], $lViewData["InspectionTime"]);
			}
			//in case Condition≠nomal
			else
			{
				//read data in Inspection No master(TINSPNOM) in spite of entry and update
				$lTblInspectionNo = $this->getInspectionNoList($lViewData["MasterNo"], $lViewData["Process"], $lViewData["CheckSheetNo"], $lViewData["RevisionNo"], self::INSPECTION_TIME_ALL);
			}

			//store in session
			Session::put('BA2010InspectionNoData', $lTblInspectionNo);
		}
		else
		{
			//in case Inspection No is in session, get it
			$lTblInspectionNo = Session::get('BA2010InspectionNoData');
		}

		//in case Condition=nomal
		If ($lViewData["Condition"] == self::CONDITION_NORMAL)
		{
			//set list of Inspection Tool Class
			$lViewData = $this->setInspectionToolClassList($lViewData, $lViewData["CheckSheetNo"], $lViewData["RevisionNo"], $lViewData["InspectionTime"]);
		}
		//in case Condition≠nomal
		else
		{
			//set list of Inspection Tool Class
			$lViewData = $this->setInspectionToolClassList($lViewData, $lViewData["CheckSheetNo"], $lViewData["RevisionNo"], self::INSPECTION_TIME_ALL);
		}

		//in case Equipment is POSTed from screen
		if (Input::has('cmbInspectonToolClass'))
		{
			//store Equipment POSTed in case of Filter button
			if (Input::has('btnFilter'))
			{
				$lViewData += [
					"InspectionToolClass" => (String)Input::get('cmbInspectonToolClass'),
				];
			}
			else
			//store previous Equipment exclusive of Filter button
			//（prevent from error）
			{
				$lViewData += [
					"InspectionToolClass" => (String)Input::get('hidEquipment'),
				];
			}
		}
		else
		//select ALL in case Equipment is not POST from screen
		{
			$lViewData += [
				"InspectionToolClass" => "0",
			];
		}

		//検査番号マスタデータのうち、フィルタに該当するものを削除する処理
		//画面で「ALL」以外が選択されている場合のみ実行
		//delete corresponding to filter in Inspection No. master data
		//selected exception of ALL
		if ($lViewData["InspectionToolClass"] != "0") {

			$lDataCount      = 0;    //line counter
			$lNeedlessNoList = [];   //unnecessary line number list
			$lWorkList       = [];   //work list for line number

			//検査番号マスタデータを順に読み込み
			//read Inspection No. master in order
			foreach ($lTblInspectionNo as $lRowInspectionNo)
			{
				//Cast
				$lRowInspectionNo = (Array)$lRowInspectionNo;

				//Inspection Tool Classが、画面の選択値と異なる場合
				//if Inspection Tool Class differ to value selected of screen
				if ($lRowInspectionNo["INSPECTION_TOOL_CLASS"] != $lViewData["InspectionToolClass"])
				{
					//不要な行番号Listに追加
					//add to unnecessary line number list
					array_push($lNeedlessNoList, $lDataCount);
				}

				//行番号カウントアップ
				//count up line number
				$lDataCount += 1;
			}

			//不要な行番号ListをForEachする
			//ForEach unnecessary line number list
			foreach ($lNeedlessNoList as $lDeleteRowNum)
			{
				//検査番号マスタデータから消す
				//delete from Inspection No. master data
				unset($lTblInspectionNo[$lDeleteRowNum]);
			}

			//残ったデータを0行目から順に入れ直す必要がある。
			//reenter data from 0 line
			foreach ($lTblInspectionNo as $lRowInspectionNo)
			{
				array_push($lWorkList, $lRowInspectionNo);
			}

			//検査番号マスタテーブルを一度クリアする
			//clear Inspection No. master table
			$lTblInspectionNo = [];

			//ワークで番号を振り直したデータを元に戻す
			//replace data in work
			foreach ($lWorkList as $lWorkRow)
			{
				array_push($lTblInspectionNo, $lWorkRow);
			}
		}

		//検査番号マスタの0行目の情報を取得
		//get line 0 data in Inspection No.master
		$lRowInspectionNo = (Array)$lTblInspectionNo[0];

		//検査番号を、0行目の情報とする（画面初回Load時を想定）
		//Inspection No. = data of line 0
		$lInspectionNo                 = $lRowInspectionNo["INSPECTION_NO"];
		
		if (Input::has('btnPageMove'))                      //■■■PageMovebutton■■■
		{
			//log
			Log::write('info', 'BA2010 PageMove Button Click.',
				[
					"Inspection No."           => Input::get('txtInspectionNo', ''),
					"Inspection No.(Hidden)"   => Input::get('hidInspectionNo', ''),
				]
			);

			//must check Inspection No.
			$lValidator = Validator::make(
				array('txtInspectionNo' => Input::get('txtInspectionNo')),
				array('txtInspectionNo' => array('required'))
			);

			//if error
			if ($lValidator->fails())
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E025 : Enter Inspection No. ."
				]);
			}

			//if no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//Inspection No. follows to value of entry
				$lInspectionNo  = (String)Input::get('txtInspectionNo','');
			}
			else
			//if error
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}
		}
		elseif (Input::has('btnPrevPage'))                  //■■■<<button■■■
		{
			//log
			Log::write('info', 'BA2010 Prev. Page Button Click.',
				[
					"Inspection No."           => Input::get('txtInspectionNo', ''),
					"Inspection No.(Hidden)"   => Input::get('hidInspectionNo', ''),
				]
			);

			//follow value of Hidden exclusive of PageMove
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}

			//set parameter to decrease line number
			$lAdjustmentNo = -1;
		}
		elseif (Input::has('btnNextPage'))                  //■■■>>button■■■
		{
			//log
			Log::write('info', 'BA2010 Next Page Button Click.',
				[
					"Inspection No."           => Input::get('txtInspectionNo', ''),
					"Inspection No.(Hidden)"   => Input::get('hidInspectionNo', ''),
				]
			);

			//follow value of Hidden exclusive of PageMove
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}

			//set parameter for line number
			$lAdjustmentNo = 1;
		}
		elseif (Input::has('btnReturn'))                    //■■■Return button■■■
		{
			//log
			Log::write('info', 'BA2010 Return Button Click.');

			//■■16-02-2017 Motooka added for Kohbyo■■
			//transport to list screen
			//return Redirect::route("user/list");
			//in case screen is transported from Modify or Entry finished
			if (Session::has('BA1010InspectionResultNo'))
			{
				//in case screen is transported from Modify
				//transport to list screen
				return Redirect::route("user/list");
			}
			else
			{
				//in case screen is transported from Pre Entry
				//transport to preentry screen
				return Redirect::route("user/preentry");
			}
		}
		elseif (Input::has('btnEntry'))                     //■■■Entry button■■■
		{
			//log
			Log::write('info', 'BA2010 Entry Button Click.',
				[
					"Inspection Result"         => Input::get('txtInspectionResult'              ,''),
					"Result Class"              => Input::get('rdiResultClass'                   ,''),
					"OFFSET/NG Bunrui"          => Input::get('cmbOffsetNGBunrui'                ,0),
					"OFFSET/NG Reason"          => Input::get('cmbOffsetNGReason'                ,''),
					"OFFSET/NG Action"          => Input::get('rdiOffsetNGAction'                ,''),
					"OFFSET/NG Action Detail1"  => Input::get('chkOffsetNGActionDetailInternal'  ,'0'),
					"OFFSET/NG Action Detail2"  => Input::get('chkOffsetNGActionDetailRequest'   ,'0'),
					"OFFSET/NG Action Detail3"  => Input::get('chkOffsetNGActionDetailTSR'       ,'0'),
					"OFFSET/NG Action Detail4"  => Input::get('chkOffsetNGActionDetailSorting'   ,'0'),
					"OFFSET/NG Action Detail5"  => Input::get('chkOffsetNGActionDetailNoAction'  ,'0'),
					"Action Details"            => Input::get('cmbActionDetails'                 ,''),
					"TecReqNo"                  => Input::get('txtTecReqNo     '                 ,''),
					"TecReqPerson"              => Input::get('cmbTecReqPerson'                ,''),
					"TSRNo"                     => Input::get('txtTSRNo'                         ,''),
					"Equipment"                 => Input::get('cmbInspectonToolClass'            ,''),
					"Inspection No."            => Input::get('txtInspectionNo'                  ,''),
					"Inspection No.(Hidden)"    => Input::get('hidInspectionNo'                  ,''),
				]
			);

			//follow value of Hidden exclusive of PageMove
			//PageMove以外の場合（<<、>>、Entry）は、画面入力値ではなく、Hiddenで保持しているInspection Noに対する処理をする必要があるため、Hidden値に従う
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}
			
			//flag to find data
			$lDataFindFlg = false;
			
			//Fetch Inspection No data and get specific master data
			foreach ($lTblInspectionNo As $lRowInspectionNo)
			{
				//change corresponding line to Array
				$lCurrentlRowInspectionNo = (Array)$lRowInspectionNo;

				if ($lDataFindFlg == false)
				{
					//if Work Inspection No.= present Inspection No. 
					//退避した検査番号と読み込んだ検査番号が一致する場合
					if($lInspectionNo == $lCurrentlRowInspectionNo["INSPECTION_NO"])
					{
						//workに退避
						$lAnalyticalGroup          = $lCurrentlRowInspectionNo["ANALYTICAL_GRP"];
						$lAnalyticalGroupPoint     = $lCurrentlRowInspectionNo["ANALYTICAL_GRP_POINT"];
						$lAnalyticalGroupHowmany   = $lCurrentlRowInspectionNo["ANALYTICAL_GRP_HOWMANY"];
						$lPictureURL               = $lCurrentlRowInspectionNo["PICTURE_URL"];
						$lStdSpecUnder             = number_format($lCurrentlRowInspectionNo["THRESHOLD_NG_UNDER"], 4);
						$lStdSpecOver              = number_format($lCurrentlRowInspectionNo["THRESHOLD_NG_OVER"], 4);
						$lStdSpecCenter            = number_format($lCurrentlRowInspectionNo["REFERENCE_VALUE"], 4);
						
						$lDataFindFlg = true;
						break;
					}
				}
			}
			
			//in case the number is entried
			//数値入力パターンの場合
			if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_NUMBER)
			{
				//if ResultClass is not selected or OK、OFFSET、NG（not No Check nor Line Stop）
				//ResultClassが未選択、OK、OFFSET、NGの場合（＝No Checkでなく、Line Stopでもない）
				if ((Input::get('rdiResultClass','') != self::RESULT_CLASS_NO_CHECK) and (Input::get('rdiResultClass','') != self::RESULT_CLASS_LINE_STOP))
				{
					//must check Inspection Result
					$lValidator = Validator::make(
						array('txtInspectionResult' => Input::get('txtInspectionResult')),
						array('txtInspectionResult' => array('required'))
					);

					//if error
					if ($lValidator->fails())
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E021 : Enter Inspection Result."
						]);
					}

					//if no error
					if (array_key_exists("errors", $lViewData) == false)
					{
						//check type of Inspection Result
						$lValidator = Validator::make(
							array('txtInspectionResult' => Input::get('txtInspectionResult')),
							array('txtInspectionResult' => array('numeric'))
						);

						//if error
						if ($lValidator->fails())
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E022 : Inspection Result is invalid."
							]);
						}
					}
				}
			}
			else
			//if OK/NG raddio button is entried
			//OK/NGラジオボタン入力パターンの場合
			{
				//if ResultClass is not selected or OK、OFFSET、NG（not No Check nor Line Stop）
				//ResultClassが未選択、OK、OFFSET、NGの場合（＝No Checkでなく、Line Stopでもない）
				if ((Input::get('rdiResultClass','') != self::RESULT_CLASS_NO_CHECK) and (Input::get('rdiResultClass','') != self::RESULT_CLASS_LINE_STOP))
				{
					//must check selection of raddio button in Inspection Result
					$lValidator = Validator::make(
						array('rdiInspectionResult' => Input::get('rdiInspectionResult')),
						array('rdiInspectionResult' => array('required'))
					);

					//if error
					if ($lValidator->fails())
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E021 : Enter Inspection Result."
						]);
					}
				}
			}

			//if no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//数値入力の場合
				if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_NUMBER)
				{
					//初期値を入れる

					//Insert TRESANAL table
					//Calculated Number (何個の値で計算したか)
					$lCalcNam =  1;
					
					//Max Value (最大値)
					$lMaxValue = Input::get('txtInspectionResult');
					
					//Min Value (最小値)
					$lMinValue = Input::get('txtInspectionResult');
					
					//Average Value (平均値)
					$lAverageValue = number_format(Input::get('txtInspectionResult'),4);
					
					//Range Value (幅)
					$lRangeValue = 0;
					
					//Stdev (ｼｸﾞﾏσ)
					$lStdev = 0;
					
					//CPK (CPK)
					$lCpk = 1;
					
					//Xbar UCL (Xbarの上方管理限界線)
					$lXbarUcl = Input::get('txtInspectionResult');
					
					//Xbar LCL (Xbarの下方管理限界線)
					$lXbarLcl = Input::get('txtInspectionResult');
					
					//RChart UCL (RChartの上方管理限界線)
					$lRchartUcl = 0;
					
					//RChart  LCL (RChartの下方管理限界線)
					$lRchartLcl = 0;
					
					//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
					//画面入力値の為、下で画面の値を入れる

					//テーブル内容を取得する
					if ($lInspectionResultNo != 0)
					{
						//明細table select (計算用)
						//store (count, value) of TRESDETT Table
						$lTblBeforeCommitInspectionResultDetail1 = $this->getBeforeCommitInspectionResultDetailData1($lInspectionResultNo, $lInspectionNo);
						$lRowBeforeCommitInspectionResultDetail1 = (Array)$lTblBeforeCommitInspectionResultDetail1[0];

						//store (analytical Group How many) of TRESDETT Table
						$lTblBeforeCommitInspectionResultDetail2 = $this->getBeforeCommitInspectionResultDetailData2($lInspectionResultNo, $lAnalyticalGroup);
						$lRowBeforeCommitInspectionResultDetail2 = (Array)$lTblBeforeCommitInspectionResultDetail2[0];

						//明細テーブルにデータが存在する
						if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
						{
							//新規登録
							$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"];
						}
						else
						{
							//Modifyのとき
							$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] + 1;
						}

						//store (Calculated Number, Max Value, Min Value, Average Value, Stdev) of TRESANAL Table
						$lTblBeforeCommitInspectionResultAnalyze = $this->getBeforeCommitInspectionResultAnalyzeData($lInspectionResultNo, $lAnalyticalGroup);
						$lRowBeforeCommitInspectionResultAnalyze = (Array)$lTblBeforeCommitInspectionResultAnalyze[0];

						//分析用テーブルにデータが存在する
						if ($lRowBeforeCommitInspectionResultAnalyze["COUNT"] != 0)
						{
						}

						//store value of 係数 Table (Xbar)
						//store value of TCCIENTMST Table (Xbar)
						$lTblCoefficientXbar = $this->getCoefficientData(self::COEFFICIENT_XBAR, $lCodeNum);
						if($lTblCoefficientXbar != null)
						{
							$lRowCoefficientXbar = (Array)$lTblCoefficientXbar[0];
						}
						else
						{
							$lRowCoefficientXbar["COEFFICIENT"] = 0;
						}

						//store value of TCCIENTMST Table (RChart UCL)
						$lTblCoefficientRChartUCL = $this->getCoefficientData(self::COEFFICIENT_RCHAT_UCL, $lCodeNum);
						if($lTblCoefficientRChartUCL != null)
						{
							$lRowCoefficientRChartUCL = (Array)$lTblCoefficientRChartUCL[0];
						}
						else
						{
							$lRowCoefficientRChartUCL["COEFFICIENT"] = 0;
						}

						//store value of TCCIENTMST Table (RChart LCL)
						$lTblCoefficientRChartLCL = $this->getCoefficientData(self::COEFFICIENT_RCHAT_LCL, $lCodeNum);
						if($lTblCoefficientRChartLCL != null)
						{
							$lRowCoefficientRChartLCL = (Array)$lTblCoefficientRChartLCL[0];
						}
						else
						{
							$lRowCoefficientRChartLCL["COEFFICIENT"] = 0;
						}
					}
					else
					{
						//何もしない
					}

					//calculate (Update TRESANAL table)
					//分析用テーブルに入れる値を計算する
					if($lTblBeforeCommitInspectionResultAnalyze != null)
					{
						if ($lRowBeforeCommitInspectionResultAnalyze["COUNT"] != 0)
						{
							if ($lRowBeforeCommitInspectionResultAnalyze["STDEV"] == 0 && $lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
							{
								//複数検査番号は登録されてなく、再び自検査番号レコードを更新しようとする場合は、何もしない
							}
							else
							{
								Log::write('info', 'BA2010 Setteing TRESANAL table item');
								
								//Calculated Number (何個の値で計算したか)
								if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
								{
									$lCalcNam =  $lRowBeforeCommitInspectionResultAnalyze["CALC_NUM"];
								}
								else
								{
									$lCalcNam = $lRowBeforeCommitInspectionResultAnalyze["CALC_NUM"] + 1;
								}
								
								//Max Value・Min Value (最大値・最小値)
								$lTblBeforeCommitInspectionResultDetailValueData = $this->getBeforeCommitInspectionResultDetailValueData($lInspectionResultNo, $lAnalyticalGroup);
								
								$lWorkMaxValue = Input::get('txtInspectionResult');
								$lWorkMinValue = Input::get('txtInspectionResult');
								
								if ($lTblBeforeCommitInspectionResultDetailValueData != null)
								{
									foreach ($lTblBeforeCommitInspectionResultDetailValueData as $lRowBeforeCommitInspectionResultDetailValueData)
									{
										$lRowBeforeCommitInspectionResultDetailValueData = (Array)$lRowBeforeCommitInspectionResultDetailValueData;
										
										if ($lRowBeforeCommitInspectionResultDetailValueData["INSPECTION_RESULT_VALUE"] != $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
										{
											if ($lWorkMaxValue < $lRowBeforeCommitInspectionResultDetailValueData["INSPECTION_RESULT_VALUE"])
											{
												$lWorkMaxValue = $lRowBeforeCommitInspectionResultDetailValueData["INSPECTION_RESULT_VALUE"];
											}
											
											if ($lWorkMinValue > $lRowBeforeCommitInspectionResultDetailValueData["INSPECTION_RESULT_VALUE"])
											{
												$lWorkMinValue = $lRowBeforeCommitInspectionResultDetailValueData["INSPECTION_RESULT_VALUE"];
											}
										}
									}
									
									$lMaxValue = $lWorkMaxValue;
									$lMinValue = $lWorkMinValue;
								}
								else
								{
									$lMaxValue = Input::get('txtInspectionResult');
									$lMinValue = Input::get('txtInspectionResult');
								}
								
								//Average Value (平均値)
								if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
								{
									$lAverageValue =  number_format((Input::get('txtInspectionResult') - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"], 4);
								}
								else
								{
									$lAverageValue = number_format((Input::get('txtInspectionResult') - $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"], 4);
								}
								
								//Range Value (幅)
								$lRangeValue = $lMaxValue - $lMinValue;
								
								//Stdev (ｼｸﾞﾏσ)
								//前回の平均値と今回の平均値が同じ場合→同値を入力した場合は、計算せず、前回のσをそのまま入れる
								if (($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"]) == 0)
								{
									$lStdev = $lRowBeforeCommitInspectionResultAnalyze["STDEV"];
								}
								else
								{
									//if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
									//{
									//    //既存測定値の修正の場合
									//	$lStdev = number_format(sqrt(
									//	                             (
									//	                              pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2)
									//	                              +
									//	                              pow($lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"] - $lAverageValue, 2)
									//	                              +
									//	                              (
									//	                               (
									//	                                ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
									//	                                *
									//	                                (
									//	                                 ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
									//	                                 -
									//	                                 (2 * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
									//	                                 +
									//	                                 (2 * $lAverageValue)
									//	                                )
									//	                               )
									//	                               / $lCalcNam
									//	                              )
									//	                             )
									//	                            )
									//	                        ,4);
									//}
									//else
									//{
									//    //新しい測定値の追加の場合
									//	$lStdev = number_format(sqrt(
									//	                              ((pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2) + pow($lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"] - $lAverageValue, 2)) * ($lCalcNam - 1) / $lCalcNam)
									//	                              + (pow($lAverageValue - Input::get('txtInspectionResult'), 2) / $lCalcNam)
									//	                             )
									//	                       ,4);
									//}
									
									
									//■■上はSTDEV.P(n法)の場合　下はSTDEV(n-1法)の場合　工場は通常STDEVを使用する■■
									
									if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
									{
										//既存測定値の修正の場合
										if ($lCalcNam >= 3)
										{
											$lStdev = number_format(sqrt(
											                              pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2)
											                              +
											                              (
											                               (($lCalcNam * pow($lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"] - $lAverageValue, 2))
											                                +
											                                pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'), 2)
											                                -
											                                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
											                                +
											                                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lAverageValue)
											                               ) / ($lCalcNam - 1)
											                              )
											                             )
											                       ,4);
										}
										elseif ($lCalcNam = 2)
										{
											$lStdev = number_format(sqrt(
											                              pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2)
											                              +
											                              (2 * pow($lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"] - $lAverageValue, 2))
											                              +
											                              pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'), 2)
											                              -
											                              (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
											                              +
											                              (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lAverageValue)
											                             )
											                       ,4);
										}
										else
										{
											$lStdev = 0;
										}
									}
									else
									{
										//新しい測定値の追加の場合
										if ($lCalcNam >= 3)
										{
											$lStdev = number_format(sqrt(
											                               ( ($lCalcNam - 2) * pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2)
											                                 + (($lCalcNam - 1) * pow($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"], 2))
											                                 + pow($lAverageValue - Input::get('txtInspectionResult'), 2)
											                               ) / ($lCalcNam - 1)
											                             )
											                       ,4);
										}
										elseif ($lCalcNam = 2)
										{
											$lStdev = number_format(sqrt(
											                              pow($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"], 2)
											                               + pow($lAverageValue - Input::get('txtInspectionResult') ,2)
											                             )
											                       ,4);
										}
										else
										{
											$lStdev = 0;
										}
									}

									Log::write('info', 'BA2010 Cal',
										[
											"Stdev"            => $lRowBeforeCommitInspectionResultAnalyze["STDEV"],
											"STDEV2"           => pow($lRowBeforeCommitInspectionResultAnalyze["STDEV"], 2),
											"n"                => $lCalcNam,
											"S"                => (2 * pow($lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"] - $lAverageValue, 2)),
											"d"                => (Input::get('txtInspectionResult') - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]),
											"Result"           => Input::get('txtInspectionResult'              ,''),
											"AverageValueold"  =>$lRowBeforeCommitInspectionResultAnalyze["AVERAGE_VALUE"],
											"AverageValuenow"  => $lAverageValue,
										]
									);
								}
								
								//CPK (CPK)
								if ($lStdev != 0)
								{
									$lWorkCpkStdSpecOver = number_format((($lStdSpecOver- $lAverageValue) / (3 * $lStdev)), 4);
									$lWorkCpkStdSpecUnder = number_format((($lAverageValue - $lStdSpecUnder) / (3 * $lStdev)), 4);
								}
								else
								{
									$lWorkCpkStdSpecOver = 0;
									$lWorkCpkStdSpecUnder = 0;
								}
								
								if ($lWorkCpkStdSpecOver < $lWorkCpkStdSpecUnder)
								{
									$lCpk = $lWorkCpkStdSpecOver;
								}
								else
								{
									$lCpk = $lWorkCpkStdSpecUnder;
								}
							
								//Xbar UCL (Xbarの上方管理限界線)
								$lXbarUcl = $lAverageValue + $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue;
								
								//Xbar LCL (Xbarの下方管理限界線)
								$lXbarLcl = $lAverageValue - $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue;
								
								//RChart UCL (RChartの上方管理限界線)
								$lRchartUcl = $lRowCoefficientRChartUCL["COEFFICIENT"] * $lRangeValue;
								
								//RChart  LCL (RChartの下方管理限界線)
								$lRchartLcl = $lRowCoefficientRChartLCL["COEFFICIENT"] * $lRangeValue;
								
								//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
								//画面入力値の為、下で画面の値を入れる
							}
						}
					}
				}
				//ビジュアルチェックの場合
				else
				{
					//初期値を入れる

					//Insert TRESANAL table
					//Calculated Number (何個の値で計算したか)
					$lCalcNam =  1;
					
					//Max Value (最大値)
					$lMaxValue = 0;
					
					//Min Value (最小値)
					$lMinValue = 0;
					
					//Average Value (平均値)
					$lAverageValue = 0;
					
					//Range Value (幅)
					$lRangeValue = 0;
					
					//Stdev (ｼｸﾞﾏσ)
					$lStdev = 0;
					
					//CPK (CPK)
					$lCpk = 1;
					
					//Xbar UCL (Xbarの上方管理限界線)
					$lXbarUcl = 0;
					
					//Xbar LCL (Xbarの下方管理限界線)
					$lXbarLcl = 0;
					
					//RChart UCL (RChartの上方管理限界線)
					$lRchartUcl = 0;
					
					//RChart  LCL (RChartの下方管理限界線)
					$lRchartLcl = 0;
					
					//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
					//画面入力値の為、下で画面の値を入れる
				}	
					
				
				//Judgeに使う値を求める
				//Get Value for Judge
				//初回、又は、全てのデータが同じ値
				//in case the first inspection or all inspection value is same
				if ($lStdev == 0)
				{
					//規格センター値を入れエラーとならないようにする
					//value for judge is center value
					$lJudgeMin = 0;
					$lJudgeMax = 0;
				}
				else
				{
					$lJudgeMin = number_format($lAverageValue - $lStdev * 3, 4);
					$lJudgeMax = number_format($lAverageValue + $lStdev * 3, 4);
				}
			}

			//in case Result Class is OFFSET/NG
			//Result ClassがOFFSET/NGの場合
			if ((Input::get('rdiResultClass','') == self::RESULT_CLASS_OFFSET) or (Input::get('rdiResultClass','') == self::RESULT_CLASS_NG))
			{
				//must check OFFSET/NG Reason
				//if no error,check
				if (array_key_exists("errors", $lViewData) == false)
				{
					$lValidator = Validator::make(
						array('cmbOffsetNGReason' => Input::get('cmbOffsetNGReason')),
						array('cmbOffsetNGReason' => array('required'))
					);

					//if error
					if ($lValidator->fails())
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E023 : Enter OFFSET Reason / NG Reason."
						]);
					}
				}

				//must check OFFSET/NG Action
				//if no error,check
				if (array_key_exists("errors", $lViewData) == false)
				{
					$lValidator = Validator::make(
						array('rdiOffsetNGAction' => Input::get('rdiOffsetNGAction')),
						array('rdiOffsetNGAction' => array('required'))
					);

					//if error
					if ($lValidator->fails())
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E024 : Select OFFSET Action / NG Action."
						]);
					}
				}
			}
			
			//if no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				if ($lJudgeMin == 0 && $lJudgeMax == 0)
				{
					//Nothing
				}
				else
				{
				
					Log::write('info', 'BA2010 Judge',[
									"JudgeMin"         => $lJudgeMin,
									"JudgeMax"         => $lJudgeMax,
									"StdSpecUnder"     => $lStdSpecUnder,
									"StdSpecOver"      => $lStdSpecOver,
								]);
					
					if(Input::has('rdiResultClass') and Input::get('rdiResultClass') == 1){
						// Status = OK
						// Not thing to do
					}else{
						// Status = NG
						if(($lStdSpecUnder > $lJudgeMin) or ($lStdSpecOver < $lJudgeMax))
						{
							if (array_key_exists("errors", $lViewData) == false)
							{
								$lValidator = Validator::make(
									array('cmbJudgeReason' => Input::get('cmbJudgeReason')),
									array('cmbJudgeReason' => array('required'))
								);

								//if error
								if ($lValidator->fails())
								{
									//error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E052 : Enter Reason."
									]);
								}
							}

							//Validatorがきかない場合
							if(Input::get('cmbJudgeReason') == 0)
							{
									//error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E052 : Enter Reason."
									]);
							}
							
							
							//set text＋DataRevision
							$lViewData += [
								"Xbar"           => number_format($lAverageValue, 4),
								"Max"            => number_format($lMaxValue, 4),
								"Min"            => number_format($lMinValue, 4),
								"Range"          => number_format($lRangeValue, 4),
								"Stdev"          => number_format($lStdev, 4),
								"JudgeMin"       => number_format($lJudgeMin, 4),
								"JudgeMax"       => number_format($lJudgeMax, 4),
								"Judge"          => "",
								"JudgeReason"    => "",
							];
						}	
					}
				}
			}
			
			//if no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				if ($lJudgeMin == 0 && $lJudgeMax == 0)
				{
					$lJudgeReason = "";
				}
				else
				{
					if (($lStdSpecUnder > $lJudgeMin) or ($lStdSpecUnder > $lJudgeMax) or ($lStdSpecOver < $lJudgeMin) or ($lStdSpecOver < $lJudgeMax))
					{
						//Judge Reason is value of Input
						$lJudgeReason = Input::get('cmbJudgeReason');
					}
					else
					{
						//clear Judge Reason
						$lJudgeReason = "";
					}
				}
			}

			//if no error
			//ここまででエラーが無い場合は以下の処理を実施
			if (array_key_exists("errors", $lViewData) == false)
			{
				Log::write('info', 'BA2010 Entry Button Click2',
					[
						"InspectionResultNo"       => $lInspectionResultNo,
						"lInspectionNo"             => $lInspectionNo,
					]
				);
			
				//Entry flag ON
				$lEntryFlg = true;
				
				//register/update
				$lInspectionResultNo = $this->insertAndUpdateInspectionResult($lInspectionResultNo
			                                                                 ,$lViewData["CheckSheetNo"]
			                                                                 ,$lViewData["RevisionNo"]
			                                                                 ,$lViewData["MachineNo"]
			                                                                 ,$lViewData["InspectorCode"]
			                                                                 ,$lViewData["InspectionDate"]
			                                                                 ,$lViewData["Condition"]
			                                                                 ,$lViewData["InspectionTime"]
			                                                                 ,Session::get('BA1010ModifyDataRev',0)
			                                                                 ,$lInspectionNo
			                                                                 ,Input::get('txtInspectionResult')
			                                                                 ,Input::get('rdiInspectionResult'            ,'')
			                                                                 ,Input::get('rdiResultClass'                 ,'')
			                                                                 ,Input::get('cmbOffsetNGReason')
			                                                                 ,Input::get('rdiOffsetNGAction'              ,'')
			                                                                 ,Input::get('chkOffsetNGActionDetailInternal','0')
			                                                                 ,Input::get('chkOffsetNGActionDetailRequest' ,'0')
			                                                                 ,Input::get('chkOffsetNGActionDetailTSR'     ,'0')
			                                                                 ,Input::get('chkOffsetNGActionDetailSorting' ,'0')
			                                                                 ,Input::get('chkOffsetNGActionDetailNoAction','0')
			                                                                 ,Input::get('cmbActionDetails')
			                                                                 ,Input::get('txtTecReqNo')
			                                                                 ,Input::get('txtTSRNo')
			                                                                 ,Input::get('cmbTecReqPerson')
			                                                                 
			                                                                 ,$lViewData["MasterNo"]
			                                                                 ,$lViewData["Process"]
			                                                                 ,$lViewData["MaterialLotNoForEntry"]
			                                                                 ,$lViewData["CertificateNoForEntry"]
			                                                                 ,$lViewData["PONoForEntry"]
			                                                                 ,$lViewData["InvoiceNoForEntry"]
			                                                                 ,$lViewData["QuantityCoilForEntry"]
			                                                                 ,$lViewData["QuantityWeightForEntry"]
			                                                                 ,$lViewData["LotNoForEntry"]
			                                                                 
			                                                                 ,$lAnalyticalGroup
			                                                                 ,$lAnalyticalGroupPoint
			                                                                 ,$lAnalyticalGroupHowmany
			                                                                 
			                                                                 ,$lStdSpecUnder
			                                                                 ,$lStdSpecOver
			                                                                 ,$lPictureURL
			                                                                 ,$lCalcNam
			                                                                 ,$lMaxValue
			                                                                 ,$lMinValue
			                                                                 ,$lAverageValue
			                                                                 ,$lRangeValue
			                                                                 ,$lStdev
			                                                                 ,$lCpk
			                                                                 ,$lXbarUcl
			                                                                 ,$lXbarLcl
			                                                                 ,$lRchartUcl
			                                                                 ,$lRchartLcl
			                                                                 
			                                                                 ,$lJudgeReason
			                                                                 
			                                                                 ,Input::get('hidInspectionResultInputType')
			                                                                 );

				//1件登録した後は更新モードと同じ挙動に入るため、セッションで変数保持
				//store parameters in session
				Session::put('BA1010InspectionResultNo', $lInspectionResultNo);
				//同じく、「Modifyで画面遷移した場合と同じ状況」を作るため、Modify時に一覧から渡される内容をここで保持
				//store list from Modify
				Session::put('BA1010CheckSheetNoForModify', $lViewData["CheckSheetNo"]);
				Session::put('BA1010MachineNoForModify', $lViewData["MachineNo"]);
				Session::put('BA1010MachineNoForModifyName', $lViewData["MachineNoName"]);
				Session::put('BA1010InspectorCodeForModify', $lViewData["InspectorCode"]);
				Session::put('BA1010InspectionDateForModify', $lViewData["InspectionDate"]);
				Session::put('BA1010ConditionForModify', $lViewData["Condition"]);
				Session::put('BA1010ConditionForModifyName', $lViewData["ConditionName"]);
				Session::put('BA1010InspectionTimeForModify', $lViewData["InspectionTime"]);
				Session::put('BA1010InspectionTimeForModifyName', $lViewData["InspectionTimeName"]);
				Session::put('BA1010MasterNo', $lViewData["MasterNo"]);
				Session::put('BA1010ProcessForModify', $lViewData["Process"]);
				Session::put('BA1010ProcessForModifyName', $lViewData["ProcessName"]);
				Session::put('BA1010MaterialNameForModify', $lViewData["MaterialName"]);
				Session::put('BA1010MaterialSizeForModify', $lViewData["MaterialSize"]);
				Session::put('BA2020PONoForEntry', $lViewData["PONoForEntry"]);
				Session::put('BA2020CertificateNoForEntry', $lViewData["CertificateNoForEntry"]);
				Session::put('BA2020InvoiceNoForEntry', $lViewData["InvoiceNoForEntry"]);
				Session::put('BA1010HeatTreatmentType', $lViewData["HeatTreatmentType"]);
				Session::put('BA1010PlatingKind', $lViewData["PlatingKind"]);
				Session::put('BA2020MaterialLotNoForEntry', $lViewData["MaterialLotNoForEntry"]);

				//add Revision No. in header
				Session::put('BA1010ModifyDataRev', Session::get('BA1010ModifyDataRev',0)+1);

				//set parameter for line number
				$lAdjustmentNo = 1;
			}
		}
		elseif (Input::has('btnFilter'))                     //■■■Filter button■■■
		{
			//log
			Log::write('info', 'BA2010 Filter Button Click.',
				[
					"Equipment"                => Input::get('cmbInspectonToolClass' ,''),
					"Inspection No."           => Input::get('txtInspectionNo'       ,''),
					"Inspection No.(Hidden)"   => Input::get('hidInspectionNo'       ,''),
				]
			);
		}
		//■■15-08-2016 Motooka added for SUNLIT■■
		elseif (Input::has('btnExcelLoad'))                 //■■■ExcelLoad button■■■
		{
			//store Equipment Code
			$EquipmentCode = Session::get('BA2010EquipmentCodeForEntry');
			
			//read instrument maset corresponding to instrument Code
			$lTblEquipmentFileName = $this->getEquipmentFileName($EquipmentCode);
			
			//get instrument Code corresponding to Inspection No.
			if($lTblEquipmentFileName != null)
			{
				//get information of 0 line in instrument master
				$lRowEquipmentFileName = (Array)$lTblEquipmentFileName[0];
				
				$lEquipmentFileName = $lRowEquipmentFileName["SOKUTEIKI_FILE_NAME"];
				
				//if file does not exist, message
				if($lEquipmentFileName != null)
				{
					//open text file
					$objReader = fopen($lEquipmentFileName,"r");
					
					//get text file
					$objReader = file_get_contents($lEquipmentFileName);
					
					//enter text file in InspectionResult
					$lViewData += [
						"InspectionResult"              => $objReader,
					];
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E038 : Not Exists File."
					]);
				}
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E038 : Not Exists File."
				]);
			}
			//follow value of Hidden exclusive of PageMove
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}
		}
		else                                                //■■■when transpotion from other screen or Load■■■
		{
			//in case value of combo storing in hiddn before Submit is changed
			//flag available
			//store InspectionNo.
			if (Input::get('hidOffsetNGBunrui') != Input::get('cmbOffsetNGBunrui'))
			{
				if (Input::has('hidInspectionNo'))
				{
					$lInspectionNo  = (String)Input::get('hidInspectionNo');
				}

				$lOnchangeFlg = "1";
			}
		}


		//■must pass

		//flag to find data
		$lDataFindFlg = false;

		//Fetch data and get count to specify master data
		foreach ($lTblInspectionNo as $lRowInspectionNo)
		{
			if (strcmp($lInspectionNo,$lRowInspectionNo->INSPECTION_NO) == 0)
			{
				//break FOR because Inspection No match
				$lDataFindFlg = true;
				break;
			}

			//count up line number of data
			$lDataRowNo += 1;
		}

		//in case no data, error
		if ($lDataFindFlg == false)
		{
			//return to data of line 1
			$lDataRowNo = 0;

			//error message
			$lViewData["errors"] = new MessageBag([
				"error" => "E996 : Since Inspection No. does not exist, the first data is shown."
			]);
		}

		//move data line infront and back in case of putting "<<",">>","Entry"
		//in case the last data is push Entry
		//need to push Entry button in condition
		if (($lDataRowNo == count($lTblInspectionNo) -1) and ($lEntryFlg == true))
		{
			$lDataRowNo = 0;
		}
		else
		{
			$lDataRowNo = $lDataRowNo + $lAdjustmentNo;
		}

		//現在の検査番号に該当する$lTblInspectionNoの行番号を特定し、該当行のデータを画面向けの変数配列に格納
		//specify line No. of $lTblInspectionNo corresponding to present inspection No. and store in Array for screen
		$lRowInspectionNo = (Array)$lTblInspectionNo[$lDataRowNo];

		$lViewData += [
			"InspectionNo"              => $lRowInspectionNo["INSPECTION_NO"],
			"hidInspectionNo"           => $lRowInspectionNo["INSPECTION_NO"],
			"InspectionItem"            => $lRowInspectionNo["INSPECTION_ITEM"],
			"ToolCode"                  => $lRowInspectionNo["TOOL_CD"],
			"PictureURL"                => $lRowInspectionNo["PICTURE_URL"],
			"XRChartNoPokayokeNo"       => $lRowInspectionNo["XR_CHART_NO_POKAYOKE_NO"],
			"ReferToDocumentFrequency"  => $lRowInspectionNo["REFER_TO_DOCUMENT_FREQUENCY"],
			"Equipment"                 => $lRowInspectionNo["INSPECTION_TOOL_CLASS_NAME"],
			"EquipmentCode"             => $lRowInspectionNo["INSPECTION_TOOL_CLASS"],
			"InspectionResultInputType" => $lRowInspectionNo["INSPECTION_RESULT_INPUT_TYPE"],
			"NGUnder"                   => number_format($lRowInspectionNo["THRESHOLD_NG_UNDER"], 4),
			"OFFSETUnder"               => number_format($lRowInspectionNo["THRESHOLD_OFFSET_UNDER"], 4),
			"ReferenceValue"            => number_format($lRowInspectionNo["REFERENCE_VALUE"], 4),
			"OFFSETOver"                => number_format($lRowInspectionNo["THRESHOLD_OFFSET_OVER"], 4),
			"NGOver"                    => number_format($lRowInspectionNo["THRESHOLD_NG_OVER"], 4),
			"NGUnderDiff"               => number_format(($lRowInspectionNo["REFERENCE_VALUE"] - $lRowInspectionNo["THRESHOLD_NG_UNDER"]) * -1, 4),
			"OFFSETUnderDiff"           => number_format(($lRowInspectionNo["REFERENCE_VALUE"] - $lRowInspectionNo["THRESHOLD_OFFSET_UNDER"]) * -1, 4),
			"ReferenceValueDiff"        => number_format(($lRowInspectionNo["REFERENCE_VALUE"] - $lRowInspectionNo["REFERENCE_VALUE"]) * -1, 4),
			"OFFSETOverDiff"            => number_format(($lRowInspectionNo["REFERENCE_VALUE"] - $lRowInspectionNo["THRESHOLD_OFFSET_OVER"]) * -1, 4),
			"NGOverDiff"                => number_format(($lRowInspectionNo["REFERENCE_VALUE"] - $lRowInspectionNo["THRESHOLD_NG_OVER"]) * -1, 4),
		];
		
		
		//■■15-08-2016 Motooka added for SUNLIT■■
		//record present Equipment Code
		Session::put('BA2010EquipmentCodeForEntry', $lViewData["EquipmentCode"]);

		//lock << button in case data is in 0 line
		if ($lDataRowNo == 0)
		{
			$lViewData += [
				"prevButtonLock" => "lock",
			];
		}
		else
		{
			$lViewData += [
				"prevButtonLock" => "",
			];
		}

		//lock >> button in case data is last data
		if ($lDataRowNo == count($lTblInspectionNo) -1)
		{
			$lViewData += [
				"nextButtonLock" => "lock",
			];
		}
		else
		{
			$lViewData += [
				"nextButtonLock" => "",
			];
		}



		//既に実績が登録されている場合は明細テーブルの値読み込む（Modifyの実績Noか、2件目以降の登録時の実績Noを用いてRead）
		//read (Read by inspection result No. from Modify or inspection result No. from Entry) in case result has entried already
		if ($lInspectionResultNo != 0)
		{
			$lTblInspectionResultDetail = $this->getInspectionResultDetailData($lInspectionResultNo, $lViewData["InspectionNo"]);
		}

		//検索結果がある場合のみset in screen only in case result of search exists
		if ($lTblInspectionResultDetail != null)
		{
			$lRowInspectionResultDetail = (Array)$lTblInspectionResultDetail[0];

			//set text＋DataRevision
			$lViewData += [
				"InspectionResult"    => $lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"],
				"OffsetNGBunrui"      => $lRowInspectionResultDetail["OFFSET_NG_BUNRUI_ID"],
				"OffsetNGReason"      => $lRowInspectionResultDetail["OFFSET_NG_REASON"],
				"ActionDetails"       => $lRowInspectionResultDetail["ACTION_DETAILS_TR_NO_TSR_NO"],
				"TecReqNo"            => $lRowInspectionResultDetail["TR_NO"],
				"TecReqPerson"        => $lRowInspectionResultDetail["TECHNICIAN_USER_ID"],
				"TSRNo"               => $lRowInspectionResultDetail["TSR_NO"],
				"Status"              => "",
				"hidDataRev"          => $lRowInspectionResultDetail["DATA_REV"],
			];

			//if Inspection Result is OK/NG
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"] == "1")
			{
				$lViewData += [
					"InspectionResultRadio1"       => true,
					"InspectionResultRadio2"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"] == "2")
			{
				$lViewData += [
					"InspectionResultRadio1"       => "",
					"InspectionResultRadio2"       => true,
				];
			}
			else
			{
				$lViewData += [
					"InspectionResultRadio1"       => "",
					"InspectionResultRadio2"       => "",
				];
			}

			//change position to set as value of Result Class
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK)
			{
				$lViewData += [
					"ResultClass1"       => true,
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => true,
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => true,
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NO_CHECK)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => true,
					"ResultClass5"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_LINE_STOP)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => true,
				];
			}
			else
			{
				//must check Result Class
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}

			//change position to set as value of OFFSET Action / NG Action
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION"] == self::OFFSET_NG_ACTION_RUNNING)
			{
				$lViewData += [
					"OffsetNGAction1"       => true,
					"OffsetNGAction2"       => "",
				];
			}
			elseif ($lRowInspectionResultDetail["OFFSET_NG_ACTION"] == self::OFFSET_NG_ACTION_STOP_LINE)
			{
				$lViewData += [
					"OffsetNGAction1"       => "",
					"OffsetNGAction2"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGAction1"       => "",
					"OffsetNGAction2"       => "",
				];
			}

			//set OFFSET Action Detail / NG Action Detail（5）
			//Internal Approve
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_INTERNAL"] == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailInternal"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailInternal"       => "",
				];
			}

			//Request Offset
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_REQUEST"] == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailRequest"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailRequest"       => "",
				];
			}

			//TSR
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_TSR"] == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailTSR"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailTSR"       => "",
				];
			}

			//Sorting
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_SORTING"] == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailSorting"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailSorting"       => "",
				];
			}

			//No Action
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_NOACTION"] == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailNoAction"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailNoAction"       => "",
				];
			}
		}
		else
		{
			//clear in case result of search does not exist
			$lViewData += [
				"InspectionResult"              => "",
				"OffsetNGBunrui"                => 0,
				"OffsetNGReason"                => "",
				"TecReqPerson"                  => "",
				"ActionDetails"                 => "",
				"TecReqNo"                      => "",
				"TSRNo"                         => "",
				"Status"                        => "",
				"hidDataRev"                    => 0,
				"InspectionResultRadio1"        => "",
				"InspectionResultRadio2"        => "",
				"ResultClass1"                  => "",
				"ResultClass2"                  => "",
				"ResultClass3"                  => "",
				"ResultClass4"                  => "",
				"ResultClass5"                  => "",
				"OffsetNGAction1"               => "",
				"OffsetNGAction2"               => "",
				"OffsetNGActionDetailInternal"  => "",
				"OffsetNGActionDetailRequest"   => "",
				"OffsetNGActionDetailTSR"       => "",
				"OffsetNGActionDetailSorting"   => "",
				"OffsetNGActionDetailNoAction"  => "",
			];
		}
		
		
		$lAnalyticalGroup          = $lRowInspectionNo["ANALYTICAL_GRP"];
		$lAnalyticalGroupPoint     = $lRowInspectionNo["ANALYTICAL_GRP_POINT"];
		$lAnalyticalGroupHowmany   = $lRowInspectionNo["ANALYTICAL_GRP_HOWMANY"];
		
		
		//既に実績が登録されている場合は分析テーブルの値を読み込む（Modifyの実績Noか、2件目以降の登録時の実績Noを用いてRead）
		//read TRESANAL (Read by inspection result No. from Modify or inspection result No. from Entry) in case result has entried already
		if ($lInspectionResultNo != 0)
		{
			$lTblInspectionResultAnalyze = $this->getInspectionResultAnalyzeData($lViewData["MasterNo"]
			                                                                     ,$lViewData["Process"]
			                                                                     ,$lViewData["CheckSheetNo"]
			                                                                     ,$lViewData["RevisionNo"]
			                                                                     ,$lViewData["InspectionDate"]
			                                                                     ,$lViewData["InspectionTime"]
			                                                                     ,$lViewData["Condition"]
			                                                                     ,$lAnalyticalGroup
			                                                                     ,$lInspectionResultNo);
		}

		//検索結果がある場合のみset in screen only in case result of search exists
		if ($lTblInspectionResultAnalyze != null)
		{
			$lRowInspectionResultAnalyze = (Array)$lTblInspectionResultAnalyze[0];

			//set text＋DataRevision
			$lViewData += [
				"Xbar"           => $lRowInspectionResultAnalyze["AVERAGE_VALUE"],
				"Max"            => $lRowInspectionResultAnalyze["MAX_VALUE"],
				"Min"            => $lRowInspectionResultAnalyze["MIN_VALUE"],
				"Range"          => $lRowInspectionResultAnalyze["RANGE_VALUE"],
				"Stdev"          => $lRowInspectionResultAnalyze["STDEV"],
				"Judge"          => "",
				"JudgeReason"    => $lRowInspectionResultAnalyze["JUDGE_REASON"],
			];
			
			//Judgeに使う値を求める
			//Get Value for Judge
			//初回、又は、全てのデータが同じ値
			//in case the first inspection or all inspection value is same
			if ($lViewData["Stdev"] == 0)
			{
				//規格センター値を入れNo Goodとならないようにする
				//value for judge is center value
				$lViewData += [
					"JudgeMin"       => 0,
					"JudgeMax"       => 0,
				];
			}
			else
			{
				$lViewData += [
					"JudgeMin"       => number_format($lRowInspectionResultAnalyze["AVERAGE_VALUE"] - $lRowInspectionResultAnalyze["STDEV"] * 3,4),
					"JudgeMax"       => number_format($lRowInspectionResultAnalyze["AVERAGE_VALUE"] + $lRowInspectionResultAnalyze["STDEV"] * 3,4),
				];
			}
		}
		else
		{
			//clear in case result of search does not exist
			$lViewData += [
				"Xbar"           => 0,
				"Max"            => 0,
				"Min"            => 0,
				"Range"          => 0,
				"Stdev"          => 0,
				"JudgeMin"       => 0,
				"JudgeMax"       => 0,
				"Judge"          => "",
				"JudgeReason"    => "",
			];
		}

		//clear error color
		$lViewData += [
			"StatusColorClass"  => "SearchResultTable",
			"JudgeColorClass"   => "SearchResultTable",
		];

		//when entry error,store again value of screen
		//do not clear in case of error of no entry Inspection No.
		//add condition whether data is found
		//else event to change combo
		if (((array_key_exists("errors", $lViewData) == true) and ($lDataFindFlg == True))
		    OR ($lOnchangeFlg == "1")
		   )
		{
			//in spite data exists or not,clear
			unset($lViewData["InspectionResult"]);
			unset($lViewData["OffsetNGReason"]);
			unset($lViewData["OffsetNGBunrui"]);
			unset($lViewData["ActionDetails"]);
			unset($lViewData["TecReqNo"]);
			unset($lViewData["TecReqPerson"]);
			unset($lViewData["TSRNo"]);
			unset($lViewData["Status"]);
			unset($lViewData["InspectionResultRadio1"]);
			unset($lViewData["InspectionResultRadio2"]);
			unset($lViewData["ResultClass1"]);
			unset($lViewData["ResultClass2"]);
			unset($lViewData["ResultClass3"]);
			unset($lViewData["ResultClass4"]);
			unset($lViewData["ResultClass5"]);
			unset($lViewData["OffsetNGAction1"]);
			unset($lViewData["OffsetNGAction2"]);
			unset($lViewData["OffsetNGActionDetailInternal"]);
			unset($lViewData["OffsetNGActionDetailRequest"]);
			unset($lViewData["OffsetNGActionDetailTSR"]);
			unset($lViewData["OffsetNGActionDetailSorting"]);
			unset($lViewData["OffsetNGActionDetailNoAction"]);
			unset($lViewData["JudgeReason"]);
			unset($lViewData["Judge"]);

			$lViewData += [
				"InspectionResult"              => Input::get('txtInspectionResult'),
				"OffsetNGReason"                => Input::get('cmbOffsetNGReason'),
				"ActionDetails"                 => Input::get('cmbActionDetails'),
				"OffsetNGBunrui"                => Input::get('cmbOffsetNGBunrui'),
				"TecReqNo"                      => Input::get('txtTecReqNo'),
				"TSRNo"                         => Input::get('txtTSRNo'),
				"TecReqPerson"                  => Input::get('cmbTecReqPerson'),
				"JudgeReason"                   => Input::get('cmbJudgeReason'),
			];
			
			//if Judge is No Good
			if ($lJudgeMin == 0 && $lJudgeMax == 0)
			{
				//Nothing
			}
			else
			{
				if(($lStdSpecUnder > $lJudgeMin) or ($lStdSpecUnder > $lJudgeMax) or ($lStdSpecOver < $lJudgeMin) or ($lStdSpecOver < $lJudgeMax))
				{
					//Change Judge Color
					unset($lViewData["JudgeColorClass"]);
					
					$lViewData += [
						"JudgeColorClass"  => "NGColor",
					];
				}
			}
			
			//if Inspection Result is OK/NG
			if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_OKNG)
			{
				if (Input::get('rdiInspectionResult') == "1")
				{
					$lViewData += [
						"InspectionResultRadio1"       => true,
						"InspectionResultRadio2"       => "",
					];
				}
				elseif (Input::get('rdiInspectionResult') == "2")
				{
					$lViewData += [
						"InspectionResultRadio1"       => "",
						"InspectionResultRadio2"       => true,
					];
				}
				else
				{
					$lViewData += [
						"InspectionResultRadio1"       => "",
						"InspectionResultRadio2"       => "",
					];
				}
			}

			//change position to set as value of Result Class
			if (Input::get('rdiResultClass') == self::RESULT_CLASS_OK)
			{
				$lViewData += [
					"ResultClass1"       => true,
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}
			elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_OFFSET)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => true,
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];

				unset($lViewData["StatusColorClass"]);
				$lViewData += [
					"StatusColorClass"  => "OFFSETColor",
				];

			}
			elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_NG)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => true,
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];

				unset($lViewData["StatusColorClass"]);
				$lViewData += [
					"StatusColorClass"  => "NGColor",
				];

			}
			elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_NO_CHECK)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => true,
					"ResultClass5"       => "",
				];
			}
			elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_LINE_STOP)
			{
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => true,
				];
			}
			else
			{
				//must check Result Class
				$lViewData += [
					"ResultClass1"       => "",
					"ResultClass2"       => "",
					"ResultClass3"       => "",
					"ResultClass4"       => "",
					"ResultClass5"       => "",
				];
			}

			//change position to set as value of OFFSET Action / NG Action
			if (Input::get('rdiOffsetNGAction') == self::OFFSET_NG_ACTION_RUNNING)
			{
				$lViewData += [
					"OffsetNGAction1"       => true,
					"OffsetNGAction2"       => "",
				];
			}
			elseif (Input::get('rdiOffsetNGAction') == self::OFFSET_NG_ACTION_STOP_LINE)
			{
				$lViewData += [
					"OffsetNGAction1"       => "",
					"OffsetNGAction2"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGAction1"       => "",
					"OffsetNGAction2"       => "",
				];
			}

			//set OFFSET Action Detail / NG Action Detail（5）
			//Internal Approve
			if (Input::get('chkOffsetNGActionDetailInternal') == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailInternal"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailInternal"       => "",
				];
			}

			//Request Offset
			if (Input::get('chkOffsetNGActionDetailRequest') == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailRequest"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailRequest"       => "",
				];
			}

			//TSR
			if (Input::get('chkOffsetNGActionDetailTSR') == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailTSR"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailTSR"       => "",
				];
			}

			//Sorting
			if (Input::get('chkOffsetNGActionDetailSorting') == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailSorting"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailSorting"       => "",
				];
			}

			//No Action
			if (Input::get('chkOffsetNGActionDetailNoAction') == "1")
			{
				$lViewData += [
					"OffsetNGActionDetailNoAction"       => true,
				];
			}
			else
			{
				$lViewData += [
					"OffsetNGActionDetailNoAction"       => "",
				];
			}
		}

		//set OFFSET reason/NG reason combo
		$lViewData = $this->setOffsetNGReasonList($lViewData);

		//エラーではなく、かつ、登録された実績がある場合（変更呼び出し時を想定）
		//in case not error and entried result exists,
		if ((array_key_exists("errors", $lViewData) == False) and ($lTblInspectionResultDetail != null))
		{
			//change color again OFFSET or NG
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET)
			{
				unset($lViewData["StatusColorClass"]);
				$lViewData += [
					"StatusColorClass"  => "OFFSETColor",
				];
			}

			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG)
			{
				unset($lViewData["StatusColorClass"]);
				$lViewData += [
					"StatusColorClass"  => "NGColor",
				];
			}
			
			//if Judge is No Good
			if ($lViewData["JudgeMin"] == 0 && $lViewData["JudgeMax"] == 0)
			{
				//Nothing
			}
			else
			{
				if(($lViewData["NGUnder"] > $lViewData["JudgeMin"]) or ($lViewData["NGUnder"] > $lViewData["JudgeMax"]) or ($lViewData["NGOver"] < $lViewData["JudgeMin"]) or ($lViewData["NGOver"] < $lViewData["JudgeMax"]))
				{
					//Change Judge Color
					unset($lViewData["JudgeColorClass"]);
					$lViewData += [
						"JudgeColorClass"  => "NGColor",
					];
				}
			}
		}

		//display screen
		return View::make("user/entry", $lViewData);
	}

	//**************************************************************************
	// process            getInspectionNoList
	// overview           select Inspection Sheet Master
	// argument           ①MasterNo ②Process ③CheckSheetNo ④RevisionNo ⑤InspectionTime
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 2017.03.10 Motooka added
	// Remarks            Condition≠Normal →Use InspectionTime="ZZ"
	//**************************************************************************
	private function getInspectionNoList($pMasterNo, $pProcess, $pCheckSheetNo, $pRevisionNo, $pInspectionTime)
	{
		$lTblInspectionNo = [];

			$lTblInspectionNo = DB::select('
				    SELECT ISNO.INSPECTION_NO
				          ,ISNO.INSPECTION_ITEM
				          ,ISNO.TOOL_CD
				          ,ISNO.PICTURE_URL
				          ,ISNO.XR_CHART_NO_POKAYOKE_NO
				          ,ISNO.REFER_TO_DOCUMENT_FREQUENCY
				          ,ISNO.INSPECTION_TOOL_CLASS
				          ,COALESCE(TSKM.SOKUTEIKI_NAME,"") AS INSPECTION_TOOL_CLASS_NAME
				          ,ISNO.INSPECTION_RESULT_INPUT_TYPE
				          ,ISNO.THRESHOLD_NG_UNDER
				          ,ISNO.THRESHOLD_OFFSET_UNDER
				          ,ISNO.REFERENCE_VALUE
				          ,ISNO.THRESHOLD_OFFSET_OVER
				          ,ISNO.THRESHOLD_NG_OVER
				          ,ISNO.ANALYTICAL_GRP
				          ,ISNO.ANALYTICAL_GRP_POINT
				          ,ISNO.ANALYTICAL_GRP_HOWMANY
				          
				      FROM TINSPNOM AS ISNO
				      
				INNER JOIN TINSNTMM AS ISTM
				        ON ISNO.MASTER_NO             = ISTM.MASTER_NO
				       AND ISNO.PROCESS_ID            = ISTM.PROCESS_ID
				       AND ISNO.INSPECTION_SHEET_NO   = ISTM.INSPECTION_SHEET_NO
				       AND ISNO.REV_NO                = ISTM.REV_NO
				       AND ISNO.INSPECTION_NO         = ISTM.INSPECTION_NO
				       
				 LEFT JOIN TSOKTEIM AS TSKM
				        ON ISNO.INSPECTION_TOOL_CLASS = TSKM.SOKUTEIKI_CD
				       AND TSKM.DELETE_FLG            = "0"
				       
				     WHERE ISNO.MASTER_NO             = :MasterNo
				       AND ISNO.PROCESS_ID            = :ProcessId
				       AND ISNO.INSPECTION_SHEET_NO   = :CheckSheetNo
				       AND ISNO.REV_NO                = :RevisionNo
				       AND ISTM.INSPECTION_TIME_ID    = :InspectionTime
				       AND ISNO.DELETE_FLG            = "0"
				       AND ISTM.DELETE_FLG            = "0"
				       
				  ORDER BY ISNO.DISPLAY_ORDER
				          ,ISNO.INSPECTION_NO
			',
				[
					"MasterNo"        => TRIM((String)$pMasterNo),
					"ProcessId"       => TRIM((String)$pProcess),
					"CheckSheetNo"    => TRIM((String)$pCheckSheetNo),
					"RevisionNo"      => TRIM((String)$pRevisionNo),
					"InspectionTime"  => TRIM((String)$pInspectionTime),
				]
			);

		return $lTblInspectionNo;
	}

	//**************************************************************************
	// process            getInspectionResultDetailData
	// overview           select Inspection Result Detail (transaction table)
	// argument           ①InspectionResultNo ②InspectionNo
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultDetailData($pInspectionResultNo, $pInspectionNo)
	{
		$lTblInspectionResultDetail = [];      //Inspection Result Detai Table

			$lTblInspectionResultDetail = DB::select('
				SELECT REDE.INSPECTION_RESULT_VALUE
				      ,REDE.INSPECTION_RESULT_TYPE
				      ,REDE.OFFSET_NG_REASON
				      ,REDE.OFFSET_NG_ACTION
				      ,REDE.OFFSET_NG_ACTION_DTL_INTERNAL
				      ,REDE.OFFSET_NG_ACTION_DTL_REQUEST
				      ,REDE.OFFSET_NG_ACTION_DTL_TSR
				      ,REDE.OFFSET_NG_ACTION_DTL_SORTING
				      ,REDE.OFFSET_NG_ACTION_DTL_NOACTION
				      ,REDE.ACTION_DETAILS_TR_NO_TSR_NO
				      ,REDE.TR_NO
				      ,REDE.TECHNICIAN_USER_ID
				      ,REDE.TSR_NO
				      ,REDE.SOKUTEIKI_KANRI_NO
				      ,REDE.ANALYTICAL_GRP
				      ,REDE.ANALYTICAL_GRP_POINT
				      ,REDE.ANALYTICAL_GRP_HOWMANY
				      ,REDE.DATA_REV
				      ,IFNULL(NRSN.OFFSET_NG_BUNRUI_ID,0) AS OFFSET_NG_BUNRUI_ID
				      
				  FROM TRESDETT REDE
				  
				LEFT OUTER JOIN TNGRESNM NRSN
				             ON REDE.OFFSET_NG_REASON = NRSN.OFFSET_NG_REASON
				             
				 WHERE REDE.INSPECTION_RESULT_NO = :InspectionResultNo
				   AND REDE.INSPECTION_NO        = :InspectionNo
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"InspectionNo"       => TRIM((String)$pInspectionNo),
				]
			);

		return $lTblInspectionResultDetail;
	}

	//**************************************************************************
	// process            getInspectionResultAnalyzeData
	// overview           select Inspection Result Analyze (transaction table)
	// argument           primary key of Analyze Table & InspectionResultNo 
	// return value       Array
	// record of updates  No,1 2017.03.10
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultAnalyzeData($pMasterNo
	                                               ,$pProcessID
	                                               ,$pInspectionSheetNo
	                                               ,$pRevisionNo
	                                               ,$pInspectionDate
	                                               ,$pInspectionTime
	                                               ,$pCondition
	                                               ,$pAnalyzeGRP
	                                               ,$pInspectionResultNo)
	{
		$lTblInspectionResultAnalyze = [];      //Inspection Result Analyze Table
		//検査日を英語書式から日本語書式に変換（英語書式が入ってくる前提で、日付型に変換してからフォーマット）
		$pInspectionDate = date_format(date_create($pInspectionDate), 'Y/m/d');

			$lTblInspectionResultAnalyze = DB::select('
				SELECT ANA.AVERAGE_VALUE
				      ,ANA.MAX_VALUE
				      ,ANA.MIN_VALUE
				      ,ANA.RANGE_VALUE
				      ,ANA.STDEV
				      ,ANA.JUDGE_REASON
				  FROM TRESANAL ANA
				 WHERE ANA.MASTER_NO                 = IF(:MasterNo1 <> "", :MasterNo2, ANA.MASTER_NO)                /* value of input in screen　無ければ条件にしない */
			            AND ANA.PROCESS_ID           = IF(:ProcessId1 <> "", :ProcessId2, ANA.PROCESS_ID)             /* value of input in screen　無ければ条件にしない */
			            AND ANA.INSPECTION_SHEET_NO  LIKE :CheckSheetNo                                               /* 検査シート番号は前方一致検索　未入力なら%のみ */
			            AND ANA.REV_NO               = IF(:RevisionNo1 <> "", :RevisionNo2, ANA.REV_NO)               /* value of input in screen　無ければ条件にしない */
			            AND ANA.INSPECTION_YMD       = IF(:Date1 <> "", :Date2, ANA.INSPECTION_YMD)                   /* value of input in screen　無ければ条件にしない */
			            AND ANA.INSPECTION_TIME_ID   = IF(:TimeID1 <> "", :TimeID2, ANA.INSPECTION_TIME_ID)           /* value of input in screen　無ければ条件にしない */
			            AND ANA.CONDITION_CD         = IF(:ConditionCD1 <> "", :ConditionCD2, ANA.CONDITION_CD)       /* value of input in screen　無ければ条件にしない */
			            AND ANA.ANALYTICAL_GRP       = IF(:AnalyticalGRP1 <> "", :AnalyticalGRP2, ANA.ANALYTICAL_GRP) /* value of parameter　無ければ条件にしない*/
			            AND ANA.INSPECTION_RESULT_NO = :InspectionResultNo
			',
				[
					"MasterNo1"          => TRIM((String)$pMasterNo),
					"MasterNo2"          => TRIM((String)$pMasterNo),
					"ProcessId1"         => TRIM((String)$pProcessID),
					"ProcessId2"         => TRIM((String)$pProcessID),
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
					"RevisionNo1"        => TRIM((String)$pRevisionNo),
					"RevisionNo2"        => TRIM((String)$pRevisionNo),
					"Date1"              => TRIM((String)$pInspectionDate),
					"Date2"              => TRIM((String)$pInspectionDate),
					"TimeID1"            => TRIM((String)$pInspectionTime),
					"TimeID2"            => TRIM((String)$pInspectionTime),
					"ConditionCD1"       => TRIM((String)$pCondition),
					"ConditionCD2"       => TRIM((String)$pCondition),
					"AnalyticalGRP1"     => TRIM((String)$pAnalyzeGRP),
					"AnalyticalGRP2"     => TRIM((String)$pAnalyzeGRP),
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
				]
			);

		return $lTblInspectionResultAnalyze;
	}

	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailData1
	// overview           select Inspection Result Detail (transaction table)
	// argument           ①InspectionResultNo ②InspectionNo
	// return value       Array
	// record of updates  No,1 2017.03.10
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailData1($pInspectionResultNo, $pInspectionNo)
	{
		$lTblInspectionResultDetail1 = [];      //Inspection Result Detai Table

			$lTblInspectionResultDetail1 = DB::select('
				SELECT Count(REDE.INSPECTION_RESULT_NO) AS COUNT
				      ,REDE.INSPECTION_RESULT_VALUE
				      
				  FROM TRESDETT REDE
				  
				 WHERE REDE.INSPECTION_RESULT_NO = :InspectionResultNo
				   AND REDE.INSPECTION_NO        = :InspectionNo
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"InspectionNo"       => TRIM((String)$pInspectionNo),
				]
			);

		return $lTblInspectionResultDetail1;
	}

	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailData2
	// overview           select Inspection Result Detail (transaction table)
	// argument           ①InspectionResultNo ②AnalyticalGroup
	// return value       Array
	// record of updates  No,1 2017.03.10
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailData2($pInspectionResultNo, $pAnalyticalGroup)
	{
		$lTblInspectionResultDetail2 = [];      //Inspection Result Detai Table

			$lTblInspectionResultDetail2 = DB::select('
				SELECT Count(*) AS COUNT
				
				  FROM TRESDETT REDE
				  
				 WHERE REDE.INSPECTION_RESULT_NO     = :InspectionResultNo
				   AND REDE.ANALYTICAL_GRP           = :AnalyticalGroup
			',
				[
					"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
					"AnalyticalGroup"          => TRIM((String)$pAnalyticalGroup),
				]
			);

		return $lTblInspectionResultDetail2;
	}


	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailValueData
	// overview           select Inspection Result Detail (transaction table)
	// argument           
	// return value       Array
	// record of updates  No,1 2017.04.21
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailValueData($pInspectionResultNo, $pAnalyticalGroup)
	{
		$lTblInspectionResultDetail3 = [];      //Inspection Result Detai Table
		
			$lTblInspectionResultDetail3 = DB::select('
				SELECT REDE.INSPECTION_RESULT_VALUE
				
				  FROM TRESDETT REDE
				  
				 WHERE REDE.INSPECTION_RESULT_NO     = :InspectionResultNo
				   AND REDE.ANALYTICAL_GRP           = :AnalyticalGroup
			',
				[
					"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
					"AnalyticalGroup"          => TRIM((String)$pAnalyticalGroup),
				]
			);

		return $lTblInspectionResultDetail3;
	}


	//**************************************************************************
	// process            getBeforeCommitInspectionResultAnalyzeData
	// overview           select Inspection Result Analize (transaction table)
	// argument           ①InspectionResultNo ②AnalyticalGroup
	// return value       Array
	// record of updates  No,1 2017.03.10
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultAnalyzeData($pInspectionResultNo, $pAnalyticalGroup)
	{
		$lTblAnalyze = [];      //Inspection Result Analyze Table

			$lTblAnalyze = DB::select('
				SELECT Count(ANALY.ANALYTICAL_GRP) AS COUNT
				      ,ANALY.CALC_NUM
				      ,ANALY.MAX_VALUE
				      ,ANALY.MIN_VALUE
				      ,ANALY.AVERAGE_VALUE
				      ,ANALY.STDEV
				      
				 FROM TRESANAL ANALY
				 
				WHERE ANALY.ANALYTICAL_GRP       = :AnalyticalGroup
				  AND ANALY.INSPECTION_RESULT_NO = :InspectionResultNo
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"AnalyticalGroup"    => TRIM((String)$pAnalyticalGroup),
				]
			);

		return $lTblAnalyze;
	}

	//**************************************************************************
	// process            getCoefficientData
	// overview           select Coefficient (Master table)
	// argument           ①CodeKind
	// return value       Array
	// record of updates  No,1 2017.03.10
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getCoefficientData($pCodeKind, $pAnalyticalGroupHowManyMax)
	{
		$lTblCoefficient = [];      //Coefficient Table

			$lTblCoefficient = DB::select('
				SELECT COEFFICIENT
				  FROM tccientmst
				 WHERE CODE_KIND  = :CodeKind
				   AND CODE_NUM   = :GroupHowManyMax
			',
				[
					"CodeKind"        => $pCodeKind,
					"GroupHowManyMax" => $pAnalyticalGroupHowManyMax,
				]
			);

		return $lTblCoefficient;
	}

	//**************************************************************************
	// process            insertAndUpdateInspectionResult
	// overview           insert or update inspection head & detail & analyze (transaction table)
	// argument           many...
	// return value       Inspection Result No.
	// record of updates  No,1 2014.08.26
	//                    No,2 2017.03.10
	// Remarks            
	//**************************************************************************
	private function insertAndUpdateInspectionResult($pInspectionResultNo
	                                                ,$pCheckSheetNo
	                                                ,$pRevisionNo
	                                                ,$pMachineNo
	                                                ,$pInspectorCode
	                                                ,$pInspectionDate
	                                                ,$pCondition
	                                                ,$pInspectionTimeID
	                                                ,$pDataRevision
	                                                ,$pInspectionNo
	                                                ,$pInspectionResultValue
	                                                ,$pInspectionResultValueOKNGRadio
	                                                ,$pResultClass
	                                                ,$pOffsetNGReason
	                                                ,$pOffsetNGAction
	                                                ,$pOffsetNGActionDetailInternal
	                                                ,$pOffsetNGActionDetailRequest
	                                                ,$pOffsetNGActionDetailTSR
	                                                ,$pOffsetNGActionDetailSorting
	                                                ,$pOffsetNGActionDetailNoAction
	                                                ,$pActionDetails
	                                                ,$pTecReqNo
	                                                ,$pTSRNo
	                                                ,$pTecReqPerson
	                                                
	                                                ,$pMasterNo
	                                                ,$pProcessID
	                                                ,$pMaterialLotNo
	                                                ,$pCertificateNo
	                                                ,$pPONo
	                                                ,$pInvoiceNo
	                                                ,$pQuantityCoil
	                                                ,$pQuantityWeight
	                                                ,$pLotNo
	                                                
	                                                ,$pAnalyticalGroup
	                                                ,$pAnalyticalGroupPoint
	                                                ,$pAnalyticalGroupHowmany
	                                                
	                                                ,$pStdSpecUnder
			                                        ,$pStdSpecOver
			                                        ,$pPictureURL
			                                        ,$pCalcNam
			                                        ,$pMaxValue
			                                        ,$pMinValue
			                                        ,$pAverageValue
			                                        ,$pRangeValue
			                                        ,$pStdev
			                                        ,$pCpk
			                                        ,$pXbarUcl
			                                        ,$pXbarLcl
			                                        ,$pRchartUcl
			                                        ,$pRchartLcl
			                                        
			                                        ,$pJudgeReason
			                                        
			                                        ,$pResultInputType
	                                                )
	{

		Log::write('info', 'BA2010 insert/update',
			[
				"StdSpecUnder"            => $pStdSpecUnder,
				"StdSpecOver"             => $pStdSpecOver,
				"PictureURL"              => $pPictureURL,
				"CalcNam"                 => $pCalcNam,
				"MaxValue"                => $pMaxValue,
				"MinValue"                => $pMinValue,
				"AverageValue"            => $pAverageValue,
				"RangeValue"              => $pRangeValue,
				"Stdev"                   => $pStdev,
				"Cpk"                     => $pCpk,
				"XbarUcl"                 => $pXbarUcl,
				"XbarLcl"                 => $pXbarLcl,
				"RchartUcl"               => $pRchartUcl,
				"RchartLcl"               => $pRchartLcl,
				
				"AnalyticalGroup"         => $pAnalyticalGroup,
				"AnalyticalGroupPoint"    => $pAnalyticalGroupPoint,
				"AnalyticalGroupHowmany"  => $pAnalyticalGroupHowmany,
				"JudgeReason"             => $pJudgeReason,
				]
			);
		
		
		//登録用の検査結果
		$lInspectionResultValue = "";

		//ラジオボタンの結果が入っていない場合は、テキストボックスの結果を使う
		if ($pInspectionResultValueOKNGRadio == "")
		{
			$lInspectionResultValue = $pInspectionResultValue;
		}
		else
		//ラジオボタンの結果が入っている場合は、その値を使う（ここでは1か2を想定）
		{
			$lInspectionResultValue = $pInspectionResultValueOKNGRadio;
		}

		//$pActionDetailsに30文字以上入っている場合は改行コードを挿入 2014/11/07
		$pActionDetails = wordwrap($pActionDetails, self::ACTION_DETAILS_KAIGYO, PHP_EOL, true);

		//検査日を英語書式から日本語書式に変換（英語書式が入ってくる前提で、日付型に変換してからフォーマット）
		$pInspectionDate = date_format(date_create($pInspectionDate), 'Y/m/d');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		//無名関数にパラメータの値を渡し、処理後に採番後の検査実績Noを受け取っている
		$pInspectionResultNo = DB::transaction
		(
			function() use ($pInspectionResultNo
	                       ,$pCheckSheetNo
	                       ,$pRevisionNo
	                       ,$pMachineNo
	                       ,$pInspectorCode
	                       ,$pInspectionDate
	                       ,$pCondition
	                       ,$pInspectionTimeID
	                       ,$pDataRevision
	                       ,$pInspectionNo
	                       ,$lInspectionResultValue
	                       ,$pResultClass
	                       ,$pOffsetNGReason
	                       ,$pOffsetNGAction
	                       ,$pOffsetNGActionDetailInternal
	                       ,$pOffsetNGActionDetailRequest
	                       ,$pOffsetNGActionDetailTSR
	                       ,$pOffsetNGActionDetailSorting
	                       ,$pOffsetNGActionDetailNoAction
	                       ,$pActionDetails
	                       ,$pTecReqNo
	                       ,$pTSRNo
	                       ,$pTecReqPerson
	                       
	                       ,$pMasterNo
	                       ,$pProcessID
	                       ,$pMaterialLotNo
	                       ,$pCertificateNo
	                       ,$pPONo
	                       ,$pInvoiceNo
	                       ,$pQuantityCoil
	                       ,$pQuantityWeight
	                       ,$pLotNo
	                       
	                       ,$pAnalyticalGroup
	                       ,$pAnalyticalGroupPoint
	                       ,$pAnalyticalGroupHowmany
	                       
	                       ,$pStdSpecUnder
	                       ,$pStdSpecOver
	                       ,$pPictureURL
	                       ,$pCalcNam
	                       ,$pMaxValue
	                       ,$pMinValue
	                       ,$pAverageValue
	                       ,$pRangeValue
	                       ,$pStdev
	                       ,$pCpk
	                       ,$pXbarUcl
	                       ,$pXbarLcl
	                       ,$pRchartUcl
	                       ,$pRchartLcl
	                       
	                       ,$pJudgeReason
	                       
	                       ,$pResultInputType
	                       )
			{
				//検査実績Noがゼロ→完全新規登録なのでヘッダ登録が必要
				if ($pInspectionResultNo == 0)
				{
					DB::insert('
							INSERT INTO TRESHEDT
							           (MASTER_NO
							           ,PROCESS_ID
							           ,INSPECTION_SHEET_NO
							           ,REV_NO
							           ,MACHINE_NO
							           ,INSERT_USER_ID
							           ,INSPECTION_YMD
							           ,CONDITION_CD
							           ,INSPECTION_TIME_ID
							           ,MATERIAL_LOT_NO
							           ,CERTIFICATE_NO
							           ,PO_NO
							           ,INVOICE_NO
							           ,QUANTITY_COIL
							           ,QUANTITY_WEIGHT
							           ,LOT_NO
							           ,INSERT_YMDHMS
							           ,LAST_UPDATE_USER_ID
							           ,UPDATE_YMDHMS
							           ,DATA_REV)
							     VALUES (:MasterNo
							            ,:ProcessID
							            ,:CheckSheetNo
							            ,:RevisionNo
							            ,:MachineNo
							            ,:InspectionUserID
							            ,:InspectionDate
							            ,:Condition
							            ,:InspectionTimeID
							            ,:MaterialLotNo
							            ,:CertificateNo
							            ,:PONo
							            ,:InvoiceNo
							            ,:QuantityCoil
							            ,:QuantityWeight
							            ,:LotNo
							            ,now()
							            ,:LastUpdateUserID
							            ,now()
							            ,1)
					',
						[
							"MasterNo"         => $pMasterNo,
							"ProcessID"        => $pProcessID,
							"CheckSheetNo"     => $pCheckSheetNo,
							"RevisionNo"       => $pRevisionNo,
							"MachineNo"        => $pMachineNo,
							"InspectionUserID" => $pInspectorCode,
							"InspectionDate"   => $pInspectionDate,
							"Condition"        => $pCondition,
							"InspectionTimeID" => $pInspectionTimeID,
							"MaterialLotNo"    => $pMaterialLotNo,
							"CertificateNo"    => $pCertificateNo,
							"PONo"             => $pPONo,
							"InvoiceNo"        => $pInvoiceNo,
							"QuantityCoil"     => $pQuantityCoil,
							"QuantityWeight"   => $pQuantityWeight,
							"LotNo"            => $pLotNo,
							"LastUpdateUserID" => $pInspectorCode,
						]
					);

					//自動採番した検査実績No.を保持
					$lTblLastInsertID = DB::select('
						SELECT LAST_INSERT_ID();
					'
					);

					$lRowLastInsertID = (Array)$lTblLastInsertID[0];
					$pInspectionResultNo = $lRowLastInsertID["LAST_INSERT_ID()"];
				}
				else
				{
					//検査実績Noがゼロ以外→Modifyもしくは2件目以降なので、ヘッダは更新
					DB::update('
							UPDATE TRESHEDT
							   SET LAST_UPDATE_USER_ID = :LastUpdateUserID
							      ,UPDATE_YMDHMS = now()
							      ,DATA_REV = DATA_REV + 1
							 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
							   AND DATA_REV             = :DataRevision
					',
						[
							"LastUpdateUserID"    => $pInspectorCode,
							"InspectionResultNo"  => $pInspectionResultNo,
							"DataRevision"        => $pDataRevision,
						]
					);
				}

				//明細はあれば更新、無ければ登録
				DB::insert('
					     INSERT INTO TRESDETT
					                (INSPECTION_RESULT_NO
					                ,INSPECTION_NO
					                ,INSPECTION_RESULT_VALUE
					                ,INSPECTION_RESULT_TYPE
					                ,OFFSET_NG_REASON
					                ,OFFSET_NG_ACTION
					                ,OFFSET_NG_ACTION_DTL_INTERNAL
					                ,OFFSET_NG_ACTION_DTL_REQUEST
					                ,OFFSET_NG_ACTION_DTL_TSR
					                ,OFFSET_NG_ACTION_DTL_SORTING
					                ,OFFSET_NG_ACTION_DTL_NOACTION
					                ,ACTION_DETAILS_TR_NO_TSR_NO
					                ,TR_NO
					                ,TECHNICIAN_USER_ID
					                ,TSR_NO
					                ,SOKUTEIKI_KANRI_NO
					                ,ANALYTICAL_GRP
					                ,ANALYTICAL_GRP_POINT
					                ,ANALYTICAL_GRP_HOWMANY
					                ,INSERT_YMDHMS
					                ,LAST_UPDATE_USER_ID
					                ,UPDATE_YMDHMS
					                ,DATA_REV)
					          VALUES (:InspectionResultNo
					                 ,:InspectionNo
					                 ,:InspectionResultValue1
					                 ,:InspectionResultType1
					                 ,:OffsetNGReason1
					                 ,:OffsetNGAction1
					                 ,:OffsetNGActionDetailInternal1
					                 ,:OffsetNGActionDetailRequest1
					                 ,:OffsetNGActionDetailTSR1
					                 ,:OffsetNGActionDetailSorting1
					                 ,:OffsetNGActionDetailNoAction1
					                 ,:ActionDetails1
					                 ,:TecReqNo1
					                 ,:TecReqPerson1
					                 ,:TSRNo1
					                 ,""
					                 ,:AnalyticalGroup1
					                 ,:AnalyticalGroupPoint1
					                 ,:AnalyticalGroupHowmany1
					                 ,now()
					                 ,:LastUpdateUserID1
					                 ,now()
					                 ,1)
					ON DUPLICATE KEY
					          UPDATE INSPECTION_RESULT_VALUE       = :InspectionResultValue2
					                ,INSPECTION_RESULT_TYPE        = :InspectionResultType2
					                ,OFFSET_NG_REASON              = :OffsetNGReason2
					                ,OFFSET_NG_ACTION              = :OffsetNGAction2
					                ,OFFSET_NG_ACTION_DTL_INTERNAL = :OffsetNGActionDetailInternal2
					                ,OFFSET_NG_ACTION_DTL_REQUEST  = :OffsetNGActionDetailRequest2
					                ,OFFSET_NG_ACTION_DTL_TSR      = :OffsetNGActionDetailTSR2
					                ,OFFSET_NG_ACTION_DTL_SORTING  = :OffsetNGActionDetailSorting2
					                ,OFFSET_NG_ACTION_DTL_NOACTION = :OffsetNGActionDetailNoAction2
					                ,ACTION_DETAILS_TR_NO_TSR_NO   = :ActionDetails2
					                ,TR_NO                         = :TecReqNo2
					                ,TECHNICIAN_USER_ID            = :TecReqPerson2
					                ,TSR_NO                        = :TSRNo2
					                ,SOKUTEIKI_KANRI_NO            = ""
					                ,ANALYTICAL_GRP                = :AnalyticalGroup2
					                ,ANALYTICAL_GRP_POINT          = :AnalyticalGroupPoint2
					                ,ANALYTICAL_GRP_HOWMANY        = :AnalyticalGroupHowmany2
					                ,LAST_UPDATE_USER_ID           = :LastUpdateUserID2
					                ,UPDATE_YMDHMS                 = now()
					                ,DATA_REV                      = DATA_REV + 1
				',
					[
						"InspectionResultNo"            => $pInspectionResultNo,
						"InspectionNo"                  => $pInspectionNo,
						"InspectionResultValue1"        => $lInspectionResultValue,
						"InspectionResultValue2"        => $lInspectionResultValue,
						"InspectionResultType1"         => $pResultClass,
						"InspectionResultType2"         => $pResultClass,
						"OffsetNGReason1"               => $pOffsetNGReason,
						"OffsetNGReason2"               => $pOffsetNGReason,
						"OffsetNGAction1"               => $pOffsetNGAction,
						"OffsetNGAction2"               => $pOffsetNGAction,
						"OffsetNGActionDetailInternal1" => $pOffsetNGActionDetailInternal,
						"OffsetNGActionDetailInternal2" => $pOffsetNGActionDetailInternal,
						"OffsetNGActionDetailRequest1"  => $pOffsetNGActionDetailRequest,
						"OffsetNGActionDetailRequest2"  => $pOffsetNGActionDetailRequest,
						"OffsetNGActionDetailTSR1"      => $pOffsetNGActionDetailTSR,
						"OffsetNGActionDetailTSR2"      => $pOffsetNGActionDetailTSR,
						"OffsetNGActionDetailSorting1"  => $pOffsetNGActionDetailSorting,
						"OffsetNGActionDetailSorting2"  => $pOffsetNGActionDetailSorting,
						"OffsetNGActionDetailNoAction1" => $pOffsetNGActionDetailNoAction,
						"OffsetNGActionDetailNoAction2" => $pOffsetNGActionDetailNoAction,
						"ActionDetails1"                => $pActionDetails,
						"ActionDetails2"                => $pActionDetails,
						"TecReqNo1"                     => $pTecReqNo,
						"TecReqNo2"                     => $pTecReqNo,
						"TecReqPerson1"                 => $pTecReqPerson,
						"TecReqPerson2"                 => $pTecReqPerson,
						"TSRNo1"                        => $pTSRNo,
						"TSRNo2"                        => $pTSRNo,
						"AnalyticalGroup1"              => $pAnalyticalGroup,
						"AnalyticalGroup2"              => $pAnalyticalGroup,
						"AnalyticalGroupPoint1"         => $pAnalyticalGroupPoint,
						"AnalyticalGroupPoint2"         => $pAnalyticalGroupPoint,
						"AnalyticalGroupHowmany1"       => $pAnalyticalGroupHowmany,
						"AnalyticalGroupHowmany2"       => $pAnalyticalGroupHowmany,
						"LastUpdateUserID1"             => $pInspectorCode,
						"LastUpdateUserID2"             => $pInspectorCode,
					]
				);
				
				Log::write('info', 'Finish Insert/Update Detail');

				//分析用はあれば更新、無ければ登録
				DB::insert('
					     INSERT INTO TRESANAL
					                (MASTER_NO
					                ,PROCESS_ID
					                ,INSPECTION_SHEET_NO
					                ,REV_NO
					                ,INSPECTION_YMD
					                ,INSPECTION_TIME_ID
					                ,CONDITION_CD
					                ,ANALYTICAL_GRP
					                ,INSPECTION_RESULT_NO
					                ,STD_MIN_SPEC
					                ,STD_MAX_SPEC
					                ,PICTURE_URL
					                ,CALC_NUM
					                ,MAX_VALUE
					                ,MIN_VALUE
					                ,AVERAGE_VALUE
					                ,RANGE_VALUE
					                ,STDEV
					                ,CPK
					                ,XBAR_UCL
					                ,XBAR_LCL
					                ,RCHART_UCL
					                ,RCHART_LCL
					                ,JUDGE_REASON
					                ,INSERT_YMDHMS
					                ,LAST_UPDATE_USER_ID
					                ,UPDATE_YMDHMS
					                ,DATA_REV)
					          VALUES (:MasterNo
									 ,:ProcessID
									 ,:CheckSheetNo
									 ,:RevisionNo
									 ,:InspectionDate
									 ,:InspectionTimeID
									 ,:Condition
									 ,:AnalyticalGroup
									 ,:InspectionResultNo1
									 ,:StandardMinSpec1
									 ,:StandardMaxSpec1
									 ,:PictureURL1
									 ,:CalcNam1
									 ,:MaxValue1
									 ,:MinValue1
									 ,:AverageValue1
									 ,:RangeValue1
									 ,:Stdev1
									 ,:Cpk1
									 ,:XbarUcl1
									 ,:XbarLcl1
									 ,:RchartUcl1
									 ,:RchartLcl1
									 ,:JudgeReason1
									 ,now()
									 ,:LastUpdateUserID1
									 ,now()
									 ,1)
					ON DUPLICATE KEY
					          UPDATE INSPECTION_RESULT_NO = :InspectionResultNo2
					                ,STD_MIN_SPEC         = :StandardMinSpec2
					                ,STD_MAX_SPEC         = :StandardMaxSpec2
					                ,PICTURE_URL          = :PictureURL2
					                ,CALC_NUM             = :CalcNam2
					                ,MAX_VALUE            = :MaxValue2
					                ,MIN_VALUE            = :MinValue2
					                ,AVERAGE_VALUE        = :AverageValue2
					                ,RANGE_VALUE          = :RangeValue2
					                ,STDEV                = :Stdev2
					                ,CPK                  = :Cpk2
					                ,XBAR_UCL             = :XbarUcl2
					                ,XBAR_LCL             = :XbarLcl2
					                ,RCHART_UCL           = :RchartUcl2
					                ,RCHART_LCL           = :RchartLcl2
					                ,JUDGE_REASON         = :JudgeReason2
					                ,LAST_UPDATE_USER_ID  = :LastUpdateUserID2
					                ,UPDATE_YMDHMS        = now()
					                ,DATA_REV             = DATA_REV + 1
				',
					[
						"MasterNo"             => $pMasterNo,
						"ProcessID"            => $pProcessID,
						"CheckSheetNo"         => $pCheckSheetNo,
						"RevisionNo"           => $pRevisionNo,
						"InspectionDate"       => $pInspectionDate,
						"InspectionTimeID"     => $pInspectionTimeID,
						"Condition"            => $pCondition,
						"AnalyticalGroup"      => $pAnalyticalGroup,
						"InspectionResultNo1"  => $pInspectionResultNo,
						"InspectionResultNo2"  => $pInspectionResultNo,
						"StandardMinSpec1"     => $pStdSpecUnder,
						"StandardMinSpec2"     => $pStdSpecUnder,
						"StandardMaxSpec1"     => $pStdSpecOver,
						"StandardMaxSpec2"     => $pStdSpecOver,
						"PictureURL1"          => $pPictureURL,
						"PictureURL2"          => $pPictureURL,
						"CalcNam1"             => $pCalcNam,
						"CalcNam2"             => $pCalcNam,
						"MaxValue1"            => $pMaxValue,
						"MaxValue2"            => $pMaxValue,
						"MinValue1"            => $pMinValue,
						"MinValue2"            => $pMinValue,
						"AverageValue1"        => $pAverageValue,
						"AverageValue2"        => $pAverageValue,
						"RangeValue1"          => $pRangeValue,
						"RangeValue2"          => $pRangeValue,
						"Stdev1"               => $pStdev,
						"Stdev2"               => $pStdev,
						"Cpk1"                 => $pCpk,
						"Cpk2"                 => $pCpk,
						"XbarUcl1"             => $pXbarUcl,
						"XbarUcl2"             => $pXbarUcl,
						"XbarLcl1"             => $pXbarLcl,
						"XbarLcl2"             => $pXbarLcl,
						"RchartUcl1"           => $pRchartUcl,
						"RchartUcl2"           => $pRchartUcl,
						"RchartLcl1"           => $pRchartLcl,
						"RchartLcl2"           => $pRchartLcl,
						"JudgeReason1"         => $pJudgeReason,
						"JudgeReason2"         => $pJudgeReason,
						"LastUpdateUserID1"    => $pInspectorCode,
						"LastUpdateUserID2"    => $pInspectorCode,
					]
				);
				
				Log::write('info', 'Finish Insert/Update Analyze');
				
				
				//無名関数から呼び出し元へリターンするだけ
				return $pInspectionResultNo;
			}

		);

	    return $pInspectionResultNo;
	}

	//**************************************************************************
	// process            setInspectionToolClassList
	// overview           Show InspectionToolClass(Equipment) List
	// argument           ①ScreenArray ②InspectionCheckSheetNo ③RevisionNo ④InspectionTime
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            Condition≠Normal →Use InspectionTime="ZZ"
	//**************************************************************************
	private function setInspectionToolClassList($pViewData, $pInspectionSheetNo, $pRevisionNo, $pInspectionTime)
	{
		$lArrDataListInspectionToolClass     = ["" => ""];  //list of Inspection Tool Class data to return to screen

		//if list of Inspection Tool Class does not exist in session,search and store in session
		if (is_null(Session::get('BA2010InspectionToolClassDropdownListData')))
		{
			//search Inspection Tool Class
			$lArrDataListInspectionToolClass = $this->getInspectionToolClassList($pInspectionSheetNo, $pRevisionNo, $pInspectionTime);

			//store in session
			Session::put('BA2010InspectionToolClassDropdownListData', $lArrDataListInspectionToolClass);
		}
		else
		{
			//in case Inspection No is in session, get it
			$lArrDataListInspectionToolClass = Session::get('BA2010InspectionToolClassDropdownListData');
		}

		//Add ViewData (Write +=)
		$pViewData += [
			"arrDataListInspectionToolClass" => $lArrDataListInspectionToolClass
		];

		return $pViewData;
	}

	//**************************************************************************
	// process            getInspectionToolClassList
	// overview           Select Table
	// argument           ①InspectionCheckSheetNo ②RevisionNo ③InspectionTime
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            Condition≠Normal →Use InspectionTime="ZZ"
	//**************************************************************************
	private function getInspectionToolClassList($pInspectionSheetNo, $pRevisionNo, $pInspectionTime)
	{
		$lTblInspectionToolClass         = []; //DataTable
		$lRowInspectionToolClass         = []; //DataRow
		$lArrDataListInspectionToolClass = []; //list of Inspection Tool Class data to return to screen

		$lTblInspectionToolClass = DB::select('
		      SELECT DISTINCT ISNO.INSPECTION_TOOL_CLASS
		            ,TSKM.SOKUTEIKI_NAME AS INSPECTION_TOOL_CLASS_NAME
		            
		        FROM TINSPNOM AS ISNO
		        
		  INNER JOIN TINSNTMM AS ISTM
		          ON ISNO.INSPECTION_SHEET_NO = ISTM.INSPECTION_SHEET_NO
		         AND ISNO.REV_NO              = ISTM.REV_NO
		         AND ISNO.INSPECTION_NO       = ISTM.INSPECTION_NO
		         
		  INNER JOIN TSOKTEIM AS TSKM
		          ON ISNO.INSPECTION_TOOL_CLASS = TSKM.SOKUTEIKI_CD
		          
		       WHERE ISNO.INSPECTION_SHEET_NO = :InspectionSheetNo
		         AND ISNO.REV_NO              = :RevisionNo
		         AND ISTM.INSPECTION_TIME_ID  = :InspectionTime
		         AND ISNO.DELETE_FLG          = "0"
		         AND ISTM.DELETE_FLG          = "0"
		         AND TSKM.DELETE_FLG          = "0"
		         
		    ORDER BY TSKM.DISPLAY_ORDER
		',
				[
					"InspectionSheetNo" => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"        => TRIM((String)$pRevisionNo),
					"InspectionTime"    => TRIM((String)$pInspectionTime),
				]
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblInspectionToolClass = (array)$lTblInspectionToolClass;

		//ALLだけ固定で格納
		$lArrDataListInspectionToolClass += [
			"0" => "ALL"
		];

		//データが存在する
		if ($lTblInspectionToolClass != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblInspectionToolClass as $lRowInspectionToolClass)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowInspectionToolClass = (Array)$lRowInspectionToolClass;

				$lArrDataListInspectionToolClass += [
					$lRowInspectionToolClass["INSPECTION_TOOL_CLASS"] => $lRowInspectionToolClass["INSPECTION_TOOL_CLASS_NAME"]
				];
			}
		}

		return $lArrDataListInspectionToolClass;
	}

	//**************************************************************************
	// 処理名    setJudgeReason
	// 概要      JudgeReasonボックス用のデータを取得し、画面表示用の配列にセットする
	// 引数      画面表示用配列
	// 戻り値    画面表示用配列
	//**************************************************************************
	private function setJudgeReason($pViewData)
	{
		$lJudgeReasonList     = ["" => ""];  //返却用ビューデータ

		//Judge理由の検索処理
		$lJudgeReasonList = $this->getJudgeReasonList();

		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrJudgeReasonList" => $lJudgeReasonList
		];

		return $pViewData;
	}

	//**************************************************************************
	// 処理名    getJudgeReasonList
	// 概要      JudgeReasonコンボボックス用のデータをSQLで取得
	// 引数      無し
	// 戻り値    取得結果の配列
	//**************************************************************************
	private function getJudgeReasonList()
	{
		$lTblJudgeReason			= [];
		$lRowJudgeReason			= [];
		$lArrJudgeReason			= [];

		$lTblJudgeReason = DB::select(
		'
		      SELECT CODE.CODE_ORDER
		            ,CODE.CODEORDER_NAME
		        FROM TCODEMST AS CODE
		       WHERE CODE.CODE_NO = "003"
		    ORDER BY CODE.DISPLAY_ORDER
		'
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblJudgeReason = (array)$lTblJudgeReason;

		//空白行を追加
		$lArrJudgeReason += [
			"" => ""
		];

		//データが存在する
		if ($lTblJudgeReason != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblJudgeReason as $lRowJudgeReason)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowJudgeReason = (Array)$lRowJudgeReason;

				$lArrJudgeReason += [
					$lRowJudgeReason["CODE_ORDER"] => $lRowJudgeReason["CODEORDER_NAME"]
				];
			}
		}

		return $lArrJudgeReason;
	}

	//**************************************************************************
	// 処理名    setOffsetNGReasonList
	// 概要      OFFSET理由/NG理由コンボボックス用のデータを取得し、画面表示用の配列にセットする
	// 引数      画面表示用配列
	// 戻り値    画面表示用配列
	//**************************************************************************
	private function setOffsetNGReasonList($pViewData)
	{
		$lOffsetNGReasonList     = ["" => ""];  //返却用ビューデータ

		//OFFSET理由/NG理由の検索処理
		$lOffsetNGReasonList = $this->getOffsetNGReasonList($pViewData["OffsetNGBunrui"]);

		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrOffsetNGReasonList" => $lOffsetNGReasonList
		];

		return $pViewData;
	}

	//**************************************************************************
	// 処理名    getOffsetNGReasonList
	// 概要      OFFSET理由/NG理由コンボボックス用のデータをSQLで取得
	// 引数      無し
	// 戻り値    取得結果の配列
	//**************************************************************************
	private function getOffsetNGReasonList($pOffsetNGBunrui)
	{
		$lTblOffsetNGReason			= []; //OFFSET理由/NG理由情報取得結果
		$lRowOffsetNGReason			= []; //配列変換時のワーク領域
		$lArrOffsetNGReason			= []; //返却するOFFSET理由/NG理由リスト

		$lTblOffsetNGReason = DB::select(
		'
		      SELECT NRSN.OFFSET_NG_REASON
		            ,NRSN.OFFSET_NG_REASON AS OFFSET_NG_REASON_NAME
		        FROM TNGRESNM AS NRSN
		       WHERE NRSN.DELETE_FLG = "0"
		         AND NRSN.OFFSET_NG_BUNRUI_ID = :OffsetNGBunrui
		    ORDER BY NRSN.DISPLAY_ORDER
		',
			[
				"OffsetNGBunrui" => $pOffsetNGBunrui,
			]
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblOffsetNGReason = (array)$lTblOffsetNGReason;

		//空白行を追加
		$lArrOffsetNGReason += [
			"" => ""
		];

		//データが存在する
		if ($lTblOffsetNGReason != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblOffsetNGReason as $lRowOffsetNGReason)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowOffsetNGReason = (Array)$lRowOffsetNGReason;

				$lArrOffsetNGReason += [
					$lRowOffsetNGReason["OFFSET_NG_REASON"] => $lRowOffsetNGReason["OFFSET_NG_REASON_NAME"]
				];
			}
		}

		return $lArrOffsetNGReason;
	}

	//**************************************************************************
	// 処理名    setOffsetNGActionDetail
	// 概要      OFFSET/NGアクションコンボボックス用のデータを取得し、画面表示用の配列にセットする
	// 引数      画面表示用配列
	// 戻り値    画面表示用配列
	//**************************************************************************
	private function setOffsetNGActionDetail($pViewData)
	{
		$lOffsetNGActionDetailList     = ["" => ""];  //返却用ビューデータ

		//OFFSET理由/NG理由の検索処理
		$lOffsetNGActionDetailList = $this->getOffsetNGActionDetailList();

		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrActionDetailsList" => $lOffsetNGActionDetailList
		];

		return $pViewData;
	}

	//**************************************************************************
	// 処理名    getOffsetNGActionDetailList
	// 概要      OFFSET/NGアクションコンボボックス用のデータをSQLで取得
	// 引数      無し
	// 戻り値    取得結果の配列
	//**************************************************************************
	private function getOffsetNGActionDetailList()
	{
		$lTblOffsetNGActionDetail			= []; //OFFSET/NGアクション情報取得結果
		$lRowOffsetNGActionDetail			= []; //配列変換時のワーク領域
		$lArrOffsetNGActionDetail			= []; //返却するOFFSET/NGアクションリスト

		$lTblOffsetNGActionDetail = DB::select(
		'
		      SELECT ACTN.ACTION_DETAILS_NAME AS ACTION_DETAIL
		            ,ACTN.ACTION_DETAILS_NAME AS ACTION_DETAILS_NAME
		        FROM TACTIONM AS ACTN
		       WHERE ACTN.DELETE_FLG = "0"
		    ORDER BY ACTN.DISPLAY_ORDER
		'
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblOffsetNGActionDetail = (array)$lTblOffsetNGActionDetail;

		//空白行を追加
		$lArrOffsetNGActionDetail += [
			"" => ""
		];

		//データが存在する
		if ($lTblOffsetNGActionDetail != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblOffsetNGActionDetail as $lRowOffsetNGActionDetail)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowOffsetNGActionDetail = (Array)$lRowOffsetNGActionDetail;

				$lArrOffsetNGActionDetail += [
					$lRowOffsetNGActionDetail["ACTION_DETAIL"] => $lRowOffsetNGActionDetail["ACTION_DETAILS_NAME"]
				];
			}
		}

		return $lArrOffsetNGActionDetail;
	}

	//**************************************************************************
	// 処理名    setTecReqPerson
	// 概要      テクニシャン作業者コンボボックス用のデータを取得し、画面表示用の配列にセットする
	// 引数      画面表示用配列
	// 戻り値    画面表示用配列
	//**************************************************************************
	private function setTecReqPerson($pViewData)
	{
		$lTecReqPersonList     = ["" => ""];  //返却用ビューデータ

		//OFFSET理由/NG理由の検索処理
		$lTecReqPersonList = $this->getTecReqPersonList();

		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrTecReqPersonList" => $lTecReqPersonList
		];

		return $pViewData;
	}

	//**************************************************************************
	// 処理名    getTecReqPersonList
	// 概要      テクニシャン作業者コンボボックス用のデータをSQLで取得
	// 引数      無し
	// 戻り値    取得結果の配列
	//**************************************************************************
	private function getTecReqPersonList()
	{
		$lTblTecReqPerson			= []; //テクニシャン作業者取得結果
		$lRowTecReqPerson			= []; //配列変換時のワーク領域
		$lArrTecReqPerson			= []; //返却するテクニシャン作業者リスト

		$lTblTecReqPerson = DB::select(
		'
		      SELECT TCUR.TECHNICIAN_USER_ID
		            ,TCUR.TECHNICIAN_USER_NAME
		        FROM TTECUSRT AS TCUR
		       WHERE TCUR.DELETE_FLG = "0"
		    ORDER BY TCUR.DISPLAY_ORDER
		'
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblTecReqPerson = (array)$lTblTecReqPerson;

		//空白行を追加
		$lArrTecReqPerson += [
			"" => ""
		];

		//データが存在する
		if ($lTblTecReqPerson != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblTecReqPerson as $lRowTecReqPerson)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowTecReqPerson = (Array)$lRowTecReqPerson;

				$lArrTecReqPerson += [
					$lRowTecReqPerson["TECHNICIAN_USER_ID"] => $lRowTecReqPerson["TECHNICIAN_USER_NAME"]
				];
			}
		}

		return $lArrTecReqPerson;
	}

	//**************************************************************************
	// 処理名    setOffsetNGBunrui
	// 概要      NG/OFFSet分類ボックス用のデータを取得し、画面表示用の配列にセットする
	// 引数      画面表示用配列
	// 戻り値    画面表示用配列
	//**************************************************************************
	private function setOffsetNGBunrui($pViewData)
	{
		$lOffsetNGBunruiList     = ["" => ""];  //返却用ビューデータ

		//OFFSET理由/NG理由の検索処理
		$lOffsetNGBunruiList = $this->getOffsetNGBunruiList();

		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrOffsetNGBunruiList" => $lOffsetNGBunruiList
		];

		return $pViewData;
	}

	//**************************************************************************
	// 処理名    getOffsetNGBunruiList
	// 概要      NG/OFFSet分類コンボボックス用のデータをSQLで取得
	// 引数      無し
	// 戻り値    取得結果の配列
	//**************************************************************************
	private function getOffsetNGBunruiList()
	{
		$lTblOffsetNGBunrui			= []; //NG/OFFSet分類取得結果
		$lRowOffsetNGBunrui			= []; //配列変換時のワーク領域
		$lArrOffsetNGBunrui			= []; //返却するNG/OFFSet分類リスト

		$lTblOffsetNGBunrui = DB::select(
		'
		      SELECT NRBN.OFFSET_NG_BUNRUI_ID
		            ,NRBN.OFFSET_NG_BUNRUI_NAME
		        FROM TNGREBNM AS NRBN
		       WHERE NRBN.DELETE_FLG = "0"
		    ORDER BY NRBN.DISPLAY_ORDER
		'
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblOffsetNGBunrui = (array)$lTblOffsetNGBunrui;

		//空白行を追加
		$lArrOffsetNGBunrui += [
			0 => ""
		];

		//データが存在する
		if ($lTblOffsetNGBunrui != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblOffsetNGBunrui as $lRowOffsetNGBunrui)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowOffsetNGBunrui = (Array)$lRowOffsetNGBunrui;

				$lArrOffsetNGBunrui += [
					$lRowOffsetNGBunrui["OFFSET_NG_BUNRUI_ID"] => $lRowOffsetNGBunrui["OFFSET_NG_BUNRUI_NAME"]
				];
			}
		}

		return $lArrOffsetNGBunrui;
	}

	//**************************************************************************
	// 処理名    getEquipmentFileName
	// 概要      測定器マスタを検索して、呼び出し元に配列で返却する
	// 引数
	// 戻り値    Array
	// 作成者    m-motooka
	// 作成日    2016.08.15
	// 更新履歴  2016.08.15 v0.01 初回作成
	//**************************************************************************
	private function getEquipmentFileName($pEquipmentCode)
	{
		$lTblEquipmentFileName = [];      //検査番号マスタのデータテーブル

			$EquipmentFileName = DB::select('
				    SELECT TSKM.SOKUTEIKI_FILE_NAME
				      FROM TSOKTEIM AS TSKM /*測定器マスタ*/
				     WHERE TSKM.SOKUTEIKI_CD = :EquipmentCode
			',
				[
					"EquipmentCode"    => TRIM((String)$pEquipmentCode),
				]
			);

		return $EquipmentFileName;
	}


}
