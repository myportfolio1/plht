<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Model\tcodemst;
use App\Model\tngrebnm;
use App\Model\tngresnm;
use App\Model\tactionm;
use App\Model\ttecusrt;

use App\Model\treshedt;
use App\Model\tresdett;
use App\Model\tresanal;

use App\Parts\calculate;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;

use Auth;
use DB;
use Log;
use Session;
use Validator;
use Redirect;
use Exception;



// error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);


class BA2010EntryController
extends Controller
{
	//Result Class
	CONST RESULT_CLASS_OK            = "1"; //OK
	CONST RESULT_CLASS_OFFSET        = "2"; //OFFSET
	CONST RESULT_CLASS_NG            = "3"; //NG
	CONST RESULT_CLASS_NO_CHECK      = "4"; //NO CHECK
	CONST RESULT_CLASS_LINE_STOP     = "5"; //LINE STOP

	//Result Input Type
	CONST RESULT_INPUT_TYPE_NUMBER   = "1"; //NUMBER(TEXT)
	CONST RESULT_INPUT_TYPE_OKNG     = "2"; //OK/NG(RadioButton)

	//OFFSET NG Action
	CONST OFFSET_NG_ACTION_RUNNING   = "1"; //RUNNING
	CONST OFFSET_NG_ACTION_STOP_LINE = "2"; //STOP LINE

	//アクション明細の改行する数字
	CONST ACTION_DETAILS_KAIGYO      = 30;

	//Planing Inspection (Normal)
	CONST CONDITION_NORMAL           = "01";
	//No Planing Inspection (Abnormal)
	CONST INSPECTION_TIME_ALL        = "ZZ";
	
	// //XBAR (UCL&LCL)
	// CONST COEFFICIENT_XBAR           = "A2";
	// //RCHAT_UCL
	// CONST COEFFICIENT_RCHAT_UCL      = "D4";
	// //RCHAT_LCL
	// CONST COEFFICIENT_RCHAT_LCL      = "D3";

	CONST CONVDATA_ARG2              = "2";

	//**************************************************************************
	// 処理名    EntryAction
	// 概要      検査結果登録画面の初期表示を行う。
	//           また、Entry等のボタンにより処理を分岐し、
	//           それぞれに対応した処理を行う。
	// 引数      無し
	// 戻り値    無し
	// 作成者    s-miyamoto
	// 作成日    2014.05.23
	// 更新履歴  2014.05.23 v0.01 初回作成
	//           2014.08.26 v1.00 納品で一旦FIX
	//           2017.03.31 v1.01 added For Kohbyo
	//**************************************************************************
	public function EntryAction()
	{   
		$lViewData = [];                     //画面へのデータ移送用
		$pViewData = [];

		$lTblHeaderData = [];
		$lRowHeaderData = [];

		$lTblInspectionNo           = [];    //検査番号マスタのDataTable
		$lRowInspectionNo           = [];    //検査番号マスタのDataRow

		$lInspectionNo              = "";    //検査番号（これは文字型も入る可能性がある。例：1、2、3a、4、5a、6a、7…）

		$lDataRowNo                 = 0;     //現在処理中の検査番号に該当するマスタ行番（0開始）
		$lAdjustmentNo              = 0;     //前後の項目に移動するための調整用番号

		$lEntryFlg                  = false; //Entryで遷移した場合のみTrueになるフラグ

		$lInspectionResultNo        = 0;     //検査実績番号　変更時はパラメータの値を格納
											 //新規登録時は1件目の時初期値（ゼロ）、2件目以降は1件目の登録で採番された値

		$lTblInspectionResultDetail = [];    //検査実績明細DataTable
		$lRowInspectionResultDetail = [];    //検査実績明細DataRow
		$WorkScreensetPtn           = "0";
		$lFilterBtnFlg              = "0";
		$lOnchangeFlg               = "0";   //flag to change combo

		//Motooka added for Kohbyo
		$lCurrentlRowInspectionNo                = [];
		$lTblBeforeCommitInspectionResultDetail1 = [];
		$lRowBeforeCommitInspectionResultDetail1 = [];
		$lTblBeforeCommitInspectionResultDetail2 = [];
		$lRowBeforeCommitInspectionResultDetail2 = [];
		$lTblBeforeCommitInspectionResultDetail3 = [];
		$lRowBeforeCommitInspectionResultDetail3 = [];
		$lTblBeforeCommitInspectionResultAnalyze1 = [];
		$lRowBeforeCommitInspectionResultAnalyze1 = [];
		$lTblCoefficientXbar                     = [];
		$lRowCoefficientXbar                     = [];
		$lTblCoefficientRChartUCL                = [];
		$lRowCoefficientRChartUCL                = [];
		$lTblCoefficientRChartLCL                = [];
		$lRowCoefficientRChartLCL                = [];
		$lTblInspectionResultAnalyze             = [];
		$lRowInspectionResultAnalyze             = [];
		
		$lInspectionPoint                        = 0;
		$lAnalGrp01                              = 0;
		$lAnalGrp02                              = 0;
		$lStdSpecUnder                           = 0;
		$lStdSpecOver                            = 0;
		$lPictureURL                             = "";




		//get parameter from login screen through Session and enter to Array to transport to screen
		$lViewData += [
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		$this->OutputLog01("BA2010 Start");

	try
	{
		if (Session::has('BA1010InspectionResultNo'))
		{
			$lInspectionResultNo = Session::get('BA1010InspectionResultNo');
		}
		else if (Session::has('BA2020InspectionResultNo'))
		{
			$lInspectionResultNo = Session::get('BA2020InspectionResultNo');
		}
		else
		{
			throw new Exception("E997 : Target data does not exist.");
		}

		//InspectionResultNo is not 0
		$lTblHeaderData = $this->getHeaderTblData($lInspectionResultNo);

		if ($lTblHeaderData != null)
		{
			$lRowHeaderData = (array)$lTblHeaderData[0];
		}
		else
		{
			throw new Exception("E997 : Target data does not exist.");
		}

		//program first throw
		if (is_null(Session::get('BA2010InspectionNoData')))
		{
			$lTblInspectionNo = $this->getInspectionNoList($lInspectionResultNo);
			Session::put('BA2010InspectionNoData', $lTblInspectionNo);
		}
		else
		{
			$lTblInspectionNo = Session::get('BA2010InspectionNoData');
		}

		//set list combo
		$lViewData = $this->setJudgeReason($lViewData);
		$lViewData = $this->setOffsetNGBunrui($lViewData);
		$lViewData = $this->setOffsetNGActionDetail($lViewData);
		$lViewData = $this->setTecReqPerson($lViewData);
		$lViewData = $this->setInspectionToolClassList($lViewData, $lRowHeaderData);

		//get line 0 data in Inspection No.master
		$lRowInspectionNo = (Array)$lTblInspectionNo[0];
		//Inspection No. = data of line 0
		$lInspectionNo = $lRowInspectionNo["INSPECTION_NO"];


		if (Input::has('btnPageMove'))                      //PageMovebutton
		{ 
			$this->OutputLog02("BA2010 PageMove Btn Click");

            //Inspection No.
			$lValidator = Validator::make(
				array('txtInspectionNo' => Input::get('txtInspectionNo')),
				array('txtInspectionNo' => array('required'))
			);
			if ($lValidator->fails())
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
				throw new Exception("E025 : Enter Inspection No. .");
			}

			$lInspectionNo  = (String)Input::get('txtInspectionNo','');
		}
		elseif (Input::has('btnPrevPage'))                  //<<button
		{
			$this->OutputLog02("BA2010 Prev.Page Btn Click");

			//follow value of Hidden exclusive of PageMove
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}

			//set parameter to decrease line number
			$lAdjustmentNo = -1;
		}
		elseif (Input::has('btnNextPage'))                  //>>button
		{
			$this->OutputLog02("BA2010 Next.Page Btn Click");

			//follow value of Hidden exclusive of PageMove
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}

			//set parameter for line number
			$lAdjustmentNo = 1;
		}
		elseif (Input::has('btnReturn'))                    //Return button
		{
			$this->OutputLog02("BA2010 Rtn Btn Click");

			Session::forget('BA2010InspectionNoData');
			Session::forget('BA2010InspectionToolClassDropdownListData');

			// //in case screen is transported from Modify or Entry finished
			// if (Session::has('BA1010InspectionResultNo'))
			// {
			// 	return Redirect::route("user/list");
			// }
			// else
			// {
			// 	return Redirect::route("user/preentry");
			// }

			//Laravel VerUp From 4.2 To 5.6
			//return Redirect::route("user/list");
			return redirect()->route('user.list');
		}
		elseif (Input::has('btnEntry'))                     //Entry button
		{
			$this->OutputLog03("BA2010 Entry Btn Click");

			//follow value of Hidden exclusive of PageMove
			//PageMove以外の場合（<<、>>、Entry）は、画面入力値ではなく、Hiddenで保持しているInspection Noに対する処理をする必要があるため、Hidden値に従う
			if (Input::has('hidInspectionNo'))
			{
				$lInspectionNo  = (String)Input::get('hidInspectionNo');
			}

			//error check
			$this->ErrorCheck1();
			$this->ErrorCheck2();
  
			//flag to find data
			$lDataFindFlg = false;
			
			foreach ($lTblInspectionNo As $lRowInspectionNo)
			{
				$lCurrentlRowInspectionNo = (Array)$lRowInspectionNo;

				if ($lDataFindFlg == false)
				{
					if($lInspectionNo == $lCurrentlRowInspectionNo["INSPECTION_NO"])
					{
						$lInspectionPoint          = $lCurrentlRowInspectionNo["INSPECTION_POINT"];
						$lAnalGrp01                = $lCurrentlRowInspectionNo["ANALYTICAL_GRP_01"];
						$lAnalGrp02                = $lCurrentlRowInspectionNo["ANALYTICAL_GRP_02"];

						$lPictureURL               = $lCurrentlRowInspectionNo["PICTURE_URL"];
						$lStdSpecUnder             = number_format($lCurrentlRowInspectionNo["THRESHOLD_NG_UNDER"], 4);
						$lStdSpecOver              = number_format($lCurrentlRowInspectionNo["THRESHOLD_NG_OVER"], 4);
						
						$lDataFindFlg = true;
						break;
					}
				}
			}

			$lCalculateArrayData = []; 
			$lCalculateArrayData += [
				"InspectionResultValue"  => Input::get('txtInspectionResult'),
				"InspectionResultNo"     => $lInspectionResultNo,
				"InspectionNo"           => $lInspectionNo,
				"InspectionPoint"        => $lInspectionPoint,
				"AnalGrp01"              => $lAnalGrp01,
				"AnalGrp02"              => $lAnalGrp02,
				"StdSpecUnder"           => $lStdSpecUnder,
				"StdSpecOver"            => $lStdSpecOver,
			];

			//Calculate
			$objcalculate = new calculate;
			$lAfterCalculateData = $objcalculate->getCalculate($lCalculateArrayData, Input::get('hidInspectionResultInputType'), Input::get('rdiResultClass',''));

			if (is_null($lAfterCalculateData))
			{
				throw new Exception("E997XX : Calc No Target data does not exist.");
			}

			//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
			$lJudgeReason = "";
			if ($lAfterCalculateData["JudgeMinValue"] == 0 && $lAfterCalculateData["JudgeMaxValue"] == 0)
			{
				//Nothing
			}
			else
			{
				if (($lCalculateArrayData["StdSpecUnder"] > $lAfterCalculateData["JudgeMinValue"]) or ($lCalculateArrayData["StdSpecUnder"] > $lAfterCalculateData["JudgeMaxValue"]) or ($lCalculateArrayData["StdSpecOver"] < $lAfterCalculateData["JudgeMinValue"]) or ($lCalculateArrayData["StdSpecOver"] < $lAfterCalculateData["JudgeMaxValue"]))
				{
					//Judge Reason is value of Input
					$lJudgeReason = Input::get('cmbJudgeReason');
				}
				else
				{
					//Nothing
				}
			}

			//error check
			$this->ErrorCheck3($lCalculateArrayData, $lAfterCalculateData);

			//Entry flag ON
			$lEntryFlg = true;

			//30文字以上入っている場合は改行コードを挿入
			$lActionDetails = wordwrap(Input::get('cmbActionDetails'), self::ACTION_DETAILS_KAIGYO, PHP_EOL, true);

			//日付を英語書式から日本語書式に変換（英語書式が入ってくる前提で、日付型に変換してからフォーマット）
			// $lInspectionDate = date_format(date_create($lViewData["InspectionDate"]), 'Y/m/d');
			$lInspectionDate = date_format(date_create($lRowHeaderData["INSPECTION_YMD"]), 'Y/m/d');


			$tablemodeltreshedt = new treshedt;
			$tablemodeltreshedt->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
			// $tablemodeltreshedt->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
			$tablemodeltreshedt->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
			// $tablemodeltreshedt->DATA_REV = Session::get('BA1010ModifyDataRev',0);

			Log::write('info', '162');
			$tablemodeltresdett = new tresdett;
			$tablemodeltresdett->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
			$tablemodeltresdett->INSPECTION_NO = $lCalculateArrayData["InspectionNo"];
			$tablemodeltresdett->INSPECTION_RESULT_VALUE = $lAfterCalculateData["InspectionResultValue"];
			$tablemodeltresdett->INSPECTION_RESULT_TYPE = Input::get('rdiResultClass' ,'');
			// $tablemodeltresdett->OK_QTY = 
			// $tablemodeltresdett->NG_QTY = 
			$tablemodeltresdett->OFFSET_NG_BUNRUI_ID = Input::get('cmbOffsetNGBunrui');
			$tablemodeltresdett->OFFSET_NG_REASON_ID = Input::get('cmbOffsetNGReason');
			$tablemodeltresdett->OFFSET_NG_REASON = "";
			$tablemodeltresdett->OFFSET_NG_ACTION = Input::get('rdiOffsetNGAction','');
			$tablemodeltresdett->OFFSET_NG_ACTION_DTL_INTERNAL = Input::get('chkOffsetNGActionDetailInternal','0');
			$tablemodeltresdett->OFFSET_NG_ACTION_DTL_REQUEST = Input::get('chkOffsetNGActionDetailRequest','0');
			$tablemodeltresdett->OFFSET_NG_ACTION_DTL_TSR = Input::get('chkOffsetNGActionDetailTSR','0');
			$tablemodeltresdett->OFFSET_NG_ACTION_DTL_SORTING = Input::get('chkOffsetNGActionDetailSorting','0');
			$tablemodeltresdett->OFFSET_NG_ACTION_DTL_NOACTION = Input::get('chkOffsetNGActionDetailNoAction','0');
			$tablemodeltresdett->ACTION_DETAILS_ID = $lActionDetails;
			$tablemodeltresdett->ACTION_DETAILS_TR_NO_TSR_NO = "";
			$tablemodeltresdett->TR_NO = Input::get('txtTecReqNo');
			$tablemodeltresdett->TECHNICIAN_USER_ID = Input::get('cmbTecReqPerson');
			$tablemodeltresdett->TSR_NO = Input::get('txtTSRNo');
			// $tablemodeltresdett->INSPECTION_POINT = 
			// $tablemodeltresdett->ANALYTICAL_GRP_01 = 
			// $tablemodeltresdett->ANALYTICAL_GRP_02 = 
			// $tablemodeltresdett->SAMPLE_NO = 
			$tablemodeltresdett->ANALYTICAL_GRP_01_MEMO = Input::get('txtCavityMemo');
			// $tablemodeltresdett->INSERT_USER_ID = 
			// $tablemodeltresdett->DISPLAY_ORDER = 
			// $tablemodeltresdett->INSERT_YMDHMS = 
			// $tablemodeltresdett->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
			$tablemodeltresdett->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
			// $tablemodeltresdett->UPDATE_YMDHMS = 
			// $tablemodeltresdett->DATA_REV = 

			$tablemodeltresanal = new tresanal;
			$tablemodeltresanal->INSPECTION_SHEET_NO = $lRowHeaderData["INSPECTION_SHEET_NO"];
			$tablemodeltresanal->REV_NO = $lRowHeaderData["REV_NO"];
			$tablemodeltresanal->INSPECTION_POINT = $lCalculateArrayData["InspectionPoint"];
			$tablemodeltresanal->ANALYTICAL_GRP_01 = $lCalculateArrayData["AnalGrp01"];
			$tablemodeltresanal->ANALYTICAL_GRP_02 = $lCalculateArrayData["AnalGrp02"];
			$tablemodeltresanal->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
			$tablemodeltresanal->PROCESS_ID = $lRowHeaderData["PROCESS_ID"];
			$tablemodeltresanal->INSPECTION_YMD = $lInspectionDate;
			$tablemodeltresanal->INSPECTION_TIME_ID = $lRowHeaderData["INSPECTION_TIME_ID"];
			$tablemodeltresanal->CONDITION_CD = $lRowHeaderData["CONDITION_CD"];
			$tablemodeltresanal->MOLD_NO = $lRowHeaderData["MOLD_NO"];
			$tablemodeltresanal->TEAM_ID = $lRowHeaderData["TEAM_ID"];
			$tablemodeltresanal->STD_MIN_SPEC = $lCalculateArrayData["StdSpecUnder"];
			$tablemodeltresanal->STD_MAX_SPEC = $lCalculateArrayData["StdSpecOver"];
			$tablemodeltresanal->PICTURE_URL = $lPictureURL;
			$tablemodeltresanal->CALC_NUM = $lAfterCalculateData["CalcNam"];
			$tablemodeltresanal->MAX_VALUE = $lAfterCalculateData["MaxValue"];
			$tablemodeltresanal->MIN_VALUE = $lAfterCalculateData["MinValue"];
			$tablemodeltresanal->AVERAGE_VALUE = $lAfterCalculateData["AverageValue"];
			$tablemodeltresanal->RANGE_VALUE = $lAfterCalculateData["RangeValue"];
			$tablemodeltresanal->STDEV = $lAfterCalculateData["StdevValue"];
			$tablemodeltresanal->CPK = $lAfterCalculateData["CpkValue"];
			$tablemodeltresanal->XBAR_UCL = $lAfterCalculateData["XbarUclValue"];
			$tablemodeltresanal->XBAR_LCL = $lAfterCalculateData["XbarLclValue"];
			$tablemodeltresanal->RCHART_UCL = $lAfterCalculateData["RchartUclValue"];
			$tablemodeltresanal->RCHART_LCL = $lAfterCalculateData["RchartLclValue"];
			$tablemodeltresanal->JUDGE_REASON = $lJudgeReason;
			// $tablemodeltresanal->INSERT_YMDHMS = 
			// $tablemodeltresanal->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
			$tablemodeltresanal->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
			// $tablemodeltresanal->UPDATE_YMDHMS = 
			// $tablemodeltresanal->DATA_REV = 

			// //register/update
			$this->insertAndUpdateInspectionResult($lAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett, $tablemodeltresanal);

			//add Revision No. in header
			Session::put('BA1010ModifyDataRev', Session::get('BA1010ModifyDataRev',0) + 1);

			//set parameter for line number
			$lAdjustmentNo = 1;
		}
		elseif (Input::has('btnFilter'))                     //Filter button
		{
			Log::write('info', 'BA2010 Filter Button Click.');

			//検査番号マスタデータのうち、フィルタに該当するものを削除する処理
			//画面で「ALL」以外が選択されている場合のみ実行
			//delete corresponding to filter in Inspection No. master data
			//selected exception of ALL
			if ((String)Input::get('cmbInspectonToolClass') != "0")
			{
				Log::write('info', '1001');
				$lDataCount      = 0;    //line counter
				$lNeedlessNoList = [];   //unnecessary line number list
				$lWorkList       = [];   //work list for line number

				foreach ($lTblInspectionNo as $lRowInspectionNo)
				{
					Log::write('info', '1002');
					$lRowInspectionNo = (Array)$lRowInspectionNo;

					if ($lRowInspectionNo["SOKUTEIKI_CD"] != (String)Input::get('cmbInspectonToolClass'))
					{
						Log::write('info', '1003');
						array_push($lNeedlessNoList, $lDataCount);
					}

					$lDataCount += 1;
				}
Log::write('info', '1004');
				foreach ($lNeedlessNoList as $lDeleteRowNum)
				{
					Log::write('info', '1005');
					//delete from Inspection No. master data
					unset($lTblInspectionNo[$lDeleteRowNum]);
				}
Log::write('info', '1006');
				//残ったデータを0行目から順に入れ直す必要がある。
				//reenter data from 0 line
				foreach ($lTblInspectionNo as $lRowInspectionNo)
				{
					Log::write('info', '1007');
					array_push($lWorkList, $lRowInspectionNo);
				}
Log::write('info', '1008');
				//検査番号マスタテーブルを一度クリアする
				//clear Inspection No. master table
				$lTblInspectionNo = [];

				//ワークで番号を振り直したデータを元に戻す
				//replace data in work
				foreach ($lWorkList as $lWorkRow)
				{
					Log::write('info', '1009');
					array_push($lTblInspectionNo, $lWorkRow);
				}
			}

			$lFilterBtnFlg = "1";

			//get line 0 data in Inspection No.master
			$lRowInspectionNo = (Array)$lTblInspectionNo[0];
			//Inspection No. = data of line 0
			$lInspectionNo = $lRowInspectionNo["INSPECTION_NO"];
		}
		else                                                //when transpotion from other screen or Load
		{
			Log::write('info', 'BA2010 Else');

			Log::write('info', '164',
				[
					"hidOffsetNGBunrui"  => Input::get('hidOffsetNGBunrui'),
					"cmbOffsetNGBunrui"  => Input::get('cmbOffsetNGBunrui'),
				]
			);

			//in case value of combo storing in hiddn before Submit is changed
			//flag available
			//store InspectionNo.
			if (Input::get('hidOffsetNGBunrui') != Input::get('cmbOffsetNGBunrui'))
			{
				if (Input::has('hidInspectionNo'))
				{
					$lInspectionNo  = (String)Input::get('hidInspectionNo');
				}

				$lOnchangeFlg = "1";
			}
		}

	}catch(Exception $e) 
		{
			$lViewData["errors"] = new MessageBag([
				"error" => $e->getMessage()
			]);

			Log::write('info', 'ExceptionError',
				[
					"error" => $e->getMessage(),
				]
			);
		}







		Log::write('info', 'Start mustpass');
		//■must pass

		//flag to find data
		$lDataFindFlg = false;

		//Fetch data and get count to specify master data
		foreach ($lTblInspectionNo as $lRowInspectionNo)
		{
			//Comparison  If same value is return "1". Not same is return "0".
			if (strcmp($lInspectionNo, $lRowInspectionNo->INSPECTION_NO) == 0)
			{
				Log::write('info', 'compear',
					[
						"InspectionNo"			=> $lInspectionNo,
						"RowInspectionNo"		=> $lRowInspectionNo,
					]
				);

				//break FOR because Inspection No match
				$lDataFindFlg = true;
				break;
			}

			//count up line number of data
			$lDataRowNo += 1;
		}

		//in case no data, error
		if ($lDataFindFlg == false)
		{
			//return to data of line 1
			$lDataRowNo = 0;
			//error message
			$lViewData["errors"] = new MessageBag([
				"error" => "E996 : Since Inspection No. does not exist, the first data is shown."
			]);
		}

		Log::write('info', 'mustpass1',
			[
				"DataRowNo"			    => $lDataRowNo,
				"countTblInspectionNo"	=> count($lTblInspectionNo),
				"EntryFlg"			    => $lEntryFlg,
				"DataFindFlg"			=> $lDataFindFlg,
			]
		);

		//move data line infront and back in case of putting "<<",">>","Entry"
		//in case the last data is push Entry
		//need to push Entry button in condition
		if (($lDataRowNo == count($lTblInspectionNo) -1) and ($lEntryFlg == true))
		{
			Log::write('info', '0000');
			$lDataRowNo = 0;
		}
		else
		{
			Log::write('info', '0001');
			$lDataRowNo = $lDataRowNo + $lAdjustmentNo;
		}

		//現在の検査番号に該当する$lTblInspectionNoの行番号を特定し、該当行のデータを画面向けの変数配列に格納
		//specify line No. of $lTblInspectionNo corresponding to present inspection No. and store in Array for screen
		$lRowInspectionNo = (Array)$lTblInspectionNo[$lDataRowNo];

		Log::write('info', 'mustpass2',
			[
				"DataRowNo"			    => $lDataRowNo,
				"RowInspectionNo"		=> $lRowInspectionNo,
			]
		);

		//set viewdata
		$lViewData  = $this->setviewDataScreenHeader($lViewData, $lRowHeaderData);
		$lViewData  = $this->setviewDataScreendetail($lViewData, $lRowInspectionNo);
		$lViewData  = $this->setviewDataPrevNextBtn($lViewData, $lTblInspectionNo, $lDataRowNo);

		Log::write('info', '103');
		if (((array_key_exists("errors", $lViewData) == true) and ($lDataFindFlg == True)) or ($lOnchangeFlg == "1"))
		{
			Log::write('info', '103-1');
			$lViewData  = $this->setviewDataScreenResult2($lViewData, $lRowInspectionNo);
		}
		else
		{
			Log::write('info', '103-2');
			$lViewData  = $this->setviewDataScreenResult1($lViewData, $lRowInspectionNo, $lFilterBtnFlg, $lOnchangeFlg);
		}
		Log::write('info', '104');

		$lViewData  = $this->setviewDataScreenAnalResult($lViewData, $lRowInspectionNo);

		// echo "<pre>";
		// print_r($lViewData);
		// echo "</pre>";

		//display screen
		//Laravel VerUp From 4.2 To 5.5
		// return View::make("user/entry", $lViewData);
		return View("user.entry", $lViewData);
	}



	//*****************************************************************************************************************
	//*****************************************************************************************************************
	//*****************************************************************************************************************

	//**************************************************************************
	// process            getHeaderTblData
	// overview           
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getHeaderTblData($pInspectionResultNo)
	{
		$lTblHeaderTblData = [];

			$lTblHeaderTblData = DB::select('
					SELECT HEAD.INSPECTION_SHEET_NO
							,SHEE.INSPECTION_SHEET_NAME
							,HEAD.REV_NO
							,HEAD.PROCESS_ID
							,PROCESS.CODE_NAME AS PROCESS_NAME
							,HEAD.MACHINE_NO
							,MACH.MACHINE_NAME
							,HEAD.MOLD_NO
							,HEAD.MATERIAL_NAME
							,HEAD.MATERIAL_SIZE
							,HEAD.HEAT_TREATMENT_TYPE
							,HEAD.PLATING_KIND
							,HEAD.COLOR_ID
							,COLOR.CODE_NAME AS COLOR_NAME
							,HEAD.INSERT_USER_ID
							,USER.USER_NAME
							,USER.TEAM_ID
							,TEAM.CODE_NAME AS TEAM_NAME
							,HEAD.INSPECTION_YMD
							,HEAD.CONDITION_CD
							,COND.CODE_NAME AS CONDITION_NAME
							,HEAD.INSPECTION_TIME_ID
							,TIME.INSPECTION_TIME_NAME
							,HEAD.MATERIAL_LOT_NO
							,HEAD.CERTIFICATE_NO
							,HEAD.PO_NO
							,HEAD.INVOICE_NO
							,HEAD.QUANTITY_COIL
							,HEAD.QUANTITY_WEIGHT
							,HEAD.LOT_NO
							,HEAD.PRODUCTION_YMD
							,SHEE.CUSTOMER_ID
							,CUST.CUSTOMER_NAME
							,SHEE.ITEM_NO
							,ITEM.ITEM_NAME
							,SHEE.MODEL_NAME
							,SHEE.DRAWING_NO
							,SHEE.ISO_KANRI_NO
							,ITEM.PRODUCT_WEIGHT
						FROM TRESHEDT AS HEAD
			      INNER JOIN TCODEMST AS COND
			              ON COND.CODE_CLASS           = "001"
			             AND HEAD.CONDITION_CD         = COND.CODE_ORDER
			      INNER JOIN TISHEETM AS SHEE
			              ON HEAD.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
			             AND HEAD.REV_NO               = SHEE.REV_NO 
			             AND SHEE.DELETE_FLG           = "0"
			      INNER JOIN TCODEMST AS PROCESS
			              ON PROCESS.CODE_CLASS        = "002"
			             AND HEAD.PROCESS_ID           = PROCESS.CODE_ORDER
			 LEFT OUTER JOIN TITEMMST AS ITEM
			              ON SHEE.ITEM_NO              = ITEM.ITEM_NO
			             AND ITEM.DELETE_FLG           = "0"
			 LEFT OUTER JOIN TCODEMST AS COLOR
			              ON COLOR.CODE_CLASS          = "006"
			             AND ITEM.COLOR_ID             = COLOR.CODE_ORDER
			 LEFT OUTER JOIN TCUSTOMM AS CUST
			              ON SHEE.CUSTOMER_ID          = CUST.CUSTOMER_ID
			             AND CUST.DELETE_FLG           = "0"
			      INNER JOIN TMACHINM AS MACH
			              ON HEAD.MACHINE_NO           = MACH.MACHINE_NO
			      INNER JOIN TINSPTIM AS TIME
			              ON HEAD.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
			      INNER JOIN TUSERMST AS USER
			              ON HEAD.INSERT_USER_ID       = USER.USER_ID
			 LEFT OUTER JOIN TCODEMST AS TEAM
			              ON TEAM.CODE_CLASS           = "004"
			             AND USER.TEAM_ID              = TEAM.CODE_ORDER
					   WHERE HEAD.INSPECTION_RESULT_NO = :InspectionResultNo
			',
				[
					"InspectionResultNo"	=> TRIM((String)$pInspectionResultNo),
				]
			);

		return $lTblHeaderTblData;
	}

	//**************************************************************************
	// process            getInspectionNoList
	// overview           
	// argument           
	// return value       Array
	// Remarks            
	//**************************************************************************
	private function getInspectionNoList($pInspectionResultNo)
	{
		$lTblInspectionNo = [];

			$lTblInspectionNo = DB::select('
					SELECT DETL.INSPECTION_RESULT_NO
							,DETL.INSPECTION_NO
							,DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
							,DETL.SAMPLE_NO
							,DETL.ANALYTICAL_GRP_01_MEMO
							,ISNO.INSPECTION_SHEET_NO
							,ISNO.REV_NO
							,ISNO.INSPECTION_POINT
							,ISNO.INSPECTION_ITEM
							,TSKM.TOOL_CD
							,ISNO.UNIT_TYPE_ID
							,COALESCE(UNIT.CODE_NAME,"") AS UNIT_TYPE_NAME
							,ISNO.PICTURE_URL
							,ISNO.XR_CHART_NO_POKAYOKE_NO
							,ISNO.REFER_TO_DOCUMENT_FREQUENCY
							,ISNO.SOKUTEIKI_CD
							,COALESCE(TSKM.SOKUTEIKI_NAME,"") AS INSPECTION_TOOL_CLASS_NAME
							,TSKM.INSPECTION_RESULT_INPUT_TYPE
							,ISNO.THRESHOLD_NG_UNDER
							,ISNO.THRESHOLD_OFFSET_UNDER
							,ISNO.REFERENCE_VALUE
							,ISNO.THRESHOLD_OFFSET_OVER
							,ISNO.THRESHOLD_NG_OVER
							,ISNO.SPEC_WRITE_FORMAT
							,ISNO.NG_UNDER_WRITE_FORMAT
							,ISNO.NG_OVER_WRITE_FORMAT
							,ISNO.GRP01_ID
							,ISNO.GRP02_ID
							,ISNO.SAMPLE_QTY
						FROM TRESHEDT AS HEAD
				  INNER JOIN TRESDETT AS DETL
						  ON DETL.INSPECTION_RESULT_NO   = HEAD.INSPECTION_RESULT_NO	
				  INNER JOIN TINSPNOM AS ISNO
						  ON ISNO.INSPECTION_SHEET_NO    = HEAD.INSPECTION_SHEET_NO
						 AND ISNO.REV_NO                 = HEAD.REV_NO
						 AND ISNO.INSPECTION_POINT       = DETL.INSPECTION_POINT
						 AND ISNO.DELETE_FLG             = "0"
			 LEFT OUTER JOIN TCODEMST AS UNIT
						  ON UNIT.CODE_CLASS             = "007"
						 AND ISNO.UNIT_TYPE_ID           = UNIT.CODE_ORDER
			 LEFT OUTER JOIN TSOKTEIM AS TSKM
						  ON TSKM.SOKUTEIKI_CD           = ISNO.SOKUTEIKI_CD
						 AND TSKM.DELETE_FLG             = "0"
					   WHERE HEAD.INSPECTION_RESULT_NO   = :InspectionResultNo
					ORDER BY DETL.DISPLAY_ORDER
							,DETL.INSPECTION_NO
			',
				[
					"InspectionResultNo"	=> TRIM((String)$pInspectionResultNo),
				]
			);

		return $lTblInspectionNo;
	}

	//**************************************************************************
	// process         setJudgeReason
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function setJudgeReason($pViewData)
	{
		$lArrJudgeReasonList = ["" => ""];

		$objtcodemst = new tcodemst;
		$lArrJudgeReasonList = $objtcodemst->getJudgeReasonList();

		$pViewData += [
			"arrJudgeReasonList" => $lArrJudgeReasonList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setOffsetNGBunrui
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function setOffsetNGBunrui($pViewData)
	{
		$lArrOffsetNGBunruiList = ["" => ""];

		$objtngrebnm = new tngrebnm;
		$lArrOffsetNGBunruiList = $objtngrebnm->getOffsetNGBunruiList();

		$pViewData += [
			"arrOffsetNGBunruiList" => $lArrOffsetNGBunruiList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setOffsetNGReasonList
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function setOffsetNGReasonList($pViewData, $pPtn)
	{
		$lArrOffsetNGReasonList = ["" => ""];

		$WorkNGBunrui = 0;

		if ($pPtn == "1")
		{
			$WorkNGBunrui = $pViewData["OffsetNGBunrui"];
		}
		else
		{
			$WorkNGBunrui = (String)Input::get('cmbOffsetNGBunrui',0);
		}
		

		$objtngresnm = new tngresnm;
		$lArrOffsetNGReasonList = $objtngresnm->getOffsetNGReasonList((string)($WorkNGBunrui));


		$pViewData += [
			"arrOffsetNGReasonList" => $lArrOffsetNGReasonList
		];


		return $pViewData;
	}

	//**************************************************************************
	// process         setOffsetNGActionDetail
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function setOffsetNGActionDetail($pViewData)
	{
		$lArrOffsetNGActionDetailList = ["" => ""];

		$objtactionm = new tactionm;
		$lArrOffsetNGActionDetailList = $objtactionm->getOffsetNGActionDetailList();

		$pViewData += [
			"arrActionDetailsList" => $lArrOffsetNGActionDetailList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setTecReqPerson
	// overview        
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function setTecReqPerson($pViewData)
	{
		$lArrTecReqPersonlList = ["" => ""];

		$objttecusrt = new ttecusrt;
		$lArrTecReqPersonlList = $objttecusrt->getTecReqPersonList();

		$pViewData += [
			"arrTecReqPersonList" => $lArrTecReqPersonlList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process            setInspectionToolClassList
	// overview           
	// argument           
	// return value       Array
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function setInspectionToolClassList($pViewData, $pRowHeaderData)
	{
		$lArrDataListInspectionToolClass     = ["" => ""];

		if (is_null(Session::get('BA2010InspectionToolClassDropdownListData')))
		{
			$lArrDataListInspectionToolClass = $this->getInspectionToolClassList($pRowHeaderData["INSPECTION_SHEET_NO"], $pRowHeaderData["REV_NO"]);

			Session::put('BA2010InspectionToolClassDropdownListData', $lArrDataListInspectionToolClass);
		}
		else
		{
			$lArrDataListInspectionToolClass = Session::get('BA2010InspectionToolClassDropdownListData');
		}

		$pViewData += [
			"arrDataListInspectionToolClass" => $lArrDataListInspectionToolClass
		];

		return $pViewData;
	}

	//**************************************************************************
	// process            getInspectionToolClassList
	// overview           Select Table
	// argument           
	// return value       Array
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getInspectionToolClassList($pInspectionSheetNo, $pRevisionNo)
	{
		$lTblInspectionToolClass         = [];
		$lRowInspectionToolClass         = [];
		$lArrDataListInspectionToolClass = [];

		$lTblInspectionToolClass = DB::select('
		     SELECT DISTINCT ISNO.SOKUTEIKI_CD
				            ,TSKM.SOKUTEIKI_NAME AS INSPECTION_TOOL_CLASS_NAME
				        FROM TINSPNOM AS ISNO
				  INNER JOIN TSOKTEIM AS TSKM
				          ON ISNO.SOKUTEIKI_CD        = TSKM.SOKUTEIKI_CD
				       WHERE ISNO.INSPECTION_SHEET_NO = :InspectionSheetNo
				         AND ISNO.REV_NO              = :RevisionNo
				         AND ISNO.DELETE_FLG          = "0"
				         AND TSKM.DELETE_FLG          = "0"
				    ORDER BY TSKM.DISPLAY_ORDER
		',
				[
					"InspectionSheetNo" => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"        => TRIM((String)$pRevisionNo),
				]
		);

		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblInspectionToolClass = (array)$lTblInspectionToolClass;

		//ALLだけ固定で格納
		$lArrDataListInspectionToolClass += [ "0" => "ALL" ];

		if ($lTblInspectionToolClass != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblInspectionToolClass as $lRowInspectionToolClass)
			{
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowInspectionToolClass = (Array)$lRowInspectionToolClass;

				$lArrDataListInspectionToolClass += [
					$lRowInspectionToolClass["SOKUTEIKI_CD"] => $lRowInspectionToolClass["INSPECTION_TOOL_CLASS_NAME"]
				];
			}
		}

		return $lArrDataListInspectionToolClass;
	}

	//**************************************************************************
	// process            getInspectionResultDetailData
	// overview           select Inspection Result Detail (transaction table)
	// argument           
	// return value       Array
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultDetailData($pInspectionResultNo, $pInspectionNo)
	{
		$lTblInspectionResultDetail = []; 

			$lTblInspectionResultDetail = DB::select('
				SELECT REDE.INSPECTION_RESULT_VALUE
						,REDE.INSPECTION_RESULT_TYPE
						,REDE.OFFSET_NG_BUNRUI_ID
						,REDE.OFFSET_NG_REASON_ID
						,REDE.OFFSET_NG_REASON
						,REDE.OFFSET_NG_ACTION
						,REDE.OFFSET_NG_ACTION_DTL_INTERNAL
						,REDE.OFFSET_NG_ACTION_DTL_REQUEST
						,REDE.OFFSET_NG_ACTION_DTL_TSR
						,REDE.OFFSET_NG_ACTION_DTL_SORTING
						,REDE.OFFSET_NG_ACTION_DTL_NOACTION
						,REDE.ACTION_DETAILS_ID
						,REDE.ACTION_DETAILS_TR_NO_TSR_NO
						,REDE.TR_NO
						,REDE.TECHNICIAN_USER_ID
						,REDE.TSR_NO
						,REDE.INSPECTION_POINT
				 		,REDE.ANALYTICAL_GRP_01
						,REDE.ANALYTICAL_GRP_02
						,REDE.SAMPLE_NO
						,REDE.DISPLAY_ORDER
						,REDE.DATA_REV
						,IFNULL(NRSN.OFFSET_NG_BUNRUI_ID,0) AS OFFSET_NG_BUNRUI_ID
					FROM TRESDETT AS REDE
		 LEFT OUTER JOIN TNGRESNM AS NRSN
					  ON NRSN.OFFSET_NG_BUNRUI_ID  = REDE.OFFSET_NG_BUNRUI_ID
					 AND NRSN.OFFSET_NG_REASON_ID  = REDE.OFFSET_NG_REASON_ID
				   WHERE REDE.INSPECTION_RESULT_NO = :InspectionResultNo
					 AND REDE.INSPECTION_NO        = :InspectionNo
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"InspectionNo"       => TRIM((String)$pInspectionNo),
				]
			);

		return $lTblInspectionResultDetail;
	}

	//**************************************************************************
	// process            getInspectionResultAnalyzeData
	// overview           select Inspection Result Analyze (transaction table)
	// argument           
	// return value       Array
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultAnalyzeData($pViewData ,$pRowInspectionNo)
	{
		$lTblInspectionResultAnalyze = [];

			$lTblInspectionResultAnalyze = DB::select('
				SELECT ANA.AVERAGE_VALUE
						,ANA.MAX_VALUE
						,ANA.MIN_VALUE
						,ANA.RANGE_VALUE
						,ANA.STDEV
						,ANA.JUDGE_REASON
				  FROM TRESANAL AS ANA
				 WHERE ANA.INSPECTION_POINT     = IF(:InspectionPoint1 <> "", :InspectionPoint2, ANA.INSPECTION_POINT)
				   AND ANA.ANALYTICAL_GRP_01    = IF(:AnalGrp011 <> "", :AnalGrp012, ANA.ANALYTICAL_GRP_01)
				   AND ANA.ANALYTICAL_GRP_02    = IF(:AnalGrp021 <> "", :AnalGrp022, ANA.ANALYTICAL_GRP_02)
				   AND ANA.INSPECTION_RESULT_NO = :InspectionResultNo
			',
				[
					"InspectionPoint1"   => TRIM((String)$pRowInspectionNo["INSPECTION_POINT"]),
					"InspectionPoint2"   => TRIM((String)$pRowInspectionNo["INSPECTION_POINT"]),
					"AnalGrp011"         => TRIM((String)$pRowInspectionNo["ANALYTICAL_GRP_01"]),
					"AnalGrp012"         => TRIM((String)$pRowInspectionNo["ANALYTICAL_GRP_01"]),
					"AnalGrp021"         => TRIM((String)$pRowInspectionNo["ANALYTICAL_GRP_02"]),
					"AnalGrp022"         => TRIM((String)$pRowInspectionNo["ANALYTICAL_GRP_02"]),
					"InspectionResultNo" => TRIM((String)$pRowInspectionNo["INSPECTION_RESULT_NO"]),
				]
			);

		return $lTblInspectionResultAnalyze;
	}


	// **************************************************************************
	// process            getCalculate
	// overview           
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	// **************************************************************************
// 	private function getCalculate($pCalculateArray)
// 	{
// 		Log::write('info', 'BA2010 start Calculate');

// 		$lCalcFlg                                = false;
// 		$lBeforeCalcFlg                          = false;
// 		$lWorkInspectionResult                   = 0; 
// 		$lWorkMaxValue                           = 0;
// 		$lWorkMinValue                           = 0;
// 		$lCalcNam                                = 0;
// 		$lCodeNum                                = 0;
// 		$lMaxValue                               = 0;
// 		$lMinValue                               = 0;
// 		$lAverageValue                           = 0;
// 		$lRangeValue                             = 0;
// 		$lStdev                                  = 0;
// 		$lWorkCpkStdSpecOver                     = 0;
// 		$lWorkCpkStdSpecUnder                    = 0;
// 		$lCpk                                    = 0;
// 		$lXbarUcl                                = 0;
// 		$lXbarLcl                                = 0;
// 		$lRchartUcl                              = 0;
// 		$lRchartLcl                              = 0;
// 		$lJudgeReason                            = "";
// 		$lJudgeMin                               = 0;
// 		$lJudgeMax                               = 0;

// 		$lWorkInspectionResult = number_format(Input::get('txtInspectionResult'),4);

// 		$pArray= [];


// 		$lTblBeforeCommitInspectionResultDetail1 = $this->getBeforeCommitInspectionResultDetailData1($pCalculateArray["InspectionResultNo"], $pCalculateArray["InspectionNo"]);
// 		$lRowBeforeCommitInspectionResultDetail1 = (Array)$lTblBeforeCommitInspectionResultDetail1[0];

// 		$lTblBeforeCommitInspectionResultDetail2 = $this->getBeforeCommitInspectionResultDetailData2($pCalculateArray["InspectionResultNo"], $pCalculateArray["WorkInspectionPoint"], $pCalculateArray["WorkAnalGrp01"], $pCalculateArray["WorkAnalGrp02"]);
// 		$lRowBeforeCommitInspectionResultDetail2 = (Array)$lTblBeforeCommitInspectionResultDetail2[0];

// 		$lTblBeforeCommitInspectionResultAnalyze1 = $this->getBeforeCommitInspectionResultAnalyzeData1($pCalculateArray["InspectionResultNo"], $pCalculateArray["WorkInspectionPoint"], $pCalculateArray["WorkAnalGrp01"], $pCalculateArray["WorkAnalGrp02"]);




// 		if (count($lRowBeforeCommitInspectionResultDetail1) != 0)
// 		{
// 			if (($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] != null AND $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] != 0) AND ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK OR $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET OR $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG))
// 			{
// 				$lBeforeCalcFlg = true;
// 			}
// 			else
// 			{
// 				$lBeforeCalcFlg = false;
// 			}
// 		}
// 		else
// 		{
// 			$lBeforeCalcFlg = false;
// 		}

// 		if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_NUMBER)
// 		{
// 			if (Input::get('rdiResultClass','') == self::RESULT_CLASS_OK OR Input::get('rdiResultClass','') == self::RESULT_CLASS_OFFSET OR Input::get('rdiResultClass','') == self::RESULT_CLASS_NG)
// 			{
// 				$lCalcFlg = true;
// 			}
// 			else
// 			{
// 				$lCalcFlg = false;
// 			}
// 		}
// 		else
// 		{
// 			$lCalcFlg = false;
// 		}

// 		Log::write('info', '2',
// 			[
// 				"INSPECTION_RESULT_VALUE" => $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"],
// 				"BeforeCalcFlg" => $lBeforeCalcFlg,
// 				"CalcFlg"       => $lCalcFlg,
// 			]
// 		);

// 		//default
// 		if ($lCalcFlg == true)
// 		{
// 			//Calculated Number (何個の値で計算したか)
// 			$lCalcNam =  1;
// 			//Max Value (最大値)
// 			$lMaxValue = $lWorkInspectionResult;
// 			//Min Value (最小値)
// 			$lMinValue = $lWorkInspectionResult;
// 			//Average Value (平均値)
// 			$lAverageValue = $lWorkInspectionResult;
// 			//Range Value (幅)
// 			$lRangeValue = 0;
// 			//Stdev (ｼｸﾞﾏσ)
// 			$lStdev = 0;
// 			//CPK (CPK)
// 			$lCpk = 1;
// 			//Xbar UCL (Xbarの上方管理限界線)
// 			$lXbarUcl = $lWorkInspectionResult;
// 			//Xbar LCL (Xbarの下方管理限界線)
// 			$lXbarLcl = $lWorkInspectionResult;
// 			//RChart UCL (RChartの上方管理限界線)
// 			$lRchartUcl = 0;
// 			//RChart  LCL (RChartの下方管理限界線)
// 			$lRchartLcl = 0;
// 			//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
// 			$lJudgeReason = "";
// 		}

// 		if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
// 		{
// 			$lRowBeforeCommitInspectionResultAnalyze1 = (Array)$lTblBeforeCommitInspectionResultAnalyze1[0];

// 			if ($lBeforeCalcFlg == true and $lCalcFlg == true)
// 			{
// 				$lCodeNum = number_format($lRowBeforeCommitInspectionResultDetail2["COUNT"]);
// 				$lCalcNam = number_format($lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"]);
// 			}
// 			elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
// 			{
// 				$lCodeNum = number_format($lRowBeforeCommitInspectionResultDetail2["COUNT"] - 1);
// 				$lCalcNam = number_format($lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"] - 1);
// 			}
// 			elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
// 			{
// 				$lCodeNum = number_format($lRowBeforeCommitInspectionResultDetail2["COUNT"] + 1);
// 				$lCalcNam = number_format($lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"] + 1);
// 			}
// 			else
// 			{
// 				$lCodeNum = 0;
// 				$lCalcNam = 0;
// 			}
// 		}
// 		else
// 		{
// 			if ($lBeforeCalcFlg == true and $lCalcFlg == true)
// 			{
// 				//analyTBLは存在する →あり得ない
// 				// $lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"];
// 				// $lCalcNam = 1;
// 			}
// 			elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
// 			{
// 				//analyTBLは存在する →あり得ない
// 				// $lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] - 1;
// 				// $lCalcNam = 0;
// 			}
// 			elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
// 			{
// 				$lCodeNum = number_format($lRowBeforeCommitInspectionResultDetail2["COUNT"] + 1);
// 				$lCalcNam = 1;
// 			}
// 			else
// 			{
// 				$lCodeNum = 0;
// 				$lCalcNam = 0;
// 			}
// 		}

// 		Log::write('info', '3',
// 			[
// 				"CodeNum" => $lCodeNum,
// 				"CalcNam" => $lCalcNam,
// 			]
// 		);

				
// 			if ($lBeforeCalcFlg == false and $lCalcFlg == false)
// 			{
// 				//Nothing
// 			}
// 			else
// 			{
// 				Log::write('info', 'BA2010 Start Setteing TRESANAL table item');
							
// 				//store value of TCCIENTMST Table (Xbar)
// 				$lTblCoefficientXbar = [];
// 				$lRowCoefficientXbar = [];
// 				$objtccientmst = new tccientmst;
// 				$lTblCoefficientXbar = $objtccientmst->getCoefficientData(self::COEFFICIENT_XBAR, $lCodeNum);
// 				if (count($lTblCoefficientXbar) != 0)
// 				{
// 					$lRowCoefficientXbar = (Array)$lTblCoefficientXbar[0];
// 				}
// 				else
// 				{
// 					$lRowCoefficientXbar["COEFFICIENT"] = 0;
// 				}

// 				//store value of TCCIENTMST Table (RChart UCL)
// 				$lTblCoefficientRChartUCL = [];
// 				$lRowCoefficientRChartUCL = [];
// 				$objtccientmst = new tccientmst;
// 				$lTblCoefficientRChartUCL = $objtccientmst->getCoefficientData(self::COEFFICIENT_RCHAT_UCL, $lCodeNum);
// 				if (count($lTblCoefficientRChartUCL) != 0)
// 				{
// 					$lRowCoefficientRChartUCL = (Array)$lTblCoefficientRChartUCL[0];
// 				}
// 				else
// 				{
// 					$lRowCoefficientRChartUCL["COEFFICIENT"] = 0;
// 				}

// 				//store value of TCCIENTMST Table (RChart LCL)
// 				$lTblCoefficientRChartLCL = [];
// 				$lRowCoefficientRChartLCL = [];
// 				$objtccientmst = new tccientmst;
// 				$lTblCoefficientRChartLCL = $objtccientmst->getCoefficientData(self::COEFFICIENT_RCHAT_LCL, $lCodeNum);
// 				if (count($lTblCoefficientRChartLCL) != 0)
// 				{
// 					$lRowCoefficientRChartLCL = (Array)$lTblCoefficientRChartLCL[0];
// 				}
// 				else
// 				{
// 					$lRowCoefficientRChartLCL["COEFFICIENT"] = 0;
// 				}


// 				//Calculate  Max Min Value
// 				if ($lBeforeCalcFlg == true and $lCalcFlg == true)
// 				{
// 					$lWorkMaxValue = $lWorkInspectionResult;
// 					$lWorkMinValue = $lWorkInspectionResult;
// 				}
// 				elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
// 				{
// 					$lWorkMaxValue = -9999999999999;
// 					$lWorkMinValue = 9999999999999;
// 				}
// 				elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
// 				{
// 					$lWorkMaxValue = $lWorkInspectionResult;
// 					$lWorkMinValue = $lWorkInspectionResult;
// 				}
// 				else
// 				{
// 					//入ってこない →No Calculate
// 				}

// 				Log::write('info', '4',
// 					[
// 						"WorkMaxValue" => $lWorkMaxValue,
// 						"WorkMinValue" => $lWorkMinValue,
// 					]
// 				);

// 				$lTblBeforeCommitInspectionResultDetail3 = $this->getBeforeCommitInspectionResultDetailData3($pCalculateArray["InspectionResultNo"], $pCalculateArray["WorkInspectionPoint"], $pCalculateArray["WorkAnalGrp01"], $pCalculateArray["WorkAnalGrp02"]);

// 				//Calculate  Max Value and Min Value
// 				if ($lTblBeforeCommitInspectionResultDetail3 != null)
// 				{
// 					foreach ($lTblBeforeCommitInspectionResultDetail3 as $lRowBeforeCommitInspectionResultDetail3)
// 					{
// 						$lRowBeforeCommitInspectionResultDetail3 = (Array)$lRowBeforeCommitInspectionResultDetail3;
								
// 								Log::write('info', '4-1',
// 					[
// 						"111" => $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],
// 						"222" => $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"],
// 					]
// 				);
// 						if ($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"] != $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
// 						{
// 							if ($lWorkMaxValue < number_format($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],4))
// 							{
// 								$lWorkMaxValue = number_format($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],4);
// 							}
												
// 							if ($lWorkMinValue > number_format($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],4))
// 							{
// 								$lWorkMinValue = number_format($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],4);
// 							}
// 						}
// 					}
// 				}
// 				$lMaxValue = $lWorkMaxValue;
// 				$lMinValue = $lWorkMinValue;
		
// 				Log::write('info', '5',
// 					[
// 						"MaxValue" => $lMaxValue,
// 						"MinValue" => $lMinValue,
// 					]
// 				);

// 				//Calculate  Average Value
// 				if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
// 				{
// 					Log::write('info', '11');
// 					if ($lBeforeCalcFlg == true and $lCalcFlg == true)
// 					{
// 						$lAverageValue =  number_format((($lWorkInspectionResult - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]), 4);
// 					}
// 					elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
// 					{
// 						$lAverageValue =  number_format(((0 - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]), 4);
// 					}
// 					elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
// 					{
// 						$lAverageValue =  number_format((($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] * ($lCalcNam - 1) + $lWorkInspectionResult) / $lCalcNam),4);
// 					}
// 					else
// 					{
// 						//入ってこない →No Calculate
// 					}
// 				}
// 				else
// 				{
// 					Log::write('info', '12');
// 					if ($lBeforeCalcFlg == true and $lCalcFlg == true)
// 					{
// 						//Nothing to do
// 					}
// 					elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
// 					{
// 						//Nothing to do
// 					}
// 					elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
// 					{
// 						$lAverageValue =  $lWorkInspectionResult;
// 					}
// 					else
// 					{
// 						//Nothing to do
// 					}
// 				}

// 				//Calculate  Range Value
// 				$lRangeValue = number_format(($lMaxValue - $lMinValue),4);
								
// 				Log::write('info', '6',
// 					[
// 						"AverageValue" => $lAverageValue,
// 						"RangeValue" => $lRangeValue,
// 					]
// 				);

// 				//Calculate  Stdev (ｼｸﾞﾏσ)
// 				if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
// 				{
// 					//前回の平均値と今回の平均値が同じ場合→同値を入力した場合は、計算せず、前回のσをそのまま入れる
// 					if (($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]) == 0)
// 					{
// Log::write('info', '29');
// 						$lStdev = number_format($lRowBeforeCommitInspectionResultAnalyze1["STDEV"],4);
// 					}
// 					else
// 					{
// Log::write('info', '30');
// 							//if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
// 							//{
// 							//    //既存測定値の修正の場合
// 							//	$lStdev = number_format(sqrt(
// 							//	                             (
// 							//	                              pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
// 							//	                              +
// 							//	                              pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)
// 							//	                              +
// 							//	                              (
// 							//	                               (
// 							//	                                ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
// 							//	                                *
// 							//	                                (
// 							//	                                 ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
// 							//	                                 -
// 							//	                                 (2 * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
// 							//	                                 +
// 							//	                                 (2 * $lAverageValue)
// 							//	                                )
// 							//	                               )
// 							//	                               / $lCalcNam
// 							//	                              )
// 							//	                             )
// 							//	                            )
// 							//	                        ,4);
// 							//}
// 							//else
// 							//{
// 							//    //新しい測定値の追加の場合
// 							//	$lStdev = number_format(sqrt(
// 							//	                              ((pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2) + pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)) * ($lCalcNam - 1) / $lCalcNam)
// 							//	                              + (pow($lAverageValue - Input::get('txtInspectionResult'), 2) / $lCalcNam)
// 							//	                             )
// 							//	                       ,4);
// 							//}
							
									
// 							//↑ 上はSTDEV.P(n法)の場合
// 							//↓ 下はSTDEV(n-1法)の場合
// 							//■■工場は通常STDEVを使用する■■
									
// 						if ($lBeforeCalcFlg == true)
// 						{
// Log::write('info', '31');

// 							//When modifying existing measurement values 既存測定値を修正する場合
// 							if ($lCalcNam >= 3)
// 							{
// 								$lStdev = number_format(sqrt(
// 								                              pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
// 								                              +
// 								                              (
// 								                               (($lCalcNam * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2))
// 								                                +
// 								                                pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'), 2)
// 								                                -
// 								                                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
// 								                                +
// 								                                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lAverageValue)
// 								                               ) / ($lCalcNam - 1)
// 								                              )
// 								                             )
// 								                       ,4);
// 							}
// 							elseif ($lCalcNam == 2)
// 							{
// 								$lStdev = number_format(sqrt(
// 								                              pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
// 								                              +
// 								                              (2 * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2))
// 								                              +
// 								                              pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'), 2)
// 								                              -
// 								                              (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
// 								                              +
// 								                              (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult')) * $lAverageValue)
// 							    	                         )
// 							        	               ,4);
// 							}
// 							else
// 							{
// 								$lStdev = 0;
// 							}
// 						}
// 						else
// 						{
// Log::write('info', '32');
// 							//When adding new measurement values 新しい測定値を追加する場合
// 							if ($lCalcNam >= 3)
// 							{
// 								Log::write('info', '32-1');
// 								$lStdev = number_format(sqrt(
// 								                               ( ($lCalcNam - 2) * pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
// 								                                 + (($lCalcNam - 1) * pow($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"], 2))
// 								                                 + pow($lAverageValue - Input::get('txtInspectionResult'), 2)
// 								                               ) / ($lCalcNam - 1)
// 								                             )
// 								                       ,4);
// 							}
// 							elseif ($lCalcNam == 2)
// 							{
// 								$lStdev = number_format(sqrt(
// 								                              pow(($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]), 2)
// 								                               + pow(($lAverageValue - Input::get('txtInspectionResult')) ,2)
// 								                             )
// 								                       ,4);
// 							}
// 							else
// 							{
// 								$lStdev = 0;
// 							}
// 						}

// 							// Log::write('info', 'BA2010 After Calculate Stdev',
// 							// 	[
// 							// 		"Stdev"            => $lRowBeforeCommitInspectionResultAnalyze1["STDEV"],
// 							// 		"STDEV2"           => pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2),
// 							// 		"n"                => $lCalcNam,
// 							// 		"S"                => (2 * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)),
// 							// 		"d"                => (Input::get('txtInspectionResult') - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]),
// 							// 		"Result"           => Input::get('txtInspectionResult'              ,''),
// 							// 		"AverageValueold"  => $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"],
// 							// 		"AverageValuenow"  => $lAverageValue,
// 							// 	]
// 							// );
// 					}
// 				}
// 				else
// 				{
// 					$lStdev = 0;
// 				}

// 				Log::write('info', '7',
// 					[
// 						"Stdev" => $lStdev,
// 					]
// 				);

// 				//Calculate  CPK
// 				if ($lStdev != 0)
// 				{
// 					$lWorkCpkStdSpecOver = number_format((($pCalculateArray["StdSpecOver"]- $lAverageValue) / (3 * $lStdev)), 4);
// 					$lWorkCpkStdSpecUnder = number_format((($lAverageValue - $pCalculateArray["StdSpecUnder"]) / (3 * $lStdev)), 4);
// 				}
// 				else
// 				{
// 					$lWorkCpkStdSpecOver = 0;
// 					$lWorkCpkStdSpecUnder = 0;
// 				}
								
// 				if ($lWorkCpkStdSpecOver < $lWorkCpkStdSpecUnder)
// 				{
// 					$lCpk = $lWorkCpkStdSpecOver;
// 				}
// 				else
// 				{
// 					$lCpk = $lWorkCpkStdSpecUnder;
// 				}
								
// 				Log::write('info', '8',
// 					[
// 						"WorkCpkStdSpecOver"  => $lWorkCpkStdSpecOver,
// 						"WorkCpkStdSpecUnder" => $lWorkCpkStdSpecUnder,
// 						"Cpk"                 => $lCpk,
// 					]
// 				);

// 				//Calculate  Xbar UCL (Xbarの上方管理限界線)
// 				$lXbarUcl = number_format(($lAverageValue + $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue),4);
// 				//Calculate  Xbar LCL (Xbarの下方管理限界線)
// 				$lXbarLcl = number_format(($lAverageValue - $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue),4);

// 				//Calculate  RChart UCL (RChartの上方管理限界線)
// 				$lRchartUcl = number_format(($lRowCoefficientRChartUCL["COEFFICIENT"] * $lRangeValue),4);
// 				//Calculate  RChart  LCL (RChartの下方管理限界線)
// 				$lRchartLcl = number_format(($lRowCoefficientRChartLCL["COEFFICIENT"] * $lRangeValue),4);

// 				Log::write('info', '9',
// 					[
// 						"XbarUcl"    => $lXbarUcl,
// 						"XbarLcl"    => $lXbarLcl,
// 						"RchartUcl"  => $lRchartUcl,
// 						"RchartLcl"  => $lRchartLcl,
// 					]
// 				);

// 				//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
// 				//画面入力値の為、下で画面の値を入れる
				
// 				//Judgeに使う値を求める
// 				//Get Value for Judge
// 				//初回、又は、全てのデータが同じ値
// 				//in case the first inspection or all inspection value is same
// 				if ($lStdev == 0)
// 				{
// 					$lJudgeMin = 0;
// 					$lJudgeMax = 0;
// 				}
// 				else
// 				{
// 					$lJudgeMin = number_format(($lAverageValue - $lStdev * 3), 4);
// 					$lJudgeMax = number_format(($lAverageValue + $lStdev * 3), 4);
// 				}

// 				Log::write('info', '10',
// 					[
// 						"JudgeMin"    => $lJudgeMin,
// 						"JudgeMax"    => $lJudgeMax,
// 					]
// 				);

// 				if ($lJudgeMin == 0 && $lJudgeMax == 0)
// 				{
// 					//Nothing
// 				}
// 				else
// 				{
// 					if (($pCalculateArray["StdSpecUnder"] > $lJudgeMin) or ($pCalculateArray["StdSpecUnder"] > $lJudgeMax) or ($pCalculateArray["StdSpecOver"] < $lJudgeMin) or ($pCalculateArray["StdSpecOver"] < $lJudgeMax))
// 					{
// 						//Judge Reason is value of Input
// 						$lJudgeReason = Input::get('cmbJudgeReason');
// 					}
// 					else
// 					{
// 						//Nothing
// 					}
// 				}
// 			}


// 		$pArray += [
// 				"BeforeCalcFlg"          => $lBeforeCalcFlg,
// 				"CalcFlg"                => $lCalcFlg,
// 				"CalcNam"                => $lCalcNam,
// 				"InspectionResultValue"  => $lWorkInspectionResult,
// 				"MaxValue"               => $lMaxValue,
// 				"MinValue"               => $lMinValue,
// 				"AverageValue"           => $lAverageValue,
// 				"RangeValue"             => $lRangeValue,
// 				"StdevValue"             => $lStdev,
// 				"CpkValue"               => $lCpk,
// 				"XbarUclValue"           => $lXbarUcl,
// 				"XbarLclValue"           => $lXbarLcl,
// 				"RchartUclValue"         => $lRchartUcl,
// 				"RchartLclValue"         => $lRchartLcl,
// 				"JudgeReason"            => $lJudgeReason,
// 				"JudgeMinValue"          => $lJudgeMin,
// 				"JudgeMaxValue"          => $lJudgeMax,
// 			];

// 	Log::write('info', 'Array',
// 		[
// 			"Array"                => $pArray,
// 		]
// 	);

// 	return $pArray;
// 	}


	// //**************************************************************************
	// // process            getBeforeCommitInspectionResultDetailData1
	// // overview           
	// // argument           
	// // return value       
	// // record of updates  No,1 
	// // Remarks            
	// //**************************************************************************
	// private function getBeforeCommitInspectionResultDetailData1($pInspectionResultNo, $pInspectionNo)
	// {
	// 	$lTblInspectionResultDetail1 = [];

	// 		$lTblInspectionResultDetail1 = DB::select('
	// 			SELECT DETL.INSPECTION_RESULT_VALUE
	// 			      ,DETL.INSPECTION_RESULT_TYPE
	// 			  FROM TRESDETT AS DETL
	// 			 WHERE DETL.INSPECTION_RESULT_NO = :InspectionResultNo
	// 			   AND DETL.INSPECTION_NO        = :InspectionNo
	// 		',
	// 			[
	// 				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
	// 				"InspectionNo"       => TRIM((String)$pInspectionNo),
	// 			]
	// 		);

	// 	return $lTblInspectionResultDetail1;
	// }

	// //**************************************************************************
	// // process            getBeforeCommitInspectionResultDetailData2
	// // overview           
	// // argument           
	// // return value       
	// // record of updates  No,1 
	// // Remarks            
	// //**************************************************************************
	// private function getBeforeCommitInspectionResultDetailData2($pInspectionResultNo, $pInspectionPoint, $pAnalGrp01, $pAnalGrp02)
	// {
	// 	$lTblInspectionResultDetail2 = [];

	// 		$lTblInspectionResultDetail2 = DB::select('
	// 			SELECT Count(*) AS COUNT
	// 			  FROM TRESDETT AS DETL
	// 			 WHERE DETL.INSPECTION_RESULT_NO     = :InspectionResultNo
	// 			   AND DETL.INSPECTION_POINT         = :InspectionPoint
	// 			   AND DETL.ANALYTICAL_GRP_01        = :AnalyticalGroup01
	// 			   AND DETL.ANALYTICAL_GRP_02        = :AnalyticalGroup02
	// 			   AND DETL.INSPECTION_RESULT_TYPE   = ""
	// 			   AND DETL.INSPECTION_RESULT_TYPE   <> ""
	// 		',
	// 			[
	// 				"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
	// 				"InspectionPoint"          => TRIM((String)$pInspectionPoint),
	// 				"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
	// 				"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
	// 			]
	// 		);

	// 	return $lTblInspectionResultDetail2;
	// }

	// //**************************************************************************
	// // process            getBeforeCommitInspectionResultDetailData3
	// // overview           
	// // argument           
	// // return value       Array
	// // record of updates  No,1 
	// // Remarks            
	// //**************************************************************************
	// private function getBeforeCommitInspectionResultDetailData3($pInspectionResultNo, $pInspectionPoint, $pAnalGrp01, $pAnalGrp02)
	// {
	// 	$lTblInspectionResultDetail3 = [];
		
	// 		$lTblInspectionResultDetail3 = DB::select('
	// 			SELECT DETL.INSPECTION_RESULT_VALUE
	// 			  FROM TRESDETT AS DETL
	// 			 WHERE DETL.INSPECTION_RESULT_NO     = :InspectionResultNo
	// 			   AND DETL.INSPECTION_POINT         = :InspectionPoint
	// 			   AND DETL.ANALYTICAL_GRP_01        = :AnalyticalGroup01
	// 			   AND DETL.ANALYTICAL_GRP_02        = :AnalyticalGroup02
	// 			   AND DETL.INSPECTION_RESULT_TYPE   IN ("1","2","3")
	// 		',
	// 			[
	// 				"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
	// 				"InspectionPoint"          => TRIM((String)$pInspectionPoint),
	// 				"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
	// 				"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
	// 			]
	// 		);

	// 	return $lTblInspectionResultDetail3;
	// }

	// //**************************************************************************
	// // process            getBeforeCommitInspectionResultAnalyzeData1
	// // overview           
	// // argument           
	// // return value       Array
	// // record of updates  No,1 
	// // Remarks            
	// //**************************************************************************
	// private function getBeforeCommitInspectionResultAnalyzeData1($pInspectionResultNo, $pInspPoint, $pAnalGrp01, $pAnalGrp02)
	// {
	// 	$lTblInspectionResultAnalyze1 = [];

	// 		$lTblInspectionResultAnalyze1 = DB::select('
	// 			SELECT ANALY.CALC_NUM
	//  					,ANALY.MAX_VALUE
	//  					,ANALY.MIN_VALUE
	//  					,ANALY.AVERAGE_VALUE
	//  					,ANALY.STDEV
	// 			 FROM TRESANAL AS ANALY
	// 			WHERE ANALY.INSPECTION_POINT      = :InspectionPoint
	// 			  AND ANALY.ANALYTICAL_GRP_01     = :AnalyticalGroup01
	// 			  AND ANALY.ANALYTICAL_GRP_02     = :AnalyticalGroup02
	// 			  AND ANALY.INSPECTION_RESULT_NO  = :InspectionResultNo
	// 		',
	// 			[
	// 				"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
	// 				"InspectionPoint"          => TRIM((String)$pInspPoint),
	// 				"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
	// 				"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
	// 			]
	// 		);

	// 	return $lTblInspectionResultAnalyze1;
	// }

	//**************************************************************************
	// process            insertAndUpdateInspectionResult
	// overview           transaction table
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function insertAndUpdateInspectionResult($pAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett ,$tablemodeltresanal)
	{
		Log::write('info', 'BA2010 start insert/update');

			Log::write('info', 'insertAndUpdateInspectionResult',
				[
					"BeforeCalcFlg"  => $pAfterCalculateData["BeforeCalcFlg"],
					"CalcFlg"        => $pAfterCalculateData["CalcFlg"],
					"$tablemodeltresdett" => $tablemodeltresdett,
				]
			);
		//All processing is 1 Transaction
		DB::transaction
		(
			function() use ($pAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett ,$tablemodeltresanal)
			{
				DB::update('
						UPDATE TRESHEDT
						   SET LAST_UPDATE_USER_ID  = :LastUpdateUserID
						      ,UPDATE_YMDHMS        = now()
						      ,DATA_REV             = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
				',
					[
						"LastUpdateUserID"    => $tablemodeltreshedt->LAST_UPDATE_USER_ID,
						"InspectionResultNo"  => $tablemodeltreshedt->INSPECTION_RESULT_NO,
					]
				);
				Log::write('info', 'BA2010 Finish Update Header');

				DB::update('
				        UPDATE TRESDETT
				           SET INSPECTION_RESULT_VALUE       = :InspectionResultValue
				              ,INSPECTION_RESULT_TYPE        = :InspectionResultType
				              ,OFFSET_NG_BUNRUI_ID           = :OffsetNGBunruiId
				              ,OFFSET_NG_REASON_ID           = :OffsetNGReasonId
				              ,OFFSET_NG_REASON              = :OffsetNGReason
				              ,OFFSET_NG_ACTION              = :OffsetNGAction
				              ,OFFSET_NG_ACTION_DTL_INTERNAL = :OffsetNGActionDetailInternal
				              ,OFFSET_NG_ACTION_DTL_REQUEST  = :OffsetNGActionDetailRequest
				              ,OFFSET_NG_ACTION_DTL_TSR      = :OffsetNGActionDetailTSR
				              ,OFFSET_NG_ACTION_DTL_SORTING  = :OffsetNGActionDetailSorting
				              ,OFFSET_NG_ACTION_DTL_NOACTION = :OffsetNGActionDetailNoAction
				              ,ACTION_DETAILS_ID             = :ActionDetailsId
				              ,ACTION_DETAILS_TR_NO_TSR_NO   = :ActionDetails
				              ,TR_NO                         = :TecReqNo
				              ,TECHNICIAN_USER_ID            = :TecReqPerson
				              ,TSR_NO                        = :TSRNo
				              ,ANALYTICAL_GRP_01_MEMO        = :AnalGrp01Memo
				              ,LAST_UPDATE_USER_ID           = :LastUpdateUserID
				              ,UPDATE_YMDHMS                 = now()
				              ,DATA_REV                      = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO          = :InspectionResultNo
						   AND INSPECTION_NO                 = :InspectionNo
				',
					[
						"InspectionResultValue"        => $tablemodeltresdett->INSPECTION_RESULT_VALUE,
						"InspectionResultType"         => $tablemodeltresdett->INSPECTION_RESULT_TYPE,
						"OffsetNGBunruiId"             => $tablemodeltresdett->OFFSET_NG_BUNRUI_ID,
						"OffsetNGReasonId"             => $tablemodeltresdett->OFFSET_NG_REASON_ID,
						"OffsetNGReason"               => $tablemodeltresdett->OFFSET_NG_REASON,
						"OffsetNGAction"               => $tablemodeltresdett->OFFSET_NG_ACTION,
						"OffsetNGActionDetailInternal" => $tablemodeltresdett->OFFSET_NG_ACTION_DTL_INTERNAL,
						"OffsetNGActionDetailRequest"  => $tablemodeltresdett->OFFSET_NG_ACTION_DTL_REQUEST,
						"OffsetNGActionDetailTSR"      => $tablemodeltresdett->OFFSET_NG_ACTION_DTL_TSR,
						"OffsetNGActionDetailSorting"  => $tablemodeltresdett->OFFSET_NG_ACTION_DTL_SORTING,
						"OffsetNGActionDetailNoAction" => $tablemodeltresdett->OFFSET_NG_ACTION_DTL_NOACTION,
						"ActionDetailsId"              => $tablemodeltresdett->ACTION_DETAILS_ID,
						"ActionDetails"                => $tablemodeltresdett->ACTION_DETAILS_TR_NO_TSR_NO,
						"TecReqNo"                     => $tablemodeltresdett->TR_NO,
						"TecReqPerson"                 => $tablemodeltresdett->TECHNICIAN_USER_ID,
						"TSRNo"                        => $tablemodeltresdett->TSR_NO,
						"AnalGrp01Memo"                => $tablemodeltresdett->ANALYTICAL_GRP_01_MEMO,
						"LastUpdateUserID"             => $tablemodeltresdett->LAST_UPDATE_USER_ID,
						"InspectionResultNo"           => $tablemodeltresdett->INSPECTION_RESULT_NO,
						"InspectionNo"                 => $tablemodeltresdett->INSPECTION_NO,
					]
				);
				Log::write('info', 'BA2010 Finish Update Detail');

				if ($pAfterCalculateData["BeforeCalcFlg"] == false and $pAfterCalculateData["CalcFlg"] == false)
				{
					//Nothing
				}
				else
				{
					Log::write('info', 'BA2010 Start Insert/Update Analyze');
					//Not have TRESANAL → insert
					//Have TRESANAL → update
					DB::insert('
						     INSERT INTO TRESANAL
										(INSPECTION_SHEET_NO
										,REV_NO
										,INSPECTION_POINT
										,ANALYTICAL_GRP_01
										,ANALYTICAL_GRP_02
										,INSPECTION_RESULT_NO
										,PROCESS_ID
										,INSPECTION_YMD
										,INSPECTION_TIME_ID
										,CONDITION_CD
										,MOLD_NO
										,TEAM_ID
										,STD_MIN_SPEC
										,STD_MAX_SPEC
										,PICTURE_URL
										,CALC_NUM
										,MAX_VALUE
										,MIN_VALUE
										,AVERAGE_VALUE
										,RANGE_VALUE
										,STDEV
										,CPK
										,XBAR_UCL
										,XBAR_LCL
										,RCHART_UCL
										,RCHART_LCL
										,JUDGE_REASON
										,INSERT_YMDHMS
										,LAST_UPDATE_USER_ID
										,UPDATE_YMDHMS
										,DATA_REV)
								  VALUES (:CheckSheetNo
										 ,:RevisionNo
										 ,:InspectionPoint
										 ,:AnalyticalGroup01
										 ,:AnalyticalGroup02
										 ,:InspectionResultNo
										 ,:ProcessId
										 ,:InspectionDate
										 ,:InspectionTimeId
										 ,:ConditionCode
										 ,:MoldNo
										 ,:TeamId
										 ,:StandardMinSpec
										 ,:StandardMaxSpec
										 ,:PictureURL
										 ,:CalcNam1
										 ,:MaxValue1
										 ,:MinValue1
										 ,:AverageValue1
										 ,:RangeValue1
										 ,:Stdev1
										 ,:Cpk1
										 ,:XbarUcl1
										 ,:XbarLcl1
										 ,:RchartUcl1
										 ,:RchartLcl1
										 ,:JudgeReason1
										 ,now()
										 ,:LastUpdateUserID1
										 ,now()
										 ,1)
						ON DUPLICATE KEY
								  UPDATE CALC_NUM             = :CalcNam2
										,MAX_VALUE            = :MaxValue2
										,MIN_VALUE            = :MinValue2
										,AVERAGE_VALUE        = :AverageValue2
										,RANGE_VALUE          = :RangeValue2
										,STDEV                = :Stdev2
										,CPK                  = :Cpk2
										,XBAR_UCL             = :XbarUcl2
										,XBAR_LCL             = :XbarLcl2
										,RCHART_UCL           = :RchartUcl2
										,RCHART_LCL           = :RchartLcl2
										,JUDGE_REASON         = :JudgeReason2
										,LAST_UPDATE_USER_ID  = :LastUpdateUserID2
										,UPDATE_YMDHMS        = now()
										,DATA_REV             = DATA_REV + 1
					',
						[
							"CheckSheetNo"         => $tablemodeltresanal->INSPECTION_SHEET_NO,
							"RevisionNo"           => $tablemodeltresanal->REV_NO,
							"InspectionPoint"      => $tablemodeltresanal->INSPECTION_POINT,
							"AnalyticalGroup01"    => $tablemodeltresanal->ANALYTICAL_GRP_01,
							"AnalyticalGroup02"    => $tablemodeltresanal->ANALYTICAL_GRP_02,
							"InspectionResultNo"   => $tablemodeltresanal->INSPECTION_RESULT_NO,
							"ProcessId"            => $tablemodeltresanal->PROCESS_ID,
							"InspectionDate"       => $tablemodeltresanal->INSPECTION_YMD,
							"InspectionTimeId"     => $tablemodeltresanal->INSPECTION_TIME_ID,
							"ConditionCode"        => $tablemodeltresanal->CONDITION_CD,
							"MoldNo"               => $tablemodeltresanal->MOLD_NO,
							"TeamId"               => $tablemodeltresanal->TEAM_ID,
							"StandardMinSpec"      => $tablemodeltresanal->STD_MIN_SPEC,
							"StandardMaxSpec"      => $tablemodeltresanal->STD_MAX_SPEC,
							"PictureURL"           => $tablemodeltresanal->PICTURE_URL,
							"CalcNam1"             => $tablemodeltresanal->CALC_NUM,
							"CalcNam2"             => $tablemodeltresanal->CALC_NUM,
							"MaxValue1"            => $tablemodeltresanal->MAX_VALUE,
							"MaxValue2"            => $tablemodeltresanal->MAX_VALUE,
							"MinValue1"            => $tablemodeltresanal->MIN_VALUE,
							"MinValue2"            => $tablemodeltresanal->MIN_VALUE,
							"AverageValue1"        => $tablemodeltresanal->AVERAGE_VALUE,
							"AverageValue2"        => $tablemodeltresanal->AVERAGE_VALUE,
							"RangeValue1"          => $tablemodeltresanal->RANGE_VALUE,
							"RangeValue2"          => $tablemodeltresanal->RANGE_VALUE,
							"Stdev1"               => $tablemodeltresanal->STDEV,
							"Stdev2"               => $tablemodeltresanal->STDEV,
							"Cpk1"                 => $tablemodeltresanal->CPK,
							"Cpk2"                 => $tablemodeltresanal->CPK,
							"XbarUcl1"             => $tablemodeltresanal->XBAR_UCL,
							"XbarUcl2"             => $tablemodeltresanal->XBAR_UCL,
							"XbarLcl1"             => $tablemodeltresanal->XBAR_LCL,
							"XbarLcl2"             => $tablemodeltresanal->XBAR_LCL,
							"RchartUcl1"           => $tablemodeltresanal->RCHART_UCL,
							"RchartUcl2"           => $tablemodeltresanal->RCHART_UCL,
							"RchartLcl1"           => $tablemodeltresanal->RCHART_LCL,
							"RchartLcl2"           => $tablemodeltresanal->RCHART_LCL,
							"JudgeReason1"         => $tablemodeltresanal->JUDGE_REASON,
							"JudgeReason2"         => $tablemodeltresanal->JUDGE_REASON,
							"LastUpdateUserID1"    => $tablemodeltresanal->LAST_UPDATE_USER_ID,
							"LastUpdateUserID2"    => $tablemodeltresanal->LAST_UPDATE_USER_ID,

						]
					);
				}
				Log::write('info', 'BA2010 Finish Insert/Update Analyze');
			}
		);
	}

	//**************************************************************************
	// process         setviewDataScreenHeader
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataScreenHeader($pViewData, $pRowHeaderData)
	{

		$lInspectionDate = $this->convertDateFormat((String)$pRowHeaderData["INSPECTION_YMD"], self::CONVDATA_ARG2);
		$lProductionDate = $this->convertDateFormat((String)$pRowHeaderData["PRODUCTION_YMD"], self::CONVDATA_ARG2);

		$pViewData += [
				"ProcessName"            => $pRowHeaderData["PROCESS_NAME"],
				"CustomerName"           => $pRowHeaderData["CUSTOMER_NAME"],
				"CheckSheetNo"           => $pRowHeaderData["INSPECTION_SHEET_NO"],
				"CheckSheetName"         => $pRowHeaderData["INSPECTION_SHEET_NAME"],
				"RevisionNo"             => $pRowHeaderData["REV_NO"],
				"ItemNo"                 => $pRowHeaderData["ITEM_NO"],
				"ItemName"               => $pRowHeaderData["ITEM_NAME"],
				"ModelName"              => $pRowHeaderData["MODEL_NAME"],
				"MachineNo"              => $pRowHeaderData["MACHINE_NO"],
				"MachineName"            => $pRowHeaderData["MACHINE_NAME"],
				"InspectorCode"          => $pRowHeaderData["INSERT_USER_ID"],
				"InspectorName"          => $pRowHeaderData["USER_NAME"],
				"TeamName"               => $pRowHeaderData["TEAM_NAME"],
				"InspectionDate"         => $lInspectionDate,
				"ConditionName"          => $pRowHeaderData["CONDITION_NAME"],
				"InspectionTimeName"     => $pRowHeaderData["INSPECTION_TIME_NAME"],
				"MaterialName"           => $pRowHeaderData["MATERIAL_NAME"],
				"MaterialSize"           => $pRowHeaderData["MATERIAL_SIZE"],
				"ProductWeight"          => $pRowHeaderData["PRODUCT_WEIGHT"],
				"HeatTreatmentType"      => $pRowHeaderData["HEAT_TREATMENT_TYPE"],
				"PlatingKind"            => $pRowHeaderData["PLATING_KIND"],
				"ColorName"              => $pRowHeaderData["COLOR_NAME"],
				"DrawingNo"              => $pRowHeaderData["DRAWING_NO"],
				"ISOKanriNo"             => $pRowHeaderData["ISO_KANRI_NO"],
				"MaterialLotNo"          => $pRowHeaderData["MATERIAL_LOT_NO"],
				"CertificateNo"          => $pRowHeaderData["CERTIFICATE_NO"],
				"PoNo"                   => $pRowHeaderData["PO_NO"],
				"InvoiceNo"              => $pRowHeaderData["INVOICE_NO"],
				"QuantityCoil"           => $pRowHeaderData["QUANTITY_COIL"],
				"QuantityWeight"         => $pRowHeaderData["QUANTITY_WEIGHT"],
				"LotNo"                  => $pRowHeaderData["LOT_NO"],
				"ProductionDate"         => $lProductionDate,
				"ProcessId"              => $pRowHeaderData["PROCESS_ID"],
				"MoldNo"                 => $pRowHeaderData["MOLD_NO"],
				"ColorId"                => $pRowHeaderData["COLOR_ID"],
				"TeamId"                 => $pRowHeaderData["TEAM_ID"],
				"ConditionCode"          => $pRowHeaderData["CONDITION_CD"],
				"InspectionTimeId"       => $pRowHeaderData["INSPECTION_TIME_ID"],
				"CustomerCode"           => $pRowHeaderData["CUSTOMER_ID"],
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setviewDataScreendetail
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataScreendetail($pViewData, $pRowInspectionNo)
	{
		$pViewData += [
			"InspectionNo"				=> $pRowInspectionNo["INSPECTION_NO"],
			"hidInspectionNo"			=> $pRowInspectionNo["INSPECTION_NO"],
			"InspectionPoint"			=> $pRowInspectionNo["INSPECTION_POINT"],
			"AnalyticalGroup01"			=> $pRowInspectionNo["ANALYTICAL_GRP_01"],
			"AnalyticalGroup02"			=> $pRowInspectionNo["ANALYTICAL_GRP_02"],
			"SampleNo"					=> $pRowInspectionNo["SAMPLE_NO"],
			"InspectionItem"			=> $pRowInspectionNo["INSPECTION_ITEM"],
			"ToolCode"					=> $pRowInspectionNo["TOOL_CD"],
			"UnitTypeId"				=> $pRowInspectionNo["UNIT_TYPE_ID"],
			"UnitType"					=> $pRowInspectionNo["UNIT_TYPE_NAME"],
			"PictureURL"				=> $pRowInspectionNo["PICTURE_URL"],
			"XRChartNoPokayokeNo"		=> $pRowInspectionNo["XR_CHART_NO_POKAYOKE_NO"],
			"ReferToDocumentFrequency"	=> $pRowInspectionNo["REFER_TO_DOCUMENT_FREQUENCY"],
			"Equipment"					=> $pRowInspectionNo["INSPECTION_TOOL_CLASS_NAME"],
			"InspectionResultInputType"	=> $pRowInspectionNo["INSPECTION_RESULT_INPUT_TYPE"],
			"NGUnder"					=> number_format($pRowInspectionNo["THRESHOLD_NG_UNDER"], 4),
			"OFFSETUnder"				=> number_format($pRowInspectionNo["THRESHOLD_OFFSET_UNDER"], 4),
			"ReferenceValue"			=> number_format($pRowInspectionNo["REFERENCE_VALUE"], 4),
			"OFFSETOver"				=> number_format($pRowInspectionNo["THRESHOLD_OFFSET_OVER"], 4),
			"NGOver"					=> number_format($pRowInspectionNo["THRESHOLD_NG_OVER"], 4),
			"NGUnderDiff"				=> number_format((($pRowInspectionNo["REFERENCE_VALUE"] - $pRowInspectionNo["THRESHOLD_NG_UNDER"]) * -1), 4),
			"OFFSETUnderDiff"			=> number_format((($pRowInspectionNo["REFERENCE_VALUE"] - $pRowInspectionNo["THRESHOLD_OFFSET_UNDER"]) * -1), 4),
			"ReferenceValueDiff"		=> number_format((($pRowInspectionNo["REFERENCE_VALUE"] - $pRowInspectionNo["REFERENCE_VALUE"]) * -1), 4),
			"OFFSETOverDiff"			=> number_format((($pRowInspectionNo["REFERENCE_VALUE"] - $pRowInspectionNo["THRESHOLD_OFFSET_OVER"]) * -1), 4),
			"NGOverDiff"				=> number_format((($pRowInspectionNo["REFERENCE_VALUE"] - $pRowInspectionNo["THRESHOLD_NG_OVER"]) * -1), 4),
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setviewDataPrevNextBtn
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataPrevNextBtn($pViewData, $pTblInspectionNo, $pDataRowNo)
	{
		//lock << button in case data is in 0 line
		if ($pDataRowNo == 0)
		{
			$pViewData += [	"prevButtonLock" => "lock",	];
		}
		else
		{
			$pViewData += [	"prevButtonLock" => "",	];
		}

		//lock >> button in case data is last data
		if ($pDataRowNo == count($pTblInspectionNo) -1)
		{
			$pViewData += [	"nextButtonLock" => "lock",	];
		}
		else
		{
			$pViewData += [	"nextButtonLock" => "",	];
		}

		return $pViewData;
	}


	//**************************************************************************
	// process         setviewDataScreenResult1
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataScreenResult1($pViewData, $pRowInspectionNo, $pFilterBtnFlg, $pOnchangeFlg)
	{
		$WorkScreensetPtn = "1";

		$pViewData += [
				"InspectionResult"              => "",
				"CavityMemo"                    => "",
				"InspectionResultRadio1"        => "",
				"InspectionResultRadio2"        => "",
				"ResultClass1"                  => "",
				"ResultClass2"                  => "",
				"ResultClass3"                  => "",
				"ResultClass4"                  => "",
				"ResultClass5"                  => "",
				"OffsetNGBunrui"                => "",
				"OffsetNGReason"                => "",
				"OffsetNGAction1"               => "",
				"OffsetNGAction2"               => "",
				"OffsetNGActionDetailInternal"  => "",
				"OffsetNGActionDetailRequest"   => "",
				"OffsetNGActionDetailTSR"       => "",
				"OffsetNGActionDetailSorting"   => "",
				"OffsetNGActionDetailNoAction"  => "",
				"ActionDetails"                 => "",
				"TecReqNo"                      => "",
				"TecReqPerson"                  => "",
				"TSRNo"                         => "",
				"InspectionToolClass"           => "0",
				"Status"                        => "",
				"StatusColorClass"              => "SearchResultTable",
				"hidDataRev"                    => 0,
		];

		if ($pRowInspectionNo["INSPECTION_RESULT_NO"] != 0)
		{
			$lTblInspectionResultDetail = $this->getInspectionResultDetailData($pRowInspectionNo["INSPECTION_RESULT_NO"], $pRowInspectionNo["INSPECTION_NO"]);
		}


		if ($lTblInspectionResultDetail != null)
		{
			$lRowInspectionResultDetail = (Array)$lTblInspectionResultDetail[0];

			data_set($pViewData, 'InspectionResult', $lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"]);
			data_set($pViewData, 'CavityMemo', $pRowInspectionNo["ANALYTICAL_GRP_01_MEMO"]);

			if (is_null(Input::get('cmbOffsetNGBunrui')))
			{
				data_set($pViewData, 'OffsetNGBunrui'  , $lRowInspectionResultDetail["OFFSET_NG_BUNRUI_ID"]);
			}
			else
			{
				if ($pOnchangeFlg == "1")
				{
					data_set($pViewData, 'OffsetNGBunrui'  , Input::get('cmbOffsetNGBunrui'));
				}
				else
				{
					data_set($pViewData, 'OffsetNGBunrui'  , $lRowInspectionResultDetail["OFFSET_NG_BUNRUI_ID"]);
				}
			}

			$pViewData = $this->setOffsetNGReasonList($pViewData, $WorkScreensetPtn);
			if ($pViewData["OffsetNGBunrui"] == "")
			{
				data_set($pViewData, 'OffsetNGReason'  , "");
			}
			else
			{
				data_set($pViewData, 'OffsetNGReason'  , $lRowInspectionResultDetail["OFFSET_NG_REASON_ID"]);
			}

			data_set($pViewData, 'ActionDetails'   , $lRowInspectionResultDetail["ACTION_DETAILS_ID"]);
			data_set($pViewData, 'TecReqNo'        , $lRowInspectionResultDetail["TR_NO"]);
			data_set($pViewData, 'TecReqPerson'    , $lRowInspectionResultDetail["TECHNICIAN_USER_ID"]);
			data_set($pViewData, 'TSRNo'           , $lRowInspectionResultDetail["TSR_NO"]);

			if (is_null(Input::get('cmbInspectonToolClass')))
			{
				//Nothing to do
			}
			else
			{
				if ($pFilterBtnFlg == "1")
				{
					data_set($pViewData, 'InspectionToolClass'  , (String)Input::get('cmbInspectonToolClass'));
				}
				else
				{
					data_set($pViewData, 'InspectionToolClass'  , (String)Input::get('hidEquipment'));
				}
			}

			data_set($pViewData, 'Status'          , "");
			data_set($pViewData, 'hidDataRev'      , $lRowInspectionResultDetail["DATA_REV"]);

			//if Inspection Result is OK/NG
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"] == "1")
			{
				data_set($pViewData, 'InspectionResultRadio1'      , true);
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_VALUE"] == "2")
			{
				data_set($pViewData, 'InspectionResultRadio2'      , true);
			}
			else
			{
				//Nothing
			}

			//change position to set as value of Result Class
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK)
			{
				data_set($pViewData, 'ResultClass1'      , true);
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET)
			{
				data_set($pViewData, 'ResultClass2'      , true);
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG)
			{
				data_set($pViewData, 'ResultClass3'      , true);
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NO_CHECK)
			{
				data_set($pViewData, 'ResultClass4'      , true);
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_LINE_STOP)
			{
				data_set($pViewData, 'ResultClass5'      , true);
			}
			else
			{
				//Nothing
			}

			//change position to set as value of OFFSET Action / NG Action
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION"] == self::OFFSET_NG_ACTION_RUNNING)
			{
				data_set($pViewData, 'OffsetNGAction1'      , true);
			}
			elseif ($lRowInspectionResultDetail["OFFSET_NG_ACTION"] == self::OFFSET_NG_ACTION_STOP_LINE)
			{
				data_set($pViewData, 'OffsetNGAction2'      , true);
			}
			else
			{
				//Nothing
			}

			//set OFFSET Action Detail / NG Action Detail（5）
			//Internal Approve
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_INTERNAL"] == "1")
			{
				// $pViewData += [	"OffsetNGActionDetailInternal"  => true, ];
				data_set($pViewData, 'OffsetNGActionDetailInternal'      , true);
			}

			//Request Offset
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_REQUEST"] == "1")
			{
				// $pViewData += [	"OffsetNGActionDetailRequest"   => true, ];
				data_set($pViewData, 'OffsetNGActionDetailRequest'      , true);
			}

			//TSR
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_TSR"] == "1")
			{
				// $pViewData += [	"OffsetNGActionDetailTSR"       => true, ];
				data_set($pViewData, 'OffsetNGActionDetailTSR'      , true);
			}

			//Sorting
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_SORTING"] == "1")
			{
				// $pViewData += [	"OffsetNGActionDetailSorting"   => true, ];
				data_set($pViewData, 'OffsetNGActionDetailSorting'      , true);
			}

			//No Action
			if ($lRowInspectionResultDetail["OFFSET_NG_ACTION_DTL_NOACTION"] == "1")
			{
				// $pViewData += [	"OffsetNGActionDetailNoAction"  => true, ];
				data_set($pViewData, 'OffsetNGActionDetailNoAction'      , true);
			}

			//change color again OFFSET or NG
			if ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET)
			{
				unset($pViewData["StatusColorClass"]);
				data_set($pViewData, 'StatusColorClass'      , 'OFFSETColor');
			}
			elseif ($lRowInspectionResultDetail["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG)
			{
				unset($pViewData["StatusColorClass"]);
				data_set($pViewData, 'StatusColorClass'      , 'NGColor');
			}
		}

		return $pViewData;
	}


	//**************************************************************************
	// process         setviewDataScreenResult2
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataScreenResult2($pViewData, $pRowInspectionNo)
	{
		$WorkScreensetPtn = "2";

		//in spite data exists or not,clear
		unset($pViewData["InspectionResult"]);
		unset($pViewData["CavityMemo"]);
		unset($pViewData["OffsetNGBunrui"]);
		unset($pViewData["OffsetNGReason"]);
		unset($pViewData["ActionDetails"]);
		unset($pViewData["TecReqPerson"]);
		unset($pViewData["TecReqNo"]);
		unset($pViewData["TSRNo"]);
		unset($pViewData["Status"]);
		unset($pViewData["InspectionResultRadio1"]);
		unset($pViewData["InspectionResultRadio2"]);
		unset($pViewData["ResultClass1"]);
		unset($pViewData["ResultClass2"]);
		unset($pViewData["ResultClass3"]);
		unset($pViewData["ResultClass4"]);
		unset($pViewData["ResultClass5"]);
		unset($pViewData["OffsetNGAction1"]);
		unset($pViewData["OffsetNGAction2"]);
		unset($pViewData["OffsetNGActionDetailInternal"]);
		unset($pViewData["OffsetNGActionDetailRequest"]);
		unset($pViewData["OffsetNGActionDetailTSR"]);
		unset($pViewData["OffsetNGActionDetailSorting"]);
		unset($pViewData["OffsetNGActionDetailNoAction"]);
		unset($pViewData["Judge"]);

		$pViewData += [
				"InspectionResult"              => Input::get('txtInspectionResult'),
				"CavityMemo"                    => Input::get('txtCavityMemo'),
				"InspectionResultRadio1"        => "",
				"InspectionResultRadio2"        => "",
				"ResultClass1"                  => "",
				"ResultClass2"                  => "",
				"ResultClass3"                  => "",
				"ResultClass4"                  => "",
				"ResultClass5"                  => "",
				"OffsetNGBunrui"                => Input::get('cmbOffsetNGBunrui'),
				"OffsetNGReason"                => Input::get('cmbOffsetNGReason'),
				"OffsetNGAction1"               => "",
				"OffsetNGAction2"               => "",
				"OffsetNGActionDetailInternal"  => "",
				"OffsetNGActionDetailRequest"   => "",
				"OffsetNGActionDetailTSR"       => "",
				"OffsetNGActionDetailSorting"   => "",
				"OffsetNGActionDetailNoAction"  => "",
				"ActionDetails"                 => Input::get('cmbActionDetails'),
				"TecReqNo"                      => Input::get('txtTecReqNo'),
				"TecReqPerson"                  => Input::get('cmbTecReqPerson'),
				"TSRNo"                         => Input::get('txtTSRNo'),
				"InspectionToolClass"           => (String)Input::get('cmbInspectonToolClass'),
				"Status"                        => "",
				"StatusColorClass"              => "SearchResultTable",
				"hidDataRev"                    => 0,
		];

		$pViewData = $this->setOffsetNGReasonList($pViewData, $WorkScreensetPtn);

		if ($pRowInspectionNo["INSPECTION_RESULT_NO"] != 0)
		{
			$lTblInspectionResultAnalyze = $this->getInspectionResultAnalyzeData($pViewData ,$pRowInspectionNo);
		}


		//if Inspection Result is OK/NG
		if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_OKNG)
		{
			if (Input::get('rdiInspectionResult') == "1")
			{
				data_set($pViewData, 'InspectionResultRadio1'      , true);
			}
			elseif (Input::get('rdiInspectionResult') == "2")
			{
				data_set($pViewData, 'InspectionResultRadio2'      , true);
			}
			else
			{
				//Nothing
			}
		}

		//change position to set as value of Result Class
		if (Input::get('rdiResultClass') == self::RESULT_CLASS_OK)
		{
			data_set($pViewData, 'ResultClass1'      , true);
		}
		elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_OFFSET)
		{
			data_set($pViewData, 'ResultClass2'      , true);

			unset($lViewData["StatusColorClass"]);
			data_set($pViewData, 'StatusColorClass'      , 'OFFSETColor');
		}
		elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_NG)
		{
			data_set($pViewData, 'ResultClass3'      , true);

			unset($pViewData["StatusColorClass"]);
			data_set($pViewData, 'StatusColorClass'      , 'NGColor');
		}
		elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_NO_CHECK)
		{
			data_set($pViewData, 'ResultClass4'      , true);
		}
		elseif (Input::get('rdiResultClass') == self::RESULT_CLASS_LINE_STOP)
		{
			data_set($pViewData, 'ResultClass5'      , true);
		}
		else
		{
			//Nothing
		}

		//change position to set as value of OFFSET Action / NG Action
		if (Input::get('rdiOffsetNGAction') == self::OFFSET_NG_ACTION_RUNNING)
		{
			data_set($pViewData, 'OffsetNGAction1'      , true);
		}
		elseif (Input::get('rdiOffsetNGAction') == self::OFFSET_NG_ACTION_STOP_LINE)
		{
			data_set($pViewData, 'OffsetNGAction2'      , true);
		}
		else
		{
			//Noting
		}

		//set OFFSET Action Detail / NG Action Detail（5）
		//Internal Approve
		if (Input::get('chkOffsetNGActionDetailInternal') == "1")
		{
			data_set($pViewData, 'OffsetNGActionDetailInternal'      , true);
		}
		//Request Offset
		if (Input::get('chkOffsetNGActionDetailRequest') == "1")
		{
			data_set($pViewData, 'OffsetNGActionDetailRequest'      , true);
		}
		//TSR
		if (Input::get('chkOffsetNGActionDetailTSR') == "1")
		{
				data_set($pViewData, 'OffsetNGActionDetailTSR'      , true);
		}
		//Sorting
		if (Input::get('chkOffsetNGActionDetailSorting') == "1")
		{
			data_set($pViewData, 'OffsetNGActionDetailSorting'      , true);
		}
		//No Action
		if (Input::get('chkOffsetNGActionDetailNoAction') == "1")
		{
			data_set($pViewData, 'OffsetNGActionDetailNoAction'      , true);
		}

		return $pViewData;
	}


	//**************************************************************************
	// process         setviewDataScreenAnalResult
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setviewDataScreenAnalResult($pViewData, $pRowInspectionNo)
	{
		$pViewData += [
				"Xbar"                          => 0,
				"Max"                           => 0,
				"Min"                           => 0,
				"Range"                         => 0,
				"Stdev"                         => 0,
				"JudgeMin"                      => 0,
				"JudgeMax"                      => 0,
				"Judge"                         => "",
				"JudgeReason"                   => "",
				"JudgeColorClass"               => "SearchResultTable",
		];


		if ($pRowInspectionNo["INSPECTION_RESULT_NO"] != 0)
		{
			$lTblInspectionResultAnalyze = $this->getInspectionResultAnalyzeData($pViewData ,$pRowInspectionNo);
		}

		if (count($lTblInspectionResultAnalyze) != 0)
		{
			$lRowInspectionResultAnalyze = (Array)$lTblInspectionResultAnalyze[0];

			data_set($pViewData, 'Xbar'         , $lRowInspectionResultAnalyze["AVERAGE_VALUE"]);
			data_set($pViewData, 'Max'          , $lRowInspectionResultAnalyze["MAX_VALUE"]);
			data_set($pViewData, 'Min'          , $lRowInspectionResultAnalyze["MIN_VALUE"]);
			data_set($pViewData, 'Range'        , $lRowInspectionResultAnalyze["RANGE_VALUE"]);
			data_set($pViewData, 'Stdev'        , $lRowInspectionResultAnalyze["STDEV"]);
			data_set($pViewData, 'JudgeReason'  , $lRowInspectionResultAnalyze["JUDGE_REASON"]);

			//Get Value for Judge
			//in case the first inspection or all inspection value is not same
			if ($lRowInspectionResultAnalyze["STDEV"] != 0)
			{
				data_set($pViewData, 'JudgeMin'  , number_format(($lRowInspectionResultAnalyze["AVERAGE_VALUE"] - $lRowInspectionResultAnalyze["STDEV"] * 3),4));
				data_set($pViewData, 'JudgeMax'  , number_format(($lRowInspectionResultAnalyze["AVERAGE_VALUE"] + $lRowInspectionResultAnalyze["STDEV"] * 3),4));
			}
			else
			{
				// data_set($pViewData, 'JudgeMin'  , number_format($lRowInspectionResultAnalyze["AVERAGE_VALUE"],4));
				// data_set($pViewData, 'JudgeMax'  , number_format($lRowInspectionResultAnalyze["AVERAGE_VALUE"],4));
				//Nothing to do
			}

			//if Judge is No Good
			if ($pViewData["JudgeMin"] == 0 && $pViewData["JudgeMax"] == 0)
			{
				//Nothing
			}
			else
			{
				Log::write('info', '108');
				if(($pViewData["NGUnder"] > $pViewData["JudgeMin"]) or ($pViewData["NGUnder"] > $pViewData["JudgeMax"]) or ($pViewData["NGOver"] < $pViewData["JudgeMin"]) or ($pViewData["NGOver"] < $pViewData["JudgeMax"]))
				{
					Log::write('info', '109');
					//Change Judge Color
					unset($pViewData["JudgeColorClass"]);
					data_set($pViewData, 'JudgeColorClass'      , 'NGColor');
					Log::write('info', '110');
				}
			}

		}

		return $pViewData;
	}

	//**************************************************************************
	// process         ErrorCheck1
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ErrorCheck1()
	{
		//in case the number is entried
		if (Input::get('hidInspectionResultInputType') == self::RESULT_INPUT_TYPE_NUMBER)
		{
			//03/May/2018 Hoshina Comment Nocheck&LineStop is can't input.
			// //must check Inspection Result
			// $lValidator = Validator::make(
			// 	array('txtInspectionResult' => Input::get('txtInspectionResult')),
			// 	array('txtInspectionResult' => array('required'))
			// );
			// if ($lValidator->fails())
			// {
			// 	throw new Exception("E021 : Enter Inspection Result.");
			// }

			// //check type of Inspection Result
			// $lValidator = Validator::make(
			// 	array('txtInspectionResult' => Input::get('txtInspectionResult')),
			// 	array('txtInspectionResult' => array('numeric'))
			// );
			// if ($lValidator->fails())
			// {
			// 	throw new Exception("E022 : Inspection Result is invalid.");
			// }
			//must check Inspection Result
			$lValidator = Validator::make(
				array('txtInspectionResult' => Input::get('txtInspectionResult')),
				array('txtInspectionResult' => array('required'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E021 : Enter Inspection Result.");
			}

			//check type of Inspection Result
			$lValidator = Validator::make(
				array('txtInspectionResult' => Input::get('txtInspectionResult')),
				array('txtInspectionResult' => array('numeric'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E022 : Inspection Result is invalid.");
			}


		}
		else
		{
			//03/May/2018 Hoshina Modify -Nocheck&LineStop is can't input-
			// $lValidator = Validator::make(
			// 	array('rdiInspectionResult' => Input::get('rdiInspectionResult')),
			// 	array('rdiInspectionResult' => array('required'))
			// );
			// if ($lValidator->fails())
			// {
			// 	throw new Exception("E021 : Enter Inspection Result.");
			// }
			$lValidator = Validator::make(
				array('rdiInspectionResult' => Input::get('rdiInspectionResult')),
				array('rdiInspectionResult' => array('required'))
			);
			if ($lValidator->fails())
			{
				$lValidator = Validator::make(
					array('rdiResultClass' => Input::get('rdiResultClass')),
					array('rdiResultClass' => array('required'))
				);
				if ($lValidator->fails())
				{
					throw new Exception("E021XX1 : Enter Inspection Result.");
				}

				if ((Input::get('rdiResultClass','') == self::RESULT_CLASS_OK) or (Input::get('rdiResultClass','') == self::RESULT_CLASS_OFFSET) or (Input::get('rdiResultClass','') == self::RESULT_CLASS_NG))
				{
					throw new Exception("E021XX2 : Enter Inspection Result.");
				}
			}
		}
	}

	//**************************************************************************
	// process         ErrorCheck2
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ErrorCheck2()
	{
		//in case Result Class is OFFSET/NG
		if ((Input::get('rdiResultClass','') == self::RESULT_CLASS_OFFSET) or (Input::get('rdiResultClass','') == self::RESULT_CLASS_NG))
		{
			$lValidator = Validator::make(
				array('cmbOffsetNGReason' => Input::get('cmbOffsetNGReason')),
				array('cmbOffsetNGReason' => array('required'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E023 : Enter OFFSET Reason / NG Reason.");
			}

			$lValidator = Validator::make(
				array('rdiOffsetNGAction' => Input::get('rdiOffsetNGAction')),
				array('rdiOffsetNGAction' => array('required'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E024 : Select OFFSET Action / NG Action.");
			}
		}
	}

	//**************************************************************************
	// process         ErrorCheck3
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ErrorCheck3($pCalculateArrayData, $pAfterCalculateData)
	{
		if ($pAfterCalculateData["CalcFlg"] == true)
		{
			if ($pAfterCalculateData["JudgeMinValue"] == 0 && $pAfterCalculateData["JudgeMaxValue"] == 0)
			{
				//Nothing
			}
			else
			{
				if(Input::has('rdiResultClass') and Input::get('rdiResultClass') == 1)
				{
					// Status = OK
					// Not thing to do
				}
				else
				{
					// Status = NG
					if(($pCalculateArrayData["StdSpecUnder"] > $pAfterCalculateData["JudgeMinValue"]) or ($pCalculateArrayData["StdSpecOver"] < $pAfterCalculateData["JudgeMaxValue"]))
					{
						$lValidator = Validator::make(
							array('cmbJudgeReason' => Input::get('cmbJudgeReason')),
							array('cmbJudgeReason' => array('required'))
						);
						if ($lValidator->fails())
						{
							throw new Exception("E052 : Enter Reason.");
						}

						if(Input::get('cmbJudgeReason') == 0)
						{
							throw new Exception("E052 : Enter Reason.");
						}
					}	
				}
			}
		}
	}

	//**************************************************************************
	// process         convertDateFormat
	// overview        change data type
	// argument        date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//                 pattern(1:English->Japanese 2:Japanese->English)
	// return value    
	// author          s-miyamoto
	// date            Ver.01  2014.08.26 v1.00 FIX
	// remarks         
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}

	//**************************************************************************
	// process         OutputLog01
	//**************************************************************************
	private function OutputLog01($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"BA1010 Inspection Result No."  => Session::get('BA1010InspectionResultNo'),
				"BA2020 Inspection Result No."  => Session::get('BA2020InspectionResultNo'),
				// "ViewData"                      => $lViewData,
			]
		);

		return null;
	}

	//**************************************************************************
	// process         OutputLog02
	//**************************************************************************
	private function OutputLog02($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Inspection No."           => Input::get('txtInspectionNo', ''),
				"Inspection No.(Hidden)"   => Input::get('hidInspectionNo', ''),
			]
		);

		return null;
	}

	//**************************************************************************
	// process         OutputLog03
	//**************************************************************************
	private function OutputLog03($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Inspection No."            => Input::get('txtInspectionNo'                  ,''),
				"Inspection No.(Hidden)"    => Input::get('hidInspectionNo'                  ,''), 
				"Inspection Result"         => Input::get('txtInspectionResult'              ,''),
				"Result Class"              => Input::get('rdiResultClass'                   ,''),
				"OFFSET/NG Bunrui"          => Input::get('cmbOffsetNGBunrui'                ,0),
				"OFFSET/NG Reason"          => Input::get('cmbOffsetNGReason'                ,''),
				"OFFSET/NG Action"          => Input::get('rdiOffsetNGAction'                ,''),
				"OFFSET/NG Action Detail1"  => Input::get('chkOffsetNGActionDetailInternal'  ,'0'),
				"OFFSET/NG Action Detail2"  => Input::get('chkOffsetNGActionDetailRequest'   ,'0'),
				"OFFSET/NG Action Detail3"  => Input::get('chkOffsetNGActionDetailTSR'       ,'0'),
				"OFFSET/NG Action Detail4"  => Input::get('chkOffsetNGActionDetailSorting'   ,'0'),
				"OFFSET/NG Action Detail5"  => Input::get('chkOffsetNGActionDetailNoAction'  ,'0'),
				"Action Details"            => Input::get('cmbActionDetails'                 ,''),
				"TecReqNo"                  => Input::get('txtTecReqNo     '                 ,''),
				"TecReqPerson"              => Input::get('cmbTecReqPerson'               	 ,''),
				"TSRNo"                     => Input::get('txtTSRNo'                         ,''),
				"Equipment"                 => Input::get('cmbInspectonToolClass'            ,''),
			]
		);

		return null;
	}




}
