<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;




use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;


use DB;
use Log;
use Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');

//**************************************************************************
// display:	 Monitoring
// overview: For Plasess Company
// author:	Mai Nawaphat(Mai ^^)
// date:	21/2/2018
//**************************************************************************
class BA3010MonitoringController
extends Controller
{
	CONST NUMBER_KETA = 10;               //2桁にする
	CONST NUMBER_TIME24 = 24;             //マスタ時間に合わせる
	CONST MASTER_TIME_A = 23;             //マスタの時間IDが0～23
	CONST MASTER_TIME_B = 24;             //マスタの時間IDが1～24
	CONST NUMBER_PER_PAGE = 20;   //data count per 1 page

	public function MasterAction()
	{   
		$lViewData             = [];
		$lArrayData            = [];
		$lTblHeaderData        = [];
		$lTblDetailData        = [];
		$lPagenation           = [];

		$ID24                  = "";
		$lCount                = 0;

		$lViewData += [
			"UserID"   => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];
        
		//Get Server Time
		$time = new Carbon(Carbon::now());
		//Change show hour
		$time = $time->hour;
		//Get Date
		$today = date("Y/m/d");
		$yesterday = date('Y/m/d', strtotime('-1 day'));

          
		//==make==//
		//Get header
		$lTblScreenHeaderData = $this->getCheckTimeData1();
		
		if ($lTblScreenHeaderData != null)
		{
			foreach ($lTblScreenHeaderData as $lRow1)
			{
				$lRow1 = (Array)$lRow1;
				$lCount = $lCount + 1;

				$lArrayData += [
					"ID".$lCount  => (String)$lRow1["INSPECTION_TIME_ID"]
				];

				$lViewData += [
					"Name".$lCount  => (String)$lRow1["INSPECTION_TIME_NAME"],
				];
			}
		}
// dd($lArrayData);
		//Get Detail
		$lTblDetailData = $this->getDetailData1($lArrayData, $today);
// dd($lTblDetailData);
		if(isset($lTblDetailData))
		{
			// $lPagenation = Paginator::make($lTblDetailData,Count($lTblDetailData));
			$lPagenation = new LengthAwarePaginator ($lTblDetailData, Count($lTblDetailData), self::NUMBER_PER_PAGE);
			$lPagenation->setPath(url('user/monitoring'));
		}
		else
		{
		    $lPagenation = null;
		}

		$lViewData += [
			"Pagenator"       => $lPagenation,
		];

		return View("user.monitoring", $lViewData);

	}

	//**************************************************************************
	// process         getCheckTimeData1
	// overview        select master data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckTimeData1()
	{
		$lTblData = [];

		$lTblData = DB::select
		('
			SELECT INSPECTION_TIME_ID
					,INSPECTION_TIME_NAME
			  FROM TINSPTIM
			 WHERE DELETE_FLG = "0"
			 order by DISPLAY_ORDER
		',
			[
			]
		);
		
		return $lTblData;
	}

	//**************************************************************************
	// display:	 getDetailData1
	// overview: For Plasess Company
	// author:	Mai Nawaphat(Mai ^^)
	// date:	21/2/2018
	//**************************************************************************
	private function getDetailData1($pArrayData, $ptoday)
	{	 

		$lTblData = [];

		$lTblData = DB::select('SELECT
			CASE WHEN MST.CUSTOMER_NAME <> "" THEN MST.CUSTOMER_NAME
				 ELSE YOJITU.CUSTOMER_NAME
			 END AS CUSTOMER_NAME
			,MCN.MACHINE_NAME AS MACHINE_NAME
			,CONCAT(MST.CUSTOMER_NAME, MST.ITEM_NO, MST.ITEM_NAME,MCN.MACHINE_NAME) AS RENKETSU
			,YOJITU.LOT_NO  
			,YOJITU.INSPECTION_SHEET_NO
			,CASE WHEN MST.INSPECTION_SHEET_NAME <> "" THEN MST.INSPECTION_SHEET_NAME
				  ELSE YOJITU.INSPECTION_SHEET_NAME
			 END AS INSPECTION_SHEET_NAME

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU1 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU1 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU1 = YOJITU.KENSA_JISSEKI_SU1 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI1
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU1 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU1
				  ELSE " "
			 END AS ERROR_SU1

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU2 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU2 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU2 = YOJITU.KENSA_JISSEKI_SU2 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI2
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU2 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU2
				  ELSE " "
			 END AS ERROR_SU2

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU3 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU3 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU3 = YOJITU.KENSA_JISSEKI_SU3 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI3
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU3 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU3
				  ELSE " "
			 END AS ERROR_SU3

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU4 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU4 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU4 = YOJITU.KENSA_JISSEKI_SU4 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI4
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU4 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU4
				  ELSE " "
			 END AS ERROR_SU4

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU5 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU5 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU5 = YOJITU.KENSA_JISSEKI_SU5 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI5
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU5 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU5
				  ELSE " "
			 END AS ERROR_SU5

			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
					CASE WHEN YOJITU.YOTEI_SU6 = "0" THEN " "
						 WHEN YOJITU.KENSA_JISSEKI_SU6 = "0" THEN "△"
						 WHEN YOJITU.YOTEI_SU6 = YOJITU.KENSA_JISSEKI_SU6 THEN "●"
						 ELSE "※"
					END
				  ELSE " "
			 END AS JOTAI6
			,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU6 = "0" THEN " "
				  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU6
				  ELSE " "
			 END AS ERROR_SU6

			-- ,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			-- 		CASE WHEN YOJITU.YOTEI_SU7 = "0" THEN " "
			-- 			 WHEN YOJITU.KENSA_JISSEKI_SU7 = "0" THEN "△"
			-- 			 WHEN YOJITU.YOTEI_SU7 = YOJITU.KENSA_JISSEKI_SU7 THEN "●"
			-- 			 ELSE "※"
			-- 		END
			-- 	  ELSE " "
			--  END AS JOTAI7
			-- ,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU7 = "0" THEN " "
			-- 	  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU7
			-- 	  ELSE " "
			--  END AS ERROR_SU7

			-- ,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			-- 		CASE WHEN YOJITU.YOTEI_SU8 = "0" THEN " "
			-- 			 WHEN YOJITU.KENSA_JISSEKI_SU8 = "0" THEN "△"
			-- 			 WHEN YOJITU.YOTEI_SU8 = YOJITU.KENSA_JISSEKI_SU8 THEN "●"
			-- 			 ELSE "※"
			-- 		END
			-- 	  ELSE " "
			--  END AS JOTAI8
			-- ,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU8 = "0" THEN " "
			-- 	  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU8
			-- 	  ELSE " "
			--  END AS ERROR_SU8

			FROM
					(
						SELECT SHEET.INSPECTION_SHEET_NO
								,MAX(SHEET.REV_NO) AS REV_NO
								,SHEET.INSPECTION_SHEET_NAME
								,SHEET.PROCESS_ID
								,SHEET.CUSTOMER_ID
								,CUSTM.CUSTOMER_NAME
								,SHEET.ITEM_NO AS ITEM_NO
								,TITEM.ITEM_NAME AS ITEM_NAME
						  FROM TISHEETM AS SHEET
			   LEFT OUTER JOIN TITEMMST AS TITEM
							ON TITEM.ITEM_NO = SHEET.ITEM_NO
						   AND TITEM.DELETE_FLG = "0"
			   LEFT OUTER JOIN TCUSTOMM AS CUSTM
							ON CUSTM.CUSTOMER_ID = SHEET.CUSTOMER_ID
						   AND CUSTM.DELETE_FLG = "0"
						 WHERE SHEET.DELETE_FLG = "0"
					  GROUP BY SHEET.PROCESS_ID
							  ,SHEET.INSPECTION_SHEET_NO
					) MST

		RIGHT OUTER JOIN
						(
							SELECT SUB.INSPECTION_SHEET_NO
									,SUB.INSPECTION_SHEET_NAME
									,SUB.REV_NO
									,SUB.PROCESS_ID
									,SUB.CUSTOMER_ID
									,SUB.CUSTOMER_NAME
									,SUB.MACHINE_NO AS MACHINE_NO	/*予定と実績を完全結合したものから実績のマシンNoを優先させる*/
									,SUB.MOLD_NO
									,SUB.INSPECTION_YMD
									,SUB.INSPECTION_TIME_ID
									,SUB.LOT_NO
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID1 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU1	/*1列目の検査予定数*/
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID1 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU1	/*1列目の検査実績数*/
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID1 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU1	/*1列目のエラー数*/
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID2 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU2
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID2 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU2
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID2 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU2
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID3 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU3
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID3 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU3
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID3 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU3
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID4 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU4
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID4 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU4
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID4 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU4
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID5 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU5
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID5 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU5
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID5 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU5
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID6 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU6
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID6 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU6
									,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID6 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU6
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID7 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU7
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID7 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU7
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID7 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU7
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID8 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU8
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID8 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU8
									-- ,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID8 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU8
							FROM
									(
										SELECT TRH.INSPECTION_SHEET_NO
												,SHEET.INSPECTION_SHEET_NAME
												,TRH.REV_NO
												,TRH.PROCESS_ID
												,SHEET.CUSTOMER_ID
												,CUSTM.CUSTOMER_NAME
												,TRH.MACHINE_NO AS MACHINE_NO
												,TRH.MOLD_NO
												,TRH.INSPECTION_YMD
												,TRH.INSPECTION_TIME_ID
												,TRH.LOT_NO
												,COUNT(TRT.INSPECTION_RESULT_NO) AS YOTEI_SU
												,SUM(CASE WHEN TRT.INSPECTION_RESULT_VALUE = "" THEN "0" ELSE "1" END) AS KENSA_JISSEKI_SU
												-- NG,OFFSETかつLINE STOPをエラーとする
												,SUM(CASE WHEN TRT.INSPECTION_RESULT_TYPE in ("2","3") AND TRT.OFFSET_NG_ACTION = "1" THEN "1" ELSE "0" END) AS ERROR_SU
												-- ,SUM(CASE WHEN TRT.INSPECTION_RESULT_TYPE in ("2","3") AND TRT.OFFSET_NG_ACTION = "2" THEN "1" ELSE "0" END) AS ERROR_SU
										  FROM TRESHEDT AS TRH
						 				INNER JOIN TRESDETT AS TRT
												ON TRH.INSPECTION_RESULT_NO = TRT.INSPECTION_RESULT_NO
						 				INNER JOIN TISHEETM AS SHEET
												ON SHEET.INSPECTION_SHEET_NO = TRH.INSPECTION_SHEET_NO
											   AND SHEET.REV_NO = TRH.REV_NO
						 		   LEFT OUTER JOIN TCUSTOMM AS CUSTM
												ON CUSTM.CUSTOMER_ID = SHEET.CUSTOMER_ID
										 WHERE TRH.INSPECTION_YMD = :TODAY
									  GROUP BY TRH.INSPECTION_SHEET_NO
											  ,TRH.REV_NO
											  ,TRH.MACHINE_NO
											  ,TRH.MOLD_NO
											  ,TRH.INSPECTION_YMD
											  ,TRH.INSPECTION_TIME_ID
									UNION ALL
										SELECT SHHET.INSPECTION_SHEET_NO
												,SHHET.INSPECTION_SHEET_NAME
												,SHHET.REV_NO
												,SHHET.PROCESS_ID
												,SHHET.CUSTOMER_ID
												,CUSTM.CUSTOMER_NAME
												,"" AS MACHINE_NO
												,"" AS MOLD_NO
												,"" AS INSPECTION_YMD
												,"" AS INSPECTION_TIME_ID
												,"" AS LOT_NO
												,0 AS YOTEI_SU
												,0 AS KENSA_JISSEKI_SU
												,0 AS ERROR_SU
										  FROM TISHEETM AS SHHET
						 	   LEFT OUTER JOIN TCUSTOMM AS CUSTM
											ON CUSTM.CUSTOMER_ID = SHHET.CUSTOMER_ID
										 WHERE SHHET.DELETE_FLG = "0"
									  GROUP BY SHHET.INSPECTION_SHEET_NO
											  ,SHHET.REV_NO
									) SUB
							GROUP BY SUB.INSPECTION_SHEET_NO
									,SUB.REV_NO
									,SUB.PROCESS_ID
									,SUB.MACHINE_NO
									,SUB.MOLD_NO
									,SUB.INSPECTION_YMD
									,SUB.INSPECTION_TIME_ID
									,SUB.LOT_NO
						) YOJITU
					 ON YOJITU.PROCESS_ID          = MST.PROCESS_ID
					AND YOJITU.INSPECTION_SHEET_NO = MST.INSPECTION_SHEET_NO
					AND YOJITU.REV_NO              = MST.REV_NO		

		LEFT OUTER JOIN tmachinm AS MCN
					 ON YOJITU.MACHINE_NO = MCN.MACHINE_NO
					AND MCN.DELETE_FLG = "0"
				  WHERE YOJITU.MACHINE_NO is not null
		
			   ORDER BY MST.CUSTOMER_NAME
					   ,MST.ITEM_NO
					   ,YOJITU.MACHINE_NO
        	',[
					"YID1"       => $pArrayData["ID1"],
					"YID2"       => $pArrayData["ID2"],
					"YID3"       => $pArrayData["ID3"],
					"YID4"       => $pArrayData["ID4"],
					"YID5"       => $pArrayData["ID5"],
					"YID6"       => $pArrayData["ID6"],
					// "YID7"       => $pArrayData["ID7"],
					// "YID8"       => $pArrayData["ID8"],

					"JID1"       => $pArrayData["ID1"],
					"JID2"       => $pArrayData["ID2"],
					"JID3"       => $pArrayData["ID3"],
					"JID4"       => $pArrayData["ID4"],
					"JID5"       => $pArrayData["ID5"],
					"JID6"       => $pArrayData["ID6"],
					// "JID7"       => $pArrayData["ID7"],
					// "JID8"       => $pArrayData["ID8"],

					"EID1"       => $pArrayData["ID1"],
					"EID2"       => $pArrayData["ID2"],
					"EID3"       => $pArrayData["ID3"],
					"EID4"       => $pArrayData["ID4"],
					"EID5"       => $pArrayData["ID5"],
					"EID6"       => $pArrayData["ID6"],
					// "EID7"       => $pArrayData["ID7"],
					// "EID8"       => $pArrayData["ID8"],

					"TODAY"      => $ptoday,
        	]);

		return $lTblData;
	}




	private function getID1($time , $num){
		$ID = $time - $num; //表時刻1列目（現在時刻の23時間前）$result=array_diff($a1,$a2);
		if ($ID < 0) //1列目が昨日にわたる場合
		{
			$ID = $ID + self::NUMBER_TIME24; //マスタ時間に合わせるため24時間プラスする
		}
		if ($ID < self::NUMBER_KETA) //2桁以下になる場合
		{
			$ID = sprintf('%02d', $ID); //マスタの表記に合わせるため前0処理を行う
		}
		return $ID;
	}

	private function getDAY1($ID ,$today, $yesterday){
        $DAY = $today;
		if ($ID < 0)
		{
			$DAY = $yesterday;
		}
		return $DAY;
	}

	private function getID2($time , $num){
		$ID = $time - $num; //表時刻1列目（現在時刻の23時間前）

		if ($ID <= 0) //1列目が昨日にわたる場合
		{
			$ID = $ID + self::NUMBER_TIME24; //マスタ時間に合わせるため24時間プラスする
		}
		if ($ID < self::NUMBER_KETA) //2桁以下になる場合
		{ 
			$ID = sprintf('%02d', $ID); //マスタの表記に合わせるため前0処理を行う
		}
		return $ID;
	}

	private function getDAY2($ID ,$today, $yesterday){
		$DAY = $today;
		if ($ID <= 0) //1列目が昨日にわたる場合
		{
			$DAY = $yesterday; //日付を昨日にする
	    }
	    return $DAY;
	}
}