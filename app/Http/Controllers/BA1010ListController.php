<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Model\tcodemst;
use App\Model\tcustomm;
use App\Model\tinsptim;
use App\Model\tisheetm;
use App\Model\tmachinm;
use App\Model\tusermst;
use App\Model\ttimedett;
use App\Model\tptnmst;
use App\Model\tinspnom;
use App\Model\treshedt;
use App\Model\tresdett;
use App\Model\tresanal;

use App\Model\ImportfileModel;

use App\Csv\InspectionResultCsv;
use App\Csv\DataImportNGCsv;
use App\Excel\ApprovalExcel;
use App\Excel\NGResultExcel;
use App\Excel\InspectionsheetExcel;

use App\Parts\calculate;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;

use Auth;
use DB;
use Log;
use Session;
use Validator;
use Redirect;
use Response;
use Carbon\Carbon;
use Exception;
// use PHPExcel; 
// use PHPExcel_IOFactory; 

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
// use PhpOffice\PhpSpreadsheet\Writer\Csv;

//Laravel VerUp From 4.2 To 5.6
// set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
// require 'PHPExcel.php';
// include_once 'PHPExcel/IOFactory.php';
// include_once 'PHPExcel/Writer/Excel2007.php';



class BA1010ListController
extends Controller
{
	//data count per 1 page
	CONST NUMBER_PER_PAGE      = 10;
	CONST CONVDATA_ARG         = "1";
	CONST CONDITION_NORMAL     = "01";
	CONST INSPECTION_TIME_ALL  = "ZZ";
	CONST RESULT_INPUT_TYPE_NUMBER = "1";
	
	//Result Class
	CONST RESULT_CLASS_OK            = "1"; //OK
	CONST RESULT_CLASS_OFFSET        = "2"; //OFFSET
	CONST RESULT_CLASS_NG            = "3"; //NG
	CONST RESULT_CLASS_NO_CHECK      = "4"; //NO CHECK
	CONST RESULT_CLASS_LINE_STOP     = "5"; //LINE STOP

	CONST TIMECLASS            = "051";

	//Data import
	CONST MITUTOYO_CRYSTAAPEX_S544  = "001";

	CONST BlankSpace  = " ";
	CONST DataImportPath  = "C:/laravel/factoryz/public/excel/WorkDataImport/";


	//**************************************************************************
	// process    ListAction
	// overview   Display screen for inspection result
	//           And process as Entry,Search,CSV buttuns
	// argument   Nothing
	// return value    Nothing
	// author    s-miyamoto
	// date    14.05.2014
	// record of updates  14.05.2014 v0.01 make first
	//           26.08.2014 v1.00 FIX
	//**************************************************************************
	public function ListAction()
	{
		$lViewData               = []; //Array for transition to screen
		$lPagenation             = []; //For Paging（Return to screen）
		$lTblSearchResultData    = []; //Data table of inspection result
		$lTblNotExistNoData      = []; //Data table of Inspection No(No Data)

		$lTblCheckSheetInfo      = []; //DataTable of Inspection Check Sheet
		$lRowCheckSheetInfo      = []; //DataRow of Inspection Check Sheet

		$lTblModifyTargetData    = []; //DataTable for Modify
		$lRowModifyTargetData    = []; //DataRow for Modify

		$lCheckTimeData          = [];

		// $lTblCustomerID          = [];
		// $lRowCustomerID          = [];

		$lInspectionResultNo     = ""; //PK of line
		$lDataRev                = ""; //DataRevision of line

		$lDataCount              = 0;  //data counter for redundant logic key
		// $lCheckNoCount           = 0;  //number of Inspection No. corresponding to the Time
		$lDeleteCount            = 0;  //number of data deleted
		$lSheetMasterDataCount   = 0;  //data counter for redundant logic key

		//CSV
		$lOutputCSVData          = ""; //CSV data for output
		$lMimeSetting            = ""; //MIME setting

		//Data import
		$lArrayNGData            = [];
		$lDataImportNGreason     = "";
		$llinsertupdateNGFlg     = false;

		//Value to make Combo(Set value from session when transit to screen)
		$lCmbValProcessForEntry       = "";
		$lCmbValCustomerForEntry      = "";
		$lCmbValCheckSheetNoForEntry  = "";
		$lCmbValProcessForSearch      = "";
		$lCmbValCustomerForSearch     = "";
		$lCmbValCheckSheetNoForSearch = "";
		$lWorkProcessEntry            = "";
		$lWorkCustomerEntry           = "";
		$lWorkProcessSearch           = "";
		$lWorkCustomerSearch          = "";
		$lWorkCheckSheetSearch        = "";

		$lPageReturnCd                = 0;


		//store value of entering to screen
		Input::flash();

		//get parameter from login screen throgh session and issue to array for transpotion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];
        
		// echo "<pre>";
		// print_r($lViewData);
		// echo "</pre>";

		if (is_null(Session::get('BA1010FirstOpenJudgeFlg')))
		{
			//session put
			$this->ScreenOpenSessionput();
		}

		//Inspection No List of Entry screen and Equipment list(because it must delete at transition to other screen)
		//clear at first of List screen
		//If data was deleted while Return button in Entry screen, data can't delete from menu
		Session::forget('BA2010InspectionNoData');
		Session::forget('BA2010InspectionToolClassDropdownListData');

	try
	{
		if (Input::has('btnEntry'))                                             //Push Entry button
		{
			$this->OutputLog01("BA1010 Entry Btn Click");

			//error check
			$this->RequiredInputCheckEntry();

			$lEntryArrayData = []; 

			$lWorkInspectionDate  = "";
			$lWorkInspectionDate  = $this->convertDateFormat((String)Input::get('txtInspectionDateForEntry'), self::CONVDATA_ARG);

			$lEntryArrayData += [
				"CheckSheetNo"        => Input::get('cmbCheckSheetNoForEntry'),
				"CustomerId"          => TRIM((String)Input::get('cmbCustomerForEntry')),
				"ProcessId"           => Input::get('cmbProcessForEntry'),
				"MachineNo"           => TRIM((String)Input::get('txtMachineNoForEntry')),
				"MoldNo"              => TRIM((String)Input::get('txtMoldNoForEntry')),
				"InspectorCode"       => TRIM((String)Input::get('txtInspectorCodeForEntry')),
				"InspectionDate"      => $lWorkInspectionDate,
				"ConditionId"         => Input::get('cmbConditionForEntry'),
				"InspectionTimeId"    => Input::get('cmbInspectionTimeForEntry'),
			];

			$lTblCheckSheetInfo = $this->getCheckSheet($lEntryArrayData["CheckSheetNo"]);
			$lRowCheckSheetInfo = (array)$lTblCheckSheetInfo[0];

			if ($lRowCheckSheetInfo["INSPECTION_SHEET_NO"] != null)
			{
				$lEntryArrayData += [
					"RevisionNo"          => $lRowCheckSheetInfo["MAX_REV_NO"],
					"TinsptimCodeNo"      => $lRowCheckSheetInfo["TINSPTIM_CODE_NO"],
				];
			}
			else
			{
				throw new Exception("E008 : Check Sheet No. is not registered in Master. ");
			}


			$lTblMachine = [];
			$objtmachinm = new tmachinm;
			$lTblMachine = $objtmachinm->getMachineData($lEntryArrayData["MachineNo"]);
			if (count($lTblMachine) == 0)
			{
				throw new Exception("E011 : M/C No. is not registered in Master. ");
			}


			$lTblInspector = [];
			$objtusermst = new tusermst;
			$lTblInspector = $objtusermst->getInspectorData($lEntryArrayData["InspectorCode"]);
			if (count($lTblInspector) == 0)
			{
				throw new Exception("E014 : Inspector Code is not registered in Master.");
			}


			$lSheetMasterDataCount = $this->getInspectionSheetMasterDataCount($lEntryArrayData);
			if ($lSheetMasterDataCount == 0)
			{
				throw new Exception("E997 : Target data does not exist.");
			}
			else if ($lSheetMasterDataCount == 1)
			{
				//Nothing
			}
			else
			{
				//RevNoがupする場合は2以上になる為、何もしない
			}

			//07/May/2018 Hoshina Comment This check is Shin-ei Check.とりあえず消さずに残しておく
			// $lCheckNoCount = $this->getInspectionTimeMasterDataCount($lEntryArrayData);
			// if ($lCheckNoCount == 0)
			// {
			// 	throw new Exception("E032 : You mischose the time of Inspection.");
			// }

			$lTblSheetTime = [];
			$lRowSheetTime = [];
			$objtcodemst = new tcodemst;
			$lTblSheetTime = $objtcodemst->getCodeInfo2(self::TIMECLASS, $lEntryArrayData["TinsptimCodeNo"]);
			if (count($lTblSheetTime) == 0)
			{
				throw new Exception("E067 : Inspection Time is not registered in Master. ");
			}
			else
			{
				$lRowSheetTime = (array)$lTblSheetTime[0];
			}

			$lTblTimeDetail = [];
			$lRowTimeDetail = [];
			$objtimedett = new ttimedett;
			$lTblTimeDetail = $objtimedett->getTimedetailData($lEntryArrayData["TinsptimCodeNo"], $lEntryArrayData["InspectionTimeId"]);
			if (count($lTblTimeDetail) == 0)
			{
				throw new Exception("E032 : You mischose the time of Inspection.");
			}
			else
			{
				$lRowTimeDetail = (array)$lTblTimeDetail[0];
			}

			//check duplication data in the same logic key except for no error
			//prompt to continue to regist from Modify in case of having registed by same logic key already
			//既に同一論理キーで登録されている場合は、検索一覧のModifyの方から登録の続きを行うように促す
			//Condition combo＝nomal(01)⇒check,else⇒no check（available to regist duplication in the same time）
			if ($lRowTimeDetail["INPUTNUMBER_JUDGE_FLG"] == 1)
			{
				If (Input::get('cmbConditionForEntry') == self::CONDITION_NORMAL)
				{
					$lDataCount = $this->getLogicalDuplicationDataCount($lEntryArrayData);
					if ($lDataCount > 0)
					{
						throw new Exception("E031 : The data of the time is already registered. Search the data and retry the process with Modify Button. ");
					}
				}
			}

			//session put
			$this->ScreenInputItemSessionput();

			//session initialize
			$this->initializeSessionData();

			//Laravel VerUp From 4.2 To 5.6
			// return Redirect::route("user/preentry");
			return redirect()->route('user.preentry');
		}
		elseif (Input::has('btnSearch'))                                             //Search button
		{
			$this->OutputLog02("BA1010 Search Btn Click");

			//check error
			$this->InputCheckSearch();
			$this->RequiredInputCheckSearch();

			//search
			$lTblSearchResultData = $this->getInspectionResultList();
			// $lTblNotExistNoData   = $this->getNotExistNoList();

			// echo "<pre>";
			// print_r($lTblSearchResultData);
			// echo "</pre>";

			//error check 0 data
			if (count($lTblSearchResultData) == 0)
			{
				throw new Exception("E997 : Target data does not exist.");
			}

			//session put
			$this->ScreenInputItemSessionput();

			//sessionにstore
			Session::put('BA1010InspectionResultData', $lTblSearchResultData);
			Session::put('BA1010NotExistNoData', $lTblNotExistNoData);

		}
		elseif (Input::has('btnCsv'))                                           //CSV button
		{
			$this->OutputLog02("BA1010 CSV Btn Click");

			//check error
			$this->InputCheckSearch();

			$lCsvArrayData = []; 

			$lWorkInspectionDateFrom  = "";
			$lWorkInspectionDateTo    = "";
			$lWorkProductionDateFrom  = "";
			$lWorkProductionDateTo    = "";

			$lWorkInspectionDateFrom  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), self::CONVDATA_ARG);
			$lWorkInspectionDateTo    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), self::CONVDATA_ARG);
			$lWorkProductionDateFrom  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), self::CONVDATA_ARG);
			$lWorkProductionDateTo    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), self::CONVDATA_ARG);

			$lCsvArrayData += [
				"ProcessId"           => TRIM((String)Input::get('cmbProcessForSearch')),
				"CustomerId"          => TRIM((String)Input::get('cmbCustomerForSearch')),
				"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
				"RevisionNo"          => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				"MachineNo"           => TRIM((String)Input::get('txtMachineNoForSearch')),
				"MoldNo"              => TRIM((String)Input::get('txtMoldNoForSearch')),
				"InspectorCode"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
				"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
				"InspectionDateFrom"  => $lWorkInspectionDateFrom,
				"InspectionDateTo"    => $lWorkInspectionDateTo,
				"ProductionDateFrom"  => $lWorkProductionDateFrom,
				"ProductionDateTo"    => $lWorkProductionDateTo,
				"InspectionTimeId"    => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
			];

			$objInspectionResultCsv = new InspectionResultCsv;
			$lOutputCSVData = $objInspectionResultCsv->getCsvData($lCsvArrayData);
 
			if (is_null($lOutputCSVData))
			{
				throw new Exception("E997 : Target data does not exist.");
			}
			else
			{
				$lMimeSetting = array(
					'Content-Type' => 'text/csv',
					'Content-Disposition' => 'attachment; filename="InspectionResultData.csv"',
				);

				//download
				return Response::make(rtrim($lOutputCSVData, "\n"), 200, $lMimeSetting);
			}
		}
		elseif (Input::has('btnShoninData'))                                    //Excel（for approval） button
		{
			$this->OutputLog02("BA1010 Excel Btn Click");

			//check error
			$this->InputCheckSearch();
			$this->RequiredInputCheckSearch();
			$this->RequiredInputCheckApproval();

			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
				//Nothing
			}
			else
			{
				throw new Exception("E041 : Do Not Enter Inspection Time .");
			}

			$lApprovalExcelArrayData = []; 

			$lWorkInspectionDateFrom  = "";
			$lWorkInspectionDateFrom  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), self::CONVDATA_ARG);

			$lApprovalExcelArrayData += [
				"ProcessId"           => TRIM((String)Input::get('cmbProcessForSearch')),
				"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
				"RevisionNo"          => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				"MachineNo"           => TRIM((String)Input::get('txtMachineNoForSearch')),
				"MoldNo"              => TRIM((String)Input::get('txtMoldNoForSearch')),
				"InspectorCode"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
				"InspectionDateFrom"  => $lWorkInspectionDateFrom,
			];

			$objApprovalExcel = new ApprovalExcel;
			$lOutputApprovalExcelData = $objApprovalExcel->getApprovalExcel($lApprovalExcelArrayData);

			if (is_null($lOutputApprovalExcelData))
			{
				throw new Exception("E997 : Target data does not exist.");
			}
			else
			{
				$lViewData["NormalMessage"] = "I005 : Process has been completed.";
			}
		}
		elseif (Input::has('btnShoninData2'))                                   //NGResult Excel button
		{
			$this->OutputLog02("BA1010 NGResult Btn Click");

			//check error
			$this->InputCheckSearch();
			$this->RequiredInputCheckSearch();

			$lNGResultExcelArrayData = []; 

			$lWorkInspectionDateFrom  = "";
			$lWorkInspectionDateTo    = "";
			$lWorkProductionDateFrom  = "";
			$lWorkProductionDateTo    = "";

			$lWorkInspectionDateFrom  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), self::CONVDATA_ARG);
			$lWorkInspectionDateTo    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), self::CONVDATA_ARG);
			$lWorkProductionDateFrom  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), self::CONVDATA_ARG);
			$lWorkProductionDateTo    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), self::CONVDATA_ARG);

			$lNGResultExcelArrayData += [
				"ProcessId"           => TRIM((String)Input::get('cmbProcessForSearch')),
				"CustomerId"          => TRIM((String)Input::get('cmbCustomerForSearch')),
				"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
				"RevisionNo"          => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				"MachineNo"           => TRIM((String)Input::get('txtMachineNoForSearch')),
				"MoldNo"              => TRIM((String)Input::get('txtMoldNoForSearch')),
				"InspectorCode"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
				"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
				"InspectionDateFrom"  => $lWorkInspectionDateFrom,
				"InspectionDateTo"    => $lWorkInspectionDateTo,
				"ProductionDateFrom"  => $lWorkProductionDateFrom,
				"ProductionDateTo"    => $lWorkProductionDateTo,
				"InspectionTimeId"    => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
			];

			$objNGResultExcel = new NGResultExcel;
			$lOutputNGResultExcelData = $objNGResultExcel->getNGResultExcel($lNGResultExcelArrayData);

			if (is_null($lOutputNGResultExcelData))
			{
				throw new Exception("E997 : Target data does not exist.");
			}
			else
			{
				$lViewData["NormalMessage"] = "I005 : Process has been completed.";
			}
		}
		elseif (Input::has('btnModify'))                                             //Modify button
		{
			$this->OutputLog03("BA1010 Modify Btn Click");

			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			$lDataRev = Input::get('hidDataRev');

			$lTblModifyTargetData = $this->getInspectionResultData($lInspectionResultNo, $lDataRev);

			if (count($lTblModifyTargetData) == 0)
			{
				throw new Exception("E998 : Data has been updated by another terminal. Try search again.");
			}
			else
			{
				$lRowModifyTargetData = (Array)$lTblModifyTargetData[0];
				
				//store items in session and transport to use in Modify screen
				Session::put('BA1010InspectionResultNo'			, $lInspectionResultNo);

				//session put
				$this->ScreenInputItemSessionput();

				//Laravel VerUp From 4.2 To 5.6
				// return Redirect::route("user/entry");
				return redirect()->route('user.entry');
			}
		}
		elseif (Input::has('btnLineStop'))                                    //LineStop button
		{
			$this->OutputLog03("BA1010 LineStop Btn Click");

			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			$lDataRev = Input::get('hidDataRev');

			if ($lInspectionResultNo != "")
			{
				$this->updateLineStopData($lInspectionResultNo, $lDataRev, $lViewData["UserID"]);

				$lViewData["NormalMessage"] = "I005 : Process has been completed.";

				//session initialize
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnDelete'))                                    //Delete button
		{
			$this->OutputLog03("BA1010 Delete Btn Click");

			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			$lDataRev = Input::get('hidDataRev');

			if ($lInspectionResultNo != "")
			{
				$lDeleteCount = $this->deleteInspectionResultData($lInspectionResultNo, $lDataRev);

				if ($lDeleteCount == 0)
				{
					throw new Exception("E998 : Data has been updated by another terminal. Try search again.");
				}
				else
				{
					$lViewData["NormalMessage"] = "I004 : Delete has been completed.";
				}

				//session initialize
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnPrintInspectionSheet'))                          //Inspection Sheet button
		{
			$this->OutputLog03("BA1010 InspSheet Btn Click");

			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			$lDataRev = Input::get('hidDataRev');

			if ($lInspectionResultNo != null)
			{
				$objInspectionsheetExcel = new InspectionsheetExcel;
				$lOutputInspectionSheetExcelData = $objInspectionsheetExcel->getIsnpectionSheetExcel($lInspectionResultNo, $lDataRev);

 				if (is_null($lOutputInspectionSheetExcelData))
				{
					throw new Exception("E997 : Target data does not exist.");
				}
				else
				{
					//OK
				}
			}
			else
			{
				throw new Exception("E998 : Data has been updated by another terminal. Try search again.");
			}
		}
		elseif (Input::has('btnDataImport'))                          //Data Import button
		{
			$this->OutputLog03("BA1010 DataImport Btn Click");

			$lImportInspectionResultNo = Input::get('hidInspectionResultNo');
			$lImportDataRev = Input::get('hidDataRev');

			if ($lImportInspectionResultNo != null)
			{
				//Check File
				$this->checkDataImport();

				$lTblHeader = [];
				$lRowHeader = [];
				$objtreshedt = new treshedt;
				$lTblHeader = $objtreshedt->getHeaderData($lImportInspectionResultNo, $lImportDataRev);
				if (count($lTblHeader) == 0)
				{
					throw new Exception("E997 : Target data does not exist.");
				}
				else
				{
					$lRowHeader = (array)$lTblHeader[0];
				}

				$lTblInspectorInfo = [];
				$lRowInspectorInfo = [];
				$objtusermst = new tusermst;
				$lTblInspectorInfo = $objtusermst->getInspectorData(Session::get('AA1010UserID'));
				if (count($lTblInspectorInfo) == 0)
				{
					throw new Exception("E014 : Inspector Code is not registered in Master.");
				}
				else
				{
					$lRowInspectorInfo = (array)$lTblInspectorInfo[0];
				}

				$lTblDataPatarn = [];
				$lRowDataPatarn = [];
				$objtptnmst = new tptnmst;
				$lTblDataPatarn = $objtptnmst->getPatarnData(self::MITUTOYO_CRYSTAAPEX_S544);
				if (count($lTblDataPatarn) == 0)
				{
					throw new Exception("E997 : Target data does not exist.");
				}
				else
				{
					$lRowDataPatarn = (array)$lTblDataPatarn[0];
				}	

				$lImportFileName = Input::file('filUploadFile')->getClientOriginalName();

				copy(Input::file('filUploadFile') ,self::DataImportPath.basename(Input::file('filUploadFile')->getClientOriginalName()));
				$lOutputFileName1 = "OutputNo01_".$lViewData["UserID"]."_".date("Ymd")."_".date("His")."_".$lImportFileName;
				$lOutputFileName2 = "OutputNo02_".$lViewData["UserID"]."_".date("Ymd")."_".date("His")."_".$lImportFileName.".csv";
				$lOutputFile2 = self::DataImportPath.$lOutputFileName2;

				// exec("cmd.exe /c test.bat param1 param2");
				$lCommand1st = $lRowDataPatarn["CALL_CONTENTS1"].self::BlankSpace.self::DataImportPath.$lImportFileName.self::BlankSpace.self::DataImportPath.$lOutputFileName1;
				// $lCommand1st = "cmd.exe /c C:/usr/local/OutputTalendModule/test_job_FromatConv_Plasess_0.1/test_job_FromatConv_Plasess/test_job_FromatConv_Plasess_run.bat C:/laravel/factoryz/public/excel/WorkDataImport/".$lImportFileName." C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName1;
				exec($lCommand1st, $RtnOut1, $RtnSts1);

				// Log::write('info', '1',
				// 	[
				// 		"RtnSts1"     => $RtnSts1,
				// 		"Command1st"  => $lCommand1st,
				// 	]
				// );
					
				if ($RtnSts1 == 0)
				{
					//Delete copy file
					unlink(self::DataImportPath.$lImportFileName);

					// $lCommand2nd = $lRowDataPatarn["CALL_CONTENTS2"].self::BlankSpace.self::DataImportPath.$lOutputFileName1.self::BlankSpace.self::DataImportPath.$lOutputFileName2.self::BlankSpace.$lRowHeader["INSPECTION_SHEET_NO"].self::BlankSpace.$lRowHeader["REV_NO"];
					$lCommand2nd = $lRowDataPatarn["CALL_CONTENTS2"].self::BlankSpace.self::DataImportPath.$lOutputFileName1.self::BlankSpace.self::DataImportPath.$lOutputFileName2.self::BlankSpace.$lRowHeader["INSPECTION_SHEET_NO"].self::BlankSpace.$lRowHeader["REV_NO"].self::BlankSpace.Input::get('txtSampleNoForSearch');

					exec($lCommand2nd, $RtnOut2, $RtnSts2);

					// Log::write('info', '2',
					// 	[
					// 		"RtnSts2"     => $RtnSts2,
					// 		"OutputFile2" => $lOutputFile2,
					// 		"Command2nd"  => $lCommand2nd,
					// 	]
					// );

					if ($RtnSts2 == 0)
					{
						//Delete Output1 file
						unlink(self::DataImportPath.$lOutputFileName1);

// $lOutputFile2 = "C:/laravel/factoryz/public/excel/WorkDataImport/OutputNo02.csv";
						$lopenfile = fopen($lOutputFile2, "r");

						//Start Transaction
						DB::beginTransaction();

						while ($array = fgetcsv($lopenfile))
						{
							$objImportfile = new ImportfileModel;
							$lTblImportfile = $objImportfile->ReadLine($array);

							$lTblDetail = [];
							$lRowDetail = [];
							$objtresdett = new tresdett;
							// $lTblDetail = $objtresdett->getDetailData($objImportfile->$InspectionResultNo, $objImportfile->$InspectionNo);
							$lTblDetail = $objtresdett->getDetailData($lTblImportfile["InspectionResultNo"], $lTblImportfile["InspectionNo"]);
							if (count($lTblDetail) == 0)
							{
								$llinsertupdateNGFlg = true;
								$lDataImportNGreason = "No have inspection detail data choice data mistake";
							}
							else
							{
								$lRowDetail = (array)$lTblDetail[0];

								$lTblInspNo = [];
								$lRowInspNo = [];
								$objtinspnom = new tinspnom;
								// $lTblInspNo = $objtinspnom->getInspNoData($objtreshedt->INSPECTION_SHEET_NO, $objtreshedt->REV_NO, $objImportfile->InspectionPoint);
								$lTblInspNo = $objtinspnom->getInspNoData($lRowHeader["INSPECTION_SHEET_NO"], $lRowHeader["REV_NO"], $lTblImportfile["InspectionPoint"]);

								if (count($lTblInspNo) == 0)
								{
									$llinsertupdateNGFlg = true;
									$lDataImportNGreason = "No have inspection No data choice data mistake";
								}
								else
								{
									$lRowInspNo = (array)$lTblInspNo[0];

									$lResultClass  = $this->judgeResultClass($lTblImportfile, $lRowInspNo);

									if ($lTblImportfile["InspectionResultNo"] != 0)
									{
										$lCalculateArrayData = [];
										$lCalculateArrayData += [
											"InspectionResultValue"  => $lTblImportfile["InspectionResultValue"],
											"InspectionResultNo"     => $lTblImportfile["InspectionResultNo"],
											"InspectionNo"           => $lTblImportfile["InspectionNo"],
											"InspectionPoint"        => $lTblImportfile["InspectionPoint"],
											"AnalGrp01"              => $lRowDetail["ANALYTICAL_GRP_01"],
											"AnalGrp02"              => $lRowDetail["ANALYTICAL_GRP_02"],
											"StdSpecUnder"           => $lRowInspNo["THRESHOLD_NG_UNDER"],
											"StdSpecOver"            => $lRowInspNo["THRESHOLD_NG_OVER"],
										];

										//Calculate
										$objcsv = new calculate;
										$lAfterCalculateData = $objcsv->getCalculate($lCalculateArrayData, self::RESULT_INPUT_TYPE_NUMBER, $lResultClass);
										if (is_null($lAfterCalculateData))
										{
											$llinsertupdateNGFlg = true;
											$lDataImportNGreason = "data Calculate error";
										}
										else
										{
											$tablemodeltreshedt = new treshedt;
											$tablemodeltreshedt = $this->setHeaderData($lCalculateArrayData, $tablemodeltreshedt);
											$tablemodeltresdett = new tresdett;
											$tablemodeltresdett = $this->setDetailData($lCalculateArrayData, $lAfterCalculateData, $lResultClass, $tablemodeltresdett);
											$tablemodeltresanal = new tresanal;
											$tablemodeltresanal = $this->setAnalyData($lCalculateArrayData, $lAfterCalculateData, $lRowHeader, $lRowInspNo, $lRowInspectorInfo, $tablemodeltresanal);

											$this->insertAndUpdateResult($lAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett, $tablemodeltresanal);

											//add Revision No. in header
											Session::put('BA1010ModifyDataRev', Session::get('BA1010ModifyDataRev',0) + 1);
										}
									}
								}
							}

							$lArrayNGData  = $this->setarray($lTblImportfile, $lDataImportNGreason, $lArrayNGData);
						}

						if ($llinsertupdateNGFlg == true)
						{
							DB::rollback();
						}
						else
						{
							DB::commit();
							//Delete Output1 file
							unlink(self::DataImportPath.$lOutputFileName2);
						}

						fclose($lopenfile);
					}
					else
					{
						throw new Exception("E069 : An error occurred during processing(2).");
					}
				}
				else
				{
					throw new Exception("E068 : An error occurred during processing(1).");
				}

				if ($llinsertupdateNGFlg == true)
				{
					$objDataImportNGCsv = new DataImportNGCsv;
					$lOutputNGCsv = $objDataImportNGCsv->createCSV($lArrayNGData);
 
					$lMimeSetting = array(
						'Content-Type' => 'text/csv',
						'Content-Disposition' => 'attachment; filename="ImportNGData.csv"',
					);

					//download
					return Response::make(rtrim($lOutputNGCsv, "\n"), 200, $lMimeSetting);
				}
			}
			else
			{
				throw new Exception("E998 : Data has been updated by another terminal. Try search again.");
			}
		}
		else                                                                    //transition, paging from other screen or menu
		{
			//当画面のセッション情報のうち、画面入力情報以外の情報を全てクリアする（検索結果等を消して、検索前の状態に戻すため）
			//遷移元のURLに「index.php/user/list含まない場合」（＝他画面からの遷移の場合）＝False
			//遷移元がそれ以外の場合は文字列が返ってくる（Falseにはならない）
			//clear information except for entry in screen in session（delete search result and restore before state
			//in case index.php/user/list do not include in URL,(＝transition from other screen）＝False
			//don't be False
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				// Log::write('info', '1');
				//Pagenationかプルダウンなどのイベントをきっていない処理の場合はここを通る
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/list');
				$lPageReturnCd = 1;

				if($lPrevURL == false)
				{
					// Log::write('info', '2');
					//他画面から遷移してきた場合はここを通る
					$this->initializeSessionData();
					
					//if transition from other screen,get value from session
					$lCmbValProcessForEntry = Session::get('BA1010ProcessIdForEntry');
					$lCmbValCustomerForEntry = Session::get('BA1010CustomerIdForEntry');
					$lCmbValCheckSheetNoForEntry = Session::get('BA1010CheckSheetNoForEntry');
					
					$lCmbValProcessForSearch = Session::get('BA1010ProcessIdForSearch');
					$lCmbValCustomerForSearch = Session::get('BA1010CustomerIdForSearch');
					$lCmbValCheckSheetNoForSearch = Session::get('BA1010CheckSheetNoForSearch');
					
					//上に場所移動
					//delete information of search（reset）
					//$this->initializeSessionData();

					$lPageReturnCd = 2;
				}
			}
		}


		//以下の処理は他画面またはメニューからの遷移、ページング時、ボタン処理時等、全ての場合で通過させる
		//■must pass (transition,paging,button, from other screen, menu)

	}catch(Exception $e) 
		{
			$lViewData["errors"] = new MessageBag([
				"error" => $e->getMessage()
			]);

		}

		//set list
		$lViewData = $this->setProcessList($lViewData);
		$lViewData = $this->setCustomerList($lViewData);
		$lViewData = $this->setConditionList($lViewData);
		$lViewData = $this->setInspectionTimeList($lViewData);


		if (Session::get('BA0000LastScreenInfomation') == "BA1010")
		{
			if ($lPageReturnCd != 1)
			{
				//session put
				$this->ScreenInputItemSessionput();
			}
		}

		//store and set again entry in screen
		$lViewData = $this->setListForms($lViewData);

		//↓↓ Add Hoshina 04-Aug-2017 ↓↓
		//-----------------------------
		//set combo ①etc (for Entry)
		//-----------------------------
//		if (Input::has('txtMasterNoForEntry') && Input::has('cmbProcessForEntry'))
//		{
//			//in case value exist in customer infomation for search
//			$lViewData = $this->setCustomerNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), "arrDataListCustomerList");
//
//			if (count($lViewData['arrDataListCustomerList']) == 1)
//			{
//				$lTblCustomerID = $this->getCustomerID(Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'));
//				
//				if($lTblCustomerID != null)
//				{
//					$lRowCustomerID = (Array)$lTblCustomerID[0];
//					//in case value exist in customer infomation for search
//					//$pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey
//					$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], "arrCheckSheetNoForEntryList");
//
//						if (Input::has('cmbCheckSheetNoForEntry'))
//						{
//							//in case value exist in customer infomation for search
//							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialNameForEntryList");
//							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialSizeForEntryList");
//						}
//						else
//						{
//							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
//							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
//						}
//				}
//			}
//		}
		//↑↑

		//-----------------------------
		//set combo InspectionSheetNo (for Entry)
		//-----------------------------
		// if (Input::has('cmbProcessForEntry') && Input::has('cmbCustomerForEntry'))
		// {
		// 	$lWorkProcessEntry = Input::get('cmbProcessForEntry');
		// 	$lWorkCustomerEntry = Input::get('cmbCustomerForEntry');
		// }
		// else
		// {
		// 	$lWorkProcessEntry = $lCmbValProcessForEntry;
		// 	$lWorkCustomerEntry = $lCmbValCustomerForEntry;
		// }
		// $lViewData = $this->setCheckSheetNoListEntry2($lViewData, $lWorkProcessEntry, $lWorkCustomerEntry, "arrCheckSheetNoForEntryList");

		if (Input::has('cmbCustomerForEntry'))
		{
			$lWorkCustomerEntry = Input::get('cmbCustomerForEntry');
		}
		else
		{
			$lWorkCustomerEntry = $lCmbValCustomerForEntry;
		}
		$lViewData = $this->setCheckSheetNoListEntry1($lViewData, $lWorkCustomerEntry, "arrCheckSheetNoForEntryList");


		//-----------------------------
		//set combo Inspection Sheet No (for Search)
		//-----------------------------
		// if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch'))
		// {
		// 	$lWorkProcessSearch = Input::get('cmbProcessForSearch');
		// 	$lWorkCustomerSearch = Input::get('cmbCustomerForSearch');
		// }
		// else
		// {
		// 	$lWorkProcessSearch = $lCmbValProcessForSearch;
		// 	$lWorkCustomerSearch = $lCmbValCustomerForSearch;
		// }
		// $lViewData = $this->setCheckSheetNoListSearch2($lViewData, $lWorkProcessSearch, $lWorkCustomerSearch, "arrCheckSheetNoForSearchList");

		if (Input::has('cmbCustomerForSearch'))
		{
			$lWorkProcessSearch = Input::get('cmbProcessForSearch');
			$lWorkCustomerSearch = Input::get('cmbCustomerForSearch');
		}
		else
		{
			$lWorkProcessSearch = $lCmbValProcessForSearch;
			$lWorkCustomerSearch = $lCmbValCustomerForSearch;
		}
		$lViewData = $this->setCheckSheetNoListSearch1($lViewData, $lWorkCustomerSearch, "arrCheckSheetNoForSearchList");


		//-----------------------------
		//set combo Revision No (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch') && Input::has('cmbCheckSheetNoForSearch'))
		{
			$lWorkProcessSearch = Input::get('cmbProcessForSearch');
			$lWorkCustomerSearch = Input::get('cmbCustomerForSearch');
			$lWorkCheckSheetSearch = Input::get('cmbCheckSheetNoForSearch');
		}
		else
		{
			$lWorkProcessSearch = $lCmbValProcessForSearch;
			$lWorkCustomerSearch = $lCmbValCustomerForSearch;
			$lWorkCheckSheetSearch = $lCmbValCheckSheetNoForSearch;
		}
		$lViewData = $this->setRevNoList($lViewData, $lWorkProcessSearch, $lWorkCustomerSearch, $lWorkCheckSheetSearch);



		$lViewData += [
			"InspectionDateForEntry"       => Input::get('txtInspectionDateForEntry'),
			"ProductionDateFromForSearch"  => Input::get('txtProductionDateFromForSearch'),
			"ProductionDateToForSearch"    => Input::get('txtProductionDateToForSearch'),
			"InspectionDateFromForSearch"  => Input::get('txtInspectionDateFromForSearch'),
			"InspectionDateToForSearch"    => Input::get('txtInspectionDateToForSearch'),
		];

		if (is_null(Session::get('BA1010InspectionResultData')))
		{
			$lTblSearchResultData = [];
			// $lTblNotExistNoData   = [];
		}
		else
		{
			//pop up data in session
			$lTblSearchResultData = Session::get('BA1010InspectionResultData');
			// $lTblNotExistNoData   = Session::get('BA1010NotExistNoData');
		}

		// echo "<pre>";
		// print_r($lViewData);
		// echo "</pre>";

		//make pagenation
		//data,total number of issue,number per 1page
		//Laravel VerUp From 4.2 To 5.6
		// $lPagenation = Paginator::make($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData, Count($lTblSearchResultData), self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/list'));

		//add to array for transportion to screen(written in +=))
		$lViewData += [
			"Pagenator"       => $lPagenation,
			// "NotExistNoData"  => $lTblNotExistNoData,
		];

  		//Laravel VerUp From 4.2 To 5.6
		// return View::make("user/list", $lViewData);
		return View("user.list", $lViewData);
	}



	//************************************************************************************************************************************
	//************************************************************************************************************************************
	//************************************************************************************************************************************
	//************************************************************************************************************************************


	//**************************************************************************
	// process         setListForms
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setListForms($pViewData)
	{
		//entry & Search
		$pViewData += [
				"ProcessForEntry"				=> Session::get('BA1010ProcessIdForEntry'),
				"CustomerForEntry"				=> Session::get('BA1010CustomerIdForEntry'),
				"CheckSheetNoForEntry"			=> Session::get('BA1010CheckSheetNoForEntry'),
				"MachineNoForEntry"				=> Session::get('BA1010MachineNoForEntry'),
				"MoldNoForEntry"				=> Session::get('BA1010MoldNoForEntry'),
				"InspectorCodeForEntry"			=> Session::get('BA1010InspectorCodeForEntry'),
				"InspectionDateForEntry"		=> Session::get('BA1010InspectionDateForEntry'),
				"ConditionForEntry"				=> Session::get('BA1010ConditionIdForEntry'),
				"InspectionTimeForEntry"		=> Session::get('BA1010InspectionTimeIdForEntry'),
				"ProcessForSearch"				=> Session::get('BA1010ProcessIdForSearch'),
				"CustomerForSearch"				=> Session::get('BA1010CustomerIdForSearch'),
				"CheckSheetNoForSearch"			=> Session::get('BA1010CheckSheetNoForSearch'),
				"RevisionNoForSearch"			=> Session::get('BA1010RevisionNoForSearch'),
				"MachineNoForSearch"			=> Session::get('BA1010MachineNoForSearch'),
				"MoldNoForSearch"				=> Session::get('BA1010MoldNoForSearch'),
				"InspectorCodeForSearch"		=> Session::get('BA1010InspectorCodeForSearch'),
				"ProductionDateFromForSearch"	=> Session::get('BA1010ProductionDateFromForSearch'),
				"ProductionDateToForSearch"		=> Session::get('BA1010ProductionDateToForSearch'),
				"InspectionDateFromForSearch"	=> Session::get('BA1010InspectionDateFromForSearch'),
				"InspectionDateToForSearch"		=> Session::get('BA1010InspectionDateToForSearch'),
				"PartNoForSearch"				=> Session::get('BA1010PartNoForSearch'),
				"SampleNoForSearch"				=> Session::get('BA1010SampleNoForSearch'),
				"InspectionTimeForSearch"		=> Session::get('BA1010InspectionTimeIdForSearch'),
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setProcessList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setProcessList($pViewData)
	{
		$lArrProcessList = ["" => ""];

		$objtcodemst = new tcodemst;
		$lArrProcessList = $objtcodemst->getProcessList();

		$pViewData += [
			"arrDataListProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setCustomerList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCustomerList($pViewData)
	{
		$lArrCustomerList = ["" => ""];

		$objtcustomm = new tcustomm;
		$lArrCustomerList = $objtcustomm->getCustomerList();

		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList,
			"arrDataListCustomerForSearchList" => $lArrCustomerList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setConditionList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks     
	//**************************************************************************
	private function setConditionList($pViewData)
	{
		$lArrDataListCondition = ["" => ""];

		$objtcodemst = new tcodemst;
		$lArrDataListCondition = $objtcodemst->getConditionList();

		$pViewData += [
			"arrDataListCondition" => $lArrDataListCondition
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setInspectionTimeList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks     
	//**************************************************************************
	private function setInspectionTimeList($pViewData)
	{
		$lArrDataListInspectionTime = ["" => ""];

		$objtinsptim = new tinsptim;
		$lArrDataListInspectionTime = $objtinsptim->getInspectionTimeList();

		$pViewData += [
			"arrDataListInspectionTime" => $lArrDataListInspectionTime
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setCheckSheetNoListEntry1
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks     
	//**************************************************************************
	private function setCheckSheetNoListEntry1($pViewData, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];

		$objtisheetm = new tisheetm;
		$lArrCheckSheetNoList = $objtisheetm->getCheckSheetNoList1($pCustomerId);

		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setCheckSheetNoListEntry2
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks     
	//**************************************************************************
	private function setCheckSheetNoListEntry2($pViewData, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];

		$objtisheetm = new tisheetm;
		$lArrCheckSheetNoList = $objtisheetm->getCheckSheetNoList2($pProcessId, $pCustomerId);

		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setCheckSheetNoListSearch1
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks      
	//**************************************************************************
	private function setCheckSheetNoListSearch1($pViewData, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];

		$objtisheetm = new tisheetm;
		$lArrCheckSheetNoList = $objtisheetm->getCheckSheetNoList1($pCustomerId);

		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setCheckSheetNoListSearch2
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks      
	//**************************************************************************
	private function setCheckSheetNoListSearch2($pViewData, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];

		$objtisheetm = new tisheetm;
		$lArrCheckSheetNoList = $objtisheetm->getCheckSheetNoList2($pProcessId, $pCustomerId);

		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         setRevNoList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks      
	//**************************************************************************
	private function setRevNoList($pViewData, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lArrRevNoList     = ["" => ""];

		$objtisheetm = new tisheetm;
		$lArrRevNoList = $objtisheetm->getRevNoList($pProcessId, $pCustomerId, $pInspectionSheetNo);

		$pViewData += [
			"arrRevNoForSearchList" => $lArrRevNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getInspectionSheetMasterDataCount
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectionSheetMasterDataCount($pEntryArrayData)
	{
		$lTblInspectionSheetMasterDataCount = [];
		$lRowInspectionSheetMasterDataCount = [];
		$lInspectionSheetMasterDataCount    = 0;

			$lTblInspectionSheetMasterDataCount = DB::select('
				    SELECT COUNT(SHEET.INSPECTION_SHEET_NO) AS COUNT
				      FROM TISHEETM AS SHEET
				     WHERE SHEET.INSPECTION_SHEET_NO  = :InspectionSheetNo
				       AND SHEET.CUSTOMER_ID          = :Customer
				       AND SHEET.DELETE_FLG           = "0"
			',
				[
					"Customer"            => $pEntryArrayData["CustomerId"],
					"InspectionSheetNo"   => $pEntryArrayData["CheckSheetNo"],
				]
			);

		//read result(1 line)
		$lRowInspectionSheetMasterDataCount = $lTblInspectionSheetMasterDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowInspectionSheetMasterDataCount = (Array)$lRowInspectionSheetMasterDataCount;

		//get count
		$lInspectionSheetMasterDataCount = $lRowInspectionSheetMasterDataCount["COUNT"];

		return $lInspectionSheetMasterDataCount;
	}

	//**************************************************************************
	// process         getLogicalDuplicationDataCount
	// overview        get count of redundant data of logic key 論理キー重複データ件数取得
	// argument        
	// return value    Int（count of redundant data >=0 or 1）
	// author  　　　　 s-miyamoto
	// date            Ver.01 2014.08.26 v1.00 FIX
	// remarks         Ver.02
	//**************************************************************************
	private function getLogicalDuplicationDataCount($pEntryArrayData)
	{
		$lTblLogicalDupulicationDataCount = [];
		$lRowLogicalDupulicationDataCount = [];
		$lDataCount                       = 0;

			$lTblLogicalDupulicationDataCount = DB::select('
					SELECT COUNT("X") AS COUNT
					  FROM TRESHEDT AS HEAD
					 WHERE INSPECTION_SHEET_NO = :InspectionSheetNo
					   AND REV_NO              = :RevisionNo
					   AND PROCESS_ID          = :Process
					   AND MACHINE_NO          = :MachineNo
					   AND MOLD_NO             = :MoldNo
					   -- AND INSERT_USER_ID      = :InspectorCode
					   AND INSPECTION_YMD      = :InspectionDate
					   AND CONDITION_CD        = :Condition
					   AND INSPECTION_TIME_ID  = :InspectionTime
			',
				[
					"InspectionSheetNo" => $pEntryArrayData["CheckSheetNo"],
					"RevisionNo"        => $pEntryArrayData["RevisionNo"],
					"Process"           => $pEntryArrayData["ProcessId"],
					"MachineNo"         => $pEntryArrayData["MachineNo"],
					"MoldNo"            => $pEntryArrayData["MoldNo"],
					// "InspectorCode"     => $pEntryArrayData["InspectorCode"],
					"InspectionDate"    => $pEntryArrayData["InspectionDate"],
					"Condition"         => $pEntryArrayData["ConditionId"],
					"InspectionTime"    => $pEntryArrayData["InspectionTimeId"],
				]
			);

		//read result(1 line)
		$lRowLogicalDupulicationDataCount = $lTblLogicalDupulicationDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowLogicalDupulicationDataCount = (Array)$lRowLogicalDupulicationDataCount;

		//get count
		$lDataCount = $lRowLogicalDupulicationDataCount["COUNT"];

		return $lDataCount;
	}

	//**************************************************************************
	// process         getInspectionTimeMasterDataCount
	// overview        greturn count corresponding to Check
	// argument        
	// return value    Int（count of Inspection No. data corresponding to the TIME >=0 or 1）
	// author  　　　　 s-miyamoto
	// date            Ver.01 2014.08.26 v1.00 FIX
	// remarks         Ver.02
	//**************************************************************************
	private function getInspectionTimeMasterDataCount($pEntryArrayData)
	{
		$lTblCheckNoDataCount = [];
		$lRowCheckNoDataCount = [];
		$lDataCount           = 0;
		$lWorkTime            = "";

		If ($pEntryArrayData["ConditionId"] == self::CONDITION_NORMAL) 
		{
			$lWorkTime = $pEntryArrayData["InspectionTimeId"];
		}
		else
		{
			$lWorkTime = self::INSPECTION_TIME_ALL;
		}

			$lTblCheckNoDataCount = DB::select('
				    SELECT COUNT(ISTM.INSPECTION_TIME_ID) AS NO_COUNT
				      FROM TINSPNOM AS ISNO
				INNER JOIN TISHEETM AS SHEET
				        ON SHEET.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
				       AND SHEET.REV_NO              = ISNO.REV_NO
				INNER JOIN TINSNTMM AS ISTM
				        ON ISTM.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
				       AND ISTM.INSPECTION_POINT    = ISNO.INSPECTION_POINT
				     WHERE ISNO.INSPECTION_SHEET_NO = :InspectionSheetNo
				       AND ISNO.REV_NO              = :RevisionNo
				       AND ISTM.INSPECTION_TIME_ID  = :InspectionTime
				       AND SHEET.DELETE_FLG         = "0"
				       AND ISNO.DELETE_FLG          = "0"
				       AND ISTM.DELETE_FLG          = "0"
			',
				[
					"InspectionSheetNo" => $pEntryArrayData["CheckSheetNo"],
					"RevisionNo"        => $pEntryArrayData["RevisionNo"],
					"InspectionTime"    => $lWorkTime,
				]
			);

		//read result(1 line)
		$lRowCheckNoDataCount = $lTblCheckNoDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowCheckNoDataCount = (Array)$lRowCheckNoDataCount;

		//get count
		$lDataCount = $lRowCheckNoDataCount["NO_COUNT"];

		return $lDataCount;
	}

	//**************************************************************************
	// process         getCheckSheet
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheet($pCheckSheetNo)
	{
		$lTblCheckSheetData = [];

			$lTblCheckSheetData = DB::select('
			          SELECT SHEE.INSPECTION_SHEET_NO
			                ,MAX(SHEE.REV_NO) AS MAX_REV_NO
			                ,SHEE.TINSPTIM_CODE_NO
			            FROM TISHEETM AS SHEE
			           WHERE SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
			             AND SHEE.DELETE_FLG          = "0"
			',
				[
					"CheckSheetNo" => TRIM((String)$pCheckSheetNo),
				]
			);

		return $lTblCheckSheetData;
	}

	//**************************************************************************
	// process            getInspectionResultList
	// overview           search list of inspection result and return it to calling as Array
	// overview           
	// argument           Nothing
	// return value       Array
	// author             
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultList()
	{
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search
		$lProductionDateFromForSearch  = ""; //date From for search
		$lProductionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), self::CONVDATA_ARG);
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), self::CONVDATA_ARG);

		$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), self::CONVDATA_ARG);
		$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), self::CONVDATA_ARG);

			$lTblSearchResultData = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
			          ,HEAD.PROCESS_ID
			          ,PROCESS.CODE_NAME AS PROCESS_NAME
			          ,HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,HEAD.MACHINE_NO
			          ,MACH.MACHINE_NAME
			          ,HEAD.MOLD_NO
			          ,HEAD.MATERIAL_NAME
			          ,HEAD.MATERIAL_SIZE
			          ,HEAD.COLOR_ID
			          ,HEAD.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,USER.TEAM_ID
			          ,TEAM.CODE_NAME AS TEAM_NAME
			          ,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,HEAD.CONDITION_CD
			          ,COND.CODE_NAME AS CONDITION_NAME
			          ,HEAD.INSPECTION_TIME_ID
			          ,TIME.INSPECTION_TIME_NAME
			          ,HEAD.LOT_NO
			          ,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
			          ,HEAD.DATA_REV
			          ,SHEE.CUSTOMER_ID
			          ,CUSTOMER.CUSTOMER_NAME AS CUSTOMER_NAME
			          ,SHEE.ITEM_NO
			          ,ITEM.ITEM_NAME
			          ,IFNULL(SUB2.NORESULT_COUNT,0) AS NORESULT_COUNT
			          -- ,IFNULL(SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT,0) AS RESULT_COUNT
			          ,IFNULL((SUB1.TOTAL_COUNT-IFNULL(SUB2.NORESULT_COUNT,0)),0) AS RESULT_COUNT /*Modify 28-May-2018 Hoshina Because can not 100%, Before PG is SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT*/
			          ,SUB1.TOTAL_COUNT
			          -- ,ROUND((IFNULL(SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT,0) / SUB1.TOTAL_COUNT) * 100, 2) AS RESULT_RATE
			          ,ROUND((IFNULL((SUB1.TOTAL_COUNT-IFNULL(SUB2.NORESULT_COUNT,0)),0) / SUB1.TOTAL_COUNT) * 100, 2) AS RESULT_RATE /*Modify 28-May-2018 Hoshina Because can not 100%, Before PG is SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT*/
			          
			      FROM TRESHEDT AS HEAD

			INNER JOIN TISHEETM AS SHEE
			        ON HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND HEAD.REV_NO              = SHEE.REV_NO
			INNER JOIN TMACHINM AS MACH
			        ON HEAD.MACHINE_NO          = MACH.MACHINE_NO
			INNER JOIN TCUSTOMM AS CUSTOMER
					ON SHEE.CUSTOMER_ID         = CUSTOMER.CUSTOMER_ID
			INNER JOIN TUSERMST AS USER
			        ON HEAD.INSERT_USER_ID      = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
			        ON HEAD.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
		LEFT OUTER JOIN TITEMMST AS ITEM
			        ON SHEE.ITEM_NO             = ITEM.ITEM_NO

			INNER JOIN TCODEMST AS COND
			        ON COND.CODE_CLASS          = "001"
			       AND HEAD.CONDITION_CD        = COND.CODE_ORDER
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS       = "002"
			       AND HEAD.PROCESS_ID          = PROCESS.CODE_ORDER
		LEFT OUTER JOIN TCODEMST AS TEAM
					ON TEAM.CODE_CLASS          = "004"
			       AND USER.TEAM_ID             = TEAM.CODE_ORDER

			INNER JOIN (
			                SELECT S1HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S1DETL.INSPECTION_NO) AS TOTAL_COUNT
			                  FROM TRESHEDT AS S1HEAD
			            INNER JOIN TRESDETT AS S1DETL
			                    ON S1HEAD.INSPECTION_RESULT_NO = S1DETL.INSPECTION_RESULT_NO
			            INNER JOIN TISHEETM AS S1SHEE
			                    ON S1HEAD.INSPECTION_SHEET_NO = S1SHEE.INSPECTION_SHEET_NO
			                   AND S1HEAD.REV_NO              = S1SHEE.REV_NO
			                 WHERE S1HEAD.PROCESS_ID          = IF(:S1ProcessId1 <> "", :S1ProcessId2, S1HEAD.PROCESS_ID)
			                   AND S1SHEE.CUSTOMER_ID         = IF(:S1CustomerId1 <> "", :S1CustomerId2, S1SHEE.CUSTOMER_ID)
			                   AND S1HEAD.INSPECTION_SHEET_NO LIKE :S1CheckSheetNo
			                   AND S1HEAD.REV_NO              = IF(:S1RevisionNo1 <> "", :S1RevisionNo2, S1HEAD.REV_NO)
			                   AND S1HEAD.MACHINE_NO          = IF(:S1MachineNo1 <> "", :S1MachineNo2, S1HEAD.MACHINE_NO)
			                   AND S1HEAD.MOLD_NO             = IF(:S1MoldNo1 <> "", :S1MoldNo2, S1HEAD.MOLD_NO)
			                   AND S1HEAD.INSERT_USER_ID      = IF(:S1UserID1 <> "", :S1UserID2, S1HEAD.INSERT_USER_ID)
			                   AND S1HEAD.PRODUCTION_YMD      >= IF((:S1ProdDateFrom1 is not null) and (:S1ProdDateFrom2 <> ""), :S1ProdDateFrom3, S1HEAD.PRODUCTION_YMD)
			                   AND S1HEAD.PRODUCTION_YMD      <= IF((:S1ProdDateTo1 is not null) and (:S1ProdDateTo2 <> ""), :S1ProdDateTo3, S1HEAD.PRODUCTION_YMD)
			                   AND S1HEAD.INSPECTION_YMD      >= IF((:S1InspDateFrom1 is not null) and (:S1InspDateFrom2 <> ""), :S1InspDateFrom3, S1HEAD.INSPECTION_YMD)
			                   AND S1HEAD.INSPECTION_YMD      <= IF((:S1InspDateTo1 is not null) and (:S1InspDateTo2 <> ""), :S1InspDateTo3, S1HEAD.INSPECTION_YMD)
			                   AND S1SHEE.ITEM_NO             LIKE :S1PartNo
			                   AND S1HEAD.INSPECTION_TIME_ID  = IF((:S1InspTime1 is not null) and (:S1InspTime2 <> ""), :S1InspTime3, S1HEAD.INSPECTION_TIME_ID)
			              GROUP BY S1HEAD.INSPECTION_RESULT_NO
			            ) SUB1
			        ON HEAD.INSPECTION_RESULT_NO = SUB1.INSPECTION_RESULT_NO
			LEFT OUTER JOIN (
			                SELECT S2HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S2DETL.INSPECTION_NO) AS NORESULT_COUNT
			                  FROM TRESHEDT AS S2HEAD
			            INNER JOIN TRESDETT AS S2DETL
			                    ON S2HEAD.INSPECTION_RESULT_NO = S2DETL.INSPECTION_RESULT_NO
			            INNER JOIN TISHEETM AS S2SHEE
			                    ON S2HEAD.INSPECTION_SHEET_NO = S2SHEE.INSPECTION_SHEET_NO
			                   AND S2HEAD.REV_NO              = S2SHEE.REV_NO
			                 WHERE S2HEAD.PROCESS_ID          = IF(:S2ProcessId1 <> "", :S2ProcessId2, S2HEAD.PROCESS_ID)
			                   AND S2SHEE.CUSTOMER_ID         = IF(:S2CustomerId1 <> "", :S2CustomerId2, S2SHEE.CUSTOMER_ID)
			                   AND S2HEAD.INSPECTION_SHEET_NO LIKE :S2CheckSheetNo
			                   AND S2HEAD.REV_NO              = IF(:S2RevisionNo1 <> "", :S2RevisionNo2, S2HEAD.REV_NO)
			                   AND S2HEAD.MACHINE_NO          = IF(:S2MachineNo1 <> "", :S2MachineNo2, S2HEAD.MACHINE_NO)
			                   AND S2HEAD.MOLD_NO             = IF(:S2MoldNo1 <> "", :S2MoldNo2, S2HEAD.MOLD_NO)
			                   AND S2HEAD.INSERT_USER_ID      = IF(:S2UserID1 <> "", :S2UserID2, S2HEAD.INSERT_USER_ID)
			                   AND S2HEAD.PRODUCTION_YMD      >= IF((:S2ProdDateFrom1 is not null) and (:S2ProdDateFrom2 <> ""), :S2ProdDateFrom3, S2HEAD.PRODUCTION_YMD)
			                   AND S2HEAD.PRODUCTION_YMD      <= IF((:S2ProdDateTo1 is not null) and (:S2ProdDateTo2 <> ""), :S2ProdDateTo3, S2HEAD.PRODUCTION_YMD)
			                   AND S2HEAD.INSPECTION_YMD      >= IF((:S2InspDateFrom1 is not null) and (:S2InspDateFrom2 <> ""), :S2InspDateFrom3, S2HEAD.INSPECTION_YMD)
			                   AND S2HEAD.INSPECTION_YMD      <= IF((:S2InspDateTo1 is not null) and (:S2InspDateTo2 <> ""), :S2InspDateTo3, S2HEAD.INSPECTION_YMD)
			                   AND S2SHEE.ITEM_NO             LIKE :S2PartNo
			                   AND S2HEAD.INSPECTION_TIME_ID  = IF((:S2InspTime1 is not null) and (:S2InspTime2 <> ""), :S2InspTime3, S2HEAD.INSPECTION_TIME_ID)
			                   AND S2DETL.INSPECTION_RESULT_TYPE IN ("")  /* INSPECTION_RESULT_TYPE="4","5"を外す　空白のみ除外とする */
			              GROUP BY S2HEAD.INSPECTION_RESULT_NO
			            ) SUB2
			        ON HEAD.INSPECTION_RESULT_NO = SUB2.INSPECTION_RESULT_NO

			     WHERE HEAD.PROCESS_ID          = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			       AND SHEE.CUSTOMER_ID         = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
			       AND HEAD.INSPECTION_SHEET_NO LIKE :CheckSheetNo                                      /* 前方一致検索　未入力なら%のみ */
			       AND HEAD.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
			       AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
			       AND HEAD.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
			       AND HEAD.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)
			       AND HEAD.PRODUCTION_YMD      >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, HEAD.PRODUCTION_YMD)
			       AND HEAD.PRODUCTION_YMD      <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, HEAD.PRODUCTION_YMD)
			       AND HEAD.INSPECTION_YMD      >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, HEAD.INSPECTION_YMD)
			       AND HEAD.INSPECTION_YMD      <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, HEAD.INSPECTION_YMD)
			       AND SHEE.ITEM_NO             LIKE :PartNo
			       AND HEAD.INSPECTION_TIME_ID  = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, HEAD.INSPECTION_TIME_ID)

			  ORDER BY HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,HEAD.PROCESS_ID
			          ,CUSTOMER.CUSTOMER_ID
			          ,HEAD.PRODUCTION_YMD
			          ,HEAD.INSPECTION_YMD
			          ,TIME.DISPLAY_ORDER
			          ,TIME.INSPECTION_TIME_ID
			          ,HEAD.CONDITION_CD
			          ,USER.DISPLAY_ORDER
			          ,HEAD.INSERT_USER_ID
			',
				[
					"S1ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1CustomerId1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CustomerId2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CheckSheetNo"      => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S1RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MoldNo1"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S1MoldNo2"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S1UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1ProdDateFrom1"     => $lProductionDateFromForSearch,
					"S1ProdDateFrom2"     => $lProductionDateFromForSearch,
					"S1ProdDateFrom3"     => $lProductionDateFromForSearch,
					"S1ProdDateTo1"       => $lProductionDateToForSearch,
					"S1ProdDateTo2"       => $lProductionDateToForSearch,
					"S1ProdDateTo3"       => $lProductionDateToForSearch,
					"S1InspDateFrom1"     => $lInspectionDateFromForSearch,
					"S1InspDateFrom2"     => $lInspectionDateFromForSearch,
					"S1InspDateFrom3"     => $lInspectionDateFromForSearch,
					"S1InspDateTo1"       => $lInspectionDateToForSearch,
					"S1InspDateTo2"       => $lInspectionDateToForSearch,
					"S1InspDateTo3"       => $lInspectionDateToForSearch,
					"S1PartNo"            => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"S1InspTime1"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S1InspTime2"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S1InspTime3"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),

					"S2ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2CustomerId1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CustomerId2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CheckSheetNo"      => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S2RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2MoldNo1"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S2MoldNo2"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S2UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2ProdDateFrom1"     => $lProductionDateFromForSearch,
					"S2ProdDateFrom2"     => $lProductionDateFromForSearch,
					"S2ProdDateFrom3"     => $lProductionDateFromForSearch,
					"S2ProdDateTo1"       => $lProductionDateToForSearch,
					"S2ProdDateTo2"       => $lProductionDateToForSearch,
					"S2ProdDateTo3"       => $lProductionDateToForSearch,
					"S2InspDateFrom1"     => $lInspectionDateFromForSearch,
					"S2InspDateFrom2"     => $lInspectionDateFromForSearch,
					"S2InspDateFrom3"     => $lInspectionDateFromForSearch,
					"S2InspDateTo1"       => $lInspectionDateToForSearch,
					"S2InspDateTo2"       => $lInspectionDateToForSearch,
					"S2InspDateTo3"       => $lInspectionDateToForSearch,
					"S2PartNo"            => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"S2InspTime1"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S2InspTime2"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S2InspTime3"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),

					"ProcessId1"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerId1"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerId2"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo1"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID1"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"ProdDateFrom1"       => $lProductionDateFromForSearch,
					"ProdDateFrom2"       => $lProductionDateFromForSearch,
					"ProdDateFrom3"       => $lProductionDateFromForSearch,
					"ProdDateTo1"         => $lProductionDateToForSearch,
					"ProdDateTo2"         => $lProductionDateToForSearch,
					"ProdDateTo3"         => $lProductionDateToForSearch,
					"InspDateFrom1"       => $lInspectionDateFromForSearch,
					"InspDateFrom2"       => $lInspectionDateFromForSearch,
					"InspDateFrom3"       => $lInspectionDateFromForSearch,
					"InspDateTo1"         => $lInspectionDateToForSearch,
					"InspDateTo2"         => $lInspectionDateToForSearch,
					"InspDateTo3"         => $lInspectionDateToForSearch,
					"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"InspTime1"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime2"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime3"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
			);

			// echo "<pre>";
			// print_r($lTblSearchResultData);
			// echo "</pre>";

		return $lTblSearchResultData;
	}

	// //**************************************************************************
	// // process            getNotExistNoList
	// // overview           search no entry List of Inspection No. and return it to calling as Array
	// // overview           実績が未入力の検査番号リストを検索して、呼び出し元に配列で返却する
	// // argument           Nothing
	// // return value       Array
	// // author             
	// // record of updates  No,1 2018.02.08
	// //                    No,2 
	// // Remarks            
	// //**************************************************************************
	// private function getNotExistNoList()
	// {
	// 	$lTblNotExistNoData = [];      //data table of no entry Inspection No.
	// 	$lRowNotExistNoData = [];      //data row of no entry Inspection No.

	// 	$lArrNotExistNoData = [];      //data of no entry Inspection No.（Array after treatment）
	// 	$lWorkValue         = "";      //work to treat value
	// 	$lRowCount          = 0;       //data line No.
	// 	$lOldKey            = "";      //old Key（inspection result No.）
	// 	$lNewKey            = "";      //new Key（inspection result No.）

	// 	$lInspectionDateFromForSearch  = ""; //date From for search
	// 	$lInspectionDateToForSearch    = ""; //date To for search
	// 	$lProductionDateFromForSearch  = ""; //date From for search
	// 	$lProductionDateToForSearch    = ""; //date To for search

	// 	//change English type->Japanese type（because MySQL store as Japanese type）
	// 	$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), self::CONVDATA_ARG);
	// 	$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), self::CONVDATA_ARG);

	// 	//change English type->Japanese type（because MySQL store as Japanese type）
	// 	$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), self::CONVDATA_ARG);
	// 	$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), self::CONVDATA_ARG);

	// 	$lTblNotExistNoData = DB::select('
	// 		    SELECT HEAD.INSPECTION_RESULT_NO
	// 		          ,DETL.INSPECTION_NO
	// 		      FROM TRESHEDT AS HEAD
	// 		INNER JOIN TISHEETM AS SHEE
	// 		        ON HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
	// 		       AND HEAD.REV_NO              = SHEE.REV_NO
	// 		       AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
	// 		INNER JOIN TRESDETT AS DETL
	// 		        ON HEAD.INSPECTION_RESULT_NO = DETL.INSPECTION_RESULT_NO
	// 		     WHERE HEAD.PROCESS_ID           = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)
	// 		       AND SHEE.CUSTOMER_ID          = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
	// 		       AND HEAD.INSPECTION_SHEET_NO  LIKE :CheckSheetNo
	// 		       AND HEAD.REV_NO               = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
	// 		       AND HEAD.MACHINE_NO           = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
	// 		       AND HEAD.MOLD_NO              = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
	// 		       AND HEAD.INSERT_USER_ID       = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)
	// 		       AND HEAD.PRODUCTION_YMD       >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, HEAD.PRODUCTION_YMD)
	// 		       AND HEAD.PRODUCTION_YMD       <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, HEAD.PRODUCTION_YMD)
	// 		       AND HEAD.INSPECTION_YMD       >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, HEAD.INSPECTION_YMD)
	// 		       AND HEAD.INSPECTION_YMD       <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, HEAD.INSPECTION_YMD)
	// 		       AND SHEE.ITEM_NO              LIKE :PartNo
	// 		       AND HEAD.INSPECTION_TIME_ID   = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, HEAD.INSPECTION_TIME_ID)
	// 		       AND (DETL.INSPECTION_RESULT_TYPE = "" or DETL.INSPECTION_RESULT_TYPE is null)
	// 		  ORDER BY HEAD.INSPECTION_RESULT_NO
	// 		          ,DETL.INSPECTION_NO
	// 		',
	// 			[
	// 				"ProcessId1"          => TRIM((String)Input::get('cmbProcessForSearch')),
	// 				"ProcessId2"          => TRIM((String)Input::get('cmbProcessForSearch')),
	// 				"CustomerId1"         => TRIM((String)Input::get('cmbCustomerForSearch')),
	// 				"CustomerId2"         => TRIM((String)Input::get('cmbCustomerForSearch')),
	// 				"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
	// 				"RevisionNo1"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
	// 				"RevisionNo2"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
	// 				"MachineNo1"          => TRIM((String)Input::get('txtMachineNoForSearch')),
	// 				"MachineNo2"          => TRIM((String)Input::get('txtMachineNoForSearch')),
	// 				"MoldNo1"             => TRIM((String)Input::get('txtMoldNoForSearch')),
	// 				"MoldNo2"             => TRIM((String)Input::get('txtMoldNoForSearch')),
	// 				"UserID1"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
	// 				"UserID2"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
	// 				"ProdDateFrom1"       => $lProductionDateFromForSearch,
	// 				"ProdDateFrom2"       => $lProductionDateFromForSearch,
	// 				"ProdDateFrom3"       => $lProductionDateFromForSearch,
	// 				"ProdDateTo1"         => $lProductionDateToForSearch,
	// 				"ProdDateTo2"         => $lProductionDateToForSearch,
	// 				"ProdDateTo3"         => $lProductionDateToForSearch,
	// 				"InspDateFrom1"       => $lInspectionDateFromForSearch,
	// 				"InspDateFrom2"       => $lInspectionDateFromForSearch,
	// 				"InspDateFrom3"       => $lInspectionDateFromForSearch,
	// 				"InspDateTo1"         => $lInspectionDateToForSearch,
	// 				"InspDateTo2"         => $lInspectionDateToForSearch,
	// 				"InspDateTo3"         => $lInspectionDateToForSearch,
	// 				"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
	// 				"InspTime1"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
	// 				"InspTime2"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
	// 				"InspTime3"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
	// 			]
	// 	);

	// 	//if data exist, treat to be 2row,Inspection Result No. and no entry Inspection No.
	// 	if ($lTblNotExistNoData != null)
	// 	{
	// 		//store result in Array again
	// 		foreach ($lTblNotExistNoData as $lRowNotExistNoData)
	// 		{
	// 			//Cast previously　because it has erroe to read value of array while CASTing
	// 			$lRowNotExistNoData = (Array)$lRowNotExistNoData;

	// 			//store present value in old key exception from 0 line count(prevention of key break in 1 issue)
	// 			if ($lRowCount == 0)
	// 			{
	// 				$lOldKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];
	// 			}

	// 			//set new key
	// 			$lNewKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];

	// 			//in case key break
	// 			if ($lOldKey != $lNewKey)
	// 			{
	// 				//key break
	// 				//delete the last comma in the string maked in work 
	// 				$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

	// 				//set Array to return
	// 				$lArrNotExistNoData += [
	// 					$lOldKey => $lWorkValue
	// 				];

	// 				//clear work
	// 				$lWorkValue = "";
	// 			}

	// 			//connect no exist No. in work
	// 			$lWorkValue .= $lRowNotExistNoData["INSPECTION_NO"]."-";

	// 			//set value reading to old key
	// 			$lOldKey = $lNewKey;

	// 			//add line No.
	// 			$lRowCount += 1;
	// 		}

	// 		//store data remaining after read all
	// 		//delete the last comma in the string maked in work
	// 		$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

	// 		//set Array to return
	// 		$lArrNotExistNoData += [
	// 			$lOldKey => $lWorkValue
	// 		];
	// 	}

	// 	return $lArrNotExistNoData;
	// }

	//**************************************************************************
	// process            getInspectionResultData
	// overview           search header data of inspection result and return array to calling 
	// argument           Inspection Result No.（inside key per button）、DataRevision
	// return value       Array
	// author             s-miyamoto
	// date               2014.05.28
	// record of updates  2014.05.28 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    
	//**************************************************************************
	private function getInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lTblSearchResultData = [];      //data table inspection result

		$lTblSearchResultData = DB::select('
			    SELECT REHE.INSPECTION_SHEET_NO
			          ,SHEE.INSPECTION_SHEET_NAME
			          ,REHE.REV_NO
			          ,REHE.PROCESS_ID 
			    	  ,PROCESS.CODE_NAME AS PROCESS_NAME
			          ,REHE.MACHINE_NO
			          ,MACH.MACHINE_NAME
			          ,REHE.MOLD_NO
			          ,REHE.MATERIAL_NAME
			          ,REHE.MATERIAL_SIZE
			          ,REHE.HEAT_TREATMENT_TYPE
			          ,REHE.PLATING_KIND
			          ,REHE.COLOR_ID
			          ,COLOR.CODE_NAME AS COLOR_NAME
			          ,REHE.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,USER.TEAM_ID
			          ,TEAM.CODE_NAME AS TEAM_NAME
			          ,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,REHE.CONDITION_CD
				      ,COND.CODE_NAME AS CONDITION_NAME
			          ,REHE.INSPECTION_TIME_ID
			          ,TIME.INSPECTION_TIME_NAME
			          ,REHE.MATERIAL_LOT_NO
			          ,REHE.CERTIFICATE_NO
			          ,REHE.PO_NO
			          ,REHE.INVOICE_NO
			          ,REHE.QUANTITY_COIL
			          ,REHE.QUANTITY_WEIGHT
			          ,REHE.LOT_NO
			          ,DATE_FORMAT(REHE.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
			          ,REHE.DATA_REV
			          ,SHEE.CUSTOMER_ID
			          ,CUST.CUSTOMER_NAME
			          ,SHEE.MODEL_NAME
			          ,SHEE.ITEM_NO
			          ,ITEM.ITEM_NAME
			          ,SHEE.DRAWING_NO
			          ,SHEE.ISO_KANRI_NO
			          ,ITEM.PRODUCT_WEIGHT
			      FROM TRESHEDT AS REHE

			INNER JOIN TISHEETM AS SHEE
			        ON REHE.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND REHE.REV_NO              = SHEE.REV_NO
			       -- AND REHE.PROCESS_ID          = SHEE.PROCESS_ID
			INNER JOIN TCUSTOMM AS CUST
			        ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			INNER JOIN TMACHINM AS MACH
			        ON REHE.MACHINE_NO          = MACH.MACHINE_NO
			INNER JOIN TINSPTIM AS TIME
			        ON REHE.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
			INNER JOIN TUSERMST AS USER
			        ON REHE.INSERT_USER_ID      = USER.USER_ID
	   LEFT OUTER JOIN TITEMMST AS ITEM
					ON SHEE.ITEM_NO             = ITEM.ITEM_NO
				   AND ITEM.DELETE_FLG          = "0"

			INNER JOIN TCODEMST AS COND
			        ON COND.CODE_CLASS    = "001"
			       AND REHE.CONDITION_CD  = COND.CODE_ORDER
			INNER JOIN TCODEMST AS PROCESS
				    ON PROCESS.CODE_CLASS = "002"
				   AND REHE.PROCESS_ID    = PROCESS.CODE_ORDER
	   LEFT OUTER JOIN TCODEMST AS COLOR
			        ON COLOR.CODE_CLASS   = "006"
			       AND ITEM.COLOR_ID      = COLOR.CODE_ORDER
	   LEFT OUTER JOIN TCODEMST AS TEAM
				    ON TEAM.CODE_CLASS    = "004"
				   AND USER.TEAM_ID       = TEAM.CODE_ORDER

			     WHERE REHE.INSPECTION_RESULT_NO = :InspectionResultNo
			       AND REHE.DATA_REV             = :DataRev
			       AND SHEE.DELETE_FLG           = "0"
			       AND CUST.DELETE_FLG           = "0"
			       AND MACH.DELETE_FLG           = "0"
			       AND TIME.DELETE_FLG           = "0"
			       AND USER.DELETE_FLG           = "0"
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"DataRev"            => TRIM((String)$pDataRev),
				]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         setarray
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks     
	//**************************************************************************
	private function setarray($pTblImportfile, $pDataImportNGreason, $pArrayNGData)
	{
		$pWorkArrayNGData = [];

		$pWorkArrayNGData += [
				"InspectionResultNo"     => $pTblImportfile["InspectionResultNo"],
				"InspectionNo"           => $pTblImportfile["InspectionNo"],
				"InspectionPoint"        => $pTblImportfile["InspectionPoint"],
				"InspectionResultValue"  => $pTblImportfile["InspectionResultValue"],
				"ImportNGreason"         => $pDataImportNGreason,
		];

		array_push($pArrayNGData, $pWorkArrayNGData);

		return $pArrayNGData;
	}

	//**************************************************************************
	// process            updateLineStopData
	// overview           register Inspection result Header and detail,update
	// argument           Inspection Result No., Data Revision, User ID（login ID）
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function updateLineStopData($pInspectionResultNo, $pDataRev, $pUserID)
	{
		$lTblLineStopTargetInspectionNo = [];
		$lRowLineStopTargetInspectionNo = [];

		$lTblLineStopTargetInspectionNo = DB::select('
		         SELECT DETL.INSPECTION_NO
		           FROM TRESHEDT AS HEAD
		     INNER JOIN TRESDETT AS DETL
		             ON HEAD.INSPECTION_RESULT_NO   = DETL.INSPECTION_RESULT_NO
		          WHERE HEAD.INSPECTION_RESULT_NO   = :InspectionResultNo
		            AND DETL.INSPECTION_RESULT_TYPE = ""
		       ORDER BY DETL.INSPECTION_NO
		',
			[
				"InspectionResultNo" => $pInspectionResultNo,
			]
		);

		//1 transaction for header and detail
		DB::transaction(
			function() use ($pInspectionResultNo
			               ,$pDataRev
			               ,$pUserID
			               ,$lTblLineStopTargetInspectionNo
			               ,$lRowLineStopTargetInspectionNo)
			{
				//update header
				//Log::write('info', 'BA1010 Start Linestop Update Header');

				DB::update('
						UPDATE TRESHEDT
						   SET LAST_UPDATE_USER_ID  = :LastUpdateUserID
						      ,UPDATE_YMDHMS        = now()
						      ,DATA_REV             = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
						   AND DATA_REV             = :DataRevision
				',
					[
						"LastUpdateUserID"    => TRIM((String)$pUserID),
						"InspectionResultNo"  => $pInspectionResultNo,
						"DataRevision"        => $pDataRev,
					]
				);

				//Log::write('info', 'BA1010 Finish Linestop Update Header');

				//register detail while looping data
				foreach ($lTblLineStopTargetInspectionNo as $lRowLineStopTargetInspectionNo)
				{
					//Cast previously　because it has erroe to read value of array while CASTing
					$lRowLineStopTargetInspectionNo = (Array)$lRowLineStopTargetInspectionNo;

					//register detail
					//Log::write('info', 'BA1010 Start Linestop Update Detail');

					DB::update('
							UPDATE TRESDETT
							   SET INSPECTION_RESULT_TYPE = "5"
							      ,LAST_UPDATE_USER_ID    = :LastUpdateUserID
							      ,UPDATE_YMDHMS          = now()
							      ,DATA_REV               = DATA_REV + 1
							 WHERE INSPECTION_RESULT_NO   = :InspectionResultNo
							   AND INSPECTION_NO          = :InspectionNo
							   -- AND DATA_REV               = :DataRevision
					',
						[
							"LastUpdateUserID"    => TRIM((String)$pUserID),
							"InspectionResultNo"  => $pInspectionResultNo,
							"InspectionNo"        => $lRowLineStopTargetInspectionNo["INSPECTION_NO"], //Add 28-May-2018 Hoshina Because update is 1 record only
							// "DataRevision"        => $pDataRev, //Delete 28-May-2018 Hoshina Because DataRevision is TRESHEDT only 
						]
					);

					//Log::write('info', 'BA1010 Finish Linestop Update Detail');
				}

			//return from obscurity function to the caller
			return true;
			}
		);

		return true;
	}

	//**************************************************************************
	// process            deleteInspectionResultData
	// overview           delete header and detail of inspection result
	// argument           Inspection Result No.（inside key per button）、DataRevision
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function deleteInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lDeleteCount = 0;

		$lDeleteCount = DB::delete('
			DELETE FROM TRESHEDT
			      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
			        AND DATA_REV             = :DataRevision
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
				"DataRevision"       => $pDataRev,
			]
		);

		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		DB::delete('
		DELETE FROM TRESDETT
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		DB::delete('
		DELETE FROM TRESANAL
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		return $lDeleteCount;
	}

	//**************************************************************************
	// process            insertAndUpdateResult
	// overview           transaction table
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function insertAndUpdateResult($pAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett ,$tablemodeltresanal)
	{
		// Log::write('info', 'BA1010 start insert/update');

			// Log::write('info', 'insertAndUpdateResult',
			// 	[
			// 		"BeforeCalcFlg"  => $pAfterCalculateData["BeforeCalcFlg"],
			// 		"CalcFlg"        => $pAfterCalculateData["CalcFlg"],
			// 		"$tablemodeltresdett" => $tablemodeltresdett,
			// 	]
			// );
		//All processing is 1 Transaction
		// DB::transaction
		// (
		// 	function() use ($pAfterCalculateData, $tablemodeltreshedt, $tablemodeltresdett ,$tablemodeltresanal)
		// 	{
				DB::update('
						UPDATE TRESHEDT
						   SET LAST_UPDATE_USER_ID  = :LastUpdateUserID
						      ,UPDATE_YMDHMS        = now()
						      ,DATA_REV             = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
				',
					[
						"LastUpdateUserID"    => $tablemodeltreshedt->LAST_UPDATE_USER_ID,
						"InspectionResultNo"  => $tablemodeltreshedt->INSPECTION_RESULT_NO,
					]
				);
				// Log::write('info', 'BA1010 Finish Update Header');

				DB::update('
				        UPDATE TRESDETT
				           SET INSPECTION_RESULT_VALUE       = :InspectionResultValue
				              ,INSPECTION_RESULT_TYPE        = :InspectionResultType
				              ,LAST_UPDATE_USER_ID           = :LastUpdateUserID
				              ,UPDATE_YMDHMS                 = now()
				              ,DATA_REV                      = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO          = :InspectionResultNo
						   AND INSPECTION_NO                 = :InspectionNo
				',
					[
						"InspectionResultValue"        => $tablemodeltresdett->INSPECTION_RESULT_VALUE,
						"InspectionResultType"         => $tablemodeltresdett->INSPECTION_RESULT_TYPE,
						"LastUpdateUserID"             => $tablemodeltresdett->LAST_UPDATE_USER_ID,
						"InspectionResultNo"           => $tablemodeltresdett->INSPECTION_RESULT_NO,
						"InspectionNo"                 => $tablemodeltresdett->INSPECTION_NO,
					]
				);
				// Log::write('info', 'BA1010 Finish Update Detail');

				if ($pAfterCalculateData["BeforeCalcFlg"] == false and $pAfterCalculateData["CalcFlg"] == false)
				{
					//Nothing
				}
				else
				{
					// Log::write('info', 'BA1010 Start Insert/Update Analyze');
					//Not have TRESANAL → insert
					//Have TRESANAL → update
					DB::insert('
						     INSERT INTO TRESANAL
										(INSPECTION_SHEET_NO
										,REV_NO
										,INSPECTION_POINT
										,ANALYTICAL_GRP_01
										,ANALYTICAL_GRP_02
										,INSPECTION_RESULT_NO
										,PROCESS_ID
										,INSPECTION_YMD
										,INSPECTION_TIME_ID
										,CONDITION_CD
										,MOLD_NO
										,TEAM_ID
										,STD_MIN_SPEC
										,STD_MAX_SPEC
										,PICTURE_URL
										,CALC_NUM
										,MAX_VALUE
										,MIN_VALUE
										,AVERAGE_VALUE
										,RANGE_VALUE
										,STDEV
										,CPK
										,XBAR_UCL
										,XBAR_LCL
										,RCHART_UCL
										,RCHART_LCL
										,JUDGE_REASON
										,INSERT_YMDHMS
										,LAST_UPDATE_USER_ID
										,UPDATE_YMDHMS
										,DATA_REV)
								  VALUES (:CheckSheetNo
										 ,:RevisionNo
										 ,:InspectionPoint
										 ,:AnalyticalGroup01
										 ,:AnalyticalGroup02
										 ,:InspectionResultNo
										 ,:ProcessId
										 ,:InspectionDate
										 ,:InspectionTimeId
										 ,:ConditionCode
										 ,:MoldNo
										 ,:TeamId
										 ,:StandardMinSpec
										 ,:StandardMaxSpec
										 ,:PictureURL
										 ,:CalcNam1
										 ,:MaxValue1
										 ,:MinValue1
										 ,:AverageValue1
										 ,:RangeValue1
										 ,:Stdev1
										 ,:Cpk1
										 ,:XbarUcl1
										 ,:XbarLcl1
										 ,:RchartUcl1
										 ,:RchartLcl1
										 ,:JudgeReason1
										 ,now()
										 ,:LastUpdateUserID1
										 ,now()
										 ,1)
						ON DUPLICATE KEY
								  UPDATE CALC_NUM             = :CalcNam2
										,MAX_VALUE            = :MaxValue2
										,MIN_VALUE            = :MinValue2
										,AVERAGE_VALUE        = :AverageValue2
										,RANGE_VALUE          = :RangeValue2
										,STDEV                = :Stdev2
										,CPK                  = :Cpk2
										,XBAR_UCL             = :XbarUcl2
										,XBAR_LCL             = :XbarLcl2
										,RCHART_UCL           = :RchartUcl2
										,RCHART_LCL           = :RchartLcl2
										,JUDGE_REASON         = :JudgeReason2
										,LAST_UPDATE_USER_ID  = :LastUpdateUserID2
										,UPDATE_YMDHMS        = now()
										,DATA_REV             = DATA_REV + 1
					',
						[
							"CheckSheetNo"         => $tablemodeltresanal->INSPECTION_SHEET_NO,
							"RevisionNo"           => $tablemodeltresanal->REV_NO,
							"InspectionPoint"      => $tablemodeltresanal->INSPECTION_POINT,
							"AnalyticalGroup01"    => $tablemodeltresanal->ANALYTICAL_GRP_01,
							"AnalyticalGroup02"    => $tablemodeltresanal->ANALYTICAL_GRP_02,
							"InspectionResultNo"   => $tablemodeltresanal->INSPECTION_RESULT_NO,
							"ProcessId"            => $tablemodeltresanal->PROCESS_ID,
							"InspectionDate"       => $tablemodeltresanal->INSPECTION_YMD,
							"InspectionTimeId"     => $tablemodeltresanal->INSPECTION_TIME_ID,
							"ConditionCode"        => $tablemodeltresanal->CONDITION_CD,
							"MoldNo"               => $tablemodeltresanal->MOLD_NO,
							"TeamId"               => $tablemodeltresanal->TEAM_ID,
							"StandardMinSpec"      => $tablemodeltresanal->STD_MIN_SPEC,
							"StandardMaxSpec"      => $tablemodeltresanal->STD_MAX_SPEC,
							"PictureURL"           => $tablemodeltresanal->PICTURE_URL,
							"CalcNam1"             => $tablemodeltresanal->CALC_NUM,
							"CalcNam2"             => $tablemodeltresanal->CALC_NUM,
							"MaxValue1"            => $tablemodeltresanal->MAX_VALUE,
							"MaxValue2"            => $tablemodeltresanal->MAX_VALUE,
							"MinValue1"            => $tablemodeltresanal->MIN_VALUE,
							"MinValue2"            => $tablemodeltresanal->MIN_VALUE,
							"AverageValue1"        => $tablemodeltresanal->AVERAGE_VALUE,
							"AverageValue2"        => $tablemodeltresanal->AVERAGE_VALUE,
							"RangeValue1"          => $tablemodeltresanal->RANGE_VALUE,
							"RangeValue2"          => $tablemodeltresanal->RANGE_VALUE,
							"Stdev1"               => $tablemodeltresanal->STDEV,
							"Stdev2"               => $tablemodeltresanal->STDEV,
							"Cpk1"                 => $tablemodeltresanal->CPK,
							"Cpk2"                 => $tablemodeltresanal->CPK,
							"XbarUcl1"             => $tablemodeltresanal->XBAR_UCL,
							"XbarUcl2"             => $tablemodeltresanal->XBAR_UCL,
							"XbarLcl1"             => $tablemodeltresanal->XBAR_LCL,
							"XbarLcl2"             => $tablemodeltresanal->XBAR_LCL,
							"RchartUcl1"           => $tablemodeltresanal->RCHART_UCL,
							"RchartUcl2"           => $tablemodeltresanal->RCHART_UCL,
							"RchartLcl1"           => $tablemodeltresanal->RCHART_LCL,
							"RchartLcl2"           => $tablemodeltresanal->RCHART_LCL,
							"JudgeReason1"         => $tablemodeltresanal->JUDGE_REASON,
							"JudgeReason2"         => $tablemodeltresanal->JUDGE_REASON,
							"LastUpdateUserID1"    => $tablemodeltresanal->LAST_UPDATE_USER_ID,
							"LastUpdateUserID2"    => $tablemodeltresanal->LAST_UPDATE_USER_ID,

						]
					);
				}
				// Log::write('info', 'BA1010 Finish Insert/Update Analyze');
		// 	}
		// );
	}

	//**************************************************************************
	// process            RequiredInputCheckEntry
	// overview           errorcheck for entry
	// argument           Array to reurn to screen
	// return value       Array to return to screen
	// author             s-miyamoto
	// date               2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//**************************************************************************
	private function RequiredInputCheckEntry()
	{
		//must check Process data
		$lValidator = Validator::make(
			array('cmbProcessForEntry' => Input::get('cmbProcessForEntry')),
			array('cmbProcessForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E044 : Select Process.");
		}

		//check Process data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbProcessForEntry'));
		$lValidator = Validator::make(
			array('cmbProcessForEntry' => $lSlashRemovalValue),
			array('cmbProcessForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E045 : Process is invalid.");
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCustomerForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E049 : Select Customer.");
		}

		//check customer data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCustomerForEntry'));
		$lValidator = Validator::make(
			array('cmbCustomerForEntry' => $lSlashRemovalValue),
			array('cmbCustomerForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E050 : Customer is invalid.");
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCheckSheetNoForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E006 : Enter Check Sheet No. .");
		}

		//check check sheet No. type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForEntry'));

		//Add 28-May-2018 Hoshina Because can not input "(" and ")" 
		//Validationcheck after deleting ( and ) because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("(", "", $lSlashRemovalValue);
		$lSlashRemovalValue = str_replace(")", "", $lSlashRemovalValue);


		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E007 : Check Sheet No. is invalid.");
		}

		//must check M/C No.
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E009 : Enter M/C No. .");
		}

		//check M/C No. type
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E010 : M/C No. is invalid.");
		}

		//must check Mold No.
		$lValidator = Validator::make(
			array('txtMoldNoForEntry' => Input::get('txtMoldNoForEntry')),
			array('txtMoldNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E065 : Enter Mold No. .");
		}

		//check Mold No. type
		$lValidator = Validator::make(
			array('txtMoldNoForEntry' => Input::get('txtMoldNoForEntry')),
			array('txtMoldNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E066 : Mold No. is invalid.");
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E012 : Enter Inspector Code.");
		}

		//check inspector code type
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E013 : Inspector Code is invalid.");
		}

		//must check inspection date
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E015 : Enter Inspection Date.");
		}

		//check inspection date type
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E016 : Inspection Date is invalid.");
		}

		//must check inspection nomal/abnomal
		$lValidator = Validator::make(
			array('cmbConditionForEntry' => Input::get('cmbConditionForEntry')),
			array('cmbConditionForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails())
		{
			throw new Exception("E037 : Select Condition.");
		}

		//must check inspection time
		$lValidator = Validator::make(
			array('cmbInspectionTimeForEntry' => Input::get('cmbInspectionTimeForEntry')),
			array('cmbInspectionTimeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E017 : Select Inspection Time.");
		}
	}

	//**************************************************************************
	// process         InputCheckSearch
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function InputCheckSearch()
	{
		//check Process type
		if(Input::get('cmbProcessForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('cmbProcessForSearch'	=> Input::get('cmbProcessForSearch')),
				array('cmbProcessForSearch'	=> array('alpha_dash'))
			);
			if($lValidator->fails())
			{
				throw new Exception("E045 : Process is invalid.");
			}
		}

		//check customer type
		if(Input::get('cmbCustomerForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
				array('cmbCustomerForSearch' => array('alpha_dash'))
			);
			if($lValidator->fails())
			{
				throw new Exception("E050 : Customer is invalid.");
			}
		}

		//check check sheet No. type
		//Validation check after deleting en thrash because Laravel alpha_dash can not pass thrash

		if(Input::get('cmbCheckSheetNoForSearch') != 0)
		{
			$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForSearch'));

			//Add 28-May-2018 Hoshina Because can not input "(" and ")" 
			//Validation check after deleting ( and ) because Laravel alpha_dash can not pass thrash
			$lSlashRemovalValue = str_replace("(", "", $lSlashRemovalValue);
			$lSlashRemovalValue = str_replace(")", "", $lSlashRemovalValue);

			$lValidator = Validator::make(
				array('cmbCheckSheetNoForSearch' => $lSlashRemovalValue),
				array('cmbCheckSheetNoForSearch' => array('alpha_dash'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E007 : Check Sheet No. is invalid.");
			}
		}

		//check Revision No. type
		if(Input::get('cmbRevisionNoForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
				array('cmbRevisionNoForSearch' => array('alpha_dash'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E018 : Revision No. is invalid.");
			}
		}

		//check M/C No. type
		if(Input::get('txtMachineNoForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtMachineNoForSearch' => Input::get('txtMachineNoForSearch')),
				array('txtMachineNoForSearch' => array('alpha_dash'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E010 : M/C No. is invalid.");
			}
		}

		//check Mold No. type
		if(Input::get('txtMoldNoForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtMoldNoForSearch' => Input::get('txtMoldNoForSearch')),
				array('txtMoldNoForSearch' => array('alpha_dash'))
			);
			if ($lValidator->fails())
			{
				throw new Exception("E066 : Mold No. is invalid.");
			}
		}

		//check inspector code type
		if(Input::get('txtInspectorCodeForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
				array('txtInspectorCodeForSearch' => array('alpha_dash'))
			);	

			if ($lValidator->fails())
			{
				throw new Exception("E013 : Inspector Code is invalid.");
			}
		}

		//check production date type(From)
		if(Input::get('txtProductionDateFromForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtProductionDateFromForSearch' => Input::get('txtProductionDateFromForSearch')),
				array('txtProductionDateFromForSearch' => array('date_format:d-m-Y'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E063 : Production Date is invalid.");
			}
		}

		//check production date type(To)
		if(Input::get('txtProductionDateToForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtProductionDateToForSearch' => Input::get('txtProductionDateToForSearch')),
				array('txtProductionDateToForSearch' => array('date_format:d-m-Y'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E063 : Production Date is invalid.");
			}
		}

		//check production from/to date(must check From/To)
		if ((Input::get('txtProductionDateFromForSearch') != "") and (Input::get('txtProductionDateToForSearch') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtProductionDateFromForSearch'));
			$lDateTo   = date_create(Input::get('txtProductionDateToForSearch'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				throw new Exception("E064 : The relation of Production Date(From/To) is incorrect.");
			}
		}

		//check inspection date type(From)
		if(Input::get('txtInspectionDateFromForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
				array('txtInspectionDateFromForSearch' => array('date_format:d-m-Y'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E016 : Inspection Date is invalid.");
			}
		}

		//check inspection date type(To)
		if(Input::get('txtInspectionDateToForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
				array('txtInspectionDateToForSearch' => array('date_format:d-m-Y'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E016 : Inspection Date is invalid.");
			}
		}

		//check inspection from/to date(must check From/To)
		if ((Input::get('txtInspectionDateFromForSearch') != "") and (Input::get('txtInspectionDateToForSearch') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtInspectionDateFromForSearch'));
			$lDateTo   = date_create(Input::get('txtInspectionDateToForSearch'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				throw new Exception("E019 : The relation of Inspection Date(From/To) is incorrect.");
			}
		}

		//check Part No type
		if(Input::get('txtPartNoForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('txtPartNoForSearch' => Input::get('txtPartNoForSearch')),
				array('txtPartNoForSearch' => array('alpha_dash'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E020 : Part No. is invalid.");
			}
		}

		//check Inspection time type
		if(Input::get('cmbInspectionTimeForSearch') != 0)
		{
			$lValidator = Validator::make(
				array('cmbInspectionTimeForSearch' => Input::get('cmbInspectionTimeForSearch')),
				array('cmbInspectionTimeForSearch' => array('alpha_dash'))
			);

			if ($lValidator->fails())
			{
				throw new Exception("E051 : Inspection Time is invalid.");
			}
		}
	}

	//**************************************************************************
	// process         RequiredInputCheckSearch
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function RequiredInputCheckSearch()
	{
		// //must check production date(From)
		// $lValidator = Validator::make(
		// 	array('txtProductionDateFromForSearch' => Input::get('txtProductionDateFromForSearch')),
		// 	array('txtProductionDateFromForSearch' => array('required'))
		// );
		// //error
		// if ($lValidator->fails())
		// {
		// 		//error message
		// 		$pViewData["errors"] = new MessageBag([
		// 			"error" => "E062 : Enter Production Date."
		// 		]);
		
		// 		return $pViewData;
		// }

		// //must check production date(To)
		// $lValidator = Validator::make(
		// 	array('txtProductionDateToForSearch' => Input::get('txtProductionDateToForSearch')),
		// 	array('txtProductionDateToForSearch' => array('required'))
		// );
		// //error
		// if ($lValidator->fails())
		// {
		// //		//error message
		// 		$pViewData["errors"] = new MessageBag([
		// 			"error" => "E062 : Enter Production Date."
		// 		]);
		
		// 		return $pViewData;
		// }

		//must check inspection date(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E015 : Enter Inspection Date.");
		}

		//must check inspection date(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
			throw new Exception("E015 : Enter Inspection Date.");
		}
	}

	//**************************************************************************
	// process         RequiredInputCheckApproval
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function RequiredInputCheckApproval()
	{
		//must check Process
		$lValidator = Validator::make(
			array('cmbProcessForSearch' => Input::get('cmbProcessForSearch')),
			array('cmbProcessForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E044 : Select Process.");
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
			array('cmbCustomerForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E049 : Select Customer.");
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForSearch' => Input::get('cmbCheckSheetNoForSearch')),
			array('cmbCheckSheetNoForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E006 : Enter Check Sheet No. .");
		}

		//must check revision No.
		$lValidator = Validator::make(
			array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
			array('cmbRevisionNoForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E035 : Enter Revision No. .");
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
			array('txtInspectorCodeForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E012 : Enter Inspector Code.");
		}
	}

	//**************************************************************************
	// process    checkDataImport
	// overview   
	// argument   
	//**************************************************************************
	private function checkDataImport()
	{
		//must check sample no
		$lValidator = Validator::make(
			array('txtSampleNoForSearch' => Input::get('txtSampleNoForSearch')),
			array('txtSampleNoForSearch' => array('required'))
		);
		if ($lValidator->fails())
		{
			throw new Exception("E070 : Enter Sample No. .");
		}

		//File
		$lValidator = Validator::make(
			array('filUploadFile' => Input::file('filUploadFile')),
			array('filUploadFile' => array('required'))
		);
		if ($lValidator->fails()) 
		{
			throw new Exception("E029 : Select Upload File.");
		}
	}


	//**************************************************************************
	// process    judgeResultClass
	// overview   
	// argument   
	//**************************************************************************
	private function judgeResultClass($lTblImportfile, $lRowInspNo)
	{
		if (($lRowInspNo["THRESHOLD_NG_UNDER"] == $lRowInspNo["THRESHOLD_OFFSET_UNDER"]) and ($lRowInspNo["THRESHOLD_OFFSET_OVER"] == $lRowInspNo["THRESHOLD_NG_OVER"]))
		{
			if ($lTblImportfile["InspectionResultValue"] < $lRowInspNo["THRESHOLD_NG_UNDER"])
			{
				$pResultClass = self::RESULT_CLASS_NG;
			}
			elseif (($lRowInspNo["THRESHOLD_NG_UNDER"] <= $lTblImportfile["InspectionResultValue"]) and ($lTblImportfile["InspectionResultValue"] <= $lRowInspNo["THRESHOLD_NG_OVER"]))
			{
				$pResultClass = self::RESULT_CLASS_OK;
			}
			elseif ($lRowInspNo["THRESHOLD_NG_OVER"] < $lTblImportfile["InspectionResultValue"])
			{
				$pResultClass = self::RESULT_CLASS_NG;
			}
			else
			{
				//Nothing
				$pResultClass = "";
			}
		}
		else
		{
			if ($lTblImportfile["InspectionResultValue"] <= $lRowInspNo["THRESHOLD_NG_UNDER"])
			{
				$pResultClass = self::RESULT_CLASS_NG;
			}
			elseif (($lRowInspNo["THRESHOLD_NG_UNDER"] < $lTblImportfile["InspectionResultValue"]) and ($lTblImportfile["InspectionResultValue"] <= $lRowInspNo["THRESHOLD_OFFSET_UNDER"]))
			{
				$pResultClass = self::RESULT_CLASS_OFFSET;
			}
			elseif (($lRowInspNo["THRESHOLD_OFFSET_UNDER"] < $lTblImportfile["InspectionResultValue"]) and ($lTblImportfile["InspectionResultValue"] < $lRowInspNo["THRESHOLD_OFFSET_OVER"]))
			{
				$pResultClass = self::RESULT_CLASS_OK;
			}
			elseif (($lRowInspNo["THRESHOLD_OFFSET_OVER"] <= $lTblImportfile["InspectionResultValue"]) and ($lTblImportfile["InspectionResultValue"] < $lRowInspNo["THRESHOLD_NG_OVER"]))
			{
				$pResultClass = self::RESULT_CLASS_OFFSET;
			}
			elseif ($lRowInspNo["THRESHOLD_NG_OVER"] <= $lTblImportfile["InspectionResultValue"])
			{
				$pResultClass = self::RESULT_CLASS_NG;
			}
			else
			{
				//Nothing
				$pResultClass = "";
			}
		}

		return $pResultClass;
	}


	//**************************************************************************
	// process    setHeaderData
	// overview   
	// argument   
	//**************************************************************************
	private function setHeaderData($lCalculateArrayData, $tablemodeltreshedt)
	{
		// Log::write('info', 'S setHeaderData');
		// $tablemodeltreshedt = new treshedt;
		$tablemodeltreshedt->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
		// $tablemodeltreshedt->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
		$tablemodeltreshedt->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
		// $tablemodeltreshedt->DATA_REV = Session::get('BA1010ModifyDataRev',0);
		// Log::write('info', 'E setHeaderData');

		return $tablemodeltreshedt;
	}

	//**************************************************************************
	// process    setDetailData
	// overview   
	// argument   
	//**************************************************************************
	private function setDetailData($lCalculateArrayData, $lAfterCalculateData, $lResultClass, $tablemodeltresdett)
	{
		// Log::write('info', 'S setDetailData');
		// $tablemodeltresdett = new tresdett;
		$tablemodeltresdett->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
		$tablemodeltresdett->INSPECTION_NO = $lCalculateArrayData["InspectionNo"];
		$tablemodeltresdett->INSPECTION_RESULT_VALUE = $lAfterCalculateData["InspectionResultValue"];
		$tablemodeltresdett->INSPECTION_RESULT_TYPE = $lResultClass;
		// $tablemodeltresdett->OK_QTY = 
		// $tablemodeltresdett->NG_QTY = 
		// $tablemodeltresdett->OFFSET_NG_BUNRUI_ID = 
		// $tablemodeltresdett->OFFSET_NG_REASON_ID = 
		// $tablemodeltresdett->OFFSET_NG_REASON = 
		// $tablemodeltresdett->OFFSET_NG_ACTION = 
		// $tablemodeltresdett->OFFSET_NG_ACTION_DTL_INTERNAL = 
		// $tablemodeltresdett->OFFSET_NG_ACTION_DTL_REQUEST = 
		// $tablemodeltresdett->OFFSET_NG_ACTION_DTL_TSR = 
		// $tablemodeltresdett->OFFSET_NG_ACTION_DTL_SORTING = 
		// $tablemodeltresdett->OFFSET_NG_ACTION_DTL_NOACTION = 
		// $tablemodeltresdett->ACTION_DETAILS_ID = 
		// $tablemodeltresdett->ACTION_DETAILS_TR_NO_TSR_NO = 
		// $tablemodeltresdett->TR_NO = 
		// $tablemodeltresdett->TECHNICIAN_USER_ID = 
		// $tablemodeltresdett->TSR_NO = 
		// $tablemodeltresdett->INSPECTION_POINT = 
		// $tablemodeltresdett->ANALYTICAL_GRP_01 = 
		// $tablemodeltresdett->ANALYTICAL_GRP_02 = 
		// $tablemodeltresdett->SAMPLE_NO = 
		// $tablemodeltresdett->ANALYTICAL_GRP_01_MEMO = 
		// $tablemodeltresdett->INSERT_USER_ID = 
		// $tablemodeltresdett->DISPLAY_ORDER = 
		// $tablemodeltresdett->INSERT_YMDHMS = 
		// $tablemodeltresdett->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
		$tablemodeltresdett->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
		// $tablemodeltresdett->UPDATE_YMDHMS = 
		// $tablemodeltresdett->DATA_REV = 
		// Log::write('info', 'E setDetailData');

		return $tablemodeltresdett;
	}

	//**************************************************************************
	// process    setAnalyData
	// overview   
	// argument   
	//**************************************************************************
	private function setAnalyData($lCalculateArrayData, $lAfterCalculateData, $lRowHeader, $lRowInspNo ,$lRowInspectorInfo, $tablemodeltresanal)
	{
		// Log::write('info', 'S setAnalyData');
		$lInspectionDate = date_format(date_create($lRowHeader["INSPECTION_YMD"]), 'Y/m/d');

		// $tablemodeltresanal = new tresanal;
		$tablemodeltresanal->INSPECTION_SHEET_NO = $lRowHeader["INSPECTION_SHEET_NO"];
		$tablemodeltresanal->REV_NO = $lRowHeader["REV_NO"];
		$tablemodeltresanal->INSPECTION_POINT = $lCalculateArrayData["InspectionPoint"];
		$tablemodeltresanal->ANALYTICAL_GRP_01 = $lCalculateArrayData["AnalGrp01"];
		$tablemodeltresanal->ANALYTICAL_GRP_02 = $lCalculateArrayData["AnalGrp02"];
		$tablemodeltresanal->INSPECTION_RESULT_NO = $lCalculateArrayData["InspectionResultNo"];
		$tablemodeltresanal->PROCESS_ID = $lRowHeader["PROCESS_ID"];
		$tablemodeltresanal->INSPECTION_YMD = $lInspectionDate;
		$tablemodeltresanal->INSPECTION_TIME_ID = $lRowHeader["INSPECTION_TIME_ID"];
		$tablemodeltresanal->CONDITION_CD = $lRowHeader["CONDITION_CD"];
		$tablemodeltresanal->MOLD_NO = $lRowHeader["MOLD_NO"];
		$tablemodeltresanal->TEAM_ID = $lRowInspectorInfo["TEAM_ID"];
		$tablemodeltresanal->STD_MIN_SPEC = $lCalculateArrayData["StdSpecUnder"];
		$tablemodeltresanal->STD_MAX_SPEC = $lCalculateArrayData["StdSpecOver"];
		$tablemodeltresanal->PICTURE_URL = $lRowInspNo["PICTURE_URL"];
		$tablemodeltresanal->CALC_NUM = $lAfterCalculateData["CalcNam"];
		$tablemodeltresanal->MAX_VALUE = $lAfterCalculateData["MaxValue"];
		$tablemodeltresanal->MIN_VALUE = $lAfterCalculateData["MinValue"];
		$tablemodeltresanal->AVERAGE_VALUE = $lAfterCalculateData["AverageValue"];
		$tablemodeltresanal->RANGE_VALUE = $lAfterCalculateData["RangeValue"];
		$tablemodeltresanal->STDEV = $lAfterCalculateData["StdevValue"];
		$tablemodeltresanal->CPK = $lAfterCalculateData["CpkValue"];
		$tablemodeltresanal->XBAR_UCL = $lAfterCalculateData["XbarUclValue"];
		$tablemodeltresanal->XBAR_LCL = $lAfterCalculateData["XbarLclValue"];
		$tablemodeltresanal->RCHART_UCL = $lAfterCalculateData["RchartUclValue"];
		$tablemodeltresanal->RCHART_LCL = $lAfterCalculateData["RchartLclValue"];
		// $tablemodeltresanal->JUDGE_REASON = 
		// $tablemodeltresanal->INSERT_YMDHMS = 
		// $tablemodeltresanal->LAST_UPDATE_USER_ID = $lViewData["InspectorCode"];
		$tablemodeltresanal->LAST_UPDATE_USER_ID = Session::get('AA1010UserID');
		// $tablemodeltresanal->UPDATE_YMDHMS = 
		// $tablemodeltresanal->DATA_REV = 
		// Log::write('info', 'E setAnalyData');

		return $tablemodeltresanal;
	}
			

	//**************************************************************************
	// process         ScreenOpenSessionput
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ScreenOpenSessionput()
	{
		Session::put('BA1010FirstOpenJudgeFlg'	, 1);

		//Entry
		Session::put('BA1010ProcessIdForEntry'			, "");
		Session::put('BA1010CustomerIdForEntry'			, "");
		Session::put('BA1010CheckSheetNoForEntry'		, "");
		Session::put('BA1010MachineNoForEntry'			, "");
		Session::put('BA1010MoldNoForEntry'				, "");
		Session::put('BA1010InspectorCodeForEntry'		, "");
		Session::put('BA1010InspectionDateForEntry'		, "");
		Session::put('BA1010ConditionIdForEntry'		, "");
		Session::put('BA1010InspectionTimeIdForEntry'	, "");

		//Search
		Session::put('BA1010ProcessIdForSearch'			, "");
		Session::put('BA1010CustomerIdForSearch'		, "");
		Session::put('BA1010CheckSheetNoForSearch'		, "");
		Session::put('BA1010RevisionNoForSearch'		, "");
		Session::put('BA1010MachineNoForSearch'			, "");
		Session::put('BA1010MoldNoForSearch'			, "");
		Session::put('BA1010InspectorCodeForSearch'		, "");
		Session::put('BA1010ProductionDateFromForSearch', "");
		Session::put('BA1010ProductionDateToForSearch'	, "");
		Session::put('BA1010InspectionDateFromForSearch', "");
		Session::put('BA1010InspectionDateToForSearch'	, "");
		Session::put('BA1010PartNoForSearch'			, "");
		Session::put('BA1010SampleNoForSearch'			, "");
		Session::put('BA1010InspectionTimeIdForSearch'	, "");

		return null;
	}

	//**************************************************************************
	// process         ScreenInputItemSessionput
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function ScreenInputItemSessionput()
	{
		//Last Screen Infomation
		Session::put('BA0000LastScreenInfomation'		, "BA1010");

		//Entry
		Session::put('BA1010ProcessIdForEntry'			, Input::get('cmbProcessForEntry'));
		Session::put('BA1010CustomerIdForEntry'			, Input::get('cmbCustomerForEntry'));
		Session::put('BA1010CheckSheetNoForEntry'		, Input::get('cmbCheckSheetNoForEntry'));
		Session::put('BA1010MachineNoForEntry'			, TRIM((String)Input::get('txtMachineNoForEntry')));
		Session::put('BA1010MoldNoForEntry'				, TRIM((String)Input::get('txtMoldNoForEntry')));
		Session::put('BA1010InspectorCodeForEntry'		, TRIM((String)Input::get('txtInspectorCodeForEntry')));
		Session::put('BA1010InspectionDateForEntry'		, Input::get('txtInspectionDateForEntry'));
		Session::put('BA1010ConditionIdForEntry'		, Input::get('cmbConditionForEntry'));
		Session::put('BA1010InspectionTimeIdForEntry'	, Input::get('cmbInspectionTimeForEntry'));

		//Search
		Session::put('BA1010ProcessIdForSearch'			, Input::get('cmbProcessForSearch'));
		Session::put('BA1010CustomerIdForSearch'		, Input::get('cmbCustomerForSearch'));
		Session::put('BA1010CheckSheetNoForSearch'		, Input::get('cmbCheckSheetNoForSearch'));
		Session::put('BA1010RevisionNoForSearch'		, Input::get('cmbRevisionNoForSearch'));
		Session::put('BA1010MachineNoForSearch'			, Input::get('txtMachineNoForSearch'));
		Session::put('BA1010MoldNoForSearch'			, Input::get('txtMoldNoForSearch'));
		Session::put('BA1010InspectorCodeForSearch'		, Input::get('txtInspectorCodeForSearch'));
		Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
		Session::put('BA1010ProductionDateToForSearch'	, Input::get('txtProductionDateToForSearch'));
		Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
		Session::put('BA1010InspectionDateToForSearch'	, Input::get('txtInspectionDateToForSearch'));
		Session::put('BA1010PartNoForSearch'			, Input::get('txtPartNoForSearch'));
		Session::put('BA1010SampleNoForSearch'			, Input::get('txtSampleNoForSearch'));
		Session::put('BA1010InspectionTimeIdForSearch'	, Input::get('cmbInspectionTimeForSearch'));

		return null;
	}

	//**************************************************************************
	// process            initializeSessionData
	// overview           clear session data（when it return from entry screen and delete）
	// argument           Nothing
	// return value       Nothing
	// author             s-miyamoto
	// date               2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//**************************************************************************
	private function initializeSessionData()
	{
		//clear search result data
		Session::forget('BA1010InspectionResultData');
		Session::forget('BA1010NotExistNoData');

		//clear what set when Modify button
		Session::forget('BA1010InspectionResultNo');

		return null;
	}

	//**************************************************************************
	// process         convertDateFormat
	// overview        change data type
	// argument        date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//                 pattern(1:English->Japanese 2:Japanese->English)
	// return value    
	// author          s-miyamoto
	// date            Ver.01  2014.08.26 v1.00 FIX
	// remarks         
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}

	//**************************************************************************
	// process         OutputLog01
	//**************************************************************************
	private function OutputLog01($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Process Id"			=> Input::get('cmbProcessForEntry'   ,''),
				"Customer Id"			=> Input::get('cmbCustomerForEntry'   ,''),
				"Check Sheet No."		=> Input::get('cmbCheckSheetNoForEntry'   ,''),
				"M/C No."				=> Input::get('txtMachineNoForEntry'      ,''),
				"Mold No."				=> Input::get('txtMoldNoForEntry'      ,''),
				"Inspector Code"		=> Input::get('txtInspectorCodeForEntry'  ,''),
				"Inspection Date"		=> Input::get('txtInspectionDateForEntry' ,''),
				"Condition Id"			=> Input::get('cmbConditionForEntry'      ,''),
				"Inspection Time Id"	=> Input::get('cmbInspectionTimeForEntry' ,''),
			]
		);

		return null;
	}

	//**************************************************************************
	// process         OutputLog02
	//**************************************************************************
	private function OutputLog02($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Process Id"				=> Input::get('cmbProcessForSearch'            	,''),
				"Customer Id" 				=> Input::get('cmbCustomerForSearch'           	,''),
				"Check Sheet No."			=> Input::get('cmbCheckSheetNoForSearch'       	,''),
				"Revision No."				=> Input::get('cmbRevisionNoForSearch'         	,''),
				"M/C No."					=> Input::get('txtMachineNoForSearch'          	,''),
				"Mold No."					=> Input::get('txtMoldNoForSearch'          	,''),
				"Inspector Code"			=> Input::get('txtInspectorCodeForSearch'      	,''),
				"Production Date From"		=> Input::get('txtProductionDateFromForSearch' 	,''),
				"Production Date To"		=> Input::get('txtProductionDateToForSearch'   	,''),
				"Inspection Date From"		=> Input::get('txtInspectionDateFromForSearch' 	,''),
				"Inspection Date To"		=> Input::get('txtInspectionDateToForSearch'   	,''),
				"Part No."					=> Input::get('txtPartNoForSearch'             	,''),
				"Inspection Time Id"		=> Input::get('cmbInspectionTimeForSearch'     	,''),
			]
		);

		return null;
	}

	//**************************************************************************
	// process         OutputLog03
	//**************************************************************************
	private function OutputLog03($pArgument)
	{
		Log::write('info', $pArgument,
			[
				"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
				"Data Revision"          => Input::get('hidDataRev'            ,''),
				"Sample No."             => Input::get('txtSampleNoForSearch'  ,''),
			]
		);

		return null;
	}






	// //**************************************************************************
	// // process         setCustomerNoListEntry
	// // overview        get data for Inspection No.combo box and set Array to display screen
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function setCustomerNoListEntry($pViewData, $pMasterNo, $pProcessId, $pViewDataKey)
	// {
	// 	$lArrCustomerNoList     = ["" => ""];  //view data to return

	// 	//search
	// 	$lArrCustomerNoList = $this->getCustomerNoListEntry($pMasterNo, $pProcessId);

	// 	//add to array for transportion to screen(written in +=))
	// 	$pViewData += [
	// 		$pViewDataKey => $lArrCustomerNoList
	// 	];

	// 	return $pViewData;
	// }

	// //**************************************************************************
	// // process         getCustomerNoListEntry
	// // overview        get data for Inspection No.combo box with SQL
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function getCustomerNoListEntry($pMasterNo, $pProcessId)
	// {
	// 	$lTblCustomerNo			= []; //result of getting infomation of Inspection No.
	// 	$lRowCustomerNo			= []; //work when it change to array
	// 	$lArrCustomerNo			= []; //List of Inspection No. to return
	// 	$lRowCountCustomerNo		= 0;  //

	// 	$lTblCustomerNo = DB::select('
	// 	      SELECT CUST.CUSTOMER_ID
	// 	            ,CUST.CUSTOMER_NAME
	// 	        FROM TISHEETM AS SHET
	//  LEFT OUTER JOIN TCUSTOMM CUST
	// 	          ON CUST.CUSTOMER_ID = SHET.CUSTOMER_ID
		        
	// 	       WHERE SHET.DELETE_FLG  = "0"
	// 	         AND SHET.MASTER_NO   = :MasterNo
	// 	         AND SHET.PROCESS_ID  = :ProcessId
	// 	    GROUP BY SHET.CUSTOMER_ID
	// 	    ORDER BY SHET.REV_NO DESC
	// 	',
	// 			[
	// 				"MasterNo"   => $pMasterNo,
	// 				"ProcessId"	 => $pProcessId,
	// 			]
	// 	);

	// 	//cast search result to array（two dimensions Array）
	// 	$lTblCustomerNo = (array)$lTblCustomerNo;

	// 	//data exist
	// 	if ($lTblCustomerNo != null)
	// 	{
	// 		//store result in Array again
	// 		foreach ($lTblCustomerNo as $lRowCustomerNo) {
	// 			$lRowCountCustomerNo = $lRowCountCustomerNo + 1;
	// 		}

	// 		if ($lRowCountCustomerNo != 1)
	// 		{
	// 			//add blank space
	// 			$lArrCustomerNo += [
	// 				"" => ""
	// 			];
	// 		}

	// 		//store result in Array again
	// 		foreach ($lTblCustomerNo as $lRowCustomerNo) {
	// 			//Cast previously　because it has erroe to read value of array while CASTing
	// 			$lRowCustomerNo = (Array)$lRowCustomerNo;

	// 			$lArrCustomerNo += [
	// 				$lRowCustomerNo["CUSTOMER_ID"] => $lRowCustomerNo["CUSTOMER_NAME"]
	// 			];
	// 		}
	// 	}
	// 	else
	// 	{
	// 		//add blank space
	// 		$lArrCustomerNo += [
	// 			"" => ""
	// 		];
	// 	}

	// 	return $lArrCustomerNo;
	// }


	// //**************************************************************************
	// // process         getCustomerID
	// // overview        
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function getCustomerID($pMasterNo, $pProcessId)
	// {
	// 	$lTblCustomerID			= [];
		
	// 	$lTblCustomerID = DB::select('
	// 	      SELECT SHET.CUSTOMER_ID
	// 	        FROM TISHEETM AS SHET
		        
	// 	       WHERE SHET.DELETE_FLG  = "0"
	// 	         AND SHET.MASTER_NO   = :MasterNo
	// 	         AND SHET.PROCESS_ID  = :ProcessId
	// 	    GROUP BY SHET.CUSTOMER_ID
	// 	    ORDER BY SHET.REV_NO DESC
	// 	',
	// 			[
	// 				"MasterNo"   => $pMasterNo,
	// 				"ProcessId"	 => $pProcessId,
	// 			]
	// 	);
		
	// 	return $lTblCustomerID;
	// }



}
