<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Log;
use Session;
use Validator;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    instrument master maintenance
// overview      maintenance instrument master
// programmer    k-kagawa
// date    06.12.2014
// update  
//           
//**************************************************************************
class ZA2050EquipmentMasterController
extends Controller
{
	CONST NUMBER_PER_PAGE = 10;		//number of data per 1 page

	//**************************************************************************
	// process name    MasterAction
	// overview      display initial screen
	//           separate process as Entry、Search、Modify button
	//           do process corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion to screen
		
		$lTblSearchResultData		= []; //data table for inspection result list
		$lPagenation				= []; //for paging(return it to screen)

		$lTblMasterCheck 			= []; //for check master existane

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //screen lock mode
		$lPrevMode					= ""; //lock mode before transition

		//store screen entry and re-set
		$lViewData = $this->keepFromInputValue($lViewData);

		//receive parameter from login screen through Session and issue to array to transport
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"Equipment Name"  => Input::get('txtEquipmentNameForSearch'   ,''),
					"Equipment Cd"    => Input::get('txtEquipmentCDForSearch'      ,''),
				]
			);

			//in case of no error,search processing
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search processing
				$lTblSearchResultData = $this->getSearchMasterData();

				//in case of no data,error
				if (count($lTblSearchResultData) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}

				//store in session
				Session::put('ZA2050SearchResultData', $lTblSearchResultData);

				//set lock mode
				$lMode = "Search";
				Session::put('ZA2050ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//exchange value in edit area to initial value

			//exchange view data to initial value
			Session::put('ZA2050EquipmentCDForEntry', "");
			Session::put('ZA2050EquipmentInputTypeForEntry', "");
			Session::put('ZA2050EquipmentNameForEntry', "");
			Session::put('ZA2050EquipmentShortNameForEntry', "");
			Session::put('ZA2050EquipmentToolCdForEntry', "");
			Session::put('ZA2050DisplayOrderForEntry', "");

			//exchange view data to initial value
			$lViewData["EquipmentCDForEntry"] = "";
			$lViewData["EquipmentInputTypeForEntry"] = "";
			$lViewData["EquipmentNameForEntry"] = "";
			$lViewData["EquipmentShortNameForEntry"] = "";
			$lViewData["EquipmentToolCdForEntry"] = "";
			$lViewData["DisplayOrderForEntry"] = "";

			//set lock mode
			$lMode = "NewAdd";
			Session::put('ZA2050ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"Equipment Cd"     => Input::get('txtEquipmentCDForEntry'       ,''),
					"Equipment Name"   => Input::get('txtEquipmentNameForEntry'     ,''),
					"DisplayOrder"     => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"        => Session::get('ZA2050ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2050ActionMode');

			//if no error, update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate processing corresponding to former screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case new entry
					//get data for check
					$lTblMasterCheck = $this->getMasterCheckData(Input::get('txtEquipmentCDForEntry'),0);

					//if data does not exist,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT
						$lSuccessFlg = $this->insertMasterData();

						//if update successfully,display success message and back to the first screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2050ActionMode', "");
						}
						else
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//maintain the same condition to before Modify button is push as lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2050ActionMode', $lPrevMode);
						}
					}
					else
					//if data exists
					{
						//change the result tp array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//if delete flag is available,make data available for re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update　★★そのうちSunlit用で追加　,TRIM(Input::get('txtEquipmentFileNameForEntry'))
							$lKohshinCount = $this->updateMasterData(
																	  TRIM(Input::get('txtEquipmentCDForEntry'))
																	  ,Input::get('cmbEquipmentInputTypeForEntry')
																	 ,TRIM(Input::get('txtEquipmentNameForEntry'))
																	 ,TRIM(Input::get('txtEquipmentShortNameForEntry'))
																	 ,TRIM(Input::get('txtEquipmentToolCdForEntry'))
																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//if update successfully,display success message and back to the first screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2050ActionMode', "");
							}
							else
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//maintain the same condition to before Modify button is push as lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2050ActionMode', $lPrevMode);
							}
						}
						else
						//delete flag is available
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//maintain the same condition to before Modify button is push as lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2050ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result
					$lTblSearchResultData = Session::get('ZA2050SearchResultData');

					//set list data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//transform corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;

						//get data corresponding to Machine No. in edit
						if(TRIM(Input::get('txtEquipmentCDForEntry')) == TRIM((String)$lArrDataRow["SOKUTEIKI_CD"]))
						{
							//get data for check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["SOKUTEIKI_CD"])
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case it can get data,transform corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//if no data or virsion No. isn't correspond, error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//maintain the same condition to before Modify button is push as lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2050ActionMode', $lPrevMode);
							}
							else
							//if no error, start to update
							{
								//update　★★そのうちSunlit用で追加　,TRIM(Input::get('txtEquipmentFileNameForEntry'))
								$lKohshinCount = $this->updateMasterData(
									TRIM(Input::get('txtEquipmentCDForEntry'))
																		  ,Input::get('cmbEquipmentInputTypeForEntry')
																	 ,TRIM(Input::get('txtEquipmentNameForEntry'))
																	 ,TRIM(Input::get('txtEquipmentShortNameForEntry'))
																	 ,TRIM(Input::get('txtEquipmentToolCdForEntry'))
																		 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																		 ,"0"
																		);

								//if update successfully,display success message and back to the first screen
								if ($lKohshinCount != 0)
								{
									//finishing message
									$lViewData["NormalMessage"] = "I005 : Process has been completed.";

									//set lock mode
									$this->initializeSessionData();
									$lMode = "";
									Session::put('ZA2050ActionMode', "");
								}
								else
								{
									//error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E999 : System error has occurred. Contact your system manager."
									]);
									//maintain the same condition to before Modify button is push as lock mode 
									$lMode = $lPrevMode;
									Session::put('ZA2050ActionMode', $lPrevMode);
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"Equipment Cd"     => Input::get('txtEquipmentCDForEntry'       ,''),
					"Equipment Name"   => Input::get('txtEquipmentNameForEntry'     ,''),
					"DisplayOrder"     => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"        => Session::get('ZA2050ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2050ActionMode');

			//if no error, update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result
				$lTblSearchResultData = Session::get('ZA2050SearchResultData');

				//set list data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//transform corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to Machine No. in edit
					if(TRIM(Input::get('txtEquipmentCDForEntry')) == TRIM((String)$lArrDataRow["SOKUTEIKI_CD"]))
					{
						//get data for check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["SOKUTEIKI_CD"])
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case it can get data,transform corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//if no data or virsion No. isn't correspond, error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//maintain the same condition to before Modify button is push as lock mode 
							$lMode = $lPrevMode;
							Session::put('ZA2050ActionMode', $lPrevMode);
						}
						else
						//if no error, start to update
						{
							//update　★★そのうちSunlit用で追加　,$lArrDataRow["SOKUTEIKI_FILE_NAME"]
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["SOKUTEIKI_CD"]
																	  ,$lArrDataRow["INSPECTION_RESULT_INPUT_TYPE"]
																	 ,$lArrDataRow["SOKUTEIKI_NAME"]
																	 ,$lArrDataRow["SOKUTEIKI_SHORT_NAME"]
																	 ,$lArrDataRow["TOOL_CD"]
																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//if update successfully,display success message and back to the first screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2050ActionMode', "");
							}
							else
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//maintain the same condition to before Modify button is push as lock mode 
								$lMode = $lPrevMode;
								Session::put('ZA2050ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidCustomerID"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key corresponding to select
			$lEquipmentCd = Input::get('hidPrimaryKey1');
			//get search result
			$lTblSearchResultData = Session::get('ZA2050SearchResultData');

			//set list data stored in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//transform corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//in case machine No. exists,to set it in edit,write over in lViewData and session
				if(TRIM((String)$lEquipmentCd) == TRIM((String)$lArrDataRow["SOKUTEIKI_CD"]))
				{
					//write down in session
					Session::put('ZA2050EquipmentCDForEntry', $lArrDataRow["SOKUTEIKI_CD"]);
					Session::put('ZA2050EquipmentInputTypeForEntry', $lArrDataRow["INSPECTION_RESULT_INPUT_TYPE"]);
					Session::put('ZA2050EquipmentNameForEntry', $lArrDataRow["SOKUTEIKI_NAME"]);
					Session::put('ZA2050EquipmentShortNameForEntry', $lArrDataRow["SOKUTEIKI_SHORT_NAME"]);
					Session::put('ZA2050EquipmentToolCdForEntry', $lArrDataRow["TOOL_CD"]);
					Session::put('ZA2050DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["EquipmentCDForEntry"] = Session::get('ZA2050EquipmentCDForEntry');
					$lViewData["EquipmentInputTypeForEntry"] = Session::get('ZA2050EquipmentInputTypeForEntry');
					$lViewData["EquipmentNameForEntry"] = Session::get('ZA2050EquipmentNameForEntry');
					$lViewData["EquipmentShortNameForEntry"] = Session::get('ZA2050EquipmentShortNameForEntry');
					$lViewData["EquipmentToolCdForEntry"] = Session::get('ZA2050EquipmentToolCdForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2050DisplayOrderForEntry');
				}
			}

		// echo "<pre>";
		// print_r(Session::get('ZA2050EquipmentInputTypeForEntry'));
		// echo "</pre>";

		// echo "<pre>";
		// print_r($lViewData["EquipmentInputTypeForEntry"]);
		// echo "</pre>";

			//set lock mode
			$lMode = "Edit";
			Session::put('ZA2050ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
		
			//clear all information exclusing of entry information in session
			//in case "index.php/user/machinemaster" does not include in the origin of transtionURL =False
			//string returns otherwise
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/equipmentmaster');

				if($lPrevURL == false)
				{
					//delete search information
					$this->initializeSessionData();

					//set lock mode
					$lMode = "";
					Session::put('ZA2050ActionMode', "");
				}
				else
				{
					//issue blank in case search result list does not exist in session
					if (is_null(Session::get('ZA2050SearchResultData')))
					{
						//set lock mode
						$lMode = "";
						Session::put('ZA2050ActionMode', "");
					}
					else
					{
						//set lock mode
						$lMode = "Search";
						Session::put('ZA2050ActionMode', "Search");
					}
				}
			}
		}

		//issue blank in case search result list does not exist in session
		if (is_null(Session::get('ZA2050SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//get Search Result data from session
			$lTblSearchResultData = Session::get('ZA2050SearchResultData');
		}

		//make pagenation
		
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/equipmentmaster')); 
		//data, total number,number per page
		
		//add to Array to transport to screen
		$lViewData += [
			"Pagenator"       => $lPagenation,
		];

		//set infotmation of lock
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2050ActionMode'));

		// echo "<pre>";
		// print_r($lViewData);
		// echo "</pre>";

		return View("user.equipmentmaster", $lViewData);

	}

	//**************************************************************************
	// process name    lockControls
	// overview        ボタン等の画面の各項目のロック情報を設定する。
	// parameter       ビューデータ・画面のモード
	// returned value  ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索ボタン押下時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"MachineNoEditLock" => "",
				];
				break;
			case 'NewAdd':
				//新規追加ボタン押下時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "True",
					"MachineNoEditLock" => "",
				];
				break;
			case 'Edit':
				//編集ボタン押下時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"    => "Lock",
					"DeleteLock"    => "",
					"EditVisible"   => "True",
					"MachineNoEditLock" => "Lock",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"MachineNoEditLock" => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"    => "",
					"DeleteLock"    => "Lock",
					"EditVisible"   => "",
					"MachineNoEditLock" => "Lock",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// process name    getMasterCheckData
	// overview      for check master existanceの物理キーに該当するデータを取得する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pEquipmentcd,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  SKTE.DATA_REV
		           ,SKTE.DELETE_FLG
		      FROM TSOKTEIM AS SKTE															/* 測定器マスタ */
		     WHERE SKTE.SOKUTEIKI_CD        = :Equipmentcd									/* 画面入力値 */
		       AND SKTE.DATA_REV            = IF(:DataRev1 <> 0, :DataRev2, SKTE.DATA_REV)	/* 無ければ条件にしない */
		',
			[
				"Equipmentcd"  => $pEquipmentcd,
				"DataRev1"     => $pDataRev,
				"DataRev2"     => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process name    getSearchMasterData
	// overview      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programmer    k-kagawa
	// 追加		 M-motooka
	//**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table for inspection result list

		//★★そのうちSunlit用で追加　,SKTE.SOKUTEIKI_FILE_NAME
		$lTblSearchResultData = DB::select('
		    SELECT SKTE.SOKUTEIKI_CD
		          ,SKTE.INSPECTION_RESULT_INPUT_TYPE
		          ,CASE SKTE.INSPECTION_RESULT_INPUT_TYPE
		                WHEN "1" THEN "Input Value"
		                ELSE "Input OK NG"
		            END AS INSPECTION_RESULT_INPUT_TYPE_NAME
		          ,SKTE.SOKUTEIKI_NAME
		          ,SKTE.SOKUTEIKI_SHORT_NAME
		          ,SKTE.TOOL_CD
		          ,SKTE.DISPLAY_ORDER
		          ,SKTE.DELETE_FLG
		          ,SKTE.DATA_REV
		      FROM TSOKTEIM AS SKTE                                                 /* 測定器マスタ */
		     WHERE SKTE.DELETE_FLG          = "0"                                   /* 削除フラグ */
		       AND SKTE.SOKUTEIKI_CD        LIKE :EquipmentCd                       /* 測定器コード(前方一致) */
		       AND SKTE.SOKUTEIKI_NAME      LIKE :EquipmentName                     /* 測定器名(部分一致) */
		  ORDER BY SKTE.DISPLAY_ORDER
		          ,SKTE.SOKUTEIKI_CD
		',
			[
				"EquipmentCd"      => TRIM((String)Input::get('txtEquipmentCDForSearch'))."%",
				"EquipmentName"    => "%".TRIM((String)Input::get('txtEquipmentNameForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process name    insertMasterData
	// overview      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	// 本岡追加
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lEquipmentCd   = Input::get('txtEquipmentCDForEntry');
		$lEquipmentResultInputType   = Input::get('cmbEquipmentInputTypeForEntry');
		$lEquipmentName = Input::get('txtEquipmentNameForEntry');
		$lEquipmentShortName   = Input::get('txtEquipmentShortNameForEntry');
		$lEquipmentToolCd   = Input::get('txtEquipmentToolCdForEntry');
		$lDisplayOrder  = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			//★★そのうちSunlit用で追加　,$lEquipmentFileName
			function() use	(
							  $lEquipmentCd
							  ,$lEquipmentResultInputType
							 ,$lEquipmentName
							 ,$lEquipmentShortName
							 ,$lEquipmentToolCd
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::insert
				('
						INSERT INTO TSOKTEIM
						(
						  SOKUTEIKI_CD
						  ,INSPECTION_RESULT_INPUT_TYPE
						 ,SOKUTEIKI_NAME
						 ,SOKUTEIKI_SHORT_NAME
						 ,TOOL_CD
						 ,DISPLAY_ORDER
						 ,DELETE_FLG
						 ,DATA_REV
						)
						VALUES (
						         :EquipmentCd
						         ,:EquipmentResultInputType
						        ,:EquipmentName
						        ,:EquipmentShortName
						        ,:EquipmentToolCd
						        ,:DisplayOrder
						        ,"0"
						        ,1
						       )
				', 
					[
						"EquipmentCd"	=> $lEquipmentCd,
						"EquipmentResultInputType"	=> $lEquipmentResultInputType,
						"EquipmentName"	=> $lEquipmentName,
						"EquipmentShortName"	=> $lEquipmentShortName,
						"EquipmentToolCd"	=> $lEquipmentToolCd,
						"DisplayOrder"	=> $lDisplayOrder,
					]
				);
				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	}

	//**************************************************************************
	// process name    updateMasterData
	// overview      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	// 本岡追加 ,$pEquipmentFileName
	//**************************************************************************
	private function updateMasterData
	(
	  $pEquipmentCd
	  ,$pEquipmentResultInputType
	 ,$pEquipmentName
	 ,$pEquipmentShortName
	 ,$pEquipmentToolCd
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{

		//更新の成否をセット
		$lKohshinCnt = "";

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			//★★そのうちSunlit用で追加　,$pEquipmentFileName
			function() use (
							 $pEquipmentCd
							 ,$pEquipmentResultInputType
							,$pEquipmentName
							,$pEquipmentShortName
							,$pEquipmentToolCd
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				//★★そのうちSunlit用で追加　,SOKUTEIKI_FILE_NAME 	= :EquipmentFileName　"EquipmentFileName"		=> $pEquipmentFileName,
				$lKohshinCnt = DB::update
				('
						UPDATE TSOKTEIM
						   SET  SOKUTEIKI_CD	= :Equipmentcd
						   ,INSPECTION_RESULT_INPUT_TYPE	= :EquipmentResultInputType
						       ,SOKUTEIKI_NAME	= :EquipmentName
						       ,SOKUTEIKI_SHORT_NAME	= :EquipmentShortName
						       ,TOOL_CD	= :EquipmentToolCd
						       ,DISPLAY_ORDER	= :DisplayOrder
						       ,DELETE_FLG		= :DeleteFlg
						       ,DATA_REV		= DATA_REV + 1
						 WHERE SOKUTEIKI_CD	= :EquipmentCdWhere
				', 
					[
						"Equipmentcd"		=> $pEquipmentCd,
						"EquipmentCdWhere"	=> $pEquipmentCd,
						"EquipmentResultInputType"	=> $pEquipmentResultInputType,
						"EquipmentName"		=> $pEquipmentName,
						"EquipmentShortName"	=> $pEquipmentShortName,
						"EquipmentToolCd"	=> $pEquipmentToolCd,
						"DisplayOrder"		=> $pDisplayOrder,
						"DeleteFlg"			=> $pDeleteFlg,
					]
				);
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	}

	//**************************************************************************
	// process name    keepFromInputValue
	// overview      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
		if (Input::has('txtEquipmentCDForSearch'))
		{
			Session::put('ZA2050EquipmentCDForSearch', Input::get('txtEquipmentCDForSearch'));
		}
		else
		{
			Session::put('ZA2050EquipmentCDForSearch', "");
		}
		$pViewData += [
				"EquipmentCDForSearch"  => Session::get('ZA2050EquipmentCDForSearch')
		];
		

		if (Input::has('txtEquipmentNameForSearch'))
		{
			Session::put('ZA2050EquipmentNameForSearch', Input::get('txtEquipmentNameForSearch'));
		}
		else
		{
			Session::put('ZA2050EquipmentNameForSearch', "");
		}
		$pViewData += [
				"EquipmentNameForSearch"  => Session::get('ZA2050EquipmentNameForSearch')
		];




		if (Input::has('txtEquipmentCDForEntry'))
		{
			Session::put('ZA2050EquipmentCDForEntry', Input::get('txtEquipmentCDForEntry'));
		}
		else
		{
			Session::put('ZA2050EquipmentCDForEntry', "");
		}
		$pViewData += [
				"EquipmentCDForEntry"  => Session::get('ZA2050EquipmentCDForEntry')
		];
		

		if (Input::has('cmbEquipmentInputTypeForEntry'))
		{
			Session::put('ZA2050EquipmentInputTypeForEntry', Input::get('cmbEquipmentInputTypeForEntry'));
		}
		else
		{
			Session::put('ZA2050EquipmentInputTypeForEntry', "");
		}
		$pViewData += [
				"EquipmentInputTypeForEntry"  => Session::get('ZA2050EquipmentInputTypeForEntry')
		];


		if (Input::has('txtEquipmentNameForEntry'))
		{
			Session::put('ZA2050EquipmentNameForEntry', Input::get('txtEquipmentNameForEntry'));
		}
		else
		{
			Session::put('ZA2050EquipmentNameForEntry', "");
		}
		$pViewData += [
				"EquipmentNameForEntry"  => Session::get('ZA2050EquipmentNameForEntry')
		];
		

		if (Input::has('txtEquipmentShortNameForEntry'))
		{
			Session::put('ZA2050EquipmentShortNameForEntry', Input::get('txtEquipmentShortNameForEntry'));
		}
		else
		{
			Session::put('ZA2050EquipmentShortNameForEntry', "");
		}
		$pViewData += [
				"EquipmentShortNameForEntry"  => Session::get('ZA2050EquipmentShortNameForEntry')
		];


		if (Input::has('txtEquipmentToolCdForEntry'))
		{
			Session::put('ZA2050EquipmentToolCdForEntry', Input::get('txtEquipmentToolCdForEntry'));
		}
		else
		{
			Session::put('ZA2050EquipmentToolCdForEntry', "");
		}
		$pViewData += [
				"EquipmentToolCdForEntry"  => Session::get('ZA2050EquipmentToolCdForEntry')
		];
		

		if (Input::has('txtDisplayOrderForEntry'))
		{
			Session::put('ZA2050DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			Session::put('ZA2050DisplayOrderForEntry', "");
		}
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2050DisplayOrderForEntry')
		];


		return $pViewData;
	}

	//**************************************************************************
	// process name    initializeSessionData
	// overview        Clear Session
	// parameter       nothing
	// returned value  nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2050SearchResultData');

		//Entryボタン押下時に設定しているものをクリア
		Session::forget('ZA2050EquipmentCDForEntry');
		Session::forget('ZA2050EquipmentInputTypeForEntry');
		Session::forget('ZA2050EquipmentNameForEntry');
		Session::forget('ZA2050EquipmentShortNameForEntry');
		Session::forget('ZA2050EquipmentToolCdForEntry');
		Session::forget('ZA2050DisplayOrderForEntry');

		return null;
	}

	//**************************************************************************
	// process name    isErrorForRegist
	// overview        Item Check
	// parameter       
	// returned value  
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須Check
		//-------------------------
		//EquipmentCD
		$lValidator = Validator::make(
			array('txtEquipmentCDForEntry' => Input::get('txtEquipmentCDForEntry')),
			array('txtEquipmentCDForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E040 : Enter Equipment Cd ."
			]);
			return $pViewData;
		}

		//ResultInputType
		$lValidator = Validator::make(
			array('cmbEquipmentInputTypeForEntry' => Input::get('cmbEquipmentInputTypeForEntry')),
			array('cmbEquipmentInputTypeForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "EXXX : Enter Equipment Result Input Type."
			]);
			return $pViewData;
		}

		//EquipmentName
		$lValidator = Validator::make(
			array('txtEquipmentNameForEntry' => Input::get('txtEquipmentNameForEntry')),
			array('txtEquipmentNameForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Equipment Name."
			]);
			return $pViewData;
		}

		//DisplayOrder
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}


		//-------------------------
		//型式Check
		//-------------------------
		//EquipmentCD
		$lValidator = Validator::make(
			array('txtEquipmentCDForEntry' => Input::get('txtEquipmentCDForEntry')),
			array('txtEquipmentCDForEntry' => array('alpha_dash'))
		);

		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E039 : Equipment Cd is invalid."
			]);
			return $pViewData;
		}

		//DisplayOrder
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);

		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}

}