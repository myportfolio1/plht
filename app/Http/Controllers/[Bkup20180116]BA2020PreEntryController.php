<?php

use Illuminate\Support\MessageBag;

//■■15-08-2016 Motooka added for SUNLIT■■
set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';

class BA2020PreEntryController
extends Controller
{

	CONST DECIMAL_LENG = 2;               //小数点以下は2桁
	
	//**************************************************************************
	// Processing      EntryAction
	// Overview        Open Screen, etc
	//                 (概況登録画面の初期表示を行う。また、Entry等のボタンにより
	//                 処理を分岐し、それぞれに対応した処理を行う。)
	// parameter       nothing
	// returned value  nothing
	// programer       m-motooka
	// date            2017.02.15
	// history         2017.02.15 v0.01 first making
	//                 
	//**************************************************************************
	public function EntryAction()
	{
		$lViewData = [];                     //For transportion to screen

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		//get parameter from search result list through Session and enter to Array to transport to screen
		if (Session::has('BA1010InspectionResultNo'))
		{
			
		}
		else
		{
			//in case screen is transported from Entry
			$lViewData += [
				"MasterNo"            => Session::get('BA1010MasterNoForEntry'),
				"Process"             => Session::get('BA1010ProcessForEntry'),
				"ProcessName"         => Session::get('BA1010ProcessForEntryName'),
				"Customer"            => Session::get('BA1010CustomerForEntry'),
				"CustomerName"        => Session::get('BA1010CustomerName'),
				"CheckSheetNo"        => Session::get('BA1010CheckSheetNoForEntry'),
				"CheckSheetName"      => Session::get('BA1010CheckSheetName'),
				"RevisionNo"          => Session::get('BA1010RevisionNo'),
				"ItemNo"              => Session::get('BA1010ItemNo'),
				"ItemName"            => Session::get('BA1010ItemName'),
				"MaterialName"        => Session::get('BA1010MaterialNameForEntry'),
				"MaterialSize"        => Session::get('BA1010MaterialSizeForEntry'),
				"MachineNo"           => Session::get('BA1010MachineNoForEntry'),
				"MachineNoName"       => Session::get('BA1010MachineNoForEntryName'),
				"InspectorCode"       => Session::get('BA1010InspectorCodeForEntry'),
				"InspectorName"       => Session::get('BA1010InspectorName'),
				"InspectionDate"      => Session::get('BA1010InspectionDateForEntry'),
				"Condition"           => Session::get('BA1010ConditionForEntry'),
				"ConditionName"       => Session::get('BA1010ConditionForEntryName'),
				"InspectionTime"      => Session::get('BA1010InspectionTimeForEntry'),
				"InspectionTimeName"  => Session::get('BA1010InspectionTimeForEntryName'),
				"ModelName"           => Session::get('BA1010ModelName'),
				"DrawingNo"           => Session::get('BA1010DrawingNo'),
				"ISOKanriNo"          => Session::get('BA1010ISOKanriNo'),
				"ProductWeight"       => Session::get('BA1010ProductWeight'),
				"HeatTreatmentType"   => Session::get('BA1010HeatTreatmentType'),
				"PlatingKind"         => Session::get('BA1010PlatingKind'),
			];
		}
		
		if (Input::has('btnNext'))  //Next button
		{
			//log
			Log::write('info', 'Next Button Click.',
				[
					"Process"  => Session::get('BA1010ProcessForEntry')
				]
			);

			//store input data for entry in session
			Session::put('BA2020ProcessForEntry', Session::get('BA1010ProcessForEntry'));

			//check
			//in case of Incoming (Material), Enter Material Lot No.,Certificate No.,P/O No.,Invoice No.,Quantity Coil,Quantity Weight
			if(Session::get('BA2020ProcessForEntry') == "01")
			{
				if(Input::get('txtMaterialLotNoForEntry') != null)
				{
					if(Input::get('txtCertificateNoForEntry') != null)
					{
						if(Input::get('txtPONoForEntry') != null)
						{
							if(Input::get('txtInvoiceNoForEntry') != null)
							{
								if(Input::get('txtQuantityCoilForEntry') != null)
								{
									if(Input::get('txtQuantityWeightForEntry') != null)
									{
									}
									else
									{
										//error message
										$lViewData["errors"] = new MessageBag([
											"error" => "E059 : Enter Quantity Weight."
										]);
									}
								}
								else
								{
									//error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E058 : Enter Quantity Coil."
									]);
								}
							}
							else
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E057 : Enter Invoice No.."
								]);
							}
						}
						else
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E061 : Enter P/O No.."
							]);
						}
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E056 : Enter Certificate No.."
						]);
					}
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E055 : Enter Material Lot No.."
					]);
				}
			}
			//in case of Incoming (Not Material),Enter Certificate No.,P/O No.,Invoice No.,Quantity Weight,Lot No.
			else if(Session::get('BA2020ProcessForEntry') == "02")
			{
				if(Input::get('txtCertificateNoForEntry') != null)
				{
					if(Input::get('txtPONoForEntry') != null)
					{
						if(Input::get('txtInvoiceNoForEntry') != null)
						{
							if(Input::get('txtQuantityWeightForEntry') != null)
							{
								if(Input::get('txtLotNoForEntry') != null)
								{
								}
								else
								{
									//error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E060 : Enter Lot No.."
									]);
								}
							}
							else
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E059 : Enter Quantity Weight."
								]);
							}
						}
						else
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E057 : Enter Invoice No.."
							]);
						}
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E061 : Enter P/O No.."
						]);
					}
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E056 : Enter Certificate No.."
					]);
				}
			}
			//in case of Treatment, Enter Certificate No., Invoice No., Quantity Weight, Lot No.
			else if(Session::get('BA2020ProcessForEntry') == "06" || Session::get('BA2020ProcessForEntry') == "07")
			{
				if(Input::get('txtCertificateNoForEntry') != null)
				{
					if(Input::get('txtInvoiceNoForEntry') != null)
					{
						if(Input::get('txtQuantityWeightForEntry') != null)
						{
							if(Input::get('txtLotNoForEntry') != null)
							{
							}
							else
							{
								//error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E060 : Enter Lot No.."
								]);
							}
						}
						else
						{
							//error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E059 : Enter Quantity Weight."
							]);
						}
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E057 : Enter Invoice No.."
						]);
					}
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E056 : Enter Certificate No.."
					]);
				}
			}
			//in case of Final Inspection, Enter Material Lot No., Lot No.
			else if(Session::get('BA2020ProcessForEntry') == "08")
			{
				if(Input::get('txtMaterialLotNoForEntry') != null)
				{
					if(Input::get('txtLotNoForEntry') != null)
					{
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E060 : Enter Lot No.."
						]);
					}
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E055 : Enter Material Lot No.."
					]);
				}
			}
			//in case of Header, Cutting or Rolling, Enter Lot No.
			else
			{
				if(Input::get('txtLotNoForEntry') != null)
				{
				}
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E060 : Enter Lot No.."
					]);
				}
			}

			//exept for no error(エラーが無い場合)
			if (array_key_exists("errors", $lViewData) == false)
			{
				if (Input::get('txtQuantityCoilForEntry') != null)
				{
					if (Input::get('txtQuantityCoilForEntry') <= "9999.99")
					{
						//小数点2桁チェックをしたい
						//preg_match( '/^[0-9]+(.[0-9]{1,' . self::DECIMAL_LENG . '})?$/', $val ) > 0;
						//何もしない
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E054 : Input Max is '9999.99'."
						]);
					}
				}
			}

			//exept for no error(エラーが無い場合)
			if (array_key_exists("errors", $lViewData) == false)
			{
				if(Input::get('txtQuantityWeightForEntry') != null)
				{
					if(Input::get('txtQuantityWeightForEntry') <= "9999.99")
					{
						//何もしない
					}
					else
					{
						//error message
						$lViewData["errors"] = new MessageBag([
							"error" => "E054 : Input Max is '9999.99'."
						]);
					}
				}
			}

			//exept for no error(エラーが無い場合)
			if (array_key_exists("errors", $lViewData) == false)
			{
				//store input data for entry in session
				Session::put('BA2020MaterialLotNoForEntry'  , Input::get('txtMaterialLotNoForEntry'));
				Session::put('BA2020CertificateNoForEntry'  , Input::get('txtCertificateNoForEntry'));
				Session::put('BA2020PONoForEntry'           , Input::get('txtPONoForEntry'));
				Session::put('BA2020InvoiceNoForEntry'      , Input::get('txtInvoiceNoForEntry'));
				Session::put('BA2020QuantityCoilForEntry'   , Input::get('txtQuantityCoilForEntry'));
				Session::put('BA2020QuantityWeightForEntry' , Input::get('txtQuantityWeightForEntry'));
				Session::put('BA2020LotNoForEntry'          , Input::get('txtLotNoForEntry'));

				//transition to entry screen
				return Redirect::route("user/entry");
			}
			else
			{
			
			}
		}
		elseif (Input::has('btnReturn'))    //■Return button
		{
			//log
			Log::write('info', 'Return Button Click.');
			
			//transport to list screen
			return Redirect::route("user/list");
		}
		else                                                                    //■■■■transition, paging from other screen or menu■■■■
		{
			//当画面のセッション情報のうち、リスト画面から遷移してきた場合全てクリアする（検索前の状態に戻すため）
			//遷移元のURLに「index.php/user/list含んでいる場合」（＝List画面からの遷移の場合）＝True
			//遷移元がそれ以外の場合は文字列が返ってくる（Trueにはならない）
			//clear information except for entry in screen in session（delete search result and restore before state
			//in case index.php/user/list  including in URL,(＝transition from list screen）＝True
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/list');

				if($lPrevURL == true)
				{
					//delete information of search（reset）
					$this->initializeSessionData();
				}
			}
		}
		
		//■must pass
		//store and set again entry in screen
		$lViewData = $this->setPreEntryForms($lViewData);

		//display screen
		return View::make("user/preentry", $lViewData);
	}
	
	//**************************************************************************
	// process    initializeSessionData
	// overview      clear session data（when it return from list screen and delete）
	// argument      Nothing
	// return value    Nothing
	// author    m-motooka
	// date    2017.04.10
	// record of updates  2017.04.10 v0.01 first making
	//           2017.04.10 v1.00 FIX
	//**************************************************************************
	private function initializeSessionData()
	{
		//clear what set when Entry button
		Session::forget('BA2020MaterialLotNoForEntry');
		Session::forget('BA2020CertificateNoForEntry');
		Session::forget('BA2020PONoForEntry');
		Session::forget('BA2020InvoiceNoForEntry');
		Session::forget('BA2020QuantityCoilForEntry');
		Session::forget('BA2020QuantityWeightForEntry');
		Session::forget('BA2020LotNoForEntry');

		return null;
	}
	
	//**************************************************************************
	// process    setPreEntryForms
	// overview      store and return form input information of search result list screen
	// argument      Arary
	// return value    Array
	// author    m-motooka
	// date    2017.04.10
	// record of updates  2017.04.10 v0.01 first making
	//           2017.04.10 v1.00 FIX
	//**************************************************************************
	private function setPreEntryForms($pViewData)
	{

		//■①store Material Lot No for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020MaterialLotNoForEntry'))) {

			if (Input::has('txtMaterialLotNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA2020MaterialLotNoForEntry', Input::get('txtMaterialLotNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA2020MaterialLotNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMaterialLotNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA2020MaterialLotNoForEntry', Input::get('txtMaterialLotNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialLotNoForEntry"  => Session::get('BA2020MaterialLotNoForEntry')
		];


		//■②store Certificate No for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020CertificateNoForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtCertificateNoForEntry')) {
				Session::put('BA2020CertificateNoForEntry', Input::get('txtCertificateNoForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA2020CertificateNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtCertificateNoForEntry')) {
				Session::put('BA2020CertificateNoForEntry', Input::get('txtCertificateNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CertificateNoForEntry"  => Session::get('BA2020CertificateNoForEntry')
		];


		//■③store PO No for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020PONoForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtPONoForEntry')) {
				Session::put('BA2020PONoForEntry', Input::get('txtPONoForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA2020PONoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtPONoForEntry')) {
				Session::put('BA2020PONoForEntry', Input::get('txtPONoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"PONoForEntry"  => Session::get('BA2020PONoForEntry')
		];


		//■④store Invoice No for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020InvoiceNoForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtInvoiceNoForEntry')) {
				Session::put('BA2020InvoiceNoForEntry', Input::get('txtInvoiceNoForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA2020InvoiceNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtInvoiceNoForEntry')) {
				Session::put('BA2020InvoiceNoForEntry', Input::get('txtInvoiceNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InvoiceNoForEntry"  => Session::get('BA2020InvoiceNoForEntry')
		];


		//■⑤store Quantity Coil for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020QuantityCoilForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtQuantityCoilForEntry')) {
				Session::put('BA2020QuantityCoilForEntry', Input::get('txtQuantityCoilForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA2020QuantityCoilForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtQuantityCoilForEntry')) {
				Session::put('BA2020QuantityCoilForEntry', Input::get('txtQuantityCoilForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"QuantityCoilForEntry"  => Session::get('BA2020QuantityCoilForEntry')
		];


		//■⑥store Quantity Weight for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020QuantityWeightForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('txtQuantityWeightForEntry')) {
				Session::put('BA2020QuantityWeightForEntry', Input::get('txtQuantityWeightForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA2020QuantityWeightForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('txtQuantityWeightForEntry')) {
				Session::put('BA2020QuantityWeightForEntry', Input::get('txtQuantityWeightForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"QuantityWeightForEntry"  => Session::get('BA2020QuantityWeightForEntry')
		];


		//■⑦store Lot No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA2020LotNoForEntry'))) {

			if (Input::has('txtLotNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA2020LotNoForEntry', Input::get('txtLotNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA2020LotNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtLotNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA2020LotNoForEntry', Input::get('txtLotNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"LotNoForEntry"  => Session::get('BA2020LotNoForEntry')
		];

		return $pViewData;
	}

}