<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;

use Auth;
use DB;
use Log;
use Session;
use Validator;
use Redirect;



class AA1010LoginController
extends Controller
{
	//**************************************************************************
	// 処理名    LoginAction
	// 概要      ログイン画面の初期表示、入力エラーチェック、ログイン処理、
	//           検索結果一覧への画面遷移を行う。
	// 引数      無し
	// 戻り値    無し
	// 作成者    s-miyamoto
	// 作成日    2014.05.14
	// 更新履歴  2014.05.14 v0.01 初回作成
	//           2014.08.26 v1.00 納品で一旦FIX
	//**************************************************************************
	//Laravel VerUp From 4.2 To 5.5
	// public function LoginAction()
	public function LoginAction(Request $request)
	{
		$lViewData  = [];               //画面への情報返却用（入力値、エラーメッセージ等）
		$lDataTable = [];               //DataTable
		$lDataRow   = [];               //DataRow
		$lErrors    = new MessageBag(); //エラーメッセージ格納用
		
		//直前の「Errors」が存在するなら保持
		//Laravel VerUp From 4.2 To 5.5
		// if ($lOldErrors = Input::old("Errors"))
		// {
		// 	$lErrors = $lOldErrors;
		// }
		if ($lOldErrors = $request->old("Errors"))
		{
			$lErrors = $lOldErrors;
		}
		
		//POSTされた場合にバリデーションによる入力チェックを行う
		//Laravel VerUp From 4.2 To 5.5
		// if (Input::server("REQUEST_METHOD") == "POST")
		if ($request->method() == "POST")
		{
			//UserID必須チェック
			$lValidator = Validator::make(
				array('txtUserID' => Input::get('txtUserID')),
				array('txtUserID' => array('required'))
			);
			//エラーの場合
			if ($lValidator->fails())
			{
				//エラーメッセージ設定
				$lViewData["Errors"] = new MessageBag([
					"error" => "E001 : Enter User ID."
				]);
			}
			
			//他の項目でエラーが出ていない場合
			if (array_key_exists("Errors", $lViewData) == false)
			{
				//UserID型チェック
				$lValidator = Validator::make(
					array('txtUserID' => Input::get('txtUserID')),
					array('txtUserID' => array('alpha_dash'))
				);
				//エラーの場合
				if ($lValidator->fails()) {
				
					//エラーメッセージ設定
					$lViewData["Errors"] = new MessageBag([
						"error" => "E002 : User ID is invalid."
					]);
				}
			}
			
			//他の項目でエラーが出ていない場合
			if (array_key_exists("Errors", $lViewData) == false)
			{
				//Password必須チェック
				$lValidator = Validator::make(
					array('txtPassword' => Input::get('txtPassword')),
					array('txtPassword' => array('required'))
				);
				//エラーの場合
				if ($lValidator->fails()) {
				
					//エラーメッセージ設定
					$lViewData["Errors"] = new MessageBag([
						"error" => "E003 : Enter Password."
					]);
				}
			}
			
			//他の項目でエラーが出ていない場合
			if (array_key_exists("Errors", $lViewData) == false)
			{
				//Password型チェック
				$lValidator = Validator::make(
					array('txtPassword' => Input::get('txtPassword')),
					array('txtPassword' => array('alpha_dash'))
				);
				//エラーの場合
				if ($lValidator->fails()) {
				
					//エラーメッセージ設定
					$lViewData["Errors"] = new MessageBag([
						"error" => "E004 : Password is invalid."
					]);
				}
			}
			
			//入力エラーが無い
			if (array_key_exists("Errors", $lViewData) == false)
			{
				//検索処理
				$lDataTable = DB::select('
					SELECT USER_ID
					      ,USER_NAME
					      ,ADMIN_FLG
					  FROM TUSERMST
					 WHERE USER_ID    = :UserID
					   AND PASSWORD   = :Password
					   AND DELETE_FLG = "0"
				',
					[
						"UserID"   => (String)Input::get('txtUserID'),
						"Password" => (String)Input::get('txtPassword')
					]
				);
				
				//検索結果をarrayにcast（この時点では2次元配列）
				$lDataTable = (array)$lDataTable;
				
				//データが存在する
				if ($lDataTable != null)
				{
					//2次元配列から1行目を取得
					$lDataRow = (array)$lDataTable[0];
					
					//セッションに値を格納　格納元プログラムのID（AA1010）を先頭に付与している
					Session::put('AA1010UserID',   $lDataRow["USER_ID"]);
					Session::put('AA1010UserName', $lDataRow["USER_NAME"]);
					Session::put('AA1010AdminFlg', $lDataRow["ADMIN_FLG"]);
					
					//ログイン状態に更新する（Laravel標準機能に頼っているため、変数名はこのままにしておく）
					$credentials = [
						"user_id"  => (String)Input::get('txtUserID'),
						"password" => (String)Input::get('txtPassword')
					];
					
					//正常にログインできている場合は～という条件（これもLaravel標準機能に頼っている）
					if (Auth::attempt($credentials))
					{
						//ログインのログ記録
						Log::write('info', (String)Input::get('txtUserID').' Login.');
						
						//一覧画面へ遷移
						//Laravel VerUp From 4.2 To 5.5
						// return Redirect::route("user/list");
						return Redirect::route("user.list");
					}
				}
				//データが存在しない
				else
				{
					//エラーメッセージ設定
					$lViewData["Errors"] = new MessageBag([
						"error" => "E005 : User ID or Password is incorrect."
					]);
					
					//入力エラー時、ユーザIDは直前の入力値を保持
					$lViewData["txtUserID"] = Input::get("txtUserID");
					//Laravel VerUp From 4.2 To 5.5
					// return Redirect::route("user/login") -> withInput($lViewData);
					return Redirect::route("user.login") -> withInput($lViewData);
				}
				
			}
			//入力エラーがある
			else
			{
				//入力エラー時、ユーザIDは直前の入力値を保持
				$lViewData["txtUserID"] = Input::get("txtUserID");
				//Laravel VerUp From 4.2 To 5.5
				// return Redirect::route("user/login") -> withInput($lViewData);
				return Redirect::route("user.login") -> withInput($lViewData);
			}
			
		}
		
		//ここまでにエラーが無い場合は画面描画で落ちるので対処
		$lViewData = [
			"Errors" => $lErrors
		];
		
		//ログイン画面表示
		//Laravel VerUp From 4.2 To 5.5
		// return View::make("user/login", $lViewData);
		return view("user.login", $lViewData);
	}

	//**************************************************************************
	// 処理名    LogoutAction
	// 概要      ログアウト処理を行う。
	// 引数      無し
	// 戻り値    無し
	// 作成者    s-miyamoto
	// 作成日    2014.05.15
	// 更新履歴  2014.05.15 v0.01 初回作成
	//           2014.08.26 v1.00 納品で一旦FIX
	//**************************************************************************
	public function LogoutAction()
	{
		//ログアウトのログ記録
		Log::write('info', Session::get('AA1010UserID').' Logout.');
		
		//セッションの全データを破棄してログアウト
		Session::flush();
		Auth::logout();

		//Laravel VerUp From 4.2 To 5.5
		// return Redirect::route("user/login");
		return Redirect::route("user.login");
	}


}