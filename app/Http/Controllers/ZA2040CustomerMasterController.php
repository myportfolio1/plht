<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Log;
use Session;
use Validator;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    maintenance customer master
// over view      maintenance customer master
// programer    k-kagawa
// date    2014.12.06
// update  
//           
//**************************************************************************
class ZA2040CustomerMasterController
extends Controller
{

	//-------------
	//■■define constance
	CONST NUMBER_PER_PAGE = 10;		//number of data per 1 page

	//**************************************************************************
	// processing name    MasterAction
	// over view      display initial screen
	//           separate processing as Entry,Search,Modify button
	//           do processing corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion of data to screen
		
		$lTblSearchResultData		= []; //data table of inspection result list
		$lPagenation				= []; //for paging

		$lTblMasterCheck 			= []; //for master existance check

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //lock mode of screen
		$lPrevMode					= ""; //lock mode of screen before transition

		//store and re-set entry item
		$lViewData = $this->keepFromInputValue($lViewData);

    // Select Inspection Sheet Format from database 
	// date            04/01/2018
	// author          Mai:) 
        $lViewData = $this->setInspectionSheetFormat($lViewData);

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"Customer ID"       => Input::get('txtCustomerIDForSearch'      ,''),
					"Customer Name"  => Input::get('txtCustomerNameForSearch'   ,''),
				]
			);

			//in case of no data,search
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search
				$lTblSearchResultData = $this->getSearchMasterData();

				//in case of no data,error
				if (count($lTblSearchResultData) == 0)
				{
					//set error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				
				//store in session
				Session::put('ZA2040SearchResultData', $lTblSearchResultData);

				//set lock mode in screen
				$lMode = "Search";
				Session::put('ZA2040ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//make value in edit field initial value

			//exchange session to initial value
			Session::put('ZA2040CustomerIDForEntry', "");
			Session::put('ZA2040CustomerNameForEntry', "");
	//exchange session to initial value	for Inspection Sheet Format		
	// date            05/01/2018
	// author          Mai:) 
			Session::put('ZA2040InspectionSheetFormatForEntry', "");
			//Session::put('ZA2020arrDataInspectionSheetFormat', "");

			Session::put('ZA2040DisplayOrderForEntry', "");

			//exchange view data to initial value
			$lViewData["CustomerIDForEntry"] = "";
			$lViewData["CustomerNameForEntry"] = "";
	//exchange view data to initial value for Inspection Sheet Format
	// date            05/01/2018
	// author          Mai:)
			$lViewData["InspectionSheetFormatForEntry"] = "";
			$lViewData["DisplayOrderForEntry"] = "";

			//set lock mode in screen
			$lMode = "NewAdd";
			Session::put('ZA2040ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"Customer ID"       => Input::get('txtCustomerIDForEntry'       ,''),
					"Customer Name"     => Input::get('txtCustomerNameForEntry'     ,''),
	// date            04/01/2018
	// author          Mai:) 
					"Inspection Sheet Format"      => Input::get('cmbInspectionSheetFormatForEntry'    ,''),
					"DisplayOrder"      => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"         => Session::get('ZA2040ActionMode'),
				]
			);
          
			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2040ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate processing corresponding to prevent screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case new entry
					//get data for logic check
					$lTblMasterCheck = $this->getMasterCheckData(Input::get('txtCustomerIDForEntry'),0);

					//in case data does not exist,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT
						$lSuccessFlg = $this->insertMasterData();
                       
						//in case update successfully, display message and return to initial screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode in screen
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2040ActionMode', "");
						}
						else
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2040ActionMode', $lPrevMode);
						}
					}
					else
					//in case data exists
					{
						//exchange result to array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//in case delete flag is valid,make data valid for re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  TRIM(Input::get('txtCustomerIDForEntry'))
																	 ,TRIM(Input::get('txtCustomerNameForEntry'))
	// date            04/01/2018
	// author          Mai:) 
																	 ,TRIM(Input::get('cmbInspectionSheetFormatForEntry'     ,''))
																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2040ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2040ActionMode', $lPrevMode);
							}
						}
						else
						//in case delete flag is invalid,key reduplication error
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2040ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result data
					$lTblSearchResultData = Session::get('ZA2040SearchResultData');

					//set list data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//change corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;
                         
						//get data corresponding to machine No. in edit field and start process
						if(TRIM(Input::get('txtCustomerIDForEntry')) == TRIM((String)$lArrDataRow["CUSTOMER_ID"]))
						{
							//get data for logic check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["CUSTOMER_ID"])
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case of getting data,change corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//in case data does not exist or version is not same,error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2040ActionMode', $lPrevMode);
							}
							else
							//in case of no error,start to update
							{
								// update
								$lKohshinCount = $this->updateMasterData(
																		  TRIM(Input::get('txtCustomerIDForEntry'))
																		 ,TRIM(Input::get('txtCustomerNameForEntry'))
	// date            04/01/2018
	// author          Mai:) 
																	     ,TRIM(Input::get('cmbInspectionSheetFormatForEntry'     ,''))
																		 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																		 ,"0"
																		);

								//in case update successfully, display message and return to initial screen
								if ($lKohshinCount != 0)
								{
									//finishing message
									$lViewData["NormalMessage"] = "I005 : Process has been completed.";

									//set lock mode in screen
									$this->initializeSessionData();
									$lMode = "";
									Session::put('ZA2040ActionMode', "");
								}
								else
								{
									//set error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E999 : System error has occurred. Contact your system manager."
									]);
									//keep the same condition to before update button is push for screen lock mode
									$lMode = $lPrevMode;
									Session::put('ZA2040ActionMode', $lPrevMode);
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"Customer ID"       => Input::get('txtCustomerIDForEntry'       ,''),
					"Customer Name"      => Input::get('txtCustomerNameForEntry'     ,''),
	// date            04/01/2018
	// author          Mai:) 
					"Inspection Sheet Format"      => Input::get('cmbInspectionSheetFormatForEntry'    ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2040ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2040ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result data
				$lTblSearchResultData = Session::get('ZA2040SearchResultData');

				//set list data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//change corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to machine No. in edit field and start process
					if(TRIM(Input::get('txtCustomerIDForEntry')) == TRIM((String)$lArrDataRow["CUSTOMER_ID"]))
					{
						//get data for logic check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["CUSTOMER_ID"])
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case of getting data,change corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//in case data does not exist or version is not same,error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2040ActionMode', $lPrevMode);
						}
						else
						//in case of no error,start to update
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["CUSTOMER_ID"]
																	 ,$lArrDataRow["CUSTOMER_NAME"]
	// date            04/01/2018
	// author          Mai:) 
																	 ,$lArrDataRow["INSPECTION_SHEET_FORMAT_ID"]
																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2040ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2040ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidCustomerID"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key in corresponding line
			$lCustomerID = Input::get('hidPrimaryKey1');
			//get search result data
			$lTblSearchResultData = Session::get('ZA2040SearchResultData');

			//set list data in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//change corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//in case machin No. exists,write over on lViewData and session to set in edit field
				if(TRIM((String)$lCustomerID) == TRIM((String)$lArrDataRow["CUSTOMER_ID"]))
				{
					//write down in session
					Session::put('ZA2040CustomerIDForEntry', $lArrDataRow["CUSTOMER_ID"]);
					Session::put('ZA2040CustomerNameForEntry', $lArrDataRow["CUSTOMER_NAME"]);
	// write down in session for Inspection Sheet Format
	// date            04/01/2018
	// author          Mai:) 					
					Session::put('ZA2040InspectionSheetFormatForEntry', $lArrDataRow["INSPECTION_SHEET_FORMAT_ID"]);
					Session::put('ZA2040DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["CustomerIDForEntry"] = Session::get('ZA2040CustomerIDForEntry');
					$lViewData["CustomerNameForEntry"] = Session::get('ZA2040CustomerNameForEntry');
	// exchange view data in edit field for Inspection Sheet Format 
	// date            04/01/2018
	// author          Mai:) 	
					$lViewData["InspectionSheetFormatForEntry"] = Session::get('ZA2040InspectionSheetFormatForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2040DisplayOrderForEntry');
				}
			}

			//set lock mode in screen
			$lMode = "Edit";
			Session::put('ZA2040ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
			//clear all information except entry information in this screen session
			//in case URL in origin of transition does not include"index.php/user/customermaster",False
			//in case origin of transition is other,return string
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/customermaster');

				if($lPrevURL == false)
				{
					//delete all search information
					$this->initializeSessionData();

					//set lock mode in screen
					$lMode = "";
					Session::put('ZA2040ActionMode', "");
				}
				else
				{
					//if search result data does not exist in session,issue  blank
					if (is_null(Session::get('ZA2040SearchResultData')))
					{
						//set lock mode in screen
						$lMode = "";
						Session::put('ZA2040ActionMode', "");
					}
					else
					{
						//set lock mode in screen
						$lMode = "Search";
						Session::put('ZA2040ActionMode', "Search");
					}
				}
			}
		}

		//if search result data does not exist in session,issue  blank
		if (is_null(Session::get('ZA2040SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//get search result data from session
			$lTblSearchResultData = Session::get('ZA2040SearchResultData');
		}

		//make pagenation
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/customermaster')); 
		//data,total issue,issue per 1 page
		
		//add to array for transportion to screen
		$lViewData += [
			"Pagenator"       => $lPagenation,
		];

		//set lock information in screen
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2040ActionMode'));

		return View("user.customermaster", $lViewData);

	}

	//**************************************************************************
	// processing name    lockControls
	// over view      ボタン等の画面の各項目のロック情報を設定する。
	// parameter      ビューデータ・画面のモード
	// returned value    ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			case 'NewAdd':
				//新規追加 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "",
				];
				break;
			case 'Edit':
				//編集 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "Lock",
					"DeleteLock"   => "",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "Lock",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "Lock",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getMasterCheckData
	// over view      for master existance checkの物理キーに該当するデータを取得する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pCustomerid,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  CUST.DATA_REV
		           ,CUST.DELETE_FLG
		      FROM TCUSTOMM AS CUST															/* 顧客マスタ */
		     WHERE CUST.CUSTOMER_ID         = :customerid									/* 画面入力値 */
		       AND CUST.DATA_REV            = IF(:DataRev1 <> 0, :DataRev2, CUST.DATA_REV)	/* 無ければ条件にしない */
		',
			[
				"customerid"   => $pCustomerid,
				"DataRev1"     => $pDataRev,
				"DataRev2"     => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    getSearchMasterData
	// over view      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programer    k-kagawa
	//**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table of inspection result list

		$lTblSearchResultData = DB::select('
		    SELECT CUST.CUSTOMER_ID
		          ,CUST.CUSTOMER_NAME
		          ,CUST.INSPECTION_SHEET_FORMAT_ID
		          ,TCODE.CODE_NAME
		          ,CUST.DISPLAY_ORDER
		          ,CUST.DELETE_FLG
		          ,CUST.DATA_REV
		      FROM TCUSTOMM AS CUST
		    INNER JOIN TCODEMST AS TCODE
			   ON CUST.INSPECTION_SHEET_FORMAT_ID = TCODE.CODE_ORDER
			  AND TCODE.CODE_CLASS          = "005"
		     WHERE CUST.DELETE_FLG          = "0"                                   /* deleteフラグ */
		       AND CUST.CUSTOMER_ID         LIKE :customerid                        /* 顧客ID(前方一致) */
		       AND CUST.CUSTOMER_NAME       LIKE :customername                      /* 顧客名(部分一致) */
		  ORDER BY CUST.DISPLAY_ORDER
		          ,CUST.CUSTOMER_ID
		',
			[
				"customerid"      => TRIM((String)Input::get('txtCustomerIDForSearch'))."%",
				"customername"    => "%".TRIM((String)Input::get('txtCustomerNameForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    insertMasterData
	// over view      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lCustomerID = Input::get('txtCustomerIDForEntry');
		$lCustomerName = Input::get('txtCustomerNameForEntry');
    // date            04/01/2018
    // author          Mai:) 		
		$lInspectionSheetFormatID = Input::get('cmbInspectionSheetFormatForEntry');
		$lDisplayOrder = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			function() use	(
							  $lCustomerID
							 ,$lCustomerName
	// date            04/01/2018
    // author          Mai:) 
							 ,$lInspectionSheetFormatID
	//
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::insert
				('
						INSERT INTO TCUSTOMM
						(
						  CUSTOMER_ID
						 ,CUSTOMER_NAME
	--  // date            04/01/2018
    --  // author          Mai:) 
						 ,INSPECTION_SHEET_FORMAT_ID
	-- 
						 ,DISPLAY_ORDER
						 ,DELETE_FLG
						 ,DATA_REV
						)
						VALUES (
						         :Customerid
						        ,:CustomerName
	-- // date            04/01/2018
    -- // author          Mai:)							        
						        ,:InspectionSheetFormatid
	-- 
						        ,:DisplayOrder
						        ,"0"
						        ,1
						       )
				', 
					[
						"Customerid"	=> $lCustomerID,
						"CustomerName"	=> $lCustomerName,
	// date            04/01/2018
    // author          Mai:)						
						"InspectionSheetFormatid" => $lInspectionSheetFormatID,
	//					
						"DisplayOrder"	=> $lDisplayOrder,
					]
				);
				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	    
	}

	//**************************************************************************
	// processing name    updateMasterData
	// over view      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	//**************************************************************************
	private function updateMasterData
	(
	  $pCustomerid
	 ,$pCustomerName 
	// date            04/01/2018
	// author          Mai:) 
	 ,$pInspectionSheetid
	// 
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{
		//更新の成否をセット
		$lKohshinCnt = "";

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			function() use (
							 $pCustomerid
							,$pCustomerName
	// date            04/01/2018
	// author          Mai:) 							
	 						,$pInspectionSheetid
	// 
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				$lKohshinCnt = DB::update
				('
						UPDATE TCUSTOMM
						   SET  CUSTOMER_ID		= :Customerid
						       ,CUSTOMER_NAME	= :CustomerName 
	-- // date            04/01/2018
	-- // author          Mai:) 						       
						       ,INSPECTION_SHEET_FORMAT_ID   = :InspectionSheetid
	-- 
						       ,DISPLAY_ORDER	= :DisplayOrder
						       ,DELETE_FLG		= :DeleteFlg
						       ,DATA_REV		= DATA_REV + 1
						 WHERE CUSTOMER_ID	= :CustomeridWhere
				', 
					[
						"Customerid"		=> $pCustomerid,
						"CustomeridWhere"	=> $pCustomerid,
						"CustomerName"		=> $pCustomerName,
	// date            04/01/2018
	// author          Mai:)						
						"InspectionSheetid" => $pInspectionSheetid,
	//					
						"DisplayOrder"		=> $pDisplayOrder,
						"DeleteFlg"			=> $pDeleteFlg,
					]
				);
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	    
	}

	//**************************************************************************
	// processing name    keepFromInputValue
	// over view      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
		//■登録用・Customer IDの項目保持
		if (Input::has('txtCustomerIDForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2040CustomerIDForEntry', Input::get('txtCustomerIDForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2040CustomerIDForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"CustomerIDForEntry"  => Session::get('ZA2040CustomerIDForEntry')
		];
		
		//■登録用・Customer Nameの項目保持
		if (Input::has('txtCustomerNameForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2040CustomerNameForEntry', Input::get('txtCustomerNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2040CustomerNameForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"CustomerNameForEntry"  => Session::get('ZA2040CustomerNameForEntry')
		];
		
	// date            04/01/2018
	// author          Mai:) 
		if (Input::has('cmbInspectionSheetFormatForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2040InspectionSheetFormatForEntry', Input::get('cmbInspectionSheetFormatForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2040InspectionSheetFormatForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"InspectionSheetFormatForEntry"  => Session::get('ZA2040InspectionSheetFormatForEntry')
		];

		//■登録用・Display Orderの項目保持
		if (Input::has('txtDisplayOrderForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2040DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			//画面の値が無ければサーバ日付をwrite down in session。
			Session::put('ZA2040DisplayOrderForEntry', "");
		}

		//セッションには画面値か業務日付が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2040DisplayOrderForEntry')
		];


		//■検索用・Customer IDの項目保持
		//in case value does not exist in session
		if (Input::has('txtCustomerIDForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2040CustomerNoForSearch', Input::get('txtCustomerIDForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2040CustomerNoForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"CustomerNoForSearch"  => Session::get('ZA2040CustomerNoForSearch')
		];
		
		//■検索用・Customer Nameの項目保持
		//in case value does not exist in session
		if (Input::has('txtCustomerNameForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2040CustomerNameForSearch', Input::get('txtCustomerNameForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2040CustomerNameForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"CustomerNameForSearch"  => Session::get('ZA2040CustomerNameForSearch')
		];


		return $pViewData;
	}

	//**************************************************************************
	// processing name    initializeSessionData
	// over view      セッションデータのクリア（検索時・編集時の値）
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2040SearchResultData');

		//Entry button時に設定しているものをクリア
		Session::forget('ZA2040CustomerIDForEntry');
		Session::forget('ZA2040CustomerNameForEntry');
	// date            04/01/2018
	// author          Mai:) 
		Session::forget('ZA2040InspectionSheetFormatForEntry');
		Session::forget('ZA2040DisplayOrderForEntry');

		return null;
	}

	//**************************************************************************
	// processing name    isErrorForRegist
	// over view      登録時に実施する必須・型式チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須チェック
		//-------------------------
		//Customer ID必須チェック
		$lValidator = Validator::make(
			array('txtCustomerIDForEntry' => Input::get('txtCustomerIDForEntry')),
			array('txtCustomerIDForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Customer ID ."
			]);
			return $pViewData;
		}

		//Customer 名必須チェック
		$lValidator = Validator::make(
			array('txtCustomerNameForEntry' => Input::get('txtCustomerNameForEntry')),
			array('txtCustomerNameForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Customer Name."
			]);
			return $pViewData;
		}

	// date            04/01/2018
	// author          Mai:) 
		$lValidator = Validator::make(
			array('cmbInspectionSheetFormatForEntry' => Input::get('cmbInspectionSheetFormatForEntry')),
			array('cmbInspectionSheetFormatForEntry' => array('required'))
		);
		
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Inspection Sheet Format."
			]);
			
			return $pViewData;
		}
    // 

		//表示順必須チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}

		//-------------------------
		//型式チェック
		//-------------------------
		//Customer ID型チェック
		$lValidator = Validator::make(
			array('txtCustomerIDForEntry' => Input::get('txtCustomerIDForEntry')),
			array('txtCustomerIDForEntry' => array('alpha_dash'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Customer ID is invalid."
			]);
			return $pViewData;
		}

	// date            04/01/2018
	// author          Mai:) 
		//$lValidator = Validator::make(
			//array('cmbInspectionSheetFormatForEntry' => Input::get('cmbInspectionSheetFormatForEntry')),
			//array('cmbInspectionSheetFormatForEntry' => array('alpha_dash'))
		//);
		
		//if ($lValidator->fails())
		//{
			//set error message
			//$pViewData["errors"] = new MessageBag([
				//"error" => "E994 : Inspection Sheet Format is invalid."
			//]);
			
			//return $pViewData;
		//}
    // 

		//表示順型チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}


	//**************************************************************************
	// process         setInspectionSheetFormat
	// overview        
	// argument        
	// return value    
	// date            04/01/2018
	// author          Mai:)        
	//**************************************************************************
	private function setInspectionSheetFormat($pViewData)
	{
		$lArr    = ["" => ""];  //view data to return

		$lArr = $this->getInspectionSheetFormat();

		//store in session
		Session::put('ZA2020arrDataInspectionSheetFormat', $lArr); 

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataInspectionSheetFormat" => $lArr
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getInspectionSheetFormat
	// overview        
	// argument        
	// return value    
	// date            04/01/2018
	// author          Mai:)   
	//**************************************************************************
	private function getInspectionSheetFormat()
	{
		$lTbl			= []; 
		$lRow			= []; 
		$lArrData		= []; 
		$lTbl = DB::select(
		'
			SELECT PROCESS.CODE_ORDER
			     , PROCESS.CODE_NAME
			     , PROCESS.DISPLAY_ORDER
			  FROM TCODEMST AS PROCESS
			 WHERE CODE_CLASS = "005"
			 ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTbl = (array)$lTbl;

		//add blank space
		$lArrData += [
			"" => ""
		];

		//data exist
		if ($lTbl != null)
		{
			//store result in Array again
			foreach ($lTbl as $lRow)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRow = (Array)$lRow;

				$lArrData += [
					$lRow["CODE_ORDER"] => $lRow["CODE_NAME"]
				];
			}
		}

		return $lArrData;
	}

}