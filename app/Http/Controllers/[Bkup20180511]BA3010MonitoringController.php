<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use DB;
use Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// display:	 Monitoring
// overview: For Plasess Company
// author:	Mai Nawaphat(Mai ^^)
// date:	21/2/2018
//**************************************************************************
class BA3010MonitoringController
extends Controller
{

	//-------------
	//■■定数定義
	CONST NUMBER_KETA = 10;               //2桁にする
	CONST NUMBER_TIME24 = 24;             //マスタ時間に合わせる
	CONST MASTER_TIME_A = 23;             //マスタの時間IDが0～23
	CONST MASTER_TIME_B = 24;             //マスタの時間IDが1～24
	//data count per 1 page
	CONST NUMBER_PER_PAGE = 10;

	public function MasterAction()
	{   
		$lViewData					= []; //画面へのデータ移送用
		$lTblCheckTimeResultData	= []; //INSPECTION TIME IDのMAX値のデータテーブル
		$lTblHeaderResultData		= []; //一覧ヘッダーのデータテーブル
		$lTblSearchResultData		= []; //一覧明細のデータテーブル
		$lPagenation				= []; //ページング用の仕組み（画面にはこれを返却する）
		$TimeIDMax					= []; //INSPECTION TIME IDのMAX値をとる
		$WorkErrorJudgmentF = ""; //サーバー時間取得時エラー判定用

		$ID24					= "";
		
		$DAY24					= "";

		$Name1					= "";
		$Name2					= "";
		$Name3					= "";
		$Name4					= "";
		$Name5					= "";
		$Name6					= "";
		$Name7					= "";
		$Name8					= "";
		$Name9					= "";
		$Name10					= "";
		$Name11					= "";
		$Name12					= "";
		$Name13					= "";
		$Name14					= "";
		$Name15					= "";
		$Name16					= "";
		$Name17					= "";
		$Name18					= "";
		$Name19					= "";
		$Name20					= "";
		$Name21					= "";
		$Name22					= "";
		$Name23					= "";
		$Name24					= "";
		
		//AM12時のID
		$MID					= "";
		
		//Session経由でログイン画面からのパラメータを受け取り、画面への移送用配列に投入
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		//サーバ時刻取得
		$time = new Carbon(Carbon::now());
			
		//取得した時刻を時間表記する
		$time = $time->hour;
			
		//日付取得
		$today = date("Y/m/d");
		$yesterday = date('Y/m/d', strtotime('-1 day'));
			
		//INSPECTION TIME IDのMAX値をとる

		$lTblCheckTimeData = $this->getCheckTimeData();
			$TimeIDMax = (array)$lTblCheckTimeData[0];
		if($TimeIDMax["MAX(INSPECTION_TIME_ID)"] != NULL){
			
			//INSPECTION TIME IDのMAX値が23のとき
			if ($TimeIDMax["MAX(INSPECTION_TIME_ID)"] == self::MASTER_TIME_A)
			{	
				for($i = 0; $i < 23; $i++){ 
	                $ID[] = $this->getID1($time, self::MASTER_TIME_A - $i);
	            }
	            foreach ($ID as $key => $value) {
	            	$DAY[] = $this->getDAY1($value ,$today, $yesterday);
	            }
					
				$ID24 = $time;
				$DAY24 = $today;
				if ($ID24 < self::NUMBER_KETA)
				{
					$ID24 = sprintf('%02d', $ID24);
				}
					
				//AM12時のID
				$MID = 00;
			}
			//INSPECTION TIME IDのMAX値が24のとき
			else if ($TimeIDMax["MAX(INSPECTION_TIME_ID)"] == self::MASTER_TIME_B)
			{  
	            for($i = 0; $i < 23; $i++){ 
	                $ID[] = $this->getID2($time, self::MASTER_TIME_A - $i);
	            }
	            foreach ($ID as $key => $value) {
	            	$DAY[] = $this->getDAY2($value ,$today, $yesterday);
	            }

				$ID24 = $time;
				$DAY24 = $today;
				if ($ID24 == 0)
				{
					$ID24 = $ID24 + self::NUMBER_TIME24;
				}
				if ($ID24 < self::NUMBER_KETA)
				{
					$ID24 = sprintf('%02d', $ID24);
				}
					
				//AM12時のID
				$MID = 24;
					
				}
				//マスタの時間IDが異常の場合
				else
				{
					$WorkErrorJudgmentF = "1";
				}
				dd($ID);
				//各時間をもとに検査時間・回数マスタの検査時間・回数名を取得する
				$lTblHeadData = $this->getHeaderData($ID[0],$ID[1],$ID[2],$ID[3],$ID[4],$ID[5],$ID[6],$ID[7],$ID[8],$ID[9],$ID[10],$ID[11],$ID[12],$ID[13],$ID[14],$ID[15],$ID[16],$ID[17],$ID[18],$ID[19],$ID[20],$ID[21],$ID[22],$ID24);

				foreach ($lTblHeadData as $lCurrentRow)
					{
						//該当行を配列に変換
						$lArrDataRow = (Array)$lCurrentRow;

						//表時刻1列目から順に検査時間・回数名を入れる
				
						if($ID[0] == (String)$lArrDataRow["INSPECTION_TIME_ID"])
						{
							$Name1 = (String)$lArrDataRow["INSPECTION_TIME_NAME"];
						}
						
						if($ID[1] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name2 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[2] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name3 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[3] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name4 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[4] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name5 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[5] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name6 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[6] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name7 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[7] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name8 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[8] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name9 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[9] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name10 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[10] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name11 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[11] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name12 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						if($ID[12] == (String)$lArrDataRow["INSPECTION_TIME_ID"])
						{
							$Name13 = (String)$lArrDataRow["INSPECTION_TIME_NAME"];
						}
						if($ID[13] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name14 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[14] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name15 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[15] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name16 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[16] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name17 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[17] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name18 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[18] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name19 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[19] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name20 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[20] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name21 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[21] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name22 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID[22] == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name23 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
						
						if($ID24 == TRIM((String)$lArrDataRow["INSPECTION_TIME_ID"]))
						{
							$Name24 = TRIM((String)$lArrDataRow["INSPECTION_TIME_NAME"]);
						}
					}
					
					$lViewData += [
						"Name1"  => $Name1,
						"Name2"  => $Name2,
						"Name3"  => $Name3,
						"Name4"  => $Name4,
						"Name5"  => $Name5,
						"Name6"  => $Name6,
						"Name7"  => $Name7,
						"Name8"  => $Name8,
						"Name9"  => $Name9,
						"Name10"  => $Name10,
						"Name11"  => $Name11,
						"Name12"  => $Name12,
						"Name13"  => $Name13,
						"Name14"  => $Name14,
						"Name15"  => $Name15,
						"Name16"  => $Name16,
						"Name17"  => $Name17,
						"Name18"  => $Name18,
						"Name19"  => $Name19,
						"Name20"  => $Name20,
						"Name21"  => $Name21,
						"Name22"  => $Name22,
						"Name23"  => $Name23,
						"Name24"  => $Name24
					]; 

					//明細部を取得する
					$lTblDetailData = $this->getDetailData($ID[0],$ID[1],$ID[2],$ID[3],$ID[4],$ID[5],$ID[6],$ID[7],$ID[8],$ID[9],$ID[10],$ID[11],$ID[12],$ID[13],$ID[14],$ID[15],$ID[16],$ID[17],$ID[18],$ID[19],$ID[20],$ID[21],$ID[22],$ID24,$DAY[0],$DAY[1],$DAY[2],$DAY[3],$DAY[4],$DAY[5],$DAY[6],$DAY[7],$DAY[8],$DAY[9],$DAY[10],$DAY[11],$DAY[12],$DAY[13],$DAY[14],$DAY[15],$DAY[16],$DAY[17],$DAY[18],$DAY[19],$DAY[20],$DAY[21],$DAY[22],$DAY24,$today,$yesterday,$MID);

					if(isset($lTblDetailData)){
						// $lPagenation = Paginator::make($lTblDetailData,Count($lTblDetailData));
						$lPagenation = new LengthAwarePaginator ($lTblDetailData,Count($lTblDetailData),self::NUMBER_PER_PAGE);
					}else{
					    $lPagenation = null;
					}

					$lViewData += [
						"Pagenator"       => $lPagenation,
						"ID24"            => $ID24,
						"ID23"            => $ID[22],
						"ID22"            => $ID[21],
						"ID21"            => $ID[20],
						"ID20"            => $ID[19],
						"ID19"            => $ID[18],
						"ID18"            => $ID[17],
						"ID17"            => $ID[16],
						"ID16"            => $ID[15],
						"ID15"            => $ID[14],
						"ID14"            => $ID[13],
						"ID13"            => $ID[12],
						"ID12"            => $ID[11],
						"ID11"            => $ID[10],
						"ID10"            => $ID[9],
						"ID09"            => $ID[8],
						"ID08"            => $ID[7],
						"ID07"            => $ID[6],
						"ID06"            => $ID[5],
						"ID05"            => $ID[4],
						"ID04"            => $ID[3],
						"ID03"            => $ID[2],
						"ID02"            => $ID[1],
						"ID01"            => $ID[0],
					];

			if ($WorkErrorJudgmentF == "1")
			{
				$lViewData["errors"] = new MessageBag([
					"error" => "E999 : System error has occurred. Contact your system manager."
					]);
			}
        }else{
        	$lViewData["errors"] = new MessageBag([
					"error" => "E997 : Target data does not exist."
					]);
        }    
		// return View::make("user/monitoring", $lViewData);
		return View("user.monitoring", $lViewData);

	}

	//**************************************************************************
	// process         getCheckTimeData
	// overview        select master data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckTimeData()
	{
		$lTblCheckTimeResultData          = [];

		$lTblCheckTimeResultData = DB::select
		('
			SELECT MAX(INSPECTION_TIME_ID)
			  FROM TINSPTIM
			 WHERE DELETE_FLG = "0"
		',
			[
			]
		);
		
		return $lTblCheckTimeResultData;
	}

	//**************************************************************************
	// 処理名    getHeaderData
	// 概要      一覧ヘッダー部の時間を取得する
	// 引数      無し
	// 戻り値    Array
	// 作成者    m-motooka
	//**************************************************************************
	private function getHeaderData
	(
	  $pID1
	 ,$pID2
	 ,$pID3
	 ,$pID4	
	 ,$pID5
	 ,$pID6
	 ,$pID7	
	 ,$pID8
	 ,$pID9
	 ,$pID10
	 ,$pID11
	 ,$pID12
	 ,$pID13
	 ,$pID14
	 ,$pID15
	 ,$pID16
	 ,$pID17
	 ,$pID18
	 ,$pID19
	 ,$pID20
	 ,$pID21
	 ,$pID22
	 ,$pID23
	 ,$pID24
	)
	{
		$lTblHeaderResultData          = []; //検査結果一覧ヘッダーのデータテーブル

		$lTblHeaderResultData = DB::select
		('
		SELECT INSPECTION_TIME_ID
		,INSPECTION_TIME_NAME
		FROM TINSPTIM
		WHERE (	  INSPECTION_TIME_ID = :ID1
		       OR INSPECTION_TIME_ID = :ID2 
		       OR INSPECTION_TIME_ID = :ID3 
		       OR INSPECTION_TIME_ID = :ID4 
		       OR INSPECTION_TIME_ID = :ID5 
		       OR INSPECTION_TIME_ID = :ID6 
		       OR INSPECTION_TIME_ID = :ID7 
		       OR INSPECTION_TIME_ID = :ID8 
		       OR INSPECTION_TIME_ID = :ID9 
		       OR INSPECTION_TIME_ID = :ID10 
		       OR INSPECTION_TIME_ID = :ID11 
		       OR INSPECTION_TIME_ID = :ID12
		       OR INSPECTION_TIME_ID = :ID13
		       OR INSPECTION_TIME_ID = :ID14
		       OR INSPECTION_TIME_ID = :ID15
		       OR INSPECTION_TIME_ID = :ID16
		       OR INSPECTION_TIME_ID = :ID17
		       OR INSPECTION_TIME_ID = :ID18
		       OR INSPECTION_TIME_ID = :ID19
		       OR INSPECTION_TIME_ID = :ID20
		       OR INSPECTION_TIME_ID = :ID21
		       OR INSPECTION_TIME_ID = :ID22 
		       OR INSPECTION_TIME_ID = :ID23 
		       OR INSPECTION_TIME_ID = :ID24)
		',
			[	"ID1"     => $pID1,
				"ID2"     => $pID2,
				"ID3"     => $pID3,
				"ID4"     => $pID4,
				"ID5"     => $pID5,
				"ID6"     => $pID6,
				"ID7"     => $pID7,
				"ID8"     => $pID8,
				"ID9"     => $pID9,
				"ID10"     => $pID10,
				"ID11"     => $pID11,
				"ID12"     => $pID12,
				"ID13"     => $pID13,
				"ID14"     => $pID14,
				"ID15"     => $pID15,
				"ID16"     => $pID16,
				"ID17"     => $pID17,
				"ID18"     => $pID18,
				"ID19"     => $pID19,
				"ID20"     => $pID20,
				"ID21"     => $pID21,
				"ID22"     => $pID22,
				"ID23"     => $pID23,
				"ID24"     => $pID24,
			]
		);
		return $lTblHeaderResultData;
	}
	
	//**************************************************************************
	// display:	 getDetailData
	// overview: For Plasess Company
	// author:	Mai Nawaphat(Mai ^^)
	// date:	21/2/2018
	//**************************************************************************
	private function getDetailData( $pID1,$pID2,$pID3,$pID4,$pID5,$pID6,$pID7,$pID8,$pID9,$pID10,$pID11,$pID12,$pID13,$pID14,$pID15,$pID16,$pID17,$pID18,$pID19,$pID20,$pID21,$pID22,$pID23,$pID24,$pDAY01,$pDAY02,$pDAY03,$pDAY04,$pDAY05,$pDAY06,$pDAY07,$pDAY08,$pDAY09,$pDAY10,$pDAY11,$pDAY12,$pDAY13,$pDAY14,$pDAY15,$pDAY16,$pDAY17,$pDAY18,$pDAY19,$pDAY20,$pDAY21,$pDAY22,$pDAY23,$pDAY24,$ptoday,$pyesterday,$pMID){	 

		$lTblSearchResultData          = [];    
		$lTblSearchResultData = DB::select('SELECT 
        	CUSTOM.CUSTOMER_NAME
		   ,MCN.MACHINE_NAME AS MACHINE_NAME
		,CONCAT(CUSTOM.CUSTOMER_NAME, MST.ITEM_NO, MST.ITEM_NAME,MCN.MACHINE_NAME) AS RENKETSU
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU1 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU1 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU1 = YOJITU.KENSA_JISSEKI_SU1 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI1
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU1 = "0" THEN " "
		      WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU1
		      ELSE " "
		  END AS ERROR_SU1
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU2 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU2 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU2 = YOJITU.KENSA_JISSEKI_SU2 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI2
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU2 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU2
		      ELSE " "
		  END AS ERROR_SU2
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU3 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU3 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU3 = YOJITU.KENSA_JISSEKI_SU3 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI3
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU3 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU3
		      ELSE " "
		  END AS ERROR_SU3
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU4 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU4 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU4 = YOJITU.KENSA_JISSEKI_SU4 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI4
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU4 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU4
		      ELSE " "
		  END AS ERROR_SU4
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU5 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU5 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU5 = YOJITU.KENSA_JISSEKI_SU5 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI5
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU5 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU5
		      ELSE " "
		  END AS ERROR_SU5
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU6 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU6 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU6 = YOJITU.KENSA_JISSEKI_SU6 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI6
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU6 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU6
		      ELSE " "
		  END AS ERROR_SU6
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU7 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU7 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU7 = YOJITU.KENSA_JISSEKI_SU7 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI7
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU7 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU7
		      ELSE " "
		  END AS ERROR_SU7
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU8 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU8 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU8 = YOJITU.KENSA_JISSEKI_SU8 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI8
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU8 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU8
		      ELSE " "
		  END AS ERROR_SU8
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU9 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU9 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU9 = YOJITU.KENSA_JISSEKI_SU9 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI9
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU9 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU9
		      ELSE " "
		  END AS ERROR_SU9
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU10 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU10 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU10 = YOJITU.KENSA_JISSEKI_SU10 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI10
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU10 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU10
		      ELSE " "
		  END AS ERROR_SU10
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU11 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU11 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU11 = YOJITU.KENSA_JISSEKI_SU11 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI11
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU11 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU11
		      ELSE " "
		  END AS ERROR_SU11
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU12 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU12 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU12 = YOJITU.KENSA_JISSEKI_SU12 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI12
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU12 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU12
		      ELSE " "
		  END AS ERROR_SU12
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU13 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU13 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU13 = YOJITU.KENSA_JISSEKI_SU13 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI13
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU13 = "0" THEN " "
		      WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU13
		      ELSE " "
		  END AS ERROR_SU13
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU14 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU14 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU14 = YOJITU.KENSA_JISSEKI_SU14 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI14
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU14 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU14
		      ELSE " "
		  END AS ERROR_SU14
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU15 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU15 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU15 = YOJITU.KENSA_JISSEKI_SU15 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI15
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU15 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU15
		      ELSE " "
		  END AS ERROR_SU15
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU16 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU16 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU16 = YOJITU.KENSA_JISSEKI_SU16 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI16
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU16 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU16
		      ELSE " "
		  END AS ERROR_SU16
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU17 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU17 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU17 = YOJITU.KENSA_JISSEKI_SU17 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI17
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU17 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU17
		      ELSE " "
		  END AS ERROR_SU17
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU18 = "0" THEN " "
			       		WHEN YOJITU.KENSA_JISSEKI_SU18 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU18 = YOJITU.KENSA_JISSEKI_SU18 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI18
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU18 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU18
		      ELSE " "
		  END AS ERROR_SU18
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU19 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU19 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU19 = YOJITU.KENSA_JISSEKI_SU19 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI19
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU19 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU19
		      ELSE " "
		  END AS ERROR_SU19
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU20 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU20 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU20 = YOJITU.KENSA_JISSEKI_SU20 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI20
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU20 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU20
		      ELSE " "
		  END AS ERROR_SU20
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU21 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU21 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU21 = YOJITU.KENSA_JISSEKI_SU21 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI21
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU21 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU21
		      ELSE " "
		  END AS ERROR_SU21
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU22 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU22 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU22 = YOJITU.KENSA_JISSEKI_SU22 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI22
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU22 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU22
		      ELSE " "
		  END AS ERROR_SU22
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU23 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU23 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU23 = YOJITU.KENSA_JISSEKI_SU23 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI23
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU23 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU23
		      ELSE " "
		  END AS ERROR_SU23
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN
			       CASE WHEN YOJITU.YOTEI_SU24 = "0" THEN " "
			            WHEN YOJITU.KENSA_JISSEKI_SU24 = "0" THEN "△"
			            WHEN YOJITU.YOTEI_SU24 = YOJITU.KENSA_JISSEKI_SU24 THEN "●"
			            ELSE "※"
			         END
		      ELSE " "
		  END AS JOTAI24
		,CASE WHEN YOJITU.INSPECTION_SHEET_NO <> "" AND YOJITU.YOTEI_SU24 = "0" THEN " "
			  WHEN YOJITU.INSPECTION_SHEET_NO <> "" THEN YOJITU.ERROR_SU24
		      ELSE " "
		  END AS ERROR_SU24
		,CASE WHEN :TIME1ID1 in (08,09,10,11,12,13,14,15,16,17,18,19) THEN YOJITU.DAY_ERROR_SU
			  ELSE YOJITU.NIGHT_ERROR_SU
		 END AS NOW_ERROR_SU
		,CASE WHEN :TIME2ID1 in (08,09,10,11,12,13,14,15,16,17,18,19) THEN YOJITU.NIGHT_ERROR_SU
			  ELSE YOJITU.DAY_ERROR_SU
		 END AS LAST_ERROR_SU 
		,YOJITU.LOT_NO  
		,YOJITU.INSPECTION_SHEET_NO
		,MST.INSPECTION_SHEET_NAME
		FROM
			(
				SELECT MAX(SHEET.ITEM_NO) AS ITEM_NO
				        ,MAX(TITEM.ITEM_NAME) AS ITEM_NAME
				        ,SHEET.INSPECTION_SHEET_NO
				        ,MAX(SHEET.REV_NO) AS REV_NO
				        ,SHEET.CUSTOMER_ID
				        ,SHEET.PROCESS_ID
				        ,SHEET.INSPECTION_SHEET_NAME
				    FROM TISHEETM AS SHEET
				    INNER JOIN TITEMMST AS TITEM
							ON SHEET.ITEM_NO = TITEM.ITEM_NO
				   WHERE SHEET.DELETE_FLG = "0"
				GROUP BY SHEET.PROCESS_ID
				        ,SHEET.INSPECTION_SHEET_NO
			) MST
		LEFT OUTER JOIN
			(
				  SELECT CUSTOMER_NAME
				        ,CUSTOMER_ID
				    FROM TCUSTOMM
			) CUSTOM
		ON MST.CUSTOMER_ID = CUSTOM.CUSTOMER_ID

		LEFT OUTER JOIN
			(
        					SELECT SUB.INSPECTION_SHEET_NO
							,SUB.REV_NO
							,SUB.PROCESS_ID
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID1 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU1	/*1列目の検査予定数*/
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID1 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU1	/*1列目の検査実績数*/
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID1 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU1	/*1列目のエラー数*/
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID2 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU2						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID2 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU2		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID2 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU2					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID3 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU3						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID3 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU3		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID3 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU3					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID4 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU4						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID4 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU4		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID4 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU4					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID5 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU5						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID5 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU5		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID5 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU5					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID6 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU6						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID6 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU6		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID6 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU6					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID7 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU7						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID7 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU7		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID7 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU7					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID8 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU8						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID8 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU8		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID8 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU8					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID9 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU9						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID9 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU9		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID9 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU9					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID10 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU10						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID10 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU10		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID10 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU10					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID11 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU11						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID11 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU11		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID11 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU11					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID12 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU12						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID12 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU12		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID12 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU12					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID13 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU13

							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID13 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU13	
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID13 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU13	

							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID14 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU14						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID14 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU14		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID14 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU14					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID15 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU15						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID15 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU15		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID15 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU15					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID16 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU16						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID16 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU16		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID16 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU16					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID17 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU17						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID17 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU17		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID17 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU17					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID18 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU18						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID18 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU18		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID18 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU18					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID19 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU19						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID19 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU19		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID19 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU19					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID20 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU20						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID20 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU20		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID20 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU20					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID21 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU21						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID21 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU21		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID21 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU21					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID22 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU22						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID22 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU22		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID22 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU22					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID23 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU23						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID23 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU23		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID23 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU23					
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :YID24 THEN SUB.YOTEI_SU ELSE 0 END) AS YOTEI_SU24						
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :JID24 THEN SUB.KENSA_JISSEKI_SU ELSE 0 END) AS KENSA_JISSEKI_SU24		
							,SUM(CASE WHEN SUB.INSPECTION_TIME_ID = :EID24 THEN SUB.ERROR_SU ELSE 0 END) AS ERROR_SU24					
							,SUB.MACHINE_NO     AS MACHINE_NO	/*予定と実績を完全結合したものから実績のマシンNoを優先させる*/
							,SUB.INSPECTION_TIME_ID
							,CASE WHEN :TIME3ID1 in(20,21,22,23) THEN
										SUM(CASE WHEN(SUB.INSPECTION_TIME_ID in(20,21,22,23) AND SUB.INSPECTION_YMD = :TODAY2)
													THEN SUB.ERROR_SU ELSE 0 END)
								  ELSE	SUM(CASE WHEN (SUB.INSPECTION_TIME_ID in(07,06,05,04,03,02,01,:MID1) AND SUB.INSPECTION_YMD = :TODAY1 )
													OR (SUB.INSPECTION_TIME_ID in(23,22,21,20) AND SUB.INSPECTION_YMD = :YEST1)
													THEN SUB.ERROR_SU ELSE 0 END)
								END AS NIGHT_ERROR_SU
							,CASE WHEN :TIME4ID1 in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23) THEN
										SUM(CASE WHEN SUB.INSPECTION_TIME_ID in (08,09,10,11,12,13,14,15,16,17,18,19)AND SUB.INSPECTION_YMD = :TODAY3 
													THEN SUB.ERROR_SU ELSE 0 END)
								  ELSE
								  		SUM(CASE WHEN SUB.INSPECTION_TIME_ID in (08,09,10,11,12,13,14,15,16,17,18,19)AND SUB.INSPECTION_YMD = :YEST2
								  					THEN SUB.ERROR_SU ELSE 0 END)
								END AS DAY_ERROR_SU
							,SUB.LOT_NO	
					FROM
							(
        									SELECT 		 TRH.PROCESS_ID
									          ,TRH.REV_NO
									          ,TRH.INSPECTION_SHEET_NO
									          ,TRH.INSPECTION_TIME_ID
									          ,0 AS YOTEI_SU
									          ,SUM(CASE WHEN TRH.INSPECTION_RESULT_NO = null THEN "0" ELSE "1" END) AS KENSA_JISSEKI_SU
									      --     /*NG,OFFSETかつLINE STOPをエラーとする*/
									      	  ,SUM(CASE WHEN TRT.INSPECTION_RESULT_TYPE in ("2","3") AND TRT.OFFSET_NG_ACTION = "1" THEN "1" ELSE "0" END) AS ERROR_SU
									          -- ,SUM(CASE WHEN TRT.INSPECTION_RESULT_TYPE in ("2","3") AND TRT.OFFSET_NG_ACTION = "2" THEN "1" ELSE "0" END) AS ERROR_SU
									          ,TRH.MACHINE_NO AS MACHINE_NO
									          ,TRH.INSPECTION_YMD
									          ,TRH.LOT_NO
									      FROM TRESHEDT AS TRH	/*検査実績ヘッダ*/
						 			INNER JOIN TRESDETT AS TRT	/*検査実績明細*/
									        ON TRH.INSPECTION_RESULT_NO = TRT.INSPECTION_RESULT_NO
									     WHERE (
													(TRH.INSPECTION_YMD = :DAY01 AND TRH.INSPECTION_TIME_ID = :IDJ1)
												OR	(TRH.INSPECTION_YMD = :DAY02 AND TRH.INSPECTION_TIME_ID = :IDJ2)
												OR	(TRH.INSPECTION_YMD = :DAY03 AND TRH.INSPECTION_TIME_ID = :IDJ3)
												OR	(TRH.INSPECTION_YMD = :DAY04 AND TRH.INSPECTION_TIME_ID = :IDJ4)
												OR	(TRH.INSPECTION_YMD = :DAY05 AND TRH.INSPECTION_TIME_ID = :IDJ5)
												OR	(TRH.INSPECTION_YMD = :DAY06 AND TRH.INSPECTION_TIME_ID = :IDJ6)
												OR	(TRH.INSPECTION_YMD = :DAY07 AND TRH.INSPECTION_TIME_ID = :IDJ7)
												OR	(TRH.INSPECTION_YMD = :DAY08 AND TRH.INSPECTION_TIME_ID = :IDJ8)
												OR	(TRH.INSPECTION_YMD = :DAY09 AND TRH.INSPECTION_TIME_ID = :IDJ9)
												OR	(TRH.INSPECTION_YMD = :DAY10 AND TRH.INSPECTION_TIME_ID = :IDJ10)
												OR	(TRH.INSPECTION_YMD = :DAY11 AND TRH.INSPECTION_TIME_ID = :IDJ11)
												OR	(TRH.INSPECTION_YMD = :DAY12 AND TRH.INSPECTION_TIME_ID = :IDJ12)
												OR	(TRH.INSPECTION_YMD = :DAY13 AND TRH.INSPECTION_TIME_ID = :IDJ13)
												OR	(TRH.INSPECTION_YMD = :DAY14 AND TRH.INSPECTION_TIME_ID = :IDJ14)
												OR	(TRH.INSPECTION_YMD = :DAY15 AND TRH.INSPECTION_TIME_ID = :IDJ15)
												OR	(TRH.INSPECTION_YMD = :DAY16 AND TRH.INSPECTION_TIME_ID = :IDJ16)
												OR	(TRH.INSPECTION_YMD = :DAY17 AND TRH.INSPECTION_TIME_ID = :IDJ17)
												OR	(TRH.INSPECTION_YMD = :DAY18 AND TRH.INSPECTION_TIME_ID = :IDJ18)
												OR	(TRH.INSPECTION_YMD = :DAY19 AND TRH.INSPECTION_TIME_ID = :IDJ19)
												OR	(TRH.INSPECTION_YMD = :DAY20 AND TRH.INSPECTION_TIME_ID = :IDJ20)
												OR	(TRH.INSPECTION_YMD = :DAY21 AND TRH.INSPECTION_TIME_ID = :IDJ21)
												OR	(TRH.INSPECTION_YMD = :DAY22 AND TRH.INSPECTION_TIME_ID = :IDJ22)
												OR	(TRH.INSPECTION_YMD = :DAY23 AND TRH.INSPECTION_TIME_ID = :IDJ23)
												OR	(TRH.INSPECTION_YMD = :DAY24 AND TRH.INSPECTION_TIME_ID = :IDJ24)
												)
								      GROUP BY TRH.PROCESS_ID
								              ,TRH.REV_NO
								              ,TRH.INSPECTION_SHEET_NO
								              ,TRH.INSPECTION_TIME_ID
								              ,TRH.MACHINE_NO
						UNION ALL

									SELECT 
        									  TSHEET.PROCESS_ID
									          ,INSNO.REV_NO
									          ,INSNO.INSPECTION_SHEET_NO
									          ,TNM.INSPECTION_TIME_ID
									          ,COUNT(INSNO.INSPECTION_SHEET_NO) AS YOTEI_SU
									          ,0 AS KENSA_JISSEKI_SU
									          ,0 AS ERROR_SU
									          ,COALESCE(TRH.MACHINE_NO,"") AS MACHINE_NO	/*実績があれば実績のマシンNoを取って、繋がっていなければ空文字*/
									          ,TRH.INSPECTION_YMD
									          ,TRH.LOT_NO
									    FROM TINSPNOM AS INSNO

									    INNER JOIN (SELECT SHHET.PROCESS_ID
									                      ,SHHET.INSPECTION_SHEET_NO
									                      ,MAX(SHHET.REV_NO) AS MAX_REV_NO
									                      ,SHHET.INSPECTION_SHEET_NAME
									                  FROM TISHEETM AS SHHET
									                 WHERE SHHET.DELETE_FLG = "0"
									              GROUP BY SHHET.PROCESS_ID
									                      ,SHHET.INSPECTION_SHEET_NO
									                      ,SHHET.REV_NO)TSHEET
									             ON TSHEET.MAX_REV_NO          = INSNO.REV_NO
									            AND TSHEET.INSPECTION_SHEET_NO = INSNO.INSPECTION_SHEET_NO
									    INNER JOIN TINSNTMM AS TNM
									             ON INSNO.INSPECTION_SHEET_NO = TNM.INSPECTION_SHEET_NO
									            AND INSNO.INSPECTION_POINT    = TNM.INSPECTION_POINT 
									            AND TNM.DELETE_FLG            = "0"
									    INNER JOIN TRESANAL AS TSN
									             ON INSNO.INSPECTION_SHEET_NO = TSN.INSPECTION_SHEET_NO
									            AND INSNO.INSPECTION_POINT    = TSN.INSPECTION_POINT
									            AND INSNO.REV_NO    		  = TSN.REV_NO
									            AND TSHEET.PROCESS_ID         = TSN.PROCESS_ID
									            AND TNM.INSPECTION_TIME_ID    = TSN.INSPECTION_TIME_ID
									    LEFT OUTER JOIN TRESHEDT AS TRH /*検査実績ヘッダ*/
									             ON TRH.INSPECTION_SHEET_NO = INSNO.INSPECTION_SHEET_NO
									     		AND TRH.PROCESS_ID          = TSHEET.PROCESS_ID
									     		AND TRH.REV_NO              = INSNO.REV_NO
									            AND TRH.INSPECTION_YMD      = ""
									            AND (
														CASE WHEN TRH.CONDITION_CD = "01" AND TRH.INSPECTION_TIME_ID = TNM.INSPECTION_TIME_ID THEN
														(
															(TRH.INSPECTION_YMD = :DAYY01 AND TRH.INSPECTION_TIME_ID = :IDP1)
														OR	(TRH.INSPECTION_YMD = :DAYY02 AND TRH.INSPECTION_TIME_ID = :IDP2)
														OR	(TRH.INSPECTION_YMD = :DAYY03 AND TRH.INSPECTION_TIME_ID = :IDP3)
														OR	(TRH.INSPECTION_YMD = :DAYY04 AND TRH.INSPECTION_TIME_ID = :IDP4)
														OR	(TRH.INSPECTION_YMD = :DAYY05 AND TRH.INSPECTION_TIME_ID = :IDP5)
														OR	(TRH.INSPECTION_YMD = :DAYY06 AND TRH.INSPECTION_TIME_ID = :IDP6)
														OR	(TRH.INSPECTION_YMD = :DAYY07 AND TRH.INSPECTION_TIME_ID = :IDP7)
														OR	(TRH.INSPECTION_YMD = :DAYY08 AND TRH.INSPECTION_TIME_ID = :IDP8)
														OR	(TRH.INSPECTION_YMD = :DAYY09 AND TRH.INSPECTION_TIME_ID = :IDP9)
														OR	(TRH.INSPECTION_YMD = :DAYY10 AND TRH.INSPECTION_TIME_ID = :IDP10)
														OR	(TRH.INSPECTION_YMD = :DAYY11 AND TRH.INSPECTION_TIME_ID = :IDP11)
														OR	(TRH.INSPECTION_YMD = :DAYY12 AND TRH.INSPECTION_TIME_ID = :IDP12)
														OR	(TRH.INSPECTION_YMD = :DAYY13 AND TRH.INSPECTION_TIME_ID = :IDP13)
														OR	(TRH.INSPECTION_YMD = :DAYY14 AND TRH.INSPECTION_TIME_ID = :IDP14)
														OR	(TRH.INSPECTION_YMD = :DAYY15 AND TRH.INSPECTION_TIME_ID = :IDP15)
														OR	(TRH.INSPECTION_YMD = :DAYY16 AND TRH.INSPECTION_TIME_ID = :IDP16)
														OR	(TRH.INSPECTION_YMD = :DAYY17 AND TRH.INSPECTION_TIME_ID = :IDP17)
														OR	(TRH.INSPECTION_YMD = :DAYY18 AND TRH.INSPECTION_TIME_ID = :IDP18)
														OR	(TRH.INSPECTION_YMD = :DAYY19 AND TRH.INSPECTION_TIME_ID = :IDP19)
														OR	(TRH.INSPECTION_YMD = :DAYY20 AND TRH.INSPECTION_TIME_ID = :IDP20)
														OR	(TRH.INSPECTION_YMD = :DAYY21 AND TRH.INSPECTION_TIME_ID = :IDP21)
														OR	(TRH.INSPECTION_YMD = :DAYY22 AND TRH.INSPECTION_TIME_ID = :IDP22)
														OR	(TRH.INSPECTION_YMD = :DAYY23 AND TRH.INSPECTION_TIME_ID = :IDP23)
														OR	(TRH.INSPECTION_YMD = :DAYY24 AND TRH.INSPECTION_TIME_ID = :IDP24)
														)
														WHEN TRH.CONDITION_CD = "02" AND TNM.INSPECTION_TIME_ID = "ZZ" THEN
														(
															(TRH.INSPECTION_YMD = :DAYYZ01 AND TRH.INSPECTION_TIME_ID = :IDPZ1)
														OR	(TRH.INSPECTION_YMD = :DAYYZ02 AND TRH.INSPECTION_TIME_ID = :IDPZ2)
														OR	(TRH.INSPECTION_YMD = :DAYYZ03 AND TRH.INSPECTION_TIME_ID = :IDPZ3)
														OR	(TRH.INSPECTION_YMD = :DAYYZ04 AND TRH.INSPECTION_TIME_ID = :IDPZ4)
														OR	(TRH.INSPECTION_YMD = :DAYYZ05 AND TRH.INSPECTION_TIME_ID = :IDPZ5)
														OR	(TRH.INSPECTION_YMD = :DAYYZ06 AND TRH.INSPECTION_TIME_ID = :IDPZ6)
														OR	(TRH.INSPECTION_YMD = :DAYYZ07 AND TRH.INSPECTION_TIME_ID = :IDPZ7)
														OR	(TRH.INSPECTION_YMD = :DAYYZ08 AND TRH.INSPECTION_TIME_ID = :IDPZ8)
														OR	(TRH.INSPECTION_YMD = :DAYYZ09 AND TRH.INSPECTION_TIME_ID = :IDPZ9)
														OR	(TRH.INSPECTION_YMD = :DAYYZ10 AND TRH.INSPECTION_TIME_ID = :IDPZ10)
														OR	(TRH.INSPECTION_YMD = :DAYYZ11 AND TRH.INSPECTION_TIME_ID = :IDPZ11)
														OR	(TRH.INSPECTION_YMD = :DAYYZ12 AND TRH.INSPECTION_TIME_ID = :IDPZ12)
														OR	(TRH.INSPECTION_YMD = :DAYYZ13 AND TRH.INSPECTION_TIME_ID = :IDPZ13)
														OR	(TRH.INSPECTION_YMD = :DAYYZ14 AND TRH.INSPECTION_TIME_ID = :IDPZ14)
														OR	(TRH.INSPECTION_YMD = :DAYYZ15 AND TRH.INSPECTION_TIME_ID = :IDPZ15)
														OR	(TRH.INSPECTION_YMD = :DAYYZ16 AND TRH.INSPECTION_TIME_ID = :IDPZ16)
														OR	(TRH.INSPECTION_YMD = :DAYYZ17 AND TRH.INSPECTION_TIME_ID = :IDPZ17)
														OR	(TRH.INSPECTION_YMD = :DAYYZ18 AND TRH.INSPECTION_TIME_ID = :IDPZ18)
														OR	(TRH.INSPECTION_YMD = :DAYYZ19 AND TRH.INSPECTION_TIME_ID = :IDPZ19)
														OR	(TRH.INSPECTION_YMD = :DAYYZ20 AND TRH.INSPECTION_TIME_ID = :IDPZ20)
														OR	(TRH.INSPECTION_YMD = :DAYYZ21 AND TRH.INSPECTION_TIME_ID = :IDPZ21)
														OR	(TRH.INSPECTION_YMD = :DAYYZ22 AND TRH.INSPECTION_TIME_ID = :IDPZ22)
														OR	(TRH.INSPECTION_YMD = :DAYYZ23 AND TRH.INSPECTION_TIME_ID = :IDPZ23)
														OR	(TRH.INSPECTION_YMD = :DAYYZ24 AND TRH.INSPECTION_TIME_ID = :IDPZ24)
														)
														END
													)
												WHERE INSNO.DELETE_FLG = "0"
									      		AND (TNM.INSPECTION_TIME_ID = :IDY1
														OR TNM.INSPECTION_TIME_ID = :IDY2
														OR TNM.INSPECTION_TIME_ID = :IDY3
														OR TNM.INSPECTION_TIME_ID = :IDY4
														OR TNM.INSPECTION_TIME_ID = :IDY5
														OR TNM.INSPECTION_TIME_ID = :IDY6
														OR TNM.INSPECTION_TIME_ID = :IDY7
														OR TNM.INSPECTION_TIME_ID = :IDY8
														OR TNM.INSPECTION_TIME_ID = :IDY9
														OR TNM.INSPECTION_TIME_ID = :IDY10
														OR TNM.INSPECTION_TIME_ID = :IDY11
														OR TNM.INSPECTION_TIME_ID = :IDY12
														OR TNM.INSPECTION_TIME_ID = :IDY13
														OR TNM.INSPECTION_TIME_ID = :IDY14
														OR TNM.INSPECTION_TIME_ID = :IDY15
														OR TNM.INSPECTION_TIME_ID = :IDY16
														OR TNM.INSPECTION_TIME_ID = :IDY17
														OR TNM.INSPECTION_TIME_ID = :IDY18
														OR TNM.INSPECTION_TIME_ID = :IDY19
														OR TNM.INSPECTION_TIME_ID = :IDY20
														OR TNM.INSPECTION_TIME_ID = :IDY21
														OR TNM.INSPECTION_TIME_ID = :IDY22
														OR TNM.INSPECTION_TIME_ID = :IDY23
														OR TNM.INSPECTION_TIME_ID = :IDY24
														OR TNM.INSPECTION_TIME_ID = "ZZ"
													  )
										GROUP BY TRH.PROCESS_ID
									          ,INSNO.REV_NO
									          ,INSNO.INSPECTION_SHEET_NO
									          ,TNM.INSPECTION_TIME_ID
									          ,COALESCE(TRH.MACHINE_NO,"")   
							) SUB
					GROUP BY SUB.PROCESS_ID
					        ,SUB.REV_NO
					        ,SUB.INSPECTION_SHEET_NO	
			 ) YOJITU
			
		 ON  YOJITU.PROCESS_ID          = MST.PROCESS_ID
		AND YOJITU.INSPECTION_SHEET_NO = MST.INSPECTION_SHEET_NO
		AND YOJITU.REV_NO              = MST.REV_NO		

		LEFT OUTER JOIN tmachinm AS MCN
		        ON YOJITU.MACHINE_NO = MCN.MACHINE_NO
		       AND MCN.DELETE_FLG = "0"
		     WHERE YOJITU.MACHINE_NO is not null
		
		ORDER BY CUSTOM.CUSTOMER_NAME
		        ,MST.ITEM_NO
		        ,YOJITU.MACHINE_NO  		        								          
								              
        	',[
        		"DAY01"      => $pDAY01,
				"DAY02"      => $pDAY02,
				"DAY03"      => $pDAY03,
				"DAY04"      => $pDAY04,
				"DAY05"      => $pDAY05,
				"DAY06"      => $pDAY06,
				"DAY07"      => $pDAY07,
				"DAY08"      => $pDAY08,
				"DAY09"      => $pDAY09,
				"DAY10"      => $pDAY10,
				"DAY11"      => $pDAY11,
				"DAY12"      => $pDAY12,
				"DAY13"      => $pDAY13,
				"DAY14"      => $pDAY14,
				"DAY15"      => $pDAY15,
				"DAY16"      => $pDAY16,
				"DAY17"      => $pDAY17,
				"DAY18"      => $pDAY18,
				"DAY19"      => $pDAY19,
				"DAY20"      => $pDAY20,
				"DAY21"      => $pDAY21,
				"DAY22"      => $pDAY22,
				"DAY23"      => $pDAY23,
				"DAY24"      => $pDAY24,
				"IDJ1"       => $pID1,
				"IDJ2"       => $pID2,
				"IDJ3"       => $pID3,
				"IDJ4"       => $pID4,
				"IDJ5"       => $pID5,
				"IDJ6"       => $pID6,
				"IDJ7"       => $pID7,
				"IDJ8"       => $pID8,
				"IDJ9"       => $pID9,
				"IDJ10"      => $pID10,
				"IDJ11"      => $pID11,
				"IDJ12"      => $pID12,
				"IDJ13"      => $pID13,
				"IDJ14"      => $pID14,
				"IDJ15"      => $pID15,
				"IDJ16"      => $pID16,
				"IDJ17"      => $pID17,
				"IDJ18"      => $pID18,
				"IDJ19"      => $pID19,
				"IDJ20"      => $pID20,
				"IDJ21"      => $pID21,
				"IDJ22"      => $pID22,
				"IDJ23"      => $pID23,
				"IDJ24"      => $pID24,
				"DAYY01"     => $pDAY01,
					"DAYY02"     => $pDAY02,
					"DAYY03"     => $pDAY03,
					"DAYY04"     => $pDAY04,
					"DAYY05"     => $pDAY05,
					"DAYY06"     => $pDAY06,
					"DAYY07"     => $pDAY07,
					"DAYY08"     => $pDAY08,
					"DAYY09"     => $pDAY09,
					"DAYY10"     => $pDAY10,
					"DAYY11"     => $pDAY11,
					"DAYY12"     => $pDAY12,
					"DAYY13"     => $pDAY13,
					"DAYY14"     => $pDAY14,
					"DAYY15"     => $pDAY15,
					"DAYY16"     => $pDAY16,
					"DAYY17"     => $pDAY17,
					"DAYY18"     => $pDAY18,
					"DAYY19"     => $pDAY19,
					"DAYY20"     => $pDAY20,
					"DAYY21"     => $pDAY21,
					"DAYY22"     => $pDAY22,
					"DAYY23"     => $pDAY23,
					"DAYY24"     => $pDAY24,
					"IDP1"       => $pID1,
					"IDP2"       => $pID2,
					"IDP3"       => $pID3,
					"IDP4"       => $pID4,
					"IDP5"       => $pID5,
					"IDP6"       => $pID6,
					"IDP7"       => $pID7,
					"IDP8"       => $pID8,
					"IDP9"       => $pID9,
					"IDP10"      => $pID10,
					"IDP11"      => $pID11,
					"IDP12"      => $pID12,
					"IDP13"      => $pID13,
					"IDP14"      => $pID14,
					"IDP15"      => $pID15,
					"IDP16"      => $pID16,
					"IDP17"      => $pID17,
					"IDP18"      => $pID18,
					"IDP19"      => $pID19,
					"IDP20"      => $pID20,
					"IDP21"      => $pID21,
					"IDP22"      => $pID22,
					"IDP23"      => $pID23,
					"IDP24"      => $pID24,
					"DAYYZ01"    => $pDAY01,
					"DAYYZ02"    => $pDAY02,
					"DAYYZ03"    => $pDAY03,
					"DAYYZ04"    => $pDAY04,
					"DAYYZ05"    => $pDAY05,
					"DAYYZ06"    => $pDAY06,
					"DAYYZ07"    => $pDAY07,
					"DAYYZ08"    => $pDAY08,
					"DAYYZ09"    => $pDAY09,
					"DAYYZ10"    => $pDAY10,
					"DAYYZ11"    => $pDAY11,
					"DAYYZ12"    => $pDAY12,
					"DAYYZ13"    => $pDAY13,
					"DAYYZ14"    => $pDAY14,
					"DAYYZ15"    => $pDAY15,
					"DAYYZ16"    => $pDAY16,
					"DAYYZ17"    => $pDAY17,
					"DAYYZ18"    => $pDAY18,
					"DAYYZ19"    => $pDAY19,
					"DAYYZ20"    => $pDAY20,
					"DAYYZ21"    => $pDAY21,
					"DAYYZ22"    => $pDAY22,
					"DAYYZ23"    => $pDAY23,
					"DAYYZ24"    => $pDAY24,
					"IDPZ1"      => $pID1,
					"IDPZ2"      => $pID2,
					"IDPZ3"      => $pID3,
					"IDPZ4"      => $pID4,
					"IDPZ5"      => $pID5,
					"IDPZ6"      => $pID6,
					"IDPZ7"      => $pID7,
					"IDPZ8"      => $pID8,
					"IDPZ9"      => $pID9,
					"IDPZ10"     => $pID10,
					"IDPZ11"     => $pID11,
					"IDPZ12"     => $pID12,
					"IDPZ13"     => $pID13,
					"IDPZ14"     => $pID14,
					"IDPZ15"     => $pID15,
					"IDPZ16"     => $pID16,
					"IDPZ17"     => $pID17,
					"IDPZ18"     => $pID18,
					"IDPZ19"     => $pID19,
					"IDPZ20"     => $pID20,
					"IDPZ21"     => $pID21,
					"IDPZ22"     => $pID22,
					"IDPZ23"     => $pID23,
					"IDPZ24"     => $pID24,
					"IDY1"       => $pID1,
					"IDY2"       => $pID2,
					"IDY3"       => $pID3,
					"IDY4"       => $pID4,
					"IDY5"       => $pID5,
					"IDY6"       => $pID6,
					"IDY7"       => $pID7,
					"IDY8"       => $pID8,
					"IDY9"       => $pID9,
					"IDY10"      => $pID10,
					"IDY11"      => $pID11,
					"IDY12"      => $pID12,
					"IDY13"      => $pID13,
					"IDY14"      => $pID14,
					"IDY15"      => $pID15,
					"IDY16"      => $pID16,
					"IDY17"      => $pID17,
					"IDY18"      => $pID18,
					"IDY19"      => $pID19,
					"IDY20"      => $pID20,
					"IDY21"      => $pID21,
					"IDY22"      => $pID22,
					"IDY23"      => $pID23,
					"IDY24"      => $pID24,
					"YID1"       => $pID1,
					"YID2"       => $pID2,
					"YID3"       => $pID3,
					"YID4"       => $pID4,
					"YID5"       => $pID5,
					"YID6"       => $pID6,
					"YID7"       => $pID7,
					"YID8"       => $pID8,
					"YID9"       => $pID9,
					"YID10"      => $pID10,
					"YID11"      => $pID11,
					"YID12"      => $pID12,
					"YID13"      => $pID13,
					"YID14"      => $pID14,
					"YID15"      => $pID15,
					"YID16"      => $pID16,
					"YID17"      => $pID17,
					"YID18"      => $pID18,
					"YID19"      => $pID19,
					"YID20"      => $pID20,
					"YID21"      => $pID21,
					"YID22"      => $pID22,
					"YID23"      => $pID23,
					"YID24"      => $pID24,
					"JID1"       => $pID1,
				"JID2"       => $pID2,
				"JID3"       => $pID3,
				"JID4"       => $pID4,
				"JID5"       => $pID5,
				"JID6"       => $pID6,
				"JID7"       => $pID7,
				"JID8"       => $pID8,
				"JID9"       => $pID9,
				"JID10"      => $pID10,
				"JID11"      => $pID11,
				"JID12"      => $pID12,
				"JID13"      => $pID13,
				"JID14"      => $pID14,
				"JID15"      => $pID15,
				"JID16"      => $pID16,
				"JID17"      => $pID17,
				"JID18"      => $pID18,
				"JID19"      => $pID19,
				"JID20"      => $pID20,
				"JID21"      => $pID21,
				"JID22"      => $pID22,
				"JID23"      => $pID23,
				"JID24"      => $pID24,
				"EID1"       => $pID1,
				"EID2"       => $pID2,
				"EID3"       => $pID3,
				"EID4"       => $pID4,
				"EID5"       => $pID5,
				"EID6"       => $pID6,
				"EID7"       => $pID7,
				"EID8"       => $pID8,
				"EID9"       => $pID9,
				"EID10"      => $pID10,
				"EID11"      => $pID11,
				"EID12"      => $pID12,
				"EID13"      => $pID13,
				"EID14"      => $pID14,
				"EID15"      => $pID15,
				"EID16"      => $pID16,
				"EID17"      => $pID17,
				"EID18"      => $pID18,
				"EID19"      => $pID19,
				"EID20"      => $pID20,
				"EID21"      => $pID21,
				"EID22"      => $pID22,
				"EID23"      => $pID23,
				"EID24"      => $pID24,
				"TIME3ID1"   => $pID1,
				"TIME4ID1"   => $pID1,
				"TODAY1"     => $ptoday,
				"TODAY2"     => $ptoday,
				"MID1"       => $pMID,
				"YEST1"      => $pyesterday,
				"YEST2"      => $pyesterday,
				"TODAY3"     => $ptoday,
				"TIME1ID1"   => $pID1,
				"TIME2ID1"   => $pID1,
        	]);

		return $lTblSearchResultData;
	}

	private function getID1($time , $num){
		$ID = $time - $num; //表時刻1列目（現在時刻の23時間前）$result=array_diff($a1,$a2);
		if ($ID < 0) //1列目が昨日にわたる場合
		{
			$ID = $ID + self::NUMBER_TIME24; //マスタ時間に合わせるため24時間プラスする
		}
		if ($ID < self::NUMBER_KETA) //2桁以下になる場合
		{
			$ID = sprintf('%02d', $ID); //マスタの表記に合わせるため前0処理を行う
		}
		return $ID;
	}

	private function getDAY1($ID ,$today, $yesterday){
        $DAY = $today;
		if ($ID < 0)
		{
			$DAY = $yesterday;
		}
		return $DAY;
	}

	private function getID2($time , $num){
		$ID = $time - $num; //表時刻1列目（現在時刻の23時間前）

		if ($ID <= 0) //1列目が昨日にわたる場合
		{
			$ID = $ID + self::NUMBER_TIME24; //マスタ時間に合わせるため24時間プラスする
		}
		if ($ID < self::NUMBER_KETA) //2桁以下になる場合
		{ 
			$ID = sprintf('%02d', $ID); //マスタの表記に合わせるため前0処理を行う
		}
		return $ID;
	}

	private function getDAY2($ID ,$today, $yesterday){
		$DAY = $today;
		if ($ID <= 0) //1列目が昨日にわたる場合
		{
			$DAY = $yesterday; //日付を昨日にする
	    }
	    return $DAY;
	}
}