<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Model\tcodemst;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use Carbon\Carbon;
use Exception;
use Session;
use DB;
set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');

//**************************************************************************
// display:	 Graph
// overview: For Plasess Company
// author:	Mai Nawaphat(Mai ^^)
// date:	14/2/2018
//**************************************************************************
class BA4010GraphController extends Controller
{  
	public function MasterAction()
	{   
		$lViewData	   = []; //Array for transportion to screen
		$lViewData    += [
			"UserID"   => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

        $lArrProductNoList[''] 			= "";
		$lArrRevNoList['']     			= "";
        $lArrMoldNoList['']    			= "";
        $lArrCavityNoList['']  			= "";
        $lArrInspectionPointList[''] 	= "";
        $lArrTimeIDList[''] 			= "";
        $lArrTeamNameList[''] 			= "";
        $lArrProcessList[''] 			= "";
        $lArrCustomerList[''] 			= "";
        $lArrConditionList[''] 			= "";
        $lArrPicture[''] 				= "";
        $lArrWhereNoList['']            = "";

		$lViewData 	+= $this->setProcessList($lViewData);
		
		if(Input::has('cmbProcessForGraph')){
            $lViewData += $this->setCustomerList($lViewData, Input::get('cmbProcessForGraph'));
		}
		else{
			$processName = data_get($lViewData, 'arrProcessList.01');
			$lViewData += $this->setCustomerListfromProcessName($lViewData, $processName);
		}
		
        $lViewData += [
        	"arrProductNoList" 			=> $lArrProductNoList,
			"arrRevNoList" 				=> $lArrRevNoList,
			"arrMoldNoList" 			=> $lArrMoldNoList,
			"arrCavityNoList" 			=> $lArrCavityNoList,
			"arrInspectionPointList" 	=> $lArrInspectionPointList,
			"arrTimeIDList" 			=> $lArrTimeIDList,
			"arrTeamNameList" 			=> $lArrTeamNameList,
			"arrProcessList" 			=> $lArrProcessList,
			"arrCustomerList" 			=> $lArrCustomerList,
			"arrConditionList" 			=> $lArrConditionList,
			"arrPicture"     			=> $lArrPicture,
			"arrWhereNoList" 			=> $lArrWhereNoList,
		];

		$lViewData += $this->getInspectionDateFromToForGraph($lViewData);
		$lViewData += $this->setListForms($lViewData);
		

		$lViewData += [
				"SelectTypeData" => Input::get('cmbSelectTypeForGraph'),
		];

        if(Input::has('btnGraph')) {

        	if(Input::get('cmbCustomerForGraph') == "") {
        		$lViewData["errors"] = new MessageBag([
					"error" => "E050 : Customer/Supplier is invalid."
				]);
        	}elseif(Input::get('cmbCheckSheetNoForGraph') == "") {
        		$lViewData["errors"] = new MessageBag([
					"error" => "E007 : Product No. is invalid."
				]);
        	}elseif(Input::get('cmbRevisionNoForGraph') == "") {
        		$lViewData["errors"] = new MessageBag([
					"error" => "E018 : Rev No. is invalid."
				]);
        	}elseif(Input::get('cmbMoldNoForGraph') == "") {
        		$lViewData["errors"] = new MessageBag([
					"error" => "E066 : Mold No. is invalid."
				]);
        	}elseif(Input::get('cmbSelectTypeForGraph') == ""){
        		$lViewData["errors"] = new MessageBag([
					"error" => "Error : Type Graph is invalid."
				]);
        	}elseif(Input::get('cmbRadioChoosePeriod') == "" and Input::get('cmbRadioChooseProductionDate') == "" and Input::get('cmbRadioChooseInspectionDate') == "") {
        		$lViewData["errors"] = new MessageBag([
					"error" => "E016 : Inspection Date or Production Date is invalid."
				]);
        	}elseif(Input::get('cmbRadioChooseInspectionDate') != "") {
        		if(Input::get('txtInspectionDateFromForGraph') == "" or Input::get('txtInspectionDateToForGraph') == "") {
	        		$lViewData["errors"] = new MessageBag([
						"error" => "Error : Inspection Date From or To is invalid."
					]);
		        }
		    }elseif(Input::get('cmbRadioChooseProductionDate') != "") {
		        if(Input::get('txtProductionDateFromForGraph') == "" or Input::get('txtProductionDateToForGraph') == "") {
		        	$lViewData["errors"] = new MessageBag([
						"error" => "Error : Production Date From or To is invalid."
					]);
		        }
        	}elseif(Input::get('cmbRadioChoosePeriod') != "") {
        		if(Input::get('txtTypeTimeForGraph') == "") {
		        	$lViewData["errors"] = new MessageBag([
						"error" => "Error : Days is invalid."
					]);
		        }
        	}
            
            if(Input::get('txtTypeTimeForGraph') != "" or Input::get('txtInspectionDateFromForGraph') != "" or Input::get('txtInspectionDateToForGraph') != "" or Input::get('txtProductionDateFromForGraph') != "" or Input::get('txtProductionDateToGraph') != ""){
            	$lTblSearchResultData = $this->getGraphResultList();
            	$cpkData = $this->getGraphCPK();
           

			if (count($lTblSearchResultData) == 0 or count($cpkData) == 0){   
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
					$lViewData += [  
						"arrCustomerID"         => null,
						"arrCustomerName"       => null,
						"arrProductNo"          => null,
						"arrProductName"        => null,
						"arrUpperSpec"	        => null,
						"arrLowerSpec"          => null,
						"arrCenterSpec"         => null,
						"arrRevNo1Dimention"    => null,
						"arrMoldNo1Dimention"   => null,
						"arrMachineNo"          => null,
						"arrPoint1Dimention" 	=> null,
						"arrCavity1Dimention" 	=> null,
						"arrWhere1Dimention" 	=> null,
						"arrCenterSpec"         => null,
						"arrUpperSpec"          => null,
						"arrLowerSpec"          => null,
						"arrAverageHeader"      => null,
						"arrMaxHeader"          => null,
						"arrMinHeader"          => null,
						"arrRange"     			=> null,
						"UCLR"                  => null,
						"LCLR"                  => null,
						"CLR"                   => null,
						"arrMax"                => null,
						"arrMin"                => null,
						"arrAverage"            => null,
						"UCLX"                  => null,
						"LCLX"                  => null,
						"CLX"                   => null,
						"UCLS"                  => null,
						"LCLS"                  => null,
						"arrLabel"              => null,
						"DateFrom" 				=> null,
						"DateTo" 				=> null,
						"arrItemName"           => null,
						"minCpk"         		=> null,
						"maxCpk"        	  	=> null,
						"totalsValue"    		=> null,
						"averageCpk"     		=> null,
						"stdevCpk"       		=> null,
						"specUpperCPK"   		=> null,
						"specLowerCPK"   		=> null,
						"Cp"             		=> null,
						"Cpu" 			 		=> null,
						"Cpl" 			 		=> null,
						"Cpk"  			 		=> null,
						"estimateStdev"  		=> null,
						"Pp" 			 		=> null,
						"Ppu"       	 		=> null,
						"Ppl"  			 		=> null,
						"Ppk"   	 	 		=> null,
						"arrBellCurve" 	 		=> null,
						"arrBarCPK"      		=> null,
						"arrLabelCPK"  	 		=> null,
						"arrIndexUSL"  	 		=> null,
						"arrIndexLSL"    		=> null,
						"arrBar"         		=> null,
						"arrStandard"           => null,
						"UCLStdev"              => null,
						"LCLStdev"              => null,
						"CLS"                   => null,
						"UCLXbarStd" 			=> null,
						"LCLXbarStd" 			=> null,
					];

			}else{	
					foreach ($lTblSearchResultData as $key => $value) 
					{   if($value->INSPECTION_POINT == ""){
							$value->INSPECTION_POINT = 0;
						}
						if($value->CAVITY_NO == ""){
							$value->CAVITY_NO = 0;
						}
						if($value->WHERE_NO == ""){
							$value->WHERE_NO = 0;
						}
						//Title For Average, Range, Xbar Or Header Chart				
						$arrPoint[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_POINT;
						$arrCavity[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CAVITY_NO;
						$arrWhere[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->WHERE_NO;
						//Range 
						$arrRange[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->RANGE_VALUE;
						//Average
						$arrAverage[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->AVERAGE_VALUE;
						$arrMax[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MAX_VALUE;
						$arrMin[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MIN_VALUE;
						//Stdev
					    $arrStandard[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STDEV;
					    //Label			
					    $arrLotNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->LOT_NO;
						$arrProcessName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PROCESS_NAME;
						$arrTeamName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->TEAM_NAME;
						$arrConditionName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CONDITION_CD;
						$arrInspection[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_YMD;
						$arrTime[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_TIME_NAME;
						$arrAverageUCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_UCL;
						$arrAverageLCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_LCL;
						$arrMoldNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MOLD_NO;
						$arrRevNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->REV_NO;
						$arrPicture[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PICTURE_URL;
						//HEADER
						$arrCustomerID[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CUSTOMER_ID;
						$arrCustomerName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CUSTOMER_NAME;  
						$arrProductNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PRODUCT_NO;
						$arrProductName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PRODUCT_NAME;
						$arrMachineNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MACHINE_NO;
						$arrUpperSpec[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STD_MAX_SPEC;
						$arrLowerSpec[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STD_MIN_SPEC;
						$arrCenterSpec[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = ($value->STD_MAX_SPEC + $value->STD_MIN_SPEC)/2;
						$arrItemName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->ITEM_NAME;

					}	

					//Title For Average, Range, Xbar Chart	Or Header Chart
					$arrRevNo1Dimention  = $this->arrThreeToOneDimention($arrRevNo);
					$arrMoldNo1Dimention = $this->arrThreeToOneDimention($arrMoldNo);
					$arrPoint1Dimention = $this->arrThreeToOneDimention($arrPoint);		
					$arrCavity1Dimention = $this->arrThreeToOneDimention($arrCavity);	
					$arrWhere1Dimention = $this->arrThreeToOneDimention($arrWhere);	

					//Range  Chart
                    $arrRangeForGraph = $this->getArrPointOnLineChart($arrRange);
                    $coefficientUCLRObject = $this->coefficientObject("D4", $arrRange);
                    $averageRange = $this->arrAverageValue($arrRange);
                    $coefficientLCLRObject = $this->coefficientObject("D3", $arrRange);
                    $UCLR = $this->arrLimitUCL($coefficientUCLRObject, $averageRange);
                    $arrUCLRFillPointForGroup = $this->fourDimentionToTwoDimention($UCLR);
                    $LCLR = $this->arrLimitLCL($coefficientLCLRObject, $averageRange);
                    $arrLCLRFillPointForGroup = $this->fourDimentionToTwoDimention($LCLR);
				    $arrCLR = $this->fourDimentionToTwoDimention($averageRange);

                    //Xbar Chart
                    $arrAverageForGraph = $this->getArrPointOnLineChart($arrAverage);
                    $arrMaxForGraph = $this->getArrPointOnLineChart($arrMax);
                    $arrMinForGraph = $this->getArrPointOnLineChart($arrMin);

				    	//UCL and LCL of Xbar
				    $averageAverage = $this->arrAverageValue($arrAverage);
                    $coefficientXObject = $this->coefficientObject("A2", $arrAverage);
                    $UCLX = $this->arrLimitUCLXbar($coefficientXObject, $averageRange, $averageAverage);
                    $arrUCLXFillPointForGroup = $this->fourDimentionToTwoDimention($UCLX);
				    $LCLX = $this->arrLimitLCLXbar($coefficientXObject, $averageRange, $averageAverage);
                    $arrLCLXFillPointForGroup = $this->fourDimentionToTwoDimention($LCLX);
                    $arrCLX = $this->fourDimentionToTwoDimention($averageAverage);

                       //UCL and LCL of Stdev
                    $UCLS = $this->fourDimentionToTwoDimention($arrUpperSpec);
                    $LCLS = $this->fourDimentionToTwoDimention($arrLowerSpec);
                    $averageStdev = $this->arrAverageValue($arrStandard);
                    $arrCLS = $this->fourDimentionToTwoDimention($averageStdev);
                    $coefficientXbarStdObject = $this->coefficientObject("A3", $arrAverage);
                    $UCLXbarStd = $this->arrLimitUCLXbar($coefficientXbarStdObject, $averageStdev, $averageAverage);
                    $arrUCLXbarStdFillPointForGroup = $this->fourDimentionToTwoDimention($UCLXbarStd);
                    $LCLXbarStd = $this->arrLimitLCLXbar($coefficientXbarStdObject, $averageStdev, $averageAverage);
                    $arrLCLXbarStdFillPointForGroup = $this->fourDimentionToTwoDimention($LCLXbarStd);


                    //Stdev Chart
				    $arrStandardForGraph = $this->getArrPointOnLineChart($arrStandard);
                    $coefficientUCLStdevObject = $this->coefficientObject("B4", $arrStandard);
                    $coefficientLCLStdevObject = $this->coefficientObject("B3", $arrStandard);
                    $averageStdevForUCL = $this->arrAverageValue($arrStandard);
                    $UCLStdev = $this->arrLimitUCL($coefficientUCLStdevObject, $averageStdevForUCL);
                    $LCLStdev = $this->arrLimitLCL($coefficientLCLStdevObject, $averageStdevForUCL);
				    $arrUCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($UCLStdev);
				    $arrLCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($LCLStdev);


					//Start Label for Average, Range, Xbar Chart
					$arrLotNoForMerge = $this->getLabelForMerge($arrLotNo, "Lot No");
					$arrTeamNameForMerge = $this->getLabelForMerge($arrTeamName, "Team Name");
					$arrConditionForMerge = $this->getLabelForMerge($arrConditionName, "Condition Name");
					$arrInspectionForMerge = $this->getLabelForMerge($arrInspection, "Inspection Date");
					$arrTimeForMerge = $this->getLabelForMerge($arrTime, "Time");
					$mergeLabel = array_merge_recursive($arrLotNoForMerge, $arrTeamNameForMerge, $arrConditionForMerge, $arrInspectionForMerge, $arrTimeForMerge);

					foreach ($mergeLabel as $key => $value) {
						foreach ($value as $key1 => $value1) {
							foreach ($value1 as $key2 => $value2) {
								$lebel1Dimention[$key1][] = $value2;
							}
						}
					}

					$arrLabel = array_values($lebel1Dimention);
                    $picture = $this->arrThreeToOneDimention($arrPicture);
					data_set($lViewData, 'arrPicture', array($picture[0]));

				//HEADER	
					$arrCustomerID = $this->arrThreeToOneDimention($arrCustomerID);
					$arrCustomerName = $this->arrThreeToOneDimention($arrCustomerName);
					$arrProductNo  = $this->arrThreeToOneDimention($arrProductNo);
					$arrProductName = $this->arrThreeToOneDimention($arrProductName);
					$arrMachineNo = $this->arrThreeToOneDimention($arrMachineNo);
					$arrUpperSpec = $this->arrThreeToOneDimention($arrUpperSpec);
					$arrLowerSpec = $this->arrThreeToOneDimention($arrLowerSpec);
					$arrCenterSpec = $this->arrThreeToOneDimention($arrCenterSpec);
					$arrAverageHeader    = $this->arrThreeToOneDimention($arrAverage);
					$arrMaxHeader  = $this->arrThreeToOneDimention($arrMax);
					$arrMinHeader  = $this->arrThreeToOneDimention($arrMin);
					$arrItemName = $this->arrThreeToOneDimention($arrItemName);

					if (Input::has('cmbRadioChoosePeriod')){
						$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
						$ldate  = Carbon::now();
						$ldateNow = $ldate->toDateTimeString();
						$ldatePast = $ldate->subDays($amountOfTime);
						$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
						$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");


					}elseif(Input::has('cmbRadioChooseInspectionDate')){
						$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
						$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
					}else{
						$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
						$lInspectionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
					}
					

					$lViewData += [     
						//HEADER
						"arrCustomerID"         => $arrCustomerID,
						"arrCustomerName"       => $arrCustomerName,
						"arrProductNo"          => $arrProductNo,
						"arrProductName"        => $arrProductName,
						"arrUpperSpec"	        => $arrUpperSpec,
						"arrLowerSpec"          => $arrLowerSpec,
						"arrCenterSpec"         => $arrCenterSpec,
						"arrRevNo1Dimention"    => $arrRevNo1Dimention,
						"arrMoldNo1Dimention"   => $arrMoldNo1Dimention,
						"arrMachineNo"          => $arrMachineNo,
						"arrPoint1Dimention" 	=> $arrPoint1Dimention,
						"arrCavity1Dimention" 	=> $arrCavity1Dimention,
						"arrWhere1Dimention" 	=> $arrWhere1Dimention,
						"arrAverageHeader"      => $arrAverageHeader,
						"arrMaxHeader"          => $arrMaxHeader,
						"arrMinHeader"          => $arrMinHeader,
						"arrRange"     			=> $arrRangeForGraph,
						"UCLR"                  => $arrUCLRFillPointForGroup,
						"LCLR"                  => $arrLCLRFillPointForGroup,
						"CLR"                   => $arrCLR,
						"arrMax"                => $arrMaxForGraph,
						"arrMin"                => $arrMinForGraph,
						"arrAverage"            => $arrAverageForGraph,
						"UCLX"                  => $arrUCLXFillPointForGroup,
						"LCLX"                  => $arrLCLXFillPointForGroup,
						"CLX"                   => $arrCLX,
						"UCLS"                  => $UCLS,
						"LCLS"                  => $LCLS,
						"arrLabel"              => $arrLabel,
						"DateFrom" 				=> $lInspectionDateFromForGraph,
						"DateTo" 				=> $lInspectionDateToForGraph,
						"arrItemName"           => $arrItemName,
						"arrStandard"           => $arrStandardForGraph,
						"UCLStdev"              => $arrUCLStdevFillPointForGroup,
						"LCLStdev"              => $arrLCLStdevFillPointForGroup,
						"CLS"                   => $arrCLS,
						"UCLXbarStd" 			=> $arrUCLXbarStdFillPointForGroup,
						"LCLXbarStd" 			=> $arrLCLXbarStdFillPointForGroup,
					];	
                    
                    //CPK
					foreach ($cpkData as $key => $value) 
					{
						if($value->INSPECTION_POINT == ""){
							$value->INSPECTION_POINT = 0;
						}
						if($value->CAVITY_NO == ""){
							$value->CAVITY_NO = 0;
						}
						if($value->WHERE_NO == ""){
							$value->WHERE_NO = 0;
						}

                        $inspec[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_RESULT_VALUE;
                        $specU[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STD_MAX_SPEC;
                        $specL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STD_MIN_SPEC;
					}
					
                    foreach ($inspec as $key => $value) {
                    	foreach ($value as $key1 => $value1) {
                    		foreach ($value1 as $key2 => $value2) {
                    			$max_cpk[] = number_format(max($value2),4);
								$min_cpk[] = number_format(min($value2),4);
								$sum_cpk[] = array_sum($value2);
								$totalsValue_cpk[] = count($value2);
								$stdev_cpk[] = number_format($this->standard_deviation($value2),4);

								$estimate_stdev_cpk[] = number_format($this->estimate_standard_deviation($value2),4);
								$arrInspectionResult[] = $value2;								
                    		}
                    	}
                    }
                    foreach ($sum_cpk as $key => $value) {
                    	$average_cpk[] = number_format($sum_cpk[$key]/$totalsValue_cpk[$key],4);
                    	
                    }
                    foreach ($specU as $key => $value) {
                    	foreach ($value as $key1 => $value1) {
                    		foreach ($value1 as $key2 => $value2) {
                    			$spc_Upper_Cpk[] = $value2[0];
                    		}
                    	}
                    }
                    foreach ($specL as $key => $value) {
                    	foreach ($value as $key1 => $value1) {
                    		foreach ($value1 as $key2 => $value2) {
                    			$spc_Lower_Cpk[] = $value2[0];
                    		}
                    	}
                    }
                    foreach ($stdev_cpk as $key => $value) {
                    	$Cp_cpk[] = number_format(( $spc_Upper_Cpk[$key] - $spc_Lower_Cpk[$key] ) / ( 6 * $value ),4);
						$Cpu_cpk[] = number_format(( $spc_Upper_Cpk[$key] - $average_cpk[$key] ) / ( 3 * $value ),4);
						$Cpl_cpk[] = number_format(( $average_cpk[$key] - $spc_Lower_Cpk[$key] ) / ( 3 * $value ),4);
                    }
                    foreach ($Cpu_cpk as $key => $value) {
                    	$arr_Cpk_value[] = array($value,$Cpl_cpk[$key]);
                    }
                    foreach ($arr_Cpk_value as $key => $value) {
                    	$arr_Cpk[] = min($value);
                    }

                    foreach ($estimate_stdev_cpk as $key => $value) {
                    	$Pp_cpk[] = number_format(( $spc_Upper_Cpk[$key] - $spc_Lower_Cpk[$key] ) / ( 6 * $value ),4);
						$Ppu_cpk[] = number_format(( $spc_Upper_Cpk[$key] - $average_cpk[$key] ) / ( 3 * $value ),4);
						$Ppl_cpk[] = number_format(( $average_cpk[$key] - $spc_Lower_Cpk[$key] ) / ( 3 * $value ),4);
                    }
                    foreach ($Ppu_cpk as $key => $value) {
                    	$arr_Ppk_value[] = array($value,$Ppl_cpk[$key]);
                    }
                    foreach ($arr_Ppk_value as $key => $value) {
                    	$arr_Ppk[] = min($value);
                    }

                    foreach ($stdev_cpk as $key => $value) {
                    	if($value == 0.000){
							$arrBellCurve_cpk[] = array(array(0, 0));
						}else{
							$arrBellCurve_cpk[] = $this->getBellCurve($value, $average_cpk[$key]);
						}
                    }

                    foreach ($arrBellCurve_cpk as $key => $value) {
                    	foreach ($value as $key1 => $value1) {
                    		foreach ($value1 as $key2 => $value2) {
								if($key2 == 0){
									$arrLabelCPK[$key][] = number_format($value2,4);
								}
								if($key2 == 1){
									$arrBarCPK[$key][]= $value2;
								}
                    		}
                    	}
					}

					foreach ($arrLabelCPK as $key => $value) {
						$count = count($value) - 1;
						foreach ($value as $key1 => $value1) {
							if($key1 < $count){
								$minInterval[$key][] = $value1;
								$maxInterval[$key][] = $value1 + ($value[$key1+1] - $value1);
							}
						}
					}
				 	$arrIndexUSL = [""];
                    $arrIndexLSL = [""];
					foreach ($minInterval as $key => $value) {
						foreach ($value as $key1 => $value1) {
							if($arrUpperSpec[$key] >= $value1 and $arrUpperSpec[$key] < $maxInterval[$key][$key1]){
                                $arrIndexUSL[$key] = ( $key1 /23 );
							}
							if($arrLowerSpec[$key] >= $value1 and $arrLowerSpec[$key] < $maxInterval[$key][$key1]){
                                $arrIndexLSL[$key] = ( $key1 /23 );
							}
						}
					}

                    if(count($arrIndexUSL) > 0){
						$diffarrIndexUSL = count($minInterval) - count($arrIndexUSL);
						if($diffarrIndexUSL != 0){
							for($i = 0; $i < $diffarrIndexUSL; $i++){
								array_push($arrIndexUSL, "");
							}
						}
					}
					if(count($arrIndexLSL) > 0){
						$diffarrIndexLSL = count($minInterval) - count($arrIndexLSL);
						if($diffarrIndexLSL != 0){
							for($i = 0; $i < $diffarrIndexLSL; $i++){
								array_push($arrIndexLSL, "");
							}
						}
					}

					foreach ($arrLabelCPK as $key => $value) {
						foreach ($value as $key1 => $value1) {
							$first[$key] = $value[0];
							$end[$key] = end($value);

							// $arrStartlineTwo[$key] = $value[0] - (($value[1] - $value[0])/2);
							// $arrEndlineTwo[$key] = end($value) + (($value[1] - $value[0])/2);
						}
					}

					// foreach ($arrEndlineTwo as $key => $value) {
					// 	$arrRangeline[$key] = $value - $arrStartlineTwo[$key];
					// }

					// foreach ($arrStartlineTwo as $key => $value) {
					// 	$arrValueUSL[$key] = $spc_Upper_Cpk[$key]-$value;
					// 	$arrValueLSL[$key] = $spc_Lower_Cpk[$key]-$value;
						
					// }

					// foreach ($arrRangeline as $key => $value) {
					// 	$arrIndexValueUSL[$key] = number_format(($spc_Upper_Cpk[$key]-$arrStartlineTwo[$key]) / $value , 4);
					// 	$arrIndexValueLSL[$key] = number_format(($spc_Lower_Cpk[$key]-$arrStartlineTwo[$key]) / $value , 4);
					// }

					
					foreach ($first as $key => $value) {
						$diff[$key] = $end[$key] - $value;
					}

					foreach ($arrLowerSpec as $key => $value) {
						$arrLSL[$key] = (($value - $first[$key])*1.1)/$diff[$key];
					}

   				    foreach ($arrInspectionResult as $key => $value) {
   				    	foreach ($value as $key1 => $value1) {
   				    		if($value1 >= $minInterval[$key][0] and $value1 < $maxInterval[$key][2]){
   				    				$arrBar[$key][1][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][2] and $value1 < $maxInterval[$key][4]){
   				    				$arrBar[$key][3][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][4] and $value1 < $maxInterval[$key][6]){
   				    				$arrBar[$key][5][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][6] and $value1 < $maxInterval[$key][8]){
   				    				$arrBar[$key][7][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][8] and $value1 < $maxInterval[$key][10]){
   				    				$arrBar[$key][9][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][10] and $value1 < $maxInterval[$key][12]){
   				    				$arrBar[$key][11][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][12] and $value1 < $maxInterval[$key][14]){
   				    				$arrBar[$key][13][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][14] and $value1 < $maxInterval[$key][16]){
   				    				$arrBar[$key][15][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][16] and $value1 < $maxInterval[$key][18]){
   				    				$arrBar[$key][17][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][18] and $value1 < $maxInterval[$key][20]){
   				    				$arrBar[$key][19][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][20] and $value1 < $maxInterval[$key][22]){
   				    				$arrBar[$key][21][] = $value1;
   				    		}elseif($value1 >= $minInterval[$key][22] and $value1 < $maxInterval[$key][24]){
   				    				$arrBar[$key][23][] = $value1;
   				    		}
   				    	}
   				    }

					foreach ($arrBar as $key => $value) {
						foreach ($value as $key1 => $value1) {
							$arrBarCount[$key][$key1] = count($value1);
						}
					}
					
					$arrBarTemplate = [];

					foreach ($arrBarCount as $key => $value) {
						$dimention = count($arrBarCount); 
						if($dimention > 0){
							data_set($arrBarTemplate, $key, array("","","","","","","","","","","","","","","","","","","","","","","","",""));
						}
					}
					
					foreach ($arrBarCount as $key => $value) {
						foreach ($value as $key1 => $value1) {
							data_set($arrBarTemplate, $key.'.'.$key1, $value1);
						}
					}

					foreach ($arrBarTemplate as $key => $value) {
						foreach ($value as $key1 => $value1) {
							$arrBarAxisXY[$key][$key1] = array("x"=>$key1,"y"=>$value1);
						}
					}

					$lViewData += [     
						//CPK
						"minCpk"         => $min_cpk,
						"maxCpk"         => $max_cpk,
						"totalsValue"    => $totalsValue_cpk,
						"averageCpk"     => $average_cpk,
						"stdevCpk"       => $stdev_cpk,
						"specUpperCPK"   => $spc_Upper_Cpk,
						"specLowerCPK"   => $spc_Lower_Cpk,
						"Cp"             => $Cp_cpk,
						"Cpu" 			 => $Cpu_cpk,
						"Cpl" 			 => $Cpl_cpk,
						"Cpk"  			 => $arr_Cpk,
						"estimateStdev"  => $estimate_stdev_cpk,
						"Pp" 			 => $Pp_cpk,
						"Ppu"       	 => $Ppu_cpk,
						"Ppl"  			 => $Ppl_cpk,
						"Ppk"   	 	 => $arr_Ppk,
						"arrBellCurve" 	 => $arrBellCurve_cpk,
						"arrBarCPK"      => $arrBarCPK,
						"arrLabelCPK"  	 => $arrLabelCPK,
						"arrIndexUSL"  	 => $arrIndexUSL,
						"arrIndexLSL"    => $arrIndexLSL,
						"arrBar"         => $arrBarAxisXY,
									
					];	
				}	
            } 	
            
		}else if(Input::has('btnExcel')){
                $lTblSearchResultData = $this->getGraphResultList();
                $cpkData = $this->getGraphCPK();

			 	if (count($lTblSearchResultData) == 0  or count($cpkData) == 0)
				{ 
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);

					$lViewData += [  
						"arrCustomerID"         => null,
						"arrCustomerName"       => null,
						"arrProductNo"          => null,
						"arrProductName"        => null,
						"arrUpperSpec"	        => null,
						"arrLowerSpec"          => null,
						"arrCenterSpec"         => null,
						"arrRevNo1Dimention"    => null,
						"arrMoldNo1Dimention"   => null,
						"arrMachineNo"          => null,
						"arrPoint1Dimention" 	=> null,
						"arrCavity1Dimention" 	=> null,
						"arrWhere1Dimention" 	=> null,
						"arrAverageHeader"      => null,
						"arrMaxHeader"          => null,
						"arrMinHeader"          => null,
						"arrRange"     			=> null,
						"UCLR"                  => null,
						"LCLR"                  => null,
						"CLR"                   => null,
						"arrMax"                => null,
						"arrMin"                => null,
						"arrAverage"            => null,
						"UCLX"                  => null,
						"LCLX"                  => null,
						"CLX"                   => null,
						"UCLS"                  => null,
						"LCLS"                  => null,
						"arrLabel"              => null,
						"DateFrom" 				=> null,
						"DateTo" 				=> null,
						"arrItemName"           => null,
						"minCpk"         		=> null,
						"maxCpk"        	  	=> null,
						"totalsValue"    		=> null,
						"averageCpk"     		=> null,
						"stdevCpk"       		=> null,
						"specUpperCPK"   		=> null,
						"specLowerCPK"   		=> null,
						"Cp"             		=> null,
						"Cpu" 			 		=> null,
						"Cpl" 			 		=> null,
						"Cpk"  			 		=> null,
						"estimateStdev"  		=> null,
						"Pp" 			 		=> null,
						"Ppu"       	 		=> null,
						"Ppl"  			 		=> null,
						"Ppk"   	 	 		=> null,
						"arrBellCurve" 	 		=> null,
						"arrBarCPK"      		=> null,
						"arrLabelCPK"  	 		=> null,
						"arrIndexUSL"  	 		=> null,
						"arrIndexLSL"    		=> null,
						"arrBar"         		=> null,
						"arrStandard"           => null,
						"UCLStdev"              => null,
						"LCLStdev"              => null,
						"CLS"                   => null,
						"UCLXbarStd" 			=> null,
						"LCLXbarStd" 			=> null,
					];

				}else{ 

				 	try 
				    {
				    	$spreadsheet = new Spreadsheet();
				    	$lTemplateFileNameInspectionSheet = "DataOnGraphPageV1_template.xlsx";
						$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameInspectionSheet);

						$sheet = $spreadsheet->getActiveSheet();
						foreach ($lTblSearchResultData as $key => $value) {
				        	$cell = $key + 2;
				        	$sheet->getCell("A".$cell)->setValue($value->PRODUCT_NO);
				        	$sheet->getCell("B".$cell)->setValue($value->REV_NO);
				        	$sheet->getCell("C".$cell)->setValue($value->PROCESS_NAME);
				        	$sheet->getCell("D".$cell)->setValue($value->INSPECTION_YMD);
				        	$sheet->getCell("E".$cell)->setValue($value->INSPECTION_TIME_ID);
				        	$sheet->getCell("F".$cell)->setValue($value->INSPECTION_TIME_NAME);
				        	$sheet->getCell("G".$cell)->setValue($value->CONDITION_CD_NAME);
				        	$sheet->getCell("H".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("I".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("J".$cell)->setValue($value->TEAM_ID);
				        	$sheet->getCell("K".$cell)->setValue($value->TEAM_NAME);
				        	$sheet->getCell("M".$cell)->setValue($value->CAVITY_NO);
				        	$sheet->getCell("N".$cell)->setValue($value->WHERE_NO);
				        	$sheet->getCell("O".$cell)->setValue($value->PICTURE_URL);
				        	$sheet->getCell("P".$cell)->setValue($value->MIN_VALUE);
				        	$sheet->getCell("Q".$cell)->setValue($value->MAX_VALUE);
				        	$sheet->getCell("R".$cell)->setValue($value->AVERAGE_VALUE);
				        	$sheet->getCell("S".$cell)->setValue($value->RANGE_VALUE);
				        	$sheet->getCell("T".$cell)->setValue($value->STDEV);
				        	$sheet->getCell("U".$cell)->setValue($value->CPK);
				        	$sheet->getCell("V".$cell)->setValue($value->STD_MIN_SPEC);
				        	$sheet->getCell("W".$cell)->setValue($value->STD_MAX_SPEC);
				        	$sheet->getCell("X".$cell)->setValue($value->LOT_NO);
				        	$sheet->getCell("Y".$cell)->setValue($value->PRODUCTION_YMD);
						}

					    //set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
						$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet);

						$lSaveFilePathAndName = "excel/".$lSaveFileName;  

						//save file（save in server temporarily）
						$writer = new XlsxWriter($spreadsheet);
						//if many data exist,it need very long time for SAVE.(it is available to download directly without save
						$writer->save($lSaveFilePathAndName);

						//download
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Length: '.filesize($lSaveFilePathAndName));
						header('Content-disposition: attachment; filename='.$lSaveFileName);
						readfile($lSaveFilePathAndName);

						//delete fime(saved in server)
						unlink($lSaveFilePathAndName);

						//message
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";	
						    
				    }
				    catch(Exception $e) 
				    {
				        die('Error loading file "'.pathinfo($lSaveFilePathAndName,PATHINFO_BASENAME).'": '.$e->getMessage());
				    }	

					$lViewData += [  
						"arrCustomerID"         => null,
						"arrCustomerName"       => null,
						"arrProductNo"          => null,
						"arrProductName"        => null,
						"arrUpperSpec"	        => null,
						"arrLowerSpec"          => null,
						"arrCenterSpec"         => null,
						"arrRevNo1Dimention"    => null,
						"arrMoldNo1Dimention"   => null,
						"arrMachineNo"          => null,
						"arrPoint1Dimention" 	=> null,
						"arrCavity1Dimention" 	=> null,
						"arrWhere1Dimention" 	=> null,
						"arrAverageHeader"      => null,
						"arrMaxHeader"          => null,
						"arrMinHeader"          => null,
						"arrRange"     			=> null,
						"UCLR"                  => null,
						"LCLR"                  => null,
						"CLR"                   => null,
						"arrMax"                => null,
						"arrMin"                => null,
						"arrAverage"            => null,
						"UCLX"                  => null,
						"LCLX"                  => null,
						"CLX"                   => null,
						"UCLS"                  => null,
						"LCLS"                  => null,
						"arrLabel"              => null,
						"DateFrom" 				=> null,
						"DateTo" 				=> null,
						"arrItemName"           => null,
						"minCpk"         		=> null,
						"maxCpk"        	  	=> null,
						"totalsValue"    		=> null,
						"averageCpk"     		=> null,
						"stdevCpk"       		=> null,
						"specUpperCPK"   		=> null,
						"specLowerCPK"   		=> null,
						"Cp"             		=> null,
						"Cpu" 			 		=> null,
						"Cpl" 			 		=> null,
						"Cpk"  			 		=> null,
						"estimateStdev"  		=> null,
						"Pp" 			 		=> null,
						"Ppu"       	 		=> null,
						"Ppl"  			 		=> null,
						"Ppk"   	 	 		=> null,
						"arrBellCurve" 	 		=> null,
						"arrBarCPK"      		=> null,
						"arrLabelCPK"  	 		=> null,
						"arrIndexUSL"  	 		=> null,
						"arrIndexLSL"    		=> null,
						"arrBar"         		=> null,
						"arrStandard"           => null,
						"UCLStdev"              => null,
						"LCLStdev"              => null,
						"CLS"                   => null,
						"UCLXbarStd" 			=> null,
						"LCLXbarStd" 			=> null,
					];
				}
		}

	    if(Input::has('cmbProcessForGraph')){
	    	$lArrCustomerList = $this->setCustomerList($lViewData,Input::get('cmbProcessForGraph'));
	    	data_set($lViewData, 'arrCustomerList', $lArrCustomerList);
	    }

		if (Input::has('cmbProcessForGraph') and Input::has('cmbCustomerForGraph')){
			
			$lArrProductNoList = $this->setProductNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'));
			 data_set($lViewData, 'arrProductNoList', $lArrProductNoList);
		}

		if(Input::has('cmbCheckSheetNoForGraph')){
			$lArrRevNoList = $this->setRevNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'));
			data_set($lViewData, 'arrRevNoList', $lArrRevNoList);
		}

		if(Input::has('cmbRevisionNoForGraph')){
          
			$lArrMoldList = $this->setMoldList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrMoldNoList', $lArrMoldList);
        }

        if(Input::has('cmbMoldNoForGraph')){
			$lArrInspectionPointList = $this->setInspectionPointList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrInspectionPointList', $lArrInspectionPointList);

			$lArrCavityNoList = $this->setCavityNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbInspectionPointForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrCavityNoList', $lArrCavityNoList);

			$lArrTeamList = $this->setTeamList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrTeamNameList', $lArrTeamList);

			$lArrTimeIDList = $this->setTimeIDList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrTimeIDList', $lArrTimeIDList);

			$lArrConditionList = $this->setConditionList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrConditionList', $lArrConditionList);

			$lArrWhereNoList = $this->setWhereNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'), Input::get('cmbInspectionPointForGraph'), Input::get('cmbMoldNoForGraph'));
			data_set($lViewData, 'arrWhereNoList', $lArrWhereNoList);
		}

		return View("user.graph", $lViewData);
        
	}

	private function getInspectionDateFromToForGraph($pViewData)
	{  
		//Inspection Date
		if(Input::has('cmbRadioChooseInspectionDate')) {
			Session::put('BA1010RadioChooseInspectionDate', Input::get('cmbRadioChooseInspectionDate'));
		}else{
			Session::put('BA1010RadioChooseInspectionDate', "");
		}
        
		if (Input::has('txtInspectionDateFromForGraph')) {
			Session::put('BA1010InspectionDateFromForGraph', Input::get('txtInspectionDateFromForGraph'));
		}else{
			Session::put('BA1010InspectionDateFromForGraph', "");
		}

		if (Input::has('txtInspectionDateToForGraph')) {
			Session::put('BA1010InspectionDateToForGraph', Input::get('txtInspectionDateToForGraph'));
		}else{
			Session::put('BA1010InspectionDateToForGraph', "");
		}

        //Product
        if (Input::has('cmbRadioChooseProductionDate')) {
        	Session::put('BA1010RadioChooseProductionDate', Input::get('cmbRadioChooseProductionDate'));
        }else{
        	Session::put('BA1010RadioChooseProductionDate', "");
        }

		if (Input::has('txtProductionDateFromForGraph')) {
			Session::put('BA1010ProductionDateFromForGraph', Input::get('txtProductionDateFromForGraph'));
		}else{
			Session::put('BA1010ProductionDateFromForGraph', "");
		}

		if (Input::has('txtProductionDateToForGraph')) {
			Session::put('BA1010ProductionDateToForGraph', Input::get('txtProductionDateToForGraph'));
		}else{
			Session::put('BA1010ProductionDateToForGraph', date(""));
		}

		//Old days
		if (Input::has('cmbRadioChoosePeriod')) {
			Session::put('BA1010cmbRadioChoosePeriod', Input::get('cmbRadioChoosePeriod'));
		}else{
			Session::put('BA1010cmbRadioChoosePeriod', "");
		}

		if(Input::has('txtTypeTimeForGraph')){
			Session::put('BA1010TypeTimeForGraph', Input::get('txtTypeTimeForGraph'));
		}else{
			Session::put('BA1010TypeTimeForGraph', "");
		}

		$pViewData += [
			//Inspection Date
			"RadioChooseInspectionDate"  	=> Session::get('BA1010InspectionDateFromForGraph')
			,"InspectionDateFromForGraph"  	=> Session::get('BA1010InspectionDateFromForGraph')
			,"InspectionDateToForGraph"  	=> Session::get('BA1010InspectionDateToForGraph')
			//Product Date
			,"RadioChooseProductionDate" 	=> Session::get('BA1010RadioChooseProductionDate')
			,"ProductionDateFromForGraph"  	=> Session::get('BA1010ProductionDateFromForGraph')
			,"ProductionDateToForGraph"  	=> Session::get('BA1010ProductionDateToForGraph')
			//Old Days
			,"RadioChoosePeriod" 			=> Session::get('BA1010cmbRadioChoosePeriod')
			,"TypeTimeForGraph"  			=> Session::get('BA1010TypeTimeForGraph')
			
		];

		return $pViewData;
	}

	private function setProcessList($pViewData)
	{
		$lArrProcessList = ["" => ""];

		$objtcodemst = new tcodemst;
		$lArrProcessList = $objtcodemst->getProcessList();

		$pViewData += [
			"arrProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	private function setCustomerList($pViewData, $processID)
	{  
		$lArrObjCustomerList   = []; 
		$lArrCustomerList      = [];
		$lArrCustomerList['']    = "";
       
        if($processID != ""){
			$lArrObjCustomerList = DB::table('TISHEETM as TISHE')
			->select('TISHE.CUSTOMER_ID as CUSTOMER_ID', 'TCUST.CUSTOMER_NAME as CUSTOMER_NAME')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->join('TCODEMST as TCODE', function ($join) {
	        		$join->on('TISHE.PROCESS_ID', '=', 'TCODE.CODE_ORDER')
	        		     ->where('TCODE.CODE_CLASS', '=', "002");
	        	})
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->where('TISHE.PROCESS_ID', '=', $processID)
	        ->groupBy('TCUST.CUSTOMER_ID')
	        ->get();

			foreach ($lArrObjCustomerList as $key => $value) {
	           	$lArrCustomerList[$value->CUSTOMER_ID] = $value->CUSTOMER_NAME;
	        }
		}

		return $lArrCustomerList;
	}

	private function setCustomerListfromProcessName($pViewData, $processName)
	{ 
		$lArrObjCustomerList   = []; 
		$lArrCustomerList      = [];
		$lArrCustomerList['']    = "";

		$lArrObjCustomerList = DB::table('TISHEETM as TISHE')
		->select('TISHE.CUSTOMER_ID as CUSTOMER_ID', 'TCUST.CUSTOMER_NAME as CUSTOMER_NAME')
        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
        ->join('TRESANAL as TRESA', function ($join) {
	            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
	                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
	                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
	        })
        ->join('TCODEMST as TCODE', function ($join) {
        		$join->on('TISHE.PROCESS_ID', '=', 'TCODE.CODE_ORDER')
        		     ->where('TCODE.CODE_CLASS', '=', "002");
        	})
        ->where('TCUST.DELETE_FLG', '=', "0")
        ->where('TCODE.CODE_NAME', '=', $processName)
        ->groupBy('TCUST.CUSTOMER_ID')
        ->get();

		foreach ($lArrObjCustomerList as $key => $value) {
           	$lArrCustomerList[$value->CUSTOMER_ID] = $value->CUSTOMER_NAME;
        }

		$pViewData += [
			"arrCustomerList" => $lArrCustomerList
		];

		return $pViewData;
	}

	private function setProductNoList($pViewData, $processID, $customerID)
	{  
		$lArrObjProductNoList   = []; 
		$lArrProductNoList      = [];
		$lArrProductNoList['']    = "";
       
        if($processID != "" and $customerID != ""){
			$lArrObjProductNoList = DB::table('TISHEETM as TISHE')
			->select('TISHE.INSPECTION_SHEET_NO as PRODUCT_NO')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->where('TISHE.PROCESS_ID', $processID)
			->where('TISHE.CUSTOMER_ID', $customerID)
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->groupBy('TISHE.INSPECTION_SHEET_NO')
	        ->get();

	        foreach ($lArrObjProductNoList as $key => $value) {
	            $lArrProductNoList[$value->PRODUCT_NO] = $value->PRODUCT_NO;
	        }
        }
        return $lArrProductNoList;
	}

	private function setRevNoList($pViewData, $processID, $customerID, $checksheetNo)
	{  
		$lArrObjRevNoList   = []; 
		$lArrRevNoList      = [];
        $lArrRevNoList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != ""){
			$lArrObjRevNoList = DB::table('TISHEETM as TISHE')
			->select('TISHE.REV_NO as REV_NO')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->where('TISHE.PROCESS_ID', $processID)
			->where('TISHE.CUSTOMER_ID', $customerID)
			->where('TISHE.INSPECTION_SHEET_NO', $checksheetNo)
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->groupBy('TISHE.REV_NO')
	        ->get();
	        foreach ($lArrObjRevNoList as $key => $value) {
	           	$lArrRevNoList[$value->REV_NO] = $value->REV_NO;
	        }
	    }
		 return $lArrRevNoList;
	}

	private function setMoldList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjMoldNoList   = []; 
		$lArrMoldNoList      = [];
        $lArrMoldNoList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){
	        $lArrObjMoldNoList = DB::table('TRESANAL as TRESA')
				->select('TRESA.MOLD_NO')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.MOLD_NO')
				->get();    

            if(empty($lArrObjMoldNoList)){
	                $lArrMoldNoList[''] = "";
	        }else{
	        	foreach ($lArrObjMoldNoList as $key => $value) {
	           		$lArrMoldNoList[$value->MOLD_NO] = $value->MOLD_NO;
	        	}
	        }
	    }
	   
        return $lArrMoldNoList;
	}

	private function setInspectionPointList($pViewData, $processID, $customerID, $checksheetNo, $revNo ,$moldNo)
	{  
		$lArrObjInspectionPointList   = []; 
		$lArrInspectionPointList      = [];
        $lArrInspectionPointList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
		    $lArrObjInspectionPointList = DB::table('TRESANAL as TRESA')
				->select('TRESA.INSPECTION_POINT')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.INSPECTION_POINT')
				->get();     
	      
	        if(empty($lArrObjInspectionPointList)){
	                $lArrInspectionPointList[''] = "";
	        }else{
	        	foreach ($lArrObjInspectionPointList as $key => $value) {
	           		$lArrInspectionPointList[$value->INSPECTION_POINT] = $value->INSPECTION_POINT;
	        	}
	        }
	    }
        return $lArrInspectionPointList;
	}

	private function setCavityNoList($pViewData, $processID, $customerID, $checksheetNo, $revNo ,$inspectionPoint, $moldNo)
	{  
		$lArrObjCavityNoList   = []; 
		$lArrCavityNoList      = [];
        $lArrCavityNoList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
		    $lArrObjCavityNoList = DB::table('TRESANAL as TRESA')
				->select('TRESA.ANALYTICAL_GRP_01')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "004");
		        })
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.ANALYTICAL_GRP_01')
				->get();        
	      
	        if(empty($lArrObjCavityNoList)){
	                $lArrCavityNoList[''] = "";
	        }else{
	        	foreach ($lArrObjCavityNoList as $key => $value) {
	           		$lArrCavityNoList[$value->ANALYTICAL_GRP_01] = $value->ANALYTICAL_GRP_01;
	        	}
	        }
	    }
        return $lArrCavityNoList;
	}
     
	private function setTeamList($pViewData, $processID, $customerID, $checksheetNo, $revNo, $moldNo)
	{  
		$lArrObjTeamList   = []; 
		$lArrTeamList      = [];
        $lArrTeamList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
	        $lArrObjTeamList = DB::table('TRESANAL as TRESA')
				->select('TRESA.TEAM_ID', 'TCODE.CODE_NAME')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "004");
		        })
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.INSPECTION_POINT')
				->get();      

	        if(empty($lArrObjTeamList)){
	                $lArrTeamList[''] = "";
	        }else{
	        	foreach ($lArrObjTeamList as $key => $value) {
	           		$lArrTeamList[$value->TEAM_ID] = $value->CODE_NAME;
	        	}
	        }
	    }
        return $lArrTeamList;
	}
	 
	private function setTimeIDList($pViewData, $processID, $customerID, $checksheetNo, $revNo, $moldNo)
	{  
		$lArrObjTimeIDList   = []; 
		$lArrTimeIDList      = [];
        $lArrTimeIDList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
	        $lArrObjTimeIDList = DB::table('TRESANAL as TRESA')
				->select('TRESA.INSPECTION_TIME_ID', 'TIMEE.INSPECTION_TIME_NAME')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.INSPECTION_TIME_ID')
				->get();   
	      
	        if(empty($lArrObjTimeIDList)){
	                $lArrTimeIDList[''] = "";
	        }else{
	        	foreach ($lArrObjTimeIDList as $key => $value) {
	           		$lArrTimeIDList[$value->INSPECTION_TIME_ID] = $value->INSPECTION_TIME_NAME;
	        	}
	        }
	    }
        return $lArrTimeIDList;
	}

	private function setConditionList($pViewData, $processID, $customerID, $checksheetNo, $revNo, $moldNo)
	{  
		$lArrObjConditionList   = []; 
		$lArrConditionList      = [];
		$lArrConditionList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
	        $lArrObjConditionList = DB::table('TRESANAL as TRESA')
				->select('TRESA.CONDITION_CD', 'TCODE.CODE_NAME')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "001");
		        })
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.CONDITION_CD')
				->get();  
        
			if(empty($lArrObjConditionList)){
	                $lArrConditionList[''] = "";
	        }else{
	        	foreach ($lArrObjConditionList as $key => $value) {
	           		$lArrConditionList[$value->CONDITION_CD] = $value->CODE_NAME;
	        	}
	        }
	    }
        return $lArrConditionList;
	}

	private function setWhereNoList($pViewData, $processID, $customerID, $checksheetNo, $revNo ,$inspectionPoint, $moldNo)
	{  
		$lArrObjWhereNoList   = []; 
		$lArrWhereNoList      = [];
        $lArrWhereNoList['']    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != "" and $moldNo != ""){
		    $lArrObjWhereNoList = DB::table('TRESANAL as TRESA')
				->select('TRESA.ANALYTICAL_GRP_02')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "004");
		        })
				->where('TRESA.PROCESS_ID',$processID)
				->where('TRESA.INSPECTION_SHEET_NO',$checksheetNo)
				->where('TRESA.REV_NO',$revNo)
				->where('TISHE.CUSTOMER_ID',$customerID)
				->where('TRESA.MOLD_NO',$moldNo)
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.ANALYTICAL_GRP_02')
				->get();        

	        if(empty($lArrObjWhereNoList)){
	                $lArrWhereNoList[''] = "";
	        }else{
	        	foreach ($lArrObjWhereNoList as $key => $value) {
	           		$lArrWhereNoList[$value->ANALYTICAL_GRP_02] = $value->ANALYTICAL_GRP_02;
	        	}
	        }
	    }
        return $lArrWhereNoList;
	}

	private function getGraphResultList()
	{  
		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$lProductionDateFromForGraph    = "";
		$lProductionDateToForGraph      = "";


		if (Input::has('cmbRadioChoosePeriod')){
			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);
			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");


		}elseif(Input::has('cmbRadioChooseInspectionDate')){
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
		}else{
			$lProductionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
			$lProductionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
		}

		if(Input::get('cmbProcessForGraph') != "" AND Input::get('cmbCustomerForGraph') != "" AND Input::get('cmbCheckSheetNoForGraph') != "" AND  Input::get('cmbRevisionNoForGraph') != "" AND Input::get('cmbMoldNoForGraph') != ""){
				
        	if($lInspectionDateFromForGraph != "" and $lInspectionDateToForGraph != ""){
        		if($lInspectionDateFromForGraph != $lInspectionDateToForGraph){
        				$query = DB::table('TRESANAL as TRESA')
						->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
						        	,'TRESA.REV_NO as REV_NO'
						        	,'PROCESS.CODE_NAME as PROCESS_NAME'
						        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
						        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
						        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
						        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
						        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
						        	,'TRESA.MOLD_NO' 
									,'TRESA.MOLD_NO as MOLD_NAME'
									,'TRESA.TEAM_ID'
									,'TEAM.CODE_NAME as TEAM_NAME'
									,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
									,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
						 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
						 		    ,'TRESA.STD_MIN_SPEC'
						 		    ,'TRESA.STD_MAX_SPEC'							 
									,'TRESA.PICTURE_URL'
						 		   	,'TRESA.MIN_VALUE'
						 		   	,'TRESA.MAX_VALUE'
									,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
									,'TRESA.RANGE_VALUE as RANGE_VALUE'
									,'TRESA.STDEV'
									,'TRESA.CPK as CPK'
						 		   	,'TRESA.XBAR_UCL'									
						 		  	,'TRESA.XBAR_LCL'
						 		  	,'TRESA.RCHART_UCL'									
						 		  	,'TRESA.RCHART_LCL'
						 			,'TRESA.JUDGE_REASON'
						 			,'TRESH.LOT_NO'
						 			,'CUST.CUSTOMER_ID'
						 			,'CUST.CUSTOMER_NAME'
						 			, DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
						 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 			,'TRESH.MACHINE_NO'
						 			,'TITEM.ITEM_NAME'
						 		    )
						->join('TISHEETM as TISHE', function ($join) {
						    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
						        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
						})
						->join('TRESHEDT as TRESH', function ($join) {
							$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
								->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
								->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
						})
						->join('TCODEMST as PROCESS', function ($join) {
							$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
							     ->where('PROCESS.CODE_CLASS', '=', '002');
						})
						->join('TINSPTIM as TIMEE', function ($join) {
							$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
						})
						->join('TCODEMST as TEAM', function ($join) {
							$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
							     ->where('TEAM.CODE_CLASS', '=', '004');
						})
						->join('TCUSTOMM as CUST', function ($join) {
							$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
						})
						->join('TITEMMST as TITEM', function ($join) {
							$join->on('TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO');
						})
						->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
						->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
						->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
						->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
						->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
						->whereBetween('TRESA.INSPECTION_YMD', array($lInspectionDateFromForGraph, $lInspectionDateToForGraph));
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $query->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $query->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $query->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $query->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultData = $query->get();
        		}else{
        			$query = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TITEM.ITEM_NAME'
					 		)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->join('TITEMMST as TITEM', function ($join) {
						$join->on('TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
					->where('TRESA.INSPECTION_YMD',$lInspectionDateFromForGraph);
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $query->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $query->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $query->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $query->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultData = $query->get();
        		}
			}else{
				if($lProductionDateFromForGraph != $lProductionDateToForGraph){
					$query = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TITEM.ITEM_NAME'
					 			)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->join('TITEMMST as TITEM', function ($join) {
						$join->on('TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
					->whereBetween('TRESH.PRODUCTION_YMD', array($lProductionDateFromForGraph, $lProductionDateToForGraph));
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $query->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $query->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $query->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $query->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultData = $query->get();

				}else{
					$query = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'	
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TITEM.ITEM_NAME'
					 		)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->join('TITEMMST as TITEM', function ($join) {
						$join->on('TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO',Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESH.PRODUCTION_YMD', $lProductionDateFromForGraph);
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $query->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $query->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $query->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $query->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $query->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultData = $query->get();
				}
			}
        }
	    
		return $lTblSearchResultData;
	}

	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{

		//not to entry date
		if ($pTargetDate == "") {
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1") {
			return date_format($lWorkDate, 'Y-m-d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');

	}

	private function setListForms($pViewData)
	{

		if (Input::has('cmbProcessForGraph')) {
			Session::put('BA1010ProcessForGraph', Input::get('cmbProcessForGraph'));
		}
		else
		{
			Session::put('BA1010ProcessForGraph', "");
		}

		$pViewData += [
				"ProcessForGraph"  => Session::get('BA1010ProcessForGraph')
		];

		if (Input::has('cmbCustomerForGraph')) {
			Session::put('BA1010CustomerForGraph', Input::get('cmbCustomerForGraph'));
		}
		else 
		{
			Session::put('BA1010CustomerForGraph', "");
		}
		
		$pViewData += [
				"CustomerForGraph"  => Session::get('BA1010CustomerForGraph')
		];

		if (Input::has('cmbCheckSheetNoForGraph')) {
			Session::put('BA1010ProductNoForGraph', Input::get('cmbCheckSheetNoForGraph'));
		}
		else 
		{
			Session::put('BA1010ProductNoForGraph', "");
		}
		$pViewData += [
				"ProductNoForGraph"  => Session::get('BA1010ProductNoForGraph')
		];

		if (Input::has('cmbRevisionNoForGraph')) {
			Session::put('BA1010RevNoForGraph', Input::get('cmbRevisionNoForGraph'));
		}
		else 
		{
			Session::put('BA1010RevNoForGraph', "");
		}
		$pViewData += [
				"RevNoForGraph"  => Session::get('BA1010RevNoForGraph')
		];

		if (Input::has('cmbMoldNoForGraph')) {
			Session::put('BA1010MoldNoForGraph', Input::get('cmbMoldNoForGraph'));
		}
		else 
		{
			Session::put('BA1010MoldNoForGraph', "");
		}
		$pViewData += [
				"MoldNoForGraph"  => Session::get('BA1010MoldNoForGraph')
		];

		if (Input::has('cmbInspectionPointForGraph')) {
			Session::put('BA1010InspectionPointForGraph', Input::get('cmbInspectionPointForGraph'));
		}
		else 
		{
			Session::put('BA1010InspectionPointForGraph', "");
		}
		$pViewData += [
				"InspectionPointForGraph"  => Session::get('BA1010InspectionPointForGraph')
		];
		

		if (Input::has('cmbCavityForGraph')) {
			Session::put('BA1010CavityNoForGraph', Input::get('cmbCavityForGraph'));
		}
		else 
		{
			Session::put('BA1010CavityNoForGraph', "");
		}
		$pViewData += [
				"CavityNoForGraph"  => Session::get('BA1010CavityNoForGraph')
		];
		
		if (Input::has('cmbTeamNameForGraph')) {
			Session::put('BA1010TeamNoForGraph', Input::get('cmbTeamNameForGraph'));
		}
		else 
		{
			Session::put('BA1010TeamNoForGraph', "");
		}
		$pViewData += [
				"TeamNameForGraph"  => Session::get('BA1010TeamNoForGraph')
		];
		
        if(Input::has('cmbTimeIDForGraph')){
           Session::put('BA1010TimeIDForGraph', Input::get('cmbTimeIDForGraph'));
		}
		else 
		{
			Session::put('BA1010TimeIDForGraph', "");
		}
		$pViewData += [
				"TimeIDForGraph"  => Session::get('BA1010TimeIDForGraph')
		];
	
		if (Input::has('cmbConditionForGraph')) {
			Session::put('BA1010ConditionForGraph', Input::get('cmbConditionForGraph'));
		}
		else
		{
			Session::put('BA1010ConditionForGraph', "");
		}
		$pViewData += [
				"ConditionForGraph"  => Session::get('BA1010ConditionForGraph')
		];

		if (Input::has('cmbWhereForGraph')) {
			Session::put('BA1010WhereNoForGraph', Input::get('cmbWhereForGraph'));
		}
		else 
		{
			Session::put('BA1010WhereNoForGraph', "");
		}
		$pViewData += [
				"WhereNoForGraph"  => Session::get('BA1010WhereNoForGraph')
		];

		return $pViewData;
	}

	private function getBellCurve($STDEV_VALUE,$AVERAGE_VALUE){
		$Min6Sigma = $AVERAGE_VALUE - (6 * $STDEV_VALUE);
		$Max6Sigma = $AVERAGE_VALUE + (6 * $STDEV_VALUE);
		$Min5_5Sigma = $AVERAGE_VALUE - (5.5 * $STDEV_VALUE);
		$Max5_5Sigma = $AVERAGE_VALUE + (5.5 * $STDEV_VALUE);
		$Min5Sigma = $AVERAGE_VALUE - (5 * $STDEV_VALUE);
		$Max5Sigma = $AVERAGE_VALUE + (5 * $STDEV_VALUE);
		$Min4_5Sigma = $AVERAGE_VALUE - (4.5 * $STDEV_VALUE);
		$Max4_5Sigma = $AVERAGE_VALUE + (4.5 * $STDEV_VALUE);
		$Min4Sigma = $AVERAGE_VALUE - (4 * $STDEV_VALUE);
		$Max4Sigma = $AVERAGE_VALUE + (4 * $STDEV_VALUE);
		$Min3_5Sigma = $AVERAGE_VALUE - (3.5 * $STDEV_VALUE);
		$Max3_5Sigma = $AVERAGE_VALUE + (3.5 * $STDEV_VALUE);
		$Min3Sigma = $AVERAGE_VALUE - (3 * $STDEV_VALUE);
		$Max3Sigma = $AVERAGE_VALUE + (3 * $STDEV_VALUE);
		$Min2_5Sigma = $AVERAGE_VALUE - (2.5 * $STDEV_VALUE);
		$Max2_5Sigma = $AVERAGE_VALUE + (2.5 * $STDEV_VALUE);
		$Min2Sigma = $AVERAGE_VALUE - (2 * $STDEV_VALUE);
		$Max2Sigma = $AVERAGE_VALUE + (2 * $STDEV_VALUE);
		$Min1_5Sigma = $AVERAGE_VALUE - (1.5 * $STDEV_VALUE);
		$Max1_5Sigma = $AVERAGE_VALUE + (1.5 * $STDEV_VALUE);
		$MinSigma = $AVERAGE_VALUE - $STDEV_VALUE;
		$MaxSigma = $AVERAGE_VALUE + $STDEV_VALUE;
		$Min0_5Sigma = $AVERAGE_VALUE - (0.5 * $STDEV_VALUE);
		$Max0_5Sigma = $AVERAGE_VALUE + (0.5 * $STDEV_VALUE);
        
        $bar = 0.5;
        //-6sigma to < -5.5sigma
		for($i = $Min6Sigma; $i < $Min5_5Sigma; $i = $i + $bar){
			
			$valueX6[0] = $i;
			$valueY6[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine6Sigma = [];
		foreach($valueX6 as $key => $value) {
			    $valueLine6Sigma[$key] = [$valueX6[$key],$valueY6[$key]];
		}
         
		//-5.5sigma to < -5sigma
		for($i = $Min5_5Sigma; $i < $Min5Sigma; $i = $i + $bar){
			
			$valueX5_5[0] = $i;
			$valueY5_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine5_5Sigma = [];
		foreach($valueX5_5 as $key => $value) {
			    $valueLine5_5Sigma[$key] = [$valueX5_5[$key],$valueY5_5[$key]];
		}

		//-5sigma to < -4.5sigma
		for($i = $Min5Sigma; $i < $Min4_5Sigma; $i = $i + $bar){
			
			$valueX5[0] = $i;
			$valueY5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine5Sigma = [];
		foreach($valueX5 as $key => $value) {
			    $valueLine5Sigma[$key] = [$valueX5[$key],$valueY5[$key]];
		}

 		//-4.5sigma to < -4sigma
		for($i = $Min4_5Sigma; $i < $Min4Sigma; $i = $i + $bar){
			
			$valueX4_5[0] = $i;
			$valueY4_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine4_5Sigma = [];
		foreach($valueX4_5 as $key => $value) {
			    $valueLine4_5Sigma[$key] = [$valueX4_5[$key],$valueY4_5[$key]];
		}

 		//-4sigma to < -3.5sigma
		for($i = $Min4Sigma; $i < $Min3_5Sigma; $i = $i + $bar){
			
			$valueX4[0] = $i;
			$valueY4[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine4Sigma = [];
		foreach($valueX4 as $key => $value) {
			    $valueLine4Sigma[$key] = [$valueX4[$key],$valueY4[$key]];
		}

        //-3.5sigma to < -3sigma
		for($i = $Min3_5Sigma; $i < $Min3Sigma; $i = $i + $bar){
			
			$valueX3_5[0] = $i;
			$valueY3_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine3_5Sigma = [];
		foreach($valueX3_5 as $key => $value) {
			    $valueLine3_5Sigma[$key] = [$valueX3_5[$key],$valueY3_5[$key]];
		}
        
        //-3sigma to < -2.5sigma
        for($i = $Min3Sigma; $i < $Min2_5Sigma; $i = $i + $bar){
			
			$valueX3[0] = $i;
			$valueY3[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine3Sigma = [];
		foreach($valueX3 as $key => $value) {
			    $valueLine3Sigma[$key] = [$valueX3[$key],$valueY3[$key]];
		}

		//-2.5sigma to < -2sigma
 		for($i = $Min2_5Sigma; $i < $Min2Sigma; $i = $i + $bar){
			
			$valueX2_5[0] = $i;
			$valueY2_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine2_5Sigma = [];
		foreach($valueX2_5 as $key => $value) {
			    $valueLine2_5Sigma[$key] = [$valueX2_5[$key],$valueY2_5[$key]];
		}

		//-2sigma to < -1.5sigma
 		for($i = $Min2Sigma; $i < $Min1_5Sigma; $i = $i + $bar){
			
			$valueX2[0] = $i;
			$valueY2[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine2Sigma = [];
		foreach($valueX2 as $key => $value) {
			    $valueLine2Sigma[$key] = [$valueX2[$key],$valueY2[$key]];
		}

		//-1.5sigma to < -sigma
 		for($i = $Min1_5Sigma; $i < $MinSigma; $i = $i + $bar){
			
			$valueX1_5[0] = $i;
			$valueY1_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine1_5Sigma = [];
		foreach($valueX1_5 as $key => $value) {
			    $valueLine1_5Sigma[$key] = [$valueX1_5[$key],$valueY1_5[$key]];
		}

		//-sigma to < -0.5sigma to
		for($i = $MinSigma; $i < $Min0_5Sigma; $i = $i + $bar){
			
			$valueX[0] = $i;
			$valueY[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}

		$valueLineSigma = [];
		foreach($valueX as $key => $value) {
			    $valueLineSigma[$key] = [$valueX[$key],$valueY[$key]];
		}

		//-0.5sigma to < average
		for($i = $Min0_5Sigma; $i < $AVERAGE_VALUE; $i = $i + $bar){
			
			$valueX0_5[0] = $i;
			$valueY0_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}

		$valueLine0_5Sigma = [];
		foreach($valueX0_5 as $key => $value) {
			    $valueLine0_5Sigma[$key] = [$valueX0_5[$key],$valueY0_5[$key]];
		}

		//average
		$valueYAvergage = number_format($this->normal($AVERAGE_VALUE, $AVERAGE_VALUE, $STDEV_VALUE),3);
		$valueLineAvergage = array(
			  array($AVERAGE_VALUE,$valueYAvergage)
			  );

		//>average to +0.5sigma
		for($i = $Max0_5Sigma; $i > $AVERAGE_VALUE; $i -= $bar){
			
			$valueXRight0_5[0] = $i;
			$valueYRight0_5[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus0_5Sigma = [];
		foreach($valueXRight0_5 as $key => $value) {
			    $valueLinePlus0_5Sigma[$key] = [$valueXRight0_5[$key],$valueYRight0_5[$key]];
		}
		krsort($valueLinePlus0_5Sigma);

		//+0.5sigma to +sigma
		for($i = $MaxSigma; $i > $Max0_5Sigma; $i -= $bar){
			
			$valueXRight[0] = $i;
			$valueYRight[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlusSigma = [];
		foreach($valueXRight as $key => $value) {
			    $valueLinePlusSigma[$key] = [$valueXRight[$key],$valueYRight[$key]];
		}
		krsort($valueLinePlusSigma);

		//>+sigma to +1.5sigma
		for($i = $Max1_5Sigma; $i > $MaxSigma; $i -= $bar){
			
			$valueX1_5Right[0] = $i;
			$valueY1_5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus1_5Sigma = [];
		foreach($valueX1_5Right as $key => $value) {
			    $valueLinePlus1_5Sigma[$key] = [$valueX1_5Right[$key],$valueY1_5Right[$key]];
		}
		krsort($valueLinePlus1_5Sigma);

		//>+1.5sigma to +2sigma
		for($i = $Max2Sigma; $i > $Max1_5Sigma; $i -= $bar){
			
			$valueX2Right[0] = $i;
			$valueY2Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus2Sigma = [];
		foreach($valueX2Right as $key => $value) {
			    $valueLinePlus2Sigma[$key] = [$valueX2Right[$key],$valueY2Right[$key]];
		}
		krsort($valueLinePlus2Sigma);

		//>+2sigma to +2.5sigma
		for($i = $Max2_5Sigma; $i > $Max2Sigma; $i -= $bar){
			
			$valueX2_5Right[0] = $i;
			$valueY2_5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus2_5Sigma = [];
		foreach($valueX2_5Right as $key => $value) {
			    $valueLinePlus2_5Sigma[$key] = [$valueX2_5Right[$key],$valueY2_5Right[$key]];
		}
		krsort($valueLinePlus2_5Sigma);

		//>+2.5sigma to +3sigma
		for($i = $Max3Sigma; $i > $Max2_5Sigma; $i -= $bar){
			
			$valueX3Right[0] = $i;
			$valueY3Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus3Sigma = [];
		foreach($valueX3Right as $key => $value) {
			    $valueLinePlus3Sigma[$key] = [$valueX3Right[$key],$valueY3Right[$key]];
		}
		krsort($valueLinePlus3Sigma);

		//>+3sigma to +3.5sigma
		for($i = $Max3_5Sigma; $i > $Max3Sigma; $i -= $bar){
			
			$valueX3_5Right[0] = $i;
			$valueY3_5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus3_5Sigma = [];

		foreach($valueX3_5Right as $key => $value) {
			    $valueLinePlus3_5Sigma[$key] = [$valueX3_5Right[$key],$valueY3_5Right[$key]];
		}
		krsort($valueLinePlus3_5Sigma);

		//>+3.5sigma to +4sigma
		for($i = $Max4Sigma; $i > $Max3_5Sigma; $i -= $bar){
			
			$valueX4Right[0] = $i;
			$valueY4Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus4Sigma = [];

		foreach($valueX4Right as $key => $value) {
			    $valueLinePlus4Sigma[$key] = [$valueX4Right[$key],$valueY4Right[$key]];
		}
		krsort($valueLinePlus4Sigma);

		//>+4sigma to +4.5sigma
		for($i = $Max4_5Sigma; $i > $Max4Sigma; $i -= $bar){
			
			$valueX4_5Right[0] = $i;
			$valueY4_5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus4_5Sigma = [];

		foreach($valueX4_5Right as $key => $value) {
			    $valueLinePlus4_5Sigma[$key] = [$valueX4_5Right[$key],$valueY4_5Right[$key]];
		}
		krsort($valueLinePlus4_5Sigma);

		//>+4.5sigma to +5sigma
		for($i = $Max5Sigma; $i > $Max4_5Sigma; $i -= $bar){
			
			$valueX5Right[0] = $i;
			$valueY5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus5Sigma = [];

		foreach($valueX5Right as $key => $value) {
			    $valueLinePlus5Sigma[$key] = [$valueX5Right[$key],$valueY5Right[$key]];
		}
		krsort($valueLinePlus5Sigma);

		//>+5sigma to +5.5sigma
		for($i = $Max5Sigma; $i > $Max4_5Sigma; $i -= $bar){
			
			$valueX5_5Right[0] = $i;
			$valueY5_5Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus5_5Sigma = [];

		foreach($valueX5_5Right as $key => $value) {
			    $valueLinePlus5_5Sigma[$key] = [$valueX5_5Right[$key],$valueY5_5Right[$key]];
		}
		krsort($valueLinePlus5_5Sigma);

		//>+5.5sigma to +6sigma
		for($i = $Max5_5Sigma; $i > $Max5Sigma; $i -= $bar){
			
			$valueX6Right[0] = $i;
			$valueY6Right[0] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus6Sigma = [];

		foreach($valueX6Right as $key => $value) {
			    $valueLinePlus6Sigma[$key] = [$valueX6Right[$key],$valueY6Right[$key]];
		}
		krsort($valueLinePlus6Sigma);

		$valueLine = array_merge($valueLine6Sigma, $valueLine5_5Sigma, $valueLine5Sigma, $valueLine4_5Sigma, $valueLine4Sigma, $valueLine3_5Sigma, $valueLine3Sigma, $valueLine2_5Sigma, $valueLine2Sigma, $valueLine1_5Sigma, $valueLineSigma, $valueLine0_5Sigma, $valueLineAvergage,$valueLinePlus0_5Sigma, $valueLinePlusSigma, $valueLinePlus1_5Sigma, $valueLinePlus2Sigma, $valueLinePlus2_5Sigma, $valueLinePlus3Sigma, $valueLinePlus3_5Sigma, $valueLinePlus4Sigma, $valueLinePlus4_5Sigma, $valueLinePlus5Sigma, $valueLinePlus5_5Sigma, $valueLinePlus6Sigma);  
		 return $valueLine;
	}

	private function normal($x, $mu, $sigma) {
	    return exp(-0.5 * ($x - $mu) * ($x - $mu) / ($sigma*$sigma))
	        / ($sigma * sqrt(2.0 * M_PI));
	}

	private function fourDimentionToTwoDimention($fourDimention){

		foreach ($fourDimention as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$fourDimentionFillPoint[$key][$key1][$key2][0] = number_format($value3,4);
						$fourDimentionFillPoint[$key][$key1][$key2][] = number_format($value3,4);
					}
				$fourDimentionFillPoint[$key][$key1][$key2][] = number_format($value3,4);
				}
			}
		}

  		foreach ($fourDimentionFillPoint as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {

					$arrTwoDimention[] = $value2;
				}
			}
		}
		return $arrTwoDimention;
	}
    
    private function getArrPointOnLineChart($arrValue){
    	foreach ($arrValue as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrPoint[] = $value2;
				}
			}
		}
		foreach ($arrPoint as $key => $value) {
			foreach ($value as $k => $val) {
				$arrPointOnLineChart[$key][0] = null;
				$arrPointOnLineChart[$key][] = $val;
			}
			$arrPointOnLineChart[$key][] = null;
		}
        return $arrPointOnLineChart;
    }

	private function arrLimitUCL($coefficient, $averageOfChart){
       
	    foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
								$arrUCL[$key][$key1][$key2][$key3] = (double)$value3 * $averageOfChart[$key][$key1][$key2][$key3];
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrLimitLCL($coefficient, $averageOfChart){
       
	    foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}
        
        foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrUCL[$key][$key1][$key2][$key3] = (double)$value3 * $averageOfChart[$key][$key1][$key2][$key3];
					}
				}
			}
		}

        return $arrUCL;
	}

	private function coefficientObject($CODE_KIND, $arrayValue){
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$coefficientObject[$key][$key1][$key2][$key3] = (array)$this->getCoefficient($CODE_KIND, count($value2));
					}
				}
			}
		}

		return $coefficientObject;
	}

	private function getCoefficient($CODE_KIND, $CODE_NUM) {
		if( $CODE_NUM > 25 ){ $CODE_NUM = 25;}
		if( $CODE_NUM < 2 ) { $CODE_NUM = 1; }
		$coefficient = DB::table('tccientmst')
		->select('COEFFICIENT AS 0')
		->where('COEFF_KIND', '=', $CODE_KIND)
		->where('COEFF_NUM', '=', $CODE_NUM)
		->first();
        
		return $coefficient;
	}

	private function arrAverageValue($arrayValue){
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrAverageValue[$key][$key1][$key2][$key3] = array_sum($value2) / count($value2);
					}
				}
			}
		}
		return $arrAverageValue;
	}



	private function arrLimitUCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrUCL[$key][$key1][$key2][$key3] = ((double)$value3 * $averageRange[$key][$key1][$key2][$key3]) + $averageAverage[$key][$key1][$key2][$key3];
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrLimitLCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrUCL[$key][$key1][$key2][$key3] =  $averageAverage[$key][$key1][$key2][$key3] - ((double)$value3 * $averageRange[$key][$key1][$key2][$key3]);
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrThreeToOneDimention($arrThreeDimentionValue){

		foreach ($arrThreeDimentionValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						if($key3 == 0){
							$arrOneDimentionValue[] = $value3;
						}
					}							
				}
			}
		}
        return $arrOneDimentionValue;
        
	}

	private function getLabelForMerge($valueLabel, $name){

		foreach ($valueLabel as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrForGroup[] = $value2;
				}
			}
		}
		foreach ($arrForGroup as $key => $value) {
			$arrForGraph[$key][0] = $name;
			foreach ($value as $k => $val) {
				$arrForGraph[$key][] = $val;
			}
			$arrForGraph[$key][] = "";
		}
		foreach ($arrForGraph as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$arrForMerge["group".$key]["graph".$key]["key".$key1] = [$value1];
			}
		}
        return $arrForMerge;

	}

	private function getGraphCPK(){
		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$lProductionDateFromForGraph    = "";
		$lProductionDateToForGraph      = "";


		if (Input::has('cmbRadioChoosePeriod')){
			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);
			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");


		}elseif(Input::has('cmbRadioChooseInspectionDate')){
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
		}else{
			$lProductionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
			$lProductionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
		}

		if(Input::get('cmbProcessForGraph') != "" AND Input::get('cmbCustomerForGraph') != "" AND Input::get('cmbCheckSheetNoForGraph') != "" AND  Input::get('cmbRevisionNoForGraph') != "" AND Input::get('cmbMoldNoForGraph') != ""){
				
        	if($lInspectionDateFromForGraph != "" and $lInspectionDateToForGraph != ""){
        		if($lInspectionDateFromForGraph != $lInspectionDateToForGraph){
        			
						$CPK = DB::table('TRESANAL as TRESA')
						->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
						        	,'TRESA.REV_NO as REV_NO'
						        	,'PROCESS.CODE_NAME as PROCESS_NAME'
						        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
						        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
						        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
						        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
						        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
						        	,'TRESA.MOLD_NO' 
									,'TRESA.MOLD_NO as MOLD_NAME'
									,'TRESA.TEAM_ID'
									,'TEAM.CODE_NAME as TEAM_NAME'
									,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
									,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
						 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
						 		    ,'TRESA.STD_MIN_SPEC'
						 		    ,'TRESA.STD_MAX_SPEC'							 
									,'TRESA.PICTURE_URL'
						 		   	,'TRESA.MIN_VALUE'
						 		   	,'TRESA.MAX_VALUE'
									,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
									,'TRESA.RANGE_VALUE as RANGE_VALUE'
									,'TRESA.STDEV'
									,'TRESA.CPK as CPK'
						 		   	,'TRESA.XBAR_UCL'									
						 		  	,'TRESA.XBAR_LCL'
						 		  	,'TRESA.RCHART_UCL'									
						 		  	,'TRESA.RCHART_LCL'
						 			,'TRESA.JUDGE_REASON'
						 			,'TRESH.LOT_NO'
						 			,'CUST.CUSTOMER_ID'
						 			,'CUST.CUSTOMER_NAME'
						 			, DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
						 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 			,'TRESH.MACHINE_NO'
						 		    ,'TRESD.INSPECTION_RESULT_VALUE')
						->join('TISHEETM as TISHE', function ($join) {
						    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
						        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
						})
						->join('TRESHEDT as TRESH', function ($join) {
							$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
								->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
								->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
						})
						->join('TCODEMST as PROCESS', function ($join) {
							$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
							     ->where('PROCESS.CODE_CLASS', '=', '002');
						})
						->join('TINSPTIM as TIMEE', function ($join) {
							$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
						})
						->join('TCODEMST as TEAM', function ($join) {
							$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
							     ->where('TEAM.CODE_CLASS', '=', '004');
						})
						->join('TCUSTOMM as CUST', function ($join) {
							$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
						})
						->join('TRESDETT as TRESD', function ($join) {
							$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESD.INSPECTION_RESULT_NO')
							     ->on('TRESA.INSPECTION_POINT', '=', 'TRESD.INSPECTION_POINT')
							     ->on('TRESA.ANALYTICAL_GRP_01', '=', 'TRESD.ANALYTICAL_GRP_01')
							     ->on('TRESA.ANALYTICAL_GRP_02', '=', 'TRESD.ANALYTICAL_GRP_02')
							     ->where('TRESD.INSPECTION_RESULT_VALUE', '!=', "");
						})
						->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
						->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
						->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
						->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
						->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
						->whereBetween('TRESA.INSPECTION_YMD', array($lInspectionDateFromForGraph, $lInspectionDateToForGraph));
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $CPK->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $CPK->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $CPK->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $CPK->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultCPK = $CPK->get();
        		}else{

        			$CPK = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TRESD.INSPECTION_RESULT_VALUE'
					 		)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->join('TRESDETT as TRESD', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESD.INSPECTION_RESULT_NO')
							 ->on('TRESA.INSPECTION_POINT', '=', 'TRESD.INSPECTION_POINT')
							 ->on('TRESA.ANALYTICAL_GRP_01', '=', 'TRESD.ANALYTICAL_GRP_01')
							 ->on('TRESA.ANALYTICAL_GRP_02', '=', 'TRESD.ANALYTICAL_GRP_02')
							 ->where('TRESD.INSPECTION_RESULT_VALUE', '!=', "");
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
					->where('TRESA.INSPECTION_YMD',$lInspectionDateFromForGraph);
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $CPK->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $CPK->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $CPK->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $CPK->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}
						$lTblSearchResultCPK = $CPK->get();
        		}
			}else{
				if($lProductionDateFromForGraph != $lProductionDateToForGraph){

					$CPK = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TRESD.INSPECTION_RESULT_VALUE'
					 			)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TRESDETT as TRESD', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESD.INSPECTION_RESULT_NO')
							 ->on('TRESA.INSPECTION_POINT', '=', 'TRESD.INSPECTION_POINT')
							 ->on('TRESA.ANALYTICAL_GRP_01', '=', 'TRESD.ANALYTICAL_GRP_01')
							 ->on('TRESA.ANALYTICAL_GRP_02', '=', 'TRESD.ANALYTICAL_GRP_02')
							 ->where('TRESD.INSPECTION_RESULT_VALUE', '!=', "");
					})					
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO', 'like', Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESA.MOLD_NO',Input::get('cmbMoldNoForGraph'))
					->whereBetween('TRESH.PRODUCTION_YMD', array($lProductionDateFromForGraph, $lProductionDateToForGraph));
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $CPK->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $CPK->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $CPK->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $CPK->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}

						$lTblSearchResultCPK = $CPK->get();

				}else{

					$CPK = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			,'TISHE.CUSTOMER_ID'
						 		,'CUST.CUSTOMER_NAME'	
					 			,DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
					 			,'TISHE.INSPECTION_SHEET_NAME as PRODUCT_NAME'
						 		,'TRESH.MACHINE_NO'
						 		,'TRESD.INSPECTION_RESULT_VALUE'
					 		)
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->join('TCUSTOMM as CUST', function ($join) {
						$join->on('TISHE.CUSTOMER_ID', '=', 'CUST.CUSTOMER_ID');
					})
					->join('TRESDETT as TRESD', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESD.INSPECTION_RESULT_NO')
							 ->on('TRESA.INSPECTION_POINT', '=', 'TRESD.INSPECTION_POINT')
							 ->on('TRESA.ANALYTICAL_GRP_01', '=', 'TRESD.ANALYTICAL_GRP_01')
							 ->on('TRESA.ANALYTICAL_GRP_02', '=', 'TRESD.ANALYTICAL_GRP_02')
							 ->where('TRESD.INSPECTION_RESULT_VALUE', '!=', "");
					})					
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO',Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESH.PRODUCTION_YMD', $lProductionDateFromForGraph);
						if(!empty(Input::get('cmbInspectionPointForGraph'))){
						    $CPK->where('TRESA.INSPECTION_POINT', Input::get('cmbInspectionPointForGraph'));
						}
						if(!empty(Input::get('cmbCavityForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_01', Input::get('cmbCavityForGraph'));
						}
						if(!empty(Input::get('cmbTeamNameForGraph'))){
						    $CPK->where('TRESA.TEAM_ID', Input::get('cmbTeamNameForGraph'));
						}
						if(!empty(Input::get('cmbTimeIDForGraph'))){
						    $CPK->where('TIMEE.INSPECTION_TIME_ID', Input::get('cmbTimeIDForGraph'));
						}
						if(!empty(Input::get('cmbConditionForGraph'))){
						    $CPK->where('TRESA.CONDITION_CD', Input::get('cmbConditionForGraph'));
						}
						if(!empty(Input::get('cmbWhereForGraph'))){
						    $CPK->where('TRESA.ANALYTICAL_GRP_02', Input::get('cmbWhereForGraph'));
						}

						$lTblSearchResultCPK = $CPK->get();
				}
			}
        }
	    
		return $lTblSearchResultCPK;
	}

	private function standard_deviation($aValues, $bSample = false)
	{
	    $fMean = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}

	private function estimate_standard_deviation($aValues)
	{
	    $fMean = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);

	    }       
	    $size = count($aValues) - 1;
	    return (float) sqrt($fVariance)/sqrt($size);
	}

}	