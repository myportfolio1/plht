<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Model\tcodemst;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use DB;
use Session;
use Carbon\Carbon;
use PHPExcel; 
use PHPExcel_IOFactory; 

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Exception;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');

//**************************************************************************
// display:	 Graph
// overview: For Plasess Company
// author:	Mai Nawaphat(Mai ^^)
// date:	14/2/2018
//**************************************************************************
class BA4010GraphController
extends Controller
{  
	public function MasterAction()
	{  
		$lViewData						= []; //Array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

        $lArrProductNoList[] = "";
		$lArrRevNoList[] = "";
        $lArrMoldNoList[] = "";
        $lArrCavityNoList[] = "";
        $lArrInspectionPointList[] = "";
        $lArrTimeIDList[] = "";
        $lArrTeamNameList[] = "";
        $lArrProcessList[] = "";
        $lArrCustomerList[] = "";
		$lViewData += $this->setProcessList($lViewData);
		
		if(Input::has('cmbProcessForGraph')){
            $lViewData += $this->setCustomerList($lViewData, Input::get('cmbProcessForGraph'));
		}
		else{
			$processName = data_get($lViewData, 'arrProcessList.01');
			$lViewData += $this->setCustomerListfromProcessName($lViewData, $processName);
		}
		
        $lViewData += [
        	"arrProductNoList" => $lArrProductNoList,
			"arrRevNoList" => $lArrRevNoList,
			"arrMoldNoList" => $lArrMoldNoList,
			"arrCavityNoList" => $lArrCavityNoList,
			"arrInspectionPointList" => $lArrInspectionPointList,
			"arrTimeIDList" => $lArrTimeIDList,
			"arrTeamNameList" => $lArrTeamNameList,
			"arrProcessList" => $lArrProcessList,
			"arrCustomerList" => $lArrCustomerList,
			"arrConditionList" => $lArrConditionList,
		];

		$lViewData += $this->getInspectionDateFromToForGraph($lViewData);
		// $lViewData += $this->getConditionList($lViewData);
		$lViewData += $this->setListForms($lViewData);

		$lViewData += [
				"SelectTypeData" => Input::get('cmbSelectTypeForGraph'),
		];
		if(Input::has('btnGraph') and Input::get('cmbSelectTypeForGraph') != ""){

			 	$lTblSearchResultData = $this->getGraphResultList();
			 	if (count($lTblSearchResultData) == 0)
				{ 
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
										$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"LCLStdev"              => null,
										"CLS"                   => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
										"arrMaxXbarChart"       => null,
										"arrMinXbarChart"       => null,
										"arrMaxRangeChart"      => null,
										"arrMinRangeChart"      => null,
										"arrMaxStdevChart"      => null,
										"arrMinStdevChart"      => null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexLCL"              => null,
										"indexUCL"              => null,
					];

				}else{	
					foreach ($lTblSearchResultData as $key => $value) 
					{  if($value->WHERE_NO == ""){
							$value->WHERE_NO = 0;
						}
						//Title For Average, Range, Xbar Or Header Chart				
						$arrPoint[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_POINT;
						$arrCavity[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CAVITY_NO;
						$arrWhere[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->WHERE_NO;
						// //Range 
						$arrRange[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->RANGE_VALUE;
						// //Average
						$arrAverage[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->AVERAGE_VALUE;
						$arrMax[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MAX_VALUE;
						$arrMin[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MIN_VALUE;
						  //Stdev
					    $arrStandard[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STDEV;
					    // //Label			
					    $arrLotNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->LOT_NO;
						$arrProcessName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PROCESS_NAME;
						$arrTeamName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->TEAM_NAME;
						$arrConditionName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CONDITION_CD;
						$arrInspection[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_YMD;
						$arrTime[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_TIME_NAME;
						$arrAverageUCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_UCL;
						$arrAverageLCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_LCL;

					}

					//Title For Average, Range, Xbar Chart	Or Header Chart
					$arrPoint1Dimention = $this->arrThreeToOneDimention($arrPoint);		
					$arrCavity1Dimention = $this->arrThreeToOneDimention($arrCavity);	
					$arrWhere1Dimention = $this->arrThreeToOneDimention($arrWhere);	

					//Range  Chart
                    $arrRangeForGraph = $this->getArrPointOnLineChart($arrRange);
                    $coefficientUCLRObject = $this->coefficientObject("D4", $arrRange);
                    $averageRange = $this->arrAverageValue($arrRange);
                    $coefficientLCLRObject = $this->coefficientObject("D3", $arrStandard);

                    $UCLR = $this->arrLimitUCLOrLCL($coefficientUCLRObject, $averageRange);
     //                $UCLRUpper = $this->arrMaxOfChart($coefficientUCLRObject, $averageRange);
                    $arrUCLRFillPointForGroup = $this->fourDimentionToTwoDimention($UCLR);
				 //    $arrUCLRUpper = $this->fourDimentionToTwoDimention($UCLRUpper);

                    $LCLR = $this->arrLimitUCLOrLCL($coefficientLCLRObject, $averageRange);
     //                $LCLRLower = $this->arrMinOfChart($coefficientLCLRObject, $averageRange);
                    $arrLCLRFillPointForGroup = $this->fourDimentionToTwoDimention($LCLR);
				 //    $arrLCLRLower = $this->fourDimentionToTwoDimention($LCLRLower);

				    $arrCLR = $this->fourDimentionToTwoDimention($averageRange);

                    //Xbar Chart
                    $arrAverageForGraph = $this->getArrPointOnLineChart($arrAverage);
                    $arrMaxForGraph = $this->getArrPointOnLineChart($arrMax);
                    $arrMinForGraph = $this->getArrPointOnLineChart($arrMin);

				 //    	//UCL and LCL of Xbar
				    $averageAverage = $this->arrAverageValue($arrAverage);
                    $coefficientXObject = $this->coefficientObject("A2", $arrAverage);
                    // echo "<pre>test "; print_r($coefficientXObject); echo "</pre>";

                    $UCLX = $this->arrLimitUCLXbar($coefficientXObject, $averageRange, $averageAverage);
                     // echo "<pre>"; print_r($UCLX); echo "</pre>";
     //                $UCLXUpper = $this->arrMaxOfXbarChart($coefficientXObject, $averageRange, $averageAverage); 
                    $arrUCLXFillPointForGroup = $this->fourDimentionToTwoDimention($UCLX);
				 //    $arrUCLXUpper = $this->fourDimentionToTwoDimention($UCLXUpper);
				    $LCLX = $this->arrLimitLCLXbar($coefficientXObject, $averageRange, $averageAverage);
     //                $LCLXLower = $this->arrMinOfXbarChart($coefficientXObject, $averageRange, $averageAverage);
                    $arrLCLXFillPointForGroup = $this->fourDimentionToTwoDimention($LCLX);
				 //    $arrLCLXLower = $this->fourDimentionToTwoDimention($LCLXLower);
				 //     //CL of Xbar and Stdev
                    $arrCLX = $this->fourDimentionToTwoDimention($averageAverage);

     //                   //UCL and LCL of Stdev

                    $averageStdev = $this->arrAverageValue($arrStandard);
                    $arrCLS = $this->fourDimentionToTwoDimention($averageStdev);

                    $coefficientSObject = $this->coefficientObject("A3", $averageStdev);
                    $UCLS = $this->arrLimitUCLXbar($coefficientSObject, $averageStdev, $averageAverage);
     //                $UCLSUpper = $this->arrMaxOfXbarChart($coefficientSObject, $averageStdev, $averageAverage);
                    $arrUCLSFillPointForGroup = $this->fourDimentionToTwoDimention($UCLS);
				 //    $arrUCLSUpper = $this->fourDimentionToTwoDimention($UCLSUpper);

                    $coefficientSLObject = $this->coefficientObject("A2", $averageStdev);
				    $LCLS = $this->arrLimitLCLXbar($coefficientSLObject, $averageStdev, $averageAverage);
     //                $LCLSLower = $this->arrMinOfXbarChart($coefficientSLObject, $averageStdev, $averageAverage);
                    $arrLCLSFillPointForGroup = $this->fourDimentionToTwoDimention($LCLS);
				 //    $arrLCLSLower = $this->fourDimentionToTwoDimention($LCLSLower);

     //                //Stdev Chart
				    $arrStandardForGraph = $this->getArrPointOnLineChart($arrStandard);
				 //    // dd($USL);
                    $coefficientUCLStdevObject = $this->coefficientObject("B4", $arrStandard);
                    $coefficientLCLStdevObject = $this->coefficientObject("B3", $arrStandard);
                    $averageStdevForUCL = $this->arrAverageValue($arrStandard);
                    $UCLStdev = $this->arrLimitUCLOrLCL($coefficientUCLStdevObject, $averageStdevForUCL);
                    $LCLStdev = $this->arrLimitUCLOrLCL($coefficientLCLStdevObject, $averageStdevForUCL);
				    $arrUCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($UCLStdev);
				    $arrLCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($LCLStdev);

					//Start Label for Average, Range, Xbar Chart
					$arrLotNoForMerge = $this->getLabelForMerge($arrLotNo, "Lot No");
					// $arrProcessNameForMerge = $this->getLabelForMerge($arrProcessName, "Process Name");
					$arrTeamNameForMerge = $this->getLabelForMerge($arrTeamName, "Team Name");
					$arrConditionForMerge = $this->getLabelForMerge($arrConditionName, "Condition Name");
					$arrInspectionForMerge = $this->getLabelForMerge($arrInspection, "Inspection Date");
					$arrTimeForMerge = $this->getLabelForMerge($arrTime, "Time");
					// $mergeLabel = array_merge_recursive($arrLotNoForMerge, $arrProcessNameForMerge,$arrTeamNameForMerge, $arrConditionForMerge, $arrInspectionForMerge, $arrTimeForMerge);
					$mergeLabel = array_merge_recursive($arrLotNoForMerge, $arrTeamNameForMerge, $arrConditionForMerge, $arrInspectionForMerge, $arrTimeForMerge);
					foreach ($mergeLabel as $key => $value) {
						foreach ($value as $key1 => $value1) {
							foreach ($value1 as $key2 => $value2) {
								$lebel1Dimention[$key1][] = $value2;
							}
						}
					}

					$arrLabel = array_values($lebel1Dimention);

					$lViewData += [     
										"arrRange"     			=> $arrRangeForGraph,
										"UCLR"                  => $arrUCLRFillPointForGroup,
										// "UCLRUpper"             => $arrUCLRUpper,
										"LCLR"                  => $arrLCLRFillPointForGroup,
										// "LCLRLower"             => $arrLCLRLower,
										"CLR"                   => $arrCLR,
										"arrMax"                => $arrMaxForGraph,
										"arrMin"                => $arrMinForGraph,
										"arrAverage"            => $arrAverageForGraph,
										"UCLX"                  => $arrUCLXFillPointForGroup,
										// "UCLXUpper"             => $arrUCLXUpper,
										"LCLX"                  => $arrLCLXFillPointForGroup,
										// "LCLXLower"             => $arrLCLXLower,
										"CLX"                   => $arrCLX,
										"UCLS"                  => $arrUCLSFillPointForGroup,
										// "UCLSUpper"             => $arrUCLSUpper,
										"LCLS"                  => $arrLCLSFillPointForGroup,
										// "LCLSLower"             => $arrLCLSLower,
										"arrStandard"           => $arrStandardForGraph,
										"UCLStdev"              => $arrUCLStdevFillPointForGroup,
										"LCLStdev"              => $arrLCLStdevFillPointForGroup,
										"CLS"                   => $arrCLS,
										"arrLabel"              => $arrLabel,
										"arrPoint1Dimention" 	=> $arrPoint1Dimention,
										"arrCavity1Dimention" 	=> $arrCavity1Dimention,
										"arrWhere1Dimention" 	=> $arrWhere1Dimention,
										// "arrBarCPK"    			=> $arrBarCPK,
										// "arrLabelCPK"    		=> $arrLabelCPK,
										// "arrCurve"              => $arrCurve,
										// "arrCurveValue"         => $arrCurveValue,
										// "arrMinutOneSigma"      => $arrMinutOneSigma,
										// "arrMinutTwoSigma"      => $arrMinutTwoSigma,
										// "arrMinutThreeSigma"    => $arrMinutThreeSigma,
										// "arrMinutFourSigma"     => $arrMinutFourSigma,
										// "arrPlusOneSigma"       => $arrPlusOneSigma,
										// "arrPlusTwoSigma"       => $arrPlusTwoSigma,
										// "arrPlusThreeSigma"     => $arrPlusThreeSigma,
										// "arrPlusFourSigma"      => $arrPlusFourSigma,
										// "arrAvg"				=> $arrAvg,
										// "indexMax"              => $indexMax,
										// "indexMin"              => $indexMin,
										// "arrAverageUCLCPK"      => $arrAverageUCLCPK,
										// "arrAverageLCLCPK"      => $arrAverageLCLCPK,
										// "arrPointCPK"			=> $arrPointCPK,
										// "arrCavityCPK"			=> $arrCavityCPK,
										// "arrWhereCPK"			=> $arrWhereCPK,
										// "arrStdev"				=> $arrStdev,
										// "arrMaxXbarChart"       => $arrMaxXbarChart,
										// "arrMinXbarChart"       => $arrMinXbarChart,
										// "arrMaxRangeChart"      => $arrMaxRangeChart,
										// "arrMinRangeChart"      => $arrMinRangeChart,
										// "arrMaxStdevChart"      => $arrMaxStdevChart,
										// "arrMinStdevChart"      => $arrMinStdevChart,
										// "indexMinutOneSigma"    => $indexMinutOneSigma,
										// "indexMinutTwoSigma"    => $indexMinutTwoSigma,
										// "indexMinutThreeSigma"  => $indexMinutThreeSigma,
										// "indexMinutFourSigma"   => $indexMinutFourSigma,
										// "indexAvg"              => $indexAvg,
										// "indexPlusOneSigma"     => $indexPlusOneSigma,
										// "indexPlusTwoSigma"     => $indexPlusTwoSigma,
										// "indexPlusThreeSigma"   => $indexPlusThreeSigma,
										// "indexPlusFourSigma"    => $indexPlusFourSigma,
										// "indexLCL"              => $indexLCL,
										// "indexUCL"              => $indexUCL,
										// "USL"                   => $USL,
										// "LSL"                   => $LSL
					];		
					// echo "<pre>"; print_r($arrLCLXLower); echo "</pre>";

				}
		}else if(Input::has('btnExcel')){
                $lTblSearchResultData = $this->getGraphResultList();

			 	if (count($lTblSearchResultData) == 0)
				{ 
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
										$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"LCLStdev"              => null,
										"CLS"                   => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
										"arrMaxXbarChart"       => null,
										"arrMinXbarChart"       => null,
										"arrMaxRangeChart"      => null,
										"arrMinRangeChart"      => null,
										"arrMaxStdevChart"      => null,
										"arrMinStdevChart"      => null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexLCL"              => null,
										"indexUCL"              => null,
					];


				}else{ 
				 	try 
				    {
				    	$spreadsheet = new Spreadsheet();
				    	$lTemplateFileNameInspectionSheet = "DataOnGraphPageV1_template.xlsx";
						$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameInspectionSheet);

						$sheet = $spreadsheet->getActiveSheet();
						foreach ($lTblSearchResultData as $key => $value) {
				        	$cell = $key + 2;
				        	$sheet->getCell("A".$cell)->setValue($value->PRODUCT_NO);
				        	$sheet->getCell("B".$cell)->setValue($value->REV_NO);
				        	$sheet->getCell("C".$cell)->setValue($value->PROCESS_NAME);
				        	$sheet->getCell("D".$cell)->setValue($value->INSPECTION_YMD);
				        	$sheet->getCell("E".$cell)->setValue($value->INSPECTION_TIME_ID);
				        	$sheet->getCell("F".$cell)->setValue($value->INSPECTION_TIME_NAME);
				        	$sheet->getCell("G".$cell)->setValue($value->CONDITION_CD_NAME);
				        	$sheet->getCell("H".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("I".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("J".$cell)->setValue($value->TEAM_ID);
				        	$sheet->getCell("K".$cell)->setValue($value->TEAM_NAME);
				        	$sheet->getCell("M".$cell)->setValue($value->CAVITY_NO);
				        	$sheet->getCell("N".$cell)->setValue($value->WHERE_NO);
				        	$sheet->getCell("O".$cell)->setValue($value->PICTURE_URL);
				        	$sheet->getCell("P".$cell)->setValue($value->MIN_VALUE);
				        	$sheet->getCell("Q".$cell)->setValue($value->MAX_VALUE);
				        	$sheet->getCell("R".$cell)->setValue($value->AVERAGE_VALUE);
				        	$sheet->getCell("S".$cell)->setValue($value->RANGE_VALUE);
				        	$sheet->getCell("T".$cell)->setValue($value->STDEV);
				        	$sheet->getCell("U".$cell)->setValue($value->CPK);
				        	$sheet->getCell("V".$cell)->setValue($value->STD_MIN_SPEC);
				        	$sheet->getCell("W".$cell)->setValue($value->STD_MAX_SPEC);
				        	$sheet->getCell("X".$cell)->setValue($value->LOT_NO);
				        	$sheet->getCell("Y".$cell)->setValue($value->PRODUCTION_YMD);
						}

					    //set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
						$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet);

						$lSaveFilePathAndName = "excel/".$lSaveFileName;  
						// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

						//save file（save in server temporarily）
						$writer = new XlsxWriter($spreadsheet);
						//if many data exist,it need very long time for SAVE.(it is available to download directly without save
						$writer->save($lSaveFilePathAndName);

						//download
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Length: '.filesize($lSaveFilePathAndName));
						header('Content-disposition: attachment; filename='.$lSaveFileName);
						readfile($lSaveFilePathAndName);

						//delete fime(saved in server)
						unlink($lSaveFilePathAndName);

						//message
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";	
						    
				    }
				    catch(Exception $e) 
				    {
				        die('Error loading file "'.pathinfo($lSaveFilePathAndName,PATHINFO_BASENAME).'": '.$e->getMessage());
				    }	

					$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"MaxofSChart"           => null,
										"LCLStdev"              => null,
										"MinofSChart"           => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
					];
				}
		}else{
			//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "Please select type graph."
					]);
										$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"LCLStdev"              => null,
										"CLS"                   => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
										"arrMaxXbarChart"       => null,
										"arrMinXbarChart"       => null,
										"arrMaxRangeChart"      => null,
										"arrMinRangeChart"      => null,
										"arrMaxStdevChart"      => null,
										"arrMinStdevChart"      => null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexLCL"              => null,
										"indexUCL"              => null,
					];

		}

	    if(Input::has('cmbProcessForGraph')){
	    	$lArrCustomerList = $this->setCustomerList($lViewData,Input::get('cmbProcessForGraph'));
	    	data_set($lViewData, 'arrCustomerList', $lArrCustomerList);
	    }

		if (Input::has('cmbProcessForGraph') and Input::has('cmbCustomerForGraph')){
			
			$lArrProductNoList = $this->setProductNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'));
			 data_set($lViewData, 'arrProductNoList', $lArrProductNoList);
		}

		if(Input::has('cmbCheckSheetNoForGraph')){
			$lArrRevNoList = $this->setRevNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'));
			data_set($lViewData, 'arrRevNoList', $lArrRevNoList);
		}

		if(Input::has('cmbRevisionNoForGraph')){

			$lArrMoldList = $this->setMoldList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrMoldNoList', $lArrMoldList);

			$lArrInspectionPointList = $this->setInspectionPointList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrInspectionPointList', $lArrInspectionPointList);

			$lArrCavityNoList = $this->setCavityNoList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrCavityNoList', $lArrCavityNoList);

			$lArrCavityNoList = $this->setTimeIDList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrTimeIDList', $lArrCavityNoList);

			$lArrTeamList = $this->setTeamList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrTeamNameList', $lArrTeamList);

			$lArrTeamList = $this->setTeamList($lViewData, Input::get('cmbProcessForGraph'), Input::get('cmbCustomerForGraph'), Input::get('cmbCheckSheetNoForGraph'), Input::get('cmbRevisionNoForGraph'));
			data_set($lViewData, 'arrTeamNameList', $lArrTeamList);
			
		}
        // echo "<pre>"; print_r($lViewData); echo "</pre>";
		return View("user.graph", $lViewData);
        
	}

	private function setProcessList($pViewData)
	{
		$lArrProcessList = ["" => ""];

		$objtcodemst = new tcodemst;
		$lArrProcessList = $objtcodemst->getProcessList();

		$pViewData += [
			"arrProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	private function setCustomerList($pViewData, $processID)
	{  
		$lArrObjCustomerList   = []; 
		$lArrCustomerList      = [];
		$lArrCustomerList[]    = "";
       
        if($processID != ""){
			$lArrObjCustomerList = DB::table('TISHEETM as TISHE')
			->select('TISHE.CUSTOMER_ID as CUSTOMER_ID', 'TCUST.CUSTOMER_NAME as CUSTOMER_NAME')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->join('TCODEMST as TCODE', function ($join) {
	        		$join->on('TISHE.PROCESS_ID', '=', 'TCODE.CODE_ORDER')
	        		     ->where('TCODE.CODE_CLASS', '=', "002");
	        	})
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->where('TISHE.PROCESS_ID', '=', $processID)
	        ->groupBy('TCUST.CUSTOMER_ID')
	        ->get();

			foreach ($lArrObjCustomerList as $key => $value) {
	           	$lArrCustomerList[$value->CUSTOMER_ID] = $value->CUSTOMER_NAME;
	        }
		}

		return $lArrCustomerList;
	}

	private function setCustomerListfromProcessName($pViewData, $processName)
	{ 
		$lArrObjCustomerList   = []; 
		$lArrCustomerList      = [];
		$lArrCustomerList[]    = "";

		$lArrObjCustomerList = DB::table('TISHEETM as TISHE')
		->select('TISHE.CUSTOMER_ID as CUSTOMER_ID', 'TCUST.CUSTOMER_NAME as CUSTOMER_NAME')
        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
        ->join('TRESANAL as TRESA', function ($join) {
	            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
	                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
	                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
	        })
        ->join('TCODEMST as TCODE', function ($join) {
        		$join->on('TISHE.PROCESS_ID', '=', 'TCODE.CODE_ORDER')
        		     ->where('TCODE.CODE_CLASS', '=', "002");
        	})
        ->where('TCUST.DELETE_FLG', '=', "0")
        ->where('TCODE.CODE_NAME', '=', $processName)
        ->groupBy('TCUST.CUSTOMER_ID')
        ->get();

		foreach ($lArrObjCustomerList as $key => $value) {
           	$lArrCustomerList[$value->CUSTOMER_ID] = $value->CUSTOMER_NAME;
        }

		$pViewData += [
			"arrCustomerList" => $lArrCustomerList
		];

		return $pViewData;
	}

	private function setProductNoList($pViewData, $processID, $customerID)
	{  
		$lArrObjProductNoList   = []; 
		$lArrProductNoList      = [];
		$lArrProductNoList[]    = "";
       
        if($processID != "" and $customerID != ""){
			$lArrObjProductNoList = DB::table('TISHEETM as TISHE')
			->select('TISHE.INSPECTION_SHEET_NO as PRODUCT_NO')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->where('TISHE.PROCESS_ID', $processID)
			->where('TISHE.CUSTOMER_ID', $customerID)
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->groupBy('TISHE.INSPECTION_SHEET_NO')
	        ->get();

	        foreach ($lArrObjProductNoList as $key => $value) {
	            $lArrProductNoList[$value->PRODUCT_NO] = $value->PRODUCT_NO;
	        }
        }
        return $lArrProductNoList;
	}

	private function setRevNoList($pViewData, $processID, $customerID, $checksheetNo)
	{  
		$lArrObjRevNoList   = []; 
		$lArrRevNoList      = [];
        $lArrRevNoList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != ""){
			$lArrObjRevNoList = DB::table('TISHEETM as TISHE')
			->select('TISHE.REV_NO as REV_NO')
	        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
	        ->join('TRESANAL as TRESA', function ($join) {
		            $join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
		                 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
		                 ->on('TISHE.PROCESS_ID', '=', 'TRESA.PROCESS_ID');
		        })
	        ->where('TISHE.PROCESS_ID', $processID)
			->where('TISHE.CUSTOMER_ID', $customerID)
			->where('TISHE.INSPECTION_SHEET_NO', $checksheetNo)
	        ->where('TCUST.DELETE_FLG', '=', "0")
	        ->groupBy('TISHE.REV_NO')
	        ->get();
	        foreach ($lArrObjRevNoList as $key => $value) {
	           	$lArrRevNoList[$value->REV_NO] = $value->REV_NO;
	        }
	    }
		 return $lArrRevNoList;
	}

	private function setMoldList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjMoldNoList   = []; 
		$lArrMoldNoList      = [];
        $lArrMoldNoList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){
			if (Input::has('cmbRadioChoosePeriod')){

				$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
				$ldate  = Carbon::now();
				$ldateNow = $ldate->toDateTimeString();
				$ldatePast = $ldate->subDays($amountOfTime);

				$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
				$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
	            $lProductionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
				$lProductionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
	 
			}else{
				//change English type->Japanese type（because MySQL store as Japanese type）
				$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
				$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
				$lProductionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
				$lProductionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
			}

	        $lArrObjMoldNoList = DB::table('TRESANAL as TRESA')
				->select('TRESA.MOLD_NO')
				->join('TISHEETM as TISHE', function ($join) {
				    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
				        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
				})
				->join('TRESHEDT as TRESH', function ($join) {
					$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
						->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
						->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
				})
				->join('TINSPTIM as TIMEE', function ($join) {
					$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
				})
				->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
				->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
				->where('TRESA.INSPECTION_SHEET_NO',Input::get('cmbCheckSheetNoForGraph'))
				->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
				->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
				->whereBetween('TRESA.INSPECTION_YMD', array($lInspectionDateFromForGraph, $lInspectionDateToForGraph))
				->whereBetween('TRESH.PRODUCTION_YMD', array($lProductionDateFromForGraph, $lProductionDateToForGraph))
				->where('TCUST.DELETE_FLG', '=', "0")
			    ->where('TIMEE.DELETE_FLG', '=', "0")
				->groupBy('TRESA.MOLD_NO')
				->get();    
	        echo "<pre>"; print_r($lArrObjMoldNoList); echo "</pre>";
	        if(empty($lArrObjMoldNoList)){
	                $lArrMoldNoList[''] = "";
	        }else{
	        	foreach ($lArrObjMoldNoList as $key => $value) {
	           		$lArrMoldNoList[$value->MOLD_NO] = $value->MOLD_NO;
	        	}
	        }
	    }
        return $lArrMoldNoList;
	}

	private function setInspectionPointList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjInspectionPointList   = []; 
		$lArrInspectionPointList      = [];
        $lArrInspectionPointList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){

	        $lArrObjInspectionPointList = DB::table('TISHEETM as TISHE')
				->select('TRESA.INSPECTION_POINT')
				->join('TINSPNOM as TINSP', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
					     ->on('TISHE.REV_NO', '=', 'TINSP.REV_NO');
				})
				->join('TINSNTMM as TINSN', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
					     ->on('TINSP.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT');
				})
				->join('TRESANAL as TRESA', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
					 	 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
					 	 ->on('TINSP.INSPECTION_POINT', '=', 'TRESA.INSPECTION_POINT');
				})
		        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
		        ->join('TINSPTIM as TIMEE', 'TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID')
		        ->where('TISHE.PROCESS_ID', Input::get('cmbProcessForGraph'))
				->where('TISHE.CUSTOMER_ID', Input::get('cmbCustomerForGraph'))
				->where('TISHE.INSPECTION_SHEET_NO', Input::get('cmbCheckSheetNoForGraph'))
				->where('TISHE.REV_NO', Input::get('cmbRevisionNoForGraph'))
		        ->where('TCUST.DELETE_FLG', '=', "0")
		        ->where('TINSN.DELETE_FLG', '=', "1")
		        ->where('TIMEE.DELETE_FLG', '=', "0")
		        ->groupBy('TRESA.INSPECTION_POINT')
		        ->get();
	      
	        if(empty($lArrObjInspectionPointList)){
	                $lArrInspectionPointList[''] = "";
	        }else{
	        	foreach ($lArrObjInspectionPointList as $key => $value) {
	           		$lArrInspectionPointList[$value->INSPECTION_POINT] = $value->INSPECTION_POINT;
	        	}
	        }
	    }
        return $lArrInspectionPointList;
	}

	private function setCavityNoList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjCavityNoList   = []; 
		$lArrCavityNoList      = [];
        $lArrCavityNoList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){

	        $lArrObjCavityNoList = DB::table('TISHEETM as TISHE')
				->select('TRESA.ANALYTICAL_GRP_01')
				->join('TINSPNOM as TINSP', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
					     ->on('TISHE.REV_NO', '=', 'TINSP.REV_NO');
				})
				->join('TINSNTMM as TINSN', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
					     ->on('TINSP.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT');
				})
				->join('TRESANAL as TRESA', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
					 	 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
					 	 ->on('TINSP.INSPECTION_POINT', '=', 'TRESA.INSPECTION_POINT');
				})
		        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
		        ->join('TINSPTIM as TIMEE', 'TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID')
		        ->where('TISHE.PROCESS_ID', Input::get('cmbProcessForGraph'))
				->where('TISHE.CUSTOMER_ID', Input::get('cmbCustomerForGraph'))
				->where('TISHE.INSPECTION_SHEET_NO', Input::get('cmbCheckSheetNoForGraph'))
				->where('TISHE.REV_NO', Input::get('cmbRevisionNoForGraph'))
		        ->where('TCUST.DELETE_FLG', '=', "0")
		        ->where('TINSN.DELETE_FLG', '=', "1")
		        ->where('TIMEE.DELETE_FLG', '=', "0")
		        ->groupBy('TRESA.ANALYTICAL_GRP_01')
		        ->get();
	      
	        if(empty($lArrObjCavityNoList)){
	                $lArrCavityNoList[''] = "";
	        }else{
	        	foreach ($lArrObjCavityNoList as $key => $value) {
	           		$lArrCavityNoList[$value->ANALYTICAL_GRP_01] = $value->ANALYTICAL_GRP_01;
	        	}
	        }
	    }
        return $lArrCavityNoList;
	}

    private function setTimeIDList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjTimeIDList   = []; 
		$lArrTimeIDList      = [];
        $lArrTimeIDList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){

	        $lArrObjTimeIDList = DB::table('TISHEETM as TISHE')
				->select('TRESA.INSPECTION_TIME_ID', 'TIMEE.INSPECTION_TIME_NAME')
				->join('TINSPNOM as TINSP', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
					     ->on('TISHE.REV_NO', '=', 'TINSP.REV_NO');
				})
				->join('TINSNTMM as TINSN', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
					     ->on('TINSP.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT');
				})
				->join('TRESANAL as TRESA', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
					 	 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
					 	 ->on('TINSP.INSPECTION_POINT', '=', 'TRESA.INSPECTION_POINT');
				})
		        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
		        ->join('TINSPTIM as TIMEE', 'TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID')
		        ->where('TISHE.PROCESS_ID', Input::get('cmbProcessForGraph'))
				->where('TISHE.CUSTOMER_ID', Input::get('cmbCustomerForGraph'))
				->where('TISHE.INSPECTION_SHEET_NO', Input::get('cmbCheckSheetNoForGraph'))
				->where('TISHE.REV_NO', Input::get('cmbRevisionNoForGraph'))
		        ->where('TCUST.DELETE_FLG', '=', "0")
		        ->where('TINSN.DELETE_FLG', '=', "1")
		        ->where('TIMEE.DELETE_FLG', '=', "0")
		        ->groupBy('TRESA.INSPECTION_TIME_ID')
		        ->get();
	      
	        if(empty($lArrObjTimeIDList)){
	                $lArrTimeIDList[''] = "";
	        }else{
	        	foreach ($lArrObjTimeIDList as $key => $value) {
	           		$lArrTimeIDList[$value->INSPECTION_TIME_ID] = $value->INSPECTION_TIME_NAME;
	        	}
	        }
	    }
        return $lArrTimeIDList;
	}
     

    private function setTeamList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjTeamList   = []; 
		$lArrTeamList      = [];
        $lArrTeamList[]    = "";
		if($processID != "" and $customerID != "" and $checksheetNo != "" and $revNo != ""){

	        $lArrObjTeamList = DB::table('TISHEETM as TISHE')
				->select('TRESA.TEAM_ID', 'TCODE.CODE_NAME')
				->join('TINSPNOM as TINSP', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
					     ->on('TISHE.REV_NO', '=', 'TINSP.REV_NO');
				})
				->join('TINSNTMM as TINSN', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
					     ->on('TINSP.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT');
				})
				->join('TRESANAL as TRESA', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
					 	 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
					 	 ->on('TINSP.INSPECTION_POINT', '=', 'TRESA.INSPECTION_POINT');
				})
		        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
		        ->join('TINSPTIM as TIMEE', 'TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID')
		        ->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "004");
		        })
		        ->where('TRESA.PROCESS_ID', Input::get('cmbProcessForGraph'))
				->where('TISHE.CUSTOMER_ID', Input::get('cmbCustomerForGraph'))
				->where('TRESA.INSPECTION_SHEET_NO', Input::get('cmbCheckSheetNoForGraph'))
				->where('TRESA.REV_NO', Input::get('cmbRevisionNoForGraph'))
		        ->where('TCUST.DELETE_FLG', '=', "0")
		        ->where('TINSN.DELETE_FLG', '=', "1")
		        ->where('TIMEE.DELETE_FLG', '=', "0")
		        ->groupBy('TRESA.TEAM_ID')
		        ->get();

	        if(empty($lArrObjTeamList)){
	                $lArrTeamList[''] = "";
	        }else{
	        	foreach ($lArrObjTeamList as $key => $value) {
	           		$lArrTeamList[$value->TEAM_ID] = $value->CODE_NAME;
	        	}
	        }
	    }
        return $lArrTeamList;
	}
	 

	private function getConditionList($pViewData, $processID, $customerID, $checksheetNo, $revNo)
	{  
		$lArrObjConditionList   = []; 
		$lArrConditionList      = [];
		$lArrConditionList[]    = "";
		$lArrObjConditionList = DB::table('TISHEETM as TISHE')
				->select('TRESA.CONDITION_CD', 'TCODE.CODE_NAME')
				->join('TINSPNOM as TINSP', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
					     ->on('TISHE.REV_NO', '=', 'TINSP.REV_NO');
				})
				->join('TINSNTMM as TINSN', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
					     ->on('TINSP.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT');
				})
				->join('TRESANAL as TRESA', function ($join){
					$join->on('TISHE.INSPECTION_SHEET_NO', '=', 'TRESA.INSPECTION_SHEET_NO')
					 	 ->on('TISHE.REV_NO', '=', 'TRESA.REV_NO')
					 	 ->on('TINSP.INSPECTION_POINT', '=', 'TRESA.INSPECTION_POINT');
				})
		        ->join('TCUSTOMM as TCUST', 'TISHE.CUSTOMER_ID', '=', 'TCUST.CUSTOMER_ID')
		        ->join('TINSPTIM as TIMEE', 'TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID')
		        ->join('TCODEMST as TCODE', function ($join) {
		        		$join->on('TRESA.TEAM_ID', '=', 'TCODE.CODE_ORDER')
		        			 ->where('TCODE.CODE_CLASS', '=', "001");
		        })
		        ->where('TISHE.PROCESS_ID', Input::get('cmbProcessForGraph'))
				->where('TISHE.CUSTOMER_ID', Input::get('cmbCustomerForGraph'))
				->where('TISHE.INSPECTION_SHEET_NO', Input::get('cmbCheckSheetNoForGraph'))
				->where('TISHE.REV_NO', Input::get('cmbRevisionNoForGraph'))
		        ->where('TCUST.DELETE_FLG', '=', "0")
		        ->where('TINSN.DELETE_FLG', '=', "1")
		        ->where('TIMEE.DELETE_FLG', '=', "0")
		        ->groupBy('TRESA.CONDITION_CD')
		        ->get();

		        if(empty($lArrObjConditionList)){
	                $lArrConditionList[''] = "";
	        }else{
	        	foreach ($lArrObjConditionList as $key => $value) {
	           		$lArrConditionList[$value->CONDITION_CD] = $value->CODE_NAME;
	        	}
	        }
	    }
        return $lArrConditionList;
	}
	 
	private function getInspectionDateFromToForGraph($pViewData)
	{  
		if (Input::has('txtInspectionDateFromForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010InspectionDateFromForGraph', Input::get('txtInspectionDateFromForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010InspectionDateFromForGraph', date("d-m-Y",strtotime("-1 month")));
		}

		if (Input::has('txtInspectionDateToForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010InspectionDateToForGraph', Input::get('txtInspectionDateToForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010InspectionDateToForGraph', date("d-m-Y"));
		}

		if (Input::has('txtProductionDateFromForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010ProductionDateFromForGraph', Input::get('txtProductionDateFromForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010ProductionDateFromForGraph', date("d-m-Y",strtotime("-1 month")));
		}

		if (Input::has('txtProductionDateToForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010ProductionDateToForGraph', Input::get('txtProductionDateToForGraph'));
		}
		else 
		{
			//if value of screen,write down system date in session
			Session::put('BA1010ProductionDateToForGraph', date("d-m-Y"));
		}

		$pViewData += [
			 "InspectionDateFromForGraph"  => Session::get('BA1010InspectionDateFromForGraph')
			,"InspectionDateToForGraph"  => Session::get('BA1010InspectionDateToForGraph')
			,"ProductionDateFromForGraph"  => Session::get('BA1010InspectionDateFromForGraph')
			,"ProductionDateToForGraph"  => Session::get('BA1010ProductionDateToForGraph')
		];

		return $pViewData;
	}


	private function getGraphResultList()
	{  
		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$lProductionDateFromForGraph    = "";
		$lProductionDateToForGraph    = "";


		if (Input::has('cmbRadioChoosePeriod')){

			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);

			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
            $lProductionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lProductionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
 
		}else{
			//change English type->Japanese type（because MySQL store as Japanese type）
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
			$lProductionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
			$lProductionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
		}

		if(Input::get('cmbProcessForGraph') != "" AND Input::get('cmbCustomerForGraph') != "" AND Input::get('cmbCheckSheetNoForGraph') != "" AND  Input::get('cmbRevisionNoForGraph')){
        // echo "<pre>"; print_r($lProductionDateFromForGraph); echo "</pre>";
        // echo "<pre>"; print_r($lProductionDateToForGraph); echo "</pre>";
			if($lProductionDateFromForGraph != $lProductionDateToForGraph){
				$lTblSearchResultData = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			, DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD'))
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO',Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					// ->whereBetween('TRESA.INSPECTION_YMD', array($lInspectionDateFromForGraph, $lInspectionDateToForGraph))
					->whereBetween('TRESH.PRODUCTION_YMD', array($lProductionDateFromForGraph, $lProductionDateToForGraph))
					->get();
				}else{
					$lTblSearchResultData = DB::table('TRESANAL as TRESA')
					->select('TRESA.INSPECTION_SHEET_NO as PRODUCT_NO'
					        	,'TRESA.REV_NO as REV_NO'
					        	,'PROCESS.CODE_NAME as PROCESS_NAME'
					        	, DB::raw('DATE_FORMAT(TRESA.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
					        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
					        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "N" WHEN TRESA.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
					        	, DB::raw('CASE WHEN TRESA.CONDITION_CD = "01" THEN "Normal" WHEN TRESA.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
					        	,'TRESA.MOLD_NO' 
								,'TRESA.MOLD_NO as MOLD_NAME'
								,'TRESA.TEAM_ID'
								,'TEAM.CODE_NAME as TEAM_NAME'
								,'TRESA.INSPECTION_POINT as INSPECTION_POINT'
								,'TRESA.ANALYTICAL_GRP_01 as CAVITY_NO'
					 		    ,'TRESA.ANALYTICAL_GRP_02 as WHERE_NO'
					 		    ,'TRESA.STD_MIN_SPEC'
					 		    ,'TRESA.STD_MAX_SPEC'							 
								,'TRESA.PICTURE_URL'
					 		   	,'TRESA.MIN_VALUE'
					 		   	,'TRESA.MAX_VALUE'
								,'TRESA.AVERAGE_VALUE as AVERAGE_VALUE'
								,'TRESA.RANGE_VALUE as RANGE_VALUE'
								,'TRESA.STDEV'
								,'TRESA.CPK as CPK'
					 		   	,'TRESA.XBAR_UCL'									
					 		  	,'TRESA.XBAR_LCL'
					 		  	,'TRESA.RCHART_UCL'									
					 		  	,'TRESA.RCHART_LCL'
					 			,'TRESA.JUDGE_REASON'
					 			,'TRESH.LOT_NO'
					 			, DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD'))
					->join('TISHEETM as TISHE', function ($join) {
					    $join->on('TRESA.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
					        ->on('TRESA.REV_NO', '=', 'TISHE.REV_NO');
					})
					->join('TRESHEDT as TRESH', function ($join) {
						$join->on('TRESA.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO')
							->on('TRESA.INSPECTION_SHEET_NO', '=', 'TRESH.INSPECTION_SHEET_NO')
							->on('TRESA.PROCESS_ID', '=', 'TRESH.PROCESS_ID');
					})
					->join('TCODEMST as PROCESS', function ($join) {
						$join->on('TRESA.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
						     ->where('PROCESS.CODE_CLASS', '=', '002');
					})
					->join('TINSPTIM as TIMEE', function ($join) {
						$join->on('TRESA.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
					})
					->join('TCODEMST as TEAM', function ($join) {
						$join->on('TRESA.TEAM_ID', '=', 'TEAM.CODE_ORDER')
						     ->where('TEAM.CODE_CLASS', '=', '004');
					})
					->where('TRESA.PROCESS_ID',Input::get('cmbProcessForGraph'))
					->where('TRESA.INSPECTION_SHEET_NO',Input::get('cmbCheckSheetNoForGraph'))
					->where('TRESA.REV_NO',Input::get('cmbRevisionNoForGraph'))
					->where('TISHE.CUSTOMER_ID',Input::get('cmbCustomerForGraph'))
					->where('TRESH.PRODUCTION_YMD', $lProductionDateFromForGraph)
					->get();
				}
		

        // echo "<pre>"; print_r($lTblSearchResultData); echo "</pre>";
			}else{
	        	    $lTblSearchResultData = [];
	        }
		return $lTblSearchResultData;
	}

	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{

		//not to entry date
		if ($pTargetDate == "") {
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1") {
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');

	}

	private function setListForms($pViewData)
	{

		if (Input::has('cmbProcessForGraph')) {
			Session::put('BA1010ProcessForGraph', Input::get('cmbProcessForGraph'));
		}
		else
		{
			Session::put('BA1010ProcessForGraph', "");
		}

		$pViewData += [
				"ProcessForGraph"  => Session::get('BA1010ProcessForGraph')
		];

		if (Input::has('cmbCustomerForGraph')) {
			Session::put('BA1010CustomerForGraph', Input::get('cmbCustomerForGraph'));
		}
		else 
		{
			Session::put('BA1010CustomerForGraph', "");
		}
		
		$pViewData += [
				"CustomerForGraph"  => Session::get('BA1010CustomerForGraph')
		];

		if (Input::has('cmbCheckSheetNoForGraph')) {
			Session::put('BA1010ProductNoForGraph', Input::get('cmbCheckSheetNoForGraph'));
		}
		else 
		{
			Session::put('BA1010ProductNoForGraph', "");
		}
		$pViewData += [
				"ProductNoForGraph"  => Session::get('BA1010ProductNoForGraph')
		];

		if (Input::has('cmbRevisionNoForGraph')) {
			Session::put('BA1010RevNoForGraph', Input::get('cmbRevisionNoForGraph'));
		}
		else 
		{
			Session::put('BA1010RevNoForGraph', "");
		}
		$pViewData += [
				"RevNoForGraph"  => Session::get('BA1010RevNoForGraph')
		];

		if (Input::has('cmbMoldNoForGraph')) {
			Session::put('BA1010MoldNoForGraph', Input::get('cmbMoldNoForGraph'));
		}
		else 
		{
			Session::put('BA1010MoldNoForGraph', "");
		}
		$pViewData += [
				"MoldNoForGraph"  => Session::get('BA1010MoldNoForGraph')
		];

		if (Input::has('cmbInspectionPointForGraph')) {
			Session::put('BA1010InspectionPointForGraph', Input::get('cmbInspectionPointForGraph'));
		}
		else 
		{
			Session::put('BA1010InspectionPointForGraph', "");
		}
		$pViewData += [
				"InspectionPointForGraph"  => Session::get('BA1010InspectionPointForGraph')
		];
		

		if (Input::has('cmbCavityForGraph')) {
			Session::put('BA1010CavityNoForGraph', Input::get('cmbCavityForGraph'));
		}
		else 
		{
			Session::put('BA1010CavityNoForGraph', "");
		}
		$pViewData += [
				"CavityNoForGraph"  => Session::get('BA1010CavityNoForGraph')
		];
		
		if (Input::has('cmbTeamNameForGraph')) {
			Session::put('BA1010TeamNoForGraph', Input::get('cmbTeamNameForGraph'));
		}
		else 
		{
			Session::put('BA1010TeamNoForGraph', "");
		}
		$pViewData += [
				"TeamNameForGraph"  => Session::get('BA1010TeamNoForGraph')
		];
		
        if(Input::has('cmbTimeIDForGraph')){
           Session::put('BA1010TimeIDForGraph', Input::get('cmbTimeIDForGraph'));
		}
		else 
		{
			Session::put('BA1010TimeIDForGraph', "");
		}
		$pViewData += [
				"TimeIDForGraph"  => Session::get('BA1010TimeIDForGraph')
		];
		// if (Input::has('cmbMaterialNameForGraph')) {
		// 	Session::put('BA1010MaterialNameForGraph', Input::get('cmbMaterialNameForGraph'));
		// }
		// else 
		// {
		// 	Session::put('BA1010MaterialNameForGraph', "");
		// }
		// $pViewData += [
		// 		"MaterialNameForGraph"  => Session::get('BA1010MaterialNameForGraph')
		// ];

		
		// if (Input::has('cmbMaterialSizeForGraph')) {
		// 	Session::put('BA1010MaterialSizeForGraph', Input::get('cmbMaterialSizeForGraph'));
		// }
		// else
		// {
		// 	Session::put('BA1010MaterialSizeForGraph', "");
		// }

		// $pViewData += [
		// 		"MaterialSizeForGraph"  => Session::get('BA1010MaterialSizeForGraph')
		// ];
	
		if (Input::has('cmbConditionForGraph')) {
			Session::put('BA1010ConditionForGraph', Input::get('cmbConditionForGraph'));
		}
		else
		{
			Session::put('BA1010ConditionForGraph', "");
		}

		$pViewData += [
				"ConditionForGraph"  => Session::get('BA1010ConditionForGraph')
		];
		return $pViewData;
	}

	private function getBellCurve($STDEV_VALUE,$AVERAGE_VALUE){
		$Min5Sigma = $AVERAGE_VALUE - (5 * $STDEV_VALUE);
		$Max5Sigma = $AVERAGE_VALUE + (5 * $STDEV_VALUE);
		$Min4Sigma = $AVERAGE_VALUE - (4 * $STDEV_VALUE);
		$Max4Sigma = $AVERAGE_VALUE + (4 * $STDEV_VALUE);
		$Min3Sigma = $AVERAGE_VALUE - (3 * $STDEV_VALUE);
		$Max3Sigma = $AVERAGE_VALUE + (3 * $STDEV_VALUE);
		$Min2Sigma = $AVERAGE_VALUE - (2 * $STDEV_VALUE);
		$Max2Sigma = $AVERAGE_VALUE + (2 * $STDEV_VALUE);
		$MinSigma = $AVERAGE_VALUE - $STDEV_VALUE;
		$MaxSigma = $AVERAGE_VALUE + $STDEV_VALUE;
        
        $bar = 0.5;
 		//-5sigma to < -4sigma
		for($i = $Min5Sigma; $i < $Min4Sigma; $i = $i + $bar){
			
			$valueX5[] = $i;
			$valueY5[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine5Sigma = [];
		foreach($valueX5 as $key => $value) {
			    $valueLine5Sigma[$key] = [$valueX5[$key],$valueY5[$key]];
		}

        //-4sigma to < -3sigma
		for($i = $Min4Sigma; $i < $Min3Sigma; $i = $i + $bar){
			
			$valueX4[] = $i;
			$valueY4[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine4Sigma = [];
		foreach($valueX4 as $key => $value) {
			    $valueLine4Sigma[$key] = [$valueX4[$key],$valueY4[$key]];
		}
        
        //-3sigma to < -2sigma
        for($i = $Min3Sigma; $i < $Min2Sigma; $i = $i + $bar){
			
			$valueX3[] = $i;
			$valueY3[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine3Sigma = [];
		foreach($valueX3 as $key => $value) {
			    $valueLine3Sigma[$key] = [$valueX3[$key],$valueY3[$key]];
		}

		//-2sigma to < -sigma
 		for($i = $Min2Sigma; $i < $MinSigma; $i = $i + $bar){
			
			$valueX2[] = $i;
			$valueY2[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine2Sigma = [];
		foreach($valueX2 as $key => $value) {
			    $valueLine2Sigma[$key] = [$valueX2[$key],$valueY2[$key]];
		}

		//-sigma to < average
		for($i = $MinSigma; $i < $AVERAGE_VALUE; $i = $i + $bar){
			
			$valueX[] = $i;
			$valueY[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}

		$valueLineSigma = [];
		foreach($valueX as $key => $value) {
			    $valueLineSigma[$key] = [$valueX[$key],$valueY[$key]];
		}

		//average
		$valueYAvergage = number_format($this->normal($AVERAGE_VALUE, $AVERAGE_VALUE, $STDEV_VALUE),3);
		$valueLineAvergage = array(
			  array($AVERAGE_VALUE,$valueYAvergage)
			  );

		//>average to +sigma
		for($i = $MaxSigma; $i > $AVERAGE_VALUE; $i -= $bar){
			
			$valueXRight[] = $i;
			$valueYRight[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlusSigma = [];
		foreach($valueXRight as $key => $value) {
			    $valueLinePlusSigma[$key] = [$valueXRight[$key],$valueYRight[$key]];
		}
		krsort($valueLinePlusSigma);

		//>+sigma to +2sigma
		for($i = $Max2Sigma; $i > $MaxSigma; $i -= $bar){
			
			$valueX2Right[] = $i;
			$valueY2Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus2Sigma = [];
		foreach($valueX2Right as $key => $value) {
			    $valueLinePlus2Sigma[$key] = [$valueX2Right[$key],$valueY2Right[$key]];
		}
		krsort($valueLinePlus2Sigma);

		//>+2sigma to +3sigma
		for($i = $Max3Sigma; $i > $Max2Sigma; $i -= $bar){
			
			$valueX3Right[] = $i;
			$valueY3Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus3Sigma = [];
		foreach($valueX3Right as $key => $value) {
			    $valueLinePlus3Sigma[$key] = [$valueX3Right[$key],$valueY3Right[$key]];
		}
		krsort($valueLinePlus3Sigma);

		//>+3sigma to +4sigma
		for($i = $Max4Sigma; $i > $Max3Sigma; $i -= $bar){
			
			$valueX4Right[] = $i;
			$valueY4Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus4Sigma = [];

		foreach($valueX4Right as $key => $value) {
			    $valueLinePlus4Sigma[$key] = [$valueX4Right[$key],$valueY4Right[$key]];
		}
		krsort($valueLinePlus4Sigma);

		//>+4sigma to +5sigma
		for($i = $Max5Sigma; $i > $Max4Sigma; $i -= $bar){
			
			$valueX5Right[] = $i;
			$valueY5Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus5Sigma = [];

		foreach($valueX5Right as $key => $value) {
			    $valueLinePlus5Sigma[$key] = [$valueX5Right[$key],$valueY5Right[$key]];
		}
		krsort($valueLinePlus5Sigma);

		$valueLine = array_merge($valueLine5Sigma, $valueLine4Sigma, $valueLine3Sigma, $valueLine2Sigma, $valueLineSigma, $valueLineAvergage, $valueLinePlusSigma, $valueLinePlus2Sigma, $valueLinePlus3Sigma, $valueLinePlus4Sigma, $valueLinePlus5Sigma);
		 return $valueLine;
	}

	private function normal($x, $mu, $sigma) {
	    return exp(-0.5 * ($x - $mu) * ($x - $mu) / ($sigma*$sigma))
	        / ($sigma * sqrt(2.0 * M_PI));
	}

	private function fourDimentionToTwoDimention($fourDimention){

		foreach ($fourDimention as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						// echo "<pre>cc"; print_r($value3); echo "</pre>";
						$fourDimentionFillPoint[$key][$key1][$key2][0] = number_format($value3,3);
						$fourDimentionFillPoint[$key][$key1][$key2][] = number_format($value3,3);
					}
				$fourDimentionFillPoint[$key][$key1][$key2][] = number_format($value3,3);
				}
			}
		}

  		foreach ($fourDimentionFillPoint as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {

					$arrTwoDimention[] = $value2;
				}
			}
		}
		return $arrTwoDimention;
	}
    
    private function getArrPointOnLineChart($arrValue){
    	foreach ($arrValue as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrPoint[] = $value2;
				}
			}
		}
		foreach ($arrPoint as $key => $value) {
			foreach ($value as $k => $val) {
				$arrPointOnLineChart[$key][0] = null;
				$arrPointOnLineChart[$key][] = $val;
			}
			$arrPointOnLineChart[$key][] = null;
		}
        return $arrPointOnLineChart;
    }

	private function arrLimitUCLOrLCL($coefficient, $averageOfChart){
       
	    foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						// foreach ($value3 as $key4 => $value4) {
							// foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] = (double)$value2 * $averageOfChart[$key][$key1][$key2][$key3];
							// }
						// }
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrMaxOfChart($coefficient , $averageOfChart){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								 $arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageOfChart[$key][$key1][$key2][$key3]) + (double)0.001;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrMinOfChart($coefficient , $averageOfChart){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageOfChart[$key][$key1][$key2][$key3]) - (double)0.001;
							}
							
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function coefficientObject($CODE_KIND, $arrayValue){
		// echo "<pre>"; print_r($CODE_KIND); echo "</pre>";
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
							// echo "<pre>"; print_r(count($value2)); echo "</pre>";
						$coefficientObject[$key][$key1][$key2][$key3] = (array)$this->getCoefficient($CODE_KIND, count($value2));
					}
				}
			}
		}

		return $coefficientObject;
	}

	private function getCoefficient($CODE_KIND, $CODE_NUM) {
		if( $CODE_NUM > 25 ){ $CODE_NUM = 25;}
		if( $CODE_NUM < 2 ) { $CODE_NUM = 1; }
		$coefficient = DB::table('tccientmst')
		->select('COEFFICIENT AS 0')
		->where('COEFF_KIND', '=', $CODE_KIND)
		->where('COEFF_NUM', '=', $CODE_NUM)
		->first();
        
		return $coefficient;
	}

	private function arrAverageValue($arrayValue){
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrAverageValue[$key][$key1][$key2][$key3] = array_sum($value2) / count($value2);
					}
				}
			}
		}
		return $arrAverageValue;
	}



	private function arrLimitUCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}
		// echo "<pre>"; print_r($coefficientArray); echo "</pre>";
		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						// echo "<pre>"; print_r($value3); echo "</pre>";
		// 				foreach ($value3 as $key4 => $value4) {
		// 					// foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] = ((double)$value3 * $averageRange[$key][$key1][$key2][$key3]) + $averageAverage[$key][$key1][$key2][$key3];
		// 					// }
		// 				}
					}
				}
			}
		}
		// echo "<pre>"; print_r($arrUCL); echo "</pre>";
        return $arrUCL;
	}

	private function arrLimitLCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						// foreach ($value3 as $key4 => $value4) {
							// foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] =  $averageAverage[$key][$key1][$key2][$key3] - ((double)$value3 * $averageRange[$key][$key1][$key2][$key3]);
						// 	}
						// }
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrMaxOfXbarChart($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageRange[$key][$key1][$key2][$key3]) + $averageAverage[$key][$key1][$key2][$key3] + (double)0.01;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrMinOfXbarChart($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ($averageAverage[$key][$key1][$key2][$key3] - ((double)$value5 * $averageRange[$key][$key1][$key2][$key3])) - (double)0.01;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrThreeToOneDimention($arrThreeDimentionValue){

		foreach ($arrThreeDimentionValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						if($key3 == 0){
							$arrOneDimentionValue[] = $value3;
						}
					}							
				}
			}
		}
        return $arrOneDimentionValue;
        
	}

	private function getLabelForMerge($valueLabel, $name){

		foreach ($valueLabel as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrForGroup[] = $value2;
				}
			}
		}
		foreach ($arrForGroup as $key => $value) {
			$arrForGraph[$key][0] = $name;
			foreach ($value as $k => $val) {
				$arrForGraph[$key][] = $val;
			}
			$arrForGraph[$key][] = "";
		}
		foreach ($arrForGraph as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$arrForMerge["group".$key]["graph".$key]["key".$key1] = [$value1];
			}
		}
        return $arrForMerge;

	}

	private function getThreeToTwoDimention($arrValue, $k){

		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == $k){
						$arrTwoDimention[$key][] = $value2;  
					}
				}
			}
		}

		return $arrTwoDimention;
	}

	private function editThreeDimention($arrValue){
		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == 0){
						$arrEdit[$key][$key1]["x"] = $value2;  
					}
					if($key2 == 1){
						$arrEdit[$key][$key1]["y"] = $value2;  
					}
				}
			}
		}
		return $arrEdit;
	}

	private function essadtest($arrValue){
		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == 0){
						$arrEdit[$key][$key1]["x"] = $value2;  
					}
					if($key2 == 1){
						$arrEdit[$key][$key1]["y"] = $value2;  
					}
				}
			}
		}
		return $arrEdit;
	}
}	