<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Session;
use Log;
use Validator;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Hash;
use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
//**************************************************************************
// screen name    maintenance user master
// over view      maintenance user master
// programer    k-kagawa
// date    2014.12.06
// update  
//           
//**************************************************************************
class ZA2020UserMasterController
extends Controller
{

	//-------------
	//■■define constance
	CONST NUMBER_PER_PAGE = 10;		//number of data per 1 page

	//**************************************************************************
	// processing name    MasterAction
	// over view      display initial screen
	//           separate processing as Entry,Search,Modify button
	//           do processing corresponding
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData					= []; //for transportion of data to screen
		
		$lTblSearchResultData		= []; //data table of inspection result list
		$lPagenation				= []; //for paging

		$lTblMasterCheck 			= []; //for master existance check

		$lMimeSetting				= ""; //set MIME

		$lMode						= ""; //lock mode of screen
		$lPrevMode					= ""; //lock mode of screen before transition

		//store and re-set entry item
		$lViewData = $this->keepFromInputValue($lViewData);
        
    // Select Team Name from database 
	// date            04/01/2018
	// author          Mai:) 
        $lViewData = $this->setTeam($lViewData);

		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		if (Input::has('btnSearch'))       //Search button
		{
			//log
			Log::write('info', 'Search Button Click.', 
				[
					"User Name"  => Input::get('txtUserNameForSearch'   ,''),
					"User ID"    => Input::get('txtUserIDForSearch'      ,''),
				]
			);

			//in case of no data,search
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search
				$lTblSearchResultData = $this->getSearchMasterData();
				
				//in case of no data,error
				if (count($lTblSearchResultData) == 0)
				{
					//set error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				
				//store in session
				Session::put('ZA2020SearchResultData', $lTblSearchResultData);

				//set lock mode in screen
				$lMode = "Search";
				Session::put('ZA2020ActionMode', "Search");
			}
		}
		elseif (Input::has('btnNewAdd'))  //New Add button
		{
			//log
			Log::write('info', 'New Add Button Click.',[]);

			//----------------------------
			//make value in edit field initial value

			//exchange session to initial value
			Session::put('ZA2020UserIDForEntry', "");
			Session::put('ZA2020UserNameForEntry', "");
			Session::put('ZA2020PasswordForEntry', "");
			Session::put('ZA2020AdminFlgForEntry', "");
    //exchange session to initial value for Team
    // date            04/01/2018
    // author          Mai:) 
            Session::put('ZA2020TeamIDForEntry', "");
            //Session::put('ZA2020arrDataTeam', "");
            Session::put('ZA2020DisplayOrderForEntry', "");
 
            //exchange view data to initial value
            $lViewData["UserIDForEntry"] = "";
            $lViewData["UserNameForEntry"] = "";
            $lViewData["PasswordForEntry"] = "";
            $lViewData["AdminFlgForEntry"] = "";
 
 
    //exchange session to initial value for Team
    // date            09/01/2018
    // author          Mai:) 
            $lViewData["TeamIDForEntry"] = "";
            $lViewData["DisplayOrderForEntry"] = "";

			//set lock mode in screen
			$lMode = "NewAdd";
			Session::put('ZA2020ActionMode', "NewAdd");

		}
		elseif (Input::has('btnResistUpload'))     //entry/update button
		{
			//log
			Log::write('info', 'Regist Button Click.', 
				[
					"User ID"       => Input::get('txtUserIDForEntry'       ,''),
					"User Name"     => Input::get('txtUserNameForEntry'     ,''),
					"Password"      => Input::get('txtPasswordForEntry'     ,''),
					"AdminFlg"      => Input::get('cmbAdminFlgForEntry'     ,''),
		    //for Team	
					"Team ID"       => Input::get('cmbTeamIDForEntry'     ,''),    
                    "DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),           
                    "ShoriMode"     => Session::get('ZA2020ActionMode'), 
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2020ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//separate processing corresponding to prevent screen
				if ($lPrevMode == "NewAdd")
				{
				//--------------
				//in case new entry
					//get data for logic check
					$lTblMasterCheck = $this->getMasterCheckData(Input::get('txtUserIDForEntry'),0);

					//in case data does not exist,start to entry
					if (count($lTblMasterCheck) == 0)
					{
						//INSERT
						$lSuccessFlg = $this->insertMasterData();

						//in case update successfully, display message and return to initial screen
						if ($lSuccessFlg == "True")
						{
							//finishing message
							$lViewData["NormalMessage"] = "I005 : Process has been completed.";

							//set lock mode in screen
							$this->initializeSessionData();
							$lMode = "";
							Session::put('ZA2020ActionMode', "");
						}
						else
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E999 : System error has occurred. Contact your system manager."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2020ActionMode', $lPrevMode);
						}
					}
					else
					//in case data exists
					{
						//exchange result to array
						$lArrCheckMaster = (Array)$lTblMasterCheck[0];

						//in case delete flag is valid,make data valid for re-entry
						if ($lArrCheckMaster["DELETE_FLG"] == "1")
						{
							//update
							$lKohshinCount = $this->updateMasterData(
																	  TRIM(Input::get('txtUserIDForEntry'))
																	 ,TRIM(Input::get('txtUserNameForEntry'))
																	 ,TRIM(Input::get('txtPasswordForEntry'     ,''))
																	 ,TRIM(Input::get('cmbAdminFlgForEntry'     ,''))
	// date            04/01/2018
	// author          Mai:)  
																	 ,TRIM(Input::get('cmbTeamIDForEntry'     ,''))

																	 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																	 ,"0"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2020ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2020ActionMode', $lPrevMode);
							}
						}
						else
						//in case delete flag is invalid,key reduplication error
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E992 : Same data is already registered."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2020ActionMode', $lPrevMode);
						}
					}
				}
				else
				{
				//----------
				//in case update

					//get search result data
					$lTblSearchResultData = Session::get('ZA2020SearchResultData');

					//set list data in session
					foreach ($lTblSearchResultData as $lCurrentRow)
					{
						//change corresponding line to array
						$lArrDataRow = (Array)$lCurrentRow;

						//get data corresponding to user ID in edit field and start process
						if(TRIM(Input::get('txtUserIDForEntry')) == TRIM((String)$lArrDataRow["USER_ID"]))
						{
							//get data for logic check
							$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["USER_ID"])
							                                             ,$lArrDataRow["DATA_REV"]
							                                            );

							$lArrCheckMaster = [];
							//in case of getting data,change corresponding line to array
							if ((count($lTblMasterCheck) != 0))
							{
								$lArrCheckMaster = (Array)$lTblMasterCheck[0];
							}

							//in case data does not exist or version is not same,error
							if ((count($lTblMasterCheck) == 0)
							     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
							   )
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E998 : Data has been updated by another terminal. Try search again."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2020ActionMode', $lPrevMode);
							}
							else
							//in case of no error,start to update
							{

								//update
								$lKohshinCount = $this->updateMasterData(
																		  TRIM(Input::get('txtUserIDForEntry'))
																		 ,TRIM(Input::get('txtUserNameForEntry'))
																		 ,TRIM(Input::get('txtPasswordForEntry'     ,''))
																		 ,TRIM(Input::get('cmbAdminFlgForEntry'     ,''))

	// date            04/01/2018
	// author          Mai:)  																		 
																		 ,TRIM(Input::get('cmbTeamIDForEntry'     ,''))

																		 ,TRIM(Input::get('txtDisplayOrderForEntry'))
																		 ,"0"
																		);

								//in case update successfully, display message and return to initial screen
								if ($lKohshinCount != 0)
								{
									//finishing message
									$lViewData["NormalMessage"] = "I005 : Process has been completed.";

									//set lock mode in screen
									$this->initializeSessionData();
									$lMode = "";
									Session::put('ZA2020ActionMode', "");
								}
								else
								{
									//set error message
									$lViewData["errors"] = new MessageBag([
										"error" => "E999 : System error has occurred. Contact your system manager."
									]);
									//keep the same condition to before update button is push for screen lock mode
									$lMode = $lPrevMode;
									Session::put('ZA2020ActionMode', $lPrevMode);
								}
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnDelete'))    //delete button
		{
			//log
			Log::write('info', 'Delete Button Click.', 
				[
					"User ID"       => Input::get('txtUserIDForEntry'       ,''),
					"User Name"     => Input::get('txtUserNameForEntry'     ,''),
					"Password"      => Input::get('txtPasswordForEntry'     ,''),
					"AdminFlg"      => Input::get('cmbAdminFlgForEntry'     ,''),
    //for Team
	// date            04/01/2018
	// author          Mai:) 
					"Team ID"       => Input::get('cmbTeamIDForEntry'     ,''),
					"DisplayOrder"  => Input::get('txtDisplayOrderForEntry'    ,''),
					"ShoriMode"     => Session::get('ZA2020ActionMode'),
				]
			);

			//error check
			$lViewData = $this->isErrorForRegist($lViewData);
			$lPrevMode = Session::get('ZA2020ActionMode');

			//in case of no error,update
			if (array_key_exists("errors", $lViewData) == false)
			{
				//----------
				//in case update

				//get search result data
				$lTblSearchResultData = Session::get('ZA2020SearchResultData');

				//set list data in session
				foreach ($lTblSearchResultData as $lCurrentRow)
				{
					//change corresponding line to array
					$lArrDataRow = (Array)$lCurrentRow;

					//get data corresponding to user ID in edit field and start process
					if(TRIM(Input::get('txtUserIDForEntry')) == TRIM((String)$lArrDataRow["USER_ID"]))
					{
						//get data for logic check
						$lTblMasterCheck = $this->getMasterCheckData(TRIM((String)$lArrDataRow["USER_ID"])
						                                             ,$lArrDataRow["DATA_REV"]
						                                            );

						$lArrCheckMaster = [];
						//in case of getting data,change corresponding line to array
						if ((count($lTblMasterCheck) != 0))
						{
							$lArrCheckMaster = (Array)$lTblMasterCheck[0];
						}

						//in case data does not exist or version is not same,error
						if ((count($lTblMasterCheck) == 0)
						     or ($lArrDataRow["DATA_REV"] != $lArrCheckMaster["DATA_REV"])
						   )
						{
							//set error message
							$lViewData["errors"] = new MessageBag([
								"error" => "E998 : Data has been updated by another terminal. Try search again."
							]);
							//keep the same condition to before update button is push for screen lock mode
							$lMode = $lPrevMode;
							Session::put('ZA2020ActionMode', $lPrevMode);
						}
						else
						//in case of no error,start to update
						{

							//update
							$lKohshinCount = $this->updateMasterData(
																	  $lArrDataRow["USER_ID"]
																	 ,$lArrDataRow["USER_NAME"]
																	 ,$lArrDataRow["PASSWORD"]
																	 ,$lArrDataRow["ADMIN_FLG"]
	// date            04/01/2018
	// author          Mai:)  																	 
																	 ,$lArrDataRow["TEAM_ID"]

																	 ,$lArrDataRow["DISPLAY_ORDER"]
																	 ,"1"
																	);

							//in case update successfully, display message and return to initial screen
							if ($lKohshinCount != 0)
							{
								//finishing message
								$lViewData["NormalMessage"] = "I005 : Process has been completed.";

								//set lock mode in screen
								$this->initializeSessionData();
								$lMode = "";
								Session::put('ZA2020ActionMode', "");
							}
							else
							{
								//set error message
								$lViewData["errors"] = new MessageBag([
									"error" => "E999 : System error has occurred. Contact your system manager."
								]);
								//keep the same condition to before update button is push for screen lock mode
								$lMode = $lPrevMode;
								Session::put('ZA2020ActionMode', $lPrevMode);
							}
						}
					}
				}
			}
		}
		elseif (Input::has('btnModify'))  //Modify button
		{
			//log
			Log::write('info', 'Modify Button Click.', 
				[
					"hidUserID"  => Input::get('hidPrimaryKey1' ,''),
				]
			);

			//get primary key in corresponding line
			$lUserID = Input::get('hidPrimaryKey1');
			//get search result data
			$lTblSearchResultData = Session::get('ZA2020SearchResultData');

			//set list data in session
			foreach ($lTblSearchResultData as $lCurrentRow)
			{
				//change corresponding line to array
				$lArrDataRow = (Array)$lCurrentRow;

				//in case userID exists,write over on lViewData and session to set in edit field
				if(TRIM((String)$lUserID) == TRIM((String)$lArrDataRow["USER_ID"]))
				{
					//write down in session
					Session::put('ZA2020UserIDForEntry', $lArrDataRow["USER_ID"]);
					Session::put('ZA2020UserNameForEntry', $lArrDataRow["USER_NAME"]);
					Session::put('ZA2020PasswordForEntry', $lArrDataRow["PASSWORD"]);
					Session::put('ZA2020AdminFlgForEntry', $lArrDataRow["ADMIN_FLG"]);
	// date            04/01/2018
	// author          Mai:)  					
					Session::put('ZA2020TeamIDForEntry', $lArrDataRow["TEAM_ID"]);
					Session::put('ZA2020DisplayOrderForEntry', $lArrDataRow["DISPLAY_ORDER"]);

					//exchange view data in edit field
					$lViewData["UserIDForEntry"] = Session::get('ZA2020UserIDForEntry');
					$lViewData["UserNameForEntry"] = Session::get('ZA2020UserNameForEntry');
					$lViewData["PasswordForEntry"] = Session::get('ZA2020PasswordForEntry');
					$lViewData["AdminFlgForEntry"] = Session::get('ZA2020AdminFlgForEntry');
	// date            04/01/2018
	// author          Mai:)   					
					$lViewData["TeamIDForEntry"] = Session::get('ZA2020TeamIDForEntry');
					$lViewData["DisplayOrderForEntry"] = Session::get('ZA2020DisplayOrderForEntry');
				}
			}
            
			//set lock mode in screen
			$lMode = "Edit";
			Session::put('ZA2020ActionMode', "Edit");
		}
		else  //transition from other screen or menu,paging
		{
		
			//clear all information except entry information in this screen session
			//in case URL in origin of transition does not include"index.php/user/usermaster",False
			//in case origin of transition is other,return string
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/usermaster');

				if($lPrevURL == false)
				{
					//delete all search information
					$this->initializeSessionData();

					//set lock mode in screen
					$lMode = "";
					Session::put('ZA2020ActionMode', "");
				}
				else
				{
					//if search result data does not exist in session,issue  blank
					if (is_null(Session::get('ZA2020SearchResultData')))
					{
						//set lock mode in screen
						$lMode = "";
						Session::put('ZA2020ActionMode', "");
					}
					else
					{
						//set lock mode in screen
						$lMode = "Search";
						Session::put('ZA2020ActionMode', "Search");
					}
				}
			}
		}

		//if search result data does not exist in session,issue  blank

		if (is_null(Session::get('ZA2020SearchResultData')))
		{
			$lTblSearchResultData = [];
		}
		else
		{
			//get search result data from session
			$lTblSearchResultData = Session::get('ZA2020SearchResultData');
		}
 
		//make pagenation
		$lPagenation = new LengthAwarePaginator ($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);
		$lPagenation->setPath(url('user/usermaster'));
		// $currentPage = LengthAwarePaginator::resolveCurrentPage();

		//add to array for transportion to screen
		$lViewData += [
			"PersonCount"     => Count($lTblSearchResultData),
			"Pagenator"       => $lPagenation,
		];

		//set lock information in screen
		$lViewData = $this->lockControls($lViewData,Session::get('ZA2020ActionMode'));

		// echo "<pre>";
		// print_r($lViewData);
		// echo "</pre>";
		
		return View("user.usermaster", $lViewData);
	}

	//**************************************************************************
	// processing name    lockControls
	// over view      ボタン等の画面の各項目のロック情報を設定する。
	// parameter      ビューデータ・画面のモード
	// returned value    ビューデータ
	//**************************************************************************
	private function lockControls($pViewData,$pMode)
	{
		//画面のモードに応じて処理を分岐
		switch ($pMode)
		{
			case 'Search':
				//検索 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			case 'NewAdd':
				//新規追加 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "",
				];
				break;
			case 'Edit':
				//編集 button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "",
					"NewAddLock"   => "Lock",
					"DeleteLock"   => "",
					"EditVisible"  => "True",
					"MachineNoEditLock" => "Lock",
				];
				break;
			case 'Regist':
				//entry/update button時
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "",
				];
				break;
			default:
				//全てに該当しない場合(初期処理時)
				$pViewData += [
					"SearchLock"    => "",
					"RegistLock"    => "Lock",
					"NewAddLock"   => "",
					"DeleteLock"   => "Lock",
					"EditVisible"  => "",
					"MachineNoEditLock" => "Lock",
				];
		}
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getMasterCheckData
	// over view      for master existance checkの物理キーに該当するデータを取得する
	// parameter      マスタのキー値
	//           データ版数）
	// returned value    取得結果のテーブル
	//**************************************************************************
	private function getMasterCheckData($pUserid,$pDataRev)
	{
		$lTblSearchResultData = [];       //DataTable

		$lTblSearchResultData = DB::select
		('
		    SELECT  USER.DATA_REV
		           ,USER.DELETE_FLG
		      FROM TUSERMST AS USER														/* ユーザーマスタ */
		     WHERE USER.USER_ID         = :Userid										/* 画面入力値 */
		       AND USER.DATA_REV        = IF(:DataRev1 <> 0, :DataRev2, USER.DATA_REV)	/* 無ければ条件にしない */
		',
			[
				"Userid"   => $pUserid,
				"DataRev1" => $pDataRev,
				"DataRev2" => $pDataRev,
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    getSearchMasterData
	// over view      検索時の一覧用のマスタデータを取得する
	// parameter      nothing
	// returned value    Array
	// programer    k-kagawa
    //**************************************************************************
	private function getSearchMasterData()
	{
		$lTblSearchResultData          = []; //data table of inspection result list

		$lTblSearchResultData = DB::select
		('
		    SELECT USEM.USER_ID
		          ,USEM.USER_NAME
		          ,USEM.PASSWORD
		          ,USEM.ADMIN_FLG
		          ,CASE USEM.ADMIN_FLG 
		                WHEN "1" THEN "Administrator"
		                ELSE "Not Administrator"
		            END AS ADMIN_FLG_NAME
	-- // date            04/01/2018
	-- // author          Mai:) 		          
		          ,USEM.TEAM_ID
		          ,TCODE.CODE_NAME
		         
		          ,USEM.DISPLAY_ORDER
		          ,USEM.DELETE_FLG
		          ,USEM.DATA_REV
		      FROM TUSERMST AS USEM  
-- // date            09/01/2018
-- // author          Mai:) 		      
		    INNER JOIN TCODEMST AS TCODE
			   ON USEM.TEAM_ID = TCODE.CODE_ORDER
			  AND TCODE.CODE_CLASS          = "004"
--                                            /* ユーザマスタ */
		     WHERE USEM.DELETE_FLG          = "0"                               /* deleteフラグ */
		       AND USEM.USER_ID             LIKE :Userid                        /* ユーザID(前方一致) */
		       AND USEM.USER_NAME           LIKE :Username                      /* ユーザー名(部分一致) */
		  ORDER BY USEM.DISPLAY_ORDER
		          ,USEM.USER_ID
		',
			[
				"Userid"      => TRIM((String)Input::get('txtUserIDForSearch'))."%",
				"Username"    => "%".TRIM((String)Input::get('txtUserNameForSearch'))."%",
			]
		);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// processing name    insertMasterData
	// over view      マスタに対して新規登録(INSERT処理を行う)
	// parameter      nothing
	// returned value    処理の成否の値(成功時：True)
	//**************************************************************************
	private function insertMasterData()
	{
		//登録用の結果をセット
		$lSuccessFlg = "";

		//登録項目を編集部より取得
		$lUserID = Input::get('txtUserIDForEntry');
		$lUserName = Input::get('txtUserNameForEntry');
		$lPassword = Input::get('txtPasswordForEntry');
		$lPasswordForUserMaster = Hash::make($lPassword);
		$lAdminflg = Input::get('cmbAdminFlgForEntry');
	// date            04/01/2018
	// author          Mai:) 	
		$lTeamID = Input::get('cmbTeamIDForEntry');
		$lDisplayOrder = Input::get('txtDisplayOrderForEntry');

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lSuccessFlg = DB::transaction
		(
			function() use	($lUserID
							 ,$lUserName
							 ,$lPassword
							 ,$lPasswordForUserMaster
							 ,$lAdminflg			 
							 ,$lTeamID
							 ,$lDisplayOrder
							)
			{
				$lSuccessFlg = DB::table('TUSERMST')->insert(
						[
							'USER_ID' => $lUserID
							,'USER_NAME' => $lUserName
							,'PASSWORD' => $lPassword
							,'ADMIN_FLG' => $lAdminflg
							,'TEAM_ID' => $lTeamID
							,'DISPLAY_ORDER' => $lDisplayOrder
							,'DELETE_FLG' => "0"
							,'DATA_REV' => 1
						]
					);
				
				
				//本体のユーザマスタが登録出来た場合はLaravel用ユーザマスタを登録
				if ($lSuccessFlg == "True")
				{
					$lSuccessFlg = DB::table('users')->insert(
						[
							'user_id' => $lUserID
							,'user_name' => $lUserName
							,'password' => $lPasswordForUserMaster
							,'email' => ''
							,'created_at' => date('Y-m-d H:i:s')
							,'updated_at' => date('Y-m-d H:i:s')
							,'remember_token' => ''
						]
					);
				}

				return $lSuccessFlg;
			}
			
		);
		
	    return $lSuccessFlg;
	    
	}

	//**************************************************************************
	// processing name    updateMasterData
	// over view      マスタに対して更新処理
	// parameter      データ版数を除く該当マスタの全項目(parameterをそのまま更新)
	// returned value    実行結果の成否
	//**************************************************************************
	private function updateMasterData
	(
	  $pUserid
	 ,$pUserName
	 ,$pPassword
	 ,$pAdminflg
	// date            04/01/2018
	// author          Mai:) 	 
	 ,$pTeamid
	//
	 ,$pDisplayOrder
	 ,$pDeleteFlg
	)
	{

		//更新の成否をセット
		$lKohshinCnt = "";

		//パスワードを暗号化
		$lPasswordForUserMaster = Hash::make($pPassword);

		//全処理を1トランザクションで処理するため、手動でトランザクションを記載
		$lKohshinCnt = DB::transaction
		(
			function() use (
							 $pUserid
							,$pUserName
							,$pPassword
							,$lPasswordForUserMaster
							,$pAdminflg				
							,$pTeamid
							,$pDisplayOrder
							,$pDeleteFlg
						   )
			{
				$lKohshinCnt = DB::update
				('
						UPDATE TUSERMST
						   SET  USER_ID			= :Userid
						       ,USER_NAME		= :UserName
						       ,PASSWORD		= :Password
						       ,ADMIN_FLG		= :AdminFlg   
						       ,TEAM_ID         = :Teamid
						       ,DISPLAY_ORDER	= :DisplayOrder
						       ,DELETE_FLG		= :DeleteFlg
						       ,DATA_REV		= DATA_REV + 1
						 WHERE USER_ID	= :UseridWhere
				', 
					[
						"Userid"		=> $pUserid,
						"UseridWhere"	=> $pUserid,
						"UserName"		=> $pUserName,
						"Password"		=> $pPassword,
						"AdminFlg"		=> $pAdminflg,
						"Teamid"        => $pTeamid,
						"DisplayOrder"	=> $pDisplayOrder,
						"DeleteFlg"		=> $pDeleteFlg,
					]
				);

				//更新が成功した場合はパスワードの変更を考慮し、Laravel用ユーザマスタを更新する
				if($lKohshinCnt != 0)
				{
					DB::table('users')
 					->where('user_id',$pUserid)
					->update(
						[
							'user_name' => $pUserName
							,'password' => $lPasswordForUserMaster
							,'updated_at' => date('Y-m-d H:i:s')
						]
					);
				}
				return $lKohshinCnt;
			}
		);

	    return $lKohshinCnt;
	    
	}

	//**************************************************************************
	// processing name    keepFromInputValue
	// over view      検索結果一覧画面の、フォーム入力情報の保持や画面への返却を行う
	// parameter      Arary
	// returned value    Array
	//**************************************************************************
	private function keepFromInputValue($pViewData)
	{
				//■登録用・User IDの項目保持
		if (Input::has('txtUserIDForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020UserIDForEntry', Input::get('txtUserIDForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020UserIDForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"UserIDForEntry"  => Session::get('ZA2020UserIDForEntry')
		];
		
		//■登録用・User Nameの項目保持
		if (Input::has('txtUserNameForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020UserNameForEntry', Input::get('txtUserNameForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020UserNameForEntry', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"UserNameForEntry"  => Session::get('ZA2020UserNameForEntry')
		];

		//■登録用・パスワードの項目保持
		if (Input::has('txtPasswordForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020PasswordForEntry', Input::get('txtPasswordForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020PasswordForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"PasswordForEntry"  => Session::get('ZA2020PasswordForEntry')
		];

		//■登録用・管理者フラグの項目保持
		if (Input::has('cmbAdminFlgForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020AdminFlgForEntry', Input::get('cmbAdminFlgForEntry'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020AdminFlgForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"AdminFlgForEntry"  => Session::get('ZA2020AdminFlgForEntry')
		];
	// for Team
	// date            04/01/2018
	// author          Mai:) 
		if (Input::has('cmbTeamIDForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020TeamIDForEntry', Input::get('cmbTeamIDForEntry'));

		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020TeamIDForEntry', "");
		}
		
		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"TeamIDForEntry"  => Session::get('ZA2020TeamIDForEntry')
		];


		//■登録用・DisplayOrderの項目保持
		if (Input::has('txtDisplayOrderForEntry'))
		{
			//画面の値があるならwrite down in session
			Session::put('ZA2020DisplayOrderForEntry', Input::get('txtDisplayOrderForEntry'));
		}
		else
		{
			//画面の値が無ければサーバ日付をwrite down in session。
			Session::put('ZA2020DisplayOrderForEntry', "");
		}

		//セッションには画面値か業務日付が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"DisplayOrderForEntry"  => Session::get('ZA2020DisplayOrderForEntry')
		];

		//■検索用・User IDの項目保持
		//in case value does not exist in session
		if (Input::has('txtUserIDForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2020UserNoForSearch', Input::get('txtUserIDForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020UserNoForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"UserNoForSearch"  => Session::get('ZA2020UserNoForSearch')
		];
		
		//■検索用・User Nameの項目保持
		//in case value does not exist in session
		if (Input::has('txtUserNameForSearch')) {
			//画面の値があるならwrite down in session
			Session::put('ZA2020UserNameForSearch', Input::get('txtUserNameForSearch'));
		}
		else
		{
			//画面の値が無ければ空白で書き込んでおく
			Session::put('ZA2020UserNameForSearch', "");
		}

		//セッションには画面値か空白が絶対にあるのでそれを取得して画面に送り返す
		$pViewData += [
				"UserNameForSearch"  => Session::get('ZA2020UserNameForSearch')
		];

		return $pViewData;
	}

	//**************************************************************************
	// processing name    initializeSessionData
	// over view      セッションデータのクリア（検索時・編集時の値）
	// parameter      nothing
	// returned value    nothing
	//**************************************************************************
	private function initializeSessionData()
	{
		//検索結果をクリア
		Session::forget('ZA2020SearchResultData');

		//Entry button時に設定しているものをクリア
		Session::forget('ZA2020UserIDForEntry');
		Session::forget('ZA2020UserNameForEntry');
		Session::forget('ZA2020PasswordForEntry');
		Session::forget('ZA2020AdminFlgForEntry');
	// for Team
	// date            09/01/2018
	// author          Mai:) 
		Session::forget('ZA2020TeamIDForEntry');
		Session::forget('ZA2020DisplayOrderForEntry');

		return null;
	}

	//**************************************************************************
	// processing name    isErrorForRegist
	// over view      登録時に実施する必須・型式チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	//**************************************************************************
	private function isErrorForRegist($pViewData)
	{
		//-------------------------
		//必須チェック
		//-------------------------
		//User ID必須チェック
		$lValidator = Validator::make(
			array('txtUserIDForEntry' => Input::get('txtUserIDForEntry')),
			array('txtUserIDForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter User ID ."
			]);
			return $pViewData;
		}

		//ユーザー名必須チェック
		$lValidator = Validator::make(
			array('txtUserNameForEntry' => Input::get('txtUserNameForEntry')),
			array('txtUserNameForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter User Name."
			]);
			return $pViewData;
		}

		//パスワード必須チェック
		$lValidator = Validator::make(
			array('txtPasswordForEntry' => Input::get('txtPasswordForEntry')),
			array('txtPasswordForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Password."
			]);
			return $pViewData;
		}

		//管理者フラグ必須チェック
		$lValidator = Validator::make(
			array('cmbAdminFlgForEntry' => Input::get('cmbAdminFlgForEntry')),
			array('cmbAdminFlgForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Administrator."
			]);
			return $pViewData;
		}

    // set error message for Team	
	// date            04/01/2018
	// author          Mai:) 
		$lValidator = Validator::make(
			array('cmbTeamIDForEntry' => Input::get('cmbTeamIDForEntry')),
			array('cmbTeamIDForEntry' => array('required'))
		);

		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Team."
			]);
			return $pViewData;
		}

		//表示順必須チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E993 : Enter Display Order."
			]);
			return $pViewData;
		}

		//-------------------------
		//型式チェック
		//-------------------------
		//User ID型チェック
		$lValidator = Validator::make(
			array('txtUserIDForEntry' => Input::get('txtUserIDForEntry')),
			array('txtUserIDForEntry' => array('alpha_dash'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : User ID is invalid."
			]);
			return $pViewData;
		}

		//パスワード型チェック
		$lValidator = Validator::make(
			array('txtPasswordForEntry' => Input::get('txtPasswordForEntry')),
			array('txtPasswordForEntry' => array('alpha_dash'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Password is invalid."
			]);
			return $pViewData;
		}

    // set error message for Team	
	// date            05/01/2018
	// author          Mai:) 
		$lValidator = Validator::make(
			array('txtUserIDForEntry' => Input::get('txtUserIDForEntry')),
			array('txtUserIDForEntry' => array('alpha_dash'))
		);

		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Team is invalid."
			]);
			return $pViewData;
		}

		//表示順型チェック
		$lValidator = Validator::make(
			array('txtDisplayOrderForEntry' => Input::get('txtDisplayOrderForEntry')),
			array('txtDisplayOrderForEntry' => array('integer'))
		);
		//エラーの場合
		if ($lValidator->fails())
		{
			//set error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E994 : Display Order is invalid."
			]);
			
			return $pViewData;
		}

		return $pViewData;
	}

	//**************************************************************************
	// process         setTeam
	// overview        
	// argument        
	// return value    
	// date            04/01/2018
	// author          Mai:)        
	//**************************************************************************
	private function setTeam($pViewData)
	{
		$lArrTeam    = ["" => ""];  //view data to return

		$lArrTeam = $this->getTeam();

		//store in session
		Session::put('ZA2020arrDataTeam', $lArrTeam); 

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataTeam" => $lArrTeam
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getTeam
	// overview        
	// argument        
	// return value    
	// date            04/01/2018
	// author          Mai:)   
	//**************************************************************************
	private function getTeam()
	{
		$lTblTeam			= []; 
		$lRowTeam			= []; 
		$lArrDataTeam		= []; 
		$lTblTeam = DB::select(
		'
			SELECT PROCESS.CODE_ORDER
			     , PROCESS.CODE_NAME
			     , PROCESS.DISPLAY_ORDER
			  FROM TCODEMST AS PROCESS
			 WHERE CODE_CLASS = "004"
			 ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblTeam = (array)$lTblTeam;

		//add blank space
		$lArrDataTeam += [
			"" => ""
		];

		//data exist
		if ($lTblTeam != null)
		{
			//store result in Array again
			foreach ($lTblTeam as $lRowTeam) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowTeam = (Array)$lRowTeam;

				$lArrDataTeam += [
					$lRowTeam["CODE_ORDER"] => $lRowTeam["CODE_NAME"]
				];
			}

		}

		return $lArrDataTeam;
	}

}