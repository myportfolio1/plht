<?php

use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';

class BA1010ListController
extends Controller
{
	//data count per 1 page
	CONST NUMBER_PER_PAGE = 10;

	CONST CONDITION_NORMAL     = "01";
	CONST INSPECTION_TIME_ALL  = "ZZ";


	//Inspection Result Form for Approval
	CONST SHONINEXCEL_HEADER_STARTROW = 4; //line number for header
	CONST SHONINEXCEL_MEISAI_STARTROW = 7; //start line number for detail

	//NG Result
	CONST NGRESULTEXCEL_HEADER_STARTROW = 3; //line number for header
	CONST NGRESULTEXCEL_MEISAI_STARTROW = 7; //start line number for detail

	//Inspection Sheet
	CONST INSPECTIONSHEETEXCEL_PIC_MAX = 15;
	CONST INSPECTIONSHEETEXCEL_DETAIL_COUNT  = 38;
	CONST INSPECTIONSHEETEXCEL_COL_OFFSET = 0;
	CONST INSPECTIONSHEETEXCEL_ROW_OFFSET = 52;
	//CONST INSPECTIONSHEETEXCEL_HEADER_STARTCOL      = XX;  //line number for header (列 0から開始)
	CONST INSPECTIONSHEETEXCEL_HEADER_STARTROW    = 1;  //line number for header (行 1から開始)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTCOL    = 0;  //line number for detail(列)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTROW    = 52; //line number for detail(行)
	CONST LOGOPATH     = "img/CompanyLogo.png";
	CONST NOTHING      = "-";
	CONST BLANK        = " ";
	CONST OK           = "OK";
	CONST NG           = "NG";
	CONST STAR         = "★";
	CONST CEL_E        = "4";

	CONST RESULT_INPUT_TYPE_NUMBER   = "1";
	CONST RESULT_INPUT_TYPE_OKNG     = "2";

	//Data import
	CONST MITUTOYO_CRYSTAAPEX_S544   = "001";





	//**************************************************************************
	// process    ListAction
	// overview   Display screen for inspection result
	//           And process as Entry,Search,CSV buttuns
	// argument   Nothing
	// return value    Nothing
	// author    s-miyamoto
	// date    14.05.2014
	// record of updates  14.05.2014 v0.01 make first
	//           26.08.2014 v1.00 FIX
	//**************************************************************************
	public function ListAction()
	{
		$lViewData               = []; //Array for transition to screen
		$lPagenation             = []; //For Paging（Return to screen）
		$lTblSearchResultData    = []; //Data table of inspection result
		$lTblNotExistNoData      = []; //Data table of Inspection No(No Data)

		$lTblCheckSheetInfo      = []; //DataTable of Inspection Check Sheet
		$lRowCheckSheetInfo      = []; //DataRow of Inspection Check Sheet

		$lTblMachine             = []; //DataTable of Machine
		$lRowMachine             = []; //DataRow of Machine
		$lTblInspector           = []; //DataTable of inspecter
		$lRowInspector           = []; //DataRow of inspecter
		// $lTblCustomerID          = [];
		// $lRowCustomerID          = [];

		$lInspectionResultNo     = ""; //PK of line
		$lDataRev                = ""; //DataRevision of line

		$lTblModifyTargetData    = []; //DataTable for Modify
		$lRowModifyTargetData    = []; //DataRow for Modify

		//CSV
		$lTblInspectionResultCSV = []; //Datatable of inspection result(CSV)
		$lOutputCSVData          = ""; //CSV data for output
		$lMimeSetting            = ""; //MIME setting

		$lDataCount              = 0;  //data counter for redundant logic key
		$lCheckNoCount           = 0;  //number of Inspection No. corresponding to the Time
		$lDeleteCount            = 0;  //number of data deleted
		$lSheetMasterDataCount   = 0;  //data counter for redundant logic key

		//Excel template File Name
		$lTemplateFileNameShonin     = ""; //set template file name for approval
		$lTemplateFileNameNGResult   = ""; //set template file name for NGResult

		//NG Result
		$lWorkINSPECTIONSHEET        = "";
		$lWorkMACHINENO              = "";
		$lWorkMOLDNO                 = "";
		$lWorkINSPECTIONPOINT        = "";
		$lWorkANAGRP01               = "";
		$lWorkANAGRP02               = "";
		$NGFLG                       = 0;  //Flag to output

		//Inspection Sheet
		$lWork1InspectionPoint      = "";
		$lWork1AnalyGrp01           = "";
		$lWork1AnalyGrp02           = "";
		$lWork2InspectionPoint      = "";
		$lWork2AnalyGrp01           = "";
		$lWork2AnalyGrp02           = "";

		$lWorkPicCount = 0 ;
		$lWorkCount                      = 0;
		$lWorkResultCount                      = 0;
		$lWorkJudge                           = 0;
		$lFinalJudge              = 0;
		$lWorkFinalJudge   = 0;
		$lFinalJudge     = "";
$lPageCount = 1;
$lRemainder = 0;
$lWorkValue   = "";
$lWorkPosition   = 0;



		//Value to make Combo(Set value from session when transit to screen)
		$lCmbValProcessForEntry = "";
		$lCmbValCustomerForEntry = "";
		$lCmbValCheckSheetNoForEntry = "";
		$lCmbValProcessForSearch = "";
		$lCmbValCustomerForSearch = "";
		$lCmbValCheckSheetNoForSearch = "";




		//store value of entering to screen
		Input::flash();

		//get parameter from login screen throgh session and issue to array for transpotion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];
        
		//echo "<pre>";
		//print_r($lViewData);
		//echo "</pre>";

		//Inspection No List of Entry screen and Equipment list(because it must delete at transition to other screen)
		//clear at first of List screen
		//If data was deleted while Return button in Entry screen, data can't delete from menu
		Session::forget('BA2010InspectionNoData');
		Session::forget('BA2010InspectionToolClassDropdownListData');


		if (Input::has('btnEntry'))                                             //Push Entry button
		{
			//log
			Log::write('info', 'BA1010 Entry Button Click.',
				[
					"Process Id"			=> Input::get('cmbProcessForEntry'   ,''),
					"Customer Id"			=> Input::get('cmbCustomerForEntry'   ,''),
					"Check Sheet No."		=> Input::get('cmbCheckSheetNoForEntry'   ,''),
					"Material Name"			=> Input::get('cmbMaterialNameForEntry'   ,''),
					"Material Size"			=> Input::get('cmbMaterialSizeForEntry'   ,''),
					"M/C No."				=> Input::get('txtMachineNoForEntry'      ,''),
					"Mold No."				=> Input::get('txtMoldNoForEntry'      ,''),
					"Inspector Code"		=> Input::get('txtInspectorCodeForEntry'  ,''),
					"Inspection Date"		=> Input::get('txtInspectionDateForEntry' ,''),
					"Condition Id"			=> Input::get('cmbConditionForEntry'      ,''),
					"Inspection Time Id"	=> Input::get('cmbInspectionTimeForEntry' ,''),
				]
			);

			//error check
			$lViewData = $this->isErrorForEntry($lViewData);

			//check inspection check sheet No. except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//check inspection check sheet No. and get max Revision No.
				$lTblCheckSheetInfo = $this->getCheckSheetData(Input::get('cmbCheckSheetNoForEntry'));

				//echo "<pre>";
				//print_r($lTblCheckSheetInfo);
				//echo "</pre>";

				//get data in 0 line（if no data,get NULL because query use MAX　if data exist, it must be one）
				$lRowCheckSheetInfo = (array)$lTblCheckSheetInfo[0];

				//data exist
				if ($lRowCheckSheetInfo["INSPECTION_SHEET_NO"] != null)
				{
					//store search result in session to transit to PreEntry screen
					//Get screen item put Session →　Programming at the bottom　(Tentatively also here)
					Session::put('BA1010ProcessIdForEntry', $lRowCheckSheetInfo["PROCESS_ID"]);
					Session::put('BA1010ProcessName', $lRowCheckSheetInfo["PROCESS_NAME"]);
					Session::put('BA1010CustomerIdForEntry', $lRowCheckSheetInfo["CUSTOMER_ID"]);
					Session::put('BA1010CustomerName', $lRowCheckSheetInfo["CUSTOMER_NAME"]);
					Session::put('BA1010CheckSheetNoForEntry', $lRowCheckSheetInfo["INSPECTION_SHEET_NO"]);
					Session::put('BA1010CheckSheetName', $lRowCheckSheetInfo["INSPECTION_SHEET_NAME"]);
					Session::put('BA1010MaterialNameForEntry', $lRowCheckSheetInfo['MATERIAL_NAME']);
					Session::put('BA1010MaterialSizeForEntry', $lRowCheckSheetInfo['MATERIAL_SIZE']);
					//Machine →　Programming at the bottom
					Session::put('BA1010MoldNoForEntry', TRIM((String)Input::get('txtMoldNoForEntry')));
					//Inspector →　Programming at the bottom
					//InspectionDate →　Programming at the bottom
					//Condition →　Programming at the bottom
					//InspectionTime →　Programming at the bottom

					Session::put('BA1010RevisionNo', $lRowCheckSheetInfo["MAX_REV_NO"]);
					Session::put('BA1010ModelName', $lRowCheckSheetInfo["MODEL_NAME"]);
					Session::put('BA1010ItemNo', $lRowCheckSheetInfo["ITEM_NO"]);
					Session::put('BA1010ItemName', $lRowCheckSheetInfo["ITEM_NAME"]);
					Session::put('BA1010DrawingNo', $lRowCheckSheetInfo["DRAWING_NO"]);
					Session::put('BA1010ISOKanriNo', $lRowCheckSheetInfo["ISO_KANRI_NO"]);
					Session::put('BA1010ProductWeight', $lRowCheckSheetInfo["PRODUCT_WEIGHT"]);
					Session::put('BA1010HeatTreatmentType', $lRowCheckSheetInfo['HEAT_TREATMENT_TYPE']);
					Session::put('BA1010PlatingKind', $lRowCheckSheetInfo["PLATING_KIND"]);
					Session::put('BA1010ColorId', $lRowCheckSheetInfo["COLOR_ID"]);
					Session::put('BA1010ColorName', $lRowCheckSheetInfo["COLOR_NAME"]);
				}
				//data does no exist
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E008 : Check Sheet No. is not registered in Master. "
					]);
				}
			}

			//check M/C No. except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//check M/C No. and get M/C Name
				$lTblMachine = $this->getMachineData(Input::get('txtMachineNoForEntry'));

				if ($lTblMachine != null)
				{
					//data exist
					//get data in 0 line
					$lRowMachine = (array)$lTblMachine[0];

					//store search result in session to transit to PreEntry screen
					Session::put('BA1010MachineNoForEntry', TRIM((String)Input::get('txtMachineNoForEntry')));
					Session::put('BA1010MachineName', $lRowMachine["MACHINE_NAME"]);
				}
				else
				{
					//data does not exist
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E011 : M/C No. is not registered in Master. "
					]);
				}
			}

			//check Inspector Code except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//check Inspector Code and get name of inspector
				$lTblInspector = $this->getInspectorData(Input::get('txtInspectorCodeForEntry'));

				if ($lTblInspector != null)
				{
					//data exist
					//get data in 0 line
					$lRowInspector = (array)$lTblInspector[0];

					//store session search result to transit to PreEntry screen
					Session::put('BA1010InspectorCodeForEntry', TRIM((String)Input::get('txtInspectorCodeForEntry')));
					Session::put('BA1010InspectorName', $lRowInspector["USER_NAME"]);
					Session::put('BA1010TeamId', $lRowInspector["TEAM_ID"]);
					Session::put('BA1010TeamName', $lRowInspector["TEAM_NAME"]);
				}
				else
				{
					//data does not exist
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E014 : Inspector Code is not registered in Master."
					]);
				}
			}

			//select master data (use input item)
			if (array_key_exists("errors", $lViewData) == false)
			{

				$lSheetMasterDataCount = $this->getInspectionSheetMasterDataCount(Input::get('cmbProcessForEntry') ,Input::get('cmbCustomerForEntry') ,Input::get('cmbCheckSheetNoForEntry'));

				if ($lSheetMasterDataCount == 0)
				{
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else if ($lSheetMasterDataCount == 1)
				{
					//何もしない
				}
				else
				{
					//RevNoがupする場合は2以上になる為、コメント
					//$lViewData["errors"] = new MessageBag([
					//	"error" => "E997 : Target data does not exist."
					//]);
				}
			}

			//check duplication data in the same logic key except for no error
			//prompt to continue to regist from Modify in case of having registed by same logic key already
			//他の項目でエラーがない場合　同一論理キーの重複データチェック
			//既に同一論理キーで登録されている場合は、検索一覧のModifyの方から登録の続きを行うように促す
			if (array_key_exists("errors", $lViewData) == false)
			{
				//Condition combo＝nomal(01)⇒check,else⇒no check（available to regist duplication in the same time）
				If (Input::get('cmbConditionForEntry') == self::CONDITION_NORMAL)
				{
					//get count of same logic key
					$lDataCount = $this->getLogicalDuplicationDataCount(Input::get('cmbCheckSheetNoForEntry') ,$lRowCheckSheetInfo["MAX_REV_NO"] ,Input::get('cmbProcessForEntry') ,Input::get('txtMachineNoForEntry') ,Input::get('txtMoldNoForEntry') ,Input::get('txtInspectorCodeForEntry') ,Input::get('txtInspectionDateForEntry') ,Input::get('cmbConditionForEntry') ,Input::get('cmbInspectionTimeForEntry'));

					if ($lDataCount > 0)
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E031 : The data of the time is already registered. Search the data and retry the process with Modify Button. "
						]);
					}
				}
			}
Log::write('info', '13');
			//check inspection corresponding to Inspection Time except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				If (Input::get('cmbConditionForEntry') == self::CONDITION_NORMAL) 
				{
					Log::write('info', '14');
					//get count of inspection No.data corresponding to time
					$lCheckNoCount = $this->getInspectionTimeMasterDataCount(Input::get('cmbCheckSheetNoForEntry') ,$lRowCheckSheetInfo["MAX_REV_NO"] ,Input::get('cmbInspectionTimeForEntry'));
				}
				else
				{
					//get count of inspection No.data corresponding to time
					$lCheckNoCount = $this->getInspectionTimeMasterDataCount(Input::get('cmbCheckSheetNoForEntry') ,$lRowCheckSheetInfo["MAX_REV_NO"] ,self::INSPECTION_TIME_ALL);
				}

				//display error of mischosing time in case of no data
				if ($lCheckNoCount == 0)
				{
					$lViewData["errors"] = new MessageBag([
						"error" => "E032 : You mischose the time of Inspection."
					]);
				}
			}

			//transit screen except no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				$lArrDataListCondition = ["" => ""];  //List to return Condition data to screen
				$lArrDataListCondition = Session::get('BA1010ConditionDropdownListData');

				$lArrDataListInspectionTime = ["" => ""];  //List to return Inspection Time data to screen
				$lArrDataListInspectionTime = Session::get('BA1010InspectionTimeDropdownListData');

				//Entry
				Session::put('BA1010InspectionDateForEntry'		, Input::get('txtInspectionDateForEntry'));
				Session::put('BA1010ConditionIdForEntry'		, Input::get('cmbConditionForEntry'));
				Session::put('BA1010ConditionName'				, $lArrDataListCondition[Input::get('cmbConditionForEntry')]);
				Session::put('BA1010InspectionTimeIdForEntry'	, Input::get('cmbInspectionTimeForEntry'));
				Session::put('BA1010InspectionTimeName'			, $lArrDataListInspectionTime[Input::get('cmbInspectionTimeForEntry')]);

				//Search (Session put is Return Screen show)
				Session::put('BA1010ProcessIdForSearch'			, Input::get('cmbProcessForSearch'));
				Session::put('BA1010CustomerIdForSearch'		, Input::get('cmbCustomerForSearch'));
				Session::put('BA1010CheckSheetNoForSearch'		, Input::get('cmbCheckSheetNoForSearch'));
				Session::put('BA1010RevisionNoForSearch'		, Input::get('cmbRevisionNoForSearch'));
				Session::put('BA1010MaterialNameForSearch'		, Input::get('cmbMaterialNameForSearch'));
				Session::put('BA1010MaterialSizeForSearch'		, Input::get('cmbMaterialSizeForSearch'));
				Session::put('BA1010MachineNoForSearch'			, Input::get('txtMachineNoForSearch'));
				Session::put('BA1010MoldNoForSearch'			, Input::get('txtMoldNoForSearch'));
				Session::put('BA1010InspectorCodeForSearch'		, Input::get('txtInspectorCodeForSearch'));
				Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
				Session::put('BA1010ProductionDateToForSearch'	, Input::get('txtProductionDateToForSearch'));
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
				Session::put('BA1010InspectionDateToForSearch'	, Input::get('txtInspectionDateToForSearch'));
				Session::put('BA1010PartNoForSearch'			, Input::get('txtPartNoForSearch'));
				Session::put('BA1010InspectionTimeIdForSearch'	, Input::get('cmbInspectionTimeForSearch'));

				//delete Inspection Result No. at Entry because it transport inspection result No. only at Modify
				Session::forget('BA1010InspectionResultNo');

				//transition to Entry screen
				return Redirect::route("user/preentry");
			}
		}
		elseif (Input::has('btnSearch'))                                             //Search button
		{
			//log
			Log::write('info', 'BA1010 Search Button Click.',
				[
					"Process Id"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer Id" 				=> Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."			=> Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."				=> Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"				=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"				=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."					=> Input::get('txtMachineNoForSearch'          	,''),
					"Mold No."					=> Input::get('txtMoldNoForSearch'          	,''),
					"Inspector Code"			=> Input::get('txtInspectorCodeForSearch'      	,''),
					"Production Date From"		=> Input::get('txtProductionDateFromForSearch' 	,''),
					"Production Date To"		=> Input::get('txtProductionDateToForSearch'   	,''),
					"Inspection Date From"		=> Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"		=> Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."					=> Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"			=> Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForSearchEnterCheck($lViewData);
			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

//			//check error at entering Inspection_Time
//			if(Input::get('cmbInspectionTimeForSearch') == 0)
//			{
//				//nothing
//			}
//			else
//			{
//				//error message
//				$lViewData["errors"] = new MessageBag([
//					"error" => "E041 : Do Not Enter Inspection Time ."
//				]);
//			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//clear in case search requirment remain in session
				Session::forget('BA1010ProcessIdForSearch');
				Session::forget('BA1010CustomerIdForSearch');
				Session::forget('BA1010CheckSheetNoForSearch');
				Session::forget('BA1010RevisionNoForSearch');
				Session::forget('BA1010MaterialNameForSearch');
				Session::forget('BA1010MaterialSizeForSearch');
				Session::forget('BA1010MachineNoForSearch');
				Session::forget('BA1010MoldNoForSearch');
				Session::forget('BA1010ProductionDateFromForSearch');
				Session::forget('BA1010ProductionDateToForSearch');
				Session::forget('BA1010InspectorCodeForSearch');
				Session::forget('BA1010InspectionDateFromForSearch');
				Session::forget('BA1010InspectionDateToForSearch');
				Session::forget('BA1010PartNoForSearch');
				Session::forget('BA1010InspectionTimeIdForSearch');

				//search
				$lTblSearchResultData = $this->getInspectionResultList();
				$lTblNotExistNoData   = $this->getNotExistNoList();

				//error check 0 data
				if (count($lTblSearchResultData) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}

				//sessionにstore
				Session::put('BA1010ProcessIdForSearch'			, Input::get('cmbProcessForSearch'));
				Session::put('BA1010CustomerIDForSearch'		, Input::get('cmbCustomerForSearch'));
				Session::put('BA1010CheckSheetNoForSearch'		, Input::get('cmbCheckSheetNoForSearch'));
				Session::put('BA1010RevisionNoForSearch'		, Input::get('cmbRevisionNoForSearch'));
				Session::put('BA1010MaterialNameForSearch'		, Input::get('cmbMaterialNameForSearch'));
				Session::put('BA1010MaterialSizeForSearch'		, Input::get('cmbMaterialSizeForSearch'));
				Session::put('BA1010MachineNoForSearch'			, Input::get('txtMachineNoForSearch'));
				Session::put('BA1010MoldNoForSearch'			, Input::get('txtMoldNoForSearch'));
				Session::put('BA1010InspectorCodeForSearch'		, Input::get('txtInspectorCodeForSearch'));
				Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
				Session::put('BA1010ProductionDateToForSearch'	, Input::get('txtProductionDateToForSearch'));
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
				Session::put('BA1010InspectionDateToForSearch'	, Input::get('txtInspectionDateToForSearch'));
				Session::put('BA1010PartNoForSearch'			, Input::get('txtPartNoForSearch'));
				Session::put('BA1010InspectionTimeIdForSearch'	, Input::get('cmbInspectionTimeForSearch'));

				Session::put('BA1010InspectionResultData', $lTblSearchResultData);
				Session::put('BA1010NotExistNoData', $lTblNotExistNoData);
			}
		}
		elseif (Input::has('btnCsv'))                                           //CSV button
		{
			//log
			Log::write('info', 'BA1010 CSV Button Click.',
				[
					"Process Id"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer Id" 				=> Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."			=> Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."				=> Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"				=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"				=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."					=> Input::get('txtMachineNoForSearch'          	,''),
					"Mold No."					=> Input::get('txtMoldNoForSearch'          	,''),
					"Inspector Code"			=> Input::get('txtInspectorCodeForSearch'      	,''),
					"Production Date From"		=> Input::get('txtProductionDateFromForSearch' 	,''),
					"Production Date To"		=> Input::get('txtProductionDateToForSearch'   	,''),
					"Inspection Date From"		=> Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"		=> Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."					=> Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"			=> Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//■■13/06/2016 Motooka NG Result■■
			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
				//Nothing
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search data for CSV
				$lTblInspectionResultCSV = $this->getInspectionResultCSV();

				if (count($lTblInspectionResultCSV) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else
				{
					$lTblInspectionResultCSV = (Array)$lTblInspectionResultCSV;

					$lOutputCSVData = $this->createInspectionResultCSV($lTblInspectionResultCSV);

					$lMimeSetting = array(
						'Content-Type' => 'text/csv',
						'Content-Disposition' => 'attachment; filename="InspectionResultData.csv"',
					);

					//download
					return Response::make(rtrim($lOutputCSVData, "\n"), 200, $lMimeSetting);
				}
			}
		}
		elseif (Input::has('btnShoninData'))                                    //Excel（for approval） button
		{
			Log::write('info', 'BA1010 Excel Button Click.',
				[
					"Process Id"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer Id" 				=> Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."			=> Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."				=> Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"				=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"				=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."					=> Input::get('txtMachineNoForSearch'          	,''),
					"Mold No."					=> Input::get('txtMoldNoForSearch'          	,''),
					"Inspector Code"			=> Input::get('txtInspectorCodeForSearch'      	,''),
					"Production Date From"		=> Input::get('txtProductionDateFromForSearch' 	,''),
					"Production Date To"		=> Input::get('txtProductionDateToForSearch'   	,''),
					"Inspection Date From"		=> Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"		=> Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."					=> Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"			=> Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForShonin($lViewData);
			//check error(must check)
			$lViewData = $this->isErrorForSearchEnterCheck($lViewData);
			//check error(check type)
			$lViewData = $this->isErrorForSearch($lViewData);

			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
				//Nothing
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//get master data,result data corresponding
				$lTblDataShonin = $this->getInspectionDataForShonin();

				//error when master data does not exist
				//output in case result data does not exist
				if (count($lTblDataShonin) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E036 : Not Exists Target Data."
					]);
				}
				else  //data exists
				{
					//templete Excel file 
					$objReader = new PHPExcel_Reader_Excel2007();
					$lTemplateFileNameShonin = "InspectionResultDataforApproval_template.xlsx";
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameShonin);
					// $objPHPExcel = $objReader->load(public_path()."/excel/".$lTemplateFileNameShonin);
					//do active sheet No. 0
					$objPHPExcel->setActiveSheetIndex(0);
					$objSheet = $objPHPExcel->getActiveSheet();

					//----------------------------
					//set infomation to header
					//----------------------------
					$lExcelRowCount = self::SHONINEXCEL_HEADER_STARTROW; //define first line of header
					//pull out master data 1 line and set value
					$lTblDataShoninRow = (Array)$lTblDataShonin[0];
					$objSheet->setCellValueByColumnAndRow(0, $lExcelRowCount, $lTblDataShoninRow["INSPECTION_SHEET_NO"]);
					$objSheet->setCellValueByColumnAndRow(1, $lExcelRowCount, $lTblDataShoninRow["REV_NO"]);
					$objSheet->setCellValueByColumnAndRow(2, $lExcelRowCount, $lTblDataShoninRow["PROCESS_NAME"]);
					$objSheet->setCellValueByColumnAndRow(3, $lExcelRowCount, $lTblDataShoninRow["MODEL_NAME"]);
					$objSheet->setCellValueByColumnAndRow(4, $lExcelRowCount, $lTblDataShoninRow["INSPECTION_SHEET_NAME"]);
					$objSheet->setCellValueByColumnAndRow(5, $lExcelRowCount, $lTblDataShoninRow["MACHINE_NAME"]);
					$objSheet->setCellValueByColumnAndRow(6, $lExcelRowCount, $lTblDataShoninRow["MOLD_NO"]);
					$objSheet->setCellValueByColumnAndRow(7, $lExcelRowCount, $lTblDataShoninRow["INSERT_USER_ID"]);
					$objSheet->setCellValueByColumnAndRow(8, $lExcelRowCount, $lTblDataShoninRow["USER_NAME"]);
					$objSheet->setCellValueByColumnAndRow(9, $lExcelRowCount, $lTblDataShoninRow["INSPECTION_YMD"]);

					//----------------------------
					//set infomation on detail
					//----------------------------
					//define start line of detail(decrease 1 line first,because loop counter add 1 more line)
					//明細の開始行を定義(ループカウンタで1追加するため最初に1減らす)
					$lExcelRowCount = self::SHONINEXCEL_MEISAI_STARTROW - 1;

					//in case data exists more than 2line(default),add lines
					//テンプレートのデフォルト行数(2行)以上にデータがある場合は、その分の行を追加
					if (count($lTblDataShonin) > 2)
					{
						$objSheet->insertNewRowBefore(self::SHONINEXCEL_MEISAI_STARTROW + 1, count($lTblDataShonin) - 2);
					}

					foreach ($lTblDataShonin As $lRow)
					{
						$lExcelRowCount = $lExcelRowCount + 1;
						$lCurrentDataShoninRow = (Array)$lRow;

						//マスタデータを書き込み(検査番号、検査時間、検査項目、基準値、NG値(上限)、NG値(下限)
						//2015.04.28 保科  検査項目を追加
						$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_POINT"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["ANALYTICAL_GRP_01"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["ANALYTICAL_GRP_02"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["SAMPLE_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["CONDITION_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["LOT_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["PRODUCTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

						$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						//検査結果がNGの場合は基準値との差分を求めて出力する
						$addWord = "";
						if ($lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"] == "NG")
						{
							//基準値と実績値の差を求める(小数点7桁まで求める) ※単純に減算すると勝手に浮動小数点で計算して値がずれるので関数を使用する
							$deff =bcadd($lCurrentDataShoninRow["INSPECTION_RESULT_VALUE"], - $lCurrentDataShoninRow["REFERENCE_VALUE"], 7);
							//求めた値を加工する(小数点の後ろゼロを取り除く)
							$addWord = "(".preg_replace("/\.?0+$/","",$deff).")";
						}
						$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"].$addWord, PHPExcel_Cell_DataType::TYPE_STRING);
						if ($lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"] == "")
						{
							$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
										->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
						}
						else
						{
							$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
										->setValueExplicit($lCurrentDataShoninRow["INSERT_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
						}
						$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_INTERNAL"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getStyleByColumnAndRow(19, $lExcelRowCount)
									->getAlignment()->setWrapText(true);
						$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_REQUEST"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_TSR"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_SORTING"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_NOACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(26, $lExcelRowCount)
									->setValueExplicit($lCurrentDataShoninRow["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
					}

					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					//保存ファイル名とパスの設定（テンプレートファイル名の「template」を「現在の年月日_時分秒」で置換した値
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameShonin);
					$lSaveFilePathAndName = "excel/".$lSaveFileName;
					//$lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName;

					//save file（save in server temporarily）（サーバに一旦ファイルを保存）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					//データ件数が多いと、このSAVE処理が異常に長い。保存せず、直接DL出来れば改善の可能性。
					$objWriter->save($lSaveFilePathAndName);

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete file（that was saved in server)
					unlink($lSaveFilePathAndName);

					//message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";
				}
			}
		}
		elseif (Input::has('btnShoninData2'))                                   //NGResult Excel button
		{
			Log::write('info', 'BA1010 NGResult Button Click.',
				[
					"Process Id"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer Id" 				=> Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."			=> Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."				=> Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"				=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"				=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."					=> Input::get('txtMachineNoForSearch'          	,''),
					"Mold No."					=> Input::get('txtMoldNoForSearch'          	,''),
					"Inspector Code"			=> Input::get('txtInspectorCodeForSearch'      	,''),
					"Production Date From"		=> Input::get('txtProductionDateFromForSearch' 	,''),
					"Production Date To"		=> Input::get('txtProductionDateToForSearch'   	,''),
					"Inspection Date From"		=> Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"		=> Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."					=> Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"			=> Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForSearchEnterCheck($lViewData);
			//check error(check type)
			$lViewData = $this->isErrorForSearch($lViewData);

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				$lTblResultDataNGResult = $this->getInspectionResultDataForNGResult();

				if (count($lTblResultDataNGResult) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E036 : Not Exists Target Data."
					]);
				}
				else
				{
					//define templete Excel file
					$objReader = new PHPExcel_Reader_Excel2007();//
					$lTemplateFileNameNGResult = "InspectionResultNGData_template.xlsx";
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameNGResult);

					//active the number 0 inspection check sheet
					$objPHPExcel->setActiveSheetIndex(0); //select the first inspection check sheet
					$objSheet = $objPHPExcel->getActiveSheet(); //access to inspection check sheet selected

					//----------------------------
					//set information in header
					//----------------------------
					$lExcelRowCount = self::NGRESULTEXCEL_HEADER_STARTROW; //start header line

					if(Input::has('cmbProcessForSearch'))
					{
						$lTblResultDataHeadProcessName = $this->getProcessName();
						if ($lTblResultDataHeadProcessName != null)
						{
							$ArrResultDataHeadProcessName = [];
							$ArrResultDataHeadProcessName = (Array)$lTblResultDataHeadProcessName[0];
							$objSheet->setCellValueByColumnAndRow(0, $lExcelRowCount, $ArrResultDataHeadProcessName["CODEORDER_NAME"] );
						}
					}
					if(Input::has('cmbCustomerForSearch'))
					{
						$lTblResultDataHeadCustomerName = $this->getCustmerName();
						if ($lTblResultDataHeadCustomerName != null)
						{
							$ArrResultDataHeadCustmerName = [];
							$ArrResultDataHeadCustmerName = (Array)$lTblResultDataHeadCustomerName[0];
							$objSheet->setCellValueByColumnAndRow(1, $lExcelRowCount, $ArrResultDataHeadCustmerName["CUSTOMER_NAME"] );
						}
					}
					if(Input::has('cmbCheckSheetNoForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(2, $lExcelRowCount, Input::get('cmbCheckSheetNoForSearch'));
					}
					if(Input::has('cmbCheckSheetNoForSearch') && Input::has('cmbRevisionNoForSearch'))
					{
						$lTblResultDataHeadCheckSheetName = $this->getCheckSheetName();
						if ($lTblResultDataHeadCheckSheetName != null)
						{
							$lTblResultDataHeadCheckSheetName = [];
							$ArrResultDataHeadCheckSheetName = (Array)$lTblResultDataHeadCheckSheetName[0];
							$objSheet->setCellValueByColumnAndRow(3, $lExcelRowCount, $ArrResultDataHeadCheckSheetName["INSPECTION_SHEET_NAME"]);
						}
					}
					if(Input::has('cmbRevisionNoForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(4, $lExcelRowCount, Input::get('cmbRevisionNoForSearch'));
					}
					if(Input::has('cmbMaterialNameForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(5, $lExcelRowCount, Input::get('cmbMaterialNameForSearch'));
					}
					if(Input::has('cmbMaterialSizeForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(6, $lExcelRowCount, Input::get('cmbMaterialSizeForSearch'));
					}
					if(Input::has('txtMachineNoForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(7, $lExcelRowCount, Input::get('txtMachineNoForSearch'));
					}
					if(Input::has('txtMoldNoForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(8, $lExcelRowCount, Input::get('txtMoldNoForSearch'));
					}
					if(Input::has('txtInspectorCodeForSearch'))
					{
						$lTblResultDataHeadUserName = $this->getInspectorName();//get result data
						if ($lTblResultDataHeadUserName != null)
						{
							$ArrResultDataHeadUserName = [];
							$ArrResultDataHeadUserName = (Array)$lTblResultDataHeadUserName[0];
							$objSheet->setCellValueByColumnAndRow(9, $lExcelRowCount, $ArrResultDataHeadUserName["USER_NAME"]);
						}
					}
					if(Input::has('txtProductionDateFromForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(10, $lExcelRowCount, Input::get('txtProductionDateFromForSearch'));
					}
					if(Input::has('txtProductionDateToForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(11, $lExcelRowCount, Input::get('txtProductionDateToForSearch'));
					}
					if(Input::has('txtInspectionDateFromForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(12, $lExcelRowCount, Input::get('txtInspectionDateFromForSearch'));
					}
					if(Input::has('txtInspectionDateToForSearch'))
					{
						$objSheet->setCellValueByColumnAndRow(13, $lExcelRowCount, Input::get('txtInspectionDateToForSearch'));
					}
					$objSheet->setCellValueByColumnAndRow(15, $lExcelRowCount, date("d-m-Y"));
					$objSheet->setCellValueByColumnAndRow(17, $lExcelRowCount, date("H:i:s"));


					//----------------------------
					//set infomation on detail
					//----------------------------
					$lExcelRowCount = self::NGRESULTEXCEL_MEISAI_STARTROW - 1;

					//set down data while loop result
					foreach ($lTblResultDataNGResult As $lResultRow)
					{
						$lTblDataNGResultRow = (Array)$lResultRow;

						if(($lWorkINSPECTIONSHEET == $lTblDataNGResultRow["INSPECTION_SHEET_NO"]) and ($lWorkMACHINENO == $lTblDataNGResultRow["MACHINE_NO"]) and ($lWorkMOLDNO == $lTblDataNGResultRow["MOLD_NO"]) and ($lWorkINSPECTIONPOINT == $lTblDataNGResultRow["INSPECTION_POINT"]) and ($lWorkANAGRP01 == $lTblDataNGResultRow["ANALYTICAL_GRP_01"]) and ($lWorkANAGRP02 == $lTblDataNGResultRow["ANALYTICAL_GRP_02"]))
						{
							//Nothing
						}
						//not corresponding,ＮＧflag is 0
						else
						{
							$NGFLG = 0;
						}

						//leave Inspection No. of result data
						$lWorkINSPECTIONSHEET = $lTblDataNGResultRow["INSPECTION_SHEET_NO"];
						$lWorkMACHINENO       = $lTblDataNGResultRow["MACHINE_NO"];
						$lWorkMOLDNO          = $lTblDataNGResultRow["MOLD_NO"];
						$lWorkINSPECTIONPOINT = $lTblDataNGResultRow["INSPECTION_POINT"];
						$lWorkANAGRP01        = $lTblDataNGResultRow["ANALYTICAL_GRP_01"];
						$lWorkANAGRP02        = $lTblDataNGResultRow["ANALYTICAL_GRP_02"];

						//OK data
						if ($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"] == "1")
						{
							//flag is 0
							if ($NGFLG == 0)
							{
								//not output
								//flag leave as it is
							}
							//when flag is 1
							else
							{
								//add 1 more line at 8 line and write down data.last 2 lines are blank. 
								$objSheet->insertNewRowBefore($lExcelRowCount+2,1 );
								//add line No. to putout
								$lExcelRowCount = $lExcelRowCount + 1;

								//set down result
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PROCESS_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MOLD_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_POINT"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_01"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_02"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["SAMPLE_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PRODUCTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(26, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(27, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(28, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(29, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(30, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

								//flag is 0
								$NGFLG = 0;
							}
						}
						//inspection reult is Line Stop
						else if($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"] == "5")
						{
							//in case TSR No. is entried
							if ($lTblDataNGResultRow["TSR_NO"] != null)
							{
								//flag = 1
								$NGFLG = 1;
								//add 1 more line at 8 line and write down data.last 2 lines are blank.
								$objSheet->insertNewRowBefore($lExcelRowCount+2,1 );
								//add line No. to putout
								$lExcelRowCount = $lExcelRowCount + 1;

								//set down result
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PROCESS_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MOLD_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_POINT"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_01"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_02"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["SAMPLE_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PRODUCTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(26, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(27, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(28, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(29, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(30, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
							}
							//enter nothin in TSR No.
							else
							{
								//not output
								//leave flag asa it is
							}
						}
						//in case inspection result is OFFSET,NG,No Check
						else
						{
							//change flag 1
							$NGFLG = 1;
							//add 1 line at 8 line in templete（for flame）and set down data
							//add each 1 line as data should be set down. At last line in data,leave 2 blank line（default）
							$objSheet->insertNewRowBefore($lExcelRowCount + 2, 1);
							//increase number of line corresponding to outputting
							$lExcelRowCount = $lExcelRowCount + 1;
							//set down result
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PROCESS_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["MOLD_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_POINT"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_01"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ANALYTICAL_GRP_02"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["SAMPLE_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["PRODUCTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(26, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(27, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(28, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(29, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(30, $lExcelRowCount)
											->setValueExplicit($lTblDataNGResultRow["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						}

						//leave Inspection No. of result data
						$lWorkINSPECTIONSHEET = $lTblDataNGResultRow["INSPECTION_SHEET_NO"];
						$lWorkMACHINENO       = $lTblDataNGResultRow["MACHINE_NO"];
						$lWorkMOLDNO          = $lTblDataNGResultRow["MOLD_NO"];
						$lWorkINSPECTIONPOINT = $lTblDataNGResultRow["INSPECTION_POINT"];
						$lWorkANAGRP01        = $lTblDataNGResultRow["ANALYTICAL_GRP_01"];
						$lWorkANAGRP02        = $lTblDataNGResultRow["ANALYTICAL_GRP_02"];
					}

					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameNGResult);
					$lSaveFilePathAndName = "excel/".$lSaveFileName;

					//save file（save in server temporarily）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					$objWriter->save($lSaveFilePathAndName); //start to output

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete file（that was saved in server）
					unlink($lSaveFilePathAndName);

					//finish message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";
				}
			}
		}
		elseif (Input::has('btnModify'))                                             //Modify button
		{
			Log::write('info', 'BA1010 Modify Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get PK in the line selected
			$lInspectionResultNo = Input::get('hidInspectionResultNo');

			//get DataRevision（for exclusive control）
			$lDataRev = Input::get('hidDataRev');

			//search data corresponding to PK in the line selected
			$lTblModifyTargetData = $this->getInspectionResultData($lInspectionResultNo, $lDataRev);

			//in case no hit to search,exclusive error
			if (count($lTblModifyTargetData) == 0)
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E998 : Data has been updated by another terminal. Try search again."
				]);
			}
			else  //data exists
			{
				//get the 0 line because header data corresponding to modify is 1.
				$lRowModifyTargetData = (Array)$lTblModifyTargetData[0];
				
				//store items in session and transport to use in Modify screen
				Session::put('BA1010InspectionResultNo'			, $lInspectionResultNo);

				Session::put('BA1010CheckSheetNoForModify'		, $lRowModifyTargetData["INSPECTION_SHEET_NO"]);  
				Session::put('BA1010CheckSheetNameForModify'	, $lRowModifyTargetData["INSPECTION_SHEET_NAME"]);
				Session::put('BA1010RevisionNoForModify'		, $lRowModifyTargetData["REV_NO"]);
				Session::put('BA1010ProcessIdForModify'			, $lRowModifyTargetData["PROCESS_ID"]); 
				Session::put('BA1010ProcessNameForModify'		, $lRowModifyTargetData["PROCESS_NAME"]);
				Session::put('BA1010MachineNoForModify'			, $lRowModifyTargetData["MACHINE_NO"]);
				Session::put('BA1010MachineNameForModify'		, $lRowModifyTargetData["MACHINE_NAME"]);
				Session::put('BA1010MoldNoForModify'			, $lRowModifyTargetData["MOLD_NO"]);
				Session::put('BA1010MaterialNameForModify'		, $lRowModifyTargetData["MATERIAL_NAME"]); 
				Session::put('BA1010MaterialSizeForModify'		, $lRowModifyTargetData["MATERIAL_SIZE"]); 
				Session::put('BA1010HeatTreatmentTypeForModify'	, $lRowModifyTargetData["HEAT_TREATMENT_TYPE"]);
				Session::put('BA1010PlatingKindForModify'		, $lRowModifyTargetData["PLATING_KIND"]);
				Session::put('BA1010ColorIdForModify'			, $lRowModifyTargetData["COLOR_ID"]);
				Session::put('BA1010ColorNameForModify'			, $lRowModifyTargetData["COLOR_NAME"]);
				Session::put('BA1010InspectorCodeForModify'		, $lRowModifyTargetData["INSERT_USER_ID"]); 
				Session::put('BA1010InspectorNameForModify'		, $lRowModifyTargetData["USER_NAME"]);
				Session::put('BA1010TeamIdForModify'			, $lRowModifyTargetData["TEAM_ID"]);
				Session::put('BA1010TeamNameForModify'			, $lRowModifyTargetData["TEAM_NAME"]);
				Session::put('BA1010InspectionDateForModify'	, $lRowModifyTargetData["INSPECTION_YMD"]); 
				Session::put('BA1010ConditionIdForModify'		, $lRowModifyTargetData["CONDITION_CD"]);
				Session::put('BA1010ConditionNameForModify'		, $lRowModifyTargetData["CONDITION_NAME"]);
				Session::put('BA1010InspectionTimeIdForModify'	, $lRowModifyTargetData["INSPECTION_TIME_ID"]);
				Session::put('BA1010InspectionTimeNameForModify', $lRowModifyTargetData["INSPECTION_TIME_NAME"]);
				Session::put('BA1010MaterialLotNoForModify'		, $lRowModifyTargetData["MATERIAL_LOT_NO"]);
				Session::put('BA1010CertificateNoForModify'		, $lRowModifyTargetData["CERTIFICATE_NO"]);
				Session::put('BA1010PoNoForModify'				, $lRowModifyTargetData["PO_NO"]);
				Session::put('BA1010InvoiceNoForModify'			, $lRowModifyTargetData["INVOICE_NO"]);
				Session::put('BA1010QuantityCoilForModify'		, $lRowModifyTargetData["QUANTITY_COIL"]);
				Session::put('BA1010QuantityWeightForModify'	, $lRowModifyTargetData["QUANTITY_WEIGHT"]);
				Session::put('BA1010LotNoForModify'				, $lRowModifyTargetData["LOT_NO"]);
				Session::put('BA1010ProductionDateForModify'	, $lRowModifyTargetData["PRODUCTION_YMD"]);
				Session::put('BA1010ModifyDataRev'				, $lRowModifyTargetData["DATA_REV"]); 
				Session::put('BA1010CustomerCodeForModify'		, $lRowModifyTargetData["CUSTOMER_ID"]);
				Session::put('BA1010CustomerNameForModify'		, $lRowModifyTargetData["CUSTOMER_NAME"]);
				Session::put('BA1010ModelNameForModify'			, $lRowModifyTargetData["MODEL_NAME"]); 
				Session::put('BA1010ItemNoForModify'			, $lRowModifyTargetData["ITEM_NO"]);
				Session::put('BA1010ItemNameForModify'			, $lRowModifyTargetData["ITEM_NAME"]);
				Session::put('BA1010DrawingNoForModify'			, $lRowModifyTargetData["DRAWING_NO"]);  
				Session::put('BA1010ISOKanriNoForModify'		, $lRowModifyTargetData["ISO_KANRI_NO"]);
				Session::put('BA1010ProductWeightForModify'		, $lRowModifyTargetData["PRODUCT_WEIGHT"]);

				//Entry (Session put is Return Screen show)
				Session::put('BA1010ProcessIdForEntry'			, Input::get('cmbProcessForEntry'));
				Session::put('BA1010CustomerIdForEntry'			, Input::get('cmbCustomerForEntry'));
				Session::put('BA1010CheckSheetNoForEntry'		, Input::get('cmbCheckSheetNoForEntry'));
				Session::put('BA1010MaterialNameForEntry'		, Input::get('cmbMaterialNameForEntry'));
				Session::put('BA1010MaterialSizeForEntry'		, Input::get('cmbMaterialSizeForEntry'));
				Session::put('BA1010MachineNoForEntry'			, Input::get('txtMachineNoForEntry'));
				Session::put('BA1010MoldNoForEntry'				, Input::get('txtMoldNoForEntry'));
				Session::put('BA1010InspectorCodeForEntry'		, Input::get('txtInspectorCodeForEntry'));
				Session::put('BA1010InspectionDateForEntry'		, Input::get('txtInspectionDateForEntry'));
				Session::put('BA1010ConditionIdForEntry'		, Input::get('cmbConditionForEntry'));
				Session::put('BA1010InspectionTimeIdForEntry'	, Input::get('cmbInspectionTimeForEntry'));

				//Search (Session put is Return Screen show)
				Session::put('BA1010ProcessIdForSearch'			, Input::get('cmbProcessForSearch'));
				Session::put('BA1010CustomerIdForSearch'		, Input::get('cmbCustomerForSearch'));
				Session::put('BA1010CheckSheetNoForSearch'		, Input::get('cmbCheckSheetNoForSearch'));
				Session::put('BA1010RevisionNoForSearch'		, Input::get('cmbRevisionNoForSearch'));
				Session::put('BA1010MaterialNameForSearch'		, Input::get('cmbMaterialNameForSearch'));
				Session::put('BA1010MaterialSizeForSearch'		, Input::get('cmbMaterialSizeForSearch'));
				Session::put('BA1010MachineNoForSearch'			, Input::get('txtMachineNoForSearch'));
				Session::put('BA1010MoldNoForSearch'			, Input::get('txtMoldNoForSearch'));
				Session::put('BA1010InspectorCodeForSearch'		, Input::get('txtInspectorCodeForSearch'));
				Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
				Session::put('BA1010ProductionDateToForSearch'	, Input::get('txtProductionDateToForSearch'));
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
				Session::put('BA1010InspectionDateToForSearch'	, Input::get('txtInspectionDateToForSearch'));
				Session::put('BA1010PartNoForSearch'			, Input::get('txtPartNoForSearch'));
				Session::put('BA1010InspectionTimeForSearch'	, Input::get('cmbInspectionTimeForSearch'));

				//transition to entry screen
				// return Redirect::route("user/entry");

				//transition to preentry screen
				return Redirect::route("user/preentry");
			}
		}
		elseif (Input::has('btnLineStop'))                                      //■■■■LineStop button■■■■
		{
			//log
			Log::write('info', 'BA1010 Line Stop Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get primary key in selected line
			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			
			//get DataRevision
			$lDataRev = Input::get('hidDataRev');

			//in case primary key exists in selected line
			if ($lInspectionResultNo != "")
			{
				//echo "Debug Line Stop"; die();
				//make Line Stop data towards no entry Inspection No.
				$this->updateLineStopData($lInspectionResultNo, $lDataRev, $lViewData["UserID"]);

				//message
				$lViewData["NormalMessage"] = "I005 : Process has been completed.";

				//delete information of search（reset）
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnDelete'))                                        //■■■■Delete button■■■■
		{
			//log
			Log::write('info', 'BA1010 Delete Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get primary key in selected line
			$lInspectionResultNo = Input::get('hidInspectionResultNo');

			//get DataRevision
			$lDataRev = Input::get('hidDataRev');

			//in case primary key exists in selected line
			if ($lInspectionResultNo != "")
			{
				//delete
				$lDeleteCount = $this->deleteInspectionResultData($lInspectionResultNo, $lDataRev);

				if ($lDeleteCount == 0)
				{
					$lViewData["errors"] = new MessageBag([
						"error" => "E998 : Data has been updated by another terminal. Try search again."
					]);
				}
				else
				{
					$lViewData["NormalMessage"] = "I004 : Delete has been completed.";
				}

				//delete information of search（reset）
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnPrintInspectionSheet'))                          //Inspection Sheet button
		{
			Log::write('info', 'BA1010 Inspection Sheet Print Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			$lDataRev = Input::get('hidDataRev');

			if ($lInspectionResultNo != null)
			{
				$lTblHeaderTableDataInspectionSheet = $this->getHeaderTableDataInspectionSheet($lInspectionResultNo, $lDataRev);
				
				if ($lTblHeaderTableDataInspectionSheet == null)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else
				{
					$lTblHeaderTableDataInspectionSheetRow = (Array)$lTblHeaderTableDataInspectionSheet[0];

					$lTblSheetMasterTaleDataInspectionSheet = $this->getSheetMasterTaleDataInspectionSheet($lTblHeaderTableDataInspectionSheetRow["INSPECTION_SHEET_NO"], $lTblHeaderTableDataInspectionSheetRow["REV_NO"]);
					$lTblSheetMasterTaleDataInspectionSheetRow = (Array)$lTblSheetMasterTaleDataInspectionSheet[0];

					//Check Number of Pic
					$lTblOutputPictureInspectionSheet = $this->getOutputPictureInspectionSheet($lTblHeaderTableDataInspectionSheetRow["INSPECTION_SHEET_NO"], $lTblHeaderTableDataInspectionSheetRow["REV_NO"]);
					$PictureCount = count($lTblOutputPictureInspectionSheet);
					if ($PictureCount > self::INSPECTIONSHEETEXCEL_PIC_MAX)
					{
						$PictureCount = self::INSPECTIONSHEETEXCEL_PIC_MAX;
					}

					//Check Count of data
					$lTblOutputDetailRecordCountInspectionSheet = $this->getOutputDetailRecordCountInspectionSheet($lInspectionResultNo, $lDataRev);
					$lTblOutputDetailRecordCountInspectionSheetRow = (Array)$lTblOutputDetailRecordCountInspectionSheet[0];
					$DetailCount = count($lTblOutputDetailRecordCountInspectionSheet);
					$TotalPageNumber = ceil($DetailCount / self::INSPECTIONSHEETEXCEL_DETAIL_COUNT) + 1;

					// Log::write('info', '1',
					// 	[
					// 		"PictureCount"  => $PictureCount,
					// 		"DetailCount"  => $DetailCount,
					// 		"TotalPageNumber"  => $TotalPageNumber,
					// 	]
					// );

					$lTblResultDetailInspectionSheet = $this->getResultDetailInspectionSheet($lInspectionResultNo);

					//define templete Excel file
					$objReader = new PHPExcel_Reader_Excel2007();

					//read templete Excel file
					$lTemplateFileNameInspectionSheet = "InspectionSheet_template.xlsx";
					
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameInspectionSheet);
					// $objPHPExcel = $objReader->load(public_path()."/excel/".$lTemplateFileNameInspectionSheet);

					//active the number 0 inspection check sheet
					$objPHPExcel->setActiveSheetIndex(0);//select the first inspection check sheet
					$objSheet = $objPHPExcel->getActiveSheet(); //access to inspection check sheet selected

					if ($TotalPageNumber >= 3)
					{
						$objSheet->insertNewRowBefore((self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * 2), self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * ($TotalPageNumber - 2));
					}

					//----------------------------
					//copy
					//----------------------------
					for ($CopyCount = 1 ; $CopyCount < ($TotalPageNumber - 1); $CopyCount++)
					{
						//1st copy (0,53)->(18,104) copy (0, 105)->(18, 156)
						//2nd copy (0,53)->(18,104) copy (0, 157)->(18, 208)
						//3rd copy (0,53)->(18,104) copy (0, 208)->(18, 259)
						//4th ・・・・
						for ($col = 0 ; $col < 19 ; $col++)
						{
							for ($row = 53 ; $row < 105 ; $row++)
							{
								//Get cell
								$cell = $objSheet->getCellByColumnAndRow($col, $row);
								//Get cell style
								$style = $objSheet->getStyleByColumnAndRow($col, $row);
								//Change (0,1)→A1
								$offsetCell = PHPExcel_Cell::stringFromColumnIndex($col + self::INSPECTIONSHEETEXCEL_COL_OFFSET) . (string)($row + ($CopyCount * self::INSPECTIONSHEETEXCEL_ROW_OFFSET));
								//Copy Value
								$objSheet->setCellValue($offsetCell, $cell->getValue());
								//Copy Style
								$objSheet->duplicateStyle($style, $offsetCell);
							}
						}
					}

					if ($TotalPageNumber >= 3)
					{
						for ($Count = 1 ; $Count < ($TotalPageNumber - 1); $Count++)
						{
							//画像用のオプジェクト作成
							$objDrawing = new PHPExcel_Worksheet_Drawing();
							//貼り付ける画像のパスを指定
							$objDrawing->setPath(self::LOGOPATH);
							//画像の高さを指定
							$objDrawing->setHeight(40);
							//画像のプロパティを見たときに表示される情報を設定
							//画像名
							// $objDrawing->setName('Image');
							//画像の概要
							// $objDrawing->setDescription('Image');
							//位置
							$lWorkPosition = PHPExcel_Cell::stringFromColumnIndex(self::CEL_E).(string)(103 + self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * $Count);
							$objDrawing->setCoordinates($lWorkPosition);
							//横方向へ何ピクセルずらすかを指定
							$objDrawing->setOffsetX(20);
							//回転の角度
							//$objDrawing->setRotation(25);
							//ドロップシャドウをつけるかどうか
							//$objDrawing->getShadow()->setVisible(true);
							//PHPExcelオブジェクトに張り込み
							$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						}
					}

					//----------------------------
					//set information in header (1st page)
					//----------------------------
					//$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTCOL;
					$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTROW;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["CUSTOMER_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MODEL_NAME"]);
					$objSheet->setCellValueByColumnAndRow(16, $lInspectionSheetRowCount, 1);
					$objSheet->setCellValueByColumnAndRow(18, $lInspectionSheetRowCount, $TotalPageNumber);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["PROCESS_NAME"]);
					$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NO"]);
					$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_SIZE"]);
					$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["DRAWING_NO"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NAME"]);
					$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["COLOR_NAME"]);
					$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblOutputDetailRecordCountInspectionSheetRow["GRP01_QTY"]);
					$objSheet->setCellValueByColumnAndRow(17, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["MOLD_NO"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["PRODUCTION_YMD"]);
					$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["INSPECTION_YMD"]);
					$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["LOT_NO"]);


					//----------------------------
					//set information in picture
					//----------------------------
					if ($PictureCount > 0)
					{
						foreach ($lTblOutputPictureInspectionSheet As $lPictureRow)
						{
							$lCurrentPictureRow = (Array)$lPictureRow;
							$lWorkPicCount = $lWorkPicCount + 1 ;

							if (self::INSPECTIONSHEETEXCEL_PIC_MAX >= $lWorkPicCount)
							{
								//画像用のオプジェクト作成
								$objDrawing = new PHPExcel_Worksheet_Drawing();
								//貼り付ける画像のパスを指定
								$objDrawing->setPath($lCurrentPictureRow["PICTURE_URL"]);
								//画像の高さを指定
								$objDrawing->setHeight(150);//
								//画像のプロパティを見たときに表示される情報を設定
								//画像名
								$objDrawing->setName('Image');
								//画像の概要
								$objDrawing->setDescription('Image');
								//位置
								if ($lWorkPicCount == 1)
								{
									$objDrawing->setCoordinates('B8');
								}
								if ($lWorkPicCount == 2)
								{
									$objDrawing->setCoordinates('G8');
								}
								if ($lWorkPicCount == 3)
								{
									$objDrawing->setCoordinates('M8');
								}
								if ($lWorkPicCount == 4)
								{
									$objDrawing->setCoordinates('B16');
								}
								if ($lWorkPicCount == 5)
								{
									$objDrawing->setCoordinates('G16');
								}
								if ($lWorkPicCount == 6)
								{
									$objDrawing->setCoordinates('M16');
								}
								if ($lWorkPicCount == 7)
								{
									$objDrawing->setCoordinates('B24');
								}
								if ($lWorkPicCount == 8)
								{
									$objDrawing->setCoordinates('G24');
								}
								if ($lWorkPicCount == 9)
								{
									$objDrawing->setCoordinates('M24');
								}
								if ($lWorkPicCount == 10)
								{
									$objDrawing->setCoordinates('B32');
								}
								if ($lWorkPicCount == 11)
								{
									$objDrawing->setCoordinates('G32');
								}
								if ($lWorkPicCount == 12)
								{
									$objDrawing->setCoordinates('M32');
								}
								if ($lWorkPicCount == 13)
								{
									$objDrawing->setCoordinates('B40');
								}
								if ($lWorkPicCount == 14)
								{
									$objDrawing->setCoordinates('G40');
								}
								if ($lWorkPicCount == 15)
								{
									$objDrawing->setCoordinates('M40');
								}

								//横方向へ何ピクセルずらすかを指定
								$objDrawing->setOffsetX(10);
								//回転の角度
								//$objDrawing->setRotation(25);
								//ドロップシャドウをつけるかどうか
								//$objDrawing->getShadow()->setVisible(true);

								//PHPExcelオブジェクトに張り込み
								$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
							}
						}
					}
					else
					{
						//Nothing
					}


					//----------------------------
					//set information in detail
					//----------------------------
					foreach ($lTblOutputDetailRecordCountInspectionSheet As $lResultDetailRow1)
					{
						$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTCOL;

						$lCurrentRow1 = (Array)$lResultDetailRow1;
						$lWorkCount = $lWorkCount + 1;
						$lRemainder = $lWorkCount % self::INSPECTIONSHEETEXCEL_DETAIL_COUNT;

						if ($lRemainder == 1)
						{
							$lPageCount = $lPageCount + 1;

							if ($lPageCount >= 2)
							{
								$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * ($lPageCount - 1);

								// Log::write('info', '1',
								// 	[
								// 		"InspectionSheetRowCount"  => $lInspectionSheetRowCount,
								// 	]
								// );
							}

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["CUSTOMER_NAME"]);

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MODEL_NAME"]);
							$objSheet->setCellValueByColumnAndRow(16, $lInspectionSheetRowCount, $lPageCount);
							$objSheet->setCellValueByColumnAndRow(18, $lInspectionSheetRowCount, $TotalPageNumber);

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["PROCESS_NAME"]);
							$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_NAME"]);

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NO"]);
							$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_SIZE"]);
							$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["DRAWING_NO"]);

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NAME"]);
							$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblSheetMasterTaleDataInspectionSheetRow["COLOR_NAME"]);
							$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblOutputDetailRecordCountInspectionSheetRow["GRP01_QTY"]);
							$objSheet->setCellValueByColumnAndRow(17, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["MOLD_NO"]);

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							$objSheet->setCellValueByColumnAndRow(2, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["PRODUCTION_YMD"]);
							$objSheet->setCellValueByColumnAndRow(7, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["INSPECTION_YMD"]);
							$objSheet->setCellValueByColumnAndRow(14, $lInspectionSheetRowCount, $lTblHeaderTableDataInspectionSheetRow["LOT_NO"]);
						}

						if ($lRemainder == 1)
						{
							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 3;
						}
						else
						{
							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
						}


						//***************************************************
						//leave item
						$lWork2InspectionPoint      = $lCurrentRow1["INSPECTION_POINT"];
						$lWork2AnalyGrp01           = $lCurrentRow1["ANALYTICAL_GRP_01"];
						$lWork2AnalyGrp02           = $lCurrentRow1["ANALYTICAL_GRP_02"];
						//***************************************************

						// Log::write('info', '1',
						// 		[
						// 			"Work1InspectionPoint"  => $lWork1InspectionPoint,
						// 			"Work1AnalyGrp01"  => $lWork1AnalyGrp01,
						// 			"Work1AnalyGrp02"  => $lWork1AnalyGrp02,
						// 			"Work2InspectionPoint"  => $lWork2InspectionPoint,
						// 			"Work2AnalyGrp01"  => $lWork2AnalyGrp01,
						// 			"Work2AnalyGrp02"  => $lWork2AnalyGrp02,
						// 		]
						// 	);

						if ($lWork1InspectionPoint == $lWork2InspectionPoint)
						{
							$lInspectionSheetColCount = $lInspectionSheetColCount + 7;
						}
						else
						{
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValueExplicit($lCurrentRow1["INSPECTION_POINT"], PHPExcel_Cell_DataType::TYPE_STRING);

							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValueExplicit($lCurrentRow1["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);

							$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValueExplicit($lCurrentRow1["SPEC_WRITE_FORMAT"], PHPExcel_Cell_DataType::TYPE_STRING);

							if ($lCurrentRow1["INSPECTION_RESULT_INPUT_TYPE"] == self::RESULT_INPUT_TYPE_NUMBER)
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											->setValueExplicit($lCurrentRow1["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											->setValueExplicit($lCurrentRow1["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											->setValueExplicit($lCurrentRow1["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
							}
							else
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
							}

							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValueExplicit($lCurrentRow1["INSPECTION_TOOL_CLASS"], PHPExcel_Cell_DataType::TYPE_STRING);
						}

						if ($lWork1InspectionPoint == $lWork2InspectionPoint and $lWork1AnalyGrp01 == $lWork2AnalyGrp01)
						{
							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						}
						else
						{
							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValueExplicit($lCurrentRow1["ANALYTICAL_GRP_01"], PHPExcel_Cell_DataType::TYPE_STRING);
						}

						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValueExplicit($lCurrentRow1["ANALYTICAL_GRP_02"], PHPExcel_Cell_DataType::TYPE_STRING);


						foreach ($lTblResultDetailInspectionSheet As $lResultDetailRow2)
						{
							$lCurrentRow2 = (Array)$lResultDetailRow2;

							if ($lWork2InspectionPoint == $lCurrentRow2["INSPECTION_POINT"] and $lWork2AnalyGrp01 == $lCurrentRow2["ANALYTICAL_GRP_01"] and $lWork2AnalyGrp02 == $lCurrentRow2["ANALYTICAL_GRP_02"])
							{
								$lWorkResultCount = $lWorkResultCount + 1;

								if ($lCurrentRow1["INSPECTION_RESULT_INPUT_TYPE"] == self::RESULT_INPUT_TYPE_NUMBER)
								{
									$lInspectionSheetColCount = $lInspectionSheetColCount + 1;

									if ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == 1 or $lCurrentRow2["INSPECTION_RESULT_TYPE"] == 2 or $lCurrentRow2["INSPECTION_RESULT_TYPE"] == 3)
									{
										if ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == 1)
										{
											$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
														->setValueExplicit($lCurrentRow2["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
											$lWorkJudge = 1;
										}
										else
										{
											$lWorkValue = self::STAR.$lCurrentRow2["INSPECTION_RESULT_VALUE"];

											$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
														->setValueExplicit($lWorkValue, PHPExcel_Cell_DataType::TYPE_STRING);
											$lWorkJudge = 2;
										}
									}
									elseif ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == 4 or $lCurrentRow2["INSPECTION_RESULT_TYPE"] == 5)
									{
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
													->setValueExplicit(self::NOTHING, PHPExcel_Cell_DataType::TYPE_STRING);
										$lWorkJudge = 3;
									}
									else
									{
										$lWorkJudge = 4;
									}
								}
								else
								{
									$lInspectionSheetColCount = $lInspectionSheetColCount + 1;

									if ($lCurrentRow2["INSPECTION_RESULT_VALUE"] == 1)
									{
										$lInspectionResultOKNG = self::OK;
										$lWorkJudge = 1;
									}
									elseif ($lCurrentRow2["INSPECTION_RESULT_VALUE"] == 2)
									{
										$lInspectionResultOKNG = self::NG;
										$lWorkJudge = 2;
									}
									else
									{
										if ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == 4 or $lCurrentRow2["INSPECTION_RESULT_TYPE"] == 5)
										{
											$lInspectionResultOKNG = self::NOTHING;
											$lWorkJudge = 3;
										}
										else
										{
											$lInspectionResultOKNG = self::BLANK;
											$lWorkJudge = 4;
										}
									}

									$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
												->setValueExplicit($lInspectionResultOKNG, PHPExcel_Cell_DataType::TYPE_STRING);
								}

								if ($lWorkFinalJudge == 0)
								{
									$lWorkFinalJudge = $lWorkJudge;
								}
								else
								{
									if ($lWorkFinalJudge == 1)
									{
										if ($lWorkJudge == 2)
										{
											$lWorkFinalJudge = $lWorkJudge;
										}
									}
									elseif ($lWorkFinalJudge == 2)
									{
										//Nothing
									}
									elseif ($lWorkFinalJudge == 3)
									{
										if ($lWorkJudge == 1 or $lWorkJudge == 2)
										{
											$lWorkFinalJudge = $lWorkJudge;
										}
									}
									else
									{
										$lWorkFinalJudge = $lWorkJudge;
									}
								}
							}
						}

						// Log::write('info', '1',
						// 		[
						// 			"WorkResultCount"  => $lWorkResultCount,
						// 		]
						// 	);

						if ($lWorkResultCount < 8)
						{
							for ($WriteCount = $lWorkResultCount +1; $WriteCount <= 8; $WriteCount++)
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							}
						}
						elseif ($lWorkResultCount == 8)
						{
							//Nothing
						}
						else
						{
							$lInspectionSheetColCount = 11;
							for ($WriteCount = 1; $WriteCount < 9; $WriteCount++)
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											->setValueExplicit(self::NOTHING, PHPExcel_Cell_DataType::TYPE_STRING);
							}
						}

						if ($lWorkFinalJudge == 1)
						{
							$lFinalJudge = self::OK;
						}
						elseif ($lWorkFinalJudge == 2)
						{
							$lFinalJudge = self::NG;
						}
						elseif ($lWorkFinalJudge == 3)
						{
							$lFinalJudge = self::NOTHING;
						}
						else
						{
							$lFinalJudge = self::BLANK;
						}

						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValueExplicit($lFinalJudge, PHPExcel_Cell_DataType::TYPE_STRING);

						//***************************************************
						//leave item
						$lWork1InspectionPoint      = $lWork2InspectionPoint;
						$lWork1AnalyGrp01           = $lWork2AnalyGrp01;
						$lWork1AnalyGrp02           = $lWork2AnalyGrp02;
						$lWorkResultCount           = 0;
						$lWorkJudge                 = 0;
						$lWorkFinalJudge            = 0; 
						//***************************************************
					}

					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					//保存ファイル名とパスの設定（テンプレートファイル名の「template」を「現在の年月日_時分秒」で置換した値
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet); 
					$lSaveFilePathAndName = "excel/".$lSaveFileName;  
					// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

					//save file（save in server temporarily）
					//ファイルの保存（サーバに一旦保存）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					//データ件数が多いと、このSAVE処理が異常に長い。保存せず、直接DL出来れば改善の可能性。
					$objWriter->save($lSaveFilePathAndName);

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete fime(saved in server)
					//ファイルの削除（サーバに保存していたもの）
					unlink($lSaveFilePathAndName);

					//message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";

// Excel 97-2003 形式で作成し、ブラウザに出力する
// header('Content-Type: application/vnd.ms-excel');
// header('Content-Disposition: attachment;filename="excel97-2003.xls"');
// header('Cache-Control: max-age=0');

// $writer = PHPExcel_IOFactory::createWriter($excel, "Excel5");
// $writer->save('php://output');
// 
				}
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E997 : Target data does not exist."
				]);
			}
		}
		elseif (Input::has('btnDataImport'))                          //Data Import button
		{
			Log::write('info', 'BA1010 Data Import Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			$lImportInspectionResultNo = Input::get('hidInspectionResultNo');
			$lImportDataRev = Input::get('hidDataRev');

			// if ($lImportInspectionResultNo != null)
			// {
				//Check File
				$lViewData = $this->checkFile($lViewData);

				if (array_key_exists("errors", $lViewData) == false)
				{
					$lTblDataPatarn = $this->getPatarn(self::MITUTOYO_CRYSTAAPEX_S544);

					if ($lTblDataPatarn != null)
					{
						$lRowDataPatarn = (Array)$lTblDataPatarn[0];

						if ($lRowDataPatarn["PTN_NO"] == self::MITUTOYO_CRYSTAAPEX_S544)
						{
							$lImportFileName = Input::file('filUploadFile')->getClientOriginalName();
							Log::write('info', '1');
							copy(Input::file('filUploadFile') ,"excel/WorkDataImport/".basename(Input::file('filUploadFile')->getClientOriginalName()));
							Log::write('info', '2');

							$lOutputFileName1 = "OutputNo01_".$lViewData["UserID"]."_".date("Ymd")."_".date("His")."_".$lImportFileName;
							Log::write('info', '3');
							$lOutputFileName2 = "OutputNo02_".$lViewData["UserID"]."_".date("Ymd")."_".date("His")."_".$lImportFileName;
							Log::write('info', '4');
							$lOutputFile2 = "C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName2;
							Log::write('info', '5');

							// exec("cmd.exe /c test.bat param1 param2");
							// exec("cmd.exe /c test.bat" ." " .$_POST["id"]　・・・);
							// exec('c:\WINDOWS\system32\cmd.exe /c START C:/FactoryZeroDataimport/AAA.bat');
							$lCommand1st = $lRowDataPatarn["CALL_CONTENTS01"]." C:/laravel/factoryz/public/excel/WorkDataImport/".$lImportFileName." C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName1;
							// $lCommand1st = "cmd.exe /c C:/usr/local/OutputTalendModule/test_job_FromatConv_Plasess_0.1/test_job_FromatConv_Plasess/test_job_FromatConv_Plasess_run.bat C:/laravel/factoryz/public/excel/WorkDataImport/".$lImportFileName." C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName1;
							exec($lCommand1st, $RtnOut1, $RtnSts1);

							Log::write('info', '1',
								[
									"Command1st"  => $lCommand1st,
									"RtnSts1"  => $RtnSts1,
								]
							);
							

							if ($RtnSts1 == 0)
							{
								$lCommand2nd = $lRowDataPatarn["CALL_CONTENTS02"]." C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName1." C:/laravel/factoryz/public/excel/WorkDataImport/".$lOutputFileName2;
								exec($lCommand2nd, $RtnOut2, $RtnSts2);

								Log::write('info', '2',
									[
										"Command2nd"  => $lCommand2nd,
										"RtnSts2"  => $RtnSts2,
									]
								);

								

								if ($RtnSts2 == 0)
								{
									$lines = file('$lOutputFile2');

									foreach ($lines as $line)
									{
										$line = (Array)$line;

										echo "<pre>";
										print_r($line);
										echo "</pre>";


										DB::update('
								        UPDATE TRESDETT
								           SET INSPECTION_RESULT_VALUE       = :InspectionResultValue
								              ,INSPECTION_RESULT_TYPE        = :InspectionResultType
								              ,OFFSET_NG_REASON              = :OffsetNGReason
								              ,OFFSET_NG_ACTION              = :OffsetNGAction
								              ,LAST_UPDATE_USER_ID           = :LastUpdateUserID
								              ,UPDATE_YMDHMS                 = now()
								              ,DATA_REV                      = DATA_REV + 1
										 WHERE INSPECTION_RESULT_NO          = :InspectionResultNo
										   AND INSPECTION_NO                 = :InspectionNo
												',
											[
												"InspectionResultValue"        => $lRowDataPatarn[""],
												"InspectionResultType"         => 1,
												"OffsetNGReason"               => 1,
												"OffsetNGAction"               => 1,
												"LastUpdateUserID"             => $pInspectorCode,
												"InspectionResultNo"           => $pInspectionResultNo,
												"InspectionNo"                 => $pInspectionNo,
											]
										);
										Log::write('info', '111');
									}
								}
								else
								{
									//Output Error List




								}
							}
							else
							{
								$lViewData["errors"] = new MessageBag([
									"error" => "E997 : Target data does not exist."
								]);
							}
						}
					}
					else
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E997 : Target data does not exist."
						]);
					}
				}




			// }
		}
		else                                                                    //transition, paging from other screen or menu
		{
			//当画面のセッション情報のうち、画面入力情報以外の情報を全てクリアする（検索結果等を消して、検索前の状態に戻すため）
			//遷移元のURLに「index.php/user/list含まない場合」（＝他画面からの遷移の場合）＝False
			//遷移元がそれ以外の場合は文字列が返ってくる（Falseにはならない）
			//clear information except for entry in screen in session（delete search result and restore before state
			//in case index.php/user/list do not include in URL,(＝transition from other screen）＝False
			//don't be False
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/list');

				if($lPrevURL == false)
				{
					//delete information of search（reset）
					$this->initializeSessionData();
					
					//if transition from other screen,get value from session
					$lCmbValProcessForEntry = Session::get('BA1010ProcessForEntry');
					$lCmbValCustomerForEntry = Session::get('BA1010CustomerForEntry');
					$lCmbValCheckSheetNoForEntry = Session::get('BA1010CheckSheetNoForEntry');
					
					$lCmbValProcessForSearch = Session::get('BA1010ProcessForSearch');
					$lCmbValCustomerForSearch = Session::get('BA1010CustomerForSearch');
					$lCmbValCheckSheetNoForSearch = Session::get('BA1010CheckSheetNoForSearch');
					
					//上に場所移動
					//delete information of search（reset）
					//$this->initializeSessionData();
				}
			}
		}


		//以下の処理は他画面またはメニューからの遷移、ページング時、ボタン処理時等、全ての場合で通過させる
		//■must pass (transition,paging,button, from other screen, menu)

		//set list
		$lViewData = $this->setInspectionTimeList($lViewData);
		$lViewData = $this->setConditionList($lViewData);

		//if list of search result does not exist in session, set blank in
		if (is_null(Session::get('BA1010InspectionResultData')))
		{
			$lTblSearchResultData = [];
			$lTblNotExistNoData   = [];
		}
		else
		{
			//pop up data in session
			$lTblSearchResultData = Session::get('BA1010InspectionResultData');
			$lTblNotExistNoData   = Session::get('BA1010NotExistNoData');
		}

		// echo "<pre>";
		// print_r($lTblSearchResultData);
		// echo "</pre>";
		// echo "<pre>";
		// print_r($lTblNotExistNoData);
		// echo "</pre>";

		//make pagenation
		//data,total number of issue,number per 1page
		$lPagenation = Paginator::make($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE);

		//add to array for transportion to screen(written in +=))
		$lViewData += [
			"Pagenator"       => $lPagenation,
			"NotExistNoData"  => $lTblNotExistNoData,
		];

		//画面入力項目の保持と再設定
		//store and set again entry in screen
		$lViewData = $this->setListForms($lViewData);
		$lViewData = $this->setProcessList($lViewData);
		//-----------------------------
		//set combo process (for Entry)
		//-----------------------------
		if (Input::has('cmbProcessForEntry'))
		{
			$lViewData += [
				"ProcessForEntry" => (String)Input::get('cmbProcessForEntry'),
			];
		}
		else
		{
			$lViewData += [
				"ProcessForEntry" => "",
			];
		}
		//-----------------------------
		//set combo process (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch'))
		{
			$lViewData += [
				"ProcessForSearch" => (String)Input::get('cmbProcessForSearch'),
			];
		}
		else
		{
			$lViewData += [
				"ProcessForSearch" => "",
			];
		}

		//↓↓ Add Hoshina 04-Aug-2017 ↓↓
		//-----------------------------
		//set combo ①etc (for Entry)
		//-----------------------------
//		if (Input::has('txtMasterNoForEntry') && Input::has('cmbProcessForEntry'))
//		{
//			//in case value exist in customer infomation for search
//			$lViewData = $this->setCustomerNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), "arrDataListCustomerList");
//
//			if (count($lViewData['arrDataListCustomerList']) == 1)
//			{
//				$lTblCustomerID = $this->getCustomerID(Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'));
//				
//				if($lTblCustomerID != null)
//				{
//					$lRowCustomerID = (Array)$lTblCustomerID[0];
//					//in case value exist in customer infomation for search
//					//$pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey
//					$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], "arrCheckSheetNoForEntryList");
//
//						if (Input::has('cmbCheckSheetNoForEntry'))
//						{
//							//in case value exist in customer infomation for search
//							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialNameForEntryList");
//							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialSizeForEntryList");
//						}
//						else
//						{
//							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
//							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
//						}
//				}
//			}
//		}
		//↑↑


		$lViewData = $this->setCustomerList($lViewData);
		//-----------------------------
		//set combo customer (for Entry)
		//-----------------------------
		if (Input::has('cmbCustomerForEntry'))
		{
			$lViewData += [
				"CustomerForEntry" => (String)Input::get('cmbCustomerForEntry'),
			];
		}
		else
		{
			$lViewData += [
				"CustomerForEntry" => "",
			];
		}
		//-----------------------------
		//set combo customer (for Search)
		//-----------------------------
		if (Input::has('cmbCustomerForSearch'))
		{
			$lViewData += [
				"CustomerForSearch" => (String)Input::get('cmbCustomerForSearch'),
			];
		}
		else
		{
			$lViewData += [
				"CustomerForSearch" => "",
			];
		}

		//-----------------------------
		//set combo InspectionSheetNo, MaterialName MaterialSize (for Entry)
		//-----------------------------
		if (Input::has('cmbProcessForEntry') && Input::has('cmbCustomerForEntry'))
		{
			$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), "arrCheckSheetNoForEntryList");
			
			if (Input::has('cmbCheckSheetNoForEntry'))
			{
				$lViewData = $this->setMaterialNameList($lViewData, Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), Input::get('cmbCheckSheetNoForEntry'), "arrMaterialNameForEntryList");
				$lViewData = $this->setMaterialSizeList($lViewData, Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), Input::get('cmbCheckSheetNoForEntry'), "arrMaterialSizeForEntryList");
			}
			else
			{
				$lViewData = $this->setMaterialNameList($lViewData, Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
				$lViewData = $this->setMaterialSizeList($lViewData, Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
			}
		}
		else
		{
			$lViewData = $this->setCheckSheetNoListEntry($lViewData, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, "arrCheckSheetNoForEntryList");
			$lViewData = $this->setMaterialNameList($lViewData, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
			$lViewData = $this->setMaterialSizeList($lViewData, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
		}

		//-----------------------------
		//set combo ①Inspection Sheet No (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch'))
		{
			$lViewData = $this->setCheckSheetNoListSearch($lViewData, Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), "arrCheckSheetNoForSearchList");
		}
		else
		{
			$lViewData = $this->setCheckSheetNoListSearch($lViewData, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, "arrCheckSheetNoForSearchList");
		}

		//-----------------------------
		//set combo ①Revision No ②MaterialName ③MaterialSize (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch') && Input::has('cmbCheckSheetNoForSearch'))
		{
			$lViewData = $this->setRevNoList($lViewData, Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'));
			$lViewData = $this->setMaterialNameList($lViewData, Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'), "arrMaterialNameForSearchList");
			$lViewData = $this->setMaterialSizeList($lViewData, Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'), "arrMaterialSizeForSearchList");
		}
		else
		{
			$lViewData = $this->setRevNoList($lViewData, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch);
			$lViewData = $this->setMaterialNameList($lViewData, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch, "arrMaterialNameForSearchList");
			$lViewData = $this->setMaterialSizeList($lViewData, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch, "arrMaterialSizeForSearchList");
		}

		return View::make("user/list", $lViewData);
	}

	//**************************************************************************
	// process         ★setInspectionTimeList
	// overview        display list of Inspection Time on screen. control value in session
	// argument        Array
	// return value    Array
	// author          s-miyamoto
	// date            2014.05.23
	// record of updates  2014.05.23 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function setInspectionTimeList($pViewData)
	{

		$lArrDataListInspectionTime     = ["" => ""];  //list of Inspection Time to return to screen

		//if list of Inspection Time does not exist in session, search and store in session
		if (is_null(Session::get('BA1010InspectionTimeDropdownListData'))) {

			//search inspection time
			$lArrDataListInspectionTime = $this->getInspectionTimeList();

			//sessionにstore
			Session::put('BA1010InspectionTimeDropdownListData', $lArrDataListInspectionTime);
		}
		else
		{
			//pop up data in session
			$lArrDataListInspectionTime = Session::get('BA1010InspectionTimeDropdownListData');
		}

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListInspectionTime" => $lArrDataListInspectionTime
		];

		return $pViewData;
	}

	//**************************************************************************
	// process            ★getInspectionTimeList
	// overview           search list of Inspection Time and return it to calling as Array
	// argument           Nothing
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionTimeList()
	{
		$lTblInspectionTime         = [];         //DataTable（for Inspection Time data）
		$lRowInspectionTime         = [];         //DataRow（for Inspection Time data）
		$lArrDataListInspectionTime = ["" => ""]; //list of Inspection Time to return to screen

		$lTblInspectionTime = DB::select('
			  SELECT INSPECTION_TIME_ID
			        ,INSPECTION_TIME_NAME
			    FROM TINSPTIM
			   WHERE DELETE_FLG = "0"
			ORDER BY DISPLAY_ORDER
			        ,INSPECTION_TIME_ID
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblInspectionTime = (array)$lTblInspectionTime;

		//data exist
		if ($lTblInspectionTime != null)
		{
			//store result in Array again
			foreach ($lTblInspectionTime as $lRowInspectionTime)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowInspectionTime = (Array)$lRowInspectionTime;

				$lArrDataListInspectionTime += [
					$lRowInspectionTime["INSPECTION_TIME_ID"] => $lRowInspectionTime["INSPECTION_TIME_NAME"]
				];
			}
		}

		return $lArrDataListInspectionTime;
	}

	//**************************************************************************
	// process            ★setConditionList
	// overview           display list of Condition on screen.control value in session
	// argument           Array
	// return value       Array
	// record of updates  No,1 2015.05.16
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function setConditionList($pViewData)
	{
		$lArrDataListCondition     = ["" => ""];  //list of condition data to return to screen

		//if list of Condition in session,search and store in session
		if (is_null(Session::get('BA1010ConditionDropdownListData')))
		{
			//search inspection time
			$lArrDataListCondition = $this->getConditionList();

			//sessionにstore
			Session::put('BA1010ConditionDropdownListData', $lArrDataListCondition);
		}
		else
		{
			//pop up data in session
			$lArrDataListCondition = Session::get('BA1010ConditionDropdownListData');
		}

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListCondition" => $lArrDataListCondition
		];

		return $pViewData;
	}

	//**************************************************************************
	// process            ★getConditionList
	// overview           search list of condition and return it to calling as Array
	// argument           Nothing
	// return value       Array
	// record of updates  No,1 2015.05.16
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getConditionList()
	{
		$lTblCondition         = [];         //DataTable（for Condition data）
		$lRowCondition         = [];         //DataRow（for Condition data）
		$lArrDataListCondition = ["" => ""]; //list of condition data to return to screen

		$lTblCondition = DB::select('
			  SELECT CODE_ORDER
			        ,CODE_NAME
			    FROM TCODEMST
			   WHERE CODE_CLASS = "001"
			ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCondition = (array)$lTblCondition;

		//data exist
		if ($lTblCondition != null)
		{
			//store result in Array again
			foreach ($lTblCondition as $lRowCondition)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCondition = (Array)$lRowCondition;

				$lArrDataListCondition += [
					$lRowCondition["CODE_ORDER"] => $lRowCondition["CODE_NAME"]
				];
			}
		}

		return $lArrDataListCondition;
	}

	//**************************************************************************
	// process            ★getInspectionResultList
	// overview           search list of inspection result and return it to calling as Array
	// overview           
	// argument           Nothing
	// return value       Array
	// author             
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultList()
	{
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search
		$lProductionDateFromForSearch  = ""; //date From for search
		$lProductionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

		$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), "1");
		$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), "1");

			$lTblSearchResultData = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
			          ,HEAD.PROCESS_ID
			          ,PROCESS.CODE_NAME AS PROCESS_NAME
			          ,HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,HEAD.MACHINE_NO
			          ,MACH.MACHINE_NAME
			          ,HEAD.MOLD_NO
			          ,HEAD.MATERIAL_NAME
			          ,HEAD.MATERIAL_SIZE
			          ,HEAD.COLOR_ID
			          ,HEAD.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,USER.TEAM_ID
			          ,TEAM.CODE_NAME AS TEAM_NAME
			          ,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,HEAD.CONDITION_CD
			          ,COND.CODE_NAME AS CONDITION_NAME
			          ,HEAD.INSPECTION_TIME_ID
			          ,TIME.INSPECTION_TIME_NAME
			          ,HEAD.LOT_NO
			          ,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
			          ,HEAD.DATA_REV
			          ,SHEE.CUSTOMER_ID
			          ,CUSTOMER.CUSTOMER_NAME AS CUSTOMER_NAME
			          ,SHEE.ITEM_NO
			          ,ITEM.ITEM_NAME
			          ,IFNULL(SUB2.NORESULT_COUNT,0) AS NORESULT_COUNT
			          ,IFNULL((SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT),0) AS RESULT_COUNT
			          ,SUB1.TOTAL_COUNT
			          ,ROUND((IFNULL((SUB1.TOTAL_COUNT-SUB2.NORESULT_COUNT),0) / SUB1.TOTAL_COUNT) * 100, 2) AS RESULT_RATE
			      FROM TRESHEDT AS HEAD

			INNER JOIN TISHEETM AS SHEE
			        ON HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND HEAD.REV_NO              = SHEE.REV_NO
			       AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			INNER JOIN TMACHINM AS MACH
			        ON HEAD.MACHINE_NO          = MACH.MACHINE_NO
			INNER JOIN TCUSTOMM AS CUSTOMER
					ON SHEE.CUSTOMER_ID         = CUSTOMER.CUSTOMER_ID
			INNER JOIN TUSERMST AS USER
			        ON HEAD.INSERT_USER_ID      = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
			        ON HEAD.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
			 LEFT JOIN TITEMMST AS ITEM
			        ON SHEE.ITEM_NO             = ITEM.ITEM_NO

			INNER JOIN TCODEMST AS COND
			        ON COND.CODE_CLASS          = "001"
			       AND HEAD.CONDITION_CD        = COND.CODE_ORDER
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS       = "002"
			       AND HEAD.PROCESS_ID          = PROCESS.CODE_ORDER
		LEFT OUTER JOIN TCODEMST AS TEAM
					ON TEAM.CODE_CLASS          = "004"
			       AND USER.TEAM_ID             = TEAM.CODE_ORDER

			INNER JOIN (
			                SELECT S1HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S1DETL.INSPECTION_NO) AS TOTAL_COUNT
			                  FROM TRESHEDT AS S1HEAD
			            INNER JOIN TRESDETT AS S1DETL
			                    ON S1HEAD.INSPECTION_RESULT_NO = S1DETL.INSPECTION_RESULT_NO
			            INNER JOIN TISHEETM AS S1SHEE
			                    ON S1HEAD.INSPECTION_SHEET_NO = S1SHEE.INSPECTION_SHEET_NO
			                   AND S1HEAD.REV_NO              = S1SHEE.REV_NO
			                 WHERE S1HEAD.PROCESS_ID          = IF(:S1ProcessId1 <> "", :S1ProcessId2, S1HEAD.PROCESS_ID)
			                   AND S1SHEE.CUSTOMER_ID         = IF(:S1CustomerId1 <> "", :S1CustomerId2, S1SHEE.CUSTOMER_ID)
			                   AND S1HEAD.INSPECTION_SHEET_NO LIKE :S1CheckSheetNo
			                   AND S1HEAD.REV_NO              = IF(:S1RevisionNo1 <> "", :S1RevisionNo2, S1HEAD.REV_NO)
			                   AND S1HEAD.MATERIAL_NAME       = IF(:S1MaterialName1 <> "", :S1MaterialName2, S1HEAD.MATERIAL_NAME)
			                   AND S1HEAD.MATERIAL_SIZE       = IF(:S1MaterialSize1 <> "", :S1MaterialSize2, S1HEAD.MATERIAL_SIZE)
			                   AND S1HEAD.MACHINE_NO          = IF(:S1MachineNo1 <> "", :S1MachineNo2, S1HEAD.MACHINE_NO)
			                   AND S1HEAD.MOLD_NO             = IF(:S1MoldNo1 <> "", :S1MoldNo2, S1HEAD.MOLD_NO)
			                   AND S1HEAD.INSERT_USER_ID      = IF(:S1UserID1 <> "", :S1UserID2, S1HEAD.INSERT_USER_ID)
			                   AND S1HEAD.PRODUCTION_YMD      >= IF((:S1ProdDateFrom1 is not null) and (:S1ProdDateFrom2 <> ""), :S1ProdDateFrom3, S1HEAD.PRODUCTION_YMD)
			                   AND S1HEAD.PRODUCTION_YMD      <= IF((:S1ProdDateTo1 is not null) and (:S1ProdDateTo2 <> ""), :S1ProdDateTo3, S1HEAD.PRODUCTION_YMD)
			                   AND S1HEAD.INSPECTION_YMD      >= IF((:S1InspDateFrom1 is not null) and (:S1InspDateFrom2 <> ""), :S1InspDateFrom3, S1HEAD.INSPECTION_YMD)
			                   AND S1HEAD.INSPECTION_YMD      <= IF((:S1InspDateTo1 is not null) and (:S1InspDateTo2 <> ""), :S1InspDateTo3, S1HEAD.INSPECTION_YMD)
			                   AND S1SHEE.ITEM_NO             LIKE :S1PartNo
			                   AND S1HEAD.INSPECTION_TIME_ID  = IF((:S1InspTime1 is not null) and (:S1InspTime2 <> ""), :S1InspTime3, S1HEAD.INSPECTION_TIME_ID)
			              GROUP BY S1HEAD.INSPECTION_RESULT_NO
			            ) SUB1
			        ON HEAD.INSPECTION_RESULT_NO = SUB1.INSPECTION_RESULT_NO
			LEFT OUTER JOIN (
			                SELECT S2HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S2DETL.INSPECTION_NO) AS NORESULT_COUNT
			                  FROM TRESHEDT AS S2HEAD
			            INNER JOIN TRESDETT AS S2DETL
			                    ON S2HEAD.INSPECTION_RESULT_NO = S2DETL.INSPECTION_RESULT_NO
			            INNER JOIN TISHEETM AS S2SHEE
			                    ON S2HEAD.INSPECTION_SHEET_NO = S2SHEE.INSPECTION_SHEET_NO
			                   AND S2HEAD.REV_NO              = S2SHEE.REV_NO
			                 WHERE S2HEAD.PROCESS_ID          = IF(:S2ProcessId1 <> "", :S2ProcessId2, S2HEAD.PROCESS_ID)
			                   AND S2SHEE.CUSTOMER_ID         = IF(:S2CustomerId1 <> "", :S2CustomerId2, S2SHEE.CUSTOMER_ID)
			                   AND S2HEAD.INSPECTION_SHEET_NO LIKE :S2CheckSheetNo
			                   AND S2HEAD.REV_NO              = IF(:S2RevisionNo1 <> "", :S2RevisionNo2, S2HEAD.REV_NO)
			                   AND S2HEAD.MATERIAL_NAME       = IF(:S2MaterialName1 <> "", :S2MaterialName2, S2HEAD.MATERIAL_NAME)
			                   AND S2HEAD.MATERIAL_SIZE       = IF(:S2MaterialSize1 <> "", :S2MaterialSize2, S2HEAD.MATERIAL_SIZE)
			                   AND S2HEAD.MACHINE_NO          = IF(:S2MachineNo1 <> "", :S2MachineNo2, S2HEAD.MACHINE_NO)
			                   AND S2HEAD.MOLD_NO             = IF(:S2MoldNo1 <> "", :S2MoldNo2, S2HEAD.MOLD_NO)
			                   AND S2HEAD.INSERT_USER_ID      = IF(:S2UserID1 <> "", :S2UserID2, S2HEAD.INSERT_USER_ID)
			                   AND S2HEAD.PRODUCTION_YMD      >= IF((:S2ProdDateFrom1 is not null) and (:S2ProdDateFrom2 <> ""), :S2ProdDateFrom3, S2HEAD.PRODUCTION_YMD)
			                   AND S2HEAD.PRODUCTION_YMD      <= IF((:S2ProdDateTo1 is not null) and (:S2ProdDateTo2 <> ""), :S2ProdDateTo3, S2HEAD.PRODUCTION_YMD)
			                   AND S2HEAD.INSPECTION_YMD      >= IF((:S2InspDateFrom1 is not null) and (:S2InspDateFrom2 <> ""), :S2InspDateFrom3, S2HEAD.INSPECTION_YMD)
			                   AND S2HEAD.INSPECTION_YMD      <= IF((:S2InspDateTo1 is not null) and (:S2InspDateTo2 <> ""), :S2InspDateTo3, S2HEAD.INSPECTION_YMD)
			                   AND S2SHEE.ITEM_NO             LIKE :S2PartNo
			                   AND S2HEAD.INSPECTION_TIME_ID  = IF((:S2InspTime1 is not null) and (:S2InspTime2 <> ""), :S2InspTime3, S2HEAD.INSPECTION_TIME_ID)
			                   AND S2DETL.INSPECTION_RESULT_TYPE IN ("","4","5")
			              GROUP BY S2HEAD.INSPECTION_RESULT_NO
			            ) SUB2
			        ON HEAD.INSPECTION_RESULT_NO = SUB2.INSPECTION_RESULT_NO

			     WHERE HEAD.PROCESS_ID          = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			       AND SHEE.CUSTOMER_ID         = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
			       AND HEAD.INSPECTION_SHEET_NO LIKE :CheckSheetNo                                      /* 前方一致検索　未入力なら%のみ */
			       AND HEAD.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
			       AND HEAD.MATERIAL_NAME       = IF(:MaterialName1 <> "", :MaterialName2, HEAD.MATERIAL_NAME)
			       AND HEAD.MATERIAL_SIZE       = IF(:MaterialSize1 <> "", :MaterialSize2, HEAD.MATERIAL_SIZE)
			       AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
			       AND HEAD.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
			       AND HEAD.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)
			       AND HEAD.PRODUCTION_YMD      >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, HEAD.PRODUCTION_YMD)
			       AND HEAD.PRODUCTION_YMD      <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, HEAD.PRODUCTION_YMD)
			       AND HEAD.INSPECTION_YMD      >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, HEAD.INSPECTION_YMD)
			       AND HEAD.INSPECTION_YMD      <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, HEAD.INSPECTION_YMD)
			       AND SHEE.ITEM_NO             LIKE :PartNo
			       AND HEAD.INSPECTION_TIME_ID  = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, HEAD.INSPECTION_TIME_ID)

			  ORDER BY HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,HEAD.PROCESS_ID
			          ,CUSTOMER.CUSTOMER_ID
			          ,HEAD.PRODUCTION_YMD
			          ,HEAD.INSPECTION_YMD
			          ,TIME.DISPLAY_ORDER
			          ,TIME.INSPECTION_TIME_ID
			          ,HEAD.CONDITION_CD
			          ,USER.DISPLAY_ORDER
			          ,HEAD.INSERT_USER_ID
			',
				[
					"S1ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1CustomerId1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CustomerId2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CheckSheetNo"      => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S1RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1MaterialName1"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"S1MaterialName2"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"S1MaterialSize1"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"S1MaterialSize2"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"S1MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MoldNo1"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S1MoldNo2"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S1UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1ProdDateFrom1"     => $lProductionDateFromForSearch,
					"S1ProdDateFrom2"     => $lProductionDateFromForSearch,
					"S1ProdDateFrom3"     => $lProductionDateFromForSearch,
					"S1ProdDateTo1"       => $lProductionDateToForSearch,
					"S1ProdDateTo2"       => $lProductionDateToForSearch,
					"S1ProdDateTo3"       => $lProductionDateToForSearch,
					"S1InspDateFrom1"     => $lInspectionDateFromForSearch,
					"S1InspDateFrom2"     => $lInspectionDateFromForSearch,
					"S1InspDateFrom3"     => $lInspectionDateFromForSearch,
					"S1InspDateTo1"       => $lInspectionDateToForSearch,
					"S1InspDateTo2"       => $lInspectionDateToForSearch,
					"S1InspDateTo3"       => $lInspectionDateToForSearch,
					"S1PartNo"            => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"S1InspTime1"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S1InspTime2"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S1InspTime3"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),

					"S2ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2CustomerId1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CustomerId2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CheckSheetNo"      => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S2RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2MaterialName1"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"S2MaterialName2"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"S2MaterialSize1"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"S2MaterialSize2"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"S2MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2MoldNo1"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S2MoldNo2"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"S2UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2ProdDateFrom1"     => $lProductionDateFromForSearch,
					"S2ProdDateFrom2"     => $lProductionDateFromForSearch,
					"S2ProdDateFrom3"     => $lProductionDateFromForSearch,
					"S2ProdDateTo1"       => $lProductionDateToForSearch,
					"S2ProdDateTo2"       => $lProductionDateToForSearch,
					"S2ProdDateTo3"       => $lProductionDateToForSearch,
					"S2InspDateFrom1"     => $lInspectionDateFromForSearch,
					"S2InspDateFrom2"     => $lInspectionDateFromForSearch,
					"S2InspDateFrom3"     => $lInspectionDateFromForSearch,
					"S2InspDateTo1"       => $lInspectionDateToForSearch,
					"S2InspDateTo2"       => $lInspectionDateToForSearch,
					"S2InspDateTo3"       => $lInspectionDateToForSearch,
					"S2PartNo"            => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"S2InspTime1"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S2InspTime2"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"S2InspTime3"         => TRIM((String)Input::get('cmbInspectionTimeForSearch')),

					"ProcessId1"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerId1"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerId2"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MaterialName1"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialName2"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialSize1"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MaterialSize2"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MachineNo1"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID1"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"ProdDateFrom1"       => $lProductionDateFromForSearch,
					"ProdDateFrom2"       => $lProductionDateFromForSearch,
					"ProdDateFrom3"       => $lProductionDateFromForSearch,
					"ProdDateTo1"         => $lProductionDateToForSearch,
					"ProdDateTo2"         => $lProductionDateToForSearch,
					"ProdDateTo3"         => $lProductionDateToForSearch,
					"InspDateFrom1"       => $lInspectionDateFromForSearch,
					"InspDateFrom2"       => $lInspectionDateFromForSearch,
					"InspDateFrom3"       => $lInspectionDateFromForSearch,
					"InspDateTo1"         => $lInspectionDateToForSearch,
					"InspDateTo2"         => $lInspectionDateToForSearch,
					"InspDateTo3"         => $lInspectionDateToForSearch,
					"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"InspTime1"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime2"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime3"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
			);
        
		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process            ★getNotExistNoList
	// overview           search no entry List of Inspection No. and return it to calling as Array
	// overview           実績が未入力の検査番号リストを検索して、呼び出し元に配列で返却する
	// argument           Nothing
	// return value       Array
	// author             
	// record of updates  No,1 2018.02.08
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getNotExistNoList()
	{
		$lTblNotExistNoData = [];      //data table of no entry Inspection No.
		$lRowNotExistNoData = [];      //data row of no entry Inspection No.

		$lArrNotExistNoData = [];      //data of no entry Inspection No.（Array after treatment）
		$lWorkValue         = "";      //work to treat value
		$lRowCount          = 0;       //data line No.
		$lOldKey            = "";      //old Key（inspection result No.）
		$lNewKey            = "";      //new Key（inspection result No.）

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search
		$lProductionDateFromForSearch  = ""; //date From for search
		$lProductionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), "1");
		$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), "1");

		$lTblNotExistNoData = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
			          ,DETL.INSPECTION_NO
			      FROM TRESHEDT AS HEAD
			INNER JOIN TISHEETM AS SHEE
			        ON HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND HEAD.REV_NO              = SHEE.REV_NO
			       AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			INNER JOIN TRESDETT AS DETL
			        ON HEAD.INSPECTION_RESULT_NO = DETL.INSPECTION_RESULT_NO
			     WHERE HEAD.PROCESS_ID           = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)
			       AND SHEE.CUSTOMER_ID          = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
			       AND HEAD.INSPECTION_SHEET_NO  LIKE :CheckSheetNo
			       AND HEAD.REV_NO               = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
			       AND HEAD.MATERIAL_NAME        = IF(:MaterialName1 <> "", :MaterialName2, HEAD.MATERIAL_NAME)
			       AND HEAD.MATERIAL_SIZE        = IF(:MaterialSize1 <> "", :MaterialSize2, HEAD.MATERIAL_SIZE)
			       AND HEAD.MACHINE_NO           = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
			       AND HEAD.MOLD_NO              = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
			       AND HEAD.INSERT_USER_ID       = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)
			       AND HEAD.PRODUCTION_YMD       >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, HEAD.PRODUCTION_YMD)
			       AND HEAD.PRODUCTION_YMD       <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, HEAD.PRODUCTION_YMD)
			       AND HEAD.INSPECTION_YMD       >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, HEAD.INSPECTION_YMD)
			       AND HEAD.INSPECTION_YMD       <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, HEAD.INSPECTION_YMD)
			       AND SHEE.ITEM_NO              LIKE :PartNo
			       AND HEAD.INSPECTION_TIME_ID   = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, HEAD.INSPECTION_TIME_ID)
			       AND (DETL.INSPECTION_RESULT_TYPE = "" or DETL.INSPECTION_RESULT_TYPE is null)
			  ORDER BY HEAD.INSPECTION_RESULT_NO
			          ,DETL.INSPECTION_NO
			',
				[
					"ProcessId1"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerId1"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerId2"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MaterialName1"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialName2"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialSize1"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MaterialSize2"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MachineNo1"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID1"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"ProdDateFrom1"       => $lProductionDateFromForSearch,
					"ProdDateFrom2"       => $lProductionDateFromForSearch,
					"ProdDateFrom3"       => $lProductionDateFromForSearch,
					"ProdDateTo1"         => $lProductionDateToForSearch,
					"ProdDateTo2"         => $lProductionDateToForSearch,
					"ProdDateTo3"         => $lProductionDateToForSearch,
					"InspDateFrom1"       => $lInspectionDateFromForSearch,
					"InspDateFrom2"       => $lInspectionDateFromForSearch,
					"InspDateFrom3"       => $lInspectionDateFromForSearch,
					"InspDateTo1"         => $lInspectionDateToForSearch,
					"InspDateTo2"         => $lInspectionDateToForSearch,
					"InspDateTo3"         => $lInspectionDateToForSearch,
					"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"InspTime1"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime2"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime3"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
		);

		//if data exist, treat to be 2row,Inspection Result No. and no entry Inspection No.
		if ($lTblNotExistNoData != null)
		{
			//store result in Array again
			foreach ($lTblNotExistNoData as $lRowNotExistNoData)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowNotExistNoData = (Array)$lRowNotExistNoData;

				//store present value in old key exception from 0 line count(prevention of key break in 1 issue)
				if ($lRowCount == 0)
				{
					$lOldKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];
				}

				//set new key
				$lNewKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];

				//in case key break
				if ($lOldKey != $lNewKey)
				{
					//key break
					//delete the last comma in the string maked in work 
					$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

					//set Array to return
					$lArrNotExistNoData += [
						$lOldKey => $lWorkValue
					];

					//clear work
					$lWorkValue = "";
				}

				//connect no exist No. in work
				$lWorkValue .= $lRowNotExistNoData["INSPECTION_NO"]."-";

				//set value reading to old key
				$lOldKey = $lNewKey;

				//add line No.
				$lRowCount += 1;
			}

			//store data remaining after read all
			//delete the last comma in the string maked in work
			$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

			//set Array to return
			$lArrNotExistNoData += [
				$lOldKey => $lWorkValue
			];
		}

		return $lArrNotExistNoData;
	}

	//**************************************************************************
	// process            ★getInspectionResultCSV
	// overview           search list of inspection result and return it to calling as Array
	// overview           
	// argument           Nothing
	// return value       Array
	// author             s-miyamoto
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function getInspectionResultCSV()
	{
		$lTblInspectionResultCSV = [];

		$lInspectionDateFromForSearch  = "";
		$lInspectionDateToForSearch    = "";
		$lProductionDateFromForSearch  = "";
		$lProductionDateToForSearch    = "";

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");
		$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), "1");
		$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), "1");

			$lTblInspectionResultCSV = DB::select('
			         SELECT REHE.INSPECTION_RESULT_NO
							,REHE.INSPECTION_SHEET_NO
							,SHEE.INSPECTION_SHEET_NAME
							,REHE.REV_NO
							,PROCESS.CODE_NAME AS PROCESS_NAME
							,CUST.CUSTOMER_NAME
							,SHEE.MODEL_NAME
							,SHEE.ITEM_NO
							,ITEM.ITEM_NAME
							,SHEE.DRAWING_NO
							,SHEE.ISO_KANRI_NO
							,MACH.MACHINE_NAME
							,REHE.MOLD_NO
							,REHE.MATERIAL_NAME
							,REHE.MATERIAL_SIZE
							,REHE.HEAT_TREATMENT_TYPE
							,REHE.PLATING_KIND
							,COLOR.CODE_NAME AS COLOR_NAME
							,REHE.INSERT_USER_ID
							,USER1.USER_NAME AS INSERT_USER_NAME
							,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
							,COND.CODE_NAME AS CONDITION_NAME
							,TIME.INSPECTION_TIME_NAME
							,REHE.MATERIAL_LOT_NO	
							,REHE.CERTIFICATE_NO
							,REHE.PO_NO
							,REHE.INVOICE_NO
							,REHE.QUANTITY_COIL
							,REHE.QUANTITY_WEIGHT
							,REHE.LOT_NO
							,DATE_FORMAT(REHE.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
							,REDE.INSPECTION_NO
							,REDE.INSPECTION_POINT
							,REDE.ANALYTICAL_GRP_01
							,REDE.ANALYTICAL_GRP_02
							,REDE.SAMPLE_NO
							,REDE.DISPLAY_ORDER
							,ISNO.INSPECTION_ITEM
							,ISNO.TOOL_CD
							,ISNO.INSPECTION_TOOL_CLASS
							,ifnull(ISNO.THRESHOLD_NG_UNDER,"") AS THRESHOLD_NG_UNDER
							,ifnull(ISNO.THRESHOLD_OFFSET_UNDER,"") AS THRESHOLD_OFFSET_UNDER
							,ifnull(ISNO.REFERENCE_VALUE,"") AS REFERENCE_VALUE
							,ifnull(ISNO.THRESHOLD_OFFSET_OVER,"") AS THRESHOLD_OFFSET_OVER
							,ifnull(ISNO.THRESHOLD_NG_OVER,"") AS THRESHOLD_NG_OVER
							,REDE.INSPECTION_RESULT_VALUE
							,CASE
								WHEN REDE.INSPECTION_RESULT_TYPE = "1" THEN "OK"
								WHEN REDE.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
								WHEN REDE.INSPECTION_RESULT_TYPE = "3" THEN "NG"
								WHEN REDE.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
								WHEN REDE.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
								ELSE ""
							END INSPECTION_RESULT_TYPE
							,REDE.OFFSET_NG_REASON
							,CASE
								WHEN REDE.OFFSET_NG_ACTION = "1" THEN "Running"
								WHEN REDE.OFFSET_NG_ACTION = "2" THEN "Stop Line"
								ELSE ""
							END OFFSET_NG_ACTION
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_INTERNAL
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_REQUEST
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_TSR
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_SORTING
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_NOACTION
							,REDE.ACTION_DETAILS_TR_NO_TSR_NO
							,ANA.CALC_NUM
							,ANA.MAX_VALUE
							,ANA.MIN_VALUE
							,ANA.AVERAGE_VALUE
							,ANA.RANGE_VALUE
							,ANA.STDEV
							,ANA.CPK
							,ANA.XBAR_UCL
							,ANA.XBAR_LCL
							,ANA.RCHART_UCL
							,ANA.RCHART_LCL
							,ANA.JUDGE_REASON
							,DATE_FORMAT(REDE.INSERT_YMDHMS,"%d-%m-%Y %k:%i:%s") AS INSERT_YMDHMS
							,REDE.LAST_UPDATE_USER_ID
							,USER2.USER_NAME AS UPDATE_USER_NAME
							,DATE_FORMAT(REDE.UPDATE_YMDHMS,"%d-%m-%Y %k:%i:%s") AS UPDATE_YMDHMS
						FROM TRESHEDT AS REHE

					INNER JOIN TRESDETT AS REDE
							ON REHE.INSPECTION_RESULT_NO = REDE.INSPECTION_RESULT_NO
			   LEFT OUTER JOIN TRESANAL AS ANA
							ON REDE.INSPECTION_RESULT_NO = ANA.INSPECTION_RESULT_NO
					INNER JOIN TISHEETM AS SHEE
							ON REHE.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
						   AND REHE.REV_NO               = SHEE.REV_NO
					INNER JOIN TINSPNOM AS ISNO
							ON REHE.INSPECTION_SHEET_NO  = ISNO.INSPECTION_SHEET_NO
						   AND REHE.REV_NO               = ISNO.REV_NO
						   AND REDE.INSPECTION_POINT     = ISNO.INSPECTION_POINT
			   LEFT OUTER JOIN TITEMMST AS ITEM
							ON SHEE.ITEM_NO              = ITEM.ITEM_NO
						   AND ITEM.DELETE_FLG           = "0"

					INNER JOIN TUSERMST AS USER1
							ON REHE.INSERT_USER_ID       = USER1.USER_ID
					INNER JOIN TUSERMST AS USER2
							ON REDE.LAST_UPDATE_USER_ID  = USER2.USER_ID
					INNER JOIN TINSPTIM AS TIME
							ON REHE.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
					INNER JOIN TMACHINM AS MACH
							ON REHE.MACHINE_NO           = MACH.MACHINE_NO
					INNER JOIN TCUSTOMM AS CUST
							ON SHEE.CUSTOMER_ID          = CUST.CUSTOMER_ID
					INNER JOIN TCODEMST AS COND
							ON COND.CODE_CLASS           = "001"
						   AND REHE.CONDITION_CD         = COND.CODE_ORDER
					INNER JOIN TCODEMST AS PROCESS
							ON PROCESS.CODE_CLASS        = "002"
						   AND REHE.PROCESS_ID           = PROCESS.CODE_ORDER
			   LEFT OUTER JOIN TCODEMST AS COLOR
							ON COLOR.CODE_CLASS          = "006"
						   AND REHE.COLOR_ID             = COLOR.CODE_ORDER

						WHERE REHE.PROCESS_ID        = IF(:ProcessId1 <> "", :ProcessId2, REHE.PROCESS_ID)
						AND SHEE.CUSTOMER_ID         = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
						AND REHE.INSPECTION_SHEET_NO LIKE :CheckSheetNo
						AND REHE.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, REHE.REV_NO)
						AND REHE.MATERIAL_NAME       = IF(:MaterialName1 <> "", :MaterialName2, REHE.MATERIAL_NAME)
						AND REHE.MATERIAL_SIZE       = IF(:MaterialSize1 <> "", :MaterialSize2, REHE.MATERIAL_SIZE)
						AND REHE.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, REHE.MACHINE_NO)
						AND REHE.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, REHE.MOLD_NO)
						AND REHE.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, REHE.INSERT_USER_ID)
						AND SHEE.ITEM_NO             LIKE :PartNo
						AND REHE.PRODUCTION_YMD      >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, REHE.PRODUCTION_YMD)
						AND REHE.PRODUCTION_YMD      <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, REHE.PRODUCTION_YMD)
						AND REHE.INSPECTION_YMD      >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, REHE.INSPECTION_YMD)
						AND REHE.INSPECTION_YMD      <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, REHE.INSPECTION_YMD)
						AND REHE.INSPECTION_TIME_ID  = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, REHE.INSPECTION_TIME_ID)

			            AND SHEE.DELETE_FLG           = "0"
			            AND ISNO.DELETE_FLG           = "0"
			            AND USER1.DELETE_FLG          = "0"
			            AND USER2.DELETE_FLG          = "0"
			            AND TIME.DELETE_FLG           = "0"
			            AND MACH.DELETE_FLG           = "0"
			            AND CUST.DELETE_FLG           = "0"

					ORDER BY REHE.INSPECTION_SHEET_NO
							,REHE.REV_NO
							,REHE.PROCESS_ID
							,REHE.PRODUCTION_YMD
							,REHE.INSPECTION_YMD
							,TIME.DISPLAY_ORDER
							,TIME.INSPECTION_TIME_ID
							,REHE.CONDITION_CD
							,USER1.DISPLAY_ORDER
							,REHE.INSERT_USER_ID
							,REDE.DISPLAY_ORDER
							,REDE.INSPECTION_NO
			',
				[
					"ProcessId1"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"          => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerId1"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerId2"         => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"        => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"         => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MaterialName1"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialName2"       => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialSize1"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MaterialSize2"       => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MachineNo1"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"          => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"             => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID1"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"             => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"PartNo"              => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"ProdDateFrom1"       => $lProductionDateFromForSearch,
					"ProdDateFrom2"       => $lProductionDateFromForSearch,
					"ProdDateFrom3"       => $lProductionDateFromForSearch,
					"ProdDateTo1"         => $lProductionDateToForSearch,
					"ProdDateTo2"         => $lProductionDateToForSearch,
					"ProdDateTo3"         => $lProductionDateToForSearch,
					"InspDateFrom1"       => $lInspectionDateFromForSearch,
					"InspDateFrom2"       => $lInspectionDateFromForSearch,
					"InspDateFrom3"       => $lInspectionDateFromForSearch,
					"InspDateTo1"         => $lInspectionDateToForSearch,
					"InspDateTo2"         => $lInspectionDateToForSearch,
					"InspDateTo3"         => $lInspectionDateToForSearch,
					"InspTime1"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime2"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspTime3"           => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
			);

		return $lTblInspectionResultCSV;
	}

	//**************************************************************************
	// process            ★createInspectionResultCSV
	// overview           treatment of header and data for inspection result CSV
	// overview           
	// argument           inspection result data table
	// return value       inspection result CSV
	// author             s-miyamoto
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function createInspectionResultCSV($pTblInspectionResultCSV)
	{
		$lOutputCSVData          = "";
		$lRowInspectionResultCSV = [];

		//header
		$lOutputCSVData .= "Inspection Result Seq.No.,";
		$lOutputCSVData .= "Inspection(Check) Sheet No.,";
		$lOutputCSVData .= "Check Sheet Title,";
		$lOutputCSVData .= "Revision No.,";
		$lOutputCSVData .= "Process,";
		$lOutputCSVData .= "Customer Name,";
		$lOutputCSVData .= "Model Name,";
		$lOutputCSVData .= "Parts No.,";
		$lOutputCSVData .= "Parts Name,";
		$lOutputCSVData .= "Drawing No.,";
		$lOutputCSVData .= "ISO No.,";
		$lOutputCSVData .= "Machine Name,";
		$lOutputCSVData .= "Mold No.,";
		$lOutputCSVData .= "Material Name,";
		$lOutputCSVData .= "Material Size,";
		$lOutputCSVData .= "Heat Treatment Type,";
		$lOutputCSVData .= "Plating Kind,";
		$lOutputCSVData .= "COLOR_ID,";
		$lOutputCSVData .= "Inspecter ID,";
		$lOutputCSVData .= "Inspector Name,";
		$lOutputCSVData .= "Inspection Date,";
		$lOutputCSVData .= "Condition,";
		$lOutputCSVData .= "Inspection Time,";
		$lOutputCSVData .= "Material Lot No.,";
		$lOutputCSVData .= "Certificate No.,";
		$lOutputCSVData .= "PO No.,";
		$lOutputCSVData .= "Invoice No.,";
		$lOutputCSVData .= "Quantity Coil,";
		$lOutputCSVData .= "Quantity Weight,";
		$lOutputCSVData .= "Lot No.,";
		$lOutputCSVData .= "Production Date,";
		$lOutputCSVData .= "Inspection(Check) No.,";
		$lOutputCSVData .= "Inspection Point,";
		$lOutputCSVData .= "Cavity,";
		$lOutputCSVData .= "Point of Cavity,";
		$lOutputCSVData .= "Sample No.,";
		$lOutputCSVData .= "Inspection Order,";
		$lOutputCSVData .= "Inspection Items,";
		$lOutputCSVData .= "Tool Code,";
		$lOutputCSVData .= "Equipment,";
		$lOutputCSVData .= "NG Under,";
		$lOutputCSVData .= "OFFSET Under,";
		$lOutputCSVData .= "Center,";
		$lOutputCSVData .= "OFFSET Over,";
		$lOutputCSVData .= "NG Over,";
		$lOutputCSVData .= "Inspection Result,";
		$lOutputCSVData .= "Result Class,";
		$lOutputCSVData .= "OFFSET Reason / NG Reason,";
		$lOutputCSVData .= "OFFSET Action / NG Action,";
		$lOutputCSVData .= "Internal Approve,";
		$lOutputCSVData .= "Request Offset,";
		$lOutputCSVData .= "TSR,";
		$lOutputCSVData .= "Sorting,";
		$lOutputCSVData .= "No Action,";
		$lOutputCSVData .= "Action Details / Technician Request No. / TSR No.,";
		$lOutputCSVData .= "Calc Qty,";
		$lOutputCSVData .= "Max Value,";
		$lOutputCSVData .= "Min Value,";
		$lOutputCSVData .= "Average Value,";
		$lOutputCSVData .= "Range Value,";
		$lOutputCSVData .= "STDEV,";
		$lOutputCSVData .= "CPK,";
		$lOutputCSVData .= "UCL(Xbar),";
		$lOutputCSVData .= "LCL(Xbar),";
		$lOutputCSVData .= "UCL(Rchart),";
		$lOutputCSVData .= "LCL(Rchart),";
		$lOutputCSVData .= "Judge Reason,";
		$lOutputCSVData .= "Entry Date/Time,";
		$lOutputCSVData .= "Modify User ID,";
		$lOutputCSVData .= "Modify User Name,";
		$lOutputCSVData .= "Modify Date/Time,";
		$lOutputCSVData .= "\n";

		foreach ($pTblInspectionResultCSV as $lRowInspectionResultCSV)
		{
			$lRowInspectionResultCSV = (Array)$lRowInspectionResultCSV;

			//string concatenation while using treatment function
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REV_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PROCESS_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CUSTOMER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MODEL_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["DRAWING_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ISO_KANRI_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MACHINE_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MOLD_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_SIZE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["HEAT_TREATMENT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PLATING_KIND"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["COLOR_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CONDITION_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TIME_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_LOT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CERTIFICATE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PO_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INVOICE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["QUANTITY_COIL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["QUANTITY_WEIGHT"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["LOT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PRODUCTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_POINT"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ANALYTICAL_GRP_01"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ANALYTICAL_GRP_02"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["SAMPLE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["DISPLAY_ORDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_ITEM"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TOOL_CD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TOOL_CLASS"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REFERENCE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_INTERNAL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_REQUEST"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_TSR"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_SORTING"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_NOACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ACTION_DETAILS_TR_NO_TSR_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CALC_NUM"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MAX_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MIN_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["AVERAGE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RANGE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["STDEV"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CPK"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["XBAR_UCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["XBAR_LCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RCHART_UCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RCHART_LCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["JUDGE_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_YMDHMS"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["LAST_UPDATE_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_YMDHMS"]);
			//newline
			$lOutputCSVData.= "\n";
		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process            createCSVValue
	// overview           treat value of CSV（escape value including "）
	// overview           
	// argument           
	// return value       
	// author             s-miyamoto
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function createCSVValue($pTargetValue)
	{
		//「"」->「""」「",」
		$pTargetValue = "\"".str_replace("\"", "\"\"", $pTargetValue)."\",";

		return $pTargetValue;

	}

	//**************************************************************************
	// process         ★getInspectionDataForShonin
	// overview        get master data for EXCEL approval and return it to calling as Array
	// argument        Nothing
	// return value    Array
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectionDataForShonin()
	{
		$lTblData = [];

		$lInspectionDateFromForSearch  = "";
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		
		$lTblData = DB::select('
				SELECT HEAD.INSPECTION_RESULT_NO
						,HEAD.INSPECTION_SHEET_NO
						,SHEE.INSPECTION_SHEET_NAME
						,HEAD.REV_NO
						,HEAD.PROCESS_ID
						,PROCESS.CODE_NAME AS PROCESS_NAME
						,SHEE.MODEL_NAME
						,HEAD.MACHINE_NO
						,MACH.MACHINE_NAME
						,HEAD.MOLD_NO
						,HEAD.MATERIAL_NAME
						,HEAD.MATERIAL_SIZE
						,HEAD.COLOR_ID
						,HEAD.INSERT_USER_ID
						,USER.USER_NAME
						,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
						,HEAD.CONDITION_CD
						,COND.CODE_NAME AS CONDITION_NAME
						,HEAD.INSPECTION_TIME_ID
						,TIME.INSPECTION_TIME_NAME
						,HEAD.LOT_NO
						,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
						,DTIL.INSPECTION_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,IPNO.THRESHOLD_NG_UNDER
						,IPNO.REFERENCE_VALUE
						,IPNO.THRESHOLD_NG_OVER
						,IPNO.INSPECTION_ITEM
						,DTIL.INSPECTION_RESULT_VALUE
						,CASE
							WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
							ELSE ""
						END INSPECTION_RESULT_TYPE
						,DATE_FORMAT(DTIL.INSERT_YMDHMS,"%T") AS INSERT_YMDHMS
						,DTIL.OFFSET_NG_REASON
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
							WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
							ELSE ""
						END OFFSET_NG_ACTION
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_INTERNAL
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_REQUEST
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_TSR
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
							ELSE ""
							END OFFSET_NG_ACTION_DTL_SORTING
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_NOACTION
						,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
						,DTIL.TR_NO
						,DTIL.TSR_NO
						,TEUR.TECHNICIAN_USER_NAME
				FROM TRESHEDT AS HEAD

			INNER JOIN TRESDETT AS DTIL
					ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
			INNER JOIN TISHEETM AS SHEE
					ON HEAD.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
				   AND HEAD.REV_NO               = SHEE.REV_NO
			INNER JOIN TINSPNOM AS IPNO
					ON SHEE.INSPECTION_SHEET_NO  = IPNO.INSPECTION_SHEET_NO
				   AND SHEE.REV_NO               = IPNO.REV_NO
				   AND DTIL.INSPECTION_POINT     = IPNO.INSPECTION_POINT

			INNER JOIN TMACHINM AS MACH
					ON HEAD.MACHINE_NO           = MACH.MACHINE_NO
			INNER JOIN TUSERMST AS USER
					ON HEAD.INSERT_USER_ID       = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
					ON HEAD.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
	   LEFT OUTER JOIN TTECUSRT AS TEUR
					ON DTIL.TECHNICIAN_USER_ID   = TEUR.TECHNICIAN_USER_ID

			INNER JOIN TCODEMST AS COND
					ON COND.CODE_CLASS           = "001"
				   AND COND.CODE_ORDER           = HEAD.CONDITION_CD
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS        = "002"
				   AND PROCESS.CODE_ORDER        = HEAD.PROCESS_ID

				  WHERE HEAD.INSPECTION_SHEET_NO = :CheckSheetNo
					AND HEAD.REV_NO              = :RevisionNo
					AND SHEE.PROCESS_ID          = :ProcessId
					AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
					AND HEAD.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
					AND HEAD.INSERT_USER_ID      = :UserID
					AND HEAD.INSPECTION_YMD      = :DateFrom
				ORDER BY DTIL.DISPLAY_ORDER
						,TIME.DISPLAY_ORDER
						,HEAD.CONDITION_CD
			',
				[
					"CheckSheetNo"   => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
					"RevisionNo"     => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"ProcessId"      => TRIM((String)Input::get('cmbProcessForSearch')),
					"MachineNo1"     => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"     => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"        => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"        => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom"       => $lInspectionDateFromForSearch,
				]
			);          
			       
		return $lTblData;
	}

	//**************************************************************************
	// process         ★getInspectionResultDataForNGResult
	// overview        get result data of NG result approval and return it asa array to calling
	// argument        Nothing
	// return value    Array
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectionResultDataForNGResult()
	{
		$lTblSearchResultData          = [];

		$lInspectionDateFromForSearch  = "";
		$lInspectionDateToForSearch    = "";
		$lProductionDateFromForSearch  = "";
		$lProductionDateToForSearch    = "";

		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");
		$lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), "1");
		$lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), "1");

			$lTblSearchResultData = DB::select('
				SELECT HEAD.INSPECTION_RESULT_NO
						,SHEE.CUSTOMER_ID
						,CUSTOM.CUSTOMER_NAME
						,SHEE.ITEM_NO
						,ITEM.ITEM_NAME
						,SHEE.MODEL_NAME
						,HEAD.INSPECTION_SHEET_NO
						,SHEE.INSPECTION_SHEET_NAME
						,HEAD.REV_NO
						,HEAD.PROCESS_ID
						,PROCESS.CODEORDER_NAME AS PROCESS_NAME
						,HEAD.MACHINE_NO
						,MACH.MACHINE_NAME
						,HEAD.MOLD_NO
						,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
						,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
						,HEAD.INSPECTION_TIME_ID
						,TIME.INSPECTION_TIME_NAME
						,HEAD.CONDITION_CD
						,COND.CODEORDER_NAME AS CONDITION_NAME
						,HEAD.LOT_NO
						,DTIL.INSPECTION_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,IPNO.INSPECTION_ITEM
						,IPNO.REFERENCE_VALUE
						,IPNO.THRESHOLD_NG_UNDER
						,IPNO.THRESHOLD_NG_OVER
						,DTIL.INSPECTION_RESULT_VALUE
						,DTIL.LAST_UPDATE_USER_ID
						,USER.USER_NAME
						,DATE_FORMAT(DTIL.UPDATE_YMDHMS,"%T") AS UPDATE_YMDHMS
						,CASE
							WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
							ELSE ""
						END INSPECTION_RESULT_TYPE
						,DTIL.OFFSET_NG_REASON
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
							WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
							ELSE ""
						END OFFSET_NG_ACTION
						,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
						,DTIL.TR_NO
						,TEUR.TECHNICIAN_USER_NAME
						,DTIL.TSR_NO
				FROM TRESHEDT AS HEAD        

			INNER JOIN TRESDETT AS DTIL
					ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
			INNER JOIN TISHEETM AS SHEE
					ON HEAD.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
				   AND HEAD.REV_NO               = SHEE.REV_NO
			INNER JOIN TINSPNOM AS IPNO
					ON SHEE.INSPECTION_SHEET_NO  = IPNO.INSPECTION_SHEET_NO
				   AND SHEE.REV_NO               = IPNO.REV_NO
				   AND DTIL.INSPECTION_POINT     = IPNO.INSPECTION_POINT
	   LEFT OUTER JOIN TITEMMST AS ITEM
					ON SHEE.ITEM_NO             = ITEM.ITEM_NO
				   AND ITEM.DELETE_FLG          = "0"

			INNER JOIN TMACHINM AS MACH
					ON HEAD.MACHINE_NO           = MACH.MACHINE_NO
			INNER JOIN TUSERMST AS USER
					ON DTIL.LAST_UPDATE_USER_ID  = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
					ON HEAD.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
	   LEFT OUTER JOIN TTECUSRT AS TEUR
					ON DTIL.TECHNICIAN_USER_ID   = TEUR.TECHNICIAN_USER_ID
	   LEFT OUTER JOIN TCUSTOMM AS CUSTOM
					ON SHEE.CUSTOMER_ID          = CUSTOM.CUSTOMER_ID

			INNER JOIN TCODEMST AS COND
					ON COND.CODE_CLASS           = "001"
				   AND COND.CODE_ORDER           = HEAD.CONDITION_CD
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS        = "002"
				   AND PROCESS.CODE_ORDER        = HEAD.PROCESS_ID

				WHERE HEAD.PROCESS_ID               = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)
					AND SHEE.CUSTOMER_ID            = IF(:CustomerID1 <> "", :CustomerID2, SHEE.CUSTOMER_ID)
					AND HEAD.INSPECTION_SHEET_NO    LIKE :CheckSheetNo
					AND HEAD.REV_NO                 = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
					AND HEAD.MATERIAL_NAME          = IF(:MaterialName1 <> "", :MaterialName2, HEAD.MATERIAL_NAME)
					AND HEAD.MATERIAL_SIZE          = IF(:MaterialSize1 <> "", :MaterialSize2, HEAD.MATERIAL_SIZE)
					AND HEAD.MACHINE_NO             = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
					AND HEAD.MOLD_NO                = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
					AND HEAD.LAST_UPDATE_USER_ID    = IF(:UserID1 <> "", :UserID2, HEAD.LAST_UPDATE_USER_ID)
					AND SHEE.ITEM_NO                LIKE :PartNo
					AND HEAD.PRODUCTION_YMD         >= :ProdDateFrom AND HEAD.PRODUCTION_YMD <= :ProdDateTo
					AND HEAD.INSPECTION_YMD         >= :InspDateFrom AND HEAD.INSPECTION_YMD <= :InspDateTo
					AND HEAD.INSPECTION_TIME_ID     = IF(:InspectionTime1 <> "", :InspectionTime2, HEAD.INSPECTION_TIME_ID)
					AND DTIL.INSPECTION_RESULT_TYPE IN ("1","2","3","4","5")
			   ORDER BY SHEE.CUSTOMER_ID
						,SHEE.ITEM_NO
						,SHEE.MODEL_NAME
						,HEAD.INSPECTION_SHEET_NO
						,HEAD.REV_NO
						,HEAD.MACHINE_NO
						,HEAD.MOLD_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,DTIL.DISPLAY_ORDER
						,HEAD.PRODUCTION_YMD
						,HEAD.INSPECTION_YMD
						,HEAD.INSPECTION_TIME_ID
						,TIME.DISPLAY_ORDER
						,HEAD.CONDITION_CD
			',
				[
					"ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerID1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerID2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"      => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MaterialName1"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialName2"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialSize1"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MaterialSize2"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MoldNo1"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"MoldNo2"           => TRIM((String)Input::get('txtMoldNoForSearch')),
					"UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"PartNo"            => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"ProdDateFrom"      => $lProductionDateFromForSearch,
					"ProdDateTo"        => $lProductionDateToForSearch,
					"InspDateFrom"      => $lInspectionDateFromForSearch,
					"InspDateTo"        => $lInspectionDateToForSearch,
					"InspectionTime1"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspectionTime2"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
			);          
			       
		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getProcessName
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getProcessName()
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
				SELECT  PROCESS.CODE_NAME
				FROM	TCODEMST AS PROCESS
				WHERE	PROCESS.CODE_CLASS = "002"
				AND 	PROCESS.CODE_ORDER = IF(:ProcessID1 <> "", :ProcessID2, PROCESS.CODE_ORDER)
			',
				[
					"ProcessID1"  =>  TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessID2"  =>  TRIM((String)Input::get('cmbProcessForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCustmerName
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCustmerName()
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
				SELECT  CUSTOM.CUSTOMER_NAME
				FROM	TCUSTOMM AS CUSTOM
				WHERE	CUSTOM.CUSTOMER_ID = IF(:CustomerID1 <> "", :CustomerID2, CUSTOM.CUSTOMER_ID)
			',
				[
					"CustomerID1"  =>  TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerID2"  =>  TRIM((String)Input::get('cmbCustomerForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getMachineName
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMachineName()
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
			    SELECT  MACHINE.MACHINE_NO
					   ,MACHINE.MACHINE_NAME
				   FROM TMACHINM AS MACHINE
				  WHERE MACHINE.MACHINE_NO = IF(:MachineNo1 <> "", :MachineNo2, MACHINE.MACHINE_NO)    /*無ければ条件にしない*/
			',
				[
					"MachineNo1"      => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"      => TRIM((String)Input::get('txtMachineNoForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCheckSheetName
	// overview        get header data of NG result approval and return it asa array to calling
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function getCheckSheetName()
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
					SELECT SHEET.INSPECTION_SHEET_NAME
						  ,SHEET.MODEL_NAME
					  FROM TISHEETM AS SHEET
					 WHERE SHEET.INSPECTION_SHEET_NO LIKE :CheckSheetNo
					   AND SHEET.REV_NO = IF(:RevisionNo1 <> "", :RevisionNo2, SHEET.REV_NO)
			',
				[
					"CheckSheetNo"  => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getInspectorName
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectorName()
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
			    SELECT USERM.USER_NAME
				  FROM TUSERMST AS USERM
				 WHERE USERM.USER_ID = IF(:UserID1 <> "", :UserID2, USERM.USER_ID)
			',
				[
					"UserID1"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process            ★setListForms
	// overview           store and return form input information of search result list screen
	// argument           Arary
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function setListForms($pViewData)
	{
		//■store Process for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProcessForEntry')))
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForEntry'))
			{
					Session::put('BA1010ProcessForEntry', Input::get('cmbProcessForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010ProcessForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForEntry'))
			{
				Session::put('BA1010ProcessForEntry', Input::get('cmbProcessForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ProcessForEntry"  => Session::get('BA1010ProcessForEntry')
		];


		//■store Customer for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CustomerForEntry')))
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForEntry'))
			{
				Session::put('BA1010CustomerForEntry', Input::get('cmbCustomerForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010CustomerForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForEntry'))
			{
				Session::put('BA1010CustomerForEntry', Input::get('cmbCustomerForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CustomerForEntry"  => Session::get('BA1010CustomerForEntry')
		];


		//■store Check Sheet No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CheckSheetNoForEntry')))
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForEntry'))
			{
				Session::put('BA1010CheckSheetNoForEntry', Input::get('cmbCheckSheetNoForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010CheckSheetNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForEntry'))
			{
				Session::put('BA1010CheckSheetNoForEntry', Input::get('cmbCheckSheetNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CheckSheetNoForEntry"  => Session::get('BA1010CheckSheetNoForEntry')
		];


		//■store Material Name for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialNameForEntry')))
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForEntry'))
			{
				Session::put('BA1010MaterialNameForEntry', Input::get('cmbMaterialNameForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010MaterialNameForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForEntry'))
			{
				Session::put('BA1010MaterialNameForEntry', Input::get('cmbMaterialNameForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialNameForEntry"  => Session::get('BA1010MaterialNameForEntry')
		];


		//■store Material Size for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialSizeForEntry')))
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForEntry'))
			{
				Session::put('BA1010MaterialSizeForEntry', Input::get('cmbMaterialSizeForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010MaterialSizeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForEntry'))
			{
				Session::put('BA1010MaterialSizeForEntry', Input::get('cmbMaterialSizeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialSizeForEntry"  => Session::get('BA1010MaterialSizeForEntry')
		];


		//■store M/C No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MachineNoForEntry')))
		{
			if (Input::has('txtMachineNoForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForEntry', Input::get('txtMachineNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MachineNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMachineNoForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForEntry', Input::get('txtMachineNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MachineNoForEntry"  => Session::get('BA1010MachineNoForEntry')
		];


		//■store Mold No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MoldNoForEntry')))
		{
			if (Input::has('txtMoldNoForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MoldNoForEntry', Input::get('txtMoldNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MoldNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMoldNoForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MoldNoForEntry', Input::get('txtMoldNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MoldNoForEntry"  => Session::get('BA1010MoldNoForEntry')
		];

		//■store Inspector Code for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectorCodeForEntry')))
		{
			if (Input::has('txtInspectorCodeForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForEntry', Input::get('txtInspectorCodeForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectorCodeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectorCodeForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForEntry', Input::get('txtInspectorCodeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectorCodeForEntry"  => Session::get('BA1010InspectorCodeForEntry')
		];


		//■store Inspection Date for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateForEntry')))
		{
			if (Input::has('txtInspectionDateForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateForEntry', Input::get('txtInspectionDateForEntry'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateForEntry', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateForEntry', Input::get('txtInspectionDateForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateForEntry"  => Session::get('BA1010InspectionDateForEntry')
		];

		//■store Condition for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ConditionForEntry')))
		{
			if (Input::has('cmbConditionForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ConditionForEntry', Input::get('cmbConditionForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010ConditionForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbConditionForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ConditionForEntry', Input::get('cmbConditionForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ConditionForEntry"  => Session::get('BA1010ConditionForEntry')
		];


		//■store Inspection Time for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionTimeForEntry')))
		{
			if (Input::has('cmbInspectionTimeForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForEntry', Input::get('cmbInspectionTimeForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectionTimeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbInspectionTimeForEntry'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForEntry', Input::get('cmbInspectionTimeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectionTimeForEntry"  => Session::get('BA1010InspectionTimeForEntry')
		];


		//===================================================
		//■store Process for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProcessForSearch')))
		{
			if (Input::has('cmbProcessForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProcessForSearch', Input::get('cmbProcessForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010ProcessForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbProcessForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProcessForSearch', Input::get('cmbProcessForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ProcessForSearch"  => Session::get('BA1010ProcessForSearch')
		];


		//■store Customer for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CustomerForSearch')))
		{
			if (Input::has('cmbCustomerForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010CustomerForSearch', Input::get('cmbCustomerForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010CustomerForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbCustomerForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010CustomerForSearch', Input::get('cmbCustomerForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CustomerForSearch"  => Session::get('BA1010CustomerForSearch')
		];


		//■store Check Sheet No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CheckSheetNoForSearch')))
		{
			if (Input::has('cmbCheckSheetNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010CheckSheetNoForSearch', Input::get('cmbCheckSheetNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010CheckSheetNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbCheckSheetNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010CheckSheetNoForSearch', Input::get('cmbCheckSheetNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CheckSheetNoForSearch"  => Session::get('BA1010CheckSheetNoForSearch')
		];


		//■store Revision No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010RevisionNoForSearch')))
		{
			if (Input::has('cmbRevisionNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010RevisionNoForSearch', Input::get('cmbRevisionNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010RevisionNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbRevisionNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010RevisionNoForSearch', Input::get('cmbRevisionNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"RevisionNoForSearch"  => Session::get('BA1010RevisionNoForSearch')
		];


		//■store Material Name for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialNameForSearch')))
		{
			if (Input::has('cmbMaterialNameForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialNameForSearch', Input::get('cmbMaterialNameForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MaterialNameForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbMaterialNameForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialNameForSearch', Input::get('cmbMaterialNameForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialNameForSearch"  => Session::get('BA1010MaterialNameForSearch')
		];


		//■store Material Size for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialSizeForSearch')))
		{
			if (Input::has('cmbMaterialSizeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialSizeForSearch', Input::get('cmbMaterialSizeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MaterialSizeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbMaterialSizeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialSizeForSearch', Input::get('cmbMaterialSizeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialSizeForSearch"  => Session::get('BA1010MaterialSizeForSearch')
		];


		//■store M/C No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MachineNoForSearch')))
		{
			if (Input::has('txtMachineNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForSearch', Input::get('txtMachineNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MachineNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMachineNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForSearch', Input::get('txtMachineNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MachineNoForSearch"  => Session::get('BA1010MachineNoForSearch')
		];


		//■store Mold No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MoldNoForSearch')))
		{
			if (Input::has('txtMoldNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MoldNoForSearch', Input::get('txtMoldNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MoldNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMoldNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010MoldNoForSearch', Input::get('txtMoldNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MoldNoForSearch"  => Session::get('BA1010MoldNoForSearch')
		];


		//■store Inspector Code for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectorCodeForSearch')))
		{
			if (Input::has('txtInspectorCodeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForSearch', Input::get('txtInspectorCodeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectorCodeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectorCodeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForSearch', Input::get('txtInspectorCodeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectorCodeForSearch"  => Session::get('BA1010InspectorCodeForSearch')
		];


		//■store Production Date（From） for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProductionDateFromForSearch')))
		{
			if (Input::has('txtProductionDateFromForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010ProductionDateFromForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtProductionDateFromForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProductionDateFromForSearch', Input::get('txtProductionDateFromForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"ProductionDateFromForSearch"  => Session::get('BA1010ProductionDateFromForSearch')
		];


		//■store Production Date（To） for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProductionDateToForSearch')))
		{
			if (Input::has('txtProductionDateToForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProductionDateToForSearch', Input::get('txtProductionDateToForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010ProductionDateToForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtProductionDateToForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010ProductionDateToForSearch', Input::get('txtProductionDateToForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"ProductionDateToForSearch"  => Session::get('BA1010ProductionDateToForSearch')
		];


		//■store Inspection Date（From）for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateFromForSearch')))
		{
			if (Input::has('txtInspectionDateFromForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateFromForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateFromForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateFromForSearch"  => Session::get('BA1010InspectionDateFromForSearch')
		];


		//■store Inspection Date（To）for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateToForSearch')))
		{
			if (Input::has('txtInspectionDateToForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateToForSearch', Input::get('txtInspectionDateToForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateToForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateToForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateToForSearch', Input::get('txtInspectionDateToForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateToForSearch"  => Session::get('BA1010InspectionDateToForSearch')
		];


		//■store Part No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010PartNoForSearch')))
		{
			if (Input::has('txtPartNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010PartNoForSearch', Input::get('txtPartNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010PartNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtPartNoForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010PartNoForSearch', Input::get('txtPartNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"PartNoForSearch"  => Session::get('BA1010PartNoForSearch')
		];


		//■store InspectionTime for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionTimeForSearch')))
		{
			if (Input::has('cmbInspectionTimeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForSearch', Input::get('cmbInspectionTimeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectionTimeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbInspectionTimeForSearch'))
			{
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForSearch', Input::get('cmbInspectionTimeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectionTimeForSearch"  => Session::get('BA1010InspectionTimeForSearch')
		];

		return $pViewData;
	}


	//**************************************************************************
	// process    checkFile
	// overview   
	// argument   
	
	//**************************************************************************
	private function checkFile($pViewData)
	{
		//File
		$lValidator = Validator::make(
			array('filUploadFile' => Input::file('filUploadFile')),
			array('filUploadFile' => array('required'))
		);

		if ($lValidator->fails()) 
		{
				$pViewData["errors"] = new MessageBag([
					"error" => "E029 : Select Upload File."
				]);
				
				return $pViewData;
		}
	
		return $pViewData;
	}


	//**************************************************************************
	// process            ★getCheckSheetData
	// overview           get information of inspection check sheet from master and return to calling
	// argument           value of Check Sheet No.（value of input in screen）
	// return value       Arary（two dimensions DataTable）
	// author             s-miyamoto
	// date               2014.05.27
	// record of updates  2014.05.27 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    
	//**************************************************************************
	private function getCheckSheetData($pCheckSheetNo)
	{
		$lTblCheckSheetInfo = [];       //DataTable

			$lTblCheckSheetInfo = DB::select('
			          SELECT SHEE.INSPECTION_SHEET_NO
			                ,MAX(SHEE.REV_NO) AS MAX_REV_NO
			                ,SHEE.PROCESS_ID
			                ,PROCESS.CODE_NAME AS PROCESS_NAME
			                ,SHEE.INSPECTION_SHEET_NAME
			                ,SHEE.CUSTOMER_ID
			                ,CUST.CUSTOMER_NAME
			                ,SHEE.MODEL_NAME
			                ,SHEE.ITEM_NO
			                ,ITEM.ITEM_NAME
			                ,SHEE.DRAWING_NO
			                ,SHEE.ISO_KANRI_NO
			                ,ITEM.PRODUCT_WEIGHT
			                ,ITEM.MATERIAL_NAME
			                ,ITEM.MATERIAL_SIZE
			                ,ITEM.HEAT_TREATMENT_TYPE
			                ,ITEM.PLATING_KIND
			                ,ITEM.COLOR_ID
			                ,COLOR.CODE_NAME AS COLOR_NAME
			            FROM TISHEETM AS SHEE
			      INNER JOIN TCODEMST AS PROCESS
			              ON PROCESS.CODE_CLASS       = "002"
			             AND SHEE.PROCESS_ID          = PROCESS.CODE_ORDER
			 LEFT OUTER JOIN TITEMMST AS ITEM
			              ON SHEE.ITEM_NO             = ITEM.ITEM_NO
			             AND ITEM.DELETE_FLG          = "0"
			 LEFT OUTER JOIN TCODEMST AS COLOR
			              ON COLOR.CODE_CLASS         = "006"
			             AND ITEM.COLOR_ID            = COLOR.CODE_ORDER
			 LEFT OUTER JOIN TCUSTOMM AS CUST
			              ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			             AND CUST.DELETE_FLG          = "0"
			           WHERE SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
			             AND SHEE.DELETE_FLG          = "0"
			',
				[
					"CheckSheetNo" => TRIM((String)$pCheckSheetNo),
				]
			);

		return $lTblCheckSheetInfo;
	}

	//**************************************************************************
	// Processing      ★getMachineData
	// Overview        get information of machine from master and return to calling
	// parameter       value of M/C No.（value of input in screen）
	// returned value  
	// programer       m-motooka
	// date            2014.08.26 v1.00 FIX
	// history         
	//                 
	//**************************************************************************
	private function getMachineData($pMachineNo)
	{
		$lTblMachineInfo = [];       //DataTable

			$lTblMachineInfo = DB::select('
				SELECT MACHINE_NAME
				  FROM TMACHINM
				 WHERE MACHINE_NO = :MachineNo
				   AND DELETE_FLG = "0"
			',
				[
					"MachineNo" => TRIM((String)$pMachineNo),
				]
			);

		return $lTblMachineInfo;
	}

	//**************************************************************************
	// Processing      ★getInspectorData
	// Overview        get information of inspector from Master and return to calling
	// parameter       value of Inspector Code.（value of input in screen）
	// returned value  
	// programer       m-motooka
	// date            2014.08.26 v1.00 FIX
	// history         
	//                 
	//**************************************************************************
	private function getInspectorData($pInspectorCode)
	{
		$lTblInspectorInfo = [];       //DataTable

			$lTblInspectorInfo = DB::select('
			    SELECT TUSE.USER_NAME
			          ,TUSE.TEAM_ID
				      ,TEAM.CODE_NAME AS TEAM_NAME
				  FROM TUSERMST AS TUSE
	   LEFT OUTER JOIN TCODEMST AS TEAM
				    ON TEAM.CODE_CLASS = "004"
				   AND TUSE.TEAM_ID    = TEAM.CODE_ORDER
				 WHERE USER_ID         = :InspectorCode
				   AND DELETE_FLG      = "0"
			',
				[
					"InspectorCode" => TRIM((String)$pInspectorCode),
				]
			);

		return $lTblInspectorInfo;
	}

	//**************************************************************************
	// process            ★getInspectionResultData
	// overview           search header data of inspection result and return array to calling 
	// argument           Inspection Result No.（inside key per button）、DataRevision
	// return value       Array
	// author             s-miyamoto
	// date               2014.05.28
	// record of updates  2014.05.28 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    
	//**************************************************************************
	private function getInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lTblSearchResultData = [];      //data table inspection result

		$lTblSearchResultData = DB::select('
			    SELECT REHE.INSPECTION_SHEET_NO
			          ,SHEE.INSPECTION_SHEET_NAME
			          ,REHE.REV_NO
			          ,REHE.PROCESS_ID 
			    	  ,PROCESS.CODE_NAME AS PROCESS_NAME
			          ,REHE.MACHINE_NO
			          ,MACH.MACHINE_NAME
			          ,REHE.MOLD_NO
			          ,REHE.MATERIAL_NAME
			          ,REHE.MATERIAL_SIZE
			          ,REHE.HEAT_TREATMENT_TYPE
			          ,REHE.PLATING_KIND
			          ,REHE.COLOR_ID
			          ,COLOR.CODE_NAME AS COLOR_NAME
			          ,REHE.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,USER.TEAM_ID
			          ,TEAM.CODE_NAME AS TEAM_NAME
			          ,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,REHE.CONDITION_CD
				      ,COND.CODE_NAME AS CONDITION_NAME
			          ,REHE.INSPECTION_TIME_ID
			          ,TIME.INSPECTION_TIME_NAME
			          ,REHE.MATERIAL_LOT_NO
			          ,REHE.CERTIFICATE_NO
			          ,REHE.PO_NO
			          ,REHE.INVOICE_NO
			          ,REHE.QUANTITY_COIL
			          ,REHE.QUANTITY_WEIGHT
			          ,REHE.LOT_NO
			          ,DATE_FORMAT(REHE.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
			          ,REHE.DATA_REV
			          ,SHEE.CUSTOMER_ID
			          ,CUST.CUSTOMER_NAME
			          ,SHEE.MODEL_NAME
			          ,SHEE.ITEM_NO
			          ,ITEM.ITEM_NAME
			          ,SHEE.DRAWING_NO
			          ,SHEE.ISO_KANRI_NO
			          ,SHEE.PRODUCT_WEIGHT
			      FROM TRESHEDT AS REHE

			INNER JOIN TISHEETM AS SHEE
			        ON REHE.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND REHE.REV_NO              = SHEE.REV_NO
			       AND REHE.PROCESS_ID          = SHEE.PROCESS_ID
			INNER JOIN TCUSTOMM AS CUST
			        ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			INNER JOIN TMACHINM AS MACH
			        ON REHE.MACHINE_NO          = MACH.MACHINE_NO
			INNER JOIN TINSPTIM AS TIME
			        ON REHE.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
			INNER JOIN TUSERMST AS USER
			        ON REHE.INSERT_USER_ID      = USER.USER_ID
	   LEFT OUTER JOIN TITEMMST AS ITEM
					ON SHEE.ITEM_NO             = ITEM.ITEM_NO
				   AND ITEM.DELETE_FLG          = "0"

			INNER JOIN TCODEMST AS COND
			        ON COND.CODE_CLASS    = "001"
			       AND REHE.CONDITION_CD  = COND.CODE_ORDER
			INNER JOIN TCODEMST AS PROCESS
				    ON PROCESS.CODE_CLASS = "002"
				   AND REHE.PROCESS_ID    = PROCESS.CODE_ORDER
	   LEFT OUTER JOIN TCODEMST AS COLOR
			        ON COLOR.CODE_CLASS   = "006"
			       AND SHEE.COLOR_ID      = COLOR.CODE_ORDER
	   LEFT OUTER JOIN TCODEMST AS TEAM
				    ON TEAM.CODE_CLASS    = "004"
				   AND USER.TEAM_ID       = TEAM.CODE_ORDER

			     WHERE REHE.INSPECTION_RESULT_NO = :InspectionResultNo
			       AND REHE.DATA_REV             = :DataRev
			       AND SHEE.DELETE_FLG           = "0"
			       AND CUST.DELETE_FLG           = "0"
			       AND MACH.DELETE_FLG           = "0"
			       AND TIME.DELETE_FLG           = "0"
			       AND USER.DELETE_FLG           = "0"
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"DataRev"            => TRIM((String)$pDataRev),
				]
		);

		return $lTblSearchResultData;
	}


	//**************************************************************************
	// process         ★getHeaderTableDataInspectionSheet
	// overview        get transaction Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getHeaderTableDataInspectionSheet($pInspectionResultNo, $pDataRev)
	{
		$lTblHeaderTableDataInspectionSheet = [];

			$lTblHeaderTableDataInspectionSheet = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
						,HEAD.INSPECTION_SHEET_NO
						,HEAD.REV_NO
						,HEAD.MOLD_NO
						,HEAD.INSPECTION_YMD
						,HEAD.LOT_NO
						,HEAD.PRODUCTION_YMD
					FROM TRESHEDT AS HEAD 

				 WHERE HEAD.INSPECTION_RESULT_NO = :InspectionResultNo
				   AND HEAD.DATA_REV             = :DataRev
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
					"DataRev"                => $pDataRev,
				]
			);

		return $lTblHeaderTableDataInspectionSheet;
	}

	//**************************************************************************
	// process         ★getSheetMasterTaleDataInspectionSheet
	// overview        get print Inspection Sheet Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getSheetMasterTaleDataInspectionSheet($pInspectionSheetNo, $pRevNo)
	{
		$lTblSheetMasterTaleDataInspectionSheet = [];

			$lTblSheetMasterTaleDataInspectionSheet = DB::select('
					SELECT SHEE.INSPECTION_SHEET_NO
							,SHEE.REV_NO
							,SHEE.PROCESS_ID
							,PROCESS.CODE_NAME AS PROCESS_NAME
							,SHEE.CUSTOMER_ID
							,CUST.CUSTOMER_NAME
							,SHEE.MODEL_NAME
							,SHEE.ITEM_NO
							,ITEM.ITEM_NAME
							,SHEE.DRAWING_NO
							,ITEM.MATERIAL_NAME
							,ITEM.MATERIAL_SIZE
							,ITEM.COLOR_ID
							,COLOR.CODE_NAME AS COLOR_NAME
						FROM TISHEETM AS SHEE
				INNER JOIN TCUSTOMM AS CUST
					    ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			LEFT OUTER JOIN TITEMMST AS ITEM
					    ON ITEM.ITEM_NO     = SHET.ITEM_NO
					   AND ITEM.DELETE_FLG  = "0"
				INNER JOIN TCODEMST AS PROCESS
					    ON PROCESS.CODE_CLASS       = "002"
					   AND SHEE.PROCESS_ID          = PROCESS.CODE_ORDER
			LEFT OUTER JOIN TCODEMST AS COLOR
				        ON COLOR.CODE_CLASS         = "006"
				       AND SHEE.COLOR_ID            = COLOR.CODE_ORDER
				     WHERE SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
					   AND SHEE.REV_NO              = :RevisionNo
					   AND SHEE.DELETE_FLG          = "0"
				',
				[
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"         => TRIM((String)$pRevNo),
				]
			);

		return $lTblSheetMasterTaleDataInspectionSheet;
	}

	//**************************************************************************
	// process         ★getOutputPictureInspectionSheet
	// overview        get print Inspection Sheet Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getOutputPictureInspectionSheet($pInspectionSheetNo, $pRevNo)
	{
		$lTblOutputPictureInspectionSheet = [];

			$lTblOutputPictureInspectionSheet = DB::select('
					SELECT IPNO.PICTURE_URL
					  FROM TINSPNOM AS IPNO
				     WHERE IPNO.INSPECTION_SHEET_NO = :CheckSheetNo
					   AND IPNO.REV_NO              = :RevisionNo
					   AND IPNO.DELETE_FLG          = "0"
				  GROUP BY IPNO.PICTURE_URL
				  ORDER BY IPNO.DISPLAY_ORDER
				          ,IPNO.INSPECTION_POINT
				',
				[
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"         => TRIM((String)$pRevNo),
				]
			);

		return $lTblOutputPictureInspectionSheet;
	}

	//**************************************************************************
	// process         ★getOutputDetailRecordCountInspectionSheet
	// overview        get transaction Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getOutputDetailRecordCountInspectionSheet($pInspectionResultNo, $pDataRev)
	{
		$lTblOutputDetailRecordCountInspectionSheet = [];

			$lTblOutputDetailRecordCountInspectionSheet = DB::select('
					SELECT DETL.INSPECTION_RESULT_NO
							,DETL.INSPECTION_NO
							,DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
							,DETL.DISPLAY_ORDER
							,ISNO.INSPECTION_SHEET_NO
							,ISNO.REV_NO
							,ISNO.INSPECTION_ITEM
							,ISNO.TOOL_CD
							,ISNO.INSPECTION_TOOL_CLASS
							,ISNO.INSPECTION_RESULT_INPUT_TYPE
							,ISNO.THRESHOLD_NG_UNDER
							,ISNO.THRESHOLD_OFFSET_UNDER
							,ISNO.REFERENCE_VALUE
							,ISNO.THRESHOLD_OFFSET_OVER
							,ISNO.THRESHOLD_NG_OVER
							,ISNO.SPEC_WRITE_FORMAT
							,ISNO.GRP01_QTY
							,ISNO.GRP02_ID
							,ISNO.SAMPLE_QTY
						FROM TRESDETT AS DETL
				  INNER JOIN TRESHEDT AS HEAD
						  ON HEAD.INSPECTION_RESULT_NO   = DETL.INSPECTION_RESULT_NO
						 AND HEAD.DATA_REV               = :DataRev
				  INNER JOIN TINSPNOM AS ISNO
						  ON ISNO.INSPECTION_SHEET_NO    = HEAD.INSPECTION_SHEET_NO
						 AND ISNO.REV_NO                 = HEAD.REV_NO
						 AND ISNO.DELETE_FLG             = "0"
					   WHERE DETL.INSPECTION_RESULT_NO   = :InspectionResultNo
						 AND DETL.INSPECTION_POINT       = ISNO.INSPECTION_POINT
					GROUP BY DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
					ORDER BY DETL.DISPLAY_ORDER
							,DETL.INSPECTION_NO
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
					"DataRev"                => $pDataRev,
				]
			);

		return $lTblOutputDetailRecordCountInspectionSheet;
	}

	//**************************************************************************
	// process         ★getResultDetailInspectionSheet
	// overview        get transaction Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getResultDetailInspectionSheet($pInspectionResultNo)
	{
		$lTblResultDetailInspectionSheet = [];

			$lTblResultDetailInspectionSheet = DB::select('
					SELECT DETL.INSPECTION_RESULT_NO
							,DETL.INSPECTION_NO
							,DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
							,DETL.SAMPLE_NO
							,DETL.INSPECTION_RESULT_VALUE
							,DETL.INSPECTION_RESULT_TYPE
							,DETL.DISPLAY_ORDER
						FROM TRESDETT AS DETL
					   WHERE DETL.INSPECTION_RESULT_NO   = :InspectionResultNo
					ORDER BY DETL.DISPLAY_ORDER
							,DETL.INSPECTION_NO
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
				]
			);

		return $lTblResultDetailInspectionSheet;
	}





	//**************************************************************************
	// process            initializeSessionData
	// overview           clear session data（when it return from entry screen and delete）
	// argument           Nothing
	// return value       Nothing
	// author             s-miyamoto
	// date               2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//**************************************************************************
	private function initializeSessionData()
	{
		//clear search result data
		Session::forget('BA1010InspectionResultData');
		Session::forget('BA1010NotExistNoData');

		//登録条件は保持しておいた方が便利なので残す事にした（コメント化した）
		//→コメント解除
		//やはり残すことにした（再コメント化）
		//leave to store entry requirment（as comment）
//		Session::forget('BA1010ProcessIdForEntry');
//		Session::forget('BA1010CustomerIdForEntry');
//		Session::forget('BA1010CheckSheetNoForEntry');
//		Session::forget('BA1010MaterialNameForEntry');
//		Session::forget('BA1010MaterialSizeForEntry');
//		Session::forget('BA1010MachineNoForEntry');
//		Session::forget('BA1010MoldNoForEntry');
//		Session::forget('BA1010InspectorCodeForEntry');
//		Session::forget('BA1010InspectionDateForEntry');
//		Session::forget('BA1010ConditionIdForEntry');
//		Session::forget('BA1010InspectionTimeIdForEntry');

		//Entry　付随情報
		//clear what set when Entry button
//		Session::forget('BA1010ProcessName');
//		Session::forget('BA1010CustomerName');
//		Session::forget('BA1010CheckSheetName');
//		Session::forget('BA1010MachineName');
//		Session::forget('BA1010ConditionName');
//		Session::forget('BA1010InspectionTimeName');
//		Session::forget('BA1010InspectorName');
//		Session::forget('BA1010TeamId');
//		Session::forget('BA1010TeamName');

//		Session::forget('BA1010RevisionNo');
//		Session::forget('BA1010ModelName');
//		Session::forget('BA1010ItemNo');
//		Session::forget('BA1010ItemName');
//		Session::forget('BA1010DrawingNo');
//		Session::forget('BA1010ISOKanriNo');
//		Session::forget('BA1010ProductWeight');
//		Session::forget('BA1010HeatTreatmentType');
//		Session::forget('BA1010PlatingKind');
//		Session::forget('BA1010ColorId');
//		Session::forget('BA1010ColorName');


		//検索条件は保持しておいた方が便利なので残すことにした（コメント化）
		//→コメント解除
		//leave to store search requirment（as comment）
//		Session::forget('BA1010ProcessIdForSearch');
//		Session::forget('BA1010CustomerIdForSearch');
//		Session::forget('BA1010CheckSheetNoForSearch');
//		Session::forget('BA1010RevisionNoForSearch');
//		Session::forget('BA1010MaterialNameForSearch');
//		Session::forget('BA1010MaterialSizeForSearch');
//		Session::forget('BA1010MachineNoForSearch');
//		Session::forget('BA1010MoldNoForSearch');
//		Session::forget('BA1010InspectorCodeForSearch');
//		Session::forget('BA1010ProductionDateFromForSearch');
//		Session::forget('BA1010ProductionDateToForSearch');
//		Session::forget('BA1010InspectionDateFromForSearch');
//		Session::forget('BA1010InspectionDateToForSearch');
//		Session::forget('BA1010PartNoForSearch');
//		Session::forget('BA1010InspectionTimeForSearch');

		//clear what set when Modify button
		Session::forget('BA1010InspectionResultNo');
		Session::forget('BA1010CheckSheetNoForModify');
		Session::forget('BA1010CheckSheetNameForModify');
		Session::forget('BA1010RevisionNoForModify');
		Session::forget('BA1010ProcessIdForModify');
		Session::forget('BA1010ProcessNameForModify');
		Session::forget('BA1010MachineNoForModify');
		Session::forget('BA1010MachineNameForModify');
		Session::forget('BA1010MoldNoForModify');
		Session::forget('BA1010MachineNameForModify');
		Session::forget('BA1010MaterialNameForModify');
		Session::forget('BA1010MaterialSizeForModify');
		Session::forget('BA1010HeatTreatmentTypeForModify');
		Session::forget('BA1010PlatingKindForModify');
		Session::forget('BA1010ColorIdForModify');
		Session::forget('BA1010ColorNameForModify');
		Session::forget('BA1010InspectorCodeForModify');
		Session::forget('BA1010InspectorNameForModify');
		Session::forget('BA1010TeamIdForModify');
		Session::forget('BA1010TeamNameForModify');
		Session::forget('BA1010InspectionDateForModify');
		Session::forget('BA1010ConditionIdForModify');
		Session::forget('BA1010ConditionNameForModify');
		Session::forget('BA1010InspectionTimeIdForModify');
		Session::forget('BA1010InspectionTimeNameForModify');
		Session::forget('BA1010MaterialLotNoForModify');
		Session::forget('BA1010CertificateNoForModify');
		Session::forget('BA1010PoNoForModify');
		Session::forget('BA1010InvoiceNoForModify');
		Session::forget('BA1010QuantityCoilForModify');
		Session::forget('BA1010QuantityWeightForModify');
		Session::forget('BA1010LotNoForModify');
		Session::forget('BA1010ProductionDateForModify');
		Session::forget('BA1010ModifyDataRev');
		Session::forget('BA1010CustomerCodeForModify');
		Session::forget('BA1010CustomerNameForModify');
		Session::forget('BA1010ModelNameForModify');
		Session::forget('BA1010ItemNoForModify');
		Session::forget('BA1010ItemNameForModify');
		Session::forget('BA1010DrawingNoForModify');
		Session::forget('BA1010ISOKanriNoForModify');
		Session::forget('BA1010ProductWeightForModify');

		return null;
	}

	//**************************************************************************
	// process            ★isErrorForEntry
	// overview           errorcheck for entry
	// argument           Array to return to screen
	// return value       Array to return to screen
	// author             s-miyamoto
	// date               2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForEntry($pViewData)
	{
		//must check Process data
		$lValidator = Validator::make(
			array('cmbProcessForEntry' => Input::get('cmbProcessForEntry')),
			array('cmbProcessForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E044 : Select Process."
				]);

				return $pViewData;
		}

		//check Process data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbProcessForEntry'));

		$lValidator = Validator::make(
			array('cmbProcessForEntry' => $lSlashRemovalValue),
			array('cmbProcessForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E045 : Process is invalid."
				]);

				return $pViewData;
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCustomerForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E049 : Select Customer."
				]);

				return $pViewData;
		}

		//check customer data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCustomerForEntry'));

		$lValidator = Validator::make(
			array('cmbCustomerForEntry' => $lSlashRemovalValue),
			array('cmbCustomerForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E050 : Customer is invalid."
				]);

				return $pViewData;
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCheckSheetNoForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E006 : Enter Check Sheet No. ."
				]);

				return $pViewData;
		}

		//check check sheet No. type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForEntry'));

		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E007 : Check Sheet No. is invalid."
				]);

				return $pViewData;
		}

		//must check Material Name
		$lValidator = Validator::make(
			array('cmbMaterialNameForEntry' => Input::get('cmbMaterialNameForEntry')),
			array('cmbMaterialNameForEntry' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E046 : Enter Material Name."
			]);

			return $pViewData;
		}

		//check Material Name Type
		//Material Name : POM GH-25(60%) + M90-44(40%)
		//%  →　cannot input OK
		//() →　cannot input NG
		//16-Jan-2018 Hoshina comment
		//$lValidator = Validator::make(
		//	array('cmbMaterialNameForEntry' => Input::get('cmbMaterialNameForEntry')),
		//	array('cmbMaterialNameForEntry' => array('alpha_dash'))
		//);
		//error
		//if($lValidator->fails()) {
			//error message
		//	$pViewData["errors"] = new MessageBag([
		//		"error" => "E047 : Material Name is invalid."
		//	]);

		//	return $pViewData;
		//}

		//must check Material Size
		$lValidator = Validator::make(
			array('cmbMaterialSizeForEntry' => Input::get('cmbMaterialSizeForEntry')),
			array('cmbMaterialSizeForEntry' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E048 : Enter Material Size."
			]);

			return $pViewData;
		}

		//check Material Size
		// $lValidator = Validator::make(
		// 	array('cmbMaterialSizeForEntry' => Input::get('cmbMaterialSizeForEntry')),
		// 	array('cmbMaterialSizeForEntry' => array('alpha_dash'))
		// );
		// //error
		// if($lValidator->fails()) {
		// 	//error message
		// 	$pViewData["errors"] = new MessageBag([
		// 		"error" => "E010 : Material Size is invalid."
		// 	]);

		// 	return $pViewData;
		// }

		//must check M/C No.
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E009 : Enter M/C No. ."
				]);

				return $pViewData;
		}

		//check M/C No. type
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E010 : M/C No. is invalid."
				]);

				return $pViewData;
		}

		//must check Mold No.
		$lValidator = Validator::make(
			array('txtMoldNoForEntry' => Input::get('txtMoldNoForEntry')),
			array('txtMoldNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E065 : Enter Mold No. ."
				]);

				return $pViewData;
		}

		//check Mold No. type
		$lValidator = Validator::make(
			array('txtMoldNoForEntry' => Input::get('txtMoldNoForEntry')),
			array('txtMoldNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E066 : Mold No. is invalid."
				]);

				return $pViewData;
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E012 : Enter Inspector Code."
				]);

				return $pViewData;
		}

		//check inspector code type
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E013 : Inspector Code is invalid."
				]);

				return $pViewData;
		}

		//must check inspection date
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		//check inspection date type
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E016 : Inspection Date is invalid."
				]);

				return $pViewData;
		}

		//must check inspection nomal/abnomal
		$lValidator = Validator::make(
			array('cmbConditionForEntry' => Input::get('cmbConditionForEntry')),
			array('cmbConditionForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails()) {
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E037 : Select Condition."
				]);

				return $pViewData;
		}

		//must check inspection time
		$lValidator = Validator::make(
			array('cmbInspectionTimeForEntry' => Input::get('cmbInspectionTimeForEntry')),
			array('cmbInspectionTimeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E017 : Select Inspection Time."
				]);

				return $pViewData;
		}

		return $pViewData;
	}

	//**************************************************************************
	// process             ★isErrorForSearch
	// overview            check type in search
	// argument            Array to return to screen
	// return value        Array to return to screen
	// author              s-miyamoto
	// date                2014.07.15
	// record of updates   2014.07.15 v0.01 first making
	//                     2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForSearch($pViewData)
	{
		//check Process type
		$lValidator = Validator::make(
			array('cmbProcessForSearch'	=> Input::get('cmbProcessForSearch')),
			array('cmbProcessForSearch'	=> array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E045 : Process is invalid."
			]);
			return $pViewData;
		}

		//check customer type
		$lValidator = Validator::make(
			array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
			array('cmbCustomerForSearch' => array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E050 : Customer is invalid."
			]);
			return $pViewData;
		}

		//check check sheet No. type
		//Validation check after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForSearch'));

		$lValidator = Validator::make(
			array('cmbCheckSheetNoForSearch' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E007 : Check Sheet No. is invalid."
			]);

			return $pViewData;
		}

		//check Revision No. type
		$lValidator = Validator::make(
			array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
			array('cmbRevisionNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E018 : Revision No. is invalid."
			]);

			return $pViewData;
		}

		//check Material Name type
		$lValidator = Validator::make(
			array('cmbMaterialNameForSearch' => Input::get('cmbMaterialNameForSearch')),
			array('cmbMaterialNameForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E047 : Material Name is invalid."
			]);

			return $pViewData;
		}

		//check Material Size type
		//$lValidator = Validator::make(
		//	array('cmbMaterialSizeForSearch' => Input::get('cmbMaterialSizeForSearch')),
		//	array('cmbMaterialSizeForSearch' => array('alpha_dash'))
		//);
		//error
		//if ($lValidator->fails())
		//{
		//	//error message
		//	$pViewData["errors"] = new MessageBag([
		//		"error" => "E★ : ★★."
		//	]);
		//
		//	return $pViewData;
		//}

		//check M/C No. type
		$lValidator = Validator::make(
			array('txtMachineNoForSearch' => Input::get('txtMachineNoForSearch')),
			array('txtMachineNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E010 : M/C No. is invalid."
			]);

			return $pViewData;
		}

		//check Mold No. type
		$lValidator = Validator::make(
			array('txtMoldNoForSearch' => Input::get('txtMoldNoForSearch')),
			array('txtMoldNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E066 : Mold No. is invalid."
			]);

			return $pViewData;
		}

		//check inspector code type
		$lValidator = Validator::make(
			array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
			array('txtInspectorCodeForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E013 : Inspector Code is invalid."
			]);

			return $pViewData;
		}

		//check production date type(From)
		$lValidator = Validator::make(
			array('txtProductionDateFromForSearch' => Input::get('txtProductionDateFromForSearch')),
			array('txtProductionDateFromForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E063 : Production Date is invalid."
			]);

			return $pViewData;
		}

		//check production date type(To)
		$lValidator = Validator::make(
			array('txtProductionDateToForSearch' => Input::get('txtProductionDateToForSearch')),
			array('txtProductionDateToForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E063 : Production Date is invalid."
			]);

			return $pViewData;
		}

		//check production from/to date(must check From/To)
		if ((Input::get('txtProductionDateFromForSearch') != "") and (Input::get('txtProductionDateToForSearch') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtProductionDateFromForSearch'));
			$lDateTo   = date_create(Input::get('txtProductionDateToForSearch'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E064 : The relation of Production Date(From/To) is incorrect."
				]);

				return $pViewData;
			}
		}

		//check inspection date type(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);

			return $pViewData;
		}

		//check inspection date type(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);

			return $pViewData;
		}

		//check inspection from/to date(must check From/To)
		if ((Input::get('txtInspectionDateFromForSearch') != "") and (Input::get('txtInspectionDateToForSearch') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtInspectionDateFromForSearch'));
			$lDateTo   = date_create(Input::get('txtInspectionDateToForSearch'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E019 : The relation of Inspection Date(From/To) is incorrect."
				]);

				return $pViewData;
			}
		}

		//check Part No type
		$lValidator = Validator::make(
			array('txtPartNoForSearch' => Input::get('txtPartNoForSearch')),
			array('txtPartNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E020 : Part No. is invalid."
			]);

			return $pViewData;
		}


		//check Inspection time type
		$lValidator = Validator::make(
			array('cmbInspectionTimeForSearch' => Input::get('cmbInspectionTimeForSearch')),
			array('cmbInspectionTimeForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E051 : Inspection Time is invalid."
			]);

			return $pViewData;
		}


		return $pViewData;
	}

	//**************************************************************************
	// process         ★isErrorForSearchEnterCheck
	// overview        Enter Check
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function isErrorForSearchEnterCheck($pViewData)
	{
		//must check production date(From)
		$lValidator = Validator::make(
			array('txtProductionDateFromForSearch' => Input::get('txtProductionDateFromForSearch')),
			array('txtProductionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E062 : Enter Production Date."
				]);
		
				return $pViewData;
		}

		//must check production date(To)
		$lValidator = Validator::make(
			array('txtProductionDateToForSearch' => Input::get('txtProductionDateToForSearch')),
			array('txtProductionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails())
		{
		//		//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E062 : Enter Production Date."
				]);
		
				return $pViewData;
		}

		//must check inspection date(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		//must check inspection date(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}


		return $pViewData;
	}

	//**************************************************************************
	// process         ★isErrorForShonin
	// overview        error check for approval
	// argument        Array to return to screen
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function isErrorForShonin($pViewData)
	{
		//must check Process
		$lValidator = Validator::make(
			array('cmbProcessForSearch' => Input::get('cmbProcessForSearch')),
			array('cmbProcessForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E044 : Select Process."
				]);

				return $pViewData;
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
			array('cmbCustomerForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E049 : Select Customer."
				]);

				return $pViewData;
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForSearch' => Input::get('cmbCheckSheetNoForSearch')),
			array('cmbCheckSheetNoForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E006 : Enter Check Sheet No. ."
				]);

				return $pViewData;
		}

		//must check revision No.
		$lValidator = Validator::make(
			array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
			array('cmbRevisionNoForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E035 : Enter Revision No. ."
				]);

				return $pViewData;
		}

		//must check Material Name
		$lValidator = Validator::make(
			array('cmbMaterialNameForSearch' => Input::get('cmbMaterialNameForSearch')),
			array('cmbMaterialNameForSearch' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E046 : Enter Material Name."
			]);

			return $pViewData;
		}

		//must check Material Size
		$lValidator = Validator::make(
			array('cmbMaterialSizeForSearch' => Input::get('cmbMaterialSizeForSearch')),
			array('cmbMaterialSizeForSearch' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E048 : Enter Material Size."
			]);

			return $pViewData;
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
			array('txtInspectorCodeForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E012 : Enter Inspector Code."
				]);

				return $pViewData;
		}


		return $pViewData;
	}

	//**************************************************************************
	// process            ★updateLineStopData
	// overview           register Inspection result Header and detail,update
	// argument           Inspection Result No., Data Revision, User ID（login ID）
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function updateLineStopData($pInspectionResultNo, $pDataRev, $pUserID)
	{
		$lTblLineStopTargetInspectionNo = [];       //LineStop扱いにする対象Inspection No.のDataTable
		$lRowLineStopTargetInspectionNo = [];       //LineStop扱いにする対象Inspection No.のDataRow

		$lTblLineStopTargetInspectionNo = DB::select('
		         SELECT DETL.INSPECTION_NO
		           FROM TRESHEDT AS HEAD
		     INNER JOIN TRESDETT AS DETL
		             ON HEAD.INSPECTION_RESULT_NO = DETL.INSPECTION_RESULT_NO
		          WHERE HEAD.INSPECTION_RESULT_NO = :InspectionResultNo
		            AND DETL.INSPECTION_RESULT_TYPE  IS NULL
		       ORDER BY DETL.INSPECTION_NO
		',
			[
				"InspectionResultNo" => $pInspectionResultNo,
			]
		);


		//1 transaction for header and detail
		DB::transaction(
			function() use ($pInspectionResultNo
			               ,$pDataRev
			               ,$pUserID
			               ,$lTblLineStopTargetInspectionNo
			               ,$lRowLineStopTargetInspectionNo)
			{
				//update header
				DB::update('
						UPDATE TRESHEDT
						   SET LAST_UPDATE_USER_ID  = :LastUpdateUserID
						      ,UPDATE_YMDHMS        = now()
						      ,DATA_REV             = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
						   AND DATA_REV             = :DataRevision
				',
					[
						"LastUpdateUserID"    => TRIM((String)$pUserID),
						"InspectionResultNo"  => $pInspectionResultNo,
						"DataRevision"        => $pDataRev,
					]
				);

				//register detail while looping data
				foreach ($lTblLineStopTargetInspectionNo as $lRowLineStopTargetInspectionNo)
				{
					//Cast previously　because it has erroe to read value of array while CASTing
					$lRowLineStopTargetInspectionNo = (Array)$lRowLineStopTargetInspectionNo;

					//register detail
					DB::update('
							UPDATE TRESDETT
							   SET INSPECTION_RESULT_TYPE = "5"
							      ,LAST_UPDATE_USER_ID    = :LastUpdateUserID
							      ,UPDATE_YMDHMS          = now()
							      ,DATA_REV               = DATA_REV + 1
							 WHERE INSPECTION_RESULT_NO   = :InspectionResultNo
							   AND DATA_REV               = :DataRevision
					',
						[
							"LastUpdateUserID"    => TRIM((String)$pUserID),
							"InspectionResultNo"  => $pInspectionResultNo,
							"DataRevision"        => $pDataRev,
						]
					);
				}

			//return from obscurity function to the caller
			return true;
			}
		);

		return true;
	}

	//**************************************************************************
	// process            ★deleteInspectionResultData
	// overview           delete header and detail of inspection result
	// argument           Inspection Result No.（inside key per button）、DataRevision
	// return value       Array
	// record of updates  No,1 2014.08.26
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function deleteInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lDeleteCount = 0;  //count of deleted header data

		$lDeleteCount = DB::delete('
			DELETE FROM TRESHEDT
			      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
			        AND DATA_REV             = :DataRevision
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
				"DataRevision"       => $pDataRev,
			]
		);

		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		DB::delete('
		DELETE FROM TRESDETT
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		DB::delete('
		DELETE FROM TRESANAL
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		return $lDeleteCount;
	}

	//**************************************************************************
	// process         ★convertDateFormat
	// overview        change data type
	// argument        date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//                 pattern(1:English->Japanese 2:Japanese->English)
	// return value    
	// author          s-miyamoto
	// date            Ver.01  2014.08.26 v1.00 FIX
	// remarks         
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}

	//**************************************************************************
	// process         ★getInspectionSheetMasterDataCount
	// overview        select master data (use input item)
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectionSheetMasterDataCount($pProcess ,$pCustomer ,$pCheckSheetNo)
	{
		$lTblInspectionSheetMasterDataCount = [];       //DataTable
		$lRowInspectionSheetMasterDataCount = [];       //DataRow
		$lInspectionSheetMasterDataCount    = 0;        //result of COUNT

			$lTblInspectionSheetMasterDataCount = DB::select('
				    SELECT COUNT(SHEET.INSPECTION_SHEET_NO) AS COUNT
				      FROM TISHEETM AS SHEET
				     WHERE SHEET.INSPECTION_SHEET_NO  = :InspectionSheetNo
				       AND SHEET.PROCESS_ID           = :Process
				       AND SHEET.CUSTOMER_ID          = :Customer
				       AND SHEET.DELETE_FLG           = "0"
			',
				[
					"Process"             => TRIM((String)$pProcess),
					"Customer"            => TRIM((String)$pCustomer),
					"InspectionSheetNo"   => TRIM((String)$pCheckSheetNo),
				]
			);

		//read result(1 line)
		$lRowInspectionSheetMasterDataCount = $lTblInspectionSheetMasterDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowInspectionSheetMasterDataCount = (Array)$lRowInspectionSheetMasterDataCount;

		//get count
		$lInspectionSheetMasterDataCount = $lRowInspectionSheetMasterDataCount["COUNT"];

		return $lInspectionSheetMasterDataCount;
	}

	//**************************************************************************
	// process         ★getLogicalDuplicationDataCount
	// overview        get count of redundant data of logic key 論理キー重複データ件数取得
	// argument        
	// return value    Int（count of redundant data >=0 or 1）
	// author  　　　　　  s-miyamoto
	// date            Ver.01 2014.08.26 v1.00 FIX
	// remarks         Ver.02
	//**************************************************************************
	private function getLogicalDuplicationDataCount($pCheckSheetNo ,$pDataRevision ,$pProcess ,$pMachineNo ,$pMoldNo ,$pInspectorCode ,$pInspectionDate ,$pCondition ,$pInspectionTime)
	{
		$lTblLogicalDupulicationDataCount = [];       //DataTable
		$lRowLogicalDupulicationDataCount = [];       //DataRow
		$lDataCount                       = 0;        //result of COUNT

			$lTblLogicalDupulicationDataCount = DB::select('
					SELECT COUNT("X") AS COUNT
					  FROM TRESHEDT AS HEAD
					 WHERE INSPECTION_SHEET_NO = :InspectionSheetNo
					   AND REV_NO              = :RevisionNo
					   AND PROCESS_ID          = :Process
					   AND MACHINE_NO          = :MachineNo
					   AND MOLD_NO             = :MoldNo
					   AND INSERT_USER_ID      = :InspectorCode
					   AND INSPECTION_YMD      = :InspectionDate
					   AND CONDITION_CD        = :Condition
					   AND INSPECTION_TIME_ID  = :InspectionTime
			',
				[
					"InspectionSheetNo" => TRIM((String)$pCheckSheetNo),
					"RevisionNo"        => TRIM((String)$pDataRevision),
					"Process"           => TRIM((String)$pProcess),
					"MachineNo"         => TRIM((String)$pMachineNo),
					"MoldNo"            => TRIM((String)$pMoldNo),
					"InspectorCode"     => TRIM((String)$pInspectorCode),
					"InspectionDate"    => $this->convertDateFormat((String)$pInspectionDate, "1"),
					"Condition"         => TRIM((String)$pCondition),
					"InspectionTime"    => TRIM((String)$pInspectionTime),
				]
			);

		//read result(1 line)
		$lRowLogicalDupulicationDataCount = $lTblLogicalDupulicationDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowLogicalDupulicationDataCount = (Array)$lRowLogicalDupulicationDataCount;

		//get count
		$lDataCount = $lRowLogicalDupulicationDataCount["COUNT"];

		return $lDataCount;
	}

	//**************************************************************************
	// process         ★getInspectionTimeMasterDataCount
	// overview        greturn count corresponding to Check
	// argument        
	// return value    Int（count of Inspection No. data corresponding to the TIME >=0 or 1）
	// author  　　　　　  s-miyamoto
	// date            Ver.01 2014.08.26 v1.00 FIX
	// remarks         Ver.02
	//**************************************************************************
	private function getInspectionTimeMasterDataCount($pCheckSheetNo ,$pRevisionNo ,$pInspectionTime)
	{
		$lTblCheckNoDataCount = [];       //DataTable
		$lRowCheckNoDataCount = [];       //DataRow
		$lDataCount           = 0;        //result of COUNT

			$lTblCheckNoDataCount = DB::select('
				    SELECT COUNT(ISTM.INSPECTION_TIME_ID) AS NO_COUNT
				      FROM TINSPNOM AS ISNO
				INNER JOIN TISHEETM AS SHEET
				        ON ISNO.INSPECTION_SHEET_NO = SHEET.INSPECTION_SHEET_NO
				       AND ISNO.REV_NO              = SHEET.REV_NO
				INNER JOIN TINSNTMM AS ISTM
				        ON ISNO.INSPECTION_SHEET_NO = ISTM.INSPECTION_SHEET_NO
				       AND ISNO.INSPECTION_POINT    = ISTM.INSPECTION_POINT
				     WHERE ISNO.INSPECTION_SHEET_NO = :InspectionSheetNo
				       AND ISNO.REV_NO              = :RevisionNo
				       AND ISTM.INSPECTION_TIME_ID  = :InspectionTime
				       AND SHEET.DELETE_FLG         = "0"
				       AND ISNO.DELETE_FLG          = "0"
				       AND ISTM.DELETE_FLG          = "0"
			',
				[
					"InspectionSheetNo" => TRIM((String)$pCheckSheetNo),
					"RevisionNo"        => TRIM((String)$pRevisionNo),
					"InspectionTime"    => TRIM((String)$pInspectionTime),
				]
			);

		//echo $pCheckSheetNo . '<br />' . $pDataRevision . '<br />' . $pInspectionTime; die();

		//read result(1 line)
		$lRowCheckNoDataCount = $lTblCheckNoDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowCheckNoDataCount = (Array)$lRowCheckNoDataCount;

		//get count
		$lDataCount = $lRowCheckNoDataCount["NO_COUNT"];

		return $lDataCount;
	}


	//**************************************************************************
	// process         ★setProcessList
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setProcessList($pViewData)
	{
		$lArrProcessList     = ["" => ""];  //view data to return

		//if process list does not exist in session, search and store in session
		if (is_null(Session::get('BA1010SetProcessDropdownListData')))
		{
			//search
			$lArrProcessList = $this->getProcessList();

			//store in session
			Session::put('BA1010SetProcessDropdownListData', $lArrProcessList); //
		}
		else
		{
			//get from session
			$lArrProcessList = Session::get('BA1010SetProcessDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getProcessList
	// overview        get data for process data combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getProcessList()
	{
		$lTblProcessList			= []; //result of getting information of code order name
		$lRowProcessList			= []; //work when it change to array
		$lArrDataProcessList		= []; //List of code order name to return

		$lTblProcessList = DB::select(
		'
			SELECT PROCESS.CODE_ORDER
			     , PROCESS.CODE_NAME
			     , PROCESS.DISPLAY_ORDER
			  FROM TCODEMST AS PROCESS
			 WHERE CODE_CLASS = "002"
			 ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblProcessList = (array)$lTblProcessList;

		//add blank space
		$lArrDataProcessList += [
			"" => ""
		];

		//data exist
		if ($lTblProcessList != null)
		{
			//store result in Array again
			foreach ($lTblProcessList as $lRowProcessList)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowProcessList = (Array)$lRowProcessList;

				$lArrDataProcessList += [
					$lRowProcessList["CODE_ORDER"] => $lRowProcessList["CODE_NAME"]
				];
			}
		}

		return $lArrDataProcessList;
	}

	//**************************************************************************
	// process         ★setCustomerList
	// overview        get data for customer combo box and set to Array for displaying screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCustomerList($pViewData)
	{
		$lArrCustomerList     = ["" => ""];  //view data to return

		//if cutomer list does not exist in session, search and store in session
		if (is_null(Session::get('BA1010SetCustemorDropdownListData'))) {
			//search
			$lArrCustomerList = $this->getCustomerList();

			//store in session
			Session::put('BA1010SetCustemorDropdownListData', $lArrCustomerList);
		}
		else
		{
			//get from session
			$lArrCustomerList = Session::get('BA1010SetCustemorDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList,
			"arrDataListCustomerForSearchList" => $lArrCustomerList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getCustomerList
	// overview        get data for customer combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCustomerList()
	{
		$lTblCustomerList			= []; //result of getting information of customer
		$lRowCustomerList			= []; //work when it change to array
		$lArrDataCustomerList		= []; //List of customer to return

		$lTblCustomerList = DB::select(
		'
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TCUSTOMM AS CUST
		       WHERE CUST.DELETE_FLG = "0"
		    ORDER BY CUST.DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCustomerList = (array)$lTblCustomerList;

		//add blank space
		$lArrDataCustomerList += [
			"" => ""
		];

		//data exist
		if ($lTblCustomerList != null)
		{
			//store result in Array again
			foreach ($lTblCustomerList as $lRowCustomerList) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCustomerList = (Array)$lRowCustomerList;

				$lArrDataCustomerList += [
					$lRowCustomerList["CUSTOMER_ID"] => $lRowCustomerList["CUSTOMER_NAME"]
				];
			}

		}

		return $lArrDataCustomerList;
	}



	// //**************************************************************************
	// // process         setCustomerNoListEntry
	// // overview        get data for Inspection No.combo box and set Array to display screen
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function setCustomerNoListEntry($pViewData, $pMasterNo, $pProcessId, $pViewDataKey)
	// {
	// 	$lArrCustomerNoList     = ["" => ""];  //view data to return

	// 	//search
	// 	$lArrCustomerNoList = $this->getCustomerNoListEntry($pMasterNo, $pProcessId);

	// 	//add to array for transportion to screen(written in +=))
	// 	$pViewData += [
	// 		$pViewDataKey => $lArrCustomerNoList
	// 	];

	// 	return $pViewData;
	// }

	// //**************************************************************************
	// // process         getCustomerNoListEntry
	// // overview        get data for Inspection No.combo box with SQL
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function getCustomerNoListEntry($pMasterNo, $pProcessId)
	// {
	// 	$lTblCustomerNo			= []; //result of getting infomation of Inspection No.
	// 	$lRowCustomerNo			= []; //work when it change to array
	// 	$lArrCustomerNo			= []; //List of Inspection No. to return
	// 	$lRowCountCustomerNo		= 0;  //

	// 	$lTblCustomerNo = DB::select('
	// 	      SELECT CUST.CUSTOMER_ID
	// 	            ,CUST.CUSTOMER_NAME
	// 	        FROM TISHEETM AS SHET
	//  LEFT OUTER JOIN TCUSTOMM CUST
	// 	          ON CUST.CUSTOMER_ID = SHET.CUSTOMER_ID
		        
	// 	       WHERE SHET.DELETE_FLG  = "0"
	// 	         AND SHET.MASTER_NO   = :MasterNo
	// 	         AND SHET.PROCESS_ID  = :ProcessId
	// 	    GROUP BY SHET.CUSTOMER_ID
	// 	    ORDER BY SHET.REV_NO DESC
	// 	',
	// 			[
	// 				"MasterNo"   => $pMasterNo,
	// 				"ProcessId"	 => $pProcessId,
	// 			]
	// 	);

	// 	//cast search result to array（two dimensions Array）
	// 	$lTblCustomerNo = (array)$lTblCustomerNo;

	// 	//data exist
	// 	if ($lTblCustomerNo != null)
	// 	{
	// 		//store result in Array again
	// 		foreach ($lTblCustomerNo as $lRowCustomerNo) {
	// 			$lRowCountCustomerNo = $lRowCountCustomerNo + 1;
	// 		}

	// 		if ($lRowCountCustomerNo != 1)
	// 		{
	// 			//add blank space
	// 			$lArrCustomerNo += [
	// 				"" => ""
	// 			];
	// 		}

	// 		//store result in Array again
	// 		foreach ($lTblCustomerNo as $lRowCustomerNo) {
	// 			//Cast previously　because it has erroe to read value of array while CASTing
	// 			$lRowCustomerNo = (Array)$lRowCustomerNo;

	// 			$lArrCustomerNo += [
	// 				$lRowCustomerNo["CUSTOMER_ID"] => $lRowCustomerNo["CUSTOMER_NAME"]
	// 			];
	// 		}
	// 	}
	// 	else
	// 	{
	// 		//add blank space
	// 		$lArrCustomerNo += [
	// 			"" => ""
	// 		];
	// 	}

	// 	return $lArrCustomerNo;
	// }


	// //**************************************************************************
	// // process         getCustomerID
	// // overview        
	// // argument        
	// // return value    
	// // date            Ver.01 
	// // remarks         
	// //**************************************************************************
	// private function getCustomerID($pMasterNo, $pProcessId)
	// {
	// 	$lTblCustomerID			= [];
		
	// 	$lTblCustomerID = DB::select('
	// 	      SELECT SHET.CUSTOMER_ID
	// 	        FROM TISHEETM AS SHET
		        
	// 	       WHERE SHET.DELETE_FLG  = "0"
	// 	         AND SHET.MASTER_NO   = :MasterNo
	// 	         AND SHET.PROCESS_ID  = :ProcessId
	// 	    GROUP BY SHET.CUSTOMER_ID
	// 	    ORDER BY SHET.REV_NO DESC
	// 	',
	// 			[
	// 				"MasterNo"   => $pMasterNo,
	// 				"ProcessId"	 => $pProcessId,
	// 			]
	// 	);
		
	// 	return $lTblCustomerID;
	// }

	//**************************************************************************
	// process         ★setCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCheckSheetNoListEntry($pViewData, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoListEntry($pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetNoListEntry($pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return
		$lRowCountCheckSheetNo		= 0;  //

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,ITEM.MATERIAL_NAME AS MATERIAL_NAME
		            ,ITEM.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		   LEFT JOIN TITEMMST AS ITEM
				  ON ITEM.ITEM_NO     = SHET.ITEM_NO
				 AND ITEM.DELETE_FLG  = "0"
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
			{
				$lRowCountCheckSheetNo = $lRowCountCheckSheetNo + 1;
			}

			if ($lRowCountCheckSheetNo != 1)
			{
				//add blank space
				$lArrCheckSheetNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrCheckSheetNo += [
				"" => ""
			];
		}

		return $lArrCheckSheetNo;
	}

	//**************************************************************************
	// process         ★setCheckSheetNoListSearch
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCheckSheetNoListSearch($pViewData, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoListSearch($pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getCheckSheetNoListSearch
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetNoListSearch($pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,ITEM.MATERIAL_NAME AS MATERIAL_NAME
		            ,ITEM.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		   LEFT JOIN TITEMMST AS ITEM
				  ON ITEM.ITEM_NO     = SHET.ITEM_NO
				 AND ITEM.DELETE_FLG  = "0"
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//add blank space
		$lArrCheckSheetNo += [
			"" => ""
		];

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}
		}

		return $lArrCheckSheetNo;
	}

	//**************************************************************************
	// process         ★setMaterialNameList
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setMaterialNameList($pViewData, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialNameList     = ["" => ""];  //view data to return
       
		//search
		$lArrMaterialNameList = $this->getMaterialNameList($pProcessId, $pCustomerId, $pCheckSheetNo);
       
		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialNameList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getMaterialNameList
	// overview        get data for Material Name combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMaterialNameList($pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialName			= []; //result of getting infomation of Material Name
		$lRowMaterialName			= []; //work when it change to array
		$lArrMaterialName			= []; //List of Material Name to return
		$lRowCountMaterialName		= 0;  //

		$lTblMaterialName = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,ITEM.MATERIAL_NAME AS MATERIAL_NAME
		            ,ITEM.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		   LEFT JOIN TITEMMST AS ITEM
				  ON ITEM.ITEM_NO     = SHET.ITEM_NO
		       WHERE SHET.DELETE_FLG  = "0"
		         AND ITEM.DELETE_FLG  = "0"
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"ProcessId"      => $pProcessId,
					"CustomerId"     => $pCustomerId,
					"CheckSheetNo1"  => $pCheckSheetNo,
					"CheckSheetNo2"  => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialName = (array)$lTblMaterialName;
      
		//data exist
		if ($lTblMaterialName != null)
		{
			foreach ($lTblMaterialName as $lRowMaterialName)
			{
				$lRowCountMaterialName = $lRowCountMaterialName + 1;
			}

			if ($lRowCountMaterialName != 1)
			{
				//add blank space
				$lArrMaterialName += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialName as $lRowMaterialName)
			{
				$lRowMaterialName = (Array)$lRowMaterialName;

				$lArrMaterialName += [
					$lRowMaterialName["MATERIAL_NAME"] => $lRowMaterialName["MATERIAL_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrMaterialName += [
				"" => ""
			];
		}

		return $lArrMaterialName;
	}

	//**************************************************************************
	// process         ★setMaterialSizeList
	// overview        get data for  Material Size combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setMaterialSizeList($pViewData, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialSizeList     = ["" => ""];  //view data to return

		//search
		$lArrMaterialSizeList = $this->getMaterialSizeList($pProcessId, $pCustomerId, $pCheckSheetNo);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialSizeList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getMaterialSizeList
	// overview        get data for Material Size combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMaterialSizeList($pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialSize			= []; //result of getting infomation of Material Size
		$lRowMaterialSize			= []; //work when it change to array
		$lArrMaterialSize			= []; //List of Material Size to return
		$lRowCountMaterialSize		= 0;  //

		$lTblMaterialSize = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,ITEM.MATERIAL_NAME AS MATERIAL_NAME
		            ,ITEM.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		   LEFT JOIN TITEMMST AS ITEM
				  ON ITEM.ITEM_NO     = SHET.ITEM_NO
		       WHERE SHET.DELETE_FLG  = "0"
		         AND ITEM.DELETE_FLG  = "0"
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"ProcessId"        => $pProcessId,
					"CustomerId"       => $pCustomerId,
					"CheckSheetNo1"    => $pCheckSheetNo,
					"CheckSheetNo2"    => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialSize = (array)$lTblMaterialSize;

		//data exist
		if ($lTblMaterialSize != null)
		{
			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize)
			{
				$lRowCountMaterialSize = $lRowCountMaterialSize + 1;
			}

			if ($lRowCountMaterialSize != 1)
			{
				//add blank space
				$lArrMaterialSize += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowMaterialSize = (Array)$lRowMaterialSize;

				$lArrMaterialSize += [
					$lRowMaterialSize["MATERIAL_SIZE"] => $lRowMaterialSize["MATERIAL_SIZE"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrMaterialSize += [
				"" => ""
			];
		}

		return $lArrMaterialSize;
	}

	//**************************************************************************
	// process         ★setRevNoList
	// overview        get data for revision No. and set to Array for displaying screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setRevNoList($pViewData, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lArrRevNoList     = ["" => ""];  //view data to return

		//search
		$lArrRevNoList = $this->getRevNoList($pProcessId, $pCustomerId, $pInspectionSheetNo);

		//add to array for transportion to screen(written in +=)
		$pViewData += [
			"arrRevNoForSearchList" => $lArrRevNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         ★getRevNoList
	// overview        get data for revision No. combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getRevNoList($pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lTblRevNo			= []; //result of getting infomstion of revision No.
		$lRowRevNo			= []; //work when it change to array
		$lArrRevNo			= []; //List of revision No. to return
		$lRowCountRevNo		= 0;  //

		$lTblRevNo = DB::select('
		      SELECT SHET.REV_NO
		            ,SHET.REV_NO AS REV_NO_NAME
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = :InspectionSheetNo
		    ORDER BY SHET.DISPLAY_ORDER
		',
				[
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
					"InspectionSheetNo" => $pInspectionSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblRevNo = (array)$lTblRevNo;

		//data exist
		if ($lTblRevNo != null)
		{
			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo)
			{
				$lRowCountRevNo = $lRowCountRevNo + 1;
			}

			if ($lRowCountRevNo != 1)
			{
				//add blank space
				$lArrRevNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowRevNo = (Array)$lRowRevNo;

				$lArrRevNo += [
					$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrRevNo += [
				"" => ""
			];
		}

		return $lArrRevNo;
	}

	//**************************************************************************
	// process         getPatarn
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getPatarn($pPatarnNo)
	{
		$lTblSearchData     = [];

			$lTblSearchData = DB::select('
				SELECT  CALL_CONTENTS
				FROM	TCHGPTNMST
				WHERE	PTN_NO = PatarnNo
			',
				[
					"PatarnNo"   =>  $pPatarnNo,
				]
			);

		return $lTblSearchData;
	}


}
