<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use DB;
use Session;
use Carbon\Carbon;
use PHPExcel; 
use PHPExcel_IOFactory; 

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Exception;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');

//**************************************************************************
// display:	 Graph
// overview: For Plasess Company
// author:	Mai Nawaphat(Mai ^^)
// date:	14/2/2018
//**************************************************************************
class BA4010GraphController
extends Controller
{  
	public function MasterAction()
	{  
		$lViewData						= []; //Array for transportion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];
		$lViewData += $this->getProcessList($lViewData);
		$lViewData += $this->getCustomerList($lViewData);
		$lViewData += $this->getProductNoList($lViewData);
		$lViewData += $this->getRevNoList($lViewData);
		$lViewData += $this->getMoldNoList($lViewData);
		$lViewData += $this->getTeamNameList($lViewData);
		$lViewData += $this->getMaterialNameList($lViewData);
		$lViewData += $this->getMaterialSizeList($lViewData);
		$lViewData += $this->getInspectionDateFromToForGraph($lViewData);
		$lViewData += $this->getConditionList($lViewData);
		$lViewData += $this->setListForms($lViewData);

		$lViewData += [
				"SelectTypeData" => Input::get('cmbSelectTypeForGraph'),
		];

		if (Input::has('btnGraph')){

			 	$lTblSearchResultData = $this->getGraphResultList();
				// echo "<pre>"; print_r($lTblSearchResultData); echo "</pre>";

			 	if (count($lTblSearchResultData) == 0)
				{ 
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
										$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"LCLStdev"              => null,
										"CLS"                   => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
										"arrMaxXbarChart"       => null,
										"arrMinXbarChart"       => null,
										"arrMaxRangeChart"      => null,
										"arrMinRangeChart"      => null,
										"arrMaxStdevChart"      => null,
										"arrMinStdevChart"      => null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexLCL"              => null,
										"indexUCL"              => null,
					];
				}else{ 	
					// dd($lTblSearchResultData);
			    	foreach ($lTblSearchResultData as $key => $value) 
					{   

						//Title For Average, Range, Xbar Or Header Chart				
						$arrPoint[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_POINT;
						$arrCavity[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CAVITY_NO;
						$arrWhere[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->WHERE_NO;
						// //Range 
						$arrRange[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->RANGE_VALUE;
						// //Average
						$arrAverage[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->AVERAGE_VALUE;
						$arrMax[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MAX_VALUE;
						$arrMin[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->MIN_VALUE;

	     //                //Stdev
					    $arrStandard[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->STDEV;
						// //Label			
					    $arrLotNo[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->LOT_NO;
						$arrProcessName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->PROCESS_NAME;
						$arrTeamName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->TEAM_NAME;
						$arrConditionName[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->CONDITION_CD;
						$arrInspection[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_YMD;
						$arrTime[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->INSPECTION_TIME_NAME;
						$arrAverageUCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_UCL;
						$arrAverageLCL[$value->INSPECTION_POINT][$value->CAVITY_NO][$value->WHERE_NO][] = $value->XBAR_LCL;
						//CPK
						if($value->STDEV == 0.000){
							$arrBellCurve[] = array(array(0, 0));
						}else{
							$arrBellCurve[] = $this->getBellCurve($value->STDEV,$value->AVERAGE_VALUE);
						}

						$arrMinutOneSigma[] = $value->AVERAGE_VALUE - $value->STDEV;
						$arrMinutTwoSigma[] = $value->AVERAGE_VALUE - ( 2 * $value->STDEV);
						$arrMinutThreeSigma[] = $value->AVERAGE_VALUE - ( 3 * $value->STDEV);
						$arrMinutFourSigma[] = $value->AVERAGE_VALUE - ( 4 * $value->STDEV);
						$arrPlusOneSigma[] = $value->AVERAGE_VALUE + $value->STDEV;
						$arrPlusTwoSigma[] = $value->AVERAGE_VALUE + ( 2 * $value->STDEV);
						$arrPlusThreeSigma[] = $value->AVERAGE_VALUE + ( 3 * $value->STDEV);
						$arrPlusFourSigma[] = $value->AVERAGE_VALUE + ( 4 * $value->STDEV);
						$arrAvg[] = $value->AVERAGE_VALUE;
						$arrAverageUCLCPK[] = $value->XBAR_UCL;
						$arrAverageLCLCPK[] = $value->XBAR_LCL;

						$arrPointCPK[] = $value->INSPECTION_POINT;
						$arrCavityCPK[] = $value->CAVITY_NO;
						$arrWhereCPK[] = $value->WHERE_NO;
						$arrStdev[] = $value->STDEV;
						$USL[]      = $value->STD_MAX_SPEC;
						$LSL[]      = $value->STD_MIN_SPEC;
					}

					//Title For Average, Range, Xbar Chart	Or Header Chart
					$arrPoint1Dimention = $this->arrThreeToOneDimention($arrPoint);		
					$arrCavity1Dimention = $this->arrThreeToOneDimention($arrCavity);	
					$arrWhere1Dimention = $this->arrThreeToOneDimention($arrWhere);	

					//Range  Chart
                    $arrRangeForGraph = $this->getArrPointOnLineChart($arrRange);
                    $coefficientUCLRObject = $this->coefficientObject("D4", $arrRange);
                    $averageRange = $this->arrAverageValue($arrRange);
                    $coefficientLCLRObject = $this->coefficientObject("D3", $arrStandard);

                    $UCLR = $this->arrLimitUCLOrLCL($coefficientUCLRObject, $averageRange);
                    $UCLRUpper = $this->arrMaxOfChart($coefficientUCLRObject, $averageRange);
                    $arrUCLRFillPointForGroup = $this->fourDimentionToTwoDimention($UCLR);
				    $arrUCLRUpper = $this->fourDimentionToTwoDimention($UCLRUpper);

                    $LCLR = $this->arrLimitUCLOrLCL($coefficientLCLRObject, $averageRange);
                    $LCLRLower = $this->arrMinOfChart($coefficientLCLRObject, $averageRange);
                    $arrLCLRFillPointForGroup = $this->fourDimentionToTwoDimention($LCLR);
				    $arrLCLRLower = $this->fourDimentionToTwoDimention($LCLRLower);

				    $arrCLR = $this->fourDimentionToTwoDimention($averageRange);

                    //Xbar Chart
                    $arrAverageForGraph = $this->getArrPointOnLineChart($arrAverage);
                    $arrMaxForGraph = $this->getArrPointOnLineChart($arrMax);
                    $arrMinForGraph = $this->getArrPointOnLineChart($arrMin);

				    	//UCL and LCL of Xbar
				    $averageAverage = $this->arrAverageValue($arrAverage);
                    $coefficientXObject = $this->coefficientObject("A2", $arrAverage);

                    $UCLX = $this->arrLimitUCLXbar($coefficientXObject, $averageRange, $averageAverage);
                    $UCLXUpper = $this->arrMaxOfXbarChart($coefficientXObject, $averageRange, $averageAverage); 
                    $arrUCLXFillPointForGroup = $this->fourDimentionToTwoDimention($UCLX);
				    $arrUCLXUpper = $this->fourDimentionToTwoDimention($UCLXUpper);
				    $LCLX = $this->arrLimitLCLXbar($coefficientXObject, $averageRange, $averageAverage);
                    $LCLXLower = $this->arrMinOfXbarChart($coefficientXObject, $averageRange, $averageAverage);
                    $arrLCLXFillPointForGroup = $this->fourDimentionToTwoDimention($LCLX);
				    $arrLCLXLower = $this->fourDimentionToTwoDimention($LCLXLower);
  
				       //CL of Xbar and Stdev
                    $arrCLX = $this->fourDimentionToTwoDimention($averageAverage);

                       //UCL and LCL of Stdev

                    $averageStdev = $this->arrAverageValue($arrStandard);
                    $arrCLS = $this->fourDimentionToTwoDimention($averageStdev);

                    $coefficientSObject = $this->coefficientObject("A3", $averageStdev);
                    $UCLS = $this->arrLimitUCLXbar($coefficientSObject, $averageStdev, $averageAverage);
                    $UCLSUpper = $this->arrMaxOfXbarChart($coefficientSObject, $averageStdev, $averageAverage);
                    $arrUCLSFillPointForGroup = $this->fourDimentionToTwoDimention($UCLS);
				    $arrUCLSUpper = $this->fourDimentionToTwoDimention($UCLSUpper);

                    $coefficientSLObject = $this->coefficientObject("A2", $averageStdev);
				    $LCLS = $this->arrLimitLCLXbar($coefficientSLObject, $averageStdev, $averageAverage);
                    $LCLSLower = $this->arrMinOfXbarChart($coefficientSLObject, $averageStdev, $averageAverage);
                    $arrLCLSFillPointForGroup = $this->fourDimentionToTwoDimention($LCLS);
				    $arrLCLSLower = $this->fourDimentionToTwoDimention($LCLSLower);

                    //Stdev Chart
				    $arrStandardForGraph = $this->getArrPointOnLineChart($arrStandard);
				    // dd($USL);
                    $coefficientUCLStdevObject = $this->coefficientObject("B4", $arrStandard);
                    $coefficientLCLStdevObject = $this->coefficientObject("B3", $arrStandard);
                    $averageStdevForUCL = $this->arrAverageValue($arrStandard);
                    $UCLStdev = $this->arrLimitUCLOrLCL($coefficientUCLStdevObject, $averageStdevForUCL);
                    $LCLStdev = $this->arrLimitUCLOrLCL($coefficientLCLStdevObject, $averageStdevForUCL);
				    $arrUCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($UCLStdev);
				    $arrLCLStdevFillPointForGroup = $this->fourDimentionToTwoDimention($LCLStdev);

					//Start Label for Average, Range, Xbar Chart
					$arrLotNoForMerge = $this->getLabelForMerge($arrLotNo, "Lot No");
					$arrProcessNameForMerge = $this->getLabelForMerge($arrProcessName, "Process Name");
					$arrTeamNameForMerge = $this->getLabelForMerge($arrTeamName, "Team Name");
					$arrConditionForMerge = $this->getLabelForMerge($arrConditionName, "Condition Name");
					$arrInspectionForMerge = $this->getLabelForMerge($arrInspection, "Inspection Date");
					$arrTimeForMerge = $this->getLabelForMerge($arrTime, "Time");
					$mergeLabel = array_merge_recursive($arrLotNoForMerge, $arrProcessNameForMerge,$arrTeamNameForMerge, $arrConditionForMerge, $arrInspectionForMerge, $arrTimeForMerge);
					foreach ($mergeLabel as $key => $value) {
						foreach ($value as $key1 => $value1) {
							foreach ($value1 as $key2 => $value2) {
								$lebel1Dimention[$key1][] = $value2;
							}
						}
					}

					$arrLabel = array_values($lebel1Dimention);

					//CPK Chart
					$arrBarCPK = $this->getThreeToTwoDimention($arrBellCurve, "1");
					// echo "<pre>"; print_r($arrBarCPK); echo "</pre>";

					$arrLabelCPK = $this->getThreeToTwoDimention($arrBellCurve, "0");

					$arrCurve = $this->editThreeDimention($arrBellCurve);

                    foreach ($arrLabelCPK as $key => $value) {
                        if(count($value) == 1){
                        	$indexMinutOneSigma[$key] = null;
                        	$indexMinutTwoSigma[$key] = null;
                        	$indexMinutThreeSigma[$key] = null;
                        	$indexMinutFourSigma[$key] = null;
                        	$indexAvg[$key] = null;
                        	$indexPlusOneSigma[$key] = null;  
                        	$indexPlusTwoSigma[$key] = null; 
                        	$indexPlusThreeSigma[$key] = null;
                        	$indexPlusFourSigma[$key] = null;
                        }
							foreach ($value as $k => $val1) {
										if($val1 == $arrMinutOneSigma[$key]){
											$arrCurveValue[$key][] = "-1σ : ".number_format($val1, 4, '.', '');
											$indexMinutOneSigma[$key] = $k;
										}elseif ($val1 == $arrMinutTwoSigma[$key]) {
											$arrCurveValue[$key][] = "-2σ : ".number_format($val1, 4, '.', '');
											$indexMinutTwoSigma[$key] = $k;
										}elseif ($val1 == $arrMinutThreeSigma[$key]){
											$arrCurveValue[$key][] = "-3σ : ".number_format($val1, 4, '.', '');
											$indexMinutThreeSigma[$key] = $k; 
										}elseif ($val1 == $arrMinutFourSigma[$key]){
											$arrCurveValue[$key][] = "-4σ : ".number_format($val1, 4, '.', ''); 
											$indexMinutFourSigma[$key] = $k;  
										}elseif ($val1 == $arrAvg[$key]){
											$arrCurveValue[$key][] = "Average : ".number_format($val1, 4, '.', '');
											$indexAvg[$key] = $k; 
										}elseif ($val1 == $arrPlusOneSigma[$key]){
											$arrCurveValue[$key][] = "+1σ : ".number_format($val1, 4, '.', '');
											$indexPlusOneSigma[$key] = $k;  
										}elseif ($val1 == $arrPlusTwoSigma[$key]){
											$arrCurveValue[$key][] = "+2σ : ".number_format($val1, 4, '.', '');
											$indexPlusTwoSigma[$key] = $k; 
										}elseif ($val1 == $arrPlusThreeSigma[$key]){
											$arrCurveValue[$key][] = "+3σ : ".number_format($val1, 4, '.', ''); 
											$indexPlusThreeSigma[$key] = $k;
										}elseif ($val1 == $arrPlusFourSigma[$key]){
											$arrCurveValue[$key][] = "+4σ : ".number_format($val1, 4, '.', ''); 
											$indexPlusFourSigma[$key] = $k;
										}else{
											$arrCurveValue[$key][] = number_format($val1, 4, '.', '');
										} 
						}
					}

					foreach ($arrBellCurve as $key => $value) {
						foreach ($value as $key1 => $value1) {
							foreach ($value1 as $key2 => $value2) {
								if($key2 == 0){
									$dataY[$key][$key1] = $value2;
								}
								if($key2 == 1){
									$dataX[$key][$key1] = $value2;
								}
							}
						}
					}
					
				    foreach ($dataY as $key => $value) {
				    	foreach ($value as $key1 => $value1) {
				    		if($value1 == $arrAvg[$key]){
				    			$same[$key] = $key1;
				    		}
				    	}
				    }

				    foreach ($dataX as $key => $value) {
						foreach ($value as $key1 => $value1) {
                          	if(count($value) == 1){
                          	 		$indexMax[] = 0.005;
                          	}else{
                          		if($key1 == $same[$key]){
				    				$indexMax[] = $value1 + 0.005; 
				    		  	}
                          	}
				    	}
				    }

                    foreach ($dataX as $key => $value) {
                    	foreach ($value as $key1 => $value1) {
                    		if($key1 == 0){
                    			$indexMin[] = $value1 - 0.005;
                    		}
                    	}
                    }

        //MIN - MAX XBAR CHART
		foreach ($arrAverageForGraph as $key => $value) {
			    $arrAllXbarChart[$key] = array_merge($value,$arrMaxForGraph[$key],$arrMinForGraph[$key],$arrUCLXFillPointForGroup[$key], $arrLCLXFillPointForGroup[$key], $arrUCLSFillPointForGroup[$key], $arrCLX[$key], $arrLCLSFillPointForGroup[$key]);
			    foreach ($arrAllXbarChart as $key1 => $value1) {
			    	foreach ($value1 as $key2 => $value2) {
				    	if($value2 != ""){
				    		$arrDataNotEmptyXbarChart[$key][] = $value2;
				    	}
			    	}
			    	
			    }
				$arrMaxXbarChart[$key] = max($arrDataNotEmptyXbarChart[$key]) + 1;
				$arrMinXbarChart[$key] = min($arrDataNotEmptyXbarChart[$key]) - 1;
		}

		//MIN - MAX RANGE CHART
		foreach ($arrRangeForGraph as $key => $value) {
			    $arrAllRangeChart[$key] = array_merge($value,$arrUCLRFillPointForGroup[$key],$arrLCLRFillPointForGroup[$key],$arrCLR[$key]);
			    foreach ($arrAllRangeChart as $key1 => $value1) {
			    	foreach ($value1 as $key2 => $value2) {
				    	if($value2 != ""){
				    		$arrDataNotEmptyRangeChart[$key][] = $value2;
				    	}
			    	}
			    	
			    }
				$arrMaxRangeChart[$key] = max($arrDataNotEmptyRangeChart[$key]) + 0.1;
				$arrMinRangeChart[$key] = min($arrDataNotEmptyRangeChart[$key]) - 0.1;
		}

		//MIN - MAX STDEV CHART
		foreach ($arrStandardForGraph as $key => $value) {
			    $arrAllStdevChart[$key] = array_merge($value,$arrUCLStdevFillPointForGroup[$key],$arrLCLStdevFillPointForGroup[$key],$arrCLS[$key]);
			    foreach ($arrAllStdevChart as $key1 => $value1) {
			    	foreach ($value1 as $key2 => $value2) {
				    	if($value2 != ""){
				    		$arrDataNotEmptyStdevChart[$key][] = $value2;
				    	}
			    	}
			    	
			    }
				$arrMaxStdevChart[$key] = max($arrDataNotEmptyStdevChart[$key]) + 1;
				$arrMinStdevChart[$key] = min($arrDataNotEmptyStdevChart[$key]) - 1;
		}
	
		foreach ($arrLabelCPK as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$arrLabelCPKforinterval[$key][$key1] = $value1;
			}
		}

		foreach ($arrLabelCPKforinterval as $key => $value) {
			if(count($value) == 1){
                $arrFinish[$key] = [];
			}else{
				foreach ($value as $key1 => $value1) {
					if($key1 < count($value) -1 ){
	 					$arrFinish[$key][] = $value[$key1+1];
					}
				}
			}
		}

        foreach ($arrFinish as $key => $value) {
        	if(count($value) == 0){
        				$rangeInterval[$key] = 0;
        	}else{
        			$rangeInterval[$key] = $value[0] - $arrLabelCPKforinterval[$key][0];
        		
        	}
        }

		foreach ($arrLabelCPKforinterval as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if($value1 == 0){
					$searchIndexLCL[$key] = "";
				}else{
					if($arrAverageLCLCPK[$key] >= $value1 and $arrAverageLCLCPK[$key] < $arrFinish[$key][$key1]){
					    $searchIndexLCL[$key][] = $key1;
					}else{
					    $searchIndexLCL[$key][] = "";
					}
				}
				
			}
		}
		foreach ($searchIndexLCL as $key => $value) {
			if($value == ""){
					$indexLCL[$key] = "";
			}else{
				foreach ($value as $key1 => $value1) {
					if($value1 != ""){
						$indexLCL[$key] = $value1;
					}
				}
			}
			
		}

		foreach ($arrLabelCPKforinterval as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if($value1 == 0){
					$searchIndexUCL[$key] = "";
				}else{
					if($arrAverageUCLCPK[$key] >= $value1 and $arrAverageUCLCPK[$key] < $arrFinish[$key][$key1]){
					    $searchIndexUCL[$key][] = $key1;
					}else{
					    $searchIndexUCL[$key][] = "";
					}
				}
				
			}
		}
		foreach ($searchIndexUCL as $key => $value) {
			if($value == ""){
					$indexUCL[$key] = "";
			}else{
				foreach ($value as $key1 => $value1) {
					if($value1 != ""){
						$indexUCL[$key] = $value1;
					}
				}
			}
			
		}
				    
					$lViewData += [     
										"arrRange"     			=> $arrRangeForGraph,
										"UCLR"                  => $arrUCLRFillPointForGroup,
										"UCLRUpper"             => $arrUCLRUpper,
										"LCLR"                  => $arrLCLRFillPointForGroup,
										"LCLRLower"             => $arrLCLRLower,
										"CLR"                   => $arrCLR,
										"arrMax"                => $arrMaxForGraph,
										"arrMin"                => $arrMinForGraph,
										"arrAverage"            => $arrAverageForGraph,
										"UCLX"                  => $arrUCLXFillPointForGroup,
										"UCLXUpper"             => $arrUCLXUpper,
										"LCLX"                  => $arrLCLXFillPointForGroup,
										"LCLXLower"             => $arrLCLXLower,
										"CLX"                   => $arrCLX,
										"UCLS"                  => $arrUCLSFillPointForGroup,
										"UCLSUpper"             => $arrUCLSUpper,
										"LCLS"                  => $arrLCLSFillPointForGroup,
										"LCLSLower"             => $arrLCLSLower,
										"arrStandard"           => $arrStandardForGraph,
										"UCLStdev"              => $arrUCLStdevFillPointForGroup,
										"LCLStdev"              => $arrLCLStdevFillPointForGroup,
										"CLS"                   => $arrCLS,
										"arrLabel"              => $arrLabel,
										"arrPoint1Dimention" 	=> $arrPoint1Dimention,
										"arrCavity1Dimention" 	=> $arrCavity1Dimention,
										"arrWhere1Dimention" 	=> $arrWhere1Dimention,
										"arrBarCPK"    			=> $arrBarCPK,
										"arrLabelCPK"    		=> $arrLabelCPK,
										"arrCurve"              => $arrCurve,
										"arrCurveValue"         => $arrCurveValue,
										"arrMinutOneSigma"      => $arrMinutOneSigma,
										"arrMinutTwoSigma"      => $arrMinutTwoSigma,
										"arrMinutThreeSigma"    => $arrMinutThreeSigma,
										"arrMinutFourSigma"     => $arrMinutFourSigma,
										"arrPlusOneSigma"       => $arrPlusOneSigma,
										"arrPlusTwoSigma"       => $arrPlusTwoSigma,
										"arrPlusThreeSigma"     => $arrPlusThreeSigma,
										"arrPlusFourSigma"      => $arrPlusFourSigma,
										"arrAvg"				=> $arrAvg,
										"indexMax"              => $indexMax,
										"indexMin"              => $indexMin,
										"arrAverageUCLCPK"      => $arrAverageUCLCPK,
										"arrAverageLCLCPK"      => $arrAverageLCLCPK,
										"arrPointCPK"			=> $arrPointCPK,
										"arrCavityCPK"			=> $arrCavityCPK,
										"arrWhereCPK"			=> $arrWhereCPK,
										"arrStdev"				=> $arrStdev,
										"arrMaxXbarChart"       => $arrMaxXbarChart,
										"arrMinXbarChart"       => $arrMinXbarChart,
										"arrMaxRangeChart"      => $arrMaxRangeChart,
										"arrMinRangeChart"      => $arrMinRangeChart,
										"arrMaxStdevChart"      => $arrMaxStdevChart,
										"arrMinStdevChart"      => $arrMinStdevChart,
										"indexMinutOneSigma"    => $indexMinutOneSigma,
										"indexMinutTwoSigma"    => $indexMinutTwoSigma,
										"indexMinutThreeSigma"  => $indexMinutThreeSigma,
										"indexMinutFourSigma"   => $indexMinutFourSigma,
										"indexAvg"              => $indexAvg,
										"indexPlusOneSigma"     => $indexPlusOneSigma,
										"indexPlusTwoSigma"     => $indexPlusTwoSigma,
										"indexPlusThreeSigma"   => $indexPlusThreeSigma,
										"indexPlusFourSigma"    => $indexPlusFourSigma,
										"indexLCL"              => $indexLCL,
										"indexUCL"              => $indexUCL,
										"USL"                   => $USL,
										"LSL"                   => $LSL
					];		

				}
		}else if(Input::has('btnExcel')){
                $lTblSearchResultData = $this->getGraphResultList();

			 	if (count($lTblSearchResultData) == 0)
				{ 
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
										$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"LCLStdev"              => null,
										"CLS"                   => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
										"arrMaxXbarChart"       => null,
										"arrMinXbarChart"       => null,
										"arrMaxRangeChart"      => null,
										"arrMinRangeChart"      => null,
										"arrMaxStdevChart"      => null,
										"arrMinStdevChart"      => null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexLCL"              => null,
										"indexUCL"              => null,
					];


				}else{ 
				 	try 
				    {
				    	$spreadsheet = new Spreadsheet();
				    	$lTemplateFileNameInspectionSheet = "DataOnGraphPageV1_template.xlsx";
						$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameInspectionSheet);

						$sheet = $spreadsheet->getActiveSheet();
						foreach ($lTblSearchResultData as $key => $value) {
				        	$cell = $key + 2;
				        	$sheet->getCell("A".$cell)->setValue($value->PRODUCT_NO);
				        	$sheet->getCell("B".$cell)->setValue($value->REV_NO);
				        	$sheet->getCell("C".$cell)->setValue($value->PROCESS_NAME);
				        	$sheet->getCell("D".$cell)->setValue($value->INSPECTION_YMD);
				        	$sheet->getCell("E".$cell)->setValue($value->INSPECTION_TIME_ID);
				        	$sheet->getCell("F".$cell)->setValue($value->INSPECTION_TIME_NAME);
				        	$sheet->getCell("G".$cell)->setValue($value->CONDITION_CD_NAME);
				        	$sheet->getCell("H".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("I".$cell)->setValue($value->MOLD_NO);
				        	$sheet->getCell("J".$cell)->setValue($value->TEAM_ID);
				        	$sheet->getCell("K".$cell)->setValue($value->TEAM_NAME);
				        	$sheet->getCell("M".$cell)->setValue($value->CAVITY_NO);
				        	$sheet->getCell("N".$cell)->setValue($value->WHERE_NO);
				        	$sheet->getCell("O".$cell)->setValue($value->PICTURE_URL);
				        	$sheet->getCell("P".$cell)->setValue($value->MIN_VALUE);
				        	$sheet->getCell("Q".$cell)->setValue($value->MAX_VALUE);
				        	$sheet->getCell("R".$cell)->setValue($value->AVERAGE_VALUE);
				        	$sheet->getCell("S".$cell)->setValue($value->RANGE_VALUE);
				        	$sheet->getCell("T".$cell)->setValue($value->STDEV);
				        	$sheet->getCell("U".$cell)->setValue($value->CPK);
				        	$sheet->getCell("V".$cell)->setValue($value->STD_MIN_SPEC);
				        	$sheet->getCell("W".$cell)->setValue($value->STD_MAX_SPEC);
				        	$sheet->getCell("X".$cell)->setValue($value->LOT_NO);
				        	$sheet->getCell("Y".$cell)->setValue($value->PRODUCTION_YMD);
						}

					    //set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
						$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet);

						$lSaveFilePathAndName = "excel/".$lSaveFileName;  
						// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

						//save file（save in server temporarily）
						$writer = new XlsxWriter($spreadsheet);
						//if many data exist,it need very long time for SAVE.(it is available to download directly without save
						$writer->save($lSaveFilePathAndName);

						//download
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Length: '.filesize($lSaveFilePathAndName));
						header('Content-disposition: attachment; filename='.$lSaveFileName);
						readfile($lSaveFilePathAndName);

						//delete fime(saved in server)
						unlink($lSaveFilePathAndName);

						//message
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";	
						    
				    }
				    catch(Exception $e) 
				    {
				        die('Error loading file "'.pathinfo($lSaveFilePathAndName,PATHINFO_BASENAME).'": '.$e->getMessage());
				    }	

					$lViewData += [  
										"arrRange"     			=> null,
										"UCLR"                  => null,
										"UCLRUpper"             => null,
										"LCLR"                  => null,
										"LCLRLower"             => null,
										"CLR"                   => null,
										"arrMax"                => null,
										"arrMin"                => null,
										"arrAverage"            => null,
										"UCLX"                  => null,
										"UCLXUpper"             => null,
										"LCLX"                  => null,
										"LCLXLower"             => null,
										"CLX"                   => null,
										"UCLS"                  => null,
										"UCLSUpper"             => null,
										"LCLS"                  => null,
										"LCLSLower"             => null,
										"arrStandard"           => null,
										"UCLStdev"              => null,
										"MaxofSChart"           => null,
										"LCLStdev"              => null,
										"MinofSChart"           => null,
										"arrLabel"              => null,
										"arrPoint1Dimention" 	=> null,
										"arrCavity1Dimention" 	=> null,
										"arrWhere1Dimention" 	=> null,
										"arrBarCPK"    			=> null,
										"arrLabelCPK"    		=> null,
										"arrCurve"              => null,
										"arrCurveValue"         => null,
										"arrMinutOneSigma"      => null,
										"arrMinutTwoSigma"      => null,
										"arrMinutThreeSigma"    => null,
										"arrMinutFourSigma"     => null,
										"arrPlusOneSigma"       => null,
										"arrPlusTwoSigma"       => null,
										"arrPlusThreeSigma"     => null,
										"arrPlusFourSigma"      => null,
										"arrAvg"				=> null,
										"indexMinutOneSigma"    => null,
										"indexMinutTwoSigma"    => null,
										"indexMinutThreeSigma"  => null,
										"indexMinutFourSigma"   => null,
										"indexAvg"              => null,
										"indexPlusOneSigma"     => null,
										"indexPlusTwoSigma"     => null,
										"indexPlusThreeSigma"   => null,
										"indexPlusFourSigma"    => null,
										"indexMax"              => null,
										"indexMin"              => null,
										"arrAverageUCLCPK"      => null,
										"arrAverageLCLCPK"      => null,
										"arrPointCPK"			=> null,
										"arrCavityCPK"			=> null,
										"arrWhereCPK"			=> null,
										"arrStdev"				=> null,
					];
				}
		}
		return View("user.graph", $lViewData);
        // return View::make("user/graph", $lViewData);
        // return View("user/graph", $lViewData);
        
	}

	private function getProcessList($pViewData)
	{  
		$lArrObjProcessList   = []; 
		$lArrProcessList      = [];

		$lArrObjProcessList = DB::table('TCODEMST AS PROCESS')
		// ->select('*')
		->select('PROCESS.CODE_ORDER as PROCESS_ID' , 'PROCESS.CODE_NAME as PROCESS_NAME')
		->where('PROCESS.CODE_CLASS' , '=', "002")
		->groupBy('PROCESS.CODE_ORDER')
		->get();

        if(empty($lArrObjProcessList)){
                $lArrProcessList[''] = "";
        }else{
        	foreach ($lArrObjProcessList as $key => $value) {
           		$lArrProcessList[$value->PROCESS_ID] = $value->PROCESS_NAME;
        	}
        }

		$pViewData += [
			"arrDataListProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	private function getCustomerList($pViewData)
	{  
		$lArrObjCustomerList   = []; 
		$lArrCustomerList      = [];

		$lArrObjCustomerList = DB::table('TCUSTOMM AS TCUST')
		->select('TCUST.CUSTOMER_ID as CUSTOMER_ID' , 'TCUST.CUSTOMER_NAME as CUSTOMER_NAME')
		->where('DELETE_FLG', '=', "0")
		->groupBy('CUSTOMER_ID')
		->get();

        if(empty($lArrObjCustomerList)){
                $lArrCustomerList[''] = "";
        }else{
        	foreach ($lArrObjCustomerList as $key => $value) {
           		$lArrCustomerList[$value->CUSTOMER_ID] = $value->CUSTOMER_NAME;
        	}
        }

		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList
		];

		return $pViewData;
	}

	private function getProductNoList($pViewData)
	{  
		$lArrObjProductNoList   = []; 
		$lArrProductNoList      = [];

		$lArrObjProductNoList = DB::select(' 
			SELECT  ANALYSIS.INSPECTION_SHEET_NO AS PRODUCT_NO
			  FROM 	TRESANAL 			AS ANALYSIS
			GROUP BY PRODUCT_NO;
		');
        if(empty($lArrObjProductNoList)){
                $lArrProductNoList[''] = "";
        }else{
        	foreach ($lArrObjProductNoList as $key => $value) {
           		$lArrProductNoList[$value->PRODUCT_NO] = $value->PRODUCT_NO;
        	}
        }

		$pViewData += [
			"arrDataListProductNoList" => $lArrProductNoList
		];

		return $pViewData;
	}

	private function getRevNoList($pViewData)
	{  
		$lArrObjRevNoList   = []; 
		$lArrRevNoList      = [];

		$lArrObjRevNoList = DB::select(' 
			SELECT  ANALYSIS.REV_NO AS REV_NO
			  FROM 	TRESANAL 			AS ANALYSIS
			GROUP BY REV_NO;
		');

        if(empty($lArrObjRevNoList)){
                $lArrRevNoList[''] = "";
        }else{
        	foreach ($lArrObjRevNoList as $key => $value) {
           		$lArrRevNoList[$value->REV_NO] = $value->REV_NO;
        	}
        }

		$pViewData += [
			"arrDataListRevNoList" => $lArrRevNoList
		];

		return $pViewData;
	}

	private function getMoldNoList($pViewData)
	{  
		$lArrObjMoldNoList   = []; 
		$lArrMoldNoList      = [];

		$lArrObjMoldNoList = DB::select(' 
			SELECT  ANALYSIS.MOLD_NO       AS MOLD_NO
			  FROM 	TRESANAL 			AS ANALYSIS 
			GROUP BY MOLD_NO;
		');

        if(empty($lArrObjMoldNoList)){
                $lArrMoldNoList[''] = "";
        }else{
        	foreach ($lArrObjMoldNoList as $key => $value) {
           		$lArrMoldNoList[$value->MOLD_NO] = $value->MOLD_NO;
        	}
        }
		$pViewData += [
			"arrDataListMoldNoList" => $lArrMoldNoList
		];

		return $pViewData;
	}

	private function getTeamNameList($pViewData)
	{  
		$lArrObjTeamNameList   = []; 
		$lArrTeamNameList      = [];

		$lArrObjTeamNameList = DB::table('TRESANAL as ANALYSIS')
		->select('ANALYSIS.TEAM_ID as TEAM_ID' , 'PROCESS.CODE_NAME as TEAM_NAME')
		->join('TCODEMST AS PROCESS', 'ANALYSIS.TEAM_ID', '=', 'PROCESS.CODE_ORDER')
		->where('PROCESS.CODE_CLASS' , '=', "004")
		->groupBy('TEAM_ID')
		->get();
        // echo "<pre>"; print_r($lArrObjTeamNameList); echo "</pre>";   
        if(empty($lArrObjTeamNameList)){
                $lArrTeamNameList[''] = "";
        }else{
        	foreach ($lArrObjTeamNameList as $key => $value) {
           		$lArrTeamNameList[$value->TEAM_ID] = $value->TEAM_NAME;
        	}
        }

		$pViewData += [
			"arrDataListTeamNoList" => $lArrTeamNameList
		];

		return $pViewData;
	}

	private function getMaterialNameList($pViewData)
	{  
		$lArrObjMaterialNameList   = []; 
		$lArrMaterialNameList      = [];

         $lArrObjMaterialNameList = DB::table('TISHEETM as TISHE')
         ->select('TITEM.MATERIAL_NAME as MATERIAL_NAME')
         ->join('TITEMMST as TITEM', 'TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO')
         ->groupBy('MATERIAL_NAME')
         ->get();
        
        if(empty($lArrObjMaterialNameList)){
                $lArrMaterialNameList[''] = "";
        }else{
        	foreach ($lArrObjMaterialNameList as $key => $value) {
           		$lArrMaterialNameList[$value->MATERIAL_NAME] = $value->MATERIAL_NAME;
        	}
        }

		$pViewData += [
			"arrDataListMaterialNameList" => $lArrMaterialNameList
		];

		return $pViewData;
	}

	private function getMaterialSizeList($pViewData)
	{  
		$lArrObjMaterialSizeList   = []; 
		$lArrMaterialSizeList      = [];

        $lArrObjMaterialSizeList = DB::table('TISHEETM as TISHE')
         ->select('TITEM.MATERIAL_SIZE as MATERIAL_SIZE')
         ->join('TITEMMST as TITEM', 'TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO')
         ->groupBy('MATERIAL_SIZE')
         ->get();

        if(empty($lArrObjMaterialSizeList)){
                $lArrMaterialSizeList[''] = "";
        }else{
        	foreach ($lArrObjMaterialSizeList as $key => $value) {
           		$lArrMaterialSizeList[$value->MATERIAL_SIZE] = $value->MATERIAL_SIZE;
        	}
        }

		$pViewData += [
			"arrDataListMaterialSizeList" => $lArrMaterialSizeList
		];

		return $pViewData;
	}

	private function getInspectionDateFromToForGraph($pViewData)
	{  
		if (Input::has('txtInspectionDateFromForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010InspectionDateFromForGraph', Input::get('txtInspectionDateFromForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010InspectionDateFromForGraph', date("d-m-Y",strtotime("-1 month")));
		}

		if (Input::has('txtInspectionDateToForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010InspectionDateToForGraph', Input::get('txtInspectionDateToForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010InspectionDateToForGraph', date("d-m-Y"));
		}

		if (Input::has('txtProductionDateFromForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010ProductionDateFromForGraph', Input::get('txtProductionDateFromForGraph'));
		}
		else
		{
			//if value of screen,write down system date in session
			Session::put('BA1010ProductionDateFromForGraph', date("d-m-Y",strtotime("-1 month")));
		}

		if (Input::has('txtProductionDateToForGraph')) {
			//if value of screen exists,write down in session
			Session::put('BA1010ProductionDateToForGraph', Input::get('txtProductionDateToForGraph'));
		}
		else 
		{
			//if value of screen,write down system date in session
			Session::put('BA1010ProductionDateToForGraph', date("d-m-Y"));
		}

		$pViewData += [
			 "InspectionDateFromForGraph"  => Session::get('BA1010InspectionDateFromForGraph')
			,"InspectionDateToForGraph"  => Session::get('BA1010InspectionDateToForGraph')
			,"ProductionDateFromForGraph"  => Session::get('BA1010InspectionDateFromForGraph')
			,"ProductionDateToForGraph"  => Session::get('BA1010ProductionDateToForGraph')
		];

		return $pViewData;
	}

	private function getConditionList($pViewData)
	{  
		$lArrObjConditionList   = []; 
		$lArrConditionList      = [];

		$lArrObjConditionList = DB::table('TRESANAL as ANALYSIS')
		->select('ANALYSIS.CONDITION_CD as CONDITION_CD', 'PROCESS.CODE_NAME as CONDITION_NAME')
		->join('TCODEMST as PROCESS', 'ANALYSIS.CONDITION_CD', '=', 'PROCESS.CODE_ORDER')
		->where('PROCESS.CODE_CLASS', '=', "001")
		->groupBy('CONDITION_CD')
		->get();

        if(empty($lArrObjConditionList)){
                $lArrConditionList[''] = "";
        }else{
        	foreach ($lArrObjConditionList as $key => $value) {
           		$lArrConditionList[$value->CONDITION_CD] = $value->CONDITION_NAME;
        	}
        }

		$pViewData += [
			"arrDataListConditionList" => $lArrConditionList
		];

		return $pViewData;
	}

	private function getGraphResultList()
	{  
		$lTblSearchResultData          	= []; //data table of inspection result

		$lInspectionDateFromForGraph  	= ""; //date From for search
		$lInspectionDateToForGraph    	= ""; //date To for search
		$ldateStringFrom				= "";
		$ldateStringTo					= "";
		$condiion						= "";
		$lProductionDateFromForGraph    = "";
		$lProductionDateToForGraph    = "";


		if (Input::has('cmbRadioChoosePeriod')){

			$amountOfTime = intval(Input::get('txtTypeTimeForGraph'));
			$ldate  = Carbon::now();
			$ldateNow = $ldate->toDateTimeString();
			$ldatePast = $ldate->subDays($amountOfTime);

			$lInspectionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
            $lProductionDateFromForGraph  = $this->convertDateFormat($ldatePast, "1");
			$lProductionDateToForGraph    = $this->convertDateFormat($ldateNow, "1");	
 
		}else{
			//change English type->Japanese type（because MySQL store as Japanese type）
			$lInspectionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForGraph'), "1");
			$lInspectionDateToForGraph    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForGraph'), "1");
			$lProductionDateFromForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForGraph'), "1");
			$lProductionDateToForGraph  = $this->convertDateFormat((String)Input::get('txtProductionDateToForGraph'), "1");
		}

		if(Input::get('cmbProcessForGraph') != "" AND Input::get('cmbCustomerForGraph') != "" AND Input::get('cmbCheckSheetNoForGraph') != "" AND Input::get('cmbRevisionNoForGraph') AND Input::get('cmbMoldNoForGraph') != "" AND Input::get('cmbTeamNameForGraph') != "" AND Input::get('cmbMaterialNameForGraph') != "" AND Input::get('cmbMaterialSizeForGraph') != "" AND Input::get('cmbConditionForGraph') != "" AND $lInspectionDateFromForGraph != "" AND $lInspectionDateToForGraph != "" AND $lProductionDateFromForGraph != "" AND $lProductionDateToForGraph != "" AND Input::get('cmbSelectTypeForGraph') != ""){

        $lTblSearchResultData = DB::table('TRESANAL as ANALYSIS')
        ->select('ANALYSIS.INSPECTION_SHEET_NO as PRODUCT_NO'
		        	,'ANALYSIS.REV_NO as REV_NO'
		        	,'PROCESS.CODE_NAME as PROCESS_NAME'
		        	, DB::raw('DATE_FORMAT(ANALYSIS.INSPECTION_YMD,"%d/%m/%Y") as INSPECTION_YMD')
		        	,'TIMEE.INSPECTION_TIME_ID as INSPECTION_TIME_ID'
		        	,'TIMEE.INSPECTION_TIME_NAME as INSPECTION_TIME_NAME'
		        	, DB::raw('CASE WHEN ANALYSIS.CONDITION_CD = "01" THEN "N" WHEN ANALYSIS.CONDITION_CD = "02" THEN "Ab" ELSE "" END CONDITION_CD')
		        	, DB::raw('CASE WHEN ANALYSIS.CONDITION_CD = "01" THEN "Normal" WHEN ANALYSIS.CONDITION_CD = "02" THEN "Abnormal" ELSE "" END CONDITION_CD_NAME')
		        	,'ANALYSIS.MOLD_NO' 
					,'ANALYSIS.MOLD_NO as MOLD_NAME'
					,'ANALYSIS.TEAM_ID'
					,'TEAM.CODE_NAME as TEAM_NAME'
					,'ANALYSIS.INSPECTION_POINT as INSPECTION_POINT'
					,'ANALYSIS.ANALYTICAL_GRP_01 as CAVITY_NO'
		 		    ,'ANALYSIS.ANALYTICAL_GRP_02 as WHERE_NO'
		 		    ,'ANALYSIS.STD_MIN_SPEC'
		 		    ,'ANALYSIS.STD_MAX_SPEC'							 
					,'ANALYSIS.PICTURE_URL'
		 		   	,'ANALYSIS.MIN_VALUE'
		 		   	,'ANALYSIS.MAX_VALUE'
					,'ANALYSIS.AVERAGE_VALUE as AVERAGE_VALUE'
					,'ANALYSIS.RANGE_VALUE as RANGE_VALUE'
					,'ANALYSIS.STDEV'
					,'ANALYSIS.CPK as CPK'
		 		   	,'ANALYSIS.XBAR_UCL'									
		 		  	,'ANALYSIS.XBAR_LCL'
		 		  	,'ANALYSIS.RCHART_UCL'									
		 		  	,'ANALYSIS.RCHART_LCL'
		 			,'ANALYSIS.JUDGE_REASON'
		 			,'TRESH.LOT_NO'
		 			, DB::raw('DATE_FORMAT(TRESH.PRODUCTION_YMD,"%d/%m/%Y") as PRODUCTION_YMD')
        )
        ->join('TRESDETT as TRESD', function ($join) {
			$join->on('ANALYSIS.INSPECTION_RESULT_NO', '=', 'TRESD.INSPECTION_RESULT_NO')
			     ->on('ANALYSIS.INSPECTION_POINT', '=', 'TRESD.INSPECTION_POINT')
			     ->on('ANALYSIS.ANALYTICAL_GRP_01', '=', 'TRESD.ANALYTICAL_GRP_01')
			     ->on('ANALYSIS.ANALYTICAL_GRP_02', '=', 'TRESD.ANALYTICAL_GRP_02');
		 })
		->join('TRESHEDT as TRESH', function ($join) {
			$join->on('TRESD.INSPECTION_RESULT_NO', '=', 'TRESH.INSPECTION_RESULT_NO');
		})	
		->join('TCODEMST as PROCESS', function ($join) {
			$join->on('ANALYSIS.PROCESS_ID', '=', 'PROCESS.CODE_ORDER')
			     ->where('PROCESS.CODE_CLASS', '=', '002');
		})	
		->join('TINSPTIM as TIMEE', function ($join) {
			$join->on('ANALYSIS.INSPECTION_TIME_ID', '=', 'TIMEE.INSPECTION_TIME_ID');
		})
		->join('TCODEMST as TEAM', function ($join) {
			$join->on('ANALYSIS.TEAM_ID', '=', 'TEAM.CODE_ORDER')
			     ->where('TEAM.CODE_CLASS', '=', '004');
		})	
		->join('TINSPNOM as TINSP', function ($join) {
			$join->on('ANALYSIS.INSPECTION_SHEET_NO', '=', 'TINSP.INSPECTION_SHEET_NO')
			     ->on('ANALYSIS.REV_NO', '=', 'TINSP.REV_NO')
			     ->on('ANALYSIS.INSPECTION_POINT', '=', 'TINSP.INSPECTION_POINT');
		})
		->join('TINSNTMM as TINSN', function ($join) {
			$join->on('ANALYSIS.INSPECTION_SHEET_NO', '=', 'TINSN.INSPECTION_SHEET_NO')
				 ->on('ANALYSIS.INSPECTION_POINT', '=', 'TINSN.INSPECTION_POINT')
				 ->on('ANALYSIS.INSPECTION_TIME_ID', '=', 'TINSN.INSPECTION_TIME_ID');
		})
		->join('TISHEETM as TISHE', function ($join) {
			$join->on('ANALYSIS.INSPECTION_SHEET_NO', '=', 'TISHE.INSPECTION_SHEET_NO')
			     ->on('ANALYSIS.REV_NO', '=', 'TISHE.REV_NO');
		})
		->join('TITEMMST as TITEM', function ($join) {
			$join->on('TISHE.ITEM_NO', '=', 'TITEM.ITEM_NO');
		})
        ->where('TRESD.INSPECTION_RESULT_TYPE', '!=', "1")
        ->where('TRESD.INSPECTION_RESULT_VALUE', '!=', "")
        ->where('TINSN.GRAPH_OUTPUT_FLG', '=', "1")
        ->where('ANALYSIS.PROCESS_ID', '=', TRIM((String)Input::get('cmbProcessForGraph')))
        ->where('TISHE.CUSTOMER_ID', '=', TRIM((String)Input::get('cmbCustomerForGraph')))
        ->where('ANALYSIS.INSPECTION_SHEET_NO', '=', TRIM((String)Input::get('cmbCheckSheetNoForGraph')))
        ->where('ANALYSIS.REV_NO', '=', TRIM((String)Input::get('cmbRevisionNoForGraph')))
        ->where('ANALYSIS.MOLD_NO', '=', TRIM((String)Input::get('cmbMoldNoForGraph')))
        ->where('ANALYSIS.TEAM_ID', '=', TRIM((String)Input::get('cmbTeamNameForGraph')))
        ->where('TITEM.MATERIAL_NAME', '=', TRIM((String)Input::get('cmbMaterialNameForGraph')))
        ->where('TITEM.MATERIAL_SIZE', '=', TRIM((String)Input::get('cmbMaterialSizeForGraph')))
        ->where('ANALYSIS.CONDITION_CD', '=', TRIM((String)Input::get('cmbConditionForGraph')))
        ->whereBetween('ANALYSIS.INSPECTION_YMD', array($lInspectionDateFromForGraph, $lInspectionDateToForGraph))
        // ->whereBetween('TRESH.PRODUCTION_YMD', array($lProductionDateFromForGraph, $lProductionDateToForGraph))
        ->groupBy('ANALYSIS.PROCESS_ID')
        ->groupBy('TISHE.CUSTOMER_ID')
        ->groupBy('ANALYSIS.INSPECTION_SHEET_NO')
        ->groupBy('ANALYSIS.REV_NO')
        ->groupBy('ANALYSIS.MOLD_NO')
        ->groupBy('ANALYSIS.TEAM_ID')
        ->groupBy('ANALYSIS.CONDITION_CD')
        ->get();
			}else{
	        	    $lTblSearchResultData = [];
	        }
		return $lTblSearchResultData;
	}

	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{

		//not to entry date
		if ($pTargetDate == "") {
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1") {
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');

	}

	private function setListForms($pViewData)
	{

		if (Input::has('cmbProcessForGraph')) {
			Session::put('BA1010ProcessForGraph', Input::get('cmbProcessForGraph'));
		}
		else
		{
			Session::put('BA1010ProcessForGraph', "");
		}

		$pViewData += [
				"ProcessForGraph"  => Session::get('BA1010ProcessForGraph')
		];

		if (Input::has('cmbCustomerForGraph')) {
			Session::put('BA1010CustomerForGraph', Input::get('cmbCustomerForGraph'));
		}
		else 
		{
			Session::put('BA1010CustomerForGraph', "");
		}
		
		$pViewData += [
				"CustomerForGraph"  => Session::get('BA1010CustomerForGraph')
		];

		if (Input::has('cmbCheckSheetNoForGraph')) {
			Session::put('BA1010ProductNoForGraphForGraph', Input::get('cmbCheckSheetNoForGraph'));
		}
		else 
		{
			Session::put('BA1010ProductNoForGraphForGraph', "");
		}
		$pViewData += [
				"ProductNoForGraph"  => Session::get('BA1010ProductNoForGraphForGraph')
		];

		if (Input::has('cmbRevisionNoForGraph')) {
			Session::put('BA1010RevNoForGraphForGraph', Input::get('cmbRevisionNoForGraph'));
		}
		else 
		{
			Session::put('BA1010RevNoForGraphForGraph', "");
		}
		$pViewData += [
				"RevNoForGraph"  => Session::get('BA1010RevNoForGraphForGraph')
		];

		if (Input::has('cmbMoldNoForGraph')) {
			Session::put('BA1010MoldNoForGraphForGraph', Input::get('cmbMoldNoForGraph'));
		}
		else 
		{
			Session::put('BA1010MoldNoForGraphForGraph', "");
		}
		$pViewData += [
				"MoldNoForGraph"  => Session::get('BA1010MoldNoForGraphForGraph')
		];
		
		if (Input::has('cmbTeamNameForGraph')) {
			Session::put('BA1010TeamNoForGraphForGraph', Input::get('cmbTeamNameForGraph'));
		}
		else 
		{
			Session::put('BA1010TeamNoForGraphForGraph', "");
		}
		$pViewData += [
				"TeamNameForGraph"  => Session::get('BA1010TeamNoForGraphForGraph')
		];
		

		if (Input::has('cmbMaterialNameForGraph')) {
			Session::put('BA1010MaterialNameForGraph', Input::get('cmbMaterialNameForGraph'));
		}
		else 
		{
			Session::put('BA1010MaterialNameForGraph', "");
		}
		$pViewData += [
				"MaterialNameForGraph"  => Session::get('BA1010MaterialNameForGraph')
		];

		
		if (Input::has('cmbMaterialSizeForGraph')) {
			Session::put('BA1010MaterialSizeForGraph', Input::get('cmbMaterialSizeForGraph'));
		}
		else
		{
			Session::put('BA1010MaterialSizeForGraph', "");
		}

		$pViewData += [
				"MaterialSizeForGraph"  => Session::get('BA1010MaterialSizeForGraph')
		];
	
		if (Input::has('cmbConditionForGraph')) {
			Session::put('BA1010ConditionForGraph', Input::get('cmbConditionForGraph'));
		}
		else
		{
			Session::put('BA1010ConditionForGraph', "");
		}

		$pViewData += [
				"ConditionForGraph"  => Session::get('BA1010ConditionForGraph')
		];
		return $pViewData;
	}

	private function getBellCurve($STDEV_VALUE,$AVERAGE_VALUE){
		$Min5Sigma = $AVERAGE_VALUE - (5 * $STDEV_VALUE);
		$Max5Sigma = $AVERAGE_VALUE + (5 * $STDEV_VALUE);
		$Min4Sigma = $AVERAGE_VALUE - (4 * $STDEV_VALUE);
		$Max4Sigma = $AVERAGE_VALUE + (4 * $STDEV_VALUE);
		$Min3Sigma = $AVERAGE_VALUE - (3 * $STDEV_VALUE);
		$Max3Sigma = $AVERAGE_VALUE + (3 * $STDEV_VALUE);
		$Min2Sigma = $AVERAGE_VALUE - (2 * $STDEV_VALUE);
		$Max2Sigma = $AVERAGE_VALUE + (2 * $STDEV_VALUE);
		$MinSigma = $AVERAGE_VALUE - $STDEV_VALUE;
		$MaxSigma = $AVERAGE_VALUE + $STDEV_VALUE;
        
        $bar = 0.5;
 		//-5sigma to < -4sigma
		for($i = $Min5Sigma; $i < $Min4Sigma; $i = $i + $bar){
			
			$valueX5[] = $i;
			$valueY5[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine5Sigma = [];
		foreach($valueX5 as $key => $value) {
			    $valueLine5Sigma[$key] = [$valueX5[$key],$valueY5[$key]];
		}

        //-4sigma to < -3sigma
		for($i = $Min4Sigma; $i < $Min3Sigma; $i = $i + $bar){
			
			$valueX4[] = $i;
			$valueY4[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine4Sigma = [];
		foreach($valueX4 as $key => $value) {
			    $valueLine4Sigma[$key] = [$valueX4[$key],$valueY4[$key]];
		}
        
        //-3sigma to < -2sigma
        for($i = $Min3Sigma; $i < $Min2Sigma; $i = $i + $bar){
			
			$valueX3[] = $i;
			$valueY3[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine3Sigma = [];
		foreach($valueX3 as $key => $value) {
			    $valueLine3Sigma[$key] = [$valueX3[$key],$valueY3[$key]];
		}

		//-2sigma to < -sigma
 		for($i = $Min2Sigma; $i < $MinSigma; $i = $i + $bar){
			
			$valueX2[] = $i;
			$valueY2[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLine2Sigma = [];
		foreach($valueX2 as $key => $value) {
			    $valueLine2Sigma[$key] = [$valueX2[$key],$valueY2[$key]];
		}

		//-sigma to < average
		for($i = $MinSigma; $i < $AVERAGE_VALUE; $i = $i + $bar){
			
			$valueX[] = $i;
			$valueY[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}

		$valueLineSigma = [];
		foreach($valueX as $key => $value) {
			    $valueLineSigma[$key] = [$valueX[$key],$valueY[$key]];
		}

		//average
		$valueYAvergage = number_format($this->normal($AVERAGE_VALUE, $AVERAGE_VALUE, $STDEV_VALUE),3);
		$valueLineAvergage = array(
			  array($AVERAGE_VALUE,$valueYAvergage)
			  );

		//>average to +sigma
		for($i = $MaxSigma; $i > $AVERAGE_VALUE; $i -= $bar){
			
			$valueXRight[] = $i;
			$valueYRight[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlusSigma = [];
		foreach($valueXRight as $key => $value) {
			    $valueLinePlusSigma[$key] = [$valueXRight[$key],$valueYRight[$key]];
		}
		krsort($valueLinePlusSigma);

		//>+sigma to +2sigma
		for($i = $Max2Sigma; $i > $MaxSigma; $i -= $bar){
			
			$valueX2Right[] = $i;
			$valueY2Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus2Sigma = [];
		foreach($valueX2Right as $key => $value) {
			    $valueLinePlus2Sigma[$key] = [$valueX2Right[$key],$valueY2Right[$key]];
		}
		krsort($valueLinePlus2Sigma);

		//>+2sigma to +3sigma
		for($i = $Max3Sigma; $i > $Max2Sigma; $i -= $bar){
			
			$valueX3Right[] = $i;
			$valueY3Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus3Sigma = [];
		foreach($valueX3Right as $key => $value) {
			    $valueLinePlus3Sigma[$key] = [$valueX3Right[$key],$valueY3Right[$key]];
		}
		krsort($valueLinePlus3Sigma);

		//>+3sigma to +4sigma
		for($i = $Max4Sigma; $i > $Max3Sigma; $i -= $bar){
			
			$valueX4Right[] = $i;
			$valueY4Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus4Sigma = [];

		foreach($valueX4Right as $key => $value) {
			    $valueLinePlus4Sigma[$key] = [$valueX4Right[$key],$valueY4Right[$key]];
		}
		krsort($valueLinePlus4Sigma);

		//>+4sigma to +5sigma
		for($i = $Max5Sigma; $i > $Max4Sigma; $i -= $bar){
			
			$valueX5Right[] = $i;
			$valueY5Right[] = number_format($this->normal($i, $AVERAGE_VALUE, $STDEV_VALUE),3);
		}
		$valueLinePlus5Sigma = [];

		foreach($valueX5Right as $key => $value) {
			    $valueLinePlus5Sigma[$key] = [$valueX5Right[$key],$valueY5Right[$key]];
		}
		krsort($valueLinePlus5Sigma);

		$valueLine = array_merge($valueLine5Sigma, $valueLine4Sigma, $valueLine3Sigma, $valueLine2Sigma, $valueLineSigma, $valueLineAvergage, $valueLinePlusSigma, $valueLinePlus2Sigma, $valueLinePlus3Sigma, $valueLinePlus4Sigma, $valueLinePlus5Sigma);
		 return $valueLine;
	}

	private function normal($x, $mu, $sigma) {
	    return exp(-0.5 * ($x - $mu) * ($x - $mu) / ($sigma*$sigma))
	        / ($sigma * sqrt(2.0 * M_PI));
	}

	private function fourDimentionToTwoDimention($fourDimention){

		foreach ($fourDimention as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$fourDimentionFillPoint[$key][$key1][$key2][0] = $value3;
						$fourDimentionFillPoint[$key][$key1][$key2][] = $value3;
					}
				$fourDimentionFillPoint[$key][$key1][$key2][] = $value3;
				}
			}
		}

  		foreach ($fourDimentionFillPoint as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrTwoDimention[] = $value2;
				}
			}
		}
		return $arrTwoDimention;
	}
    
    private function getArrPointOnLineChart($arrValue){
    	foreach ($arrValue as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrPoint[] = $value2;
				}
			}
		}
		foreach ($arrPoint as $key => $value) {
			foreach ($value as $k => $val) {
				$arrPointOnLineChart[$key][0] = null;
				$arrPointOnLineChart[$key][] = $val;
			}
			$arrPointOnLineChart[$key][] = null;
		}
        return $arrPointOnLineChart;
    }

	private function arrLimitUCLOrLCL($coefficient, $averageOfChart){
       
	    foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] = (double)$value5 * $averageOfChart[$key][$key1][$key2][$key3];
							}
						}
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrMaxOfChart($coefficient , $averageOfChart){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								 $arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageOfChart[$key][$key1][$key2][$key3]) + (double)0.001;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrMinOfChart($coefficient , $averageOfChart){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageOfChart[$key][$key1][$key2][$key3]) - (double)0.001;
							}
							
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function coefficientObject($CODE_KIND, $arrayValue){
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$coefficientObject[$key][$key1][$key2][$key3] = (array)$this->getCoefficient($CODE_KIND, count($value2));
					}
				}
			}
		}

		return $coefficientObject;
	}

	private function getCoefficient($CODE_KIND, $CODE_NUM) {
		if( $CODE_NUM > 25 ){ $CODE_NUM == 25;}
		if( $CODE_NUM < 2 ) { $CODE_NUM == 1; }

		$coefficient = DB::table('tccientmst')
		->select('COEFFICIENT AS 0')
		// ->where('CODE_KIND', '=', $CODE_KIND)
		->where('COEFF_KIND', '=', $CODE_KIND)
		// ->where('CODE_NUM', '=', $CODE_NUM)
		->where('COEFF_NUM', '=', $CODE_NUM)
		->get();

		return $coefficient;
	}

	private function arrAverageValue($arrayValue){
		foreach ($arrayValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						$arrAverageValue[$key][$key1][$key2][$key3] = array_sum($value2) / count($value2);
					}
				}
			}
		}
		return $arrAverageValue;
	}



	private function arrLimitUCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}
		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] = ((double)$value5 * $averageRange[$key][$key1][$key2][$key3]) + $averageAverage[$key][$key1][$key2][$key3];
							}
						}
					}
				}
			}
		}
        return $arrUCL;
	}

	private function arrLimitLCLXbar($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrUCL[$key][$key1][$key2][$key3] =  $averageAverage[$key][$key1][$key2][$key3] - ((double)$value5 * $averageRange[$key][$key1][$key2][$key3]);
							}
						}
					}
				}
			}
		}

        return $arrUCL;
	}

	private function arrMaxOfXbarChart($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ((double)$value5 * $averageRange[$key][$key1][$key2][$key3]) + $averageAverage[$key][$key1][$key2][$key3] + (double)0.01;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrMinOfXbarChart($coefficient, $averageRange, $averageAverage){
		foreach ($coefficient as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							$coefficientArray[$key][$key1][$key2][$key3] = $value4;
						}
					}
				}
			}
		}

		foreach ($coefficientArray as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						foreach ($value3 as $key4 => $value4) {
							foreach ($value4 as $key5 => $value5) {
								$arrCoefficient[$key][$key1][$key2][$key3] = ($averageAverage[$key][$key1][$key2][$key3] - ((double)$value5 * $averageRange[$key][$key1][$key2][$key3])) - (double)0.01;
							}
						}
					}
				}
			}
		}
		
        return $arrCoefficient;
	}

	private function arrThreeToOneDimention($arrThreeDimentionValue){

		foreach ($arrThreeDimentionValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					foreach ($value2 as $key3 => $value3) {
						if($key3 == 0){
							$arrOneDimentionValue[] = $value3;
						}
					}							
				}
			}
		}
        return $arrOneDimentionValue;
        
	}

	private function getLabelForMerge($valueLabel, $name){

		foreach ($valueLabel as $key => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $key => $value2) {
					$arrForGroup[] = $value2;
				}
			}
		}
		foreach ($arrForGroup as $key => $value) {
			$arrForGraph[$key][0] = $name;
			foreach ($value as $k => $val) {
				$arrForGraph[$key][] = $val;
			}
			$arrForGraph[$key][] = "";
		}
		foreach ($arrForGraph as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$arrForMerge["group".$key]["graph".$key]["key".$key1] = [$value1];
			}
		}
        return $arrForMerge;

	}

	private function getThreeToTwoDimention($arrValue, $k){

		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == $k){
						$arrTwoDimention[$key][] = $value2;  
					}
				}
			}
		}

		return $arrTwoDimention;
	}

	private function editThreeDimention($arrValue){
		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == 0){
						$arrEdit[$key][$key1]["x"] = $value2;  
					}
					if($key2 == 1){
						$arrEdit[$key][$key1]["y"] = $value2;  
					}
				}
			}
		}
		return $arrEdit;
	}

	private function essadtest($arrValue){
		foreach ($arrValue as $key => $value) {
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					if($key2 == 0){
						$arrEdit[$key][$key1]["x"] = $value2;  
					}
					if($key2 == 1){
						$arrEdit[$key][$key1]["y"] = $value2;  
					}
				}
			}
		}
		return $arrEdit;
	}
}	