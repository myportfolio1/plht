<?php

use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';

class ZA1010MasterController
extends Controller
{
	
	//DELIMITER to receive value for making user table for Laravel from User Master
	//it is easier than trigger because of DB structure
	CONST DELIMITER = "|#|#|";
	
	//**************************************************************************
	// processing name    MasterAction
	// over view      display initial master upload/download
	//           separate processing depending on Data Download,Data Upload button
	//           do processing as corresponding
	// parameter      nothing
	// returned value    nothing
	// programer    s-miyamoto
	// date    2014.06.18
	// update  2014.06.18 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	public function MasterAction()
	{
		$lViewData            = [];                  //to transport data to screen
		
		$lMasterCode          = "";                  //code value of selected Master in MasterList
		
		$lTemplateFileName    = "";                  //file name of ExcelTemplate
		
		$lTblMasterData       = [];                  //DataTable of selected Master
		$lRowMasterData       = [];                  //DataRow of selected Master
		$lCelMasterData       = [];                  //DataCell of selected Master
		
		$lExcelRowCount       = 0;                   //Excel row count
		$lExcelColCount       = 0;                   //Excel colum count
		
		$lExcelCellAddress    = "";                  //Excel Cell number（A1,B2,etc）
		
		$lSaveFileName        = "";                  //saved file name
		$lSaveFilePathAndName = "";                  //saved file pass + name
		
		$lCellValue           = "";                  //value in Excel cell（value will be Add/Modify/Delete/Skip）
		$lSQLValue            = "";                  //SQL from Excel
		$lSQLValueList        = [];                  //array to add SQL
		
		$lArrValue            = [];                  //array of "query+user ID+user name+password"for User master
		$lLaravelUserMSQL     = "";                  //SQL for Laravel User master
		
		//receive parameter from login screen through Session and issue to array for transportion to screen
		$lViewData += [
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];

		//-----------------------------
		//set customer combo for Search
		//-----------------------------
		$lViewData = $this->setCustomerList($lViewData);

		//set value in view data
		$lViewData += [
			"CustomerForSearch" => (String)Input::get('cmbCustomerForSearch'),
		];

		//in case download button is selected
		if (   Input::has('btnDownload') 
		    or Input::has('btnDownloadIns')
		    or Input::has('btnDownloadUpd')
		    or Input::has('btnDownloadDel')
		   )       //Download button
		{
			
			//log
			Log::write('info', 'Download Button Click.', 
				[
					"Master Name"           => Input::get('cmbMasterName' ,''),
					"Proc OK Flg"           => Input::get('hidProcOKFlg'  ,''),
				]
			);
			
			//if OK is selected in JavaScript dialogue
			if ((String)Input::get('hidProcOKFlg') == "OK")
			{
				//must check Master Name
				$lViewData = $this->checkMasterName($lViewData);
				
				//in case of no error,continue processing
				if (array_key_exists("errors", $lViewData) == false)
				{
					//get Code value per Master(reference:master.blade.php)
					$lMasterCode = Input::get('cmbMasterName');
					
					//specify templete file name corresponding
					$lTemplateFileName = $this->getTemplateFileName($lMasterCode);
					
					//get corresponding master data
					$lTblMasterData = $this->getMasterData($lMasterCode);
					
					//make Excel file
					$objReader = new PHPExcel_Reader_Excel2007();
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileName);
					
					//activate line0 in sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					//get sheet activated
					$objSheet = $objPHPExcel->getActiveSheet();
					
					//value starts row5, colum5 in templete
					//★caution! This is PHPExcel's rule that line number starts 1,colum number starts 0★
					$lExcelRowCount = 5;
					$lExcelColCount = 4;
					
					//make Validation
					//make select cell of Skip,Add,Modify,Delete
					$objValidation = $objPHPExcel->getActiveSheet()->getCell("C5")->getDataValidation();
					$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
					$objValidation->setShowDropDown(true);

					//-----------------
					//separate drop down list depending on button
					if (Input::has('btnDownload'))
					{
						$objValidation->setFormula1('"Skip,Add,Modify,Delete"');
					}
					else if (Input::has('btnDownloadIns'))
					{
						$objValidation->setFormula1('"Skip,Add"');
					}
					else if (Input::has('btnDownloadUpd'))
					{
						$objValidation->setFormula1('"Skip,Modify"');
					}
					else 
					{
						$objValidation->setFormula1('"Skip,Delete"');
					}

					//write down data while got data is being looped
					foreach ($lTblMasterData As $lRowMasterData) {
					
						//there are n rows in 1line(differs dipending on master)
						foreach ($lRowMasterData As $lCelMasterData) {
							
							//write down as string type
							$objSheet->getCellByColumnAndRow($lExcelColCount, $lExcelRowCount)->setValueExplicit($lCelMasterData, PHPExcel_Cell_DataType::TYPE_STRING);
							
							//increase colum count
							$lExcelColCount += 1;
						}
						
						//set Skip
						$objSheet->setCellValueByColumnAndRow(2, $lExcelRowCount, "Skip");
						
						//make string corresponding to cell
						$lExcelCellAddress = "C".(String)$lExcelRowCount;
						
						//write down setting maked beforehand
						$objPHPExcel->getActiveSheet()->getCell($lExcelCellAddress)->setDataValidation($objValidation);
						
						//increase row count
						$lExcelRowCount += 1;
						
						//colum count returns to the top colum count
						$lExcelColCount = 4;
						
					}
					
					//set saved file name and pass("templete"in templete file name exchange to present "year,month,day_hour,minit,second") 
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileName);
					$lSaveFilePathAndName = "excel/".$lSaveFileName;
					
					//save file(in server)
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//when there is a lot of data,SAVE processing takes very long time.
					$objWriter->save($lSaveFilePathAndName);
					
					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//in case work folder does not exist, make work folder
					if (file_exists('excel/temp') == false)
					{
						mkdir('excel/temp');
					}

					//transport file to temp folder(rename filepass to excel/temp/XXXX)
					if (
						rename($lSaveFilePathAndName
						       ,str_replace("excel/", "excel/temp/", $lSaveFilePathAndName)
						      )
						)
					{
						//finishing message
						$lViewData["NormalMessage"] = "I005 : Process has been completed.";
					}
				}
			
			}
		}
		elseif (Input::has('btnUpload'))     //Upload button
		{
			
			//log
			Log::write('info', 'Upload Button Click.', 
				[
					"Master Name"           => Input::get('cmbMasterName'  ,''),
					"Proc OK Flg"           => Input::get('hidProcOKFlg'  ,''),
				]
			);
			
			//if OK is selected in JavaScript dialogue
			if ((String)Input::get('hidProcOKFlg') == "OK")
			{
			
				//must check Master Name
				$lViewData = $this->checkMasterName($lViewData);
				
				//in case of no error
				if (array_key_exists("errors", $lViewData) == false)
				{
					//get Code value per Master(reference:master.blade.php)
					$lMasterCode = Input::get('cmbMasterName');
					
					//specify templete file name corresponding
					$lTemplateFileName = $this->getTemplateFileName($lMasterCode);
					
					//must check Upload File and file name
					$lViewData = $this->checkUploadFile($lViewData, $lTemplateFileName);
				}
				
				//in case of no error
				if (array_key_exists("errors", $lViewData) == false)
				{
					//make Excel file
					$objReader = new PHPExcel_Reader_Excel2007();
					$objPHPExcel = $objReader->load(Input::file('filUploadFile')->getRealPath());
					
					//activate line0 in sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					//get sheet activated
					$objSheet = $objPHPExcel->getActiveSheet();
					
					//"Add/Modify/Delete/Skip" starts from line5 ,colum3 in templete
					//★caution! This is PHPExcel's rule that line number starts 1,colum number starts 0★
					$lExcelRowCount = 5;
					$lExcelColCount = 2;
					
					//repeat until column writen processing will be blank
					for ($lExcelRowCount = 5; $lExcelRowCount < 500000; $lExcelRowCount++) {
					
						$lCellValue = $objSheet->getCellByColumnAndRow($lExcelColCount, $lExcelRowCount)->getValue();
						
						if ($lCellValue == "") {
							
							//finish at finding blank cell
							break;
							
						}
						elseif ($lCellValue == "Add") {
							
							//get value of cell including query for Add,Modify
							$lSQLValue = $objSheet->getCellByColumnAndRow($lExcelColCount - 2, $lExcelRowCount)->getCalculatedValue();

							//in case value include DELIMITER
							if (strpos($lSQLValue, self::DELIMITER) == true)
							{
								//because「SQL＋delimiter＋user ID＋delimiter＋name＋delimiter＋password」is stored
								//separate ordinary query and query to update user master for Laravel,store
								$lArrValue = explode(self::DELIMITER, $lSQLValue);
								
								//add ordinary query to array
								array_push($lSQLValueList, $lArrValue[0]);
								
								//set query for entry
								$lLaravelUserMSQL = 
								"INSERT INTO USER (USER_ID, USER_NAME, PASSWORD, EMAIL, CREATED_AT, UPDATED_AT)
								VALUES ('". $lArrValue[1] ."', '". $lArrValue[2] ."', '". Hash::make($lArrValue[3]) ."', '', now(), now())";
								
								//add to array
								array_push($lSQLValueList, $lLaravelUserMSQL);
								
							
							}
							else
							{
								//in case value does not include DELIMITER
								//add to array
								array_push($lSQLValueList, $lSQLValue);
								
							}
						}
						elseif ($lCellValue == "Modify") {
							
							//get value of cell including query for Add,Modify
							$lSQLValue = $objSheet->getCellByColumnAndRow($lExcelColCount - 2, $lExcelRowCount)->getCalculatedValue();
							
							//in case value includes DELIMITER
							if (strpos($lSQLValue, self::DELIMITER) == true)
							{
								//because 「SQL＋delimiter＋user ID＋delimiter＋name＋delimiter＋password」is stored
								//separate ordinary query and query to update usermaster for Laravel,store
								$lArrValue = explode(self::DELIMITER, $lSQLValue);
								
								//add ordinary query to array
								array_push($lSQLValueList, $lArrValue[0]);
								
								//set query to update
								$lLaravelUserMSQL = 
								"UPDATE USER SET USER_NAME = '". $lArrValue[2] ."', PASSWORD = '". Hash::make($lArrValue[3]) ."', UPDATED_AT = now() WHERE USER_ID = '". $lArrValue[1] ."'";
								
								//add to array
								array_push($lSQLValueList, $lLaravelUserMSQL);
								
							}
							else
							{
								//in case value does not include DELIMITER
								//add to array
								array_push($lSQLValueList, $lSQLValue);
								
							}
						}
						elseif ($lCellValue == "Delete") {
							
							//get value of cell including query for Delete
							$lSQLValue = $objSheet->getCellByColumnAndRow($lExcelColCount - 1, $lExcelRowCount)->getCalculatedValue();
							
							//in case value includes DELIMITER
							if (strpos($lSQLValue, self::DELIMITER) == true)
							{
							
								//because it is stored「SQL＋delimiter＋user ID＋delimiter＋name＋delimiter＋password」
								//separate ordinary query and query to update user master for Laravel,store
								$lArrValue = explode(self::DELIMITER, $lSQLValue);
								
								//add ordinary query to array
								array_push($lSQLValueList, $lArrValue[0]);
								
								//set query to delete
								$lLaravelUserMSQL = 
								"DELETE FROM USER WHERE USER_ID = '". $lArrValue[1] ."'";
								
								//add to array
								array_push($lSQLValueList, $lLaravelUserMSQL);
								
							}
							else
							{
								//in case value does not include "DELIMITER"
								//add to array
								array_push($lSQLValueList, $lSQLValue);
								
							}
						}
					}
					
					//start transaction
					//1 transaction
					DB::transaction(function() use ($lSQLValueList)
					{
						//until arra of SQL run out
						
						foreach ($lSQLValueList As $lSQLValue) {

							DB::statement($lSQLValue);
							
						}
						
					});
					
					//★　when Upload and getting Temp,the remains will leave
					
					//finishing message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";
					
				}
			}
		}

		//-----------------------------
		//set Model combo
		//-----------------------------
		if (Input::has('cmbCustomerForSearch'))
		{
			//in case value in customer information for search exists
			$lViewData = $this->setModelList($lViewData,Input::get('cmbCustomerForSearch'));
		}
		else
		{
			//in case value in customer information for search does not exist
			$lViewData = $this->setModelList($lViewData,"");
		}

		//store entry value in screen
		$lViewData += [
			"MasterName"         => Input::get('cmbMasterName',''),
			"CustomerForSearch"  => Input::get('cmbCustomerForSearch',''),
			"ModelForSearch"     => Input::get('cmbModelForSearch',''),
		];
		
		return View::make("user/master", $lViewData);
		
	}
	
	//**************************************************************************
	// processing name    checkMasterName
	// over view      Master Nameの必須選択チェック
	// parameter      画面返却用配列
	// returned value    画面返却用配列
	// programer    s-miyamoto
	// date    2014.07.24
	// update  2014.07.24 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function checkMasterName($pViewData)
	{
		//Master Name必須選択チェック
		$lValidator = Validator::make(
			array('cmbMasterName' => Input::get('cmbMasterName')),
			array('cmbMasterName' => array('required'))
		);
		
		//エラーの場合
		if ($lValidator->fails()) {
		
				//エラーメッセージ設定
				$pViewData["errors"] = new MessageBag([
					"error" => "E027 : Select Master Name."
				]);
				
				return $pViewData;
		}
		
		return $pViewData;
	}
	
	//**************************************************************************
	// processing name    checkUploadFile
	// over view      アップロードするファイルの必須チェック、ファイル名チェック
	// parameter      画面返却用配列、テンプレートファイル名
	// returned value    画面返却用配列
	// programer    s-miyamoto
	// date    2014.07.25
	// update  2014.07.25 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function checkUploadFile($pViewData, $pTemplateFileName)
	{
		$lTemplateFileNameHead = "";   //テンプレートファイル名の「_template」より前の部分
		$lUploadFileName       = "";   //Uploadファイル名
		
		//Upload File必須選択チェック
		$lValidator = Validator::make(
			array('filUploadFile' => Input::file('filUploadFile')),
			array('filUploadFile' => array('required'))
		);
		//エラーの場合
		if ($lValidator->fails()) {
		
				//エラーメッセージ設定
				$pViewData["errors"] = new MessageBag([
					"error" => "E029 : Select Upload File."
				]);
				
				return $pViewData;
		}
		
		//テンプレートファイル名の、「_template」より前の部分を切り出す
		$lTemplateFileNameHead = substr($pTemplateFileName, 0, strpos($pTemplateFileName, "_template"));
		
		//アップロードするファイル名を取得
		$lUploadFileName = Input::file('filUploadFile')->getClientOriginalName();
		
		//Upload File名の先頭が、テンプレートファイル名の先頭と一致すること。
		if (strncmp($lTemplateFileNameHead, $lUploadFileName, strlen($lTemplateFileNameHead)) != 0) {
			
			//★　ファイル名が不正エラー（必要に応じてメッセージを見直す）
			//エラーメッセージ設定
			$pViewData["errors"] = new MessageBag([
				"error" => "E028 : Master Name and Excel must be selected from the same Master. "
			]);
			
			return $pViewData;
		}
		
		//Upload File名の末尾が.xlsmであること。
		if (strrchr($lUploadFileName,'.xlsm') != ".xlsm") {
			
			//★　拡張子が不正エラー（必要に応じてメッセージを見直す）
			//エラーメッセージ設定
			$pViewData["errors"] = new MessageBag([
				"error" => "E028 : Master Name and Excel must be selected from the same Master. "
			]);
			
			return $pViewData;
		}
		
		return $pViewData;
	}
	
	//**************************************************************************
	// processing name    getTemplateFileName
	// over view          Excelテンプレートファイル名を返却する
	// parameter          Master Nameのリストで選択されたMaster識別用のコード値
	// returned value     Excelテンプレートファイル名
	// programer          s-miyamoto
	// date               2014.07.24
	// update             2014.07.24 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getTemplateFileName($pMasterCode)
	{
		$lExcelTemplateFileName = ""; //Excelテンプレートファイル名
		
		switch ($pMasterCode){
		
			case '001':
				$lExcelTemplateFileName = "001-User Master(Inspector Master)_template.xlsm";
				break;
			
			case '002':
				$lExcelTemplateFileName = "002-Machine Master_template.xlsm";
				break;
			
			case '003':
				$lExcelTemplateFileName = "003-Inspection Time Master_template.xlsm";
				break;
			
			case '004':
				$lExcelTemplateFileName = "004-Customer Master_template.xlsm";
				break;
				
			case '005':
				$lExcelTemplateFileName = "005-Inspection(Check) Sheet Master_template.xlsm";
				break;
				
			case '006':
				$lExcelTemplateFileName = "006-Inspection(Check) No. Master_template.xlsm";
				break;
				
			case '007':
				$lExcelTemplateFileName = "007-Inspection(Check) No. Time Master_template.xlsm";
				break;
		}
		
		return $lExcelTemplateFileName;
	}
	
	//**************************************************************************
	// processing name    getMasterData
	// over view          マスタデータを取得して返却する
	// parameter          Master Nameのリストで選択されたMaster識別用のコード値
	// returned value     マスタデータの値（テーブルイメージの2次元Array）
	// programer          s-miyamoto
	// date               2014.07.24
	// update             2014.07.24 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    ■■2017/03/01 Hoshina  case'005''006''007' add MasterNo&Process
	//**************************************************************************
	private function getMasterData($pMasterCode)
	{
		$lTblMasterData = []; //マスタのデータテーブル
		
		switch ($pMasterCode){
		
			case '001':
				$lTblMasterData = DB::select('
							  SELECT * 
							    FROM TUSERMST
							   WHERE DELETE_FLG = "0"
							ORDER BY DISPLAY_ORDER
							        ,USER_ID
				'
				);
				
				break;
				
			case '002':
				$lTblMasterData = DB::select('
							  SELECT *
							    FROM TMACHINM
							   WHERE DELETE_FLG = "0"
							ORDER BY DISPLAY_ORDER
							        ,MACHINE_NO
				'
				);
				
				break;
				
			case '003':
				$lTblMasterData = DB::select('
							  SELECT *
							    FROM TINSPTIM
							   WHERE DELETE_FLG = "0"
							ORDER BY DISPLAY_ORDER
							        ,INSPECTION_TIME_ID
				'
				);
				
				break;
				
			case '004':
				$lTblMasterData = DB::select('
							  SELECT *
							    FROM TCUSTOMM
							   WHERE DELETE_FLG = "0"
							ORDER BY DISPLAY_ORDER
							        ,CUSTOMER_ID
				'
				);
				
				break;
				
			case '005':
				$lTblMasterData = DB::select('
							  SELECT MAIN.*
							    FROM TISHEETM AS MAIN
							   WHERE MAIN.DELETE_FLG = "0"
							     AND EXISTS (
							                 SELECT 1
							                   FROM TISHEETM SUB
							                  WHERE SUB.CUSTOMER_ID = CASE :CustomerId1
							                                               WHEN "" THEN SUB.CUSTOMER_ID
							                                               ELSE :CustomerId2
							                                           END
							                    AND SUB.MODEL_NAME = CASE :ModelName1
							                                               WHEN "" THEN SUB.MODEL_NAME
							                                               ELSE :ModelName2
							                                          END
							                    AND MAIN.INSPECTION_SHEET_NO = SUB.INSPECTION_SHEET_NO
							                    AND MAIN.REV_NO = SUB.REV_NO
							                )
							ORDER BY MAIN.DISPLAY_ORDER
							        ,MAIN.MASTER_NO
							        ,MAIN.PROCESS_ID
							        ,MAIN.INSPECTION_SHEET_NO
							        ,MAIN.REV_NO
				',
						[
							"CustomerId1" => (String)Input::get('cmbCustomerForSearch'),
							"CustomerId2" => (String)Input::get('cmbCustomerForSearch'),
							"ModelName1"  => (String)Input::get('cmbModelForSearch'),
							"ModelName2"  => (String)Input::get('cmbModelForSearch'),
						]
				);
				
				break;
				
			case '006':
				$lTblMasterData = DB::select('
							  SELECT MAIN.*
							    FROM TINSPNOM MAIN
							   WHERE MAIN.DELETE_FLG = "0"
							     AND EXISTS (
							                 SELECT 1
							                   FROM TISHEETM SUB
							                  WHERE SUB.CUSTOMER_ID = CASE :CustomerId1
							                                               WHEN "" THEN SUB.CUSTOMER_ID
							                                               ELSE :CustomerId2
							                                           END
							                    AND SUB.MODEL_NAME = CASE :ModelName1
							                                               WHEN "" THEN SUB.MODEL_NAME
							                                               ELSE :ModelName2
							                                          END
							                    AND MAIN.INSPECTION_SHEET_NO = SUB.INSPECTION_SHEET_NO
							                    AND MAIN.REV_NO = SUB.REV_NO
							                )
							ORDER BY MAIN.MASTER_NO
							        ,MAIN.PROCESS_ID
							        ,MAIN.INSPECTION_SHEET_NO
							        ,MAIN.REV_NO
							        ,MAIN.DISPLAY_ORDER
							        ,MAIN.INSPECTION_NO
				',
						[
							"CustomerId1" => (String)Input::get('cmbCustomerForSearch'),
							"CustomerId2" => (String)Input::get('cmbCustomerForSearch'),
							"ModelName1"  => (String)Input::get('cmbModelForSearch'),
							"ModelName2"  => (String)Input::get('cmbModelForSearch'),
						]
				);
				
				break;
				
			case '007':
				$lTblMasterData = DB::select('
							  SELECT *
							    FROM TINSNTMM MAIN
							   WHERE MAIN.DELETE_FLG = "0"
							     AND EXISTS (
							                 SELECT 1
							                   FROM TISHEETM SUB
							                  WHERE SUB.CUSTOMER_ID = CASE :CustomerId1
							                                               WHEN "" THEN SUB.CUSTOMER_ID
							                                               ELSE :CustomerId2
							                                           END
							                    AND SUB.MODEL_NAME = CASE :ModelName1
							                                               WHEN "" THEN SUB.MODEL_NAME
							                                               ELSE :ModelName2
							                                          END
							                    AND MAIN.MASTER_NO = SUB.MASTER_NO
							                    AND MAIN.PROCESS_ID = SUB.PROCESS_ID
							                    AND MAIN.INSPECTION_SHEET_NO = SUB.INSPECTION_SHEET_NO
							                    AND MAIN.REV_NO = SUB.REV_NO
							                )
							ORDER BY MAIN.MASTER_NO
							        ,MAIN.PROCESS_ID
							        ,MAIN.INSPECTION_SHEET_NO
							        ,MAIN.REV_NO
							        ,MAIN.INSPECTION_NO
							        ,MAIN.INSPECTION_TIME_ID
				',
						[
							"CustomerId1" => (String)Input::get('cmbCustomerForSearch'),
							"CustomerId2" => (String)Input::get('cmbCustomerForSearch'),
							"ModelName1"  => (String)Input::get('cmbModelForSearch'),
							"ModelName2"  => (String)Input::get('cmbModelForSearch'),
						]
				);
				
				break;
		}
		
		return $lTblMasterData;
	}

	//**************************************************************************
	// processing name    setCustomerList
	// over view      顧客コンボボックス用のデータを取得し、画面表示用の配列にセットする
	// parameter      画面表示用配列
	// returned value    画面表示用配列
	//**************************************************************************
	private function setCustomerList($pViewData)
	{
		$lArrCustomerList     = ["" => ""];  //返却用ビューデータ

		//顧客のリストがセッションに無ければ検索してセッションに保持
		if (is_null(Session::get('ZA1010SetCustemorDropdownListData'))) {
			
			//Inspection Tool Classの検索処理
			$lArrCustomerList = $this->getCustomerList();
			
			//セッションに保持
			Session::put('ZA1010SetCustemorDropdownListData', $lArrCustomerList);
		}
		else
		{
			//セッションにある場合はここで取り出し
			$lArrCustomerList = Session::get('ZA1010SetCustemorDropdownListData');
		}
		
		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList
		];
		
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getCustomerList
	// over view
	// parameter
	// returned value
	//**************************************************************************
	private function getCustomerList()
	{
		$lTblCustomerList			= []; //Result
		$lRowCustomerList			= []; //Working Area
		$lArrDataCustomerList		= []; //Return List
		
		$lTblCustomerList = DB::select(
		'
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TCUSTOMM AS CUST
		       WHERE CUST.DELETE_FLG = "0"
		    ORDER BY CUST.DISPLAY_ORDER
		'
		);
		
		//cast search result to array（two dimensions Array）
		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblCustomerList = (array)$lTblCustomerList;
		
		//add blank space
		//空白行を追加
		$lArrDataCustomerList += [
			"" => ""
		];
		
		//data exist
		//データが存在する
		if ($lTblCustomerList != null)
		{
			//store result in Array again
			//結果を配列に格納し直す
			foreach ($lTblCustomerList as $lRowCustomerList) {
			
				//Cast previously　because it has erroe to read value of array while CASTing
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowCustomerList = (Array)$lRowCustomerList;
				
				$lArrDataCustomerList += [
					$lRowCustomerList["CUSTOMER_ID"] => $lRowCustomerList["CUSTOMER_NAME"]
				];
			}
		}
		
		return $lArrDataCustomerList;
	}

	//**************************************************************************
	// processing name    setModelList
	// over view
	// parameter
	// returned value
	//**************************************************************************
	private function setModelList($pViewData,$pCustomerId)
	{
		$lArrDataListModelList     = ["" => ""];  //返却用ビューデータ
		
		//Modelの検索処理
		$lArrDataListModelList = $this->getModelList($pCustomerId);
		
		//画面への移送用配列に追加（+=で記載する）
		$pViewData += [
			"arrDataListModelList" => $lArrDataListModelList
		];
		
		return $pViewData;
	}

	//**************************************************************************
	// processing name    getModelList
	// over view
	// parameter
	// returned value
	//**************************************************************************
	private function getModelList($pCustomerId)
	{
		$lTblModel			= []; //モデル情報取得結果
		$lRowModel			= []; //配列変換時のワーク領域
		$lArrModel			= []; //返却するモデルリスト
		
		$lTblModel = DB::select('
		      SELECT SHET.MODEL_NAME
		            ,SHET.MODEL_NAME AS MODEL_NAME_NAME
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG = "0"
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.MODEL_NAME
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"CustomerId" => $pCustomerId,
				]
		);
		
		//検索結果をarrayにcast（この時点では2次元配列）
		$lTblModel = (array)$lTblModel;
		
		//空白行を追加
		$lArrModel += [
			"" => ""
		];
		
		//データが存在する
		if ($lTblModel != null)
		{
			//結果を配列に格納し直す
			foreach ($lTblModel as $lRowModel) {
			
				//先にCastが必要　CASTしつつ列の値を読もうとすると落ちるので注意
				$lRowModel = (Array)$lRowModel;
				
				$lArrModel += [
					$lRowModel["MODEL_NAME"] => $lRowModel["MODEL_NAME_NAME"]
				];
			
			}
			
		}
		
		return $lArrModel;
	}


}