<?php

use Illuminate\Support\MessageBag;

set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER["DOCUMENT_ROOT"].'/classes/');
include_once 'PHPExcel.php';
include_once 'PHPExcel/IOFactory.php';

class BA1010ListController
extends Controller
{
	//data count per 1 page
	CONST NUMBER_PER_PAGE = 10;

	//Inspection Result Form for Approval
	CONST SHONINEXCEL_HEADER_STARTROW = 4; //line number for header
	CONST SHONINEXCEL_MEISAI_STARTROW = 7; //start line number for detail

	//NG Result
	CONST NGSHONINEXCEL_HEADER_STARTROW = 3; //line number for header
	CONST NGSHONINEXCEL_MEISAI_STARTROW = 7; //start line number for detail

	//Inspection Sheet
	//CONST INSPECTIONSHEETEXCEL_HEADER_STARTCOL      = XX;  //line number for header(列)
	CONST INSPECTIONSHEETEXCEL_HEADER_STARTROW      = 2;  //line number for header(行)
	CONST INSPECTIONSHEETEXCEL_HISTORY_STARTCOL     = 0;  //line number for history(列)
	CONST INSPECTIONSHEETEXCEL_HISTORY_STARTROW     = 39; //line number for history(行)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTCOL      = 0;  //line number for detail(列)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTROW      = 50; //line number for detail(行)
	CONST INSPECTIONSHEETEXCEL_MEISAI_NUMBER        = 10; //一列に表示する同一箇所測定回数
	CONST INSPECTIONSHEETEXCEL_XBAR_STARTPOSITION   = 18; //一列表示の際のXbarの出力開始位置
	
	CONST RESULT_INPUT_TYPE_NUMBER   = "1"; //NUMBER(TEXT)
	CONST RESULT_INPUT_TYPE_OKNG     = "2"; //OK/NG(RadioButton)

	CONST JUDGE_OK     = "OK";
	CONST JUDGE_NG     = "NG";

	CONST CONDITION_NORMAL     = "01"; //Nomal Inspection
	CONST INSPECTION_TIME_ALL  = "ZZ"; //Abnomaal Inspection


	//**************************************************************************
	// process    ListAction
	// overview   Display screen for inspection result
	//           And process as Entry,Search,CSV buttuns
	// argument   Nothing
	// return value    Nothing
	// author    s-miyamoto
	// date    14.05.2014
	// record of updates  14.05.2014 v0.01 make first
	//           26.08.2014 v1.00 FIX
	//**************************************************************************
	public function ListAction()
	{
		$lViewData               = []; //Array for transition to screen

		$lTblSearchResultData    = []; //Data table of inspection result
		$lPagenation             = []; //For Paging（Return to screen）

		$lTblNotExistNoData      = []; //Data table of Inspection No(No Data)

		$lTblCheckSheetInfo      = []; //DataTable of Inspection Check Sheet
		$lRowCheckSheetInfo      = []; //DataRow of Inspection Check Sheet


		$lTblMachine             = []; //DataTable of Machine
		$lRowMachine             = []; //DataRow of Machine

		$lTblInspector           = []; //DataTable of inspecter
		$lRowInspector           = []; //DataRow of inspecter

		$lTblCustomerID          = [];
		$lRowCustomerID          = [];

		$lInspectionResultNo     = ""; //PK of line
		$lDataRev                = ""; //DataRevision of line

		$lTblModifyTargetData    = []; //DataTable for Modify
		$lRowModifyTargetData    = []; //DataRow for Modify

		$lTblInspectionResultCSV = []; //Datatable of inspection result(CSV)

		$lOutputCSVData          = ""; //CSV data for output
		$lMimeSetting            = ""; //MIME setting

		$lDataCount              = 0;  //data counter for redundant logic key
		$lCheckNoCount           = 0;  //number of Inspection No. corresponding to the Time
		$lDeleteCount            = 0;  //number of data deleted
		$lSheetMasterDataCount   = 0;  //data counter for redundant logic key

		$lTemplateFileNameShonin   = ""; //set template file name for approval
		$lTemplateFileNameNGResult = ""; //set template file name for NGResult

		$lWork1INSPECTIONNO       = ""; //Inspection No For Work
		$lWork1INSPECTIONTIMEID   = ""; //Inspection Time ID for Work
		$lWork2INSPECTIONNO       = ""; //Inspection No For Work
		$lWork2INSPECTIONTIMEID   = ""; //Inspection Time ID for Work

		//■■13/06/2016 Motooka added NG Result output■■
		$lWorkINSPECTIONSHEET    = ""; //Inspection Check Sheet No for Work
		$lWorkMACHINENO          = ""; //Machine No for Work
		$lWorkINSPECTIONNO       = ""; //Inspection No for Work
		$NGFLG                   = 0;  //Flag to output
		$lWorkCheckSheetName     = "";
		$lWorkCheckModelName     = "";
		$NoWritingFlag           = 0;

		//Inspection Sheet
		$lWork1InspectionNo              = 0;
		$lWork1AnalyGrp                  = 0;
		$lWork1AnalyGrpHowmany           = 0;
		$lWork1InspectionResultInputType = 0;
		$lWork2InspectionNo              = 0;
		$lWork2AnalyGrp                  = 0;
		$lWork2AnalyGrpHowmany           = 0;
		$lWork2InspectionResultInputType = 0;
		$lWorkFindResultFlg              = 0;
		$lWorkCount                      = 0;
		$lWorkHistoryCount               = 0;
		$lWorkFindAnalyGrpFlg            = 0;
		$lWorkAverageValue               = 0;
		$lWorkMinValue                   = 0;
		$lWorkMaxValue                   = 0;
		$lWorkRangeValue                 = 0;
		$lWorkStdevValue                 = 0;
		$lWorkJudgeResultValue           = 1;
		//$lWorkJudgeReason                = "";
		//$lWorkInspectionResultValue      = "";
		

		//Value to make Combo(Set value from session when transit to screen)
		$lCmbValMasterNoForEntry = "";
		$lCmbValProcessForEntry = "";
		$lCmbValCustomerForEntry = "";
		$lCmbValCheckSheetNoForEntry = "";
		$lCmbValMasterNoForSearch = "";
		$lCmbValProcessForSearch = "";
		
		$lCmbValCustomerForSearch = "";
		
		$lCmbValCheckSheetNoForSearch = "";

		//store value of entering to screen
		Input::flash();

		//get parameter from login screen throgh session and issue to array for transpotion to screen
		$lViewData += [
			"UserID"  => Session::get('AA1010UserID'),
			"UserName" => Session::get('AA1010UserName'),
			"AdminFlg" => Session::get('AA1010AdminFlg')
		];
        
		//Inspection No List of Entry screen and Equipment list(because it must delete at transition to other screen)
		//clear at first of List screen
		//If data was deleted while Return button in Entry screen, data can't delete from menu
		Session::forget('BA2010InspectionNoData');
		Session::forget('BA2010InspectionToolClassDropdownListData');

		if (Input::has('btnEntry'))                                             //■■■■Entry button■■■■
		{
			//log
			Log::write('info', 'BA1010 Entry Button Click.',
				[
					"Master No."  		=> Input::get('txtMasterNoForEntry'   ,''),
					"Process"  			=> Input::get('cmbProcessForEntry'   ,''),
					"Customer"  		=> Input::get('cmbCustomerForEntry'   ,''),
					"Check Sheet No."  	=> Input::get('cmbCheckSheetNoForEntry'   ,''),
					"Material Name"  	=> Input::get('cmbMaterialNameForEntry'   ,''),
					"Material Size"  	=> Input::get('cmbMaterialSizeForEntry'   ,''),
					"M/C No."          	=> Input::get('txtMachineNoForEntry'      ,''),
					"Inspector Code"   	=> Input::get('txtInspectorCodeForEntry'  ,''),
					"Inspection Date"  	=> Input::get('txtInspectionDateForEntry' ,''),
					"Condition"        	=> Input::get('cmbConditionForEntry'      ,''),
					"Inspection Time"  	=> Input::get('cmbInspectionTimeForEntry' ,''),
				]
			);

			//error check
			$lViewData = $this->isErrorForEntry($lViewData);

			//check inspection check sheet No. except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//check inspection check sheet No. and get max Revision No.
				$lTblCheckSheetInfo = $this->getCheckSheetData(Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCheckSheetNoForEntry'));

				//get data in 0 line（if no data,get NULL because query use MAX　if data exist, it must be one）
				$lRowCheckSheetInfo = (array)$lTblCheckSheetInfo[0];

				//data exist
				if ($lRowCheckSheetInfo["INSPECTION_SHEET_NO"] != null)
				{
					//store search result in session to transit to entry screen
					Session::put('BA1010MasterNo', $lRowCheckSheetInfo["MASTER_NO"]);
					Session::put('BA1010ProcessForEntryName', $lRowCheckSheetInfo["PROCESS_NAME"]);
					Session::put('BA1010CustomerName', $lRowCheckSheetInfo["CUSTOMER_NAME"]);
					Session::put('BA1010CheckSheetName', $lRowCheckSheetInfo["INSPECTION_SHEET_NAME"]);
					Session::put('BA1010MaterialName', $lRowCheckSheetInfo['MATERIAL_NAME']);
					Session::put('BA1010MaterialSize', $lRowCheckSheetInfo['MATERIAL_SIZE']);
					Session::put('BA1010RevisionNo', $lRowCheckSheetInfo["MAX_REV_NO"]);
					Session::put('BA1010ItemNo', $lRowCheckSheetInfo["ITEM_NO"]);

					Session::put('BA1010ItemName', $lRowCheckSheetInfo["ITEM_NAME"]);
					Session::put('BA1010ModelName', $lRowCheckSheetInfo["MODEL_NAME"]);
					Session::put('BA1010DrawingNo', $lRowCheckSheetInfo["DRAWING_NO"]);
					Session::put('BA1010ISOKanriNo', $lRowCheckSheetInfo["ISO_KANRI_NO"]);
					Session::put('BA1010ProductWeight', $lRowCheckSheetInfo["PRODUCT_WEIGHT"]);
					Session::put('BA1010HeatTreatmentType', $lRowCheckSheetInfo['HEAT_TREATMENT_TYPE']);
					Session::put('BA1010PlatingKind', $lRowCheckSheetInfo["PLATING_KIND"]);
				}
				//data does no exist
				else
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E008 : Check Sheet No. is not registered in Master. "
					]);
				}
			}

			//check M/C No. except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{

				//check M/C No. and get M/C Name
				$lTblMachine = $this->getMachineData(Input::get('txtMachineNoForEntry'));

				if ($lTblMachine != null)
				{
					//data exist
					//get data in 0 line
					$lRowMachine = (array)$lTblMachine[0];

					//store search result in session to transit to entry screen
					Session::put('BA1010MachineNoForEntryName', $lRowMachine["MACHINE_NAME"]);
				}
				else
				{
					//data does not exist
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E011 : M/C No. is not registered in Master. "
					]);
				}
			}

			//check Inspector Code except for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//check Inspector Code and get name of inspector
				$lTblInspector = $this->getInspectorData(Input::get('txtInspectorCodeForEntry'));

				if ($lTblInspector != null)
				{
					//data exist
					//get data in 0 line
					$lRowInspector = (array)$lTblInspector[0];

					//store session search result to transit to entry screen
					Session::put('BA1010InspectorName', $lRowInspector["USER_NAME"]);
				}
				else
				{
					//data does not exist
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E014 : Inspector Code is not registered in Master."
					]);
				}
			}

			//select master data (use input item)
			if (array_key_exists("errors", $lViewData) == false)
			{
				//get count of same logic key
				$lSheetMasterDataCount = $this->getInspectionSheetMasterDataCount(
				                                                    Input::get('txtMasterNoForEntry')
				                                                   ,Input::get('cmbProcessForEntry')
				                                                   ,Input::get('cmbCustomerForEntry')
				                                                   ,Input::get('cmbCheckSheetNoForEntry')
				                                                   ,Input::get('cmbMaterialNameForEntry')
				                                                   ,Input::get('cmbMaterialSizeForEntry')
				                                                   );

				if ($lSheetMasterDataCount == 0)
				{
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else if ($lSheetMasterDataCount == 1)
				{
					//何もしない
				}
				else
				{
					//RevNoがupする場合は2以上になる為、コメント
					//$lViewData["errors"] = new MessageBag([
					//	"error" => "E997 : Target data does not exist."
					//]);
				}
			}

			//check duplication data in the same logic key except for no error
			//prompt to continue to regist from Modify in case of having registed by same logic key already
			//他の項目でエラーがない場合　同一論理キーの重複データチェック
			//既に同一論理キーで登録されている場合は、検索一覧のModifyの方から登録の続きを行うように促す
			if (array_key_exists("errors", $lViewData) == false)
			{
				//get count of same logic key
				$lDataCount = $this->getLogicalDuplicationDataCount(
				                                                    Input::get('txtMasterNoForEntry')
				                                                   ,Input::get('cmbProcessForEntry')
				                                                   ,Input::get('cmbCheckSheetNoForEntry')
				                                                   ,$lRowCheckSheetInfo["MAX_REV_NO"]
				                                                   ,Input::get('txtMachineNoForEntry')
				                                                   ,Input::get('txtInspectorCodeForEntry')
				                                                   ,Input::get('txtInspectionDateForEntry')
				                                                   ,Input::get('cmbConditionForEntry')
				                                                   ,Input::get('cmbInspectionTimeForEntry')
				                                                   );

				//Condition combo＝nomal(01)⇒check,else⇒no check（available to regist duplication in the same time）
				If (Input::get('cmbConditionForEntry') == self::CONDITION_NORMAL)
				{
					if ($lDataCount > 0)
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E031 : The data of the time is already registered. Search the data and retry the process with Modify Button. "
						]);
					}

					//error of logic key duplicating in case of more than one data existing
					if ($lDataCount > 0)
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E031 : The data of the time is already registered. Search the data and retry the process with Modify Button. "
						]);
					}
				}
			}

			//check inspection corresponding to Inspection Time except for no error
			//検査時間帯に該当する検査が存在するかどうかのチェック
			if (array_key_exists("errors", $lViewData) == false)
			{
				//Condition combo＝nomal(01)
				If (Input::get('cmbConditionForEntry') == self::CONDITION_NORMAL) 
				{
					//get count of inspection No.data corresponding to time
					$lCheckNoCount = $this->getCheckNoDataCount(
					                                            Input::get('txtMasterNoForEntry')
					                                            ,Input::get('cmbProcessForEntry')
					                                            ,Input::get('cmbCheckSheetNoForEntry')
						                                        ,$lRowCheckSheetInfo["MAX_REV_NO"]
						                                        ,Input::get('cmbInspectionTimeForEntry')
						                                        );

					//display error of mischosing time in case of no data
					if ($lCheckNoCount == 0)
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E032 : You mischose the time of Inspection."
						]);
					}
				}
				//Condition combo≠nomal(01) INSPECTION_TIME_ALL = "ZZ"
				else
				{
					//get count of inspection No.data corresponding to time
					$lCheckNoCount = $this->getCheckNoDataCount(
					                                            Input::get('txtMasterNoForEntry')
					                                            ,Input::get('cmbProcessForEntry')
					                                            ,Input::get('cmbCheckSheetNoForEntry')
						                                        ,$lRowCheckSheetInfo["MAX_REV_NO"]
						                                        ,self::INSPECTION_TIME_ALL
						                                        );

					//display error of nothing to register abnomal master in case of no data
					if ($lCheckNoCount == 0)
					{
						$lViewData["errors"] = new MessageBag([
							"error" => "E036 : Not Exists Target Data."
						]);
					}
				}
			}

			//transit screen except no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				$lArrDataListCondition = ["" => ""];  //List to return Condition data to screen
				$lArrDataListCondition = Session::get('BA1010ConditionDropdownListData');

				$lArrDataListInspectionTime = ["" => ""];  //List to return Inspection Time data to screen
				$lArrDataListInspectionTime = Session::get('BA1010InspectionTimeDropdownListData');

				//store input data for entry in session
				Session::put('BA1010MasterNoForEntry'			, Input::get('txtMasterNoForEntry'));
				Session::put('BA1010ProcessForEntry'			, Input::get('cmbProcessForEntry'));
				Session::put('BA1010CustomerForEntry'			, Input::get('cmbCustomerForEntry'));
				Session::put('BA1010CheckSheetNoForEntry'		, Input::get('cmbCheckSheetNoForEntry'));
				Session::put('BA1010MaterialNameForEntry'		, Input::get('cmbMaterialNameForEntry'));
				Session::put('BA1010MaterialSizeForEntry'		, Input::get('cmbMaterialSizeForEntry'));
				Session::put('BA1010MachineNoForEntry'			, Input::get('txtMachineNoForEntry'));
				Session::put('BA1010InspectorCodeForEntry'		, Input::get('txtInspectorCodeForEntry'));
				Session::put('BA1010InspectionDateForEntry'		, Input::get('txtInspectionDateForEntry'));
				Session::put('BA1010ConditionForEntry'			, Input::get('cmbConditionForEntry'));
				Session::put('BA1010ConditionForEntryName'		, $lArrDataListCondition[Input::get('cmbConditionForEntry')]);
				Session::put('BA1010InspectionTimeForEntry'		, Input::get('cmbInspectionTimeForEntry'));
				Session::put('BA1010InspectionTimeForEntryName'	, $lArrDataListInspectionTime[Input::get('cmbInspectionTimeForEntry')]);

				//Search
				Session::put('BA1010MasterNoForSearch'			, Input::get('txtMasterNoForSearch'));
				Session::put('BA1010ProcessForSearch'			, Input::get('cmbProcessForSearch'));
				Session::put('BA1010CustomerForSearch'			, Input::get('cmbCustomerForSearch'));
				Session::put('BA1010CheckSheetNoForSearch'		, Input::get('cmbCheckSheetNoForSearch'));
				Session::put('BA1010RevisionNoForSearch'		, Input::get('cmbRevisionNoForSearch'));
				Session::put('BA1010MaterialNameForSearch'		, Input::get('cmbMaterialNameForSearch'));
				Session::put('BA1010MaterialSizeForSearch'		, Input::get('cmbMaterialSizeForSearch'));
				Session::put('BA1010MachineNoForSearch'			, Input::get('txtMachineNoForSearch'));
				Session::put('BA1010InspectorCodeForSearch'		, Input::get('txtInspectorCodeForSearch'));
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
				Session::put('BA1010InspectionDateToForSearch'	, Input::get('txtInspectionDateToForSearch'));
				Session::put('BA1010PartNoForSearch'			, Input::get('txtPartNoForSearch'));
				Session::put('BA1010InspectionTimeForSearch'	, Input::get('cmbInspectionTimeForSearch'));

				//delete Inspection Result No. at Entry because it transport inspection result No. only at Modify
				Session::forget('BA1010InspectionResultNo');

				//transition to Entry screen
				return Redirect::route("user/preentry");
			}
		}
		elseif (Input::has('btnSearch'))                                        //■■■■Search button■■■■
		{
			//log
			Log::write('info', 'BA1010 Search Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForSearch'           	,''),
					"Process"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer"  		    => Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."          => Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."               => Input::get('txtMachineNoForSearch'          	,''),
					"Inspector Code"        => Input::get('txtInspectorCodeForSearch'      	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."              => Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"       => Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForSearchEnterCheck($lViewData);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//■■13/06/2016 Motooka NG Result■■
			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//clear in case search requirment remain in session
				Session::forget('BA1010MasterNoForSearch');
				Session::forget('BA1010ProcessForSearch');
				Session::forget('BA1010CustomerForSearch');
				Session::forget('BA1010CheckSheetNoForSearch');
				Session::forget('BA1010RevisionNoForSearch');
				Session::forget('BA1010MaterialNameForSearch');
				Session::forget('BA1010MaterialSizeForSearch');
				Session::forget('BA1010MachineNoForSearch');
				Session::forget('BA1010InspectorCodeForSearch');
				Session::forget('BA1010InspectionDateFromForSearch');
				Session::forget('BA1010InspectionDateToForSearch');
				Session::forget('BA1010PartNoForSearch');
				//Session::forget('BA1010InspectionTimeForSearch');

				//search
				$lTblSearchResultData = $this->getInspectionResultList();
				$lTblNotExistNoData   = $this->getNotExistNoList();

				//error check 0 data
				if (count($lTblSearchResultData) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}

				//sessionにstore
				Session::put('BA1010InspectionResultData', $lTblSearchResultData);
				Session::put('BA1010NotExistNoData', $lTblNotExistNoData);
			}
		}
		elseif (Input::has('btnCsv'))                                           //■■■■CSV button■■■■
		{
			//log
			Log::write('info', 'BA1010 CSV Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForSearch'           	,''),
					"Process"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer"  		    => Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."          => Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."               => Input::get('txtMachineNoForSearch'          	,''),
					"Inspector Code"        => Input::get('txtInspectorCodeForSearch'      	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."              => Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"       => Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//■■13/06/2016 Motooka NG Result■■
			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search data for CSV
				$lTblInspectionResultCSV = $this->getInspectionResultCSV();

				//error check 0 data
				if (count($lTblInspectionResultCSV) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else  //data exists
				{
					//Cast to Array
					$lTblInspectionResultCSV = (Array)$lTblInspectionResultCSV;

					//processing data to CSV
					$lOutputCSVData = $this->createInspectionResultCSV($lTblInspectionResultCSV);

					//set MIME
					$lMimeSetting = array(
						'Content-Type' => 'text/csv',
						'Content-Disposition' => 'attachment; filename="InspectionResultData.csv"',
					);

					//download
					return Response::make(rtrim($lOutputCSVData, "\n"), 200, $lMimeSetting);
				}
			}
		}
		elseif (Input::has('btnCsv2'))                                          //■■■■CSV（for approval）button■■■■
		{
			//log
			Log::write('info', 'BA1010 CSV(For Approval) Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForSearch'           	,''),
					"Process"				=> Input::get('cmbProcessForSearch'            	,''),
					"Customer"  		    => Input::get('cmbCustomerForSearch'           	,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Revision No."          => Input::get('cmbRevisionNoForSearch'         	,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"M/C No."               => Input::get('txtMachineNoForSearch'          	,''),
					"Inspector Code"        => Input::get('txtInspectorCodeForSearch'      	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."              => Input::get('txtPartNoForSearch'             	,''),
					//"Inspection Time"       => Input::get('cmbInspectionTimeForSearch      	,''),
				]
			);

			//check error
			$lViewData = $this->isErrorForSearch($lViewData);

			//■■13/06/2016 Motooka NG Result■■
			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//search data for CSV
				$lTblInspectionResultCSV = $this->getInspectionResultCSVforApproval();

				//error check 0 data
				if (count($lTblInspectionResultCSV) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else  //data exists
				{
					//Cast to Array
					$lTblInspectionResultCSV = (Array)$lTblInspectionResultCSV;

					//processing data to CSV
					$lOutputCSVData = $this->createInspectionResultCSVforApproval($lTblInspectionResultCSV);

					$lMimeSetting = array(
						'Content-Type' => 'text/csv',
						'Content-Disposition' => 'attachment; filename="InspectionResultDataforApproval.csv"',
					);

					return Response::make(rtrim($lOutputCSVData, "\n"), 200, $lMimeSetting);
				}
			}
		}
		elseif (Input::has('btnShoninData'))                                    //■■■■button for output for approval (Excel)■■■■
		{
			//log
			Log::write('info', 'BA1010 Excel Button Click.',
				[
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForSearch'       ,''),
					"Revision No."          => Input::get('cmbRevisionNoForSearch'         ,''),
					"M/C No."               => Input::get('txtMachineNoForSearch'          ,''),
					"Inspector Code"        => Input::get('txtInspectorCodeForSearch'      ,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForSearch' ,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForSearch'   ,''),
					"Part No."              => Input::get('txtPartNoForSearch'             ,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForShonin($lViewData);
	        //echo "<pre>";
            //print_r($lViewData);
            //echo "</pre>";
			//check error(check type)
			$lViewData = $this->isErrorForSearch($lViewData);
			//echo "<pre>";
            //print_r($lViewData);
            //echo "</pre>";
			//■■13/06/2016 Motooka NG Result■■
			//check error at entering Inspection_Time
			if(Input::get('cmbInspectionTimeForSearch') == 0)
			{
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E041 : Do Not Enter Inspection Time ."
				]);
			}

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//get master data,result data corresponding
				///normal
				$lTblMasterDataShonin = $this->getInspectionMasterDataForShonin();
				$lTblResultDataShonin = $this->getInspectionResultDataForShonin();
			
				//abnomal data
				$lTblMasterDataShonin2 = $this->getInspectionMasterDataForShonin2();
				$lTblResultDataShonin2 = $this->getInspectionResultDataForShonin2();

				//error when master data does not exist
				//output in case result data does not exist
				if (count($lTblMasterDataShonin) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E036 : Not Exists Target Data."
					]);
				}
				else  //data exists
				{
					//templete Excel file 
					$objReader = new PHPExcel_Reader_Excel2007();
					$lTemplateFileNameShonin = "InspectionResultDataforApproval_template.xlsx";
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameShonin);
					// $objPHPExcel = $objReader->load(public_path()."/excel/".$lTemplateFileNameShonin);
					//do active sheet No. 0
					$objPHPExcel->setActiveSheetIndex(0);
					$objSheet = $objPHPExcel->getActiveSheet();

					//----------------------------
					//set infomation to header
					$lExcelRowCount = self::SHONINEXCEL_HEADER_STARTROW; //define first line of header

					//pull out master data 1 line and set value(master No., process Id, sheet No., revision No., model, sheet name)
					$lTblMasterDataShoninRow = (Array)$lTblMasterDataShonin[0];
					$objSheet->setCellValueByColumnAndRow(0, $lExcelRowCount, $lTblMasterDataShoninRow["MASTER_NO"]);
					$objSheet->setCellValueByColumnAndRow(1, $lExcelRowCount, $lTblMasterDataShoninRow["PROCESS_NAME"]);
					$objSheet->setCellValueByColumnAndRow(2, $lExcelRowCount, $lTblMasterDataShoninRow["INSPECTION_SHEET_NO"]);
					$objSheet->setCellValueByColumnAndRow(3, $lExcelRowCount, $lTblMasterDataShoninRow["REV_NO"]);
					$objSheet->setCellValueByColumnAndRow(4, $lExcelRowCount, $lTblMasterDataShoninRow["MODEL_NAME"]);
					$objSheet->setCellValueByColumnAndRow(5, $lExcelRowCount, $lTblMasterDataShoninRow["INSPECTION_SHEET_NAME"]);

					//in case result data exist,add data corresponding to result
					if (count($lTblResultDataShonin) > 0)
					{
						//pull out result data in 1 line and set value(machine name, user name, inspection date)
						$lTblResultDataShoninRow = (Array)$lTblResultDataShonin[0]; //pull out 1 line
						$objSheet->setCellValueByColumnAndRow(6, $lExcelRowCount, $lTblResultDataShoninRow["MACHINE_NAME"]);
						$objSheet->setCellValueByColumnAndRow(7, $lExcelRowCount, $lTblResultDataShoninRow["USER_NAME"]);
						$objSheet->setCellValueByColumnAndRow(8, $lExcelRowCount, $lTblResultDataShoninRow["INSPECTION_YMD"]);
					}

					//----------------------------
					//set infomation on detail
					//----------------------------
					//define start line of detail(decrease 1 line first,because loop counter add 1 more line)
					//明細の開始行を定義(ループカウンタで1追加するため最初に1減らす)
					$lExcelRowCount = self::SHONINEXCEL_MEISAI_STARTROW - 1;

					//in case data exists more than 2line(default),add lines
					//テンプレートのデフォルト行数(2行)以上にデータがある場合は、その分の行を追加
					if (count($lTblMasterDataShonin) > 2)
					{
						//■■16/05/2015 Hoshina added Condition■■
						//abnomal inspection
						//$objSheet->insertNewRowBefore(self::SHONINEXCEL_MEISAI_STARTROW + 1, count($lTblMasterDataShonin) - 2);
						$objSheet->insertNewRowBefore(self::SHONINEXCEL_MEISAI_STARTROW + 1, (count($lTblMasterDataShonin) + count($lTblResultDataShonin2)) - 2);
					}


					//set down data while looping master
					foreach ($lTblMasterDataShonin As $lMasterRow)
					{
						//add line No. to putout
						$lExcelRowCount = $lExcelRowCount + 1;
						//change corresponding line to Array
						$lCurrentMasterDataShoninRow = (Array)$lMasterRow;

						//***************************************************************************************************************************
						//leave Inspection No. in master data
						//マスターデータの検査番号を退避
						$lWork2INSPECTIONNO = $lCurrentMasterDataShoninRow["INSPECTION_NO"];
						$lWork2INSPECTIONTIMEID = $lCurrentMasterDataShoninRow["INSPECTION_TIME_ID"];
						//***************************************************************************************************************************

						//***************************************************************************************************************************
						//check abnomal inspection while looping nomal inspection. When nomal「INSPECTION_NO＆INSPECTION_TIME_ID」changes,set abnomal inspection in Excel
						//予定検査LOOP中に予定外検査もチェックする。予定検査の「検査番号と検査時間」が変わるタイミングで、予定外検査を帳票に入れる。
						//***************************************************************************************************************************
						//■■compare not first and left「INSPECTION_NO＆INSPECTION_TIME_ID」with「INSPECTION_NO＆INSPECTION_TIME_ID」read
						//⇒else if abnomal inspection exists, output in Excel
						//■■最初でない＆退避した「INSPECTION_NO＆INSPECTION_TIME_ID」と読み込んだ「INSPECTION_NO＆INSPECTION_TIME_ID」を比較
						//⇒異なっていたら予定外検査を確認し、予定外検査が存在すれば帳票に出力する。
						if ($lWork1INSPECTIONNO == "")
						{
							//Nothing
						}
						else
						{
							if ($lWork1INSPECTIONNO == $lCurrentMasterDataShoninRow["INSPECTION_NO"]
								and $lWork1INSPECTIONTIMEID == $lCurrentMasterDataShoninRow["INSPECTION_TIME_ID"])
							{
								//Nothing
							}
							else
							{
								//set down data  while looping result because abnomal inspection is master:result = 1:N
								//loop result data
								//予定外検査はマスタ：実績＝１：Ｎである為、実績をループさせながらデータ書き込み
								//実績データをループさせ、一致するデータが存在するかチェック
								foreach ($lTblResultDataShonin2 As $lResultRow2)
								{
									//change corresponding line to Array
									$lCurrentResultDataShoninRow2 = (Array)$lResultRow2;

									//if Inspection No. and Inspection time left correspond to Inspection No. and Inspection time of result
									//退避した検査番号・検査時間と読み込んだ実績の検査番号・検査時間が一致する場合
									if($lWork1INSPECTIONNO <> $lCurrentResultDataShoninRow2["INSPECTION_NO"])
									{
									}
									else
									{
										if( ($lWork1INSPECTIONTIMEID == $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"])
										   or
										    ( ($lWork1INSPECTIONTIMEID < $lWork2INSPECTIONTIMEID)
										       and
										       ($lWork1INSPECTIONTIMEID < $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"]
										        and
										        $lWork2INSPECTIONTIMEID > $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"])
										    )
										   or
										    ( ($lWork1INSPECTIONTIMEID > $lWork2INSPECTIONTIMEID)
										       and
										       ( ($lWork1INSPECTIONTIMEID < $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"]
										          and
										          $lWork1INSPECTIONTIMEID < $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"]
										          and
										          $lWork2INSPECTIONTIMEID < $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"])
										         or
										         ($lWork1INSPECTIONTIMEID > $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"]
										          and
										          $lWork1INSPECTIONTIMEID > $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"]
										          and
										          $lWork2INSPECTIONTIMEID > $lCurrentResultDataShoninRow2["INSPECTION_TIME_ID"])
										       )
										    )
										  )
									{
										$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

										//■■2015/12/17 Hoshina modify abnomal inspection＆NG result
										//balance of master data■■
										//■■2015/12/17 保科修正 予定外検査＆結果NGの場合、マスタデータとの差分を計算するが、当箇所ではまだマスタデータ読み込んでおらず
										//　　システムエラーとなってしまう為　下記に場所移動■■
										////balance of reference value and result value in case result vaue is NG
										////検査結果がNGの場合は基準値との差分を求めて出力する
										//$addWord = "";
										//if ($lCurrentResultDataShoninRow2["INSPECTION_RESULT_TYPE"] == "NG")
										//{
										//	//balance of reference value and result value(till point 7)
										//	$deff =bcadd($lCurrentResultDataShoninRow2["INSPECTION_RESULT_VALUE"]
										//	             , - $lCurrentMasterDataShoninRow2["REFERENCE_VALUE"]
										//	             , 7);
										//	//treat value(remove 0 at the back of radix point)
										//	$addWord = "(".preg_replace("/\.?0+$/","",$deff).")";
										//}

										$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["CODEORDER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);

										//■■2015/12/17 Hoshina modify abnomal inspection＆NG result
										//balance of master data■■
										//■■2015/12/17 保科修正 予定外検査＆結果NGの場合、マスタデータとの差分を計算するが、当箇所ではまだマスタデータ読み込んでおらず
										//　　システムエラーとなってしまう為　下記に場所移動■■
										//$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
										//         ->setValueExplicit($lCurrentResultDataShoninRow2["INSPECTION_RESULT_TYPE"].$addWord, PHPExcel_Cell_DataType::TYPE_STRING);

										$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["INSERT_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION_DTL_INTERNAL"], PHPExcel_Cell_DataType::TYPE_STRING);
										//consider newline
										//アクション明細は改行を考慮
										$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getStyleByColumnAndRow(13, $lExcelRowCount)
										         ->getAlignment()->setWrapText(true);
										$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION_DTL_REQUEST"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION_DTL_TSR"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION_DTL_SORTING"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["OFFSET_NG_ACTION_DTL_NOACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
										$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
										         ->setValueExplicit($lCurrentResultDataShoninRow2["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

										//master data
										foreach ($lTblMasterDataShonin2 As $lMasterRow2)
										{
											//change corresponding line to Array
											$lCurrentMasterDataShoninRow2 = (Array)$lMasterRow2;

											//if Inspection No. of result corresponds to Inspection No. of master
											//実績の検査番号と読み込んだマスタの検査番号が一致する場合
											if($lCurrentResultDataShoninRow2["INSPECTION_NO"] == $lCurrentMasterDataShoninRow2["INSPECTION_NO"])
											{
												//set down master data
												//マスタデータを書き込み(検査番号、検査時間、検査項目、基準値、NG値(上限)、NG値(下限))
												$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
												         ->setValueExplicit($lCurrentMasterDataShoninRow2["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

												$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
												         ->setValueExplicit($lCurrentMasterDataShoninRow2["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
												$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
												         ->setValueExplicit($lCurrentMasterDataShoninRow2["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
												$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
												         ->setValueExplicit($lCurrentMasterDataShoninRow2["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
												$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
												         ->setValueExplicit($lCurrentMasterDataShoninRow2["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);


												//■■2015/12/17 Hoshina modify abnomal inspection＆NG result
												//balance of master data■■
												//■■2015/12/17 保科修正 予定外検査＆結果NGの場合、マスタデータとの差分を計算するが、当箇所ではまだマスタデータ読み込んでおらず
												//　　システムエラーとなってしまう為　ここに場所移動■■
												//balance of reference value and result value in case result vaue is NG
												//検査結果がNGの場合は基準値との差分を求めて出力する
												$addWord = "";
												if ($lCurrentResultDataShoninRow2["INSPECTION_RESULT_TYPE"] == "NG")
												{
													//balance of reference value and result value(till point 7)
													//基準値と実績値の差を求める(小数点7桁まで求める) ※単純に減算すると勝手に浮動小数点で計算して値がずれるので関数を使用する
													$deff =bcadd($lCurrentResultDataShoninRow2["INSPECTION_RESULT_VALUE"]
													             , - $lCurrentMasterDataShoninRow2["REFERENCE_VALUE"]
													             , 7);
													//treat value(remove 0 at the back of radix point)
													//求めた値を加工する(小数点の後ろゼロを取り除く)
													$addWord = "(".preg_replace("/\.?0+$/","",$deff).")";
												}

												$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
												         ->setValueExplicit($lCurrentResultDataShoninRow2["INSPECTION_RESULT_TYPE"].$addWord, PHPExcel_Cell_DataType::TYPE_STRING);


											}
										}

										//add line No. to putout
										$lExcelRowCount = $lExcelRowCount + 1;
										}
									}
								}
							}
						}
						//***************************************************************************************************************************
						//***************************************************************************************************************************


						//set down master data
						//マスタデータを書き込み(検査番号、検査時間、Condition、検査項目、基準値、NG値(上限)、NG値(下限))
						$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
						//とりあえず通常検査を設定しておく。
						$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["CODEORDER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
						$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
						         ->setValueExplicit($lCurrentMasterDataShoninRow["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

						//check existance of result data corresponding to key while looping result data
						//実績データのループさせ、キーと一致するデータが実績が存在するかチェック
						foreach ($lTblResultDataShonin As $lResultRow)
						{
							//change corresponding line to Array
							//該当行を配列に変換
							$lCurrentResultDataShoninRow = (Array)$lResultRow;

							//set result in inspection check sheet in case result data corresponds to Inspection No.,Inspection time
							//検査番号・検査時間が一致する実績データが存在する場合は実績をシートに反映
							if($lCurrentMasterDataShoninRow["INSPECTION_NO"] == $lCurrentResultDataShoninRow["INSPECTION_NO"]
								and $lCurrentMasterDataShoninRow["INSPECTION_TIME_ID"] == $lCurrentResultDataShoninRow["INSPECTION_TIME_ID"])
							{
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

								//balance of reference value and result value in case result vaue is NG
								//検査結果がNGの場合は基準値との差分を求めて出力する
								$addWord = "";
								if ($lCurrentResultDataShoninRow["INSPECTION_RESULT_TYPE"] == "NG")
								{
									//balance of reference value and result value(till point 7)
									//基準値と実績値の差を求める(小数点7桁まで求める) ※単純に減算すると勝手に浮動小数点で計算して値がずれるので関数を使用する
									$deff =bcadd($lCurrentResultDataShoninRow["INSPECTION_RESULT_VALUE"]
									             , - $lCurrentMasterDataShoninRow["REFERENCE_VALUE"]
									             , 7);
									//treat value(remove 0 at the back of radix point)
									//求めた値を加工する(小数点の後ろゼロを取り除く)
									$addWord = "(".preg_replace("/\.?0+$/","",$deff).")";
								}

								//overwrite value of result
								//ここで実績の値を入れ上書き
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["CODEORDER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["INSPECTION_RESULT_TYPE"].$addWord, PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["INSERT_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION_DTL_INTERNAL"], PHPExcel_Cell_DataType::TYPE_STRING);
								//consider newline
								//アクション明細は改行を考慮
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getStyleByColumnAndRow(13, $lExcelRowCount)
								         ->getAlignment()->setWrapText(true);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION_DTL_REQUEST"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION_DTL_TSR"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION_DTL_SORTING"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["OFFSET_NG_ACTION_DTL_NOACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
								         ->setValueExplicit($lCurrentResultDataShoninRow["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

							}
						}

						//***************************************************************************************************************************
						//leave Inspection No. in master data
						//マスターデータの検査番号を退避
						$lWork1INSPECTIONNO = $lCurrentMasterDataShoninRow["INSPECTION_NO"];
						$lWork1INSPECTIONTIMEID = $lCurrentMasterDataShoninRow["INSPECTION_TIME_ID"];
						//***************************************************************************************************************************
					}


					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					//保存ファイル名とパスの設定（テンプレートファイル名の「template」を「現在の年月日_時分秒」で置換した値
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameShonin);
					$lSaveFilePathAndName = "excel/".$lSaveFileName;

					//save file（save in server temporarily）
					//ファイルの保存（サーバに一旦保存）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					//データ件数が多いと、このSAVE処理が異常に長い。保存せず、直接DL出来れば改善の可能性。
					$objWriter->save($lSaveFilePathAndName);

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete fime(saved in server)
					//ファイルの削除（サーバに保存していたもの）
					unlink($lSaveFilePathAndName);

					//message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";
				}
			}
		}
		//■■13/06/2016 Motooka NG Result■■
		elseif (Input::has('btnShoninData2'))                                   //■■■■NGResult button for approval (Excel)■■■■
		{
			//log
			Log::write('info', 'BA1010 NGResult Button Click.',
				[
					"Master No."			=> Input::get('txtMasterNoForSearch'		   	,''),
					"Process"				=> Input::get('cmbProcessForSearch'		   		,''),
					"Customer"				=> Input::get('cmbCustomerForSearch'	   		,''),
					"Check Sheet No."       => Input::get('cmbCheckSheetNoForSearch'       	,''),
					"Material Name"       	=> Input::get('cmbMaterialNameForSearch'       	,''),
					"Material Size"       	=> Input::get('cmbMaterialSizeForSearch'       	,''),
					"Revision No."          => Input::get('cmbRevisionNoForSearch'         	,''),
					"M/C No."               => Input::get('txtMachineNoForSearch'          	,''),
					"Inspector Code"        => Input::get('txtInspectorCodeForSearch'      	,''),
					"Inspection Date From"  => Input::get('txtInspectionDateFromForSearch' 	,''),
					"Inspection Date To"    => Input::get('txtInspectionDateToForSearch'   	,''),
					"Part No."              => Input::get('txtPartNoForSearch'             	,''),
				]
			);

			//check error(must check)
			$lViewData = $this->isErrorForNGResult($lViewData);

			//check error(check type)
			$lViewData = $this->isErrorForSearch($lViewData);

			//search exept for no error
			if (array_key_exists("errors", $lViewData) == false)
			{
				//get result data
				$lTblResultDatarNGResult = $this->getInspectionResultDataForNGResult();//get result data

				//error in case result data does not exist
				if (count($lTblResultDatarNGResult) == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E036 : Not Exists Target Data."
					]);
				}
				else  //data exists
				{
					//define templete Excel file
					$objReader = new PHPExcel_Reader_Excel2007();//
					$lTemplateFileNameNGResult = "InspectionResultNGData_template.xlsx";//read templete Excel file
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameNGResult);

					//active the number 0 inspection check sheet
					$objPHPExcel->setActiveSheetIndex(0); //select the first inspection check sheet
					$objSheet = $objPHPExcel->getActiveSheet(); //access to inspection check sheet selected

					//----------------------------
					//set information in header
					$lExcelRowCount = self::NGSHONINEXCEL_HEADER_STARTROW; //start header line

					$objSheet->setCellValueByColumnAndRow(0, $lExcelRowCount, Input::get('txtMasterNoForSearch'));
					$objSheet->setCellValueByColumnAndRow(1, $lExcelRowCount, Input::get('cmbProcessForSearch'));
					$objSheet->setCellValueByColumnAndRow(3, $lExcelRowCount, Input::get('cmbCheckSheetNoForSearch'));
					$objSheet->setCellValueByColumnAndRow(5, $lExcelRowCount, Input::get('cmbRevisionNoForSearch'));
					$objSheet->setCellValueByColumnAndRow(6, $lExcelRowCount, Input::get('cmbMaterialNameForSearch'));
					$objSheet->setCellValueByColumnAndRow(7, $lExcelRowCount, Input::get('cmbMaterialSizeForSearch'));
					$objSheet->setCellValueByColumnAndRow(8, $lExcelRowCount, Input::get('txtMachineNoForSearch'));
					$objSheet->setCellValueByColumnAndRow(11, $lExcelRowCount, Input::get('txtInspectionDateFromForSearch'));
					$objSheet->setCellValueByColumnAndRow(12, $lExcelRowCount, Input::get('txtInspectionDateToForSearch'));
					$objSheet->setCellValueByColumnAndRow(14, $lExcelRowCount, date("d-m-Y"));
					$objSheet->setCellValueByColumnAndRow(16, $lExcelRowCount, date("H:i:s"));

					if(Input::get('cmbCustomerForSearch') == null)
					{
					}
					else
					{
						//get result data
						$lTblResultDataHeadCustomerName = $this->getCustmerName();//get result data
						if ($lTblResultDataHeadCustomerName != null)
						{
							//change data got to Array
							$ArrResultDataHeadCustmerName = [];
							$ArrResultDataHeadCustmerName = (Array)$lTblResultDataHeadCustomerName[0];
							$objSheet->setCellValueByColumnAndRow(2, $lExcelRowCount,$ArrResultDataHeadCustmerName["CUSTOMER_NAME"] );
						}
					}
					if(Input::get('cmbCheckSheetNoForSearch') == null && Input::get('cmbRevisionNoForSearch') == null &&  Input::get('txtPartNoForSearch') == null)
					{
					}
					else
					{
						//get result data
						$lTblResultDataHeadCheckSheetName = $this->getCheckSheetName();//get result data
						if ($lTblResultDataHeadCheckSheetName != null)
						{
							foreach ($lTblResultDataHeadCheckSheetName As $lRow)
							{
								//change data got to Array
								//該当行を配列に変換
								$lTblResultDataCheckSheetNameRow = (Array)$lRow;

								if ($NoWritingFlag == 0)
								{
									if ($lWorkCheckSheetName == "")
									{
										$lWorkCheckSheetName == $lTblResultDataCheckSheetNameRow["INSPECTION_SHEET_NAME"];
										$lWorkCheckModelName == $lTblResultDataCheckSheetNameRow["MODEL_NAME"];
									}
									else
									{
										if ($lWorkCheckSheetName == $lTblResultDataCheckSheetNameRow["INSPECTION_SHEET_NAME"]
											&& $lWorkCheckModelName == $lTblResultDataCheckSheetNameRow["MODEL_NAME"])
										{
										}
										else
										{
											$NoWritingFlag = 1;
										}
									}
								}
							}
							
							if ($NoWritingFlag != 1)
							{
								$objSheet->setCellValueByColumnAndRow(4, $lExcelRowCount, $lTblResultDataCheckSheetNameRow["INSPECTION_SHEET_NAME"]);
								$objSheet->setCellValueByColumnAndRow(9, $lExcelRowCount, $lTblResultDataCheckSheetNameRow["MODEL_NAME"]);
							}
						}
					}

					if(Input::get('txtInspectorCodeForSearch') == 0)
					{
					}
					else
					{
						//get result data
						$lTblResultDataHeadUserName = $this->getInspectorName();//get result data
							if ($lTblResultDataHeadUserName != null)
							{
								//change data got to Array
								$ArrResultDataHeadUserName = [];
								$ArrResultDataHeadUserName = (Array)$lTblResultDataHeadUserName[0];
								$objSheet->setCellValueByColumnAndRow(10, $lExcelRowCount, $ArrResultDataHeadUserName["USER_NAME"]);
							}
					}


					//----------------------------
					//set infomation on detail
					//----------------------------
					$lExcelRowCount = self::NGSHONINEXCEL_MEISAI_STARTROW -1 ; //define first line of detail(at first decrease count because it increase loop counter)

					//set down data while loop result
					foreach ($lTblResultDatarNGResult As $lResultRow)
					{
						//change to Array($lTblResultDataShoninRow3 is array of $lTblResultDatarNGResult
						$lTblResultDataShoninRow3 = (Array)$lResultRow;

						//compare left check sheet No.,machine No.,Inspection No. with INSPECTIONSHEET,MACHINENO,INSPECTION_NO
						//⇒corresponding
						if(($lWorkINSPECTIONSHEET == $lTblResultDataShoninRow3["INSPECTION_SHEET_NO"])
							and ($lWorkMACHINENO == $lTblResultDataShoninRow3["MACHINE_NO"])
							and ($lWorkINSPECTIONNO == $lTblResultDataShoninRow3["INSPECTION_NO"]))
						{
						}
						//not corresponding,ＮＧflag is 0
						else
						{
							//ＮＧflag is 0
							$NGFLG = 0;
						}

						//leave Inspection No. of result data
						$lWorkINSPECTIONSHEET = $lTblResultDataShoninRow3["INSPECTION_SHEET_NO"];
						$lWorkMACHINENO       = $lTblResultDataShoninRow3["MACHINE_NO"];
						$lWorkINSPECTIONNO    = $lTblResultDataShoninRow3["INSPECTION_NO"];

						//OK data
						if ($lTblResultDataShoninRow3["INSPECTION_RESULT_TYPE"] == "1")
						{
							//flag is 0
							if ($NGFLG == 0)
							{
								//not output
								//flag leave as it is
							}
							//when flag is 1
							else
							{
								//add 1 more line at 8 line and write down data.last 2 lines are blank. 
								$objSheet->insertNewRowBefore($lExcelRowCount+2,1 );
								//add line No. to putout
								$lExcelRowCount = $lExcelRowCount + 1;

								//set down result
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MASTER_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["PROCESS_ID"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["RESULT_CLASS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["NG_OFFSET_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

								//flag is 0
								$NGFLG = 0;
							}
						}

						//inspection reult is Line Stop
						else if($lTblResultDataShoninRow3["INSPECTION_RESULT_TYPE"] == "5")
						{
							//in case TSR No. is entried
							if ($lTblResultDataShoninRow3["TSR_NO"] != null)
							{
								//flag = 1
								$NGFLG = 1;
								//add 1 more line at 8 line and write down data.last 2 lines are blank.
								$objSheet->insertNewRowBefore($lExcelRowCount+2,1 );
								//add line No. to putout
								$lExcelRowCount = $lExcelRowCount + 1;

								//set down result
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MASTER_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["PROCESS_ID"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["RESULT_CLASS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["NG_OFFSET_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
							}
							//enter nothin in TSR No.
							else
							{
								//not output
								//leave flag asa it is
							}
						}

						//in case inspection result is OFFSET,NG,No Check
						else
						{
							//change flag 1
							$NGFLG = 1;
							//add 1 line at 8 line in templete（for flame）and set down data
							//add each 1 line as data should be set down. At last line in data,leave 2 blank line（default）
							$objSheet->insertNewRowBefore($lExcelRowCount+2,1 );
							//increase number of line corresponding to outputting
							$lExcelRowCount = $lExcelRowCount + 1;

							//set down result(customer,sheet No.,sheet name,revision No.,part No.,machine No.,inspector name,inspection date（ＦＲＯＭ）,inspection date（ＴＯ）,inspection time)
								$objSheet->getCellByColumnAndRow(0, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MASTER_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(1, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["PROCESS_ID"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(2, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CUSTOMER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(3, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(4, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ITEM_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(5, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_SHEET_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(6, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(7, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(8, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["MACHINE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(9, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(10, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(11, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_TIME_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(12, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["CONDITION_CD"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(13, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(14, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(15, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_UNDER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(16, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["THRESHOLD_NG_OVER"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(17, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(18, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["RESULT_CLASS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(19, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["UPDATE_YMDHMS"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(20, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["NG_OFFSET_ACTION"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(21, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["OFFSET_NG_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(22, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["ACTION_DETAILS_TR_NO_TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(23, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(24, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TECHNICIAN_USER_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
								$objSheet->getCellByColumnAndRow(25, $lExcelRowCount)
								         ->setValueExplicit($lTblResultDataShoninRow3["TSR_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
						}

						//leave Inspection No. of result data
						$lWorkINSPECTIONSHEET = $lTblResultDataShoninRow3["INSPECTION_SHEET_NO"];
						$lWorkMACHINENO       = $lTblResultDataShoninRow3["MACHINE_NO"];
						$lWorkINSPECTIONNO    = $lTblResultDataShoninRow3["INSPECTION_NO"];
					}

					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameNGResult);
					$lSaveFilePathAndName = "excel/".$lSaveFileName;

					//save file（save in server temporarily）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					$objWriter->save($lSaveFilePathAndName); //start to output

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete file（that was saved in server）
					unlink($lSaveFilePathAndName);

					//finish message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";

				}
			}
		}
		elseif (Input::has('btnModify'))                                        //■■■■Modify button■■■■
		{
			//log
			Log::write('info', 'BA1010 Modify Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get PK in the line selected
			$lInspectionResultNo = Input::get('hidInspectionResultNo');

			//get DataRevision（for exclusive control）
			$lDataRev = Input::get('hidDataRev');

			//search data corresponding to PK in the line selected
			$lTblModifyTargetData = $this->getInspectionResultData($lInspectionResultNo, $lDataRev);

			//in case no hit to search,exclusive error
			if (count($lTblModifyTargetData) == 0)
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E998 : Data has been updated by another terminal. Try search again."
				]);
			}
			else  //data exists
			{
				//get the 0 line because header data corresponding to modify is 1.
				$lRowModifyTargetData = (Array)$lTblModifyTargetData[0];
				
				//store items in session and transport to use in Modify screen
				Session::put('BA1010InspectionResultNo'			, $lInspectionResultNo);
				Session::put('BA1010MasterNo'					, $lRowModifyTargetData["MASTER_NO"]);
				Session::put('BA1010ProcessForModify'			, $lRowModifyTargetData["PROCESS_ID"]); 
				Session::put('BA1010ProcessForModifyName'		, $lRowModifyTargetData["PROCESS_NAME"]);
				Session::put('BA1010CheckSheetNoForModify'		, $lRowModifyTargetData["INSPECTION_SHEET_NO"]);  
				Session::put('BA1010CheckSheetName'				, $lRowModifyTargetData["INSPECTION_SHEET_NAME"]);
				Session::put('BA1010RevisionNo'					, $lRowModifyTargetData["REV_NO"]);
				Session::put('BA1010ItemNo'						, $lRowModifyTargetData["ITEM_NO"]);
				Session::put('BA1010ItemName'					, $lRowModifyTargetData["ITEM_NAME"]);
				Session::put('BA1010MachineNoForModify'			, $lRowModifyTargetData["MACHINE_NO"]);
				Session::put('BA1010MachineNoForModifyName'		, $lRowModifyTargetData["MACHINE_NAME"]);
				Session::put('BA1010InspectorCodeForModify'		, $lRowModifyTargetData["INSERT_USER_ID"]); 
				Session::put('BA1010InspectorName'				, $lRowModifyTargetData["USER_NAME"]);
				Session::put('BA1010InspectionDateForModify'	, $lRowModifyTargetData["INSPECTION_YMD"]); 
				Session::put('BA1010ConditionForModify'			, $lRowModifyTargetData["CONDITION_CD"]);
				Session::put('BA1010ConditionForModifyName'		, $lRowModifyTargetData["CODEORDER_NAME"]);
				Session::put('BA1010InspectionTimeForModify'	, $lRowModifyTargetData["INSPECTION_TIME_ID"]);
				Session::put('BA1010InspectionTimeForModifyName', $lRowModifyTargetData["INSPECTION_TIME_NAME"]);
				Session::put('BA1010MaterialLotNoForModify'		, $lRowModifyTargetData["MATERIAL_LOT_NO"]);
				Session::put('BA1010CertificateNoForModify'		, $lRowModifyTargetData["CERTIFICATE_NO"]);
				Session::put('BA1010PoNoForModify'				, $lRowModifyTargetData["PO_NO"]);
				Session::put('BA1010InvoiceNoForModify'			, $lRowModifyTargetData["INVOICE_NO"]);
				Session::put('BA1010CustomerName'				, $lRowModifyTargetData["CUSTOMER_NAME"]);
				Session::put('BA1010ModelName'					, $lRowModifyTargetData["MODEL_NAME"]);  
				Session::put('BA1010DrawingNo'					, $lRowModifyTargetData["DRAWING_NO"]);  
				Session::put('BA1010ISOKanriNo'					, $lRowModifyTargetData["ISO_KANRI_NO"]);
				Session::put('BA1010ModifyDataRev'				, $lRowModifyTargetData["DATA_REV"]); 
				Session::put('BA1010MaterialNameForModify'		, $lRowModifyTargetData["MATERIAL_NAME"]); 
				Session::put('BA1010MaterialSizeForModify'		, $lRowModifyTargetData["MATERIAL_SIZE"]); 

				//■■ModifyなのにEntryをセッションに入れる？
				Session::put('BA1010ProcessForEntry', Input::get('cmbProcessForEntry'));
				Session::put('BA1010CustomerForEntry', Input::get('cmbCustomerForEntry'));
				Session::put('BA1010CheckSheetNoForEntry', Input::get('cmbCheckSheetNoForEntry'));
				
				Session::put('BA1010CustomerForSearch', Input::get('cmbCustomerForSearch'));
				Session::put('BA1010ProcessForSearch', Input::get('cmbProcessForSearch'));
				Session::put('BA1010CheckSheetNoForSearch', Input::get('cmbCheckSheetNoForSearch'));
				//■■13/06/2016 Motooka NG Result■■
				Session::put('BA1010InspectionTimeForSearch', Input::get('cmbInspectionTimeForSearch'));

				//echo $lRowModifyTargetData["MASTER_NO"]; die();

				//transition to entry screen
				return Redirect::route("user/entry");
			}
		}
		elseif (Input::has('btnLineStop'))                                      //■■■■LineStop button■■■■
		{
			//log
			Log::write('info', 'BA1010 Line Stop Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get primary key in selected line
			$lInspectionResultNo = Input::get('hidInspectionResultNo');
			
			//get DataRevision
			$lDataRev = Input::get('hidDataRev');

			//in case primary key exists in selected line
			if ($lInspectionResultNo != "")
			{
				//echo "Debug Line Stop"; die();
				//make Line Stop data towards no entry Inspection No.
				$this->insertLineStopData($lInspectionResultNo, $lDataRev, $lViewData["UserID"]);

				//message
				$lViewData["NormalMessage"] = "I005 : Process has been completed.";

				//delete information of search（reset）
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnDelete'))                                        //■■■■Delete button■■■■
		{
			//log
			Log::write('info', 'BA1010 Delete Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);

			//get primary key in selected line
			$lInspectionResultNo = Input::get('hidInspectionResultNo');

			//get DataRevision
			$lDataRev = Input::get('hidDataRev');

			//in case primary key exists in selected line
			if ($lInspectionResultNo != "")
			{
				//delete
				$lDeleteCount = $this->deleteInspectionResultData($lInspectionResultNo, $lDataRev);

				//in case delete count is 0,error
				if ($lDeleteCount == 0)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E998 : Data has been updated by another terminal. Try search again."
					]);
				}
				else
				//if it delete successfully,display message
				{
					//message
					$lViewData["NormalMessage"] = "I004 : Delete has been completed.";
				}

				//delete information of search（reset）
				$this->initializeSessionData();
			}
		}
		elseif (Input::has('btnPrintInspectionSheet'))                          //■■■■Print Inspection Sheet button■■■■
		{
			//log
			Log::write('info', 'BA1010 Inspection Sheet Print Button Click.',
				[
					"Inspection Result No."  => Input::get('hidInspectionResultNo' ,''),
					"Data Revision"          => Input::get('hidDataRev'            ,''),
				]
			);
           
			//get primary key in selected line
			$lInspectionResultNo = Input::get('hidInspectionResultNo'); //46
           
			//get DataRevision
			$lDataRev = Input::get('hidDataRev'); //1

			//in case primary key exists in selected line
			if ($lInspectionResultNo != null)
			{
				//search transaction data
			
				$lTblResult3TableData = $this->getResult3TableData($lInspectionResultNo, $lDataRev);
				
				//error in case result data does not exist
				if ($lTblResult3TableData == null)
				{
					//error message
					$lViewData["errors"] = new MessageBag([
						"error" => "E997 : Target data does not exist."
					]);
				}
				else  //data exists
				{
					$lTblResult3TableDataRow = (Array)$lTblResult3TableData[0];
				 
					$lTblMasterDataInspectionSheet = $this->getMasterDataInspectionSheet($lTblResult3TableDataRow["MASTER_NO"], $lTblResult3TableDataRow["PROCESS_ID"]
					, $lTblResult3TableDataRow["INSPECTION_SHEET_NO"], $lTblResult3TableDataRow["REV_NO"], $lTblResult3TableDataRow["INSPECTION_TIME_ID"]);

					$lTblMasterDataInspectionSheetHistory = $this->getMasterDataInspectionSheetHistory($lTblResult3TableDataRow["MASTER_NO"], $lTblResult3TableDataRow["PROCESS_ID"]
					, $lTblResult3TableDataRow["INSPECTION_SHEET_NO"]);

					$lTblMasterDataInspectionSheetRow = (Array)$lTblMasterDataInspectionSheet[0];

					//Keep pic URL
					$PrintPicURL = $lTblMasterDataInspectionSheetRow["PICTURE_URL"];
  
					//get Max Analytical Group
					$lTblMasterDataMaxAnalyticalGrp = $this->getMasterDataMaxAnalyticalGrp($lTblResult3TableDataRow["MASTER_NO"], $lTblResult3TableDataRow["PROCESS_ID"]
					, $lTblResult3TableDataRow["INSPECTION_SHEET_NO"], $lTblResult3TableDataRow["REV_NO"], $lTblResult3TableDataRow["INSPECTION_TIME_ID"]);

					$lTblMasterDataMaxAnalyticalGrpRow = (Array)$lTblMasterDataMaxAnalyticalGrp[0];

					//define templete Excel file
					$objReader = new PHPExcel_Reader_Excel2007();

					//read templete Excel file
					$lTemplateFileNameInspectionSheet = "InspectionSheet_template.xlsx";
					
					$objPHPExcel = $objReader->load("excel/".$lTemplateFileNameInspectionSheet);
					// $objPHPExcel = $objReader->load(public_path()."/excel/".$lTemplateFileNameInspectionSheet);

					//active the number 0 inspection check sheet
					$objPHPExcel->setActiveSheetIndex(0);//select the first inspection check sheet

					$objSheet = $objPHPExcel->getActiveSheet(); //access to inspection check sheet selected

					//----------------------------
					//set information in header
					//----------------------------
					//$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTCOL; //start header posision
					$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTROW; //start header posision 
					$objSheet->setCellValueByColumnAndRow(10, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["MASTER_NO"]);
					$objSheet->setCellValueByColumnAndRow(12, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["CUSTOMER_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1; 

					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["ITEM_NO"]);
					$objSheet->setCellValueByColumnAndRow(16, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["MATERIAL_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["ITEM_NAME"]);
					$objSheet->setCellValueByColumnAndRow(16, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["MATERIAL_SIZE"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(16, $lInspectionSheetRowCount, $lTblResult3TableDataRow["MATERIAL_LOT_NO"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblResult3TableDataRow["QUANTITY_WEIGHT"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblMasterDataInspectionSheetRow["PRODUCT_WEIGHT"]);
					$objSheet->setCellValueByColumnAndRow(20, $lInspectionSheetRowCount, $lTblResult3TableDataRow["USER_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$objSheet->setCellValueByColumnAndRow(4, $lInspectionSheetRowCount, $lTblResult3TableDataRow["LOT_NO"]);
					$objSheet->setCellValueByColumnAndRow(20, $lInspectionSheetRowCount, date('d/m/Y',strtotime($lTblResult3TableDataRow["INSERT_YMDHMS"])));
					

					//----------------------------
					//set information in picture
					//----------------------------

					//画像が存在する場合
					if (file_exists($PrintPicURL))
					{
						///画像用のオプジェクト作成
						$objDrawing = new PHPExcel_Worksheet_Drawing();

						//画像の保存元ディレクトリ

						//貼り付ける画像のパスを指定
						$objDrawing->setPath($PrintPicURL);

						$objDrawing->setHeight(450);////画像の高さを指定

						//画像のプロパティを見たときに表示される情報を設定
						//画像名
						$objDrawing->setName('Image');
						//画像の概要
						$objDrawing->setDescription('Image');
						//位置
						$objDrawing->setCoordinates('B13');
						//横方向へ何ピクセルずらすかを指定
						$objDrawing->setOffsetX(10);
						//回転の角度
						//$objDrawing->setRotation(25);
						//ドロップシャドウをつけるかどうか
						//$objDrawing->getShadow()->setVisible(true);

						//PHPExcelオブジェクトに張り込み
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					}
					else
					{
						//画像が存在しない場合は画像ファイルについては何もしない
					}



					//----------------------------
					//set information in History
					//----------------------------
					
					$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_HISTORY_STARTCOL; //start detail posision
					$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_HISTORY_STARTROW; //start detail posision
			
					$lWorkHistoryCount = 0;
					
					foreach ($lTblMasterDataInspectionSheetHistory As $lMasterHistoryRow)
					{
						if ($lWorkHistoryCount < 7)
						{
							//change corresponding line to Array
							$lCurrentMasteHistoryrRow = (Array)$lMasterHistoryRow;
							
							$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_HISTORY_STARTCOL;
							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["REV_NO"], PHPExcel_Cell_DataType::TYPE_STRING);

							$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["REV_YMD"], PHPExcel_Cell_DataType::TYPE_STRING);

							$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["DRAWING_NO"], PHPExcel_Cell_DataType::TYPE_STRING);
							
							$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["CHANGE_REV_REASON"], PHPExcel_Cell_DataType::TYPE_STRING);
							
							$lInspectionSheetColCount = $lInspectionSheetColCount + 9;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["REV_PREPARE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
							
							$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasteHistoryrRow["REV_APPROVE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
							
							$lWorkHistoryCount = $lWorkHistoryCount + 1; //1
							
						}
					}

					//----------------------------
					//set information in detail
					//----------------------------

					//in case data exists more than 2line(default),add lines
					//テンプレートのデフォルト行数(2行)以上にデータがある場合は、その分の行を追加
					//if (count($lTblMasterDataInspectionSheet) > 2)
					//{
					//	$objSheet->insertNewRowBefore(self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW + 2, $lTblMasterDataMaxAnalyticalGrpRow["MAX_ANALYTICAL_GRP"] - 2);
					//}

					$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTCOL; //start detail posision
					$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW; //start detail posision
					foreach ($lTblMasterDataInspectionSheet As $lMasterRow)
					{
						//change corresponding line to Array
						$lCurrentMasterRow = (Array)$lMasterRow;
						//***************************************************
						//leave item in master data (keep 退避)
						$lWork2InspectionNo              = $lCurrentMasterRow["INSPECTION_NO"];
						$lWork2AnalyGrp                  = $lCurrentMasterRow["ANALYTICAL_GRP"];
						$lWork2AnalyGrpHowmany           = $lCurrentMasterRow["ANALYTICAL_GRP_HOWMANY"];
						$lWork2InspectionResultInputType = $lCurrentMasterRow["INSPECTION_RESULT_INPUT_TYPE"];
						//***************************************************

						if($lWork2AnalyGrp == $lWork1AnalyGrp)
						{
							//何もしない
						}
						else
						{
							if ($lWork1InspectionNo != 0)
							{
								if ($lWorkFindAnalyGrpFlg == 1)
								{
								    
									if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
									{
										$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_XBAR_STARTPOSITION;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkAverageValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkMaxValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkMinValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkRangeValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkStdevValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);
									}
									else if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
									{
										$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_XBAR_STARTPOSITION;
										$lInspectionSheetColCount = $lInspectionSheetColCount + 4;
									}

									//if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
									//{
										//if ($lWorkJudgeReason == null)
										//{
										//	$lInspectionResultValue = self::JUDGE_OK;
										//}
										//else
										//{
										//	$lInspectionResultValue = self::JUDGE_NG;
										//}
									//}
									//else if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
									//{
										//if ($lWorkInspectionResultValue == 1)
										//{
										//	$lInspectionResultValue = self::JUDGE_OK;
										//}
										//else
										//{
										//	$lInspectionResultValue = self::JUDGE_NG;
										//}
									//}
									//else
									//{
									//	$lInspectionResultValue = "";
									//}
									
									if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
									{
										if ($lWorkJudgeResultValue == 1)
										{
											$lInspectionResultValue = self::JUDGE_OK;
										}
										else
										{
											$lInspectionResultValue = self::JUDGE_NG;
										}
									}
									else if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
									{
										if ($lWorkJudgeResultValue == 1)
										{
											$lInspectionResultValue = self::JUDGE_OK;
										}
										else
										{
											$lInspectionResultValue = self::JUDGE_NG;
										}
									}
									else
									{
										$lInspectionResultValue = "";
									}
								}
								else
								{
									$lInspectionResultValue = "";
								}
								
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit($lInspectionResultValue, PHPExcel_Cell_DataType::TYPE_STRING);

								//setting →Next →clear
								$lWorkAverageValue               = 0;
								$lWorkMinValue                   = 0;
								$lWorkMaxValue                   = 0;
								$lWorkRangeValue                 = 0;
								$lWorkStdevValue                 = 0;
								$lWorkJudgeResultValue           = 1;
								//$lWorkJudgeReason                = "";
								//$lWorkInspectionResultValue      = "";
							}
							
							//初期化
							$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTCOL;

							//初期化
							$lWorkCount = 0;
							$lWorkFindAnalyGrpFlg = 0;

							$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
							//Noは固定
							//$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							//         ->setValueExplicit($lCurrentMasterRow["ANALYTICAL_GRP"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasterRow["INSPECTION_ITEM"], PHPExcel_Cell_DataType::TYPE_STRING);

							if ($lWork2InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
							{
								if ($lCurrentMasterRow["REFER_TO_DOCUMENT_FREQUENCY"] != "")
								{
									$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
									$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit($lCurrentMasterRow["REFER_TO_DOCUMENT_FREQUENCY"], PHPExcel_Cell_DataType::TYPE_STRING);
									
									//測定値印字位置を合わせる
									$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
								}
								else
								{
									$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
									$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit($lCurrentMasterRow["REFERENCE_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

									$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
									$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit(number_format(($lCurrentMasterRow["REFERENCE_VALUE"] - $lCurrentMasterRow["THRESHOLD_NG_OVER"]) * -1, 4), PHPExcel_Cell_DataType::TYPE_NUMERIC);

									$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
									$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit(number_format(($lCurrentMasterRow["REFERENCE_VALUE"] - $lCurrentMasterRow["THRESHOLD_NG_UNDER"]) * -1, 4), PHPExcel_Cell_DataType::TYPE_NUMERIC);
								}
							}
							
							else if ($lWork2InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								         ->setValueExplicit($lCurrentMasterRow["REFER_TO_DOCUMENT_FREQUENCY"], PHPExcel_Cell_DataType::TYPE_STRING);
								
								//測定値印字位置を合わせる
								$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
							}
							else
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 4;
							}

							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							         ->setValueExplicit($lCurrentMasterRow["INSPECTION_TOOL_CLASS"], PHPExcel_Cell_DataType::TYPE_STRING);
							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						}

						if ($lWorkCount < self::INSPECTIONSHEETEXCEL_MEISAI_NUMBER)
						{

							foreach ($lTblResult3TableData As $lResult3TablRow)
							{
								//change corresponding line to Array
								$lCurrentResult3TablRow = (Array)$lResult3TablRow;

								if ($lWorkFindResultFlg == 0)
								{  
								 
									// if($lWork2InspectionNo == $lCurrentResult3TablRow["INSPECTION_NO"])
									if(strcmp($lWork2InspectionNo, $lCurrentResult3TablRow["INSPECTION_NO"]) == 0)	
									{
										//計測検査の場合
										if ($lWork2InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
										{
											$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
											$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lCurrentResult3TablRow["INSPECTION_RESULT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);

											$lWorkAverageValue = $lCurrentResult3TablRow["AVERAGE_VALUE"];
											$lWorkMaxValue = $lCurrentResult3TablRow["MAX_VALUE"];
											$lWorkMinValue = $lCurrentResult3TablRow["MIN_VALUE"];
											$lWorkRangeValue = $lCurrentResult3TablRow["RANGE_VALUE"];
											$lWorkStdevValue = $lCurrentResult3TablRow["STDEV"];
											
											//測定結果OKでない場合は退避⇒一番右側のjudgeで使用
											if ($lCurrentResult3TablRow["INSPECTION_RESULT_TYPE"] != 1)
											{
												$lWorkJudgeResultValue = $lCurrentResult3TablRow["INSPECTION_RESULT_TYPE"];
											}
										}
										//ビジュアル検査の場合
										else if ($lWork2InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
										{
											if ($lCurrentResult3TablRow["INSPECTION_RESULT_VALUE"] == 1)
											{
												$lInspectionResultOKNGValue = self::JUDGE_OK;
											}
											else
											{
												$lInspectionResultOKNGValue = self::JUDGE_NG;
											}
											
											$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
											$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lInspectionResultOKNGValue, PHPExcel_Cell_DataType::TYPE_STRING);
											
											//測定結果OKでない場合は退避⇒一番右側のjudgeで使用
											if ($lCurrentResult3TablRow["INSPECTION_RESULT_VALUE"] != 1)
											{
												$lWorkJudgeResultValue = $lCurrentResult3TablRow["INSPECTION_RESULT_VALUE"];
											}
										}
										else
										{
											//何もしない
										}
										
										//退避する
										//$lWorkJudgeReason = $lCurrentResult3TablRow["JUDGE_REASON"];
										//$lWorkInspectionResultValue = $lCurrentResult3TablRow["INSPECTION_RESULT_VALUE"];
										
										//フラグを立てる
										$lWorkFindResultFlg = 1;
									}
								}
							}

							//データを飛び飛びで測定・入力した場合
							if ($lWorkFindResultFlg == 0)
							{
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
							}
						}

						if ($lWorkFindResultFlg == 1)
						{
							$lWorkFindAnalyGrpFlg = 1;
						}
						
						//***************************************************
						//leave item in master data (退避)
						$lWork1InspectionNo              = $lWork2InspectionNo;
						$lWork1AnalyGrp                  = $lWork2AnalyGrp;
						$lWork1AnalyGrpHowmany           = $lWork2AnalyGrpHowmany;
						$lWork1InspectionResultInputType = $lWork2InspectionResultInputType;
						//clear（初期化）
						$lWorkFindResultFlg              = 0;
						//CountUp（カウントアップ）
						$lWorkCount                      = $lWorkCount + 1;
						//***************************************************
						
					}
						if($lWork2AnalyGrp == $lWork1AnalyGrp)
						{
							//何もしない
							if ($lWork1InspectionNo != 0)
							{
								if ($lWorkFindAnalyGrpFlg == 1)
								{
								
									if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
									{
										$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_XBAR_STARTPOSITION;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkAverageValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkMaxValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkMinValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkRangeValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);

										$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
										$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											         ->setValueExplicit($lWorkStdevValue, PHPExcel_Cell_DataType::TYPE_NUMERIC);
									}
									else if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
									{
										$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_XBAR_STARTPOSITION;
										$lInspectionSheetColCount = $lInspectionSheetColCount + 4;
									}
									if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
									{
										if ($lWorkJudgeResultValue == 1)
										{
											$lInspectionResultValue = self::JUDGE_OK;
										}
										else
										{
											$lInspectionResultValue = self::JUDGE_NG;
										}
									}
									else if ($lWork1InspectionResultInputType == self::RESULT_INPUT_TYPE_OKNG)
									{
										if ($lWorkJudgeResultValue == 1)
										{
											$lInspectionResultValue = self::JUDGE_OK;
										}
										else
										{
											$lInspectionResultValue = self::JUDGE_NG;
										}
									}
									else
									{
										$lInspectionResultValue = "";
									}
								}
								else
								{
									$lInspectionResultValue = "";
								}
								
								$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
								$objSheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									         ->setValueExplicit($lInspectionResultValue, PHPExcel_Cell_DataType::TYPE_STRING);
							}
						}

					//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
					//保存ファイル名とパスの設定（テンプレートファイル名の「template」を「現在の年月日_時分秒」で置換した値
					$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet); 
					$lSaveFilePathAndName = "excel/".$lSaveFileName;  
					// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

					//save file（save in server temporarily）
					//ファイルの保存（サーバに一旦保存）
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//if many data exist,it need very long time for SAVE.(it is available to download directly without save
					//データ件数が多いと、このSAVE処理が異常に長い。保存せず、直接DL出来れば改善の可能性。
					$objWriter->save($lSaveFilePathAndName);

					//download
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Length: '.filesize($lSaveFilePathAndName));
					header('Content-disposition: attachment; filename='.$lSaveFileName);
					readfile($lSaveFilePathAndName);

					//delete fime(saved in server)
					//ファイルの削除（サーバに保存していたもの）
					unlink($lSaveFilePathAndName);

					//message
					$lViewData["NormalMessage"] = "I005 : Process has been completed.";
				}
			}
			else
			{
				//error message
				$lViewData["errors"] = new MessageBag([
					"error" => "E997 : Target data does not exist."
				]);
			}
		}
		else                                                                    //■■■■transition, paging from other screen or menu■■■■
		{
			//当画面のセッション情報のうち、画面入力情報以外の情報を全てクリアする（検索結果等を消して、検索前の状態に戻すため）
			//遷移元のURLに「index.php/user/list含まない場合」（＝他画面からの遷移の場合）＝False
			//遷移元がそれ以外の場合は文字列が返ってくる（Falseにはならない）
			//clear information except for entry in screen in session（delete search result and restore before state
			//in case index.php/user/list do not include in URL,(＝transition from other screen）＝False
			//don't be False
			if(isset($_SERVER['HTTP_REFERER']) == true)
			{
				$lPrevURL = stristr($_SERVER['HTTP_REFERER'],'index.php/user/list');

				if($lPrevURL == false)
				{
					//delete information of search（reset）
					$this->initializeSessionData();
					
					//if transition from other screen,get value from session
					$lCmbValMasterNoForEntry = Session::get('BA1010MasterNoForEntry');
					$lCmbValProcessForEntry = Session::get('BA1010ProcessForEntry');
					$lCmbValCustomerForEntry = Session::get('BA1010CustomerForEntry');
					$lCmbValCheckSheetNoForEntry = Session::get('BA1010CheckSheetNoForEntry');
					
					$lCmbValMasterNoForSearch = Session::get('BA1010MasterNoForSearch');
					$lCmbValProcessForSearch = Session::get('BA1010ProcessForSearch');
					$lCmbValCustomerForSearch = Session::get('BA1010CustomerForSearch');
					$lCmbValCheckSheetNoForSearch = Session::get('BA1010CheckSheetNoForSearch');
					
					//上に場所移動
					//delete information of search（reset）
					//$this->initializeSessionData();
				}
			}
		}


		//以下の処理は他画面またはメニューからの遷移、ページング時、ボタン処理時等、全ての場合で通過させる
		//■must pass (transition,paging,button, from other screen, menu)

		//set list of Inspection Time
		$lViewData = $this->setInspectionTimeList($lViewData);

		//set list of condition
		$lViewData = $this->setConditionList($lViewData);

		//if list of search result does not exist in session, set blank in
		if (is_null(Session::get('BA1010InspectionResultData')))
		{
			$lTblSearchResultData = [];
			$lTblNotExistNoData   = [];
		}
		else
		{
			//pop up data in session
			$lTblSearchResultData = Session::get('BA1010InspectionResultData');
			$lTblNotExistNoData   = Session::get('BA1010NotExistNoData');
		}

		//make pagenation
		$lPagenation = Paginator::make($lTblSearchResultData,Count($lTblSearchResultData),self::NUMBER_PER_PAGE); //data,total number of issue,number per 1page

		//add to array for transportion to screen(written in +=))
		$lViewData += [
			"Pagenator"       => $lPagenation,
			"NotExistNoData"  => $lTblNotExistNoData,
		];

		//画面入力項目の保持と再設定
		//store and set again entry in screen
		$lViewData = $this->setListForms($lViewData);


		//-----------------------------
		//set combo process (for Entry)
		//-----------------------------
		$lViewData = $this->setProcessList($lViewData);

		//in case Process data(for entry) is posted from screen
		if (Input::has('cmbProcessForEntry'))
		{
			$lViewData += [
				"ProcessForEntry" => (String)Input::get('cmbProcessForEntry'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData += [
				"ProcessForEntry" => "",
			];
		}

		//-----------------------------
		//set combo process (for Search)
		//-----------------------------
		//in case Process data(for search) is posted from screen
		if (Input::has('cmbProcessForSearch'))
		{
			$lViewData += [
				"ProcessForSearch" => (String)Input::get('cmbProcessForSearch'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData += [
				"ProcessForSearch" => "",
			];
		}

		//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■Add Hoshina 04-Aug-2017 KOHBYO
		//-----------------------------
		//set combo ①etc (for Entry)
		//-----------------------------
		if (Input::has('txtMasterNoForEntry') && Input::has('cmbProcessForEntry'))
		{
			//in case value exist in customer infomation for search
			$lViewData = $this->setCustomerNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), "arrDataListCustomerList");

			if (count($lViewData['arrDataListCustomerList']) == 1)
			{
				$lTblCustomerID = $this->getCustomerID(Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'));
				
				if($lTblCustomerID != null)
				{
					$lRowCustomerID = (Array)$lTblCustomerID[0];
					//in case value exist in customer infomation for search
					//$pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey
					$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], "arrCheckSheetNoForEntryList");

						if (Input::has('cmbCheckSheetNoForEntry'))
						{
							//in case value exist in customer infomation for search
							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialNameForEntryList");
							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForSearch, "arrMaterialSizeForEntryList");
						}
						else
						{
							$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
							$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), $lRowCustomerID["CUSTOMER_ID"], $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
						}
				}
			}
		}
		//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■


		//-----------------------------
		//set combo customer (for Entry)
		//-----------------------------
		$lViewData = $this->setCustomerList($lViewData);

		//in case Customer(for entry) is posted from screen
		if (Input::has('cmbCustomerForEntry'))
		{
			$lViewData += [
				"CustomerForEntry" => (String)Input::get('cmbCustomerForEntry'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData += [
				"CustomerForEntry" => "",
			];
		}

		//-----------------------------
		//set combo customer (for Search)
		//-----------------------------
		//in case Customer(for search) is posted from screen
		if (Input::has('cmbCustomerForSearch'))
		{
			$lViewData += [
				"CustomerForSearch" => (String)Input::get('cmbCustomerForSearch'),
			];
		}
		else
		//select the first line in case Equipment is not posted from screen
		{
			$lViewData += [
				"CustomerForSearch" => "",
			];
		}


		//-----------------------------
		//set combo ①Inspection Sheet No ②MaterialName ③MaterialSize (for Entry)
		//-----------------------------
		if (Input::has('cmbProcessForEntry') && Input::has('cmbCustomerForEntry'))
		{

			//in case value exist in customer infomation for search
			$lViewData = $this->setCheckSheetNoListEntry($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), "arrCheckSheetNoForEntryList");
			
			if (Input::has('cmbCheckSheetNoForEntry'))
			{
				//in case value exist in customer infomation for search
				$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), Input::get('cmbCheckSheetNoForEntry'), "arrMaterialNameForEntryList");
				$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), Input::get('cmbCheckSheetNoForEntry'), "arrMaterialSizeForEntryList");
			}
			else
			{
				$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
				$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForEntry'), Input::get('cmbProcessForEntry'), Input::get('cmbCustomerForEntry'), $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
			}
		}
		else
		{
			//in case value does not exist in customer infomation for search
			$lViewData = $this->setCheckSheetNoListEntry($lViewData, $lCmbValMasterNoForEntry, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, "arrCheckSheetNoForEntryList");
			$lViewData = $this->setMaterialNameList($lViewData, $lCmbValMasterNoForEntry, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, $lCmbValCheckSheetNoForEntry, "arrMaterialNameForEntryList");
			$lViewData = $this->setMaterialSizeList($lViewData, $lCmbValMasterNoForEntry, $lCmbValProcessForEntry, $lCmbValCustomerForEntry, $lCmbValCheckSheetNoForEntry, "arrMaterialSizeForEntryList");
		}

		//-----------------------------
		//set combo ①Inspection Sheet No (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch'))
		{
			//in case value exist in customer infomation for search
			$lViewData = $this->setCheckSheetNoListSearch($lViewData, Input::get('txtMasterNoForSearch'), Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), "arrCheckSheetNoForSearchList");
		}
		else
		{
			//in case value does not exist in customer infomation for search
			$lViewData = $this->setCheckSheetNoListSearch($lViewData, $lCmbValMasterNoForSearch, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, "arrCheckSheetNoForSearchList");
		}

		//-----------------------------
		//set combo ①Revision No ②MaterialName ③MaterialSize (for Search)
		//-----------------------------
		if (Input::has('cmbProcessForSearch') && Input::has('cmbCustomerForSearch') && Input::has('cmbCheckSheetNoForSearch'))
		{
			//in case value exits in both Inspection No. infomation for search and customer infomation for search
			$lViewData = $this->setRevNoList($lViewData, Input::get('txtMasterNoForSearch'), Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'));
			$lViewData = $this->setMaterialNameList($lViewData, Input::get('txtMasterNoForSearch'), Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'), "arrMaterialNameForSearchList");
			$lViewData = $this->setMaterialSizeList($lViewData, Input::get('txtMasterNoForSearch'), Input::get('cmbProcessForSearch'), Input::get('cmbCustomerForSearch'), Input::get('cmbCheckSheetNoForSearch'), "arrMaterialSizeForSearchList");
		}
		else
		{
			//in case value does not exit in Inspection No. for search
			$lViewData = $this->setRevNoList($lViewData, $lCmbValMasterNoForSearch, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch);
			$lViewData = $this->setMaterialNameList($lViewData, $lCmbValMasterNoForSearch, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch, "arrMaterialNameForSearchList");
			$lViewData = $this->setMaterialSizeList($lViewData, $lCmbValMasterNoForSearch, $lCmbValProcessForSearch, $lCmbValCustomerForSearch, $lCmbValCheckSheetNoForSearch, "arrMaterialSizeForSearchList");
		}

		return View::make("user/list", $lViewData);
	}

	//**************************************************************************
	// process         setInspectionTimeList
	// overview        display list of Inspection Time on screen. control value in session
	// argument        Array
	// return value    Array
	// author          s-miyamoto
	// date            2014.05.23
	// record of updates  2014.05.23 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function setInspectionTimeList($pViewData)
	{

		$lArrDataListInspectionTime     = ["" => ""];  //list of Inspection Time to return to screen

		//if list of Inspection Time does not exist in session, search and store in session
		if (is_null(Session::get('BA1010InspectionTimeDropdownListData'))) {

			//search inspection time
			$lArrDataListInspectionTime = $this->getInspectionTimeList();

			//sessionにstore
			Session::put('BA1010InspectionTimeDropdownListData', $lArrDataListInspectionTime);
		}
		else
		{
			//pop up data in session
			$lArrDataListInspectionTime = Session::get('BA1010InspectionTimeDropdownListData');
		}

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListInspectionTime" => $lArrDataListInspectionTime
		];

		return $pViewData;
	}

	//**************************************************************************
	// process    getInspectionTimeList
	// overview      search list of Inspection Time and return it to calling as Array
	// argument      Nothing
	// return value    Array
	// author    s-miyamoto
	// date    2014.05.20
	// record of updates  2014.05.20 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getInspectionTimeList()
	{
		$lTblInspectionTime         = [];         //DataTable（for Inspection Time data）
		$lRowInspectionTime         = [];         //DataRow（for Inspection Time data）
		$lArrDataListInspectionTime = ["" => ""]; //list of Inspection Time to return to screen

		$lTblInspectionTime = DB::select('
			  SELECT INSPECTION_TIME_ID
			        ,INSPECTION_TIME_NAME
			    FROM TINSPTIM
			   WHERE DELETE_FLG = "0"
			ORDER BY DISPLAY_ORDER
			        ,INSPECTION_TIME_ID
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblInspectionTime = (array)$lTblInspectionTime;

		//data exist
		if ($lTblInspectionTime != null)
		{

			//store result in Array again
			foreach ($lTblInspectionTime as $lRowInspectionTime) {

				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowInspectionTime = (Array)$lRowInspectionTime;

				$lArrDataListInspectionTime += [
					$lRowInspectionTime["INSPECTION_TIME_ID"] => $lRowInspectionTime["INSPECTION_TIME_NAME"]
				];

			}
		}

		return $lArrDataListInspectionTime;
	}


	//**************************************************************************
	// process    setConditionList
	// overview      display list of Condition on screen.control value in session
	// argument      Array
	// return value    Array
	// author    m-hoshina
	// date    2015.05.16
	// record of updates  //■■16/05/2015 Hoshina added Condition■■
	//
	//**************************************************************************
	private function setConditionList($pViewData)
	{

		$lArrDataListCondition     = ["" => ""];  //list of condition data to return to screen

		//if list of Condition in session,search and store in session
		if (is_null(Session::get('BA1010ConditionDropdownListData')))
		{
			//search inspection time
			$lArrDataListCondition = $this->getConditionList();

			//sessionにstore
			Session::put('BA1010ConditionDropdownListData', $lArrDataListCondition);
		}
		else
		{
			//pop up data in session
			$lArrDataListCondition = Session::get('BA1010ConditionDropdownListData');
		}

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			"arrDataListCondition" => $lArrDataListCondition
		];

		return $pViewData;
	}

	//**************************************************************************
	// process    getConditionList
	// overview      search list of condition and return it to calling as Array
	// argument      Nothing
	// return value    Array
	// author    m-hoshina
	// date    2015.05.16
	// record of updates  //■■16/05/2015 Hoshina added Condition■■
	//
	//**************************************************************************
	private function getConditionList()
	{
		$lTblCondition         = [];         //DataTable（for Condition data）
		$lRowCondition         = [];         //DataRow（for Condition data）
		$lArrDataListCondition = ["" => ""]; //list of condition data to return to screen

		$lTblCondition = DB::select('
			  SELECT CODE_ORDER
			        ,CODEORDER_NAME
			    FROM TCODEMST
			   WHERE CODE_NO = "001"
			ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCondition = (array)$lTblCondition;

		//data exist
		if ($lTblCondition != null)
		{

			//store result in Array again
			foreach ($lTblCondition as $lRowCondition)
			{
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCondition = (Array)$lRowCondition;

				$lArrDataListCondition += [
					$lRowCondition["CODE_ORDER"] => $lRowCondition["CODEORDER_NAME"]
				];
			}
		}

		return $lArrDataListCondition;
	}


	//**************************************************************************
	// process       getInspectionResultList
	// overview      search list of inspection result and return it to calling as Array
	// overview      検査結果実績のリストを検索して、呼び出し元に配列で返却する
	// argument      Nothing
	// return value  Array
	// author        s-miyamoto
	// date          2014.05.21
	// record of updates  2014.05.21 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    ■■16/05/2015 Hoshina added Condition■■
	//                    ■■14/09/2017 Nawaphat select Lot No. ■■
	//**************************************************************************
	private function getInspectionResultList()
	{
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

			$lTblSearchResultData = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
			          ,HEAD.MASTER_NO 
			          ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
			          ,CUSTOMER.CUSTOMER_NAME AS CUSTOMER_NAME
			          ,HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,SHEE.ITEM_NO
			          ,SHEE.ITEM_NAME
			          ,SHEE.MATERIAL_NAME
			          ,SHEE.MATERIAL_SIZE
			          ,HEAD.LOT_NO
			          ,MACH.MACHINE_NAME
			          ,HEAD.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,CODE.CODEORDER_NAME
			          ,TIME.INSPECTION_TIME_NAME
			          ,IFNULL(SUB2.RESULT_NO_COUNT,0) AS RESULT_NO_COUNT
			          ,SUB1.TOTAL_NO_COUNT
			          ,ROUND((IFNULL(SUB2.RESULT_NO_COUNT,0) / SUB1.TOTAL_NO_COUNT) * 100, 2) AS RESULTRATE
			          ,HEAD.DATA_REV
			      FROM TRESHEDT AS HEAD /* 検査実績ヘッダ */

			INNER JOIN TISHEETM AS SHEE /* 検査シートマスタ */
			        ON HEAD.MASTER_NO           = SHEE.MASTER_NO
			       AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			       AND HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND HEAD.REV_NO              = SHEE.REV_NO

			INNER JOIN TMACHINM AS MACH /* Machineマスタ */
			        ON HEAD.MACHINE_NO          = MACH.MACHINE_NO

			INNER JOIN TCUSTOMM AS CUSTOMER /* 顧客マスタ */
					ON SHEE.CUSTOMER_ID         = CUSTOMER.CUSTOMER_ID

			INNER JOIN TUSERMST AS USER /* Userマスタ */
			        ON HEAD.INSERT_USER_ID      = USER.USER_ID

			INNER JOIN TINSPTIM AS TIME /* 検査時間・回数マスタ */
			        ON HEAD.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID

			INNER JOIN TCODEMST AS CODE /* コードマスタ */
			        ON CODE.CODE_NO             = "001"
			       AND HEAD.CONDITION_CD        = CODE.CODE_ORDER

			INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
					ON PROCESS.CODE_NO          = "002"
			       AND HEAD.PROCESS_ID          = PROCESS.CODE_ORDER

			INNER JOIN (
			                SELECT S1HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S1ISNO.INSPECTION_NO) AS TOTAL_NO_COUNT
			                  FROM TRESHEDT AS S1HEAD /* 検査実績ヘッダ */
			            INNER JOIN TISHEETM AS S1SHEE /* 検査シートマスタ */
			                    ON S1HEAD.MASTER_NO           = S1SHEE.MASTER_NO
			                   AND S1HEAD.PROCESS_ID          = S1SHEE.PROCESS_ID
			                   AND S1HEAD.INSPECTION_SHEET_NO = S1SHEE.INSPECTION_SHEET_NO
			                   AND S1HEAD.REV_NO              = S1SHEE.REV_NO

			            INNER JOIN TINSPNOM AS S1ISNO /* 検査番号マスタ */
			                    ON S1HEAD.MASTER_NO           = S1ISNO.MASTER_NO
			                   AND S1HEAD.PROCESS_ID          = S1ISNO.PROCESS_ID
			                   AND S1HEAD.INSPECTION_SHEET_NO = S1ISNO.INSPECTION_SHEET_NO
			                   AND S1HEAD.REV_NO              = S1ISNO.REV_NO

			            INNER JOIN TINSNTMM AS S1INTI /* 検査番号時間帯マスタ */
			                    ON S1ISNO.MASTER_NO           = S1INTI.MASTER_NO
			                   AND S1ISNO.PROCESS_ID          = S1INTI.PROCESS_ID
			                   AND S1ISNO.INSPECTION_SHEET_NO = S1INTI.INSPECTION_SHEET_NO
			                   AND S1ISNO.REV_NO              = S1INTI.REV_NO
			                   AND S1ISNO.INSPECTION_NO       = S1INTI.INSPECTION_NO
			                   AND S1HEAD.INSPECTION_TIME_ID  = IF(S1HEAD.CONDITION_CD = "01", S1INTI.INSPECTION_TIME_ID, S1HEAD.INSPECTION_TIME_ID) /* 条件分岐させる Condition＝01⇒timeID・Condition≠01⇒条件にしない（同値設定） */
			                   AND S1INTI.INSPECTION_TIME_ID  = IF(S1HEAD.CONDITION_CD = "01", S1INTI.INSPECTION_TIME_ID, "ZZ") /* 条件分岐させる Condition＝01⇒timeID・Condition≠01⇒条件にしない（同値設定） */

			                 WHERE S1HEAD.MASTER_NO           = IF(:S1MasterNo1 <> "", :S1MasterNo2, S1HEAD.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND S1HEAD.PROCESS_ID          = IF(:S1ProcessId1 <> "", :S1ProcessId2, S1HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S1SHEE.CUSTOMER_ID         = IF(:S1CustomerId1 <> "", :S1CustomerId2, S1SHEE.CUSTOMER_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S1HEAD.INSPECTION_SHEET_NO LIKE :S1CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			                   AND S1HEAD.REV_NO              = IF(:S1RevisionNo1 <> "", :S1RevisionNo2, S1HEAD.REV_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND S1HEAD.MACHINE_NO          = IF(:S1MachineNo1 <> "", :S1MachineNo2, S1HEAD.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			                   AND S1HEAD.INSERT_USER_ID      = IF(:S1UserID1 <> "", :S1UserID2, S1HEAD.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S1HEAD.INSPECTION_YMD      >= IF((:S1DateFrom1 is not null) and (:S1DateFrom2 <> ""), :S1DateFrom3, S1HEAD.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			                   AND S1HEAD.INSPECTION_YMD      <= IF((:S1DateTo1 is not null) and (:S1DateTo2 <> ""), :S1DateTo3, S1HEAD.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			                   AND S1SHEE.ITEM_NO             LIKE :S1PartNo
			                   AND S1SHEE.DELETE_FLG          = "0"
			                   AND S1ISNO.DELETE_FLG          = "0"
			                   AND S1INTI.DELETE_FLG          = "0"
			              GROUP BY S1HEAD.INSPECTION_RESULT_NO
			            ) SUB1
			        ON HEAD.INSPECTION_RESULT_NO = SUB1.INSPECTION_RESULT_NO

			LEFT OUTER JOIN (
			                SELECT S2HEAD.INSPECTION_RESULT_NO
			                      ,COUNT(S2DETL.INSPECTION_NO) AS RESULT_NO_COUNT
			                  FROM TRESHEDT AS S2HEAD
			            INNER JOIN TISHEETM AS S2SHEE
			                    ON S2HEAD.MASTER_NO              = S2SHEE.MASTER_NO
			                   AND S2HEAD.PROCESS_ID             = S2SHEE.PROCESS_ID
			                   AND S2HEAD.INSPECTION_SHEET_NO    = S2SHEE.INSPECTION_SHEET_NO
			                   AND S2HEAD.REV_NO                 = S2SHEE.REV_NO

			            INNER JOIN TRESDETT AS S2DETL
			                    ON S2HEAD.INSPECTION_RESULT_NO   = S2DETL.INSPECTION_RESULT_NO

			                 WHERE S2HEAD.MASTER_NO              = IF(:S2MasterNo1 <> "", :S2MasterNo2, S2HEAD.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND S2HEAD.PROCESS_ID             = IF(:S2ProcessId1 <> "", :S2ProcessId2, S2HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S2SHEE.CUSTOMER_ID            = IF(:S2CustomerId1 <> "", :S2CustomerId2, S2SHEE.CUSTOMER_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S2HEAD.INSPECTION_SHEET_NO    LIKE :S2CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			                   AND S2HEAD.REV_NO                 = IF(:S2RevisionNo1 <> "", :S2RevisionNo2, S2HEAD.REV_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND S2HEAD.MACHINE_NO             = IF(:S2MachineNo1 <> "", :S2MachineNo2, S2HEAD.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			                   AND S2HEAD.INSERT_USER_ID         = IF(:S2UserID1 <> "", :S2UserID2, S2HEAD.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND S2HEAD.INSPECTION_YMD         >= IF((:S2DateFrom1 is not null) and (:S2DateFrom2 <> ""), :S2DateFrom3, S2HEAD.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			                   AND S2HEAD.INSPECTION_YMD         <= IF((:S2DateTo1 is not null) and (:S2DateTo2 <> ""), :S2DateTo3, S2HEAD.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			                   AND S2SHEE.ITEM_NO                LIKE :S2PartNo
			                   AND S2DETL.INSPECTION_RESULT_TYPE IN ("1","2","3")
			                   AND S2SHEE.DELETE_FLG             = "0"
			              GROUP BY S2HEAD.INSPECTION_RESULT_NO
			            ) SUB2
			        ON HEAD.INSPECTION_RESULT_NO = SUB2.INSPECTION_RESULT_NO

			     WHERE HEAD.MASTER_NO           = IF(:MasterNo1 <> "", :MasterNo2, HEAD.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			       AND HEAD.PROCESS_ID          = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			       AND SHEE.CUSTOMER_ID          = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)   /* value of input in screen　無ければ条件にしない */
			       AND HEAD.INSPECTION_SHEET_NO LIKE :CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			       AND HEAD.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)   /* value of input in screen　無ければ条件にしない */
			       AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			       AND HEAD.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			       AND HEAD.INSPECTION_YMD      >= IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, HEAD.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			       AND HEAD.INSPECTION_YMD      <= IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, HEAD.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			       AND SHEE.ITEM_NO             LIKE :PartNo                                          /* Part No.は前方一致検索　未入力なら%のみ */
			       AND SHEE.DELETE_FLG          = "0"
			       AND MACH.DELETE_FLG          = "0"
			       AND USER.DELETE_FLG          = "0"
			       AND TIME.DELETE_FLG          = "0"

			  /* シート表示順、シート番号、Rev.、作業日、検査時間表示順、検査時間、ユーザ表示順、ユーザID */
			  ORDER BY SHEE.DISPLAY_ORDER
			          ,HEAD.MASTER_NO
			          ,HEAD.PROCESS_ID
			          ,CUSTOMER.CUSTOMER_ID
			          ,SHEE.MATERIAL_NAME
			          ,SHEE.MATERIAL_SIZE
			          ,HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,HEAD.INSPECTION_YMD
			          ,TIME.DISPLAY_ORDER
			          ,TIME.INSPECTION_TIME_ID
			          ,USER.DISPLAY_ORDER
			          ,HEAD.INSERT_USER_ID
			',
				[
					"S1MasterNo1"     => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S1MasterNo2"     => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S1ProcessId1"    => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1ProcessId2"    => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1CustomerId1"   => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CustomerId2"   => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S1CheckSheetNo"  => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S1RevisionNo1"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1RevisionNo2"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1MachineNo1"    => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MachineNo2"    => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1UserID1"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1UserID2"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1DateFrom1"     => $lInspectionDateFromForSearch,
					"S1DateFrom2"     => $lInspectionDateFromForSearch,
					"S1DateFrom3"     => $lInspectionDateFromForSearch,
					"S1DateTo1"       => $lInspectionDateToForSearch,
					"S1DateTo2"       => $lInspectionDateToForSearch,
					"S1DateTo3"       => $lInspectionDateToForSearch,
					"S1PartNo"        => TRIM((String)Input::get('txtPartNoForSearch'))."%",

					"S2MasterNo1"     => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S2MasterNo2"     => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S2ProcessId1"    => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2ProcessId2"    => TRIM((String)Input::get('cmbProcessForSearch')),
					"S2CustomerId1"   => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CustomerId2"   => TRIM((String)Input::get('cmbCustomerForSearch')),
					"S2CheckSheetNo"  => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S2RevisionNo1"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2RevisionNo2"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S2MachineNo1"    => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2MachineNo2"    => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S2UserID1"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2UserID2"       => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S2DateFrom1"     => $lInspectionDateFromForSearch,
					"S2DateFrom2"     => $lInspectionDateFromForSearch,
					"S2DateFrom3"     => $lInspectionDateFromForSearch,
					"S2DateTo1"       => $lInspectionDateToForSearch,
					"S2DateTo2"       => $lInspectionDateToForSearch,
					"S2DateTo3"       => $lInspectionDateToForSearch,
					"S2PartNo"        => TRIM((String)Input::get('txtPartNoForSearch'))."%",

					"MasterNo1"       => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo2"       => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId1"      => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"      => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerId1"     => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerId2"     => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo"    => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"     => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"     => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo1"      => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"      => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID1"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom1"       => $lInspectionDateFromForSearch,
					"DateFrom2"       => $lInspectionDateFromForSearch,
					"DateFrom3"       => $lInspectionDateFromForSearch,
					"DateTo1"         => $lInspectionDateToForSearch,
					"DateTo2"         => $lInspectionDateToForSearch,
					"DateTo3"         => $lInspectionDateToForSearch,
					"PartNo"          => TRIM((String)Input::get('txtPartNoForSearch'))."%",
				]
			);
        
		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getNotExistNoList
	// overview        search no entry List of Inspection No. and return it to calling as Array
	// overview        実績が未入力の検査番号リストを検索して、呼び出し元に配列で返却する
	// argument        Nothing
	// return value    Array
	// author          s-miyamoto
	// date            2014.07.14
	// record of updates  2014.07.14 v0.01 first making
	//                    2014.08.26 v1.00 FIX
	//                    ■■16/05/2015 Hoshina added Condition■■
	//**************************************************************************
	private function getNotExistNoList()
	{
		$lTblNotExistNoData = [];      //data table of no entry Inspection No.
		$lRowNotExistNoData = [];      //data row of no entry Inspection No.

		$lArrNotExistNoData = [];      //data of no entry Inspection No.（Array after treatment）
		$lWorkValue         = "";      //work to treat value
		$lRowCount          = 0;       //data line No.
		$lOldKey            = "";      //old Key（inspection result No.）
		$lNewKey            = "";      //new Key（inspection result No.）

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

			$lTblNotExistNoData = DB::select('
			                SELECT HEAD.INSPECTION_RESULT_NO
			                      ,ISNO.INSPECTION_NO
			                  FROM TRESHEDT AS HEAD

			            INNER JOIN TISHEETM AS SHEE
			                    ON HEAD.MASTER_NO           = SHEE.MASTER_NO
			                   AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			                   AND HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			                   AND HEAD.REV_NO              = SHEE.REV_NO

			            INNER JOIN TINSPNOM AS ISNO
			                    ON HEAD.MASTER_NO           = ISNO.MASTER_NO
			                   AND HEAD.PROCESS_ID          = ISNO.PROCESS_ID
			                   AND HEAD.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
			                   AND HEAD.REV_NO              = ISNO.REV_NO

			            INNER JOIN TINSNTMM AS INTI
			                    ON ISNO.MASTER_NO           = INTI.MASTER_NO
			                   AND ISNO.PROCESS_ID          = INTI.PROCESS_ID
			                   AND ISNO.INSPECTION_SHEET_NO = INTI.INSPECTION_SHEET_NO
			                   AND ISNO.REV_NO              = INTI.REV_NO
			                   AND ISNO.INSPECTION_NO       = INTI.INSPECTION_NO
			                   AND HEAD.INSPECTION_TIME_ID  = IF(HEAD.CONDITION_CD = "01", INTI.INSPECTION_TIME_ID, HEAD.INSPECTION_TIME_ID) /* 条件分岐させる Condition＝01⇒timeID・Condition≠01⇒条件にしない（同値設定） */

			                 WHERE HEAD.MASTER_NO           = IF(:MasterNo1 <> "", :MasterNo2, HEAD.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND HEAD.PROCESS_ID          = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND HEAD.INSPECTION_SHEET_NO LIKE :CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			                   AND HEAD.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)   /* value of input in screen　無ければ条件にしない */
			                   AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			                   AND HEAD.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, HEAD.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			                   AND HEAD.INSPECTION_YMD      >= IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, HEAD.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			                   AND HEAD.INSPECTION_YMD      <= IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, HEAD.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			                   AND SHEE.ITEM_NO             LIKE :PartNo
			                   AND SHEE.DELETE_FLG = "0"
			                   AND ISNO.DELETE_FLG = "0"
			                   AND INTI.DELETE_FLG = "0"
			                   AND NOT EXISTS (
			                                       SELECT "X"
			                                         FROM TRESHEDT AS S1HEAD
			                                   INNER JOIN TISHEETM AS S1SHEE
			                                           ON S1HEAD.MASTER_NO           = S1SHEE.MASTER_NO
			                                          AND S1HEAD.PROCESS_ID          = S1SHEE.PROCESS_ID
			                                          AND S1HEAD.INSPECTION_SHEET_NO = S1SHEE.INSPECTION_SHEET_NO
			                                          AND S1HEAD.REV_NO              = S1SHEE.REV_NO

			                                   INNER JOIN TRESDETT AS S1DETL
			                                           ON S1HEAD.INSPECTION_RESULT_NO = S1DETL.INSPECTION_RESULT_NO

			                                        WHERE S1HEAD.MASTER_NO           = IF(:S1MasterNo1 <> "", :S1MasterNo2, HEAD.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			                                          AND S1HEAD.PROCESS_ID          = IF(:S1ProcessId1 <> "", :S1ProcessId2, HEAD.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			                                          AND S1HEAD.INSPECTION_SHEET_NO LIKE :S1CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			                                          AND S1HEAD.REV_NO              = IF(:S1RevisionNo1 <> "", :S1RevisionNo2, S1HEAD.REV_NO)   /* value of input in screen　無ければ条件にしない */
			                                          AND S1HEAD.MACHINE_NO          = IF(:S1MachineNo1 <> "", :S1MachineNo2, S1HEAD.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			                                          AND S1HEAD.INSERT_USER_ID      = IF(:S1UserID1 <> "", :S1UserID2, S1HEAD.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			                                          AND S1HEAD.INSPECTION_YMD      >= IF((:S1DateFrom1 is not null) and (:S1DateFrom2 <> ""), :S1DateFrom3, S1HEAD.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			                                          AND S1HEAD.INSPECTION_YMD      <= IF((:S1DateTo1 is not null) and (:S1DateTo2 <> ""), :S1DateTo3, S1HEAD.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			                                          AND S1SHEE.ITEM_NO             LIKE :S1PartNo
			                                          AND S1SHEE.DELETE_FLG = "0"
			                                          AND HEAD.INSPECTION_RESULT_NO = S1HEAD.INSPECTION_RESULT_NO
			                                          AND ISNO.INSPECTION_NO        = S1DETL.INSPECTION_NO
			                       )
			              ORDER BY HEAD.INSPECTION_RESULT_NO
			                      ,ISNO.DISPLAY_ORDER
			                      ,ISNO.INSPECTION_NO
			',
				[
					"MasterNo1"        => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo2"        => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId1"       => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"       => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo"     => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"      => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"      => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo1"       => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"       => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID1"          => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"          => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom1"        => $lInspectionDateFromForSearch,
					"DateFrom2"        => $lInspectionDateFromForSearch,
					"DateFrom3"        => $lInspectionDateFromForSearch,
					"DateTo1"          => $lInspectionDateToForSearch,
					"DateTo2"          => $lInspectionDateToForSearch,
					"DateTo3"          => $lInspectionDateToForSearch,
					"PartNo"           => TRIM((String)Input::get('txtPartNoForSearch'))."%",

					"S1MasterNo1"      => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S1MasterNo2"      => TRIM((String)Input::get('txtMasterNoForSearch')),
					"S1ProcessId1"     => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1ProcessId2"     => TRIM((String)Input::get('cmbProcessForSearch')),
					"S1CheckSheetNo"   => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"S1RevisionNo1"    => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1RevisionNo2"    => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"S1MachineNo1"     => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1MachineNo2"     => TRIM((String)Input::get('txtMachineNoForSearch')),
					"S1UserID1"        => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1UserID2"        => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"S1DateFrom1"      => $lInspectionDateFromForSearch,
					"S1DateFrom2"      => $lInspectionDateFromForSearch,
					"S1DateFrom3"      => $lInspectionDateFromForSearch,
					"S1DateTo1"        => $lInspectionDateToForSearch,
					"S1DateTo2"        => $lInspectionDateToForSearch,
					"S1DateTo3"        => $lInspectionDateToForSearch,
					"S1PartNo"         => TRIM((String)Input::get('txtPartNoForSearch'))."%",
				]
			);

		//if data exist, treat to be 2row,Inspection Result No. and no entry Inspection No.
		if ($lTblNotExistNoData != null)
		{

			//store result in Array again
			foreach ($lTblNotExistNoData as $lRowNotExistNoData) {

				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowNotExistNoData = (Array)$lRowNotExistNoData;

				//store present value in old key exception from 0 line count(prevention of key break in 1 issue)
				if ($lRowCount == 0) {

					$lOldKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];

				}

				//set new key
				$lNewKey = $lRowNotExistNoData["INSPECTION_RESULT_NO"];

				//in case key break
				if ($lOldKey != $lNewKey) {

					//key break
					//delete the last comma in the string maked in work 
					$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

					//set Array to return
					$lArrNotExistNoData += [
						$lOldKey => $lWorkValue
					];

					//clear work
					$lWorkValue = "";

				}

				//connect no exist No. in work
				$lWorkValue .= $lRowNotExistNoData["INSPECTION_NO"]."-";

				//set value reading to old key
				$lOldKey = $lNewKey;

				//add line No.
				$lRowCount += 1;

			}

			//store data remaining after read all
			//delete the last comma in the string maked in work
			$lWorkValue = substr($lWorkValue, 0, mb_strlen($lWorkValue)-1 );

			//set Array to return
			$lArrNotExistNoData += [
				$lOldKey => $lWorkValue
			];

		}

		return $lArrNotExistNoData;
	}

	//**************************************************************************
	// process    getInspectionResultCSV
	// overview      search list of inspection result and return it to calling as Array
	// argument      Nothing
	// return value    Array
	// author    s-miyamoto
	// date    2014.06.10
	// record of updates  2014.06.10 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getInspectionResultCSV()
	{
		$lTblInspectionResultCSV = [];      //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

			$lTblInspectionResultCSV = DB::select('
			         SELECT REHE.INSPECTION_RESULT_NO
			               ,REHE.MASTER_NO
			               ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
			               ,REHE.INSPECTION_SHEET_NO
			               ,REHE.REV_NO
			               ,SHEE.INSPECTION_SHEET_NAME
			               ,CUST.CUSTOMER_NAME
			               ,SHEE.MODEL_NAME
			               ,SHEE.ITEM_NO
			               ,SHEE.ITEM_NAME
			               ,SHEE.DRAWING_NO
			               ,MACH.MACHINE_NAME
			               ,REHE.INSERT_USER_ID
			               ,USER1.USER_NAME AS INSERT_USER_NAME
			               ,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			               ,CODE.CODEORDER_NAME
			               ,TIME.INSPECTION_TIME_NAME
			               ,REDE.INSPECTION_NO
			               ,ISNO.INSPECTION_ITEM
			               ,ISNO.TOOL_CD
			               ,CASE
			                  WHEN ISNO.INSPECTION_TOOL_CLASS = "1" THEN "Spec Check"
			                  WHEN ISNO.INSPECTION_TOOL_CLASS = "2" THEN "OK/NG"
			                  WHEN ISNO.INSPECTION_TOOL_CLASS = "3" THEN "Machine"
			                  WHEN ISNO.INSPECTION_TOOL_CLASS = "4" THEN "Other"
			                  ELSE ""
			                END INSPECTION_TOOL_CLASS
			               ,ifnull(ISNO.THRESHOLD_NG_UNDER,"") AS THRESHOLD_NG_UNDER
			               ,ifnull(ISNO.THRESHOLD_OFFSET_UNDER,"") AS THRESHOLD_OFFSET_UNDER
			               ,ifnull(ISNO.REFERENCE_VALUE,"") AS REFERENCE_VALUE
			               ,ifnull(ISNO.THRESHOLD_OFFSET_OVER,"") AS THRESHOLD_OFFSET_OVER
			               ,ifnull(ISNO.THRESHOLD_NG_OVER,"") AS THRESHOLD_NG_OVER
			               ,REDE.INSPECTION_RESULT_VALUE
			               ,CASE
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "1" THEN "OK"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "3" THEN "NG"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
			                  ELSE ""
			                END INSPECTION_RESULT_TYPE
			               ,REDE.OFFSET_NG_REASON
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION = "1" THEN "Running"
			                  WHEN REDE.OFFSET_NG_ACTION = "2" THEN "Stop Line"
			                  ELSE ""
			                END OFFSET_NG_ACTION
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_INTERNAL
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_REQUEST
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_TSR
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_SORTING
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_NOACTION
			               ,REDE.ACTION_DETAILS_TR_NO_TSR_NO
			               ,DATE_FORMAT(REDE.INSERT_YMDHMS,"%d-%m-%Y %k:%i:%s") AS INSERT_YMDHMS
			               ,REDE.LAST_UPDATE_USER_ID
			               ,USER2.USER_NAME AS UPDATE_USER_NAME
			               ,DATE_FORMAT(REDE.UPDATE_YMDHMS,"%d-%m-%Y %k:%i:%s") AS UPDATE_YMDHMS
			           FROM TRESHEDT AS REHE

			     INNER JOIN TRESDETT AS REDE
			             ON REHE.INSPECTION_RESULT_NO = REDE.INSPECTION_RESULT_NO

			     INNER JOIN TISHEETM AS SHEE
			             ON REHE.MASTER_NO            = SHEE.MASTER_NO
			            AND REHE.PROCESS_ID           = SHEE.PROCESS_ID
			            AND REHE.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
			            AND REHE.REV_NO               = SHEE.REV_NO

			     INNER JOIN TINSPNOM AS ISNO
			             ON REHE.MASTER_NO            = ISNO.MASTER_NO
			            AND REHE.PROCESS_ID           = ISNO.PROCESS_ID
			            AND REHE.INSPECTION_SHEET_NO  = ISNO.INSPECTION_SHEET_NO
			            AND REHE.REV_NO               = ISNO.REV_NO
			            AND REDE.INSPECTION_NO        = ISNO.INSPECTION_NO

			     INNER JOIN TUSERMST AS USER1
			             ON REHE.INSERT_USER_ID       = USER1.USER_ID

			     INNER JOIN TUSERMST AS USER2
			             ON REDE.LAST_UPDATE_USER_ID  = USER2.USER_ID

			     INNER JOIN TINSPTIM AS TIME
			             ON REHE.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID

			     INNER JOIN TMACHINM AS MACH
			             ON REHE.MACHINE_NO           = MACH.MACHINE_NO

			     INNER JOIN TCUSTOMM AS CUST
			             ON SHEE.CUSTOMER_ID          = CUST.CUSTOMER_ID

			     INNER JOIN TCODEMST AS CODE
			             ON CODE.CODE_NO              = "001"
			            AND REHE.CONDITION_CD         = CODE.CODE_ORDER

			     INNER JOIN TCODEMST AS PROCESS
			             ON PROCESS.CODE_NO           = "002"
			            AND REHE.PROCESS_ID           = PROCESS.CODE_ORDER

			          WHERE REHE.MASTER_NO            = IF(:MasterNo1 <> "", :MasterNo2, REHE.MASTER_NO)   /* value of input in screen　無ければ条件にしない */
			            AND REHE.PROCESS_ID           = IF(:ProcessId1 <> "", :ProcessId2, REHE.PROCESS_ID)   /* value of input in screen　無ければ条件にしない */
			            AND REHE.INSPECTION_SHEET_NO  LIKE :CheckSheetNo                                    /* 検査シート番号は前方一致検索　未入力なら%のみ */
			            AND REHE.REV_NO               = IF(:RevisionNo1 <> "", :RevisionNo2, REHE.REV_NO)   /* value of input in screen　無ければ条件にしない */
			            AND REHE.MACHINE_NO           = IF(:MachineNo1 <> "", :MachineNo2, REHE.MACHINE_NO) /* 画面選択値　無ければ条件にしない */
			            AND REHE.INSERT_USER_ID       = IF(:UserID1 <> "", :UserID2, REHE.INSERT_USER_ID)   /* value of input in screen　無ければ条件にしない */
			            AND REHE.INSPECTION_YMD       >= IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, REHE.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			            AND REHE.INSPECTION_YMD       <= IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, REHE.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			            AND SHEE.ITEM_NO              LIKE :PartNo                                          /* Part No.は前方一致検索　未入力なら%のみ */
			            AND SHEE.DELETE_FLG           = "0"
			            AND ISNO.DELETE_FLG           = "0"
			            AND USER1.DELETE_FLG          = "0"
			            AND USER2.DELETE_FLG          = "0"
			            AND TIME.DELETE_FLG           = "0"
			            AND MACH.DELETE_FLG           = "0"
			            AND CUST.DELETE_FLG           = "0"

			       /* シート表示順、シート番号、Rev.、作業日、検査時間表示順、検査時間、ユーザ表示順、ユーザID、Inspection No.表示順、Inspection No. ★表示順は気になる*/
			       ORDER BY SHEE.DISPLAY_ORDER
			               ,REHE.MASTER_NO
			               ,REHE.PROCESS_ID
			               ,REHE.INSPECTION_SHEET_NO
			               ,REHE.REV_NO
			               ,REHE.INSPECTION_YMD
			               ,TIME.DISPLAY_ORDER
			               ,REHE.INSPECTION_TIME_ID
			               ,USER1.DISPLAY_ORDER
			               ,REHE.INSERT_USER_ID
			               ,ISNO.DISPLAY_ORDER
			               ,REDE.INSPECTION_NO
			',
				[
					"MasterNo1"    => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo2"    => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId1"   => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"   => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo" => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"  => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"  => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo1"   => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"   => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID1"      => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"      => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom1"    => $lInspectionDateFromForSearch,
					"DateFrom2"    => $lInspectionDateFromForSearch,
					"DateFrom3"    => $lInspectionDateFromForSearch,
					"DateTo1"      => $lInspectionDateToForSearch,
					"DateTo2"      => $lInspectionDateToForSearch,
					"DateTo3"      => $lInspectionDateToForSearch,
					"PartNo"       => TRIM((String)Input::get('txtPartNoForSearch'))."%"
				]
			);

		return $lTblInspectionResultCSV;
	}


	//**************************************************************************
	// process    getInspectionMasterDataForShonin
	// overview      get master data for EXCEL approval and return it to calling as Array(only nomal)
	// argument      Nothing
	// return value    Array
	// author    k-kagawa
	// date    2014.05.21
	// record of updates  2014.05.21 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           2015.04.28 Hoshina  add to get MODEL_NAME from Inspection check sheet master
	//           2015.04.28 Hoshina  add to get INSPECTION_ITEM from Inspection No.master
	//           ■■16/05/2015 Hoshina added Condition■■
	//           connect code master and add to get nomal inspection text
	//
	//**************************************************************************
	private function getInspectionMasterDataForShonin()
	{
		$lTblMasterData          = []; //data table of inspection result

			$lTblMasterData = DB::select
			(
				'
				    SELECT SHEE.MASTER_NO
				          ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
				          ,SHEE.INSPECTION_SHEET_NO
				          ,SHEE.REV_NO
				          ,SHEE.INSPECTION_SHEET_NAME
				          ,SHEE.MODEL_NAME
				          ,IPNO.INSPECTION_NO
				          ,IPNO.THRESHOLD_NG_UNDER
				          ,IPNO.REFERENCE_VALUE
				          ,IPNO.THRESHOLD_NG_OVER
				          ,IPNO.INSPECTION_ITEM
				          ,INTM.INSPECTION_TIME_ID
				          ,TIME.INSPECTION_TIME_NAME
				          ,CODE.CODEORDER_NAME
				          
				      FROM TISHEETM AS SHEE /* 検査シートマスタ */
				      
				INNER JOIN TCODEMST AS CODE /* コードマスタ */
				        ON CODE.CODE_NO = "001"
				       AND CODE.CODE_ORDER = "01"
				       
				INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
				        ON PROCESS.CODE_NO    = "002"
				       AND PROCESS.CODE_ORDER = SHEE.PROCESS_ID
				       
				INNER JOIN TINSPNOM AS IPNO /* Inspection No.マスタ */
				        ON IPNO.MASTER_NO           = SHEE.MASTER_NO
				       AND IPNO.PROCESS_ID          = SHEE.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = SHEE.REV_NO
				       
				INNER JOIN TINSNTMM AS INTM /* Inspection No.時間帯マスタ */
				        ON IPNO.MASTER_NO           = INTM.MASTER_NO
				       AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = INTM.REV_NO
				       AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
				       
				INNER JOIN TINSPTIM AS TIME /* 検査時間・回数マスタ */
				        ON INTM.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
				        
				     WHERE SHEE.MASTER_NO           = :MasterNo
				       AND SHEE.PROCESS_ID          = :ProcessId
				       AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
				       AND SHEE.REV_NO              = :RevisionNo
				       
				  ORDER BY IPNO.DISPLAY_ORDER
				          ,TIME.DISPLAY_ORDER
				',
				[
					"MasterNo"     => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId"    => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo" => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
					"RevisionNo"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				]
			);

		return $lTblMasterData;
	}

	//**************************************************************************
	// process    getInspectionResultDataForShonin
	// overview      get result data for EXCEL approval and return it as ARRAY to calling(only nomal)
	// argument      Nothing
	// return value    Array
	// author    k-kagawa
	// date    2014.05.21
	// record of updates  2014.05.21 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           2015.04.28 Hoshina  add to get TECHNICIAN_USER_NAME from TECHNICIAN USER MASTER
	//           ■■16/05/2015 Hoshina added Condition■■
	//           get condition value and name(from code master)
	//           order by input time(①Inspection No.②inspection timein master search⇒order by only input time in this function
	//
	//**************************************************************************
	private function getInspectionResultDataForShonin()
	{
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		
			$lTblSearchResultData = DB::select
			(
			'
			    SELECT HEAD.MASTER_NO
				      ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
				      ,HEAD.INSPECTION_RESULT_NO
			          ,HEAD.INSPECTION_SHEET_NO
			          ,HEAD.REV_NO
			          ,MACH.MACHINE_NAME
			          ,USER.USER_NAME
			          ,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,HEAD.INSPECTION_TIME_ID
			          ,DTIL.INSPECTION_NO
			          ,DTIL.INSPECTION_RESULT_VALUE
			          ,CASE
			             WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
			             WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
			             WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
			             WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
			             WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
			             ELSE ""
			           END INSPECTION_RESULT_TYPE
			          ,DATE_FORMAT(DTIL.INSERT_YMDHMS,"%T") AS INSERT_YMDHMS
			          ,DTIL.OFFSET_NG_REASON
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
			             WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
			             ELSE ""
			           END OFFSET_NG_ACTION
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
			             ELSE ""
			           END OFFSET_NG_ACTION_DTL_INTERNAL
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
			             ELSE ""
			           END OFFSET_NG_ACTION_DTL_REQUEST
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
			             ELSE ""
			           END OFFSET_NG_ACTION_DTL_TSR
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
			             ELSE ""
			           END OFFSET_NG_ACTION_DTL_SORTING
			          ,CASE
			             WHEN DTIL.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
			             ELSE ""
			           END OFFSET_NG_ACTION_DTL_NOACTION
			          ,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
			          ,DTIL.TR_NO
			          ,DTIL.TSR_NO
			          ,TEUR.TECHNICIAN_USER_NAME
			          ,HEAD.CONDITION_CD
			          ,CODE.CODEORDER_NAME
			          
			      FROM TRESHEDT AS HEAD /* 検査実績ヘッダ */
			      
			INNER JOIN TRESDETT AS DTIL /* 検査実績明細 */
			        ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
			        
	   LEFT OUTER JOIN TTECUSRT AS TEUR /* テクニシャン作業者マスタ */
			        ON DTIL.TECHNICIAN_USER_ID = TEUR.TECHNICIAN_USER_ID
			        
			INNER JOIN TMACHINM AS MACH /* Machineマスタ */
			        ON HEAD.MACHINE_NO          = MACH.MACHINE_NO
			        
			INNER JOIN TUSERMST AS USER /* Userマスタ */
			        ON HEAD.INSERT_USER_ID      = USER.USER_ID
			        
			INNER JOIN TCODEMST AS CODE /* コードマスタ */
			        ON CODE.CODE_NO             = "001"
			       AND CODE.CODE_ORDER          = "01"
			       AND CODE.CODE_ORDER          = HEAD.CONDITION_CD
			       
			INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
			        ON PROCESS.CODE_NO          = "002"
			       AND PROCESS.CODE_ORDER       = HEAD.PROCESS_ID
			       
			     WHERE HEAD.MASTER_NO           = :MasterNo
			       AND HEAD.PROCESS_ID          = :ProcessId
			       AND HEAD.INSPECTION_SHEET_NO = :CheckSheetNo
			       AND HEAD.REV_NO              = :RevisionNo
			       AND HEAD.MACHINE_NO          = IF(:MachineNo <> "", :MachineNo1, HEAD.MACHINE_NO)
			       AND HEAD.INSERT_USER_ID      = :UserID
			       AND HEAD.INSPECTION_YMD      = :DateFrom
			       
			  ORDER BY DTIL.INSERT_YMDHMS
			',
				[
					"MasterNo"       => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId"      => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo"   => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
					"RevisionNo"     => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo"      => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo1"     => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom"       => $lInspectionDateFromForSearch,
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process    getInspectionMasterDataForShonin2
	// overview      get master data for EXCEL approval and return it as array to calling(only nomal)
	// argument      Nothing
	// return value    Array
	// author    m-hoshina
	// date    2015.05.16
	// record of updates  ■■16/05/2015 Hoshina added Condition■■
	//
	//**************************************************************************
	private function getInspectionMasterDataForShonin2()
	{
		$lTblMasterData          = []; //data table of inspection result

			$lTblMasterData = DB::select
			(
				'
				    SELECT SHEE.MASTER_NO
				          ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
				          ,SHEE.INSPECTION_SHEET_NO
				          ,SHEE.REV_NO
				          ,SHEE.INSPECTION_SHEET_NAME
				          ,SHEE.MODEL_NAME
				          ,IPNO.INSPECTION_NO
				          ,IPNO.THRESHOLD_NG_UNDER
				          ,IPNO.REFERENCE_VALUE
				          ,IPNO.THRESHOLD_NG_OVER
				          ,IPNO.INSPECTION_ITEM
				          ,INTM.INSPECTION_TIME_ID
				          
				      FROM TISHEETM AS SHEE /* 検査シートマスタ */
				      
				INNER JOIN TINSPNOM AS IPNO /* Inspection No.マスタ */
				        ON IPNO.MASTER_NO           = SHEE.MASTER_NO
				       AND IPNO.PROCESS_ID          = SHEE.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = SHEE.REV_NO
				       
				INNER JOIN TINSNTMM AS INTM /* Inspection No.時間帯マスタ */
				        ON IPNO.MASTER_NO           = INTM.MASTER_NO
				       AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = INTM.REV_NO
				       AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
				       
				INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
				        ON PROCESS.CODE_NO          = "002"
				       AND PROCESS.CODE_ORDER       = INTM.PROCESS_ID
				       
				     WHERE SHEE.MASTER_NO           = :MasterNo
				       AND SHEE.PROCESS_ID          = :ProcessId
				       AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
				       AND SHEE.REV_NO              = :RevisionNo
				       AND INTM.INSPECTION_TIME_ID  = "ZZ"
				       
				  ORDER BY IPNO.DISPLAY_ORDER
				',
				[
					"MasterNo"       => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId"      => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo" => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
					"RevisionNo"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
				]
			);

		return $lTblMasterData;
	}

	//**************************************************************************
	// process    getInspectionResultDataForShonin2
	// overview      get result data of EXCEL approval and return it asa array to calling(only abnomal)
	// argument      Nothing
	// return value    Array
	// author    m-hoshina
	// date    2015.05.16
	// record of updates  ■■16/05/2015 Hoshina added Condition■■
	//
	//**************************************************************************
	private function getInspectionResultDataForShonin2()
	{
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");

			$lTblSearchResultData = DB::select
			(
			'
				    SELECT HEAD.MASTER_NO
					      ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
					      ,HEAD.INSPECTION_RESULT_NO
				          ,HEAD.INSPECTION_SHEET_NO
				          ,HEAD.REV_NO
				          ,MACH.MACHINE_NAME
				          ,USER.USER_NAME
				          ,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
				          ,HEAD.INSPECTION_TIME_ID
				          ,TIME.INSPECTION_TIME_NAME
				          ,DTIL.INSPECTION_NO
				          ,DTIL.INSPECTION_RESULT_VALUE
				          ,CASE
				             WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
				             WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
				             WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
				             WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
				             WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
				             ELSE ""
				           END INSPECTION_RESULT_TYPE
				          ,DATE_FORMAT(DTIL.INSERT_YMDHMS,"%T") AS INSERT_YMDHMS
				          ,DTIL.OFFSET_NG_REASON
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
				             WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
				             ELSE ""
				           END OFFSET_NG_ACTION
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
				             ELSE ""
				           END OFFSET_NG_ACTION_DTL_INTERNAL
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
				             ELSE ""
				           END OFFSET_NG_ACTION_DTL_REQUEST
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
				             ELSE ""
				           END OFFSET_NG_ACTION_DTL_TSR
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
				             ELSE ""
				           END OFFSET_NG_ACTION_DTL_SORTING
				          ,CASE
				             WHEN DTIL.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
				             ELSE ""
				           END OFFSET_NG_ACTION_DTL_NOACTION
				          ,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
				          ,DTIL.TR_NO
				          ,DTIL.TSR_NO
				          ,TEUR.TECHNICIAN_USER_NAME
				          ,HEAD.CONDITION_CD
				          ,CODE.CODEORDER_NAME
				          
				      FROM TRESHEDT AS HEAD /* 検査実績ヘッダ */
				      
				INNER JOIN TRESDETT AS DTIL /* 検査実績明細 */
				        ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
				        
		   LEFT OUTER JOIN TTECUSRT AS TEUR /* テクニシャン作業者マスタ */
				        ON DTIL.TECHNICIAN_USER_ID = TEUR.TECHNICIAN_USER_ID
				        
				INNER JOIN TMACHINM AS MACH /* Machineマスタ */
				        ON HEAD.MACHINE_NO          = MACH.MACHINE_NO
				        
				INNER JOIN TUSERMST AS USER /* Userマスタ */
				        ON HEAD.INSERT_USER_ID      = USER.USER_ID
				        
				INNER JOIN TCODEMST AS CODE /* コードマスタ */
				        ON CODE.CODE_NO             = "001"
				       AND CODE.CODE_ORDER          = HEAD.CONDITION_CD
				       
				INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
				        ON PROCESS.CODE_NO          = "002"
				       AND PROCESS.CODE_ORDER       = HEAD.PROCESS_ID
				       
				INNER JOIN TINSPTIM AS TIME /* 検査時間・回数マスタ */
				        ON HEAD.INSPECTION_TIME_ID  = TIME.INSPECTION_TIME_ID
				        
				     WHERE HEAD.MASTER_NO           = :MasterNo
				       AND HEAD.PROCESS_ID          = :ProcessId
				       AND HEAD.INSPECTION_SHEET_NO = :CheckSheetNo
				       AND HEAD.REV_NO              = :RevisionNo
				       AND HEAD.MACHINE_NO          = IF(:MachineNo <> "", :MachineNo1, HEAD.MACHINE_NO)
				       AND HEAD.INSERT_USER_ID      = :UserID
				       AND HEAD.INSPECTION_YMD      = :DateFrom
				       AND HEAD.CONDITION_CD        <> "01"
				       
				  ORDER BY DTIL.INSERT_YMDHMS
				',
					[
						"MasterNo"       => TRIM((String)Input::get('txtMasterNoForSearch')),
						"ProcessId"      => TRIM((String)Input::get('cmbProcessForSearch')),
						"CheckSheetNo"   => TRIM((String)Input::get('cmbCheckSheetNoForSearch')),
						"RevisionNo"     => TRIM((String)Input::get('cmbRevisionNoForSearch')),
						"MachineNo"      => TRIM((String)Input::get('txtMachineNoForSearch')),
						"MachineNo1"     => TRIM((String)Input::get('txtMachineNoForSearch')),
						"UserID"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
						"DateFrom"       => $lInspectionDateFromForSearch,
					]
				);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getInspectionResultDataForNGResult
	// overview        get result data of NG result approval and return it asa array to calling
	// argument        Nothing
	// return value    Array
	// date            Ver.01 2016.06.13 m-motooka
	// remarks         
	//**************************************************************************
	private function getInspectionResultDataForNGResult()
	{
		//echo "Here"; die();
		$lTblSearchResultData          = []; //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

			$lTblSearchResultData = DB::select
			(
			'
			    SELECT     RESULTD.MASTER_NO                                                   /*0*/
						  ,RESULTD.PROCESS_ID                                                  /*1*/
						  ,MASTER.CUSTOMER_NAME                                                /*2*/
						  ,MASTER.ITEM_NO                                                      /*3*/
						  ,MASTER.ITEM_NAME                                                    /*4*/
						  ,RESULTD.INSPECTION_SHEET_NO                                         /*5*/
						  ,RESULTD.REV_NO                                                      /*6*/
						  ,RESULTD.INSPECTION_NO                                               /*7*/
						  ,MACHINE.MACHINE_NAME                                                /*8*/
						  ,RESULTD.USER_NAME                                                   /*9*/
						  ,DATE_FORMAT(RESULTD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD    /*10*/
						  ,RESULTD.INSPECTION_TIME_NAME                                        /*11*/
						  ,RESULTD.CONDITION_CD                                                /*12*/
						  ,MASTER.INSPECTION_ITEM                                              /*13*/
						  ,MASTER.REFERENCE_VALUE                                              /*14*/
						  ,MASTER.THRESHOLD_NG_UNDER                                           /*15*/
						  ,MASTER.THRESHOLD_NG_OVER                                            /*16*/
						  ,RESULTD.INSPECTION_RESULT_VALUE                                     /*17*/
						  ,RESULTD.RESULT_CLASS                                                /*18*/
						  ,RESULTD.UPDATE_YMDHMS                                               /*19*/
						  ,RESULTD.NG_OFFSET_ACTION                                            /*20*/
						  ,RESULTD.OFFSET_NG_REASON                                            /*21*/
						  ,RESULTD.ACTION_DETAILS_TR_NO_TSR_NO                                 /*22*/
						  ,RESULTD.TR_NO                                                       /*23*/
						  ,RESULTD.TECHNICIAN_USER_NAME                                        /*24*/
						  ,RESULTD.TSR_NO                                                      /*25*/
						  ,RESULTD.INSPECTION_RESULT_TYPE                                      /*判定のため*/
						  ,RESULTD.MACHINE_NO                                                  /*判定のため*/
						  ,MASTER.CUSTOMER_ID                                                  /*WHERE句のため*/
				FROM
								(SELECT HEAD.MASTER_NO                                                     /*0*/
									   ,HEAD.PROCESS_ID                                                    /*1*/
									   ,HEAD.INSPECTION_SHEET_NO                                           /*5*/
									   ,HEAD.REV_NO                                                        /*6*/
									   ,DETAIL.INSPECTION_NO                                               /*7*/
									   ,USERM.USER_NAME                                                    /*9*/
									   ,HEAD.INSPECTION_YMD                                                /*10*/
									   ,TIMEP.INSPECTION_TIME_NAME                                         /*11*/
									   ,CASE
									     WHEN HEAD.CONDITION_CD = "01" THEN "Nomal"
									     WHEN HEAD.CONDITION_CD = "02" THEN "Abnormal"
									     ELSE ""
									    END CONDITION_CD                                                   /*12*/
									   ,DETAIL.INSPECTION_RESULT_VALUE                                     /*17*/
									   ,CASE
									     WHEN DETAIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
									     WHEN DETAIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
									     WHEN DETAIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
									     WHEN DETAIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
									     WHEN DETAIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
									     ELSE ""
									    END AS RESULT_CLASS                                                /*18*/
									   ,DETAIL.UPDATE_YMDHMS                                               /*19*/
									   ,CASE
									     WHEN DETAIL.OFFSET_NG_ACTION = "1" THEN "Running"
									     WHEN DETAIL.OFFSET_NG_ACTION = "2" THEN "Line Stop"
									     ELSE ""
									    END AS NG_OFFSET_ACTION                                            /*20*/
									   ,DETAIL.OFFSET_NG_REASON                                            /*21*/
									   ,DETAIL.ACTION_DETAILS_TR_NO_TSR_NO                                 /*22*/
									   ,DETAIL.TR_NO                                                       /*23*/
									   ,TECH.TECHNICIAN_USER_NAME                                          /*24*/
									   ,DETAIL.TSR_NO                                                      /*25*/
									   ,HEAD.MACHINE_NO                                                    /*WHERE句のため*/
									   ,HEAD.LAST_UPDATE_USER_ID                                           /*WHERE句のため*/
									   ,HEAD.INSPECTION_TIME_ID                                            /*WHERE句のため*/
									   ,DETAIL.INSPECTION_RESULT_TYPE                                      /*判定のため*/
									   
								 FROM   TRESDETT AS DETAIL                                                  /*検査実績明細*/
								
							 INNER JOIN TRESHEDT AS HEAD                                                    /*検査実績ヘッダ*/
									 ON DETAIL.INSPECTION_RESULT_NO = HEAD.INSPECTION_RESULT_NO
									 
							 INNER JOIN TINSPTIM AS TIMEP                                                   /*検査時間・回数マスタ*/
									 ON HEAD.INSPECTION_TIME_ID    = TIMEP.INSPECTION_TIME_ID
									 
							 INNER JOIN TUSERMST AS USERM                                                   /*USERマスタ*/
									 ON USERM.USER_ID              = HEAD.INSERT_USER_ID
									 
							 LEFT OUTER JOIN TTECUSRT AS TECH                                               /*テクニシャン作業者マスタ*/
									 ON DETAIL.TECHNICIAN_USER_ID  = TECH.TECHNICIAN_USER_ID
									AND TECH.DELETE_FLG = "0"

							WHERE       HEAD.MASTER_NO           = IF(:MasterNo1 <> "", :MasterNo2, HEAD.MASTER_NO)                      /*マスタ番号     value of input in screen 無ければ条件にしない*/
									AND HEAD.PROCESS_ID          = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)                   /*工程区分       value of input in screen 無ければ条件にしない*/
									AND HEAD.INSPECTION_SHEET_NO LIKE :CheckSheetNo1                                                     /*検査シート番号 value of input in screen 無ければ条件にしない*/
									AND HEAD.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)                     /*リビジョン番号 value of input in screen 無ければ条件にしない*/
									AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)                   /*マシン番号     value of input in screen 無ければ条件にしない*/
									AND HEAD.LAST_UPDATE_USER_ID = IF(:UserID1 <> "", :UserID2, HEAD.LAST_UPDATE_USER_ID)                /*検査者         value of input in screen 無ければ条件にしない*/
									AND HEAD.INSPECTION_YMD      >= :DateFrom1 AND HEAD.INSPECTION_YMD <= :DateTo1                       /*検査日         value of input in screen */
									AND HEAD.INSPECTION_TIME_ID  = IF(:InspectionTime1 <> "", :InspectionTime2, HEAD.INSPECTION_TIME_ID) /*検査時間       value of input in screen 無ければ条件にしない*/
								)RESULTD
							
							LEFT OUTER JOIN
								(SELECT CUSTOM.CUSTOMER_NAME              /*2*/
									   ,SHEE.ITEM_NO                      /*3*/
									   ,SHEE.ITEM_NAME                    /*4*/
									   ,SHEE.MATERIAL_NAME                /**/
									   ,SHEE.MATERIAL_SIZE                /**/
									   ,IPNO.INSPECTION_ITEM              /*14*/
									   ,IPNO.REFERENCE_VALUE              /*15*/
									   ,IPNO.THRESHOLD_NG_UNDER           /*16*/
									   ,IPNO.THRESHOLD_NG_OVER            /*17*/
									   ,CUSTOM.CUSTOMER_ID                /*WHERE句のため*/
									   ,SHEE.MASTER_NO                    /*外部結合のため*/
									   ,SHEE.PROCESS_ID                   /*外部結合のため*/
									   ,SHEE.INSPECTION_SHEET_NO          /*外部結合のため*/
									   ,SHEE.REV_NO                       /*外部結合のため*/
									   ,IPNO.INSPECTION_NO                /*外部結合のため*/
									   ,SHEE.MODEL_NAME                   /*ORDERBYのため*/
									   ,IPNO.DISPLAY_ORDER                /*ORDERBYのため*/
									
								FROM    TCUSTOMM AS CUSTOM                /*顧客マスタ*/
								
							 INNER JOIN TISHEETM AS SHEE                  /*検査シートマスタ*/
									 ON CUSTOM.CUSTOMER_ID       = SHEE.CUSTOMER_ID
									AND SHEE.DELETE_FLG          = "0"
									AND CUSTOM.DELETE_FLG        = "0"
									
							 INNER JOIN TINSPNOM AS IPNO                  /*検査番号マスタ*/
									 ON IPNO.MASTER_NO           = SHEE.MASTER_NO
									AND IPNO.PROCESS_ID          = SHEE.PROCESS_ID
									AND IPNO.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
									AND IPNO.REV_NO              = SHEE.REV_NO
									AND IPNO.DELETE_FLG          = "0"
									AND SHEE.DELETE_FLG          = "0"
								)MASTER
							
						ON RESULTD.MASTER_NO            = MASTER.MASTER_NO
					   AND RESULTD.PROCESS_ID           = MASTER.PROCESS_ID
					   AND RESULTD.INSPECTION_SHEET_NO  = MASTER.INSPECTION_SHEET_NO
					   AND RESULTD.REV_NO               = MASTER.REV_NO
					   AND RESULTD.INSPECTION_NO        = MASTER.INSPECTION_NO
					   
				INNER JOIN TMACHINM AS MACHINE
						ON RESULTD.MACHINE_NO           = MACHINE.MACHINE_NO

					 WHERE RESULTD.MASTER_NO            = IF(:MasterNo3 <> "", :MasterNo4, RESULTD.MASTER_NO)                       /*マスタ番号       value of input in screen 無ければ条件にしない*/
					   AND RESULTD.PROCESS_ID           = IF(:ProcessId3 <> "", :ProcessId4, RESULTD.PROCESS_ID)                    /*工程区分         value of input in screen 無ければ条件にしない*/
					   AND MASTER.CUSTOMER_ID           = IF(:CustomerID1 <> "", :CustomerID2, MASTER.CUSTOMER_ID)                  /*顧客番号         value of input in screen 無ければ条件にしない*/
					   AND RESULTD.INSPECTION_SHEET_NO  LIKE :CheckSheetNo2                                                         /*検査シート番号   value of input in screen 無ければ条件にしない*/
					   AND RESULTD.REV_NO               = IF(:RevisionNo3 <> "", :RevisionNo4, RESULTD.REV_NO)                      /*リビジョン番号   value of input in screen 無ければ条件にしない*/
					   AND MASTER.MATERIAL_NAME         = IF(:MaterialName1 <> "", :MaterialName2, MASTER.MATERIAL_NAME)            /*マテリアル名     value of input in screen 無ければ条件にしない*/
					   AND MASTER.MATERIAL_SIZE         = IF(:MaterialSize1 <> "", :MaterialSize2, MASTER.MATERIAL_SIZE)            /*マテリアルサイズ value of input in screen 無ければ条件にしない*/
					   AND RESULTD.MACHINE_NO           = IF(:MachineNo3 <> "", :MachineNo4, RESULTD.MACHINE_NO)                    /*マシン番号       value of input in screen 無ければ条件にしない*/
					   AND RESULTD.LAST_UPDATE_USER_ID  = IF(:UserID3 <> "", :UserID4, RESULTD.LAST_UPDATE_USER_ID)                 /*検査者           value of input in screen 無ければ条件にしない*/
					   AND RESULTD.INSPECTION_YMD       >= :DateFrom2 AND RESULTD.INSPECTION_YMD <= :DateTo2                        /*検査日           value of input in screen */
					   AND MASTER.ITEM_NO               LIKE :PartNo1                                                               /*パート番号       value of input in screen 未入力なら%のみ */
					   AND RESULTD.INSPECTION_TIME_ID   = IF(:InspectionTime3 <> "", :InspectionTime4, RESULTD.INSPECTION_TIME_ID)  /*検査時間         value of input in screen 無ければ条件にしない*/

				   ORDER BY RESULTD.MASTER_NO                   /*検査実績ヘッダ　　　　マスタ番号*/
						   ,RESULTD.PROCESS_ID                  /*検査実績ヘッダ　　　　工程区分*/
						   ,MASTER.CUSTOMER_NAME                /*顧客マスタ　　　　　　カスタマー名*/
						   ,MASTER.ITEM_NO                      /*検査シートマスタ　　　パートＮＯ*/
						   ,MASTER.MODEL_NAME                   /*検査シートマスタ　　　モデル名*/
						   ,RESULTD.INSPECTION_SHEET_NO         /*検査実績ヘッダ　　　　検査シート*/
						   ,RESULTD.REV_NO                      /*検査実績ヘッダ　　　　リビジョン番号*/
						   ,RESULTD.MACHINE_NO                  /*検査実績ヘッダ　　　　マシン番号*/
						   ,MASTER.DISPLAY_ORDER                /*検査番号マスタ　　　　表示順*/
						   ,RESULTD.INSPECTION_YMD ASC          /*検査実績ヘッダ　　　　検査日*/
						   ,RESULTD.INSPECTION_TIME_NAME ASC    /*検査時間・回数マスタ　検査時間*/
						   ,RESULTD.UPDATE_YMDHMS               /*検査実績明細　　　　　最終更新時間*/
			',
				[
					"MasterNo1"         => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo2"         => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo3"         => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo4"         => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId1"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId3"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId4"        => TRIM((String)Input::get('cmbProcessForSearch')),
					"CustomerID1"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerID2"       => TRIM((String)Input::get('cmbCustomerForSearch')),
					"CheckSheetNo1"     => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"CheckSheetNo2"     => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo3"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo4"       => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MaterialName1"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialName2"     => TRIM((String)Input::get('cmbMaterialNameForSearch')),
					"MaterialSize1"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MaterialSize2"     => TRIM((String)Input::get('cmbMaterialSizeForSearch')),
					"MachineNo1"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo3"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo4"        => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID1"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID3"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID4"           => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom1"         => $lInspectionDateFromForSearch,
					"DateFrom2"         => $lInspectionDateFromForSearch,
					"DateTo1"           => $lInspectionDateToForSearch,
					"DateTo2"           => $lInspectionDateToForSearch,
					"PartNo1"           => TRIM((String)Input::get('txtPartNoForSearch'))."%",
					"InspectionTime1"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspectionTime2"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspectionTime3"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
					"InspectionTime4"   => TRIM((String)Input::get('cmbInspectionTimeForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCustmerName
	// overview        get header data of NG result approval and return it asa array to calling
	// argument        
	// return value    
	// date            Ver.01 2016.06.13 m-motooka
	// remarks         
	//**************************************************************************
	private function getCustmerName()
	{
		$lTblSearchResultData          = []; //data table of inspection result

			$lTblSearchResultData = DB::select
			(
			'
				SELECT  CUSTOM.CUSTOMER_NAME
				FROM	TCUSTOMM AS CUSTOM
				WHERE	CUSTOM.CUSTOMER_ID = IF(:CustomerID1 <> "", :CustomerID2, CUSTOM.CUSTOMER_ID)    /*無ければ条件にしない*/
			',
				[
					"CustomerID1"  =>  TRIM((String)Input::get('cmbCustomerForSearch')),
					"CustomerID2"  =>  TRIM((String)Input::get('cmbCustomerForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getMachineName
	// overview        get header data of NG result approval and return it asa array to calling
	// argument        
	// return value    
	// date            Ver.01 2016.06.13 m-motooka
	// remarks         
	//**************************************************************************
	private function getMachineName()
	{
		$lTblSearchResultData          = []; //data table of inspection result

			$lTblSearchResultData = DB::select
			(
			'
			    SELECT  MACHINE.MACHINE_NO
					   ,MACHINE.MACHINE_NAME
				   FROM TMACHINM AS MACHINE
				  WHERE MACHINE.MACHINE_NO = IF(:MachineNo1 <> "", :MachineNo2, MACHINE.MACHINE_NO)    /*無ければ条件にしない*/
			',
				[
					"MachineNo1"      => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"      => TRIM((String)Input::get('txtMachineNoForSearch')),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCheckSheetName
	// overview        get header data of NG result approval and return it asa array to calling
	// argument        
	// return value    
	// date            Ver.01
	// remarks         
	//**************************************************************************
	private function getCheckSheetName()
	{
		$lTblSearchResultData          = []; //data table of inspection result

			$lTblSearchResultData = DB::select
			(
			'
					SELECT SHEET.INSPECTION_SHEET_NAME
					      ,SHEET.MODEL_NAME
					 FROM  TISHEETM AS SHEET
					WHERE  SHEET.INSPECTION_SHEET_NO LIKE :CheckSheetNo                                       /*無ければ条件にしない*/
					   AND SHEET.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, SHEET.REV_NO)     /*無ければ条件にしない*/
					   AND SHEET.ITEM_NO             LIKE :PartNo

			',
				[
					"CheckSheetNo"  => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"   => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"PartNo"        => TRIM((String)Input::get('txtPartNoForSearch'))."%",
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getInspectorName
	// overview        get header data of NG result approval and return it asa array to calling
	// argument        
	// return value    
	// date            Ver.01 2016.06.13 m-motooka
	// remarks         
	//**************************************************************************
	private function getInspectorName()
	{
		$lTblSearchResultData          = []; //data table of inspection result

			$lTblSearchResultData = DB::select
			(
			'
			    SELECT USERM.USER_NAME
				  FROM TUSERMST AS USERM
				 WHERE USERM.USER_ID = IF(:UserID1 <> "", :UserID2, USERM.USER_ID)    /*無ければ条件にしない*/
			',
				[
					"UserID1"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"         => TRIM((String)Input::get('txtInspectorCodeForSearch')),
				]
			);

		return $lTblSearchResultData;
	}



	//**************************************************************************
	// process    getInspectionResultCSVforApproval
	// overview      search list of inspection result(approval) and return it asa array to calling
	// argument      Nothing
	// return value    Array
	// author    s-miyamoto
	// date    2014.06.10
	// record of updates  2014.06.10 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           2014.09.22 v1.01 delete some parts
	//**************************************************************************
	private function getInspectionResultCSVforApproval()
	{
		$lTblInspectionResultCSV = [];      //data table of inspection result

		$lInspectionDateFromForSearch  = ""; //date From for search
		$lInspectionDateToForSearch    = ""; //date To for search

		//change English type->Japanese type（because MySQL store as Japanese type）
		$lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		$lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");

			$lTblInspectionResultCSV = DB::select('
			         SELECT REHE.MASTER_NO
			               ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
			               ,REHE.INSPECTION_SHEET_NO
			               ,REHE.REV_NO
			               ,SHEE.INSPECTION_SHEET_NAME
			               ,MACH.MACHINE_NAME
			               ,USER1.USER_NAME AS INSERT_USER_NAME
			               ,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			               ,TIME.INSPECTION_TIME_NAME
			               ,CODE.CODEORDER_NAME
			               ,REDE.INSPECTION_NO
			               ,REDE.INSPECTION_RESULT_VALUE
			               ,CASE
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "1" THEN "OK"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "3" THEN "NG"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
			                  WHEN REDE.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
			                  ELSE ""
			                END INSPECTION_RESULT_TYPE
			               ,REDE.OFFSET_NG_REASON
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION = "1" THEN "Running"
			                  WHEN REDE.OFFSET_NG_ACTION = "2" THEN "Stop Line"
			                  ELSE ""
			                END OFFSET_NG_ACTION
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_INTERNAL
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_REQUEST
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_TSR
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_SORTING
			               ,CASE
			                  WHEN REDE.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
			                  ELSE ""
			                END OFFSET_NG_ACTION_DTL_NOACTION
			               ,REDE.ACTION_DETAILS_TR_NO_TSR_NO
			           FROM TRESHEDT AS REHE

			     INNER JOIN TRESDETT AS REDE
			             ON REHE.INSPECTION_RESULT_NO = REDE.INSPECTION_RESULT_NO

			     INNER JOIN TISHEETM AS SHEE
			             ON REHE.MASTER_NO            = SHEE.MASTER_NO
			            AND REHE.PROCESS_ID           = SHEE.PROCESS_ID
			            AND REHE.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
			            AND REHE.REV_NO               = SHEE.REV_NO

			     INNER JOIN TINSPNOM AS ISNO
			             ON REHE.MASTER_NO            = ISNO.MASTER_NO
			            AND REHE.PROCESS_ID           = ISNO.PROCESS_ID
			            AND REHE.INSPECTION_SHEET_NO  = ISNO.INSPECTION_SHEET_NO
			            AND REHE.REV_NO               = ISNO.REV_NO
			            AND REDE.INSPECTION_NO        = ISNO.INSPECTION_NO

			     INNER JOIN TUSERMST AS USER1
			             ON REHE.INSERT_USER_ID       = USER1.USER_ID

			     INNER JOIN TINSPTIM AS TIME
			             ON REHE.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID

			     INNER JOIN TMACHINM AS MACH
			             ON REHE.MACHINE_NO           = MACH.MACHINE_NO

			     INNER JOIN TCUSTOMM AS CUST
			             ON SHEE.CUSTOMER_ID          = CUST.CUSTOMER_ID
			             
			     INNER JOIN TCODEMST AS CODE
			             ON CODE.CODE_NO              = "001"
			            AND REHE.CONDITION_CD         = CODE.CODE_ORDER

			     INNER JOIN TCODEMST AS PROCESS
			             ON PROCESS.CODE_NO           = "002"
			            AND REHE.PROCESS_ID           = PROCESS.CODE_ORDER

			          WHERE REHE.MASTER_NO            = IF(:MasterNo1 <> "", :MasterNo2, REHE.MASTER_NO)                                      /* value of input in screen　無ければ条件にしない */
			            AND REHE.PROCESS_ID           = IF(:ProcessId1 <> "", :ProcessId2, REHE.PROCESS_ID)                                   /* value of input in screen　無ければ条件にしない */
			            AND REHE.INSPECTION_SHEET_NO  LIKE :CheckSheetNo                                                                      /* 検査シート番号は前方一致検索　未入力なら%のみ */
			            AND REHE.REV_NO               = IF(:RevisionNo1 <> "", :RevisionNo2, REHE.REV_NO)                                     /* value of input in screen　無ければ条件にしない */
			            AND REHE.MACHINE_NO           = IF(:MachineNo1 <> "", :MachineNo2, REHE.MACHINE_NO)                                   /* 画面選択値　無ければ条件にしない */
			            AND REHE.INSERT_USER_ID       = IF(:UserID1 <> "", :UserID2, REHE.INSERT_USER_ID)                                     /* value of input in screen　無ければ条件にしない */
			            AND REHE.INSPECTION_YMD       >= IF((:DateFrom1 is not null) and (:DateFrom2 <> ""), :DateFrom3, REHE.INSPECTION_YMD) /* value of input in screen（From）　無ければ条件にしない */
			            AND REHE.INSPECTION_YMD       <= IF((:DateTo1 is not null) and (:DateTo2 <> ""), :DateTo3, REHE.INSPECTION_YMD)       /* value of input in screen（To）　無ければ条件にしない */
			            AND SHEE.ITEM_NO              LIKE :PartNo                                                                            /* Part No.は前方一致検索　未入力なら%のみ */
			            AND SHEE.DELETE_FLG           = "0"
			            AND ISNO.DELETE_FLG           = "0"
			            AND USER1.DELETE_FLG          = "0"
			            AND TIME.DELETE_FLG           = "0"
			            AND MACH.DELETE_FLG           = "0"
			            AND CUST.DELETE_FLG           = "0"

			       /* シート表示順、シート番号、Rev.、作業日、検査時間表示順、検査時間、ユーザ表示順、ユーザID、Inspection No.表示順、Inspection No. ★表示順は気になる*/
			       ORDER BY SHEE.DISPLAY_ORDER
			               ,REHE.MASTER_NO
			               ,REHE.PROCESS_ID
			               ,REHE.INSPECTION_SHEET_NO
			               ,REHE.REV_NO
			               ,REHE.INSPECTION_YMD
			               ,TIME.DISPLAY_ORDER
			               ,REHE.INSPECTION_TIME_ID
			               ,USER1.DISPLAY_ORDER
			               ,REHE.INSERT_USER_ID
			               ,ISNO.DISPLAY_ORDER
			               ,REDE.INSPECTION_NO
			',
				[
					"MasterNo1"    => TRIM((String)Input::get('txtMasterNoForSearch')),
					"MasterNo2"    => TRIM((String)Input::get('txtMasterNoForSearch')),
					"ProcessId1"   => TRIM((String)Input::get('cmbProcessForSearch')),
					"ProcessId2"   => TRIM((String)Input::get('cmbProcessForSearch')),
					"CheckSheetNo" => TRIM((String)Input::get('cmbCheckSheetNoForSearch'))."%",
					"RevisionNo1"  => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"RevisionNo2"  => TRIM((String)Input::get('cmbRevisionNoForSearch')),
					"MachineNo1"   => TRIM((String)Input::get('txtMachineNoForSearch')),
					"MachineNo2"   => TRIM((String)Input::get('txtMachineNoForSearch')),
					"UserID1"      => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"UserID2"      => TRIM((String)Input::get('txtInspectorCodeForSearch')),
					"DateFrom1"    => $lInspectionDateFromForSearch,
					"DateFrom2"    => $lInspectionDateFromForSearch,
					"DateFrom3"    => $lInspectionDateFromForSearch,
					"DateTo1"      => $lInspectionDateToForSearch,
					"DateTo2"      => $lInspectionDateToForSearch,
					"DateTo3"      => $lInspectionDateToForSearch,
					"PartNo"       => TRIM((String)Input::get('txtPartNoForSearch'))."%"
				]
			);

		return $lTblInspectionResultCSV;
	}

	//**************************************************************************
	// process    createInspectionResultCSV
	// overview      treatment of header and data for inspection result CSV
	// argument      inspection result data table
	// return value    inspection result CSV
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function createInspectionResultCSV($pTblInspectionResultCSV)
	{
		$lOutputCSVData          = ""; //Data to output as CSV
		$lRowInspectionResultCSV = []; //Datarow of inspection result（for CSV）

		//header
		$lOutputCSVData .= "Inspection Result Seq.No.,";
		$lOutputCSVData .= "Master No.,";
		$lOutputCSVData .= "Process,";
		$lOutputCSVData .= "Inspection(Check) Sheet No.,";
		$lOutputCSVData .= "Revision No.,";
		$lOutputCSVData .= "Check Sheet Title,";
		$lOutputCSVData .= "Customer Name,";
		$lOutputCSVData .= "Model Name,";
		$lOutputCSVData .= "Parts No.,";
		$lOutputCSVData .= "Parts Name,";
		$lOutputCSVData .= "Drawing No.,";
		$lOutputCSVData .= "Machine No.,";
		$lOutputCSVData .= "Inspecter ID,";
		$lOutputCSVData .= "Inspector Name,";
		$lOutputCSVData .= "Inspection Date,";
		$lOutputCSVData .= "Condition,";
		$lOutputCSVData .= "Inspection Time,";
		$lOutputCSVData .= "Inspection(Check) No.,";
		$lOutputCSVData .= "Inspection Items,";
		$lOutputCSVData .= "Tool Code,";
		$lOutputCSVData .= "Equipment,";
		$lOutputCSVData .= "NG Under,";
		$lOutputCSVData .= "OFFSET Under,";
		$lOutputCSVData .= "Center,";
		$lOutputCSVData .= "OFFSET Over,";
		$lOutputCSVData .= "NG Over,";
		$lOutputCSVData .= "Inspection Result,";
		$lOutputCSVData .= "Result Class,";
		$lOutputCSVData .= "OFFSET Reason / NG Reason,";
		$lOutputCSVData .= "OFFSET Action / NG Action,";
		$lOutputCSVData .= "Internal Approve,";
		$lOutputCSVData .= "Request Offset,";
		$lOutputCSVData .= "TSR,";
		$lOutputCSVData .= "Sorting,";
		$lOutputCSVData .= "No Action,";
		$lOutputCSVData .= "Action Details / Technician Request No. / TSR No.,";
		$lOutputCSVData .= "Entry Date/Time,";
		$lOutputCSVData .= "Modify User ID,";
		$lOutputCSVData .= "Modify User Name,";
		$lOutputCSVData .= "Modify Date/Time,";
		$lOutputCSVData .= "\n";

		foreach ($pTblInspectionResultCSV as $lRowInspectionResultCSV) {

			$lRowInspectionResultCSV = (Array)$lRowInspectionResultCSV;

			//string concatenation while using treatment function
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MASTER_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PROCESS_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REV_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CUSTOMER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MODEL_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["DRAWING_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MACHINE_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CODEORDER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TIME_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_ITEM"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TOOL_CD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TOOL_CLASS"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REFERENCE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_INTERNAL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_REQUEST"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_TSR"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_SORTING"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_NOACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ACTION_DETAILS_TR_NO_TSR_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_YMDHMS"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["LAST_UPDATE_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_YMDHMS"]);
			//newline
			$lOutputCSVData.= "\n";

		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process    createInspectionResultCSVforApproval
	// overview      treatment of header and data for inspection result CSV（for approval）
	// argument      inspection result data table（for approval）
	// return value    inspection result CSV（for approval）
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           2014.09.22 v1.01 delete some parts
	//**************************************************************************
	private function createInspectionResultCSVforApproval($pTblInspectionResultCSV)
	{
		$lOutputCSVData          = ""; //Data to output as CSV
		$lRowInspectionResultCSV = []; //Datarow of inspection result（for CSV）

		//header
		$lOutputCSVData .= "Master No.,";
		$lOutputCSVData .= "Process,";
		$lOutputCSVData .= "Inspection(Check) Sheet No.,";
		$lOutputCSVData .= "Revision No.,";
		$lOutputCSVData .= "Check Sheet Title,";
		$lOutputCSVData .= "Machine No.,";
		$lOutputCSVData .= "Inspector Name,";
		$lOutputCSVData .= "Inspection Date,";
		$lOutputCSVData .= "Condition,";
		$lOutputCSVData .= "Inspection Time,";
		$lOutputCSVData .= "Inspection(Check) No.,";
		$lOutputCSVData .= "Inspection Result,";
		$lOutputCSVData .= "Result Class,";
		$lOutputCSVData .= "OFFSET Reason / NG Reason,";
		$lOutputCSVData .= "OFFSET Action / NG Action,";
		$lOutputCSVData .= "Internal Approve,";
		$lOutputCSVData .= "Request Offset,";
		$lOutputCSVData .= "TSR,";
		$lOutputCSVData .= "Sorting,";
		$lOutputCSVData .= "No Action,";
		$lOutputCSVData .= "Action Details / Technician Request No. / TSR No.,";
		$lOutputCSVData .= "\n";

		foreach ($pTblInspectionResultCSV as $lRowInspectionResultCSV) {

			$lRowInspectionResultCSV = (Array)$lRowInspectionResultCSV;

			//string concatenation while using treatment function
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MASTER_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PROCESS_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REV_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MACHINE_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CODEORDER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TIME_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_INTERNAL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_REQUEST"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_TSR"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_SORTING"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_NOACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ACTION_DETAILS_TR_NO_TSR_NO"]);
			//newline
			$lOutputCSVData.= "\n";

		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process    createCSVValue
	// overview      treat value of CSV（escape value including "）
	// argument      value to output for CSV
	// return value    value after escape
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function createCSVValue($pTargetValue)
	{
		//「"」->「""」「",」
		$pTargetValue = "\"".str_replace("\"", "\"\"", $pTargetValue)."\",";

		return $pTargetValue;

	}

	//**************************************************************************
	// process    setListForms
	// overview      store and return form input information of search result list screen
	// argument      Arary
	// return value    Array
	// author    s-miyamoto
	// date    2014.05.23
	// record of updates  2014.05.23 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function setListForms($pViewData)
	{

		//■①store Master No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MasterNoForEntry'))) {

			if (Input::has('txtMasterNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MasterNoForEntry', Input::get('txtMasterNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MasterNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMasterNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MasterNoForEntry', Input::get('txtMasterNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MasterNoForEntry"  => Session::get('BA1010MasterNoForEntry')
		];


		//■②store Process for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProcessForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForEntry')) {
				Session::put('BA1010ProcessForEntry', Input::get('cmbProcessForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010ProcessForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbProcessForEntry')) {
				Session::put('BA1010ProcessForEntry', Input::get('cmbProcessForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ProcessForEntry"  => Session::get('BA1010ProcessForEntry')
		];


		//■③store Customer for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CustomerForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForEntry')) {
				Session::put('BA1010CustomerForEntry', Input::get('cmbCustomerForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010CustomerForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCustomerForEntry')) {
				Session::put('BA1010CustomerForEntry', Input::get('cmbCustomerForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CustomerForEntry"  => Session::get('BA1010CustomerForEntry')
		];


		//■④store Check Sheet No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CheckSheetNoForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForEntry')) {
				Session::put('BA1010CheckSheetNoForEntry', Input::get('cmbCheckSheetNoForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010CheckSheetNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbCheckSheetNoForEntry')) {
				Session::put('BA1010CheckSheetNoForEntry', Input::get('cmbCheckSheetNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CheckSheetNoForEntry"  => Session::get('BA1010CheckSheetNoForEntry')
		];


		//■⑤store Material Name for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialNameForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForEntry')) {
				Session::put('BA1010MaterialNameForEntry', Input::get('cmbMaterialNameForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010MaterialNameForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialNameForEntry')) {
				Session::put('BA1010MaterialNameForEntry', Input::get('cmbMaterialNameForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialNameForEntry"  => Session::get('BA1010MaterialNameForEntry')
		];


		//■⑥store Material Size for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialSizeForEntry'))) {

			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForEntry')) {
				Session::put('BA1010MaterialSizeForEntry', Input::get('cmbMaterialSizeForEntry'));
			}
			else //if value of screen does not exist,write down as blank
			{
				Session::put('BA1010MaterialSizeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			//if value of screen exists,write down in session
			if (Input::has('cmbMaterialSizeForEntry')) {
				Session::put('BA1010MaterialSizeForEntry', Input::get('cmbMaterialSizeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialSizeForEntry"  => Session::get('BA1010MaterialSizeForEntry')
		];


		//■⑦store M/C No. for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MachineNoForEntry'))) {

			if (Input::has('txtMachineNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForEntry', Input::get('txtMachineNoForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MachineNoForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMachineNoForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForEntry', Input::get('txtMachineNoForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MachineNoForEntry"  => Session::get('BA1010MachineNoForEntry')
		];


		//■⑧store Inspector Code for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectorCodeForEntry'))) {

			if (Input::has('txtInspectorCodeForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForEntry', Input::get('txtInspectorCodeForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectorCodeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectorCodeForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForEntry', Input::get('txtInspectorCodeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectorCodeForEntry"  => Session::get('BA1010InspectorCodeForEntry')
		];


		//■⑨store Inspection Date for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateForEntry'))) {

			if (Input::has('txtInspectionDateForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateForEntry', Input::get('txtInspectionDateForEntry'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateForEntry', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateForEntry', Input::get('txtInspectionDateForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateForEntry"  => Session::get('BA1010InspectionDateForEntry')
		];


		//■■16/05/2015 Hoshina added Condition■■
		//■⑩store Condition for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ConditionForEntry'))) {

			if (Input::has('cmbConditionForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010ConditionForEntry', Input::get('cmbConditionForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010ConditionForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbConditionForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010ConditionForEntry', Input::get('cmbConditionForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ConditionForEntry"  => Session::get('BA1010ConditionForEntry')
		];


		//■⑪store Inspection Time for entry
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionTimeForEntry'))) {

			if (Input::has('cmbInspectionTimeForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForEntry', Input::get('cmbInspectionTimeForEntry'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectionTimeForEntry', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbInspectionTimeForEntry')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForEntry', Input::get('cmbInspectionTimeForEntry'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectionTimeForEntry"  => Session::get('BA1010InspectionTimeForEntry')
		];


		//■■■■■■■■■■■■■■■■■■■■■■■■■■■
		//■①store MasterNo for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MasterNoForSearch'))) {

			if (Input::has('txtMasterNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MasterNoForSearch', Input::get('txtMasterNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MasterNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMasterNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MasterNoForSearch', Input::get('txtMasterNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MasterNoForSearch"  => Session::get('BA1010MasterNoForSearch')
		];


		//■②store Process for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010ProcessForSearch'))) {

			if (Input::has('cmbProcessForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010ProcessForSearch', Input::get('cmbProcessForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010ProcessForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbProcessForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010ProcessForSearch', Input::get('cmbProcessForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"ProcessForSearch"  => Session::get('BA1010ProcessForSearch')
		];


		//■③store Customer for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CustomerForSearch'))) {

			if (Input::has('cmbCustomerForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010CustomerForSearch', Input::get('cmbCustomerForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010CustomerForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbCustomerForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010CustomerForSearch', Input::get('cmbCustomerForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CustomerForSearch"  => Session::get('BA1010CustomerForSearch')
		];


		//■④store Check Sheet No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010CheckSheetNoForSearch'))) {

			if (Input::has('cmbCheckSheetNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010CheckSheetNoForSearch', Input::get('cmbCheckSheetNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010CheckSheetNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbCheckSheetNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010CheckSheetNoForSearch', Input::get('cmbCheckSheetNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"CheckSheetNoForSearch"  => Session::get('BA1010CheckSheetNoForSearch')
		];


		//■⑤store Revision No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010RevisionNoForSearch'))) {

			if (Input::has('cmbRevisionNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010RevisionNoForSearch', Input::get('cmbRevisionNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010RevisionNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbRevisionNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010RevisionNoForSearch', Input::get('cmbRevisionNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"RevisionNoForSearch"  => Session::get('BA1010RevisionNoForSearch')
		];


		//■⑥store Material Name for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialNameForSearch'))) {

			if (Input::has('cmbMaterialNameForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialNameForSearch', Input::get('cmbMaterialNameForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MaterialNameForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbMaterialNameForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialNameForSearch', Input::get('cmbMaterialNameForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialNameForSearch"  => Session::get('BA1010MaterialNameForSearch')
		];


		//■⑦store Material Size for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MaterialSizeForSearch'))) {

			if (Input::has('cmbMaterialSizeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialSizeForSearch', Input::get('cmbMaterialSizeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MaterialSizeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbMaterialSizeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MaterialSizeForSearch', Input::get('cmbMaterialSizeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MaterialSizeForSearch"  => Session::get('BA1010MaterialSizeForSearch')
		];


		//■⑧store M/C No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010MachineNoForSearch'))) {

			if (Input::has('txtMachineNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForSearch', Input::get('txtMachineNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010MachineNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtMachineNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010MachineNoForSearch', Input::get('txtMachineNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"MachineNoForSearch"  => Session::get('BA1010MachineNoForSearch')
		];


		//■⑨store Inspector Code for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectorCodeForSearch'))) {

			if (Input::has('txtInspectorCodeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForSearch', Input::get('txtInspectorCodeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectorCodeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectorCodeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectorCodeForSearch', Input::get('txtInspectorCodeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectorCodeForSearch"  => Session::get('BA1010InspectorCodeForSearch')
		];


		//■⑩store Inspection Date（From）for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateFromForSearch'))) {

			if (Input::has('txtInspectionDateFromForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateFromForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateFromForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateFromForSearch', Input::get('txtInspectionDateFromForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateFromForSearch"  => Session::get('BA1010InspectionDateFromForSearch')
		];


		//■⑪store Inspection Date（To）for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionDateToForSearch'))) {

			if (Input::has('txtInspectionDateToForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateToForSearch', Input::get('txtInspectionDateToForSearch'));
			}
			else
			{
				//if value of screen,write down system date in session
				Session::put('BA1010InspectionDateToForSearch', date("d-m-Y"));
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtInspectionDateToForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionDateToForSearch', Input::get('txtInspectionDateToForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or  system date in session and transport to screen
		$pViewData += [
				"InspectionDateToForSearch"  => Session::get('BA1010InspectionDateToForSearch')
		];


		//■⑫store Part No. for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010PartNoForSearch'))) {

			if (Input::has('txtPartNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010PartNoForSearch', Input::get('txtPartNoForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010PartNoForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('txtPartNoForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010PartNoForSearch', Input::get('txtPartNoForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"PartNoForSearch"  => Session::get('BA1010PartNoForSearch')
		];


		//■■13/06/2016 Motooka NG Result■■
		//■⑬store InspectionTime for search
		//in case value in session does not exit
		if (is_null(Session::get('BA1010InspectionTimeForSearch'))) {

			if (Input::has('cmbInspectionTimeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForSearch', Input::get('cmbInspectionTimeForSearch'));
			}
			else
			{
				//if value of screen does not exist,write down as blank
				Session::put('BA1010InspectionTimeForSearch', "");
			}
		}
		else //in case value in session exists
		{
			if (Input::has('cmbInspectionTimeForSearch')) {
				//if value of screen exists,write down in session
				Session::put('BA1010InspectionTimeForSearch', Input::get('cmbInspectionTimeForSearch'));
			}
			else
			{
				//leave because read value in session
			}
		}

		//get value of screen or blank space in session and transport to screen
		$pViewData += [
				"InspectionTimeForSearch"  => Session::get('BA1010InspectionTimeForSearch')
		];

		return $pViewData;
	}

	//**************************************************************************
	// process    getProcessData
	// overview      get information of peocess data from TCODEMST and return to calling
	// argument      value of process data（value of input in screen）
	
	//**************************************************************************
	private function getProcessData($pProcess)
	{
		$lTblProcessInfo = [];       //DataTable

			$lTblProcessInfo = DB::select('
				SELECT PROCESS.CODE_ORDER
  				, PROCESS.CODEORDER_NAME
          		, PROCESS.DISPLAY_ORDER
	            FROM TCODEMST AS PROCESS
	           	WHERE PROCESS.CODE_NO = "002"
			',
				[
					"Process" => TRIM((String)$pProcess),
				]
			);

		return $lTblProcessInfo;
	}

	//**************************************************************************
	// process    getCheckSheetData
	// overview      get information of inspection check sheet from master and return to calling
	// argument      value of Check Sheet No.（value of input in screen）
	// return value    Arary（two dimensions DataTable）
	// author    s-miyamoto
	// date    2014.05.27
	// record of updates  2014.05.27 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getCheckSheetData($pMasterNo, $pProcessId, $pCheckSheetNo)
	{
		$lTblCheckSheetInfo = [];       //DataTable

			$lTblCheckSheetInfo = DB::select('
			          SELECT SHEE.MASTER_NO
			                ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
			                ,SHEE.INSPECTION_SHEET_NO
			                ,SHEE.INSPECTION_SHEET_NAME
			                ,MAX(SHEE.REV_NO) AS MAX_REV_NO
			                ,SHEE.ITEM_NO
			                ,SHEE.ITEM_NAME
			                ,CUST.CUSTOMER_NAME
			                ,SHEE.MODEL_NAME
			                ,SHEE.DRAWING_NO
			                ,SHEE.ISO_KANRI_NO
			                ,SHEE.PRODUCT_WEIGHT
			                ,SHEE.MATERIAL_NAME
			                ,SHEE.MATERIAL_SIZE
			                ,SHEE.HEAT_TREATMENT_TYPE
			                ,SHEE.PLATING_KIND
			            FROM TISHEETM AS SHEE
			      INNER JOIN TCODEMST AS PROCESS
			              ON CODE_NO                  = "002"
			             AND SHEE.PROCESS_ID          = PROCESS.CODE_ORDER 
			 LEFT OUTER JOIN TCUSTOMM AS CUST
			              ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			             AND CUST.DELETE_FLG          = "0"
			           WHERE SHEE.MASTER_NO           = :MasterNo
			             AND SHEE.PROCESS_ID          = :ProcessId
			             AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
			             AND SHEE.DELETE_FLG          = "0"
			',
				[
					"MasterNo" => TRIM((String)$pMasterNo),
					"ProcessId" => TRIM((String)$pProcessId),
					"CheckSheetNo" => TRIM((String)$pCheckSheetNo),
				]
			);

		return $lTblCheckSheetInfo;
	}

	//**************************************************************************
	// process    getMachineData
	// overview      get information of machine from master and return to calling
	// argument      value of M/C No.（value of input in screen）
	// return value    Arary（two dimensions DataTable）
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getMachineData($pMachineNo)
	{
		$lTblMachineInfo = [];       //DataTable

			$lTblMachineInfo = DB::select('
				SELECT MACHINE_NAME
				  FROM TMACHINM
				 WHERE MACHINE_NO = :MachineNo
				   AND DELETE_FLG = "0"
			',
				[
					"MachineNo" => TRIM((String)$pMachineNo),
				]
			);

		return $lTblMachineInfo;
	}

	//**************************************************************************
	// process    getInspectorData
	// overview      get information of inspector from Master and return to calling
	// argument      value of Inspector Code.（value of input in screen）
	// return value    Arary（two dimensions DataTable）
	// author    s-miyamoto
	// date    2014.05.27
	// record of updates  2014.05.27 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getInspectorData($pInspectorCode)
	{
		$lTblInspectorInfo = [];       //DataTable

			$lTblInspectorInfo = DB::select('
				SELECT USER_NAME
				  FROM TUSERMST
				 WHERE USER_ID    = :InspectorCode
				   AND DELETE_FLG = "0"
			',
				[
					"InspectorCode" => TRIM((String)$pInspectorCode),
				]
			);

		return $lTblInspectorInfo;
	}

	//**************************************************************************
	// process    getInspectionResultData
	// overview      search header data of inspection result and return array to calling 
	// argument      Inspection Result No.（inside key per button）、DataRevision
	// return value    Array
	// author    s-miyamoto
	// date    2014.05.28
	// record of updates  2014.05.28 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           ■■16/05/2015 Hoshina added Condition■■
	//**************************************************************************
	private function getInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lTblSearchResultData = [];      //data table inspection result

			$lTblSearchResultData = DB::select('
			    SELECT REHE.MASTER_NO
			    	  ,REHE.PROCESS_ID 
			    	  ,PROCESS.CODEORDER_NAME AS PROCESS_NAME
			    	  ,REHE.INSPECTION_SHEET_NO
			          ,SHEE.INSPECTION_SHEET_NAME
			          ,REHE.REV_NO
			          ,SHEE.ITEM_NO
			          ,SHEE.ITEM_NAME
			          ,REHE.MACHINE_NO
			          ,MACH.MACHINE_NAME
			          ,REHE.INSERT_USER_ID
			          ,USER.USER_NAME
			          ,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
			          ,REHE.CONDITION_CD
				      ,CODE.CODEORDER_NAME
			          ,REHE.INSPECTION_TIME_ID
			          ,TIME.INSPECTION_TIME_NAME
			          ,REHE.MATERIAL_LOT_NO
			          ,REHE.CERTIFICATE_NO
			          ,REHE.PO_NO
			          ,REHE.INVOICE_NO
			          ,CUST.CUSTOMER_NAME
			          ,SHEE.MODEL_NAME
			          ,SHEE.DRAWING_NO
			          ,SHEE.ISO_KANRI_NO
			          ,REHE.DATA_REV
			          ,SHEE.MATERIAL_NAME
			          ,SHEE.MATERIAL_SIZE
			      FROM TRESHEDT AS REHE

			INNER JOIN TISHEETM AS SHEE
			        ON REHE.MASTER_NO           = SHEE.MASTER_NO
			       AND REHE.PROCESS_ID          = SHEE.PROCESS_ID
			       AND REHE.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			       AND REHE.REV_NO              = SHEE.REV_NO

			INNER JOIN TCUSTOMM AS CUST
			        ON SHEE.CUSTOMER_ID = CUST.CUSTOMER_ID

			INNER JOIN TMACHINM AS MACH
			        ON REHE.MACHINE_NO = MACH.MACHINE_NO

			INNER JOIN TINSPTIM AS TIME /* 検査時間・回数マスタ */
			        ON REHE.INSPECTION_TIME_ID = TIME.INSPECTION_TIME_ID

			INNER JOIN TCODEMST AS CODE /* コードマスタ */
			        ON CODE.CODE_NO = "001"
			       AND REHE.CONDITION_CD = CODE.CODE_ORDER

			INNER JOIN TCODEMST AS PROCESS /* コードマスタ */
				    ON PROCESS.CODE_NO = "002"
				   AND REHE.PROCESS_ID = PROCESS.CODE_ORDER

			INNER JOIN TUSERMST AS USER
			        ON REHE.INSERT_USER_ID = USER.USER_ID

			          WHERE REHE.INSPECTION_RESULT_NO = :InspectionResultNo
			            AND REHE.DATA_REV             = :DataRev
			            AND SHEE.DELETE_FLG           = "0"
			            AND CUST.DELETE_FLG           = "0"
			            AND MACH.DELETE_FLG           = "0"
			            AND TIME.DELETE_FLG           = "0"
			            AND USER.DELETE_FLG           = "0"
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"DataRev"            => TRIM((String)$pDataRev),
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process    deleteInspectionResultData
	// overview      delete header and detail of inspection result
	// argument      Inspection Result No.（inside key per button）、DataRevision
	// return value    count of deleted header data
	// author    s-miyamoto
	// date    2014.07.14
	// record of updates  2014.07.14 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function deleteInspectionResultData($pInspectionResultNo, $pDataRev)
	{
		$lDeleteCount = 0;  //count of deleted header data

		$lDeleteCount = DB::delete('
			DELETE FROM TRESHEDT
			      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
			        AND DATA_REV             = :DataRevision
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
				"DataRevision"       => $pDataRev,
			]
		);

		//in case count of deleted header is 0,return count of deleted header data
		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		//delete detail in case detail 1 issue is deleted
		DB::delete('
		DELETE FROM TRESDETT
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		//in case count of deleted detail is 0,return count of detail header data
		if ($lDeleteCount == 0)
		{
			return $lDeleteCount;
		}

		//delete Analy in case analy 1 issue is deleted　分析TBLは必ず存在する
		DB::delete('
		DELETE FROM TRESANAL
		      WHERE INSPECTION_RESULT_NO = :InspectionResultNo
		',
			[
				"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
			]
		);

		return $lDeleteCount;
	}

	//**************************************************************************
	// process         getResult3TableData
	// overview        get transaction Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getResult3TableData($pInspectionResultNo, $pDataRev)
	{
		$lTblResult3TableData = [];      //data table

			$lTblResult3TableData = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
				      ,HEAD.MASTER_NO
				      ,HEAD.PROCESS_ID
				      ,HEAD.INSPECTION_SHEET_NO
			          ,USER.USER_NAME
			          ,HEAD.INSERT_YMDHMS
			          ,HEAD.REV_NO
			          ,HEAD.INSPECTION_YMD
			          ,HEAD.INSPECTION_TIME_ID
			          ,HEAD.CONDITION_CD
			          ,HEAD.MATERIAL_LOT_NO
			          ,HEAD.QUANTITY_WEIGHT
			          ,HEAD.LOT_NO
			          ,DTIL.INSPECTION_NO
			          ,DTIL.INSPECTION_RESULT_VALUE
			          ,DTIL.INSPECTION_RESULT_TYPE
			          ,DTIL.ANALYTICAL_GRP
			          ,DTIL.ANALYTICAL_GRP_POINT
			          ,DTIL.ANALYTICAL_GRP_HOWMANY
			          ,ANALY.MAX_VALUE
			          ,ANALY.MIN_VALUE
			          ,ANALY.AVERAGE_VALUE
			          ,ANALY.RANGE_VALUE
			          ,ANALY.STDEV
			          ,ANALY.JUDGE_REASON
			      FROM TRESHEDT AS HEAD 
			      
			INNER JOIN TRESDETT AS DTIL
			        ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO

			INNER JOIN TUSERMST AS USER 
			 		ON HEAD.INSERT_USER_ID       = USER.USER_ID

			INNER JOIN TRESANAL AS ANALY
			        ON HEAD.MASTER_NO                = ANALY.MASTER_NO
			       AND HEAD.PROCESS_ID               = ANALY.PROCESS_ID
			       AND HEAD.INSPECTION_SHEET_NO      = ANALY.INSPECTION_SHEET_NO
			       AND HEAD.REV_NO                   = ANALY.REV_NO
			       AND HEAD.INSPECTION_YMD           = ANALY.INSPECTION_YMD
			       AND HEAD.INSPECTION_TIME_ID       = ANALY.INSPECTION_TIME_ID
			       AND HEAD.CONDITION_CD             = ANALY.CONDITION_CD
			       AND DTIL.ANALYTICAL_GRP           = ANALY.ANALYTICAL_GRP
			        
			     WHERE HEAD.INSPECTION_RESULT_NO     = :InspectionResultNo
			       AND HEAD.DATA_REV                 = :DataRev
			  ORDER BY DTIL.INSPECTION_NO
			  -- ORDER BY HEAD.DISPLAY_ORDER,
			  --          DTIL.INSPECTION_NO
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
					"DataRev"                => $pDataRev,
				]
			);
		return $lTblResult3TableData;
	}

	//**************************************************************************
	// process         getMasterDataInspectionSheet
	// overview        get print Inspection Sheet Data
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMasterDataInspectionSheet($pMasterNo, $pProcessId, $pInspectionSheetNo, $pRevNo, $pInspectionTimeId)
	{
		$lTblMasterDataInspectionSheet = [];      //data table

			$lTblMasterDataInspectionSheet = DB::select('
				    SELECT SHEE.MASTER_NO
				          ,SHEE.PROCESS_ID
				          ,SHEE.INSPECTION_SHEET_NO
				          ,SHEE.REV_NO
				          ,SHEE.CUSTOMER_ID
				          ,SHEE.ITEM_NO
				          ,SHEE.ITEM_NAME
				          ,SHEE.PRODUCT_WEIGHT
				          ,SHEE.MATERIAL_NAME
				          ,SHEE.MATERIAL_SIZE
				          ,IPNO.INSPECTION_NO
				          ,IPNO.INSPECTION_ITEM
				          ,IPNO.REFER_TO_DOCUMENT_FREQUENCY
				          ,IPNO.INSPECTION_TOOL_CLASS
				          ,IPNO.PICTURE_URL
				          ,IPNO.INSPECTION_RESULT_INPUT_TYPE
				          ,IPNO.THRESHOLD_NG_UNDER
				          ,IPNO.REFERENCE_VALUE
				          ,IPNO.THRESHOLD_NG_OVER
				          ,IPNO.ANALYTICAL_GRP
				          ,IPNO.ANALYTICAL_GRP_POINT
				          ,IPNO.ANALYTICAL_GRP_HOWMANY
				          ,INTM.INSPECTION_TIME_ID
				          ,CUST.CUSTOMER_NAME
				          
				      FROM TISHEETM AS SHEE
				      
				INNER JOIN TINSPNOM AS IPNO
				        ON IPNO.MASTER_NO           = SHEE.MASTER_NO
				       AND IPNO.PROCESS_ID          = SHEE.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = SHEE.REV_NO
				       
				INNER JOIN TINSNTMM AS INTM
				        ON IPNO.MASTER_NO           = INTM.MASTER_NO
				       AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = INTM.REV_NO
				       AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
				       
				INNER JOIN TCUSTOMM AS CUST
				        ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
				       
				     WHERE SHEE.MASTER_NO           = :MasterNo
				       AND SHEE.PROCESS_ID          = :ProcessId
				       AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
				       AND SHEE.REV_NO              = :RevisionNo
				       AND INTM.INSPECTION_TIME_ID  = :InspectionTimeId
				       
				  ORDER BY IPNO.DISPLAY_ORDER
				          ,IPNO.INSPECTION_NO
				',
				[
					"MasterNo"           => TRIM((String)$pMasterNo),
					"ProcessId"          => TRIM((String)$pProcessId),
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"         => TRIM((String)$pRevNo),
					"InspectionTimeId"   => TRIM((String)$pInspectionTimeId),
				]
			);

		return $lTblMasterDataInspectionSheet;
	}


	//**************************************************************************
	// process         getMasterDataInspectionSheetHistory★★
	// overview        get Inspection Sheet History
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMasterDataInspectionSheetHistory($pMasterNo, $pProcessId, $pInspectionSheetNo)
	{
		$lTblMasterDataInspectionSheetHistory = [];      //data table

			$lTblMasterDataInspectionSheetHistory = DB::select('
				    SELECT SHEE.REV_NO
				          ,SHEE.REV_YMD
				          ,SHEE.DRAWING_NO
				          ,SHEE.CHANGE_REV_REASON
				          ,SHEE.REV_PREPARE_NAME
				          ,SHEE.REV_APPROVE_NAME
				      FROM TISHEETM AS SHEE
				     WHERE SHEE.MASTER_NO           = :MasterNo
				       AND SHEE.PROCESS_ID          = :ProcessId
				       AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
				  ORDER BY SHEE.REV_NO DESC
				',
				[
					"MasterNo"           => TRIM((String)$pMasterNo),
					"ProcessId"          => TRIM((String)$pProcessId),
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
				]
			);

		return $lTblMasterDataInspectionSheetHistory;
	}


	//**************************************************************************
	// process         getMasterDataMaxAnalyticalGrp
	// overview        get Master Data Max Analytical Grp
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMasterDataMaxAnalyticalGrp($pMasterNo, $pProcessId, $pInspectionSheetNo, $pRevNo, $pInspectionTimeId)
	{
		$lTblMasterDataMaxAnalyticalGrp = [];      //data table

			$lTblMasterDataMaxAnalyticalGrp = DB::select('
				    SELECT MAX(IPNO.ANALYTICAL_GRP) AS MAX_ANALYTICAL_GRP
				          
				      FROM TISHEETM AS SHEE
				      
				INNER JOIN TINSPNOM AS IPNO
				        ON IPNO.MASTER_NO           = SHEE.MASTER_NO
				       AND IPNO.PROCESS_ID          = SHEE.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = SHEE.REV_NO
				       
				INNER JOIN TINSNTMM AS INTM
				        ON IPNO.MASTER_NO           = INTM.MASTER_NO
				       AND IPNO.PROCESS_ID          = INTM.PROCESS_ID
				       AND IPNO.INSPECTION_SHEET_NO = INTM.INSPECTION_SHEET_NO
				       AND IPNO.REV_NO              = INTM.REV_NO
				       AND IPNO.INSPECTION_NO       = INTM.INSPECTION_NO
				       
				     WHERE SHEE.MASTER_NO           = :MasterNo
				       AND SHEE.PROCESS_ID          = :ProcessId
				       AND SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
				       AND SHEE.REV_NO              = :RevisionNo
				       AND INTM.INSPECTION_TIME_ID  = :InspectionTimeId
				       
				  ORDER BY IPNO.DISPLAY_ORDER
				          ,IPNO.INSPECTION_NO
				',
				[
					"MasterNo"           => TRIM((String)$pMasterNo),
					"ProcessId"          => TRIM((String)$pProcessId),
					"CheckSheetNo"       => TRIM((String)$pInspectionSheetNo),
					"RevisionNo"         => TRIM((String)$pRevNo),
					"InspectionTimeId"   => TRIM((String)$pInspectionTimeId),
				]
			);

		return $lTblMasterDataMaxAnalyticalGrp;
	}


	//**************************************************************************
	// process    initializeSessionData
	// overview      clear session data（when it return from entry screen and delete）
	// argument      Nothing
	// return value    Nothing
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function initializeSessionData()
	{

		//clear search result data
		Session::forget('BA1010InspectionResultData');
		Session::forget('BA1010NotExistNoData');

		//登録条件は保持しておいた方が便利なので残す事にした（コメント化）
		//→コメント解除
		//leave to store entry requirment（as comment）
		Session::forget('BA1010MasterNoForEntry');
		Session::forget('BA1010ProcessForEntry');
		Session::forget('BA1010ProcessForEntryName');
		Session::forget('BA1010CustomerForEntry');
		Session::forget('BA1010CheckSheetNoForEntry');
		Session::forget('BA1010MaterialNameForEntry');
		Session::forget('BA1010MaterialSizeForEntry');
		Session::forget('BA1010MachineNoForEntry');
		Session::forget('BA1010MachineNoForEntryName');
		Session::forget('BA1010InspectorCodeForEntry');
		Session::forget('BA1010InspectionDateForEntry');
		Session::forget('BA1010ConditionForEntry');
		Session::forget('BA1010ConditionForEntryName');
		Session::forget('BA1010InspectionTimeForEntry');
		Session::forget('BA1010InspectionTimeForEntryName');
		Session::forget('BA1010InspectionTimeDropdownListData');

		//検索条件は保持しておいた方が便利なので残すことにした（コメント化）
		//→コメント解除
		//leave to store search requirment（as comment）
		Session::forget('BA1010MasterNoForSearch');
		Session::forget('BA1010ProcessForSearch');
		Session::forget('BA1010CustomerForSearch');
		Session::forget('BA1010CheckSheetNoForSearch');
		Session::forget('BA1010RevisionNoForSearch');
		Session::forget('BA1010MaterialNameForSearch');
		Session::forget('BA1010MaterialSizeForSearch');
		Session::forget('BA1010MachineNoForSearch');
		Session::forget('BA1010InspectorCodeForSearch');
		Session::forget('BA1010InspectionDateFromForSearch');
		Session::forget('BA1010InspectionDateToForSearch');
		Session::forget('BA1010PartNoForSearch');
		Session::forget('BA1010InspectionTimeForSearch');

		//clear what set when Entry button
		Session::forget('BA1010MasterNo');
		Session::forget('BA1010CustomerName');
		Session::forget('BA1010CheckSheetName');
		Session::forget('BA1010MaterialName');
		Session::forget('BA1010MaterialSize');
		Session::forget('BA1010RevisionNo');
		Session::forget('BA1010ItemNo');
		Session::forget('BA1010ItemName');
		Session::forget('BA1010ModelName');
		Session::forget('BA1010DrawingNo');
		Session::forget('BA1010ISOKanriNo');
		Session::forget('BA1010ProductWeight');
		Session::forget('BA1010HeatTreatmentType');
		Session::forget('BA1010PlatingKind');
		Session::forget('BA1010InspectorName');

		//clear what set when Modify button（omit mutual Entry）
		Session::forget('BA1010InspectionResultNo');
		Session::forget('BA1010ProcessForModify');
		Session::forget('BA1010ProcessForModifyName');
		Session::forget('BA1010CheckSheetNoForModify');
		Session::forget('BA1010MachineNoForModify');
		Session::forget('BA1010MachineNoForModifyName');
		Session::forget('BA1010InspectorCodeForModify');
		Session::forget('BA1010InspectionDateForModify');
		Session::forget('BA1010ConditionForModify');
		Session::forget('BA1010ConditionForModifyName');
		Session::forget('BA1010InspectionTimeForModify');
		Session::forget('BA1010InspectionTimeForModifyName');
		Session::forget('BA1010ModifyDataRev');
		Session::forget('BA1010MaterialLotNoForModify');
		Session::forget('BA1010CertificateNoForModify');
		Session::forget('BA1010PoNoForModify');
		Session::forget('BA1010InvoiceNoForModify');
		Session::forget('BA1010MaterialNameForModify');
		Session::forget('BA1010MaterialSizeForModify');
		//Session::forget('BA1010InspectorName');

		return null;
	}

	//**************************************************************************
	// process    isErrorForEntry
	// overview      errorcheck for entry
	// argument      Array to return to screen
	// return value  Array to return to screen
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForEntry($pViewData)
	{
		//must check Master No.
		$lValidator = Validator::make(
			array('txtMasterNoForEntry' => Input::get('txtMasterNoForEntry')),
			array('txtMasterNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E042 : Enter Master No. ."
				]);

				return $pViewData;
		}

		//check Master No. type
		$lValidator = Validator::make(
			array('txtMasterNoForEntry' => Input::get('txtMasterNoForEntry')),
			array('txtMasterNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E043 : Master No. is invalid."
				]);

				return $pViewData;
		}

		//must check Process data
		$lValidator = Validator::make(
			array('cmbProcessForEntry' => Input::get('cmbProcessForEntry')),
			array('cmbProcessForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E044 : Select Process."
				]);

				return $pViewData;
		}

		//check Process data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbProcessForEntry'));

		$lValidator = Validator::make(
			array('cmbProcessForEntry' => $lSlashRemovalValue),
			array('cmbProcessForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E045 : Process is invalid."
				]);

				return $pViewData;
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCustomerForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E049 : Select Customer."
				]);

				return $pViewData;
		}

		//check customer data type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCustomerForEntry'));

		$lValidator = Validator::make(
			array('cmbCustomerForEntry' => $lSlashRemovalValue),
			array('cmbCustomerForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E050 : Customer is invalid."
				]);

				return $pViewData;
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => Input::get('cmbCheckSheetNoForEntry')),
			array('cmbCheckSheetNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E006 : Enter Check Sheet No. ."
				]);

				return $pViewData;
		}

		//check check sheet No. type
		//Validationcheck after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForEntry'));

		$lValidator = Validator::make(
			array('cmbCheckSheetNoForEntry' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E007 : Check Sheet No. is invalid."
				]);

				return $pViewData;
		}

		//must check Material Name
		$lValidator = Validator::make(
			array('cmbMaterialNameForEntry' => Input::get('cmbMaterialNameForEntry')),
			array('cmbMaterialNameForEntry' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E046 : Enter Material Name."
			]);

			return $pViewData;
		}

		//check Material Name Type
		$lValidator = Validator::make(
			array('cmbMaterialNameForEntry' => Input::get('cmbMaterialNameForEntry')),
			array('cmbMaterialNameForEntry' => array('alpha_dash'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E047 : Material Name is invalid."
			]);

			return $pViewData;
		}

		//must check Material Size
		$lValidator = Validator::make(
			array('cmbMaterialSizeForEntry' => Input::get('cmbMaterialSizeForEntry')),
			array('cmbMaterialSizeForEntry' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E048 : Enter Material Size."
			]);

			return $pViewData;
		}

		//check Material Size
		// $lValidator = Validator::make(
		// 	array('cmbMaterialSizeForEntry' => Input::get('cmbMaterialSizeForEntry')),
		// 	array('cmbMaterialSizeForEntry' => array('alpha_dash'))
		// );
		// //error
		// if($lValidator->fails()) {
		// 	//error message
		// 	$pViewData["errors"] = new MessageBag([
		// 		"error" => "E010 : Material Size is invalid."
		// 	]);

		// 	return $pViewData;
		// }

		//must check M/C No.
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E009 : Enter M/C No. ."
				]);

				return $pViewData;
		}

		//check M/C No. type
		$lValidator = Validator::make(
			array('txtMachineNoForEntry' => Input::get('txtMachineNoForEntry')),
			array('txtMachineNoForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E010 : M/C No. is invalid."
				]);

				return $pViewData;
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E012 : Enter Inspector Code."
				]);

				return $pViewData;
		}

		//check inspector code type
		$lValidator = Validator::make(
			array('txtInspectorCodeForEntry' => Input::get('txtInspectorCodeForEntry')),
			array('txtInspectorCodeForEntry' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E013 : Inspector Code is invalid."
				]);

				return $pViewData;
		}

		//must check inspection date
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		//check inspection date type
		$lValidator = Validator::make(
			array('txtInspectionDateForEntry' => Input::get('txtInspectionDateForEntry')),
			array('txtInspectionDateForEntry' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E016 : Inspection Date is invalid."
				]);

				return $pViewData;
		}

		//■■16/05/2015 Hoshina added Condition■■
		//must check inspection nomal/abnomal
		$lValidator = Validator::make(
			array('cmbConditionForEntry' => Input::get('cmbConditionForEntry')),
			array('cmbConditionForEntry' => array('required'))
		);

		//error
		if ($lValidator->fails()) {
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E037 : Select Condition."
				]);

				return $pViewData;
		}

		//must check inspection time
		$lValidator = Validator::make(
			array('cmbInspectionTimeForEntry' => Input::get('cmbInspectionTimeForEntry')),
			array('cmbInspectionTimeForEntry' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E017 : Select Inspection Time."
				]);

				return $pViewData;
		}

		return $pViewData;
	}

	//**************************************************************************
	// process             isErrorForSearch
	// overview            check type in search
	// argument            Array to return to screen
	// return value        Array to return to screen
	// author              s-miyamoto
	// date                2014.07.15
	// record of updates   2014.07.15 v0.01 first making
	//                     2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForSearch($pViewData)
	{
		//check Master No. type
		$lValidator = Validator::make(
			array('txtMasterNoForSearch'	=> Input::get('txtMasterNoForSearch')),
			array('txtMasterNoForSearch'	=> array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E043 : Master No. is invalid."
			]);

			return $pViewData;
		}

		//check Process type
		$lValidator = Validator::make(
			array('cmbProcessForSearch'	=> Input::get('cmbProcessForSearch')),
			array('cmbProcessForSearch'	=> array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E045 : Process is invalid."
			]);
			return $pViewData;
		}

		//check customer type
		$lValidator = Validator::make(
			array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
			array('cmbCustomerForSearch' => array('alpha_dash'))
		);
		//error
		if($lValidator->fails())
		{
			//error message 
			$pViewData["errors"] = new MessageBag([
				"error" => "E050 : Customer is invalid."
			]);
			return $pViewData;
		}

		//check check sheet No. type
		//Validation check after deleting en thrash because Laravel alpha_dash can not pass thrash
		$lSlashRemovalValue = str_replace("/", "", Input::get('cmbCheckSheetNoForSearch'));

		$lValidator = Validator::make(
			array('cmbCheckSheetNoForSearch' => $lSlashRemovalValue),
			array('cmbCheckSheetNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E007 : Check Sheet No. is invalid."
			]);

			return $pViewData;
		}

		//check Revision No. type
		$lValidator = Validator::make(
			array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
			array('cmbRevisionNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E018 : Revision No. is invalid."
			]);

			return $pViewData;
		}

		//check Material Name type
		$lValidator = Validator::make(
			array('cmbMaterialNameForSearch' => Input::get('cmbMaterialNameForSearch')),
			array('cmbMaterialNameForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E047 : Material Name is invalid."
			]);

			return $pViewData;
		}

		//check Material Size type
		//$lValidator = Validator::make(
		//	array('cmbMaterialSizeForSearch' => Input::get('cmbMaterialSizeForSearch')),
		//	array('cmbMaterialSizeForSearch' => array('alpha_dash'))
		//);
		//error
		//if ($lValidator->fails())
		//{
		//	//error message
		//	$pViewData["errors"] = new MessageBag([
		//		"error" => "E★★ : ★★."
		//	]);
		//
		//	return $pViewData;
		//}

		//check M/C No. type
		$lValidator = Validator::make(
			array('txtMachineNoForSearch' => Input::get('txtMachineNoForSearch')),
			array('txtMachineNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E010 : M/C No. is invalid."
			]);

			return $pViewData;
		}

		//check inspector code type
		$lValidator = Validator::make(
			array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
			array('txtInspectorCodeForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E013 : Inspector Code is invalid."
			]);

			return $pViewData;
		}

		//check inspection date type(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);

			return $pViewData;
		}

		//check inspection date type(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('date_format:d-m-Y'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E016 : Inspection Date is invalid."
			]);

			return $pViewData;
		}

		//check inspection from/to date(must check From/To)
		if ((Input::get('txtInspectionDateFromForSearch') != "") and (Input::get('txtInspectionDateToForSearch') != ""))
		{
			//change to data type
			$lDateFrom = date_create(Input::get('txtInspectionDateFromForSearch'));
			$lDateTo   = date_create(Input::get('txtInspectionDateToForSearch'));

			//if it mistake large or small,error
			if ($lDateFrom > $lDateTo)
			{
				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E019 : The relation of Inspection Date(From/To) is incorrect."
				]);

				return $pViewData;
			}
		}

		//check Part No type
		$lValidator = Validator::make(
			array('txtPartNoForSearch' => Input::get('txtPartNoForSearch')),
			array('txtPartNoForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E020 : Part No. is invalid."
			]);

			return $pViewData;
		}


		//check Inspection time type
		$lValidator = Validator::make(
			array('cmbInspectionTimeForSearch' => Input::get('cmbInspectionTimeForSearch')),
			array('cmbInspectionTimeForSearch' => array('alpha_dash'))
		);
		//error
		if ($lValidator->fails())
		{
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E051 : Inspection Time is invalid."
			]);

			return $pViewData;
		}

		return $pViewData;
	}

	//**************************************************************************
	// process         isErrorForSearchEnterCheck
	// overview        Enter Check
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function isErrorForSearchEnterCheck($pViewData)
	{
		//must check inspection date(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		//must check inspection date(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		return $pViewData;

	}

	//**************************************************************************
	// process    isErrorForShonin
	// overview      error check for CSV for approval
	// argument      Array to return to screen
	// return value    Array to return to screen
	// author    s-miyamoto
	// date    2014.07.15
	// record of updates  2014.07.15 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function isErrorForShonin($pViewData)
	{
		//must check Master No.
		$lValidator = Validator::make(
			array('txtMasterNoForSearch' => Input::get('txtMasterNoForSearch')),
			array('txtMasterNoForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E042 : Enter Master No. ."
				]);

				return $pViewData;
		}

		//must check Process
		$lValidator = Validator::make(
			array('cmbProcessForSearch' => Input::get('cmbProcessForSearch')),
			array('cmbProcessForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E044 : Select Process."
				]);

				return $pViewData;
		}

		//must check customer
		$lValidator = Validator::make(
			array('cmbCustomerForSearch' => Input::get('cmbCustomerForSearch')),
			array('cmbCustomerForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E049 : Select Customer."
				]);

				return $pViewData;
		}

		//must check inspection check sheet No.
		$lValidator = Validator::make(
			array('cmbCheckSheetNoForSearch' => Input::get('cmbCheckSheetNoForSearch')),
			array('cmbCheckSheetNoForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E006 : Enter Check Sheet No. ."
				]);

				return $pViewData;
		}

		//must check revision No.
		$lValidator = Validator::make(
			array('cmbRevisionNoForSearch' => Input::get('cmbRevisionNoForSearch')),
			array('cmbRevisionNoForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E035 : Enter Revision No. ."
				]);

				return $pViewData;
		}

		//must check Material Name
		$lValidator = Validator::make(
			array('cmbMaterialNameForSearch' => Input::get('cmbMaterialNameForSearch')),
			array('cmbMaterialNameForSearch' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E046 : Enter Material Name."
			]);

			return $pViewData;
		}

		//must check Material Size
		$lValidator = Validator::make(
			array('cmbMaterialSizeForSearch' => Input::get('cmbMaterialSizeForSearch')),
			array('cmbMaterialSizeForSearch' => array('required'))
		);
		//error
		if($lValidator->fails()) {
			//error message
			$pViewData["errors"] = new MessageBag([
				"error" => "E048 : Enter Material Size."
			]);

			return $pViewData;
		}

		//must check inspector code
		$lValidator = Validator::make(
			array('txtInspectorCodeForSearch' => Input::get('txtInspectorCodeForSearch')),
			array('txtInspectorCodeForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E012 : Enter Inspector Code."
				]);

				return $pViewData;
		}

		//must check inspection date(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		//must check inspection date(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		return $pViewData;

	}

	//**************************************************************************
	// process         isErrorForNGResult
	// overview        NG Result document errorcheck
	// argument        Array to return to screen
	// return value    Array to return to screen
	// author          m-motooka
	// date            2016.06.13
	// record of updates
	//**************************************************************************
	private function isErrorForNGResult($pViewData)
	{
		//must check inspection date(From)
		$lValidator = Validator::make(
			array('txtInspectionDateFromForSearch' => Input::get('txtInspectionDateFromForSearch')),
			array('txtInspectionDateFromForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);
				return $pViewData;
		}

		//must check inspection date(To)
		$lValidator = Validator::make(
			array('txtInspectionDateToForSearch' => Input::get('txtInspectionDateToForSearch')),
			array('txtInspectionDateToForSearch' => array('required'))
		);
		//error
		if ($lValidator->fails()) {

				//error message
				$pViewData["errors"] = new MessageBag([
					"error" => "E015 : Enter Inspection Date."
				]);

				return $pViewData;
		}

		return $pViewData;

	}

	//**************************************************************************
	// process    insertLineStopData
	// overview      register Inspection result Header and detail,update
	// argument      Inspection Result No.
	//           Data Revision
	//           User ID（login ID）
	// return value    Nothing
	// author    s-miyamoto
	// date    2014.07.18
	// record of updates  2014.07.18 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           ■■16/05/2015 Hoshina added Condition■■
	//**************************************************************************
	private function insertLineStopData($pInspectionResultNo, $pDataRev, $pUserID)
	{

		$lTblLineStopTargetInspectionNo = [];       //LineStop扱いにする対象Inspection No.のDataTable
		$lRowLineStopTargetInspectionNo = [];       //LineStop扱いにする対象Inspection No.のDataRow
		$lTblLineStopTargetAnalyticalGroup = [];    //LineStop扱いにする対象AnalyticalGroupのDataTable
		$lRowLineStopTargetAnalyticalGroup = [];    //LineStop扱いにする対象AnalyticalGroupのDataRow

		//extraction of data
		//extract Inspection No. with Inspection Time from Header and result is null by external coupling
		//検査予定はあるが検査していない検査番号は実績TBLは作成されていないので、その番号を見つけるSQL
			$lTblLineStopTargetInspectionNo = DB::select('
			         SELECT ISNO.INSPECTION_NO
			           FROM TRESHEDT AS HEAD /* 検査実績ヘッダ */

			     INNER JOIN TISHEETM AS SHEE /* 検査シートマスタ */
			             ON HEAD.MASTER_NO           = SHEE.MASTER_NO
			            AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			            AND HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			            AND HEAD.REV_NO              = SHEE.REV_NO

			     INNER JOIN TINSPNOM AS ISNO /* Inspection No.マスタ */
			             ON SHEE.MASTER_NO           = ISNO.MASTER_NO
			            AND SHEE.PROCESS_ID          = ISNO.PROCESS_ID
			            AND SHEE.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
			            AND SHEE.REV_NO              = ISNO.REV_NO

			     INNER JOIN TINSNTMM AS ISTM /* Inspection No.時間帯マスタ */
			             ON ISNO.MASTER_NO           = ISTM.MASTER_NO
			            AND ISNO.PROCESS_ID          = ISTM.PROCESS_ID
			            AND ISNO.INSPECTION_SHEET_NO = ISTM.INSPECTION_SHEET_NO
			            AND ISNO.REV_NO              = ISTM.REV_NO
			            AND ISNO.INSPECTION_NO       = ISTM.INSPECTION_NO
			            AND HEAD.INSPECTION_TIME_ID  = ISTM.INSPECTION_TIME_ID
			            AND HEAD.INSPECTION_TIME_ID  = IF(HEAD.CONDITION_CD <> "01", "ZZ", ISTM.INSPECTION_TIME_ID)  /* 条件分岐させる Condition≠01⇒timeID・Condition≠01⇒"ZZ" */

			LEFT OUTER JOIN TRESDETT AS DETL /* 検査実績明細 */
			             ON HEAD.INSPECTION_RESULT_NO = DETL.INSPECTION_RESULT_NO
			            AND ISNO.INSPECTION_NO        = DETL.INSPECTION_NO

			          WHERE HEAD.INSPECTION_RESULT_NO = :InspectionResultNo
			            AND SHEE.DELETE_FLG           = "0"
			            AND ISNO.DELETE_FLG           = "0"
			            AND DETL.INSPECTION_NO        IS NULL

			       ORDER BY ISNO.DISPLAY_ORDER
			               ,ISNO.INSPECTION_NO
			',
				[
					"InspectionResultNo" => $pInspectionResultNo,
				]
			);

			$lTblLineStopTargetAnalyticalGroup = DB::select('
			         SELECT ISNO.MASTER_NO
			               ,ISNO.PROCESS_ID
			               ,ISNO.INSPECTION_SHEET_NO
			               ,ISNO.REV_NO
			               ,HEAD.INSPECTION_YMD
			               ,HEAD.INSPECTION_TIME_ID
			               ,HEAD.CONDITION_CD
			               ,ISNO.ANALYTICAL_GRP
			           FROM TRESHEDT AS HEAD /* 検査実績ヘッダ */

			     INNER JOIN TISHEETM AS SHEE /* 検査シートマスタ */
			             ON HEAD.MASTER_NO           = SHEE.MASTER_NO
			            AND HEAD.PROCESS_ID          = SHEE.PROCESS_ID
			            AND HEAD.INSPECTION_SHEET_NO = SHEE.INSPECTION_SHEET_NO
			            AND HEAD.REV_NO              = SHEE.REV_NO

			     INNER JOIN TINSPNOM AS ISNO /* Inspection No.マスタ */
			             ON SHEE.MASTER_NO           = ISNO.MASTER_NO
			            AND SHEE.PROCESS_ID          = ISNO.PROCESS_ID
			            AND SHEE.INSPECTION_SHEET_NO = ISNO.INSPECTION_SHEET_NO
			            AND SHEE.REV_NO              = ISNO.REV_NO

			     INNER JOIN TINSNTMM AS ISTM /* Inspection No.時間帯マスタ */
			             ON ISNO.MASTER_NO           = ISTM.MASTER_NO
			            AND ISNO.PROCESS_ID          = ISTM.PROCESS_ID
			            AND ISNO.INSPECTION_SHEET_NO = ISTM.INSPECTION_SHEET_NO
			            AND ISNO.REV_NO              = ISTM.REV_NO
			            AND ISNO.INSPECTION_NO       = ISTM.INSPECTION_NO
			            AND HEAD.INSPECTION_TIME_ID  = ISTM.INSPECTION_TIME_ID
			            AND HEAD.INSPECTION_TIME_ID  = IF(HEAD.CONDITION_CD <> "01", "ZZ", ISTM.INSPECTION_TIME_ID)  /* 条件分岐させる Condition≠01⇒timeID・Condition≠01⇒"ZZ" */

			LEFT OUTER JOIN TRESDETT AS DETL /* 検査実績明細 */
			             ON HEAD.INSPECTION_RESULT_NO = DETL.INSPECTION_RESULT_NO
			            AND ISNO.INSPECTION_NO        = DETL.INSPECTION_NO

			          WHERE HEAD.INSPECTION_RESULT_NO = :InspectionResultNo
			            AND SHEE.DELETE_FLG           = "0"
			            AND ISNO.DELETE_FLG           = "0"
			            AND DETL.INSPECTION_NO        IS NULL

			       GROUP BY ISNO.ANALYTICAL_GRP

			       ORDER BY ISNO.ANALYTICAL_GRP
			',
				[
					"InspectionResultNo" => $pInspectionResultNo,
				]
			);

		//1 transaction for header and detail and Analy
			DB::transaction(
			function() use ($pInspectionResultNo
			               ,$pDataRev
			               ,$pUserID
			               ,$lTblLineStopTargetInspectionNo
			               ,$lRowLineStopTargetInspectionNo
			               ,$lTblLineStopTargetAnalyticalGroup
			               ,$lRowLineStopTargetAnalyticalGroup)
			{
				//update header
				DB::update('
						UPDATE TRESHEDT
						   SET LAST_UPDATE_USER_ID = :LastUpdateUserID
						      ,UPDATE_YMDHMS = now()
						      ,DATA_REV = DATA_REV + 1
						 WHERE INSPECTION_RESULT_NO = :InspectionResultNo
						   AND DATA_REV             = :DataRevision
				',
					[
						"LastUpdateUserID"    => TRIM((String)$pUserID),
						"InspectionResultNo"  => $pInspectionResultNo,
						"DataRevision"        => $pDataRev,
					]
				);

				//register detail while looping data
				foreach ($lTblLineStopTargetInspectionNo as $lRowLineStopTargetInspectionNo) {

					//Cast previously　because it has erroe to read value of array while CASTing
					$lRowLineStopTargetInspectionNo = (Array)$lRowLineStopTargetInspectionNo;

					//register detail
					DB::insert('
					            INSERT INTO TRESDETT
					                       (INSPECTION_RESULT_NO
					                       ,INSPECTION_NO
					                       ,INSPECTION_RESULT_VALUE
					                       ,INSPECTION_RESULT_TYPE
					                       ,OFFSET_NG_REASON
					                       ,OFFSET_NG_ACTION
					                       ,OFFSET_NG_ACTION_DTL_INTERNAL
					                       ,OFFSET_NG_ACTION_DTL_REQUEST
					                       ,OFFSET_NG_ACTION_DTL_TSR
					                       ,OFFSET_NG_ACTION_DTL_SORTING
					                       ,OFFSET_NG_ACTION_DTL_NOACTION
					                       ,ACTION_DETAILS_TR_NO_TSR_NO
					                       ,TR_NO
					                       ,TECHNICIAN_USER_ID
					                       ,TSR_NO
					                       ,SOKUTEIKI_KANRI_NO
					                       ,ANALYTICAL_GRP
					                       ,ANALYTICAL_GRP_POINT
					                       ,ANALYTICAL_GRP_HOWMANY
					                       ,INSERT_YMDHMS
					                       ,LAST_UPDATE_USER_ID
					                       ,UPDATE_YMDHMS
					                       ,DATA_REV)
					                 VALUES (:InspectionResultNo
					                        ,:InspectionNo
					                        ,""
					                        ,"5"
					                        ,""
					                        ,""
					                        ,"0"
					                        ,"0"
					                        ,"0"
					                        ,"0"
					                        ,"0"
					                        ,""
					                        ,""
					                        ,""
					                        ,""
					                        ,""
					                        ,"0"
					                        ,"0"
					                        ,"0"
					                        ,now()
					                        ,:LastUpdateUserID1
					                        ,now()
					                        ,1)
					       ON DUPLICATE KEY
					                 UPDATE LAST_UPDATE_USER_ID           = :LastUpdateUserID2
					                       ,UPDATE_YMDHMS                 = now()
					                       ,DATA_REV                      = DATA_REV + 1
					',
						[
							"InspectionResultNo"      => $pInspectionResultNo,
							"InspectionNo"            => TRIM((String)$lRowLineStopTargetInspectionNo["INSPECTION_NO"]),
							"LastUpdateUserID1"       => TRIM((String)$pUserID),
							"LastUpdateUserID2"       => TRIM((String)$pUserID),
						]
					);
				}

				//register detail while looping data
				foreach ($lTblLineStopTargetAnalyticalGroup as $lRowLineStopTargetAnalyticalGroup) {

					//Cast previously　because it has erroe to read value of array while CASTing
					$lRowLineStopTargetAnalyticalGroup = (Array)$lRowLineStopTargetAnalyticalGroup;

					//register detail
					DB::insert('
					            INSERT INTO TRESANAL
					                       (MASTER_NO
					                       ,PROCESS_ID
					                       ,INSPECTION_SHEET_NO
					                       ,REV_NO
					                       ,INSPECTION_YMD
					                       ,INSPECTION_TIME_ID
					                       ,CONDITION_CD
					                       ,ANALYTICAL_GRP
					                       ,INSPECTION_RESULT_NO
					                       ,STD_MIN_SPEC
					                       ,STD_MAX_SPEC
					                       ,PICTURE_URL
					                       ,CALC_NUM
					                       ,MAX_VALUE
					                       ,MIN_VALUE
					                       ,AVERAGE_VALUE
					                       ,RANGE_VALUE
					                       ,STDEV
					                       ,CPK
					                       ,XBAR_UCL
					                       ,XBAR_LCL
					                       ,RCHART_UCL
					                       ,RCHART_LCL
					                       ,INSERT_YMDHMS
					                       ,LAST_UPDATE_USER_ID
					                       ,UPDATE_YMDHMS
					                       ,DATA_REV)
					                 VALUES (:MasterNo
					                        ,:ProcessId
					                        ,:InspectionSheetNo
					                        ,:RevisionNo
					                        ,:InspectionDate
					                        ,:InspectionTimeID
					                        ,:ConditionType
					                        ,:AnalyticalGroup
					                        ,:InspectionResultNo
					                        ,0
					                        ,0
					                        ,""
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,0
					                        ,now()
					                        ,:LastUpdateUserID1
					                        ,now()
					                        ,1)
					       ON DUPLICATE KEY
					                 UPDATE LAST_UPDATE_USER_ID           = :LastUpdateUserID2
					                       ,UPDATE_YMDHMS                 = now()
					                       ,DATA_REV                      = DATA_REV + 1
					',
						[
							"MasterNo"                => TRIM((String)$lRowLineStopTargetAnalyticalGroup["MASTER_NO"]),
							"ProcessId"               => TRIM((String)$lRowLineStopTargetAnalyticalGroup["PROCESS_ID"]),
							"InspectionSheetNo"       => TRIM((String)$lRowLineStopTargetAnalyticalGroup["INSPECTION_SHEET_NO"]),
							"RevisionNo"              => TRIM((String)$lRowLineStopTargetAnalyticalGroup["REV_NO"]),
							"InspectionDate"          => TRIM((String)$lRowLineStopTargetAnalyticalGroup["INSPECTION_YMD"]),
							"InspectionTimeID"        => TRIM((String)$lRowLineStopTargetAnalyticalGroup["INSPECTION_TIME_ID"]),
							"ConditionType"           => TRIM((String)$lRowLineStopTargetAnalyticalGroup["CONDITION_CD"]),
							"AnalyticalGroup"         => TRIM((String)$lRowLineStopTargetAnalyticalGroup["ANALYTICAL_GRP"]),
							"InspectionResultNo"      => $pInspectionResultNo,
							"LastUpdateUserID1"       => TRIM((String)$pUserID),
							"LastUpdateUserID2"       => TRIM((String)$pUserID),
						]
					);
				}

				//return from obscurity function to the caller
				return true;
			}

		);

		return true;

	}

	//**************************************************************************
	// process    convertDateFormat
	// overview      change data type
	//           
	// argument      date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//           pattern(1:English->Japanese 2:Japanese->English)
	// return value    Nothing
	// author    s-miyamoto
	// date    2014.07.30
	// record of updates  2014.07.30 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{

		//not to entry date
		if ($pTargetDate == "") {
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1") {

			return date_format($lWorkDate, 'Y/m/d');

		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');

	}

	//**************************************************************************
	// process         getInspectionSheetMasterDataCount
	// overview        select master data (use input item)
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getInspectionSheetMasterDataCount($pMasterNo ,$pProcess ,$pCustomer ,$pCheckSheetNo ,$pMaterialName ,$pMaterialSize)
	{
		$lTblInspectionSheetMasterDataCount = [];       //DataTable
		$lRowInspectionSheetMasterDataCount = [];       //DataRow
		$lInspectionSheetMasterDataCount    = 0;        //result of COUNT

			$lTblInspectionSheetMasterDataCount = DB::select('
				    SELECT COUNT(SHEET.MASTER_NO) AS COUNT
				      FROM TISHEETM AS SHEET
				     WHERE SHEET.MASTER_NO            = :MasterNo
				       AND SHEET.PROCESS_ID           = :Process
				       AND SHEET.CUSTOMER_ID          = :Customer
				       AND SHEET.INSPECTION_SHEET_NO  = :InspectionSheetNo
				       AND SHEET.MATERIAL_NAME        = :MaterialName
				       AND SHEET.MATERIAL_SIZE        = :MaterialSize
				       AND SHEET.DELETE_FLG           = "0"
			',
				[
					"MasterNo"            => TRIM((String)$pMasterNo),
					"Process"             => TRIM((String)$pProcess),
					"Customer"            => TRIM((String)$pCustomer),
					"InspectionSheetNo"   => TRIM((String)$pCheckSheetNo),
					"MaterialName"        => TRIM((String)$pMaterialName),
					"MaterialSize"        => TRIM((String)$pMaterialSize),
				]
			);

		//read result(1 line)
		$lRowInspectionSheetMasterDataCount = $lTblInspectionSheetMasterDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowInspectionSheetMasterDataCount = (Array)$lRowInspectionSheetMasterDataCount;

		//
		$lInspectionSheetMasterDataCount = $lRowInspectionSheetMasterDataCount["COUNT"];

		return $lInspectionSheetMasterDataCount;
	}

	//**************************************************************************
	// process   getLogicalDuplicationDataCount
	// overview  get count of redundant data of logic key 論理キー重複データ件数取得
	//           return count of redundant data of logic key
	// argument  Check Sheet No.（value of input in screen）
	//           Data Revision（MAX value per Check Sheet No.）
	//           M/C No.（value of input in screen）
	//           Inspector Code（value of input in screen）
	//           Inspection Date（value of input in screen）
	//           Condition（value of input in screen）
	//           Inspection Time（value of input in screen）
	// return value    Int（count of redundant data >=0 or 1）
	// author    s-miyamoto
	// date      2014.07.31
	// record of updates  2014.07.31 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//           ■■16/05/2015 Hoshina added Condition■■
	//**************************************************************************
	private function getLogicalDuplicationDataCount($pMasterNo
	                                               ,$pProcess
	                                               ,$pCheckSheetNo
	                                               ,$pDataRevision
	                                               ,$pMachineNo
	                                               ,$pInspectorCode
	                                               ,$pInspectionDate
	                                               ,$pCondition
	                                               ,$pInspectionTime
	                                               )
	{
		$lTblLogicalDupulicationDataCount = [];       //DataTable
		$lRowLogicalDupulicationDataCount = [];       //DataRow
		$lDataCount                       = 0;        //result of COUNT

			$lTblLogicalDupulicationDataCount = DB::select('
					SELECT COUNT("X") AS COUNT
					  FROM TRESHEDT AS HEAD
					 WHERE MASTER_NO           = :MasterNo
					   AND PROCESS_ID          = :Process
					   AND INSPECTION_SHEET_NO = :InspectionSheetNo
					   AND REV_NO              = :RevisionNo
					   AND MACHINE_NO          = :MachineNo
					   AND INSERT_USER_ID      = :InspectorCode
					   AND INSPECTION_YMD      = :InspectionDate
					   AND CONDITION_CD        = :Condition
					   AND INSPECTION_TIME_ID  = :InspectionTime
			',
				[
					"MasterNo"          => TRIM((String)$pMasterNo),
					"Process"           => TRIM((String)$pProcess),
					"InspectionSheetNo" => TRIM((String)$pCheckSheetNo),
					"RevisionNo"        => TRIM((String)$pDataRevision),
					"MachineNo"         => TRIM((String)$pMachineNo),
					"InspectorCode"     => TRIM((String)$pInspectorCode),
					"InspectionDate"    => $this->convertDateFormat((String)$pInspectionDate, "1"),
					"Condition"         => TRIM((String)$pCondition),
					"InspectionTime"    => TRIM((String)$pInspectionTime),
				]
			);

		//read result(1 line)
		$lRowLogicalDupulicationDataCount = $lTblLogicalDupulicationDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowLogicalDupulicationDataCount = (Array)$lRowLogicalDupulicationDataCount;

		//get count
		$lDataCount = $lRowLogicalDupulicationDataCount["COUNT"];

		return $lDataCount;
	}

	//**************************************************************************
	// process    getCheckNoDataCount
	// overview      return count corresponding to 
	//           Check Sheet No.、Revsion No.、Inspection Time
	//           if Condition≠nomal,Inspection Time="ZZ"
	// argument      Check Sheet No.（value of input in screen）
	//           Data Revision（MAX value per Check Sheet No.）
	//           Inspection Time（value of input in screen）
	// return value    Int（count of Inspection No. data corresponding to the TIME >=0 or 1）
	// author    s-miyamoto
	// date    2014.07.31
	// record of updates  2014.07.31 v0.01 first making
	//           2014.08.26 v1.00 FIX
	//**************************************************************************
	private function getCheckNoDataCount($pMasterNo
	                                    ,$pProcess
	                                    ,$pCheckSheetNo
	                                    ,$pDataRevision
	                                    ,$pInspectionTime
	                                    )
	{
		$lTblCheckNoDataCount = [];       //DataTable
		$lRowCheckNoDataCount = [];       //DataRow
		$lDataCount           = 0;        //result of COUNT

			$lTblCheckNoDataCount = DB::select('
				    SELECT COUNT(ISTM.INSPECTION_TIME_ID) AS NO_COUNT
				      FROM TINSPNOM AS ISNO
				INNER JOIN TINSNTMM AS ISTM
				        ON ISNO.MASTER_NO = ISTM.MASTER_NO
				       AND ISNO.PROCESS_ID = ISTM.PROCESS_ID
				       AND ISNO.INSPECTION_SHEET_NO = ISTM.INSPECTION_SHEET_NO
				       AND ISNO.REV_NO              = ISTM.REV_NO
				       AND ISNO.INSPECTION_NO       = ISTM.INSPECTION_NO
				     WHERE ISNO.MASTER_NO = :MasterNo
				       AND ISNO.PROCESS_ID = :Process
				       AND ISNO.INSPECTION_SHEET_NO = :InspectionSheetNo
				       AND ISNO.REV_NO              = :RevisionNo
				       AND ISTM.INSPECTION_TIME_ID  = :InspectionTime
				       AND ISNO.DELETE_FLG          = "0"
				       AND ISTM.DELETE_FLG          = "0"
			',
				[
					"MasterNo"          => TRIM((String)$pMasterNo),
					"Process"           => TRIM((String)$pProcess),
					"InspectionSheetNo" => TRIM((String)$pCheckSheetNo),
					"RevisionNo"        => TRIM((String)$pDataRevision),
					"InspectionTime"    => TRIM((String)$pInspectionTime),
				]
			);

		//echo $pCheckSheetNo . '<br />' . $pDataRevision . '<br />' . $pInspectionTime; die();

		//read result(1 line)
		$lRowCheckNoDataCount = $lTblCheckNoDataCount[0];

		//Cast previously　because it has erroe to read value of array while CASTing
		$lRowCheckNoDataCount = (Array)$lRowCheckNoDataCount;

		//
		$lDataCount = $lRowCheckNoDataCount["NO_COUNT"];

		return $lDataCount;

	}


	//**************************************************************************
	// process         setProcessList
	// overview        get data for process data combo box and set to Array for displaying screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setProcessList($pViewData)
	{
		$lArrProcessList     = ["" => ""];  //view data to return

		//if process list does not exist in session, search and store in session
		if (is_null(Session::get('BA1010SetProcessDropdownListData'))) {

			//search
			$lArrProcessList = $this->getProcessList();

			//store in session
			Session::put('BA1010SetProcessDropdownListData', $lArrProcessList); //
		}
		else
		{
			//get from session
			$lArrProcessList = Session::get('BA1010SetProcessDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListProcessList" => $lArrProcessList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getProcessList
	// overview        get data for process data combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getProcessList()
	{
		$lTblProcessList			= []; //result of getting information of code order name
		$lRowProcessList			= []; //work when it change to array
		$lArrDataProcessList		= []; //List of code order name to return

		$lTblProcessList = DB::select(
		'
			SELECT PROCESS.CODE_ORDER
			     , PROCESS.CODEORDER_NAME
			     , PROCESS.DISPLAY_ORDER
			  FROM TCODEMST AS PROCESS
			 WHERE CODE_NO = "002"
			 ORDER BY DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblProcessList = (array)$lTblProcessList;

		//add blank space
		$lArrDataProcessList += [
			"" => ""
		];

		//data exist
		if ($lTblProcessList != null)
		{
			//store result in Array again
			foreach ($lTblProcessList as $lRowProcessList) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowProcessList = (Array)$lRowProcessList;

				$lArrDataProcessList += [
					$lRowProcessList["CODE_ORDER"] => $lRowProcessList["CODEORDER_NAME"]
				];
			}

		}

		return $lArrDataProcessList;
	}

	//**************************************************************************
	// process         setCustomerList
	// overview        get data for customer combo box and set to Array for displaying screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCustomerList($pViewData)
	{
		$lArrCustomerList     = ["" => ""];  //view data to return

		//if cutomer list does not exist in session, search and store in session
		if (is_null(Session::get('BA1010SetCustemorDropdownListData'))) {
			//search
			$lArrCustomerList = $this->getCustomerList();

			//store in session
			Session::put('BA1010SetCustemorDropdownListData', $lArrCustomerList);
		}
		else
		{
			//get from session
			$lArrCustomerList = Session::get('BA1010SetCustemorDropdownListData');
		}

		//add to Array to transport to screen（write in +=）
		$pViewData += [
			"arrDataListCustomerList" => $lArrCustomerList,
			"arrDataListCustomerForSearchList" => $lArrCustomerList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getCustomerList
	// overview        get data for customer combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCustomerList()
	{
		$lTblCustomerList			= []; //result of getting information of customer
		$lRowCustomerList			= []; //work when it change to array
		$lArrDataCustomerList		= []; //List of customer to return

		$lTblCustomerList = DB::select(
		'
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TCUSTOMM AS CUST
		       WHERE CUST.DELETE_FLG = "0"
		    ORDER BY CUST.DISPLAY_ORDER
		'
		);

		//cast search result to array（two dimensions Array）
		$lTblCustomerList = (array)$lTblCustomerList;

		//add blank space
		$lArrDataCustomerList += [
			"" => ""
		];

		//data exist
		if ($lTblCustomerList != null)
		{
			//store result in Array again
			foreach ($lTblCustomerList as $lRowCustomerList) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCustomerList = (Array)$lRowCustomerList;

				$lArrDataCustomerList += [
					$lRowCustomerList["CUSTOMER_ID"] => $lRowCustomerList["CUSTOMER_NAME"]
				];
			}

		}

		return $lArrDataCustomerList;
	}



	//**************************************************************************
	// process         setCustomerNoListEntry
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************
	private function setCustomerNoListEntry($pViewData, $pMasterNo, $pProcessId, $pViewDataKey)
	{
		$lArrCustomerNoList     = ["" => ""];  //view data to return

		//search
		$lArrCustomerNoList = $this->getCustomerNoListEntry($pMasterNo, $pProcessId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCustomerNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getCustomerNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************
	private function getCustomerNoListEntry($pMasterNo, $pProcessId)
	{
		$lTblCustomerNo			= []; //result of getting infomation of Inspection No.
		$lRowCustomerNo			= []; //work when it change to array
		$lArrCustomerNo			= []; //List of Inspection No. to return
		$lRowCountCustomerNo		= 0;  //

		$lTblCustomerNo = DB::select('
		      SELECT CUST.CUSTOMER_ID
		            ,CUST.CUSTOMER_NAME
		        FROM TISHEETM AS SHET
	 LEFT OUTER JOIN TCUSTOMM CUST
		          ON CUST.CUSTOMER_ID = SHET.CUSTOMER_ID
		        
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		    GROUP BY SHET.CUSTOMER_ID
		    ORDER BY SHET.REV_NO DESC
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCustomerNo = (array)$lTblCustomerNo;

		//data exist
		if ($lTblCustomerNo != null)
		{
			//store result in Array again
			foreach ($lTblCustomerNo as $lRowCustomerNo) {
				$lRowCountCustomerNo = $lRowCountCustomerNo + 1;
			}

			if ($lRowCountCustomerNo != 1)
			{
				//add blank space
				$lArrCustomerNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblCustomerNo as $lRowCustomerNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCustomerNo = (Array)$lRowCustomerNo;

				$lArrCustomerNo += [
					$lRowCustomerNo["CUSTOMER_ID"] => $lRowCustomerNo["CUSTOMER_NAME"]
				];
			}
		}
		else
		{
			//add blank space
			$lArrCustomerNo += [
				"" => ""
			];
		}

		return $lArrCustomerNo;
	}


	//**************************************************************************
	// process         getCustomerID
	// overview        
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         ■■■■Add Hoshina 04-Aug-2017 KOHBYO
	//**************************************************************************
	private function getCustomerID($pMasterNo, $pProcessId)
	{
		$lTblCustomerID			= [];
		
		$lTblCustomerID = DB::select('
		      SELECT SHET.CUSTOMER_ID
		        FROM TISHEETM AS SHET
		        
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		    GROUP BY SHET.CUSTOMER_ID
		    ORDER BY SHET.REV_NO DESC
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
				]
		);
		
		return $lTblCustomerID;
	}


	//**************************************************************************
	// process         setCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCheckSheetNoListEntry($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoListEntry($pMasterNo, $pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getCheckSheetNoListEntry
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetNoListEntry($pMasterNo, $pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return
		$lRowCountCheckSheetNo		= 0;  //

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				$lRowCountCheckSheetNo = $lRowCountCheckSheetNo + 1;
			}

			if ($lRowCountCheckSheetNo != 1)
			{
				//add blank space
				$lArrCheckSheetNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}

		}
		else
		{
			//add blank space
			$lArrCheckSheetNo += [
				"" => ""
			];
		}

		return $lArrCheckSheetNo;
	}

	//**************************************************************************
	// process         setCheckSheetNoListSearch
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setCheckSheetNoListSearch($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pViewDataKey)
	{
		$lArrCheckSheetNoList     = ["" => ""];  //view data to return

		//search
		$lArrCheckSheetNoList = $this->getCheckSheetNoListSearch($pMasterNo, $pProcessId, $pCustomerId);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrCheckSheetNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getCheckSheetNoListSearch
	// overview        get data for Inspection No.combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getCheckSheetNoListSearch($pMasterNo, $pProcessId, $pCustomerId)
	{
		$lTblCheckSheetNo			= []; //result of getting infomation of Inspection No.
		$lRowCheckSheetNo			= []; //work when it change to array
		$lArrCheckSheetNo			= []; //List of Inspection No. to return

		$lTblCheckSheetNo = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblCheckSheetNo = (array)$lTblCheckSheetNo;

		//add blank space
		$lArrCheckSheetNo += [
			"" => ""
		];

		//data exist
		if ($lTblCheckSheetNo != null)
		{
			//store result in Array again
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowCheckSheetNo = (Array)$lRowCheckSheetNo;

				$lArrCheckSheetNo += [
					$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
				];
			}
		}

		return $lArrCheckSheetNo;
	}

	//**************************************************************************
	// process         setMaterialNameList
	// overview        get data for Inspection No.combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setMaterialNameList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialNameList     = ["" => ""];  //view data to return
       
		//search
		$lArrMaterialNameList = $this->getMaterialNameList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo);
       
		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialNameList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getMaterialNameList
	// overview        get data for Material Name combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMaterialNameList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialName			= []; //result of getting infomation of Material Name
		$lRowMaterialName			= []; //work when it change to array
		$lArrMaterialName			= []; //List of Material Name to return
		$lRowCountMaterialName		= 0;  //

		$lTblMaterialName = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG          = "0"
		         AND SHET.MASTER_NO           = :MasterNo
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)   /* value of input in screen　無ければ条件にしない */
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"       => $pMasterNo,
					"ProcessId"      => $pProcessId,
					"CustomerId"     => $pCustomerId,
					"CheckSheetNo1"  => $pCheckSheetNo,
					"CheckSheetNo2"  => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialName = (array)$lTblMaterialName;
      
		//data exist
		if ($lTblMaterialName != null)
		{
			//store result in Array again
			foreach ($lTblMaterialName as $lRowMaterialName) {
				$lRowCountMaterialName = $lRowCountMaterialName + 1;
			}

			if ($lRowCountMaterialName != 1)
			{
				//add blank space
				$lArrMaterialName += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialName as $lRowMaterialName) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowMaterialName = (Array)$lRowMaterialName;

				$lArrMaterialName += [
					$lRowMaterialName["MATERIAL_NAME"] => $lRowMaterialName["MATERIAL_NAME"]
				];
			}

		}
		else
		{
			//add blank space
			$lArrMaterialName += [
				"" => ""
			];
		}

		return $lArrMaterialName;
	}

	//**************************************************************************
	// process         setMaterialSizeList
	// overview        get data for  Material Size combo box and set Array to display screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setMaterialSizeList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo, $pViewDataKey)
	{
		$lArrMaterialSizeList     = ["" => ""];  //view data to return

		//search
		$lArrMaterialSizeList = $this->getMaterialSizeList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo);

		//add to array for transportion to screen(written in +=))
		$pViewData += [
			$pViewDataKey => $lArrMaterialSizeList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getMaterialSizeList
	// overview        get data for Material Size combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getMaterialSizeList($pMasterNo, $pProcessId, $pCustomerId, $pCheckSheetNo)
	{
		$lTblMaterialSize			= []; //result of getting infomation of Material Size
		$lRowMaterialSize			= []; //work when it change to array
		$lArrMaterialSize			= []; //List of Material Size to return
		$lRowCountMaterialSize		= 0;  //

		$lTblMaterialSize = DB::select('
		      SELECT SHET.INSPECTION_SHEET_NO
		            ,SHET.INSPECTION_SHEET_NAME AS INSPECTION_SHEET_NAME
		            ,SHET.MATERIAL_NAME AS MATERIAL_NAME
		            ,SHET.MATERIAL_SIZE AS MATERIAL_SIZE
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG          = "0"
		         AND SHET.MASTER_NO           = :MasterNo
		         AND SHET.PROCESS_ID          = :ProcessId
		         AND SHET.CUSTOMER_ID         = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = IF(:CheckSheetNo1 <> "", :CheckSheetNo2, SHET.INSPECTION_SHEET_NO)   /* value of input in screen　無ければ条件にしない */
		    GROUP BY SHET.INSPECTION_SHEET_NO
		    ORDER BY MAX(SHET.DISPLAY_ORDER)
		',
				[
					"MasterNo"         => $pMasterNo,
					"ProcessId"        => $pProcessId,
					"CustomerId"       => $pCustomerId,
					"CheckSheetNo1"    => $pCheckSheetNo,
					"CheckSheetNo2"    => $pCheckSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblMaterialSize = (array)$lTblMaterialSize;

		//data exist
		if ($lTblMaterialSize != null)
		{
			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize) {
				$lRowCountMaterialSize = $lRowCountMaterialSize + 1;
			}

			if ($lRowCountMaterialSize != 1)
			{
				//add blank space
				$lArrMaterialSize += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblMaterialSize as $lRowMaterialSize) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowMaterialSize = (Array)$lRowMaterialSize;

				$lArrMaterialSize += [
					$lRowMaterialSize["MATERIAL_SIZE"] => $lRowMaterialSize["MATERIAL_SIZE"]
				];
			}

		}
		else
		{
			//add blank space
			$lArrMaterialSize += [
				"" => ""
			];
		}

		return $lArrMaterialSize;
	}

	//**************************************************************************
	// process         setRevNoList
	// overview        get data for revision No. and set to Array for displaying screen
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function setRevNoList($pViewData, $pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lArrRevNoList     = ["" => ""];  //view data to return

		//search
		$lArrRevNoList = $this->getRevNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo);

		//add to array for transportion to screen(written in +=)
		$pViewData += [
			"arrRevNoForSearchList" => $lArrRevNoList
		];

		return $pViewData;
	}

	//**************************************************************************
	// process         getRevNoList
	// overview        get data for revision No. combo box with SQL
	// argument        
	// return value    
	// date            Ver.01 
	// remarks         
	//**************************************************************************
	private function getRevNoList($pMasterNo, $pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lTblRevNo			= []; //result of getting infomstion of revision No.
		$lRowRevNo			= []; //work when it change to array
		$lArrRevNo			= []; //List of revision No. to return
		$lRowCountRevNo		= 0;  //

		$lTblRevNo = DB::select('
		      SELECT SHET.REV_NO
		            ,SHET.REV_NO AS REV_NO_NAME
		        FROM TISHEETM AS SHET
		       WHERE SHET.DELETE_FLG  = "0"
		         AND SHET.MASTER_NO   = :MasterNo
		         AND SHET.PROCESS_ID  = :ProcessId
		         AND SHET.CUSTOMER_ID = :CustomerId
		         AND SHET.INSPECTION_SHEET_NO = :InspectionSheetNo
		    ORDER BY SHET.DISPLAY_ORDER
		',
				[
					"MasterNo"   => $pMasterNo,
					"ProcessId"	 => $pProcessId,
					"CustomerId" => $pCustomerId,
					"InspectionSheetNo" => $pInspectionSheetNo,
				]
		);

		//cast search result to array（two dimensions Array）
		$lTblRevNo = (array)$lTblRevNo;

		//data exist
		if ($lTblRevNo != null)
		{
			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				$lRowCountRevNo = $lRowCountRevNo + 1;
			}

			if ($lRowCountRevNo != 1)
			{
				//add blank space
				$lArrRevNo += [
					"" => ""
				];
			}

			//store result in Array again
			foreach ($lTblRevNo as $lRowRevNo) {
				//Cast previously　because it has erroe to read value of array while CASTing
				$lRowRevNo = (Array)$lRowRevNo;

				$lArrRevNo += [
					$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
				];
			}

		}
		else
		{
			//add blank space
			$lArrRevNo += [
				"" => ""
			];
		}

		return $lArrRevNo;
	}






}
