<?php

namespace App\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class NGResultExcel
{
	CONST CONVDATA_ARG2 = "2";
	CONST NGRESULTEXCEL_HEADER_STARTROW = 3; //line number for header
	CONST NGRESULTEXCEL_MEISAI_STARTROW = 7; //start line number for detail


	public function getNGResultExcel($pExcelNGResultArrayData)
	{
		$lTemplateFileNameNGResult   = "";
		$lOutputExcelData = 0;

		//NG Result
		$lWorkINSPECTIONSHEET        = "";
		$lWorkMACHINENO              = "";
		$lWorkMOLDNO                 = "";
		$lWorkINSPECTIONPOINT        = "";
		$lWorkANAGRP01               = "";
		$lWorkANAGRP02               = "";
		$NGFLG                       = 0;


		$lTblResultDataNGResult = $this->getInspectionResultDataForNGResult($pExcelNGResultArrayData);

		if (count($lTblResultDataNGResult) == 0)
		{
			$lOutputExcelData = null;
		}
		else
		{
			$spreadsheet = new Spreadsheet();
			$lTemplateFileNameNGResult = "InspectionResultNGDataV1_template.xlsx";
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameNGResult);

			$sheet = $spreadsheet->getActiveSheet();

			//----------------------------
			//set information in header
			//----------------------------
			$lExcelRowCount = self::NGRESULTEXCEL_HEADER_STARTROW;

			if($pExcelNGResultArrayData["ProcessId"] != 0)
			{
				$lTblResultDataHeadProcessName = $this->getProcessName($pExcelNGResultArrayData);

				if ($lTblResultDataHeadProcessName != null)
				{
					$ArrResultDataHeadProcessName = [];
					$ArrResultDataHeadProcessName = (Array)$lTblResultDataHeadProcessName[0];
					$sheet->getCell('A3')->setValue($ArrResultDataHeadProcessName["CODE_NAME"]);
				}
			}

			if($pExcelNGResultArrayData["CustomerId"] != 0)
			{
				$lTblResultDataHeadCustomerName = $this->getCustmerName($pExcelNGResultArrayData);
				if ($lTblResultDataHeadCustomerName != null)
				{
					$ArrResultDataHeadCustmerName = [];
					$ArrResultDataHeadCustmerName = (Array)$lTblResultDataHeadCustomerName[0];
					$sheet->getCell('B3')->setValue($ArrResultDataHeadCustmerName["CUSTOMER_NAME"]);
				}
			}

			if($pExcelNGResultArrayData["CheckSheetNo"] != 0)
			{
				$sheet->getCell('C3')->setValue($pExcelNGResultArrayData["CheckSheetNo"]);
			}

			if($pExcelNGResultArrayData["CheckSheetNo"] != 0 && $pExcelNGResultArrayData["RevisionNo"] != 0)
			{
				$lTblResultDataHeadCheckSheetName = $this->getCheckSheetName($pExcelNGResultArrayData);
				if ($lTblResultDataHeadCheckSheetName != null)
				{
					$lTblResultDataHeadCheckSheetName = [];
					$ArrResultDataHeadCheckSheetName = (Array)$lTblResultDataHeadCheckSheetName[0];
					$sheet->getCell('D3')->setValue($ArrResultDataHeadCheckSheetName["INSPECTION_SHEET_NAME"]);
				}
			}

			if($pExcelNGResultArrayData["RevisionNo"] != 0)
			{
				$sheet->getCell('E3')->setValue($pExcelNGResultArrayData["RevisionNo"]);
			}

			// if(Input::get('cmbMaterialNameForSearch') != 0)
			// {
			// 	$sheet->getCell('F3')->setValue(Input::get('cmbMaterialNameForSearch'));
			// }
			// if(Input::has('cmbMaterialSizeForSearch'))
			// {
			// 	$objSheet->setCellValueByColumnAndRow(6, $lExcelRowCount, Input::get('cmbMaterialSizeForSearch'));
			// }
			if($pExcelNGResultArrayData["MachineNo"] != "")
			{
				$sheet->setCellValueByColumnAndRow(7, $lExcelRowCount, $pExcelNGResultArrayData["MachineNo"]);
			}
			if($pExcelNGResultArrayData["MoldNo"] != 0)
			{
				$sheet->getCell('G3')->setValue($pExcelNGResultArrayData["MoldNo"]);
			}
			if($pExcelNGResultArrayData["InspectorCode"] != 0)
			{
				$lTblResultDataHeadUserName = $this->getInspectorName($pExcelNGResultArrayData);
				if ($lTblResultDataHeadUserName != null)
				{
					$ArrResultDataHeadUserName = [];
					$ArrResultDataHeadUserName = (Array)$lTblResultDataHeadUserName[0];
					$sheet->getCell('H3')->setValue($ArrResultDataHeadUserName["USER_NAME"]);
				}
			}
			if($pExcelNGResultArrayData["ProductionDateFrom"] != 0)
			{
				$sheet->getCell('I3')->setValue($this->convertDateFormat($pExcelNGResultArrayData["ProductionDateFrom"], self::CONVDATA_ARG2));
			}
			if($pExcelNGResultArrayData["ProductionDateTo"] != 0)
			{
				$sheet->getCell('J3')->setValue($this->convertDateFormat($pExcelNGResultArrayData["ProductionDateTo"], self::CONVDATA_ARG2));
			}
			if($pExcelNGResultArrayData["InspectionDateFrom"] != 0)
			{
				$sheet->getCell('K3')->setValue($this->convertDateFormat($pExcelNGResultArrayData["InspectionDateFrom"], self::CONVDATA_ARG2));
			}
			if($pExcelNGResultArrayData["InspectionDateTo"] != 0)
			{
				$sheet->getCell('L3')->setValue($this->convertDateFormat($pExcelNGResultArrayData["InspectionDateTo"], self::CONVDATA_ARG2));
			}
			$sheet->getCell('P3')->setValue(date("d-m-Y"));
			$sheet->getCell('R3')->setValue(date("H:i:s"));


			//----------------------------
			//set infomation on detail
			//----------------------------
			$lExcelRowCount = self::NGRESULTEXCEL_MEISAI_STARTROW - 1;

			//set down data while loop result
			foreach ($lTblResultDataNGResult As $lResultRow)
			{
				$lTblDataNGResultRow = (Array)$lResultRow;

				if(($lWorkINSPECTIONSHEET == $lTblDataNGResultRow["INSPECTION_SHEET_NO"]) and ($lWorkMACHINENO == $lTblDataNGResultRow["MACHINE_NO"]) and ($lWorkMOLDNO == $lTblDataNGResultRow["MOLD_NO"]) and ($lWorkINSPECTIONPOINT == $lTblDataNGResultRow["INSPECTION_POINT"]) and ($lWorkANAGRP01 == $lTblDataNGResultRow["ANALYTICAL_GRP_01"]) and ($lWorkANAGRP02 == $lTblDataNGResultRow["ANALYTICAL_GRP_02"]))
				{
					//Nothing
				}
				else
				{
					$NGFLG = 0;
				}

				//leave Inspection No. of result data
				$lWorkINSPECTIONSHEET = $lTblDataNGResultRow["INSPECTION_SHEET_NO"];
				$lWorkMACHINENO       = $lTblDataNGResultRow["MACHINE_NO"];
				$lWorkMOLDNO          = $lTblDataNGResultRow["MOLD_NO"];
				$lWorkINSPECTIONPOINT = $lTblDataNGResultRow["INSPECTION_POINT"];
				$lWorkANAGRP01        = $lTblDataNGResultRow["ANALYTICAL_GRP_01"];
				$lWorkANAGRP02        = $lTblDataNGResultRow["ANALYTICAL_GRP_02"];

				//OK data
				if ($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"] == "1")
				{
					if ($NGFLG == 0)
					{
						//not output
						//flag leave as it is
					}
					//when flag is 1
					else
					{
						//add 1 more line at 8 line and write down data.last 2 lines are blank. 
						$sheet->insertNewRowBefore($lExcelRowCount + 2, 1);
						//add line No. to putout
						$lExcelRowCount = $lExcelRowCount + 1;

						$arrayData = [
							[
								$lTblDataNGResultRow["PROCESS_NAME"],
								$lTblDataNGResultRow["CUSTOMER_NAME"],
								$lTblDataNGResultRow["ITEM_NO"],
								$lTblDataNGResultRow["ITEM_NAME"],
								$lTblDataNGResultRow["INSPECTION_SHEET_NO"],
								$lTblDataNGResultRow["REV_NO"],
								$lTblDataNGResultRow["MACHINE_NO"],
								$lTblDataNGResultRow["MACHINE_NAME"],
								$lTblDataNGResultRow["MOLD_NO"],
								$lTblDataNGResultRow["INSPECTION_NO"],
								$lTblDataNGResultRow["INSPECTION_POINT"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_01"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_02"],
								$lTblDataNGResultRow["SAMPLE_NO"],
								$lTblDataNGResultRow["PRODUCTION_YMD"],
								$lTblDataNGResultRow["USER_NAME"],
								$lTblDataNGResultRow["INSPECTION_YMD"],
								$lTblDataNGResultRow["INSPECTION_TIME_NAME"],
								$lTblDataNGResultRow["CONDITION_CD"],
								$lTblDataNGResultRow["INSPECTION_ITEM"],
								$lTblDataNGResultRow["REFERENCE_VALUE"],
								$lTblDataNGResultRow["THRESHOLD_NG_UNDER"],
								$lTblDataNGResultRow["THRESHOLD_NG_OVER"],
								$lTblDataNGResultRow["INSPECTION_RESULT_VALUE"],
								$lTblDataNGResultRow["INSPECTION_RESULT_TYPE_NAME"],
								$lTblDataNGResultRow["UPDATE_YMDHMS"],
								$lTblDataNGResultRow["OFFSET_NG_BUNRUI_NAME"],
								$lTblDataNGResultRow["OFFSET_NG_REASON"],
								$lTblDataNGResultRow["OFFSET_NG_ACTION"],
								$lTblDataNGResultRow["ACTION_DETAILS_NAME"],
								$lTblDataNGResultRow["TR_NO"],
								$lTblDataNGResultRow["TECHNICIAN_USER_NAME"],
								$lTblDataNGResultRow["TSR_NO"],
							]
						];

						$sheet->fromArray($arrayData, null, "A".$lExcelRowCount);

						//flag is 0
						$NGFLG = 0;
					}
				}
				//inspection reult is Line Stop
				else if($lTblDataNGResultRow["INSPECTION_RESULT_TYPE"] == "5")
				{
					//in case TSR No. is entried
					if ($lTblDataNGResultRow["TSR_NO"] != null)
					{
						//flag = 1
						$NGFLG = 1;
						//add 1 more line at 8 line and write down data.last 2 lines are blank.
						$sheet->insertNewRowBefore($lExcelRowCount + 2, 1);
						//add line No. to putout
						$lExcelRowCount = $lExcelRowCount + 1;

						$arrayData = [
							[
								$lTblDataNGResultRow["PROCESS_NAME"],
								$lTblDataNGResultRow["CUSTOMER_NAME"],
								$lTblDataNGResultRow["ITEM_NO"],
								$lTblDataNGResultRow["ITEM_NAME"],
								$lTblDataNGResultRow["INSPECTION_SHEET_NO"],
								$lTblDataNGResultRow["REV_NO"],
								$lTblDataNGResultRow["MACHINE_NO"],
								$lTblDataNGResultRow["MACHINE_NAME"],
								$lTblDataNGResultRow["MOLD_NO"],
								$lTblDataNGResultRow["INSPECTION_NO"],
								$lTblDataNGResultRow["INSPECTION_POINT"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_01"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_02"],
								$lTblDataNGResultRow["SAMPLE_NO"],
								$lTblDataNGResultRow["PRODUCTION_YMD"],
								$lTblDataNGResultRow["USER_NAME"],
								$lTblDataNGResultRow["INSPECTION_YMD"],
								$lTblDataNGResultRow["INSPECTION_TIME_NAME"],
								$lTblDataNGResultRow["CONDITION_CD"],
								$lTblDataNGResultRow["INSPECTION_ITEM"],
								$lTblDataNGResultRow["REFERENCE_VALUE"],
								$lTblDataNGResultRow["THRESHOLD_NG_UNDER"],
								$lTblDataNGResultRow["THRESHOLD_NG_OVER"],
								$lTblDataNGResultRow["INSPECTION_RESULT_VALUE"],
								$lTblDataNGResultRow["INSPECTION_RESULT_TYPE_NAME"],
								$lTblDataNGResultRow["UPDATE_YMDHMS"],
								$lTblDataNGResultRow["OFFSET_NG_BUNRUI_NAME"],
								$lTblDataNGResultRow["OFFSET_NG_REASON"],
								$lTblDataNGResultRow["OFFSET_NG_ACTION"],
								$lTblDataNGResultRow["ACTION_DETAILS_NAME"],
								$lTblDataNGResultRow["TR_NO"],
								$lTblDataNGResultRow["TECHNICIAN_USER_NAME"],
								$lTblDataNGResultRow["TSR_NO"],
							]
						];

						$sheet->fromArray($arrayData, null, "A".$lExcelRowCount);
					}
					//enter nothin in TSR No.
					else
					{
						//not output
						//leave flag asa it is
					}
				}
				//in case inspection result is OFFSET,NG,No Check
				else
				{
					//change flag 1
					$NGFLG = 1;
					//add 1 line at 8 line in templete（for flame）and set down data
					//add each 1 line as data should be set down. At last line in data,leave 2 blank line（default）
					$sheet->insertNewRowBefore($lExcelRowCount + 2, 1);
					//increase number of line corresponding to outputting
					$lExcelRowCount = $lExcelRowCount + 1;
					//set down result
						$arrayData = [
							[
								$lTblDataNGResultRow["PROCESS_NAME"],
								$lTblDataNGResultRow["CUSTOMER_NAME"],
								$lTblDataNGResultRow["ITEM_NO"],
								$lTblDataNGResultRow["ITEM_NAME"],
								$lTblDataNGResultRow["INSPECTION_SHEET_NO"],
								$lTblDataNGResultRow["REV_NO"],
								$lTblDataNGResultRow["MACHINE_NO"],
								$lTblDataNGResultRow["MACHINE_NAME"],
								$lTblDataNGResultRow["MOLD_NO"],
								$lTblDataNGResultRow["INSPECTION_NO"],
								$lTblDataNGResultRow["INSPECTION_POINT"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_01"],
								$lTblDataNGResultRow["ANALYTICAL_GRP_02"],
								$lTblDataNGResultRow["SAMPLE_NO"],
								$lTblDataNGResultRow["PRODUCTION_YMD"],
								$lTblDataNGResultRow["USER_NAME"],
								$lTblDataNGResultRow["INSPECTION_YMD"],
								$lTblDataNGResultRow["INSPECTION_TIME_NAME"],
								$lTblDataNGResultRow["CONDITION_CD"],
								$lTblDataNGResultRow["INSPECTION_ITEM"],
								$lTblDataNGResultRow["REFERENCE_VALUE"],
								$lTblDataNGResultRow["THRESHOLD_NG_UNDER"],
								$lTblDataNGResultRow["THRESHOLD_NG_OVER"],
								$lTblDataNGResultRow["INSPECTION_RESULT_VALUE"],
								$lTblDataNGResultRow["INSPECTION_RESULT_TYPE_NAME"],
								$lTblDataNGResultRow["UPDATE_YMDHMS"],
								$lTblDataNGResultRow["OFFSET_NG_BUNRUI_NAME"],
								$lTblDataNGResultRow["OFFSET_NG_REASON"],
								$lTblDataNGResultRow["OFFSET_NG_ACTION"],
								$lTblDataNGResultRow["ACTION_DETAILS_NAME"],
								$lTblDataNGResultRow["TR_NO"],
								$lTblDataNGResultRow["TECHNICIAN_USER_NAME"],
								$lTblDataNGResultRow["TSR_NO"],
							]
						];

						$sheet->fromArray($arrayData, null, "A".$lExcelRowCount);
				}

				//leave Inspection No. of result data
				$lWorkINSPECTIONSHEET = $lTblDataNGResultRow["INSPECTION_SHEET_NO"];
				$lWorkMACHINENO       = $lTblDataNGResultRow["MACHINE_NO"];
				$lWorkMOLDNO          = $lTblDataNGResultRow["MOLD_NO"];
				$lWorkINSPECTIONPOINT = $lTblDataNGResultRow["INSPECTION_POINT"];
				$lWorkANAGRP01        = $lTblDataNGResultRow["ANALYTICAL_GRP_01"];
				$lWorkANAGRP02        = $lTblDataNGResultRow["ANALYTICAL_GRP_02"];
			}

			//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
			$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameNGResult);
			$lSaveFilePathAndName = "excel/".$lSaveFileName;

			//save file（save in server temporarily）
			$writer = new XlsxWriter($spreadsheet);
			//if many data exist,it need very long time for SAVE.(it is available to download directly without save
			$writer->save($lSaveFilePathAndName);

			//download
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Length: '.filesize($lSaveFilePathAndName));
			header('Content-disposition: attachment; filename='.$lSaveFileName);
			readfile($lSaveFilePathAndName);

			//delete file（that was saved in server）
			unlink($lSaveFilePathAndName);
		}

		return $lOutputExcelData;
	}


	//**************************************************************************
	// process         getInspectionResultDataForNGResult
	//**************************************************************************
	private function getInspectionResultDataForNGResult($pExcelNGResultArrayData)
	{
		$lTblSearchResultData          = [];

		// $lInspectionDateFromForSearch  = "";
		// $lInspectionDateToForSearch    = "";
		// $lProductionDateFromForSearch  = "";
		// $lProductionDateToForSearch    = "";

		// $lInspectionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtInspectionDateFromForSearch'), "1");
		// $lInspectionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtInspectionDateToForSearch'), "1");
		// $lProductionDateFromForSearch  = $this->convertDateFormat((String)Input::get('txtProductionDateFromForSearch'), "1");
		// $lProductionDateToForSearch    = $this->convertDateFormat((String)Input::get('txtProductionDateToForSearch'), "1");

			$lTblSearchResultData = DB::select('
				SELECT HEAD.INSPECTION_RESULT_NO
						,SHEE.CUSTOMER_ID
						,CUSTOM.CUSTOMER_NAME
						,SHEE.ITEM_NO
						,ITEM.ITEM_NAME
						,SHEE.MODEL_NAME
						,HEAD.INSPECTION_SHEET_NO
						,SHEE.INSPECTION_SHEET_NAME
						,HEAD.REV_NO
						,HEAD.PROCESS_ID
						,PROCESS.CODE_NAME AS PROCESS_NAME
						,HEAD.MACHINE_NO
						,MACH.MACHINE_NAME
						,HEAD.MOLD_NO
						,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
						,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
						,HEAD.INSPECTION_TIME_ID
						,TIME.INSPECTION_TIME_NAME
						,HEAD.CONDITION_CD
						,COND.CODE_NAME AS CONDITION_NAME
						,HEAD.LOT_NO
						,DTIL.INSPECTION_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,IPNO.INSPECTION_ITEM
						,IPNO.REFERENCE_VALUE
						,IPNO.THRESHOLD_NG_UNDER
						,IPNO.THRESHOLD_NG_OVER
						,DTIL.INSPECTION_RESULT_VALUE
						,DTIL.LAST_UPDATE_USER_ID
						,USER.USER_NAME
						,DATE_FORMAT(DTIL.UPDATE_YMDHMS,"%T") AS UPDATE_YMDHMS
						,DTIL.INSPECTION_RESULT_TYPE
						,CASE
							WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
							ELSE ""
						END INSPECTION_RESULT_TYPE_NAME
						,NGBUN.OFFSET_NG_BUNRUI_NAME
						,NGRES.OFFSET_NG_REASON
						-- ,DTIL.OFFSET_NG_REASON
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
							WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
							ELSE ""
						END OFFSET_NG_ACTION
						,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
						,ACTIO.ACTION_DETAILS_NAME
						,DTIL.TR_NO
						,TEUR.TECHNICIAN_USER_NAME
						,DTIL.TSR_NO
				FROM TRESHEDT AS HEAD        

			INNER JOIN TRESDETT AS DTIL
					ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
			INNER JOIN TISHEETM AS SHEE
					ON HEAD.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
				   AND HEAD.REV_NO               = SHEE.REV_NO
			INNER JOIN TINSPNOM AS IPNO
					ON SHEE.INSPECTION_SHEET_NO  = IPNO.INSPECTION_SHEET_NO
				   AND SHEE.REV_NO               = IPNO.REV_NO
				   AND DTIL.INSPECTION_POINT     = IPNO.INSPECTION_POINT
	   LEFT OUTER JOIN TITEMMST AS ITEM
					ON SHEE.ITEM_NO             = ITEM.ITEM_NO
				   AND ITEM.DELETE_FLG          = "0"

			INNER JOIN TMACHINM AS MACH
					ON HEAD.MACHINE_NO           = MACH.MACHINE_NO
			INNER JOIN TUSERMST AS USER
					ON DTIL.LAST_UPDATE_USER_ID  = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
					ON HEAD.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
	   LEFT OUTER JOIN TTECUSRT AS TEUR
					ON DTIL.TECHNICIAN_USER_ID   = TEUR.TECHNICIAN_USER_ID
	   LEFT OUTER JOIN TCUSTOMM AS CUSTOM
					ON SHEE.CUSTOMER_ID          = CUSTOM.CUSTOMER_ID
	   LEFT OUTER JOIN tngrebnm AS NGBUN
					ON NGBUN.OFFSET_NG_BUNRUI_ID = DTIL.OFFSET_NG_BUNRUI_ID
	   LEFT OUTER JOIN tngresnm AS NGRES
					ON NGRES.OFFSET_NG_REASON_ID = DTIL.OFFSET_NG_REASON_ID
				   AND NGRES.OFFSET_NG_BUNRUI_ID = DTIL.OFFSET_NG_BUNRUI_ID
	   LEFT OUTER JOIN tactionm AS ACTIO
					ON ACTIO.ACTION_DETAILS_ID   = DTIL.ACTION_DETAILS_ID

			INNER JOIN TCODEMST AS COND
					ON COND.CODE_CLASS           = "001"
				   AND COND.CODE_ORDER           = HEAD.CONDITION_CD
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS        = "002"
				   AND PROCESS.CODE_ORDER        = HEAD.PROCESS_ID

				WHERE HEAD.PROCESS_ID               = IF(:ProcessId1 <> "", :ProcessId2, HEAD.PROCESS_ID)
					AND SHEE.CUSTOMER_ID            = IF(:CustomerID1 <> "", :CustomerID2, SHEE.CUSTOMER_ID)
					AND HEAD.INSPECTION_SHEET_NO    LIKE :CheckSheetNo
					AND HEAD.REV_NO                 = IF(:RevisionNo1 <> "", :RevisionNo2, HEAD.REV_NO)
					AND HEAD.MATERIAL_NAME          = IF(:MaterialName1 <> "", :MaterialName2, HEAD.MATERIAL_NAME)
					AND HEAD.MATERIAL_SIZE          = IF(:MaterialSize1 <> "", :MaterialSize2, HEAD.MATERIAL_SIZE)
					AND HEAD.MACHINE_NO             = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
					AND HEAD.MOLD_NO                = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
					AND HEAD.LAST_UPDATE_USER_ID    = IF(:UserID1 <> "", :UserID2, HEAD.LAST_UPDATE_USER_ID)
					AND SHEE.ITEM_NO                LIKE :PartNo
					AND HEAD.PRODUCTION_YMD         >= IF(:ProdDateFrom1 <> "", :ProdDateFrom2, HEAD.PRODUCTION_YMD)
					AND HEAD.PRODUCTION_YMD         <= IF(:ProdDateTo1 <> "", :ProdDateTo2, HEAD.PRODUCTION_YMD)
					AND HEAD.INSPECTION_YMD         >= :InspDateFrom AND HEAD.INSPECTION_YMD <= :InspDateTo
					AND HEAD.INSPECTION_TIME_ID     = IF(:InspectionTime1 <> "", :InspectionTime2, HEAD.INSPECTION_TIME_ID)
					AND DTIL.INSPECTION_RESULT_TYPE IN ("1","2","3","4","5")
			   ORDER BY SHEE.CUSTOMER_ID
						,SHEE.ITEM_NO
						,SHEE.MODEL_NAME
						,HEAD.INSPECTION_SHEET_NO
						,HEAD.REV_NO
						,HEAD.MACHINE_NO
						,HEAD.MOLD_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,DTIL.DISPLAY_ORDER
						,HEAD.PRODUCTION_YMD
						,HEAD.INSPECTION_YMD
						,HEAD.INSPECTION_TIME_ID
						-- ,TIME.DISPLAY_ORDER
						,HEAD.CONDITION_CD
			',
				[
					"ProcessId1"        => $pExcelNGResultArrayData["ProcessId"],
					"ProcessId2"        => $pExcelNGResultArrayData["ProcessId"],
					"CustomerID1"       => $pExcelNGResultArrayData["CustomerId"],
					"CustomerID2"       => $pExcelNGResultArrayData["CustomerId"],
					"CheckSheetNo"      => $pExcelNGResultArrayData["CheckSheetNo"],
					"RevisionNo1"       => $pExcelNGResultArrayData["RevisionNo"],
					"RevisionNo2"       => $pExcelNGResultArrayData["RevisionNo"],
					"MaterialName1"     => "",
					"MaterialName2"     => "",
					"MaterialSize1"     => "",
					"MaterialSize2"     => "",
					"MachineNo1"        => $pExcelNGResultArrayData["MachineNo"],
					"MachineNo2"        => $pExcelNGResultArrayData["MachineNo"],
					"MoldNo1"           => $pExcelNGResultArrayData["MoldNo"],
					"MoldNo2"           => $pExcelNGResultArrayData["MoldNo"],
					"UserID1"           => $pExcelNGResultArrayData["InspectorCode"],
					"UserID2"           => $pExcelNGResultArrayData["InspectorCode"],
					"PartNo"            => $pExcelNGResultArrayData["PartNo"],
					"ProdDateFrom1"     => $pExcelNGResultArrayData["ProductionDateFrom"],
					"ProdDateFrom2"     => $pExcelNGResultArrayData["ProductionDateFrom"],
					"ProdDateTo1"       => $pExcelNGResultArrayData["ProductionDateTo"],
					"ProdDateTo2"       => $pExcelNGResultArrayData["ProductionDateTo"],
					"InspDateFrom"      => $pExcelNGResultArrayData["InspectionDateFrom"],
					"InspDateTo"        => $pExcelNGResultArrayData["InspectionDateTo"],
					"InspectionTime1"   => $pExcelNGResultArrayData["InspectionTimeId"],
					"InspectionTime2"   => $pExcelNGResultArrayData["InspectionTimeId"],
				]
			);          
			       
		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getProcessName
	//**************************************************************************
	private function getProcessName($pExcelNGResultArrayData)
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
				SELECT  PROCESS.CODE_NAME
				FROM	TCODEMST AS PROCESS
				WHERE	PROCESS.CODE_CLASS = "002"
				AND 	PROCESS.CODE_ORDER = IF(:ProcessID1 <> "", :ProcessID2, PROCESS.CODE_ORDER)
			',
				[
					"ProcessID1"  =>  $pExcelNGResultArrayData["ProcessId"],
					"ProcessID2"  =>  $pExcelNGResultArrayData["ProcessId"],
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCustmerName
	//**************************************************************************
	private function getCustmerName($pExcelNGResultArrayData)
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
				SELECT  CUSTOM.CUSTOMER_NAME
				FROM	TCUSTOMM AS CUSTOM
				WHERE	CUSTOM.CUSTOMER_ID = IF(:CustomerID1 <> "", :CustomerID2, CUSTOM.CUSTOMER_ID)
			',
				[
					"CustomerID1"  =>  $pExcelNGResultArrayData["CustomerId"],
					"CustomerID2"  =>  $pExcelNGResultArrayData["CustomerId"],
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getMachineName
	//**************************************************************************
	private function getMachineName($pExcelNGResultArrayData)
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
			    SELECT  MACHINE.MACHINE_NO
					   ,MACHINE.MACHINE_NAME
				   FROM TMACHINM AS MACHINE
				  WHERE MACHINE.MACHINE_NO = IF(:MachineNo1 <> "", :MachineNo2, MACHINE.MACHINE_NO)    /*無ければ条件にしない*/
			',
				[
					"MachineNo1"      => $pExcelNGResultArrayData["MachineNo"],
					"MachineNo2"      => $pExcelNGResultArrayData["MachineNo"],
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getCheckSheetName
	//**************************************************************************
	private function getCheckSheetName($pExcelNGResultArrayData)
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
					SELECT SHEET.INSPECTION_SHEET_NAME
						  ,SHEET.MODEL_NAME
					  FROM TISHEETM AS SHEET
					 WHERE SHEET.INSPECTION_SHEET_NO LIKE :CheckSheetNo
					   AND SHEET.REV_NO = IF(:RevisionNo1 <> "", :RevisionNo2, SHEET.REV_NO)
			',
				[
					"CheckSheetNo"  => $pExcelNGResultArrayData["CheckSheetNo"]."%",
					"RevisionNo1"   => $pExcelNGResultArrayData["RevisionNo"],
					"RevisionNo2"   => $pExcelNGResultArrayData["RevisionNo"],
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         getInspectorName
	//**************************************************************************
	private function getInspectorName($pExcelNGResultArrayData)
	{
		$lTblSearchResultData     = [];

			$lTblSearchResultData = DB::select('
			    SELECT USERM.USER_NAME
				  FROM TUSERMST AS USERM
				 WHERE USERM.USER_ID = IF(:UserID1 <> "", :UserID2, USERM.USER_ID)
			',
				[
					"UserID1"         => $pExcelNGResultArrayData["InspectorCode"],
					"UserID2"         => $pExcelNGResultArrayData["InspectorCode"],
				]
			);

		return $lTblSearchResultData;
	}

	//**************************************************************************
	// process         convertDateFormat
	// overview        change data type
	// argument        date(English type dd-mm-yyyy or Japanese type yyyy/mm/dd)
	//                 pattern(1:English->Japanese 2:Japanese->English)
	// return value    
	// author          s-miyamoto
	// date            Ver.01  2014.08.26 v1.00 FIX
	// remarks         
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}






}
