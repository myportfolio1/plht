<?php

namespace App\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class GraphExcel
{
	CONST GRAPHEXCEL_HEADER_STARTROW    = 2;  //line number for header(行)

	public function getGraphChartExcel($pImagePath, $pArray1, $pArray2)
	{
		Log::write('info', 'Start Graph sheet');

		$lTemplateFileNameGraphSheet = "";
		$lOutputExcelData = 0;

		if (is_null($pImagePath) and is_null($pArray1) and is_null($pArray2))
		{
			$spreadsheet = new Spreadsheet();
			$lTemplateFileNameGraphSheet = "GraphSheet_template.xlsx";
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameGraphSheet);

			$sheet = $spreadsheet->getActiveSheet();


			//----------------------------
			//set header
			//----------------------------
			$lRowCount = self::GRAPHEXCEL_HEADER_STARTROW;

			$today = date("Y/m/d");
			$sheet->getCell("Y".$lRowCount)->setValue($today);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["CustomerName"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["ProductNo"]);
			$sheet->getCell("J".$lRowCount)->setValue($pArray1["ProductName"]);
			$sheet->getCell("Q".$lRowCount)->setValue($pArray1["RevisionNo"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["InspectionDateFrom"]);
			$sheet->getCell("G".$lRowCount)->setValue($pArray1["InspectionDateTo"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["MoldNo"]);
			$sheet->getCell("G".$lRowCount)->setValue($pArray1["MachineNo"]);
			$sheet->getCell("K".$lRowCount)->setValue($pArray1["InspectionPoint"]);
			$sheet->getCell("O".$lRowCount)->setValue($pArray1["CavityNo"]);
			$sheet->getCell("S".$lRowCount)->setValue($pArray1["CavityWhere"]);
			$sheet->getCell("W".$lRowCount)->setValue($pArray1["Qty"]);


			$lRowCount = $lRowCount + 2;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["ItemName"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["StandardSpec"]);
			$sheet->getCell("G".$lRowCount)->setValue($pArray2["Average"]);
			$sheet->getCell("K".$lRowCount)->setValue($pArray2["SD"]);
			$sheet->getCell("O".$lRowCount)->setValue($pArray2["XaveUCL"]);
			$sheet->getCell("S".$lRowCount)->setValue($pArray2["XaveSU"]);
			$sheet->getCell("W".$lRowCount)->setValue($pArray2["RchartRU"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["UpperSpec"]);
			$sheet->getCell("G".$lRowCount)->setValue($pArray2["Maximum"]);
			$sheet->getCell("K".$lRowCount)->setValue($pArray2["Cp"]);
			$sheet->getCell("O".$lRowCount)->setValue($pArray2["XaveLCL"]);
			$sheet->getCell("S".$lRowCount)->setValue($pArray2["XaveUCL"]);
			$sheet->getCell("W".$lRowCount)->setValue($pArray2["RchartUCL"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("C".$lRowCount)->setValue($pArray1["LowerSpec"]);
			$sheet->getCell("G".$lRowCount)->setValue($pArray2["Minimum"]);
			$sheet->getCell("K".$lRowCount)->setValue($pArray2["Cpk"]);
			$sheet->getCell("O".$lRowCount)->setValue($pArray2["RchartUCL"]);
			$sheet->getCell("S".$lRowCount)->setValue($pArray2["XaveXave"]);
			$sheet->getCell("W".$lRowCount)->setValue($pArray2["RchartRave"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("S".$lRowCount)->setValue($pArray2["XaveLCL"]);

			$lRowCount = $lRowCount + 1;
			$sheet->getCell("S".$lRowCount)->setValue($pArray2["XaveSL"]);

			//----------------------------
			//set picture
			//----------------------------
			//画像用のオプジェクト作成
			$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
			//貼り付ける画像のパスを指定
			$drawing->setPath($pImagePath);
			//画像の高さを指定
			$drawing->setHeight(150);
			//画像のプロパティを見たときに表示される情報を設定
			//画像名
			$drawing->setName('Image');
			//画像の概要
			$drawing->setDescription('Image');
			//位置
			$drawing->setCoordinates('B14');
			//横方向へ何ピクセルずらすかを指定
			$drawing->setOffsetX(20);
			//回転の角度
			//$objDrawing->setRotation(25);
			//ドロップシャドウをつけるかどうか
			//$objDrawing->getShadow()->setVisible(true);
			//Excelオブジェクトに張り込み
			$drawing->setWorksheet($sheet);

			//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
			$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameGraphSheet); 
			$lSaveFilePathAndName = "excel/".$lSaveFileName;  
			// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

			//save file（save in server temporarily）
			$writer = new XlsxWriter($spreadsheet);
			//if many data exist,it need very long time for SAVE.(it is available to download directly without save
			$writer->save($lSaveFilePathAndName);

			//download
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Length: '.filesize($lSaveFilePathAndName));
			header('Content-disposition: attachment; filename='.$lSaveFileName);
			readfile($lSaveFilePathAndName);

			//delete fime(saved in server)
			unlink($lSaveFilePathAndName);
		}
		else
		{
			$lOutputExcelData = null;
		}

		return $lOutputExcelData;
	}


}
