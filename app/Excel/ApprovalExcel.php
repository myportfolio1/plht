<?php

namespace App\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class ApprovalExcel
{
	// CONST SHONINEXCEL_HEADER_STARTROW = 4; //line number for header
	CONST SHONINEXCEL_MEISAI_STARTROW = 7; //start line number for detail


	public function getApprovalExcel($pExcelApprovalArrayData)
	{
		$lTemplateFileNameApproval     = "";
		$lOutputExcelData = 0;

		$lTblDataApproval = $this->getInspectionDataForApproval($pExcelApprovalArrayData);

		if (count($lTblDataApproval) == 0)
		{
			$lOutputExcelData = null;
		}
		else
		{
			$spreadsheet = new Spreadsheet();
			$lTemplateFileNameApproval = "InspectionResultDataforApproval_template.xlsx";
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameApproval);
			// $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(public_path()."excel/".$lTemplateFileNameApproval);
			$sheet = $spreadsheet->getActiveSheet();

			//----------------------------
			//set infomation to header
			//----------------------------
			// $lExcelRowCount = self::SHONINEXCEL_HEADER_STARTROW; //define first line of header
			//pull out master data 1 line and set value
			$lTblDataApprovalRow = (Array)$lTblDataApproval[0];

			$sheet->getCell('A4')->setValue($lTblDataApprovalRow["INSPECTION_SHEET_NO"]);
			$sheet->getCell('B4')->setValue($lTblDataApprovalRow["REV_NO"]);
			$sheet->getCell('C4')->setValue($lTblDataApprovalRow["PROCESS_NAME"]);
			$sheet->getCell('D4')->setValue($lTblDataApprovalRow["MODEL_NAME"]);
			$sheet->getCell('E4')->setValue($lTblDataApprovalRow["INSPECTION_SHEET_NAME"]);
			$sheet->getCell('F4')->setValue($lTblDataApprovalRow["MACHINE_NAME"]);
			$sheet->getCell('G4')->setValue($lTblDataApprovalRow["MOLD_NO"]);
			$sheet->getCell('H4')->setValue($lTblDataApprovalRow["INSERT_USER_ID"]);
			$sheet->getCell('I4')->setValue($lTblDataApprovalRow["USER_NAME"]);
			$sheet->getCell('J4')->setValue($lTblDataApprovalRow["INSPECTION_YMD"]);

			//----------------------------
			//set infomation on detail
			//----------------------------
			//define start line of detail(decrease 1 line first,because loop counter add 1 more line)
			//明細の開始行を定義(ループカウンタで1追加するため最初に1減らす)
			$lExcelRowCount = self::SHONINEXCEL_MEISAI_STARTROW - 1;

			//in case data exists more than 2line(default),add lines
			//テンプレートのデフォルト行数(2行)以上にデータがある場合は、その分の行を追加
			if (count($lTblDataApproval) > 2)
			{
				$sheet->insertNewRowBefore(self::SHONINEXCEL_MEISAI_STARTROW + 1, count($lTblDataApproval) - 2);
			}

			foreach ($lTblDataApproval As $lRow)
			{
				$lExcelRowCount = $lExcelRowCount + 1;
				$lCurrentDataShoninRow = (Array)$lRow;

				$lWorkDate = "";
				$addWord = "";
					
				if ($lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"] == "NG")
				{
					//基準値と実績値の差を求める(小数点7桁まで求める) ※単純に減算すると勝手に浮動小数点で計算して値がずれるので関数を使用する
					$deff =bcadd($lCurrentDataShoninRow["INSPECTION_RESULT_VALUE"], - $lCurrentDataShoninRow["REFERENCE_VALUE"], 7);
					//求めた値を加工する(小数点の後ろゼロを取り除く)
					$addWord = "(".preg_replace("/\.?0+$/","",$deff).")";
				}

				if ($lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"] != "")
				{
					$lWorkDate = $lCurrentDataShoninRow["INSERT_YMDHMS"];
				}

				$arrayData = [
					[
						$lCurrentDataShoninRow["INSPECTION_NO"],
						$lCurrentDataShoninRow["INSPECTION_POINT"],
						$lCurrentDataShoninRow["ANALYTICAL_GRP_01"],
						$lCurrentDataShoninRow["ANALYTICAL_GRP_02"],
						$lCurrentDataShoninRow["SAMPLE_NO"],
						$lCurrentDataShoninRow["INSPECTION_TIME_NAME"],
						$lCurrentDataShoninRow["CONDITION_NAME"],
						$lCurrentDataShoninRow["INSPECTION_ITEM"],
						$lCurrentDataShoninRow["LOT_NO"],
						$lCurrentDataShoninRow["PRODUCTION_YMD"],
						$lCurrentDataShoninRow["REFERENCE_VALUE"],
						$lCurrentDataShoninRow["THRESHOLD_NG_UNDER"],
						$lCurrentDataShoninRow["THRESHOLD_NG_OVER"],
						$lCurrentDataShoninRow["INSPECTION_RESULT_VALUE"],
						$lCurrentDataShoninRow["INSPECTION_RESULT_TYPE"].$addWord,
						$lWorkDate,
						$lCurrentDataShoninRow["OFFSET_NG_BUNRUI_NAME"],
						$lCurrentDataShoninRow["OFFSET_NG_REASON"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_INTERNAL"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_REQUEST"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_TSR"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_SORTING"],
						$lCurrentDataShoninRow["OFFSET_NG_ACTION_DTL_NOACTION"],
						$lCurrentDataShoninRow["ACTION_DETAILS_NAME"],
						$lCurrentDataShoninRow["TR_NO"],
						$lCurrentDataShoninRow["TECHNICIAN_USER_NAME"],
						$lCurrentDataShoninRow["TSR_NO"],
					]
				];

				// $sheet->fromArray($arrayData, null, Cell::stringFromColumnIndex(0) . $lExcelRowCount);
				$sheet->fromArray($arrayData, null, "A".$lExcelRowCount);
			}

			//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
			$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameApproval);
			$lSaveFilePathAndName = "excel/".$lSaveFileName;
			//$lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName;

			//save file（save in server temporarily）（サーバに一旦ファイルを保存）
			$writer = new XlsxWriter($spreadsheet);
			//if many data exist,it need very long time for SAVE.(it is available to download directly without save
			//データ件数が多いと、このSAVE処理が異常に長い。保存せず、直接DL出来れば改善の可能性。
			$writer->save($lSaveFilePathAndName);

			//download
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Length: '.filesize($lSaveFilePathAndName));
			header('Content-disposition: attachment; filename='.$lSaveFileName);
			readfile($lSaveFilePathAndName);

			//delete file（that was saved in server)
			unlink($lSaveFilePathAndName);

			// //message
			// $lViewData["NormalMessage"] = "I005 : Process has been completed.";
		}

		return $lOutputExcelData;
	}

	//**************************************************************************
	// process         getInspectionDataForApproval
	//**************************************************************************
	private function getInspectionDataForApproval($pExcelApprovalArrayData)
	{
		$lTblData = [];
		
		$lTblData = DB::select('
				SELECT HEAD.INSPECTION_RESULT_NO
						,HEAD.INSPECTION_SHEET_NO
						,SHEE.INSPECTION_SHEET_NAME
						,HEAD.REV_NO
						,HEAD.PROCESS_ID
						,PROCESS.CODE_NAME AS PROCESS_NAME
						,SHEE.MODEL_NAME
						,HEAD.MACHINE_NO
						,MACH.MACHINE_NAME
						,HEAD.MOLD_NO
						,HEAD.MATERIAL_NAME
						,HEAD.MATERIAL_SIZE
						,HEAD.COLOR_ID
						,HEAD.INSERT_USER_ID
						,USER.USER_NAME
						,DATE_FORMAT(HEAD.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
						,HEAD.CONDITION_CD
						,COND.CODE_NAME AS CONDITION_NAME
						,HEAD.INSPECTION_TIME_ID
						,TIME.INSPECTION_TIME_NAME
						,HEAD.LOT_NO
						,DATE_FORMAT(HEAD.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
						,DTIL.INSPECTION_NO
						,DTIL.INSPECTION_POINT
						,DTIL.ANALYTICAL_GRP_01
						,DTIL.ANALYTICAL_GRP_02
						,DTIL.SAMPLE_NO
						,IPNO.THRESHOLD_NG_UNDER
						,IPNO.REFERENCE_VALUE
						,IPNO.THRESHOLD_NG_OVER
						,IPNO.INSPECTION_ITEM
						,DTIL.INSPECTION_RESULT_VALUE
						,CASE
							WHEN DTIL.INSPECTION_RESULT_TYPE = "1" THEN "OK"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "3" THEN "NG"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
							WHEN DTIL.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
							ELSE ""
						END INSPECTION_RESULT_TYPE
						,DATE_FORMAT(DTIL.INSERT_YMDHMS,"%T") AS INSERT_YMDHMS
						,NGBUN.OFFSET_NG_BUNRUI_NAME
						,NGRES.OFFSET_NG_REASON
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION = "1" THEN "Running"
							WHEN DTIL.OFFSET_NG_ACTION = "2" THEN "Stop Line"
							ELSE ""
						END OFFSET_NG_ACTION
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_INTERNAL
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_REQUEST
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_TSR
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
							ELSE ""
							END OFFSET_NG_ACTION_DTL_SORTING
						,CASE
							WHEN DTIL.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
							ELSE ""
						END OFFSET_NG_ACTION_DTL_NOACTION
						,DTIL.ACTION_DETAILS_TR_NO_TSR_NO
						,ACTIO.ACTION_DETAILS_NAME
						,DTIL.TR_NO
						,DTIL.TSR_NO
						,TEUR.TECHNICIAN_USER_NAME
				FROM TRESHEDT AS HEAD

			INNER JOIN TRESDETT AS DTIL
					ON HEAD.INSPECTION_RESULT_NO = DTIL.INSPECTION_RESULT_NO
			INNER JOIN TISHEETM AS SHEE
					ON HEAD.INSPECTION_SHEET_NO  = SHEE.INSPECTION_SHEET_NO
				   AND HEAD.REV_NO               = SHEE.REV_NO
			INNER JOIN TINSPNOM AS IPNO
					ON SHEE.INSPECTION_SHEET_NO  = IPNO.INSPECTION_SHEET_NO
				   AND SHEE.REV_NO               = IPNO.REV_NO
				   AND DTIL.INSPECTION_POINT     = IPNO.INSPECTION_POINT

			INNER JOIN TMACHINM AS MACH
					ON HEAD.MACHINE_NO           = MACH.MACHINE_NO
			INNER JOIN TUSERMST AS USER
					ON HEAD.INSERT_USER_ID       = USER.USER_ID
			INNER JOIN TINSPTIM AS TIME
					ON HEAD.INSPECTION_TIME_ID   = TIME.INSPECTION_TIME_ID
	   LEFT OUTER JOIN TTECUSRT AS TEUR
					ON DTIL.TECHNICIAN_USER_ID   = TEUR.TECHNICIAN_USER_ID
	   LEFT OUTER JOIN tngrebnm AS NGBUN
					ON NGBUN.OFFSET_NG_BUNRUI_ID = DTIL.OFFSET_NG_BUNRUI_ID
	   LEFT OUTER JOIN tngresnm AS NGRES
					ON NGRES.OFFSET_NG_REASON_ID = DTIL.OFFSET_NG_REASON_ID
				   AND NGRES.OFFSET_NG_BUNRUI_ID = DTIL.OFFSET_NG_BUNRUI_ID
	   LEFT OUTER JOIN tactionm AS ACTIO
					ON ACTIO.ACTION_DETAILS_ID   = DTIL.ACTION_DETAILS_ID

			INNER JOIN TCODEMST AS COND
					ON COND.CODE_CLASS           = "001"
				   AND COND.CODE_ORDER           = HEAD.CONDITION_CD
			INNER JOIN TCODEMST AS PROCESS
					ON PROCESS.CODE_CLASS        = "002"
				   AND PROCESS.CODE_ORDER        = HEAD.PROCESS_ID

				  WHERE HEAD.INSPECTION_SHEET_NO = :CheckSheetNo
					AND HEAD.REV_NO              = :RevisionNo
					AND SHEE.PROCESS_ID          = :ProcessId
					AND HEAD.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, HEAD.MACHINE_NO)
					AND HEAD.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, HEAD.MOLD_NO)
					AND HEAD.INSERT_USER_ID      = :UserID
					AND HEAD.INSPECTION_YMD      = :DateFrom
				ORDER BY DTIL.DISPLAY_ORDER
						,TIME.DISPLAY_ORDER
						,HEAD.CONDITION_CD
			',
				[
					"CheckSheetNo"   => $pExcelApprovalArrayData["CheckSheetNo"],
					"RevisionNo"     => $pExcelApprovalArrayData["RevisionNo"],
					"ProcessId"      => $pExcelApprovalArrayData["ProcessId"],
					"MachineNo1"     => $pExcelApprovalArrayData["MachineNo"],
					"MachineNo2"     => $pExcelApprovalArrayData["MachineNo"],
					"MoldNo1"        => $pExcelApprovalArrayData["MoldNo"],
					"MoldNo2"        => $pExcelApprovalArrayData["MoldNo"],
					"UserID"         => $pExcelApprovalArrayData["InspectorCode"],
					"DateFrom"       => $pExcelApprovalArrayData["InspectionDateFrom"],
				]
			);          
			       
		return $lTblData;
	}




}
