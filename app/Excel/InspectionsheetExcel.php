<?php

namespace App\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Model\tinsptim;

use DB;
use Log;

require_once '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

class InspectionsheetExcel
{
	CONST INSPECTIONSHEETEXCEL_PIC_MAX            = 15;
	CONST INSPECTIONSHEETEXCEL_DETAIL_COUNT       = 38;
	CONST INSPECTIONSHEETEXCEL_COL_OFFSET         = 0;
	CONST INSPECTIONSHEETEXCEL_ROW_OFFSET         = 52;
	//CONST INSPECTIONSHEETEXCEL_HEADER_STARTCOL      = XX;  //line number for header(列)
	CONST INSPECTIONSHEETEXCEL_HEADER_STARTROW    = 1;  //line number for header(行)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTCOL    = 1;  //line number for detail(列)
	CONST INSPECTIONSHEETEXCEL_MEISAI_STARTROW    = 52; //line number for detail(行)]

	CONST CONVDATA_ARG                            = "2";

	CONST SAMPLE_MAX                              = 8;
	CONST JUDGE_1                                 = 1;
	CONST JUDGE_2                                 = 2;
	CONST JUDGE_3                                 = 3;
	CONST JUDGE_4                                 = 4;

	CONST RESULT_INPUT_TYPE_NUMBER                = "1";
	CONST RESULT_INPUT_TYPE_OKNG                  = "2";

	CONST RESULT_CLASS_OK                         = "1"; //OK
	CONST RESULT_CLASS_OFFSET                     = "2"; //OFFSET
	CONST RESULT_CLASS_NG                         = "3"; //NG
	CONST RESULT_CLASS_NO_CHECK                   = "4"; //NO CHECK
	CONST RESULT_CLASS_LINE_STOP                  = "5"; //LINE STOP
	
	CONST LOGOPATH                                = "img/CompanyLogo.png";
	CONST NOTHING                                 = "-";
	CONST BLANK                                   = " ";
	CONST OK                                      = "OK";
	CONST NG                                      = "NG";
	CONST STAR                                    = "★";
	CONST CEL_E                                   = "4";


	public function getIsnpectionSheetExcel($pInspectionResultNo, $pDataRev)
	{
		Log::write('info', 'Start inspection sheet');

		$lTemplateFileNameInspectionSheet = "";
		$lOutputExcelData = 0;

		$lWork1InspectionPoint      = "";
		$lWork1AnalyGrp01           = "";
		$lWork1AnalyGrp02           = "";
		$lWork2InspectionPoint      = "";
		$lWork2AnalyGrp01           = "";
		$lWork2AnalyGrp02           = "";

		$lWorkPicCount      = 0 ;
		$lWorkCount         = 0;
		$lWorkResultCount   = 0;
		$lWorkJudge         = 0;
		$lFinalJudge        = "";
		$lWorkFinalJudge    = 0;

		$lPageCount         = 1;
		$lRemainder         = 0;
		$lWorkValue         = "";
		$lWorkPosition      = 0;

		$lTblHeaderTableDataInspectionSheet = $this->getHeaderTableDataInspectionSheet($pInspectionResultNo, $pDataRev);

		if ($lTblHeaderTableDataInspectionSheet == null)
		{
			$lOutputExcelData = null;
		}
		else
		{
			$lTblHeaderTableDataInspectionSheetRow = (Array)$lTblHeaderTableDataInspectionSheet[0];

			$lWorkInspectionDate  = "";
			$lWorkProductionDate  = "";
			$lWorkInspectionDate = $this->convertDateFormat((String)$lTblHeaderTableDataInspectionSheetRow["INSPECTION_YMD"], self::CONVDATA_ARG);
			$lWorkProductionDate = $this->convertDateFormat((String)$lTblHeaderTableDataInspectionSheetRow["PRODUCTION_YMD"], self::CONVDATA_ARG);


			$lTblSheetMasterTaleDataInspectionSheet = $this->getSheetMasterTaleDataInspectionSheet($lTblHeaderTableDataInspectionSheetRow);
			$lTblSheetMasterTaleDataInspectionSheetRow = (Array)$lTblSheetMasterTaleDataInspectionSheet[0];

			$lTblTime = [];
			$lRowTime = [];
			$objtinsptim = new tinsptim;
			$lTblTime = $objtinsptim->getTimeInfo($lTblHeaderTableDataInspectionSheetRow["INSPECTION_TIME_ID"]);
			if (count($lTblTime) == 0)
			{
				//Nothing
			}
			else
			{
				$lRowTime = (array)$lTblTime[0];
			}

			//Check Number of Pic
			$lTblOutputPictureInspectionSheet = $this->getOutputPictureInspectionSheet($lTblHeaderTableDataInspectionSheetRow);

			$PictureCount = count($lTblOutputPictureInspectionSheet);

			if ($PictureCount > self::INSPECTIONSHEETEXCEL_PIC_MAX)
			{
				$PictureCount = self::INSPECTIONSHEETEXCEL_PIC_MAX;
			}

			//Check Count of data
			$lTblOutputDetailRecordCountInspectionSheet = $this->getOutputDetailRecordCountInspectionSheet($pInspectionResultNo, $pDataRev);
			$lTblOutputDetailRecordCountInspectionSheetRow = (Array)$lTblOutputDetailRecordCountInspectionSheet[0];

			$lTblGrp01No = $this->getGrp01No($lTblOutputDetailRecordCountInspectionSheetRow["GRP01_ID"]);
			$lTblGrp01NoRow = (Array)$lTblGrp01No[0];

			$DetailCount = count($lTblOutputDetailRecordCountInspectionSheet);
			$TotalPageNumber = ceil($DetailCount / self::INSPECTIONSHEETEXCEL_DETAIL_COUNT) + 1;

			// Log::write('info', '1',
			// 	[
			// 		"PictureCount"     => $PictureCount,
			// 		"DetailCount"      => $DetailCount,
			// 		"TotalPageNumber"  => $TotalPageNumber,
			// 	]
			// );

			$lTblResultDetailInspectionSheet = $this->getResultDetailInspectionSheet($pInspectionResultNo);

			$spreadsheet = new Spreadsheet();
			$lTemplateFileNameInspectionSheet = "InspectionSheet_template.xlsx";
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("excel/".$lTemplateFileNameInspectionSheet);

			$sheet = $spreadsheet->getActiveSheet();

			if ($TotalPageNumber >= 3)
			{
				$sheet->insertNewRowBefore((self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * 2), self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * ($TotalPageNumber - 2));
			}

			//----------------------------
			//copy
			//----------------------------
			for ($CopyCount = 1 ; $CopyCount < ($TotalPageNumber - 1); $CopyCount++)
			{
				//1st copy (0,53)->(18,104) copy (0, 105)->(18, 156)
				//2nd copy (0,53)->(18,104) copy (0, 157)->(18, 208)
				//3rd copy (0,53)->(18,104) copy (0, 208)->(18, 259)
				//4th ・・・・
				for ($col = 0 ; $col < 19 ; $col++)
				{
					for ($row = 53 ; $row < 105 ; $row++)
					{
						//Get cell
						$cell = $sheet->getCellByColumnAndRow($col, $row);
						//Get cell style
						$style = $sheet->getStyleByColumnAndRow($col, $row);
						//Change (0,1)→A1
						$offsetCell = stringFromColumnIndex($col + self::INSPECTIONSHEETEXCEL_COL_OFFSET) . (string)($row + ($CopyCount * self::INSPECTIONSHEETEXCEL_ROW_OFFSET));
						//Copy Value
						$sheet->setCellValue($offsetCell, $cell->getValue());
						//Copy Style
						$sheet->duplicateStyle($style, $offsetCell);
					}
				}
			}

			if ($TotalPageNumber >= 3)
			{
				for ($Count = 1 ; $Count < ($TotalPageNumber - 1); $Count++)
				{
					//画像用のオプジェクト作成
					$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
					//貼り付ける画像のパスを指定
					$drawing->setPath(self::LOGOPATH);
					//画像の高さを指定
					$drawing->setHeight(40);
					//画像のプロパティを見たときに表示される情報を設定
					//画像名
					$drawing->setName('Image');
					//画像の概要
					$drawing->setDescription('Image');
					//位置
					$lWorkPosition = PHPExcel_Cell::stringFromColumnIndex(self::CEL_E).(string)(103 + self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * $Count);
					$drawing->setCoordinates($lWorkPosition);
					//横方向へ何ピクセルずらすかを指定
					$drawing->setOffsetX(20);
					//回転の角度
					//$objDrawing->setRotation(25);
					//ドロップシャドウをつけるかどうか
					//$objDrawing->getShadow()->setVisible(true);
					//Excelオブジェクトに張り込み
					$drawing->setWorksheet($sheet);
				}
			}

			//----------------------------
			//set information in header (1st page)
			//----------------------------
			//$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTCOL;
			$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_HEADER_STARTROW;
			$sheet->getCell("E".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["CUSTOMER_NAME"]);

			$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
			$sheet->getCell("E".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MODEL_NAME"]);
			$sheet->getCell("Q".$lInspectionSheetRowCount)->setValue(1);
			$sheet->getCell("S".$lInspectionSheetRowCount)->setValue($TotalPageNumber);

			$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
			// $sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["PROCESS_NAME"]);
			$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lRowTime["INSPECTION_TIME_NAME_FORM"]);
			$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_NAME"]);

			$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
			$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NO"]);
			$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_SIZE"]);
			$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["DRAWING_NO"]);

			$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
			$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NAME"]);
			$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["COLOR_NAME"]);
			$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblGrp01NoRow["GRP01_QTY"]);
			$sheet->getCell("R".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["MOLD_NO"]);

			$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
			$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lWorkProductionDate);
			$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lWorkInspectionDate);
			$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["LOT_NO"]);

			//----------------------------
			//set information in picture
			//----------------------------
			if ($PictureCount > 0)
			{
				foreach ($lTblOutputPictureInspectionSheet As $lPictureRow)
				{
					$lCurrentPictureRow = (Array)$lPictureRow;
					$lWorkPicCount = $lWorkPicCount + 1 ;

					if (self::INSPECTIONSHEETEXCEL_PIC_MAX >= $lWorkPicCount)
					{
						//画像用のオプジェクト作成
						$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
						//貼り付ける画像のパスを指定
						$drawing->setPath($lCurrentPictureRow["PICTURE_URL"]);
						//画像の高さを指定
						$drawing->setHeight(150);
						//画像のプロパティを見たときに表示される情報を設定
						//画像名
						$drawing->setName('Image');
						//画像の概要
						$drawing->setDescription('Image');
						//位置
						if ($lWorkPicCount == 1)
						{
							$drawing->setCoordinates('B8');
						}
						if ($lWorkPicCount == 2)
						{
							$drawing->setCoordinates('G8');
						}
						if ($lWorkPicCount == 3)
						{
							$drawing->setCoordinates('M8');
						}
						if ($lWorkPicCount == 4)
						{
							$drawing->setCoordinates('B15');
						}
						if ($lWorkPicCount == 5)
						{
							$drawing->setCoordinates('G15');
						}
						if ($lWorkPicCount == 6)
						{
							$drawing->setCoordinates('M15');
						}
						if ($lWorkPicCount == 7)
						{
							$drawing->setCoordinates('B22');
						}
						if ($lWorkPicCount == 8)
						{
							$drawing->setCoordinates('G22');
						}
						if ($lWorkPicCount == 9)
						{
							$drawing->setCoordinates('M22');
						}
						if ($lWorkPicCount == 10)
						{
							$drawing->setCoordinates('B29');
						}
						if ($lWorkPicCount == 11)
						{
							$drawing->setCoordinates('G29');
						}
						if ($lWorkPicCount == 12)
						{
							$drawing->setCoordinates('M29');
						}
						if ($lWorkPicCount == 13)
						{
							$drawing->setCoordinates('B36');
						}
						if ($lWorkPicCount == 14)
						{
							$drawing->setCoordinates('G36');
						}
						if ($lWorkPicCount == 15)
						{
							$drawing->setCoordinates('M36');
						}
						//横方向へ何ピクセルずらすかを指定
						$drawing->setOffsetX(10);
						//Excelオブジェクトに張り込み
						$drawing->setWorksheet($sheet);
					}
				}
			}
			else
			{
				//Nothing
			}

			//----------------------------
			//set information in detail
			//----------------------------
			foreach ($lTblOutputDetailRecordCountInspectionSheet As $lResultDetailRow1)
			{
				$lInspectionSheetColCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTCOL;

				$lCurrentRow1 = (Array)$lResultDetailRow1;
				$lWorkCount = $lWorkCount + 1;
				$lRemainder = $lWorkCount % self::INSPECTIONSHEETEXCEL_DETAIL_COUNT;

				if ($lRemainder == 1)
				{
					$lPageCount = $lPageCount + 1;

					if ($lPageCount >= 2)
					{
						$lInspectionSheetRowCount = self::INSPECTIONSHEETEXCEL_MEISAI_STARTROW * ($lPageCount - 1);

						Log::write('info', '211',
							[
								"InspectionSheetRowCount"  => $lInspectionSheetRowCount,
							]
						);
					}

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$sheet->getCell("E".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["CUSTOMER_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$sheet->getCell("E".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MODEL_NAME"]);
					$sheet->getCell("Q".$lInspectionSheetRowCount)->setValue($lPageCount);
					$sheet->getCell("S".$lInspectionSheetRowCount)->setValue($TotalPageNumber);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					// $sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["PROCESS_NAME"]);
					$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lRowTime["INSPECTION_TIME_NAME_FORM"]);
					$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_NAME"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NO"]);
					$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["MATERIAL_SIZE"]);
					$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["DRAWING_NO"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["ITEM_NAME"]);
					$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lTblSheetMasterTaleDataInspectionSheetRow["COLOR_NAME"]);
					$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblGrp01NoRow["GRP01_QTY"]);
					$sheet->getCell("R".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["MOLD_NO"]);

					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
					$sheet->getCell("C".$lInspectionSheetRowCount)->setValue($lWorkProductionDate);
					$sheet->getCell("H".$lInspectionSheetRowCount)->setValue($lWorkInspectionDate);
					$sheet->getCell("O".$lInspectionSheetRowCount)->setValue($lTblHeaderTableDataInspectionSheetRow["LOT_NO"]);
				}

				if ($lRemainder == 1)
				{
					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 3;
				}
				else
				{
					$lInspectionSheetRowCount = $lInspectionSheetRowCount + 1;
				}

				//***************************************************
				//leave item
				$lWork2InspectionPoint      = $lCurrentRow1["INSPECTION_POINT"];
				$lWork2AnalyGrp01           = $lCurrentRow1["ANALYTICAL_GRP_01"];
				$lWork2AnalyGrp02           = $lCurrentRow1["ANALYTICAL_GRP_02"];
				//***************************************************

				// Log::write('info', '1',
				// 		[
				// 			"Work1InspectionPoint"  => $lWork1InspectionPoint,
				// 			"Work1AnalyGrp01"  => $lWork1AnalyGrp01,
				// 			"Work1AnalyGrp02"  => $lWork1AnalyGrp02,
				// 			"Work2InspectionPoint"  => $lWork2InspectionPoint,
				// 			"Work2AnalyGrp01"  => $lWork2AnalyGrp01,
				// 			"Work2AnalyGrp02"  => $lWork2AnalyGrp02,
				// 		]
				// 	);

				if ($lWork1InspectionPoint == $lWork2InspectionPoint)
				{
					$lInspectionSheetColCount = $lInspectionSheetColCount + 7;
				}
				else
				{
					$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								->setValue($lCurrentRow1["INSPECTION_POINT"]);

					$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
					$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								->setValue($lCurrentRow1["INSPECTION_ITEM"]);

					$lInspectionSheetColCount = $lInspectionSheetColCount + 2;
					$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								->setValue($lCurrentRow1["SPEC_WRITE_FORMAT"]);

					if ($lCurrentRow1["INSPECTION_RESULT_INPUT_TYPE"] == self::RESULT_INPUT_TYPE_NUMBER)
					{
						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValue($lCurrentRow1["THRESHOLD_NG_UNDER"]);

						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValue($lCurrentRow1["REFERENCE_VALUE"]);

						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValue($lCurrentRow1["THRESHOLD_NG_OVER"]);
					}
					else
					{
						$lInspectionSheetColCount = $lInspectionSheetColCount + 3;
					}

					$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
					$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								->setValue($lCurrentRow1["SOKUTEIKI_CD"]);
				}

				if (($lWork1InspectionPoint == $lWork2InspectionPoint) and ($lWork1AnalyGrp01 == $lWork2AnalyGrp01))
				{
					$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
				}
				else
				{
					$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
					$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
								->setValue($lCurrentRow1["ANALYTICAL_GRP_01"]);
				}

				$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
				$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							->setValue($lCurrentRow1["ANALYTICAL_GRP_02"]);


				foreach ($lTblResultDetailInspectionSheet As $lResultDetailRow2)
				{
					$lCurrentRow2 = (Array)$lResultDetailRow2;

					if (($lWork2InspectionPoint == $lCurrentRow2["INSPECTION_POINT"]) and ($lWork2AnalyGrp01 == $lCurrentRow2["ANALYTICAL_GRP_01"]) and ($lWork2AnalyGrp02 == $lCurrentRow2["ANALYTICAL_GRP_02"]))
					{
						$lWorkResultCount = $lWorkResultCount + 1;

						if ($lCurrentRow1["INSPECTION_RESULT_INPUT_TYPE"] == self::RESULT_INPUT_TYPE_NUMBER)
						{
							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;

							if ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK)
							{
								$lInspectionResultValue = $lCurrentRow2["INSPECTION_RESULT_VALUE"];
								$lWorkJudge = self::JUDGE_1;
							}
							elseif (($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET) or ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG))
							{
								$lWorkValue = self::STAR.$lCurrentRow2["INSPECTION_RESULT_VALUE"];
								$lInspectionResultValue = $lWorkValue;
								$lWorkJudge = self::JUDGE_2;
							}
							elseif (($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NO_CHECK) or ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_LINE_STOP))
							{
								$lInspectionResultValue = self::NOTHING;
								$lWorkJudge = self::JUDGE_3;
							}
							else
							{
								//No input result
								$lInspectionResultValue = self::BLANK;
								$lWorkJudge = self::JUDGE_4;
							}

							$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
											->setValue($lInspectionResultValue);


						}
						else
						{
							$lInspectionSheetColCount = $lInspectionSheetColCount + 1;

							if ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK)
							{
								$lInspectionResultOKNG = self::OK;
								$lWorkJudge = self::JUDGE_1;
							}
							elseif (($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET) or ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG))
							{
								$lInspectionResultOKNG = self::NG;
								$lWorkJudge = self::JUDGE_2;
							}
							elseif (($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NO_CHECK) or ($lCurrentRow2["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_LINE_STOP))
							{
								$lInspectionResultOKNG = self::NOTHING;
								$lWorkJudge = self::JUDGE_3;
							}
							else
							{
								//No input result
								$lInspectionResultOKNG = self::BLANK;
								$lWorkJudge = self::JUDGE_4;
							}

							$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
										->setValue($lInspectionResultOKNG);
						}

						if ($lWorkFinalJudge == 0)
						{
							$lWorkFinalJudge = $lWorkJudge;
						}
						else
						{
							if ($lWorkFinalJudge == self::JUDGE_1)
							{
								if ($lWorkJudge == self::JUDGE_2)
								{
									$lWorkFinalJudge = $lWorkJudge;
								}
							}
							elseif ($lWorkFinalJudge == self::JUDGE_2)
							{
								//Nothing to do
							}
							elseif ($lWorkFinalJudge == self::JUDGE_3)
							{
								if ($lWorkJudge == self::JUDGE_1 or $lWorkJudge == self::JUDGE_2)
								{
									$lWorkFinalJudge = $lWorkJudge;
								}
							}
							else
							{
								$lWorkFinalJudge = $lWorkJudge;
							}
						}
					}
				}

				// Log::write('info', '1',
				// 		[
				// 			"WorkResultCount"  => $lWorkResultCount,
				// 		]
				// 	);

				if ($lWorkResultCount < self::SAMPLE_MAX)
				{
					for ($WriteCount = $lWorkResultCount +1; $WriteCount <= self::SAMPLE_MAX; $WriteCount++)
					{
						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
					}
				}
				elseif ($lWorkResultCount == self::SAMPLE_MAX)
				{
					//Nothing
				}
				else
				{
					$lInspectionSheetColCount = 11;
					for ($WriteCount = 1; $WriteCount <= self::SAMPLE_MAX; $WriteCount++)
					{
						$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
						$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
									->setValue(self::NOTHING);
					}
				}

				if ($lWorkFinalJudge == self::JUDGE_1)
				{
					$lFinalJudge = self::OK;
				}
				elseif ($lWorkFinalJudge == self::JUDGE_2)
				{
					$lFinalJudge = self::NG;
				}
				elseif ($lWorkFinalJudge == self::JUDGE_3)
				{
					$lFinalJudge = self::NOTHING;
				}
				else
				{
					$lFinalJudge = self::BLANK;
				}

				$lInspectionSheetColCount = $lInspectionSheetColCount + 1;
				$sheet->getCellByColumnAndRow($lInspectionSheetColCount, $lInspectionSheetRowCount)
							->setValue($lFinalJudge);

				//***************************************************
				//leave item
				$lWork1InspectionPoint      = $lWork2InspectionPoint;
				$lWork1AnalyGrp01           = $lWork2AnalyGrp01;
				$lWork1AnalyGrp02           = $lWork2AnalyGrp02;
				$lWorkResultCount           = 0;
				$lWorkJudge                 = 0;
				$lWorkFinalJudge            = 0; 
				//***************************************************
			}

			//set save file name and pass（change「template」to「now YYYYMMDD_HHmmss」
			$lSaveFileName = str_replace("template", date("Ymd")."_".date("His"), $lTemplateFileNameInspectionSheet); 
			$lSaveFilePathAndName = "excel/".$lSaveFileName;  
			// $lSaveFilePathAndName = public_path()."/excel/".$lSaveFileName; 

			//save file（save in server temporarily）
			$writer = new XlsxWriter($spreadsheet);
			//if many data exist,it need very long time for SAVE.(it is available to download directly without save
			$writer->save($lSaveFilePathAndName);

			//download
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Length: '.filesize($lSaveFilePathAndName));
			header('Content-disposition: attachment; filename='.$lSaveFileName);
			readfile($lSaveFilePathAndName);

			//delete fime(saved in server)
			unlink($lSaveFilePathAndName);
		}

		return $lOutputExcelData;
	}

	//**************************************************************************
	// process         getHeaderTableDataInspectionSheet
	//**************************************************************************
	private function getHeaderTableDataInspectionSheet($pInspectionResultNo, $pDataRev)
	{
		$lTblHeaderTableDataInspectionSheet = [];

			$lTblHeaderTableDataInspectionSheet = DB::select('
			    SELECT HEAD.INSPECTION_RESULT_NO
						,HEAD.INSPECTION_SHEET_NO
						,HEAD.REV_NO
						,HEAD.PROCESS_ID
						,PROCESS.CODE_NAME AS PROCESS_NAME
						,HEAD.MOLD_NO
						,HEAD.INSPECTION_YMD
						,HEAD.LOT_NO
						,HEAD.PRODUCTION_YMD
						,HEAD.INSPECTION_TIME_ID
				    FROM TRESHEDT AS HEAD
			  INNER JOIN TCODEMST AS PROCESS
					  ON PROCESS.CODE_CLASS       = "002"
					 AND HEAD.PROCESS_ID          = PROCESS.CODE_ORDER
				 WHERE HEAD.INSPECTION_RESULT_NO  = :InspectionResultNo
				   AND HEAD.DATA_REV              = :DataRev
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
					"DataRev"                => $pDataRev,
				]
			);

		return $lTblHeaderTableDataInspectionSheet;
	}

	//**************************************************************************
	// process         getSheetMasterTaleDataInspectionSheet
	//**************************************************************************
	private function getSheetMasterTaleDataInspectionSheet($pRow)
	{
		$lTblSheetMasterTaleDataInspectionSheet = [];

			$lTblSheetMasterTaleDataInspectionSheet = DB::select('
					SELECT SHEE.INSPECTION_SHEET_NO
							,SHEE.REV_NO
							,SHEE.CUSTOMER_ID
							,CUST.CUSTOMER_NAME
							,SHEE.MODEL_NAME
							,SHEE.ITEM_NO
							,ITEM.ITEM_NAME
							,SHEE.DRAWING_NO
							,ITEM.MATERIAL_NAME
							,ITEM.MATERIAL_SIZE
							,ITEM.COLOR_ID
							,COLOR.CODE_NAME AS COLOR_NAME
						FROM TISHEETM AS SHEE
				INNER JOIN TCUSTOMM AS CUST
					    ON SHEE.CUSTOMER_ID         = CUST.CUSTOMER_ID
			LEFT OUTER JOIN TITEMMST AS ITEM
					    ON ITEM.ITEM_NO             = SHEE.ITEM_NO
					   AND ITEM.DELETE_FLG          = "0"
			LEFT OUTER JOIN TCODEMST AS COLOR
				        ON COLOR.CODE_CLASS         = "006"
				       AND ITEM.COLOR_ID            = COLOR.CODE_ORDER
				     WHERE SHEE.INSPECTION_SHEET_NO = :CheckSheetNo
					   AND SHEE.REV_NO              = :RevisionNo
					   AND SHEE.DELETE_FLG          = "0"
				',
				[
					"CheckSheetNo"       => TRIM((String)$pRow["INSPECTION_SHEET_NO"]),
					"RevisionNo"         => TRIM((String)$pRow["REV_NO"]),
				]
			);

		return $lTblSheetMasterTaleDataInspectionSheet;
	}

	//**************************************************************************
	// process         getOutputPictureInspectionSheet
	//**************************************************************************
	private function getOutputPictureInspectionSheet($pRow)
	{
		$lTblOutputPictureInspectionSheet = [];

			$lTblOutputPictureInspectionSheet = DB::select('
					SELECT IPNO.PICTURE_URL
					  FROM TINSPNOM AS IPNO
				     WHERE IPNO.INSPECTION_SHEET_NO = :CheckSheetNo
					   AND IPNO.REV_NO              = :RevisionNo
					   AND IPNO.DELETE_FLG          = "0"
				  GROUP BY IPNO.PICTURE_URL
				  ORDER BY IPNO.DISPLAY_ORDER
				          ,IPNO.INSPECTION_POINT
				',
				[
					"CheckSheetNo"       => TRIM((String)$pRow["INSPECTION_SHEET_NO"]),
					"RevisionNo"         => TRIM((String)$pRow["REV_NO"]),
				]
			);

		return $lTblOutputPictureInspectionSheet;
	}

	//**************************************************************************
	// process         getOutputDetailRecordCountInspectionSheet
	//**************************************************************************
	private function getOutputDetailRecordCountInspectionSheet($pInspectionResultNo, $pDataRev)
	{
		$lTblOutputDetailRecordCountInspectionSheet = [];

			$lTblOutputDetailRecordCountInspectionSheet = DB::select('
					SELECT DETL.INSPECTION_RESULT_NO
							,DETL.INSPECTION_NO
							,DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
							,DETL.DISPLAY_ORDER
							,ISNO.INSPECTION_SHEET_NO
							,ISNO.REV_NO
							,ISNO.INSPECTION_ITEM
							,SOKT.TOOL_CD
							,ISNO.SOKUTEIKI_CD
							,SOKT.INSPECTION_RESULT_INPUT_TYPE
							,ISNO.THRESHOLD_NG_UNDER
							,ISNO.THRESHOLD_OFFSET_UNDER
							,ISNO.REFERENCE_VALUE
							,ISNO.THRESHOLD_OFFSET_OVER
							,ISNO.THRESHOLD_NG_OVER
							,ISNO.SPEC_WRITE_FORMAT
							,ISNO.GRP01_ID
							,ISNO.GRP02_ID
							,ISNO.SAMPLE_QTY
						FROM TRESDETT AS DETL
				  INNER JOIN TRESHEDT AS HEAD
						  ON HEAD.INSPECTION_RESULT_NO   = DETL.INSPECTION_RESULT_NO
						 AND HEAD.DATA_REV               = :DataRev
				  INNER JOIN TINSPNOM AS ISNO
						  ON ISNO.INSPECTION_SHEET_NO    = HEAD.INSPECTION_SHEET_NO
						 AND ISNO.REV_NO                 = HEAD.REV_NO
						 AND ISNO.DELETE_FLG             = "0"
				  INNER JOIN TSOKTEIM AS SOKT
						  ON SOKT.SOKUTEIKI_CD           = ISNO.SOKUTEIKI_CD
						 AND SOKT.DELETE_FLG             = "0"
					   WHERE DETL.INSPECTION_RESULT_NO   = :InspectionResultNo
						 AND DETL.INSPECTION_POINT       = ISNO.INSPECTION_POINT
					GROUP BY DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
					ORDER BY DETL.DISPLAY_ORDER
							,DETL.INSPECTION_NO
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
					"DataRev"                => $pDataRev,
				]
			);

		return $lTblOutputDetailRecordCountInspectionSheet;
	}

	//**************************************************************************
	// process         getGrp01No
	//**************************************************************************
	private function getGrp01No($pGrp01)
	{
		$lTblInfo = [];

			$lTblInfo = DB::select('
					SELECT CDMST.CODE_NAME AS GRP01_QTY
					  FROM TCODEMST AS CDMST
					 WHERE CDMST.CODE_CLASS = :Grp01
				  ORDER BY CDMST.CODE_NO desc
			',
				[
					"Grp01"  => $pGrp01,
				]
			);

		return $lTblInfo;
	}











	//**************************************************************************
	// process         getResultDetailInspectionSheet
	//**************************************************************************
	private function getResultDetailInspectionSheet($pInspectionResultNo)
	{
		$lTblResultDetailInspectionSheet = [];

			$lTblResultDetailInspectionSheet = DB::select('
					SELECT DETL.INSPECTION_RESULT_NO
							,DETL.INSPECTION_NO
							,DETL.INSPECTION_POINT
							,DETL.ANALYTICAL_GRP_01
							,DETL.ANALYTICAL_GRP_02
							,DETL.SAMPLE_NO
							,DETL.INSPECTION_RESULT_VALUE
							,DETL.INSPECTION_RESULT_TYPE
							,DETL.DISPLAY_ORDER
						FROM TRESDETT AS DETL
					   WHERE DETL.INSPECTION_RESULT_NO   = :InspectionResultNo
					ORDER BY DETL.DISPLAY_ORDER
							,DETL.INSPECTION_NO
			',
				[
					"InspectionResultNo"     => $pInspectionResultNo,
				]
			);

		return $lTblResultDetailInspectionSheet;
	}

	//**************************************************************************
	// process         convertDateFormat
	//**************************************************************************
	private function convertDateFormat($pTargetDate, $pConvertPattern)
	{
		//not to entry date
		if ($pTargetDate == "")
		{
			return "";
		}

		//store in work as date type(countermove for 2038)
		$lWorkDate = date_create($pTargetDate);

		//English tipe->Japanese tipe
		if ($pConvertPattern == "1")
		{
			return date_format($lWorkDate, 'Y/m/d');
		}

		//Japanese tipe-> English tipe
		return date_format($lWorkDate, 'd-m-Y');
	}


}
