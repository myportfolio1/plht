<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tptnmst extends Model
{
    protected $table = 'tptnmst';

	public function getPatarnData($pPtnNo)
	{
		$lTblInfo = [];

        $lTblInfo = DB::table('TPTNMST')
         ->select('PTN_NO', 'PTN_NAME', 'CALL_CONTENTS1', 'CALL_CONTENTS2')
         ->where('PTN_NO', '=', $pPtnNo)
         ->get();

		return $lTblInfo;
	}

}

