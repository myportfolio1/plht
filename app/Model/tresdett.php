<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tresdett extends Model
{
	protected $table = 'tresdett';


	public function getDetailData($pInspectionResultNo, $pInspectionNo)
	{
		$lTblDetailInfo = [];

        $lTblDetailInfo = DB::table('TRESDETT')
         ->select('INSPECTION_POINT', 'ANALYTICAL_GRP_01', 'ANALYTICAL_GRP_02')
         ->where('INSPECTION_RESULT_NO', '=', $pInspectionResultNo)
         ->where('INSPECTION_NO', '=', $pInspectionNo)
         ->get();

		return $lTblDetailInfo;
	}


}

