<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tmachinm extends Model
{
	protected $table = 'tmachinm';

	public function getMachineData($pMachineNo)
	{
		$lTblMachineInfo = [];

        $lTblMachineInfo = DB::table('TMACHINM')
         ->select('MACHINE_NAME')
         ->where('MACHINE_NO', '=', $pMachineNo)
         ->where('DELETE_FLG', '=', '0')
         ->get();

		return $lTblMachineInfo;
	}

}

