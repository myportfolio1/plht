<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tcodemst extends Model
{
	CONST ConditionCode   = "001";
	CONST ProcessCode     = "002";
	CONST JudgeReasonCode = "003";
	CONST TeamCode        = "004";

    protected $table = 'tcodemst';

	public function getProcessList()
	{
		$lArrDataListProcess = $this->getCodeList(self::ProcessCode);
		return $lArrDataListProcess;
	}


	public function getConditionList()
	{
		$lArrDataListCondition = $this->getCodeList(self::ConditionCode);
		return $lArrDataListCondition;
	}


	public function getJudgeReasonList()
	{
		$lArrDataListJudgeReason= $this->getCodeList(self::JudgeReasonCode);
		return $lArrDataListJudgeReason;
	}

	private function getCodeList($pArgument)
	{
		$lRowCountCode = 0;

		$lTblCodeList = self::where('CODE_CLASS', $pArgument)
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['CODE_ORDER', 'CODE_NAME']);

		// echo "<pre>";
		// print_r($lTblCodeList);
		// echo "</pre>";



		// $lArrDataCodeList = [ "" => "" ];

		// foreach ($lTblCodeList as $lRowCodeList)
		// {
		// 	$lArrDataCodeList += [
		// 		$lRowCodeList["CODE_ORDER"] => $lRowCodeList["CODE_NAME"]
		// 	];
		// }

		// return $lArrDataCodeList;


		if ($lTblCodeList != null)
		{
			foreach ($lTblCodeList as $lRowCodeList)
			{
				$lRowCountCode = $lRowCountCode + 1;
			}

			if ($lRowCountCode != 1)
			{
				$lArrDataCodeList = [ "" => "" ];

				foreach ($lTblCodeList as $lRowCodeList)
				{
					$lArrDataCodeList += [
						$lRowCodeList["CODE_ORDER"] => $lRowCodeList["CODE_NAME"]
					];
				}
			}
			else
			{
				foreach ($lTblCodeList as $lRowCodeList)
				{
					$lArrDataCodeList = [
						$lRowCodeList["CODE_ORDER"] => $lRowCodeList["CODE_NAME"]
					];
				}
			}
		}
		else
		{
			$lArrDataCodeList = [ "" => "" ];
		}

		return $lArrDataCodeList;
	}


	public function getCodeInfo($pCodeClass, $pCodeOrder)
	{
		$lTblCodeInfo = [];

        $lTblCodeInfo = DB::table('TCODEMST')
         ->select('CODE_ORDER', 'CODE_NAME')
         ->where('CODE_CLASS', '=', $pCodeClass)
         ->where('CODE_ORDER', '=', $pCodeOrder)
         ->get();

		return $lTblCodeInfo;
	}

	public function getCodeInfo2($pCodeClass, $pCodeNo)
	{
		$lTblCodeInfo = [];

        $lTblCodeInfo = DB::table('TCODEMST')
         ->select('CODE_ORDER', 'CODE_NAME')
         ->where('CODE_CLASS', '=', $pCodeClass)
         ->where('CODE_NO', '=', $pCodeNo)
         ->get();

		return $lTblCodeInfo;
	}
}

