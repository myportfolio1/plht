<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class tngrebnm extends Model
{
    protected $table = 'tngrebnm';

	public function getOffsetNGBunruiList()
	{
		$lTblOffsetNGBunrui = self::where('DELETE_FLG', '0')
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['OFFSET_NG_BUNRUI_ID', 'OFFSET_NG_BUNRUI_NAME']);

		$lArrDataOffsetNGBunruiList = [ "" => "" ];

		foreach ($lTblOffsetNGBunrui as $lRowOffsetNGBunruiList)
		{
			$lArrDataOffsetNGBunruiList += [
				$lRowOffsetNGBunruiList["OFFSET_NG_BUNRUI_ID"] => $lRowOffsetNGBunruiList["OFFSET_NG_BUNRUI_NAME"]
			];
		}

		return $lArrDataOffsetNGBunruiList;
	}




}

