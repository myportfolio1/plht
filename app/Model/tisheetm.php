<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Log;

class tisheetm extends Model
{
	protected $table = 'tisheetm';

	public function getCheckSheetNoList1($pCustomerId)
	{
		$lRowCountCheckSheetNo = 0;

		$lTblCheckSheetNo = self::where([
										['DELETE_FLG', '=', '0'],
										['CUSTOMER_ID', '=', $pCustomerId],
										])
								->groupBy('INSPECTION_SHEET_NO')
								// ->orderBy('DISPLAY_ORDER', 'desc')
								->get(['INSPECTION_SHEET_NO', 'INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME']);

		if ($lTblCheckSheetNo != null)
		{
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
			{
				$lRowCountCheckSheetNo = $lRowCountCheckSheetNo + 1;
			}

			if ($lRowCountCheckSheetNo != 1)
			{
				$lArrCheckSheetNo = [ "" => "" ];

				foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
				{
					$lArrCheckSheetNo += [
						$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
					];
				}
			}
			else
			{
				foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
				{
					$lArrCheckSheetNo = [
						$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
					];
				}
			}
		}
		else
		{
			$lArrCheckSheetNo = [ "" => "" ];
		}

		return $lArrCheckSheetNo;
	}


	public function getCheckSheetNoList2($pProcessId, $pCustomerId)
	{
		$lRowCountCheckSheetNo = 0;

		$lTblCheckSheetNo = self::where([
										['DELETE_FLG', '=', '0'],
										['PROCESS_ID', '=', $pProcessId],
										['CUSTOMER_ID', '=', $pCustomerId],
										])
								->groupBy('INSPECTION_SHEET_NO')
								// ->orderBy('DISPLAY_ORDER', 'desc')
								->get(['INSPECTION_SHEET_NO', 'INSPECTION_SHEET_NO AS INSPECTION_SHEET_NAME']);

		if ($lTblCheckSheetNo != null)
		{
			foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
			{
				$lRowCountCheckSheetNo = $lRowCountCheckSheetNo + 1;
			}

			if ($lRowCountCheckSheetNo != 1)
			{
				$lArrCheckSheetNo = [ "" => "" ];

				foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
				{
					$lArrCheckSheetNo += [
						$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
					];
				}
			}
			else
			{
				foreach ($lTblCheckSheetNo as $lRowCheckSheetNo)
				{
					$lArrCheckSheetNo = [
						$lRowCheckSheetNo["INSPECTION_SHEET_NO"] => $lRowCheckSheetNo["INSPECTION_SHEET_NAME"]
					];
				}
			}
		}
		else
		{
			$lArrCheckSheetNo = [ "" => "" ];
		}

		return $lArrCheckSheetNo;
	}

	public function getRevNoList($pProcessId, $pCustomerId, $pInspectionSheetNo)
	{
		$lRowCountRevNo		= 0;

		$lTblRevNo = self::where([
									['DELETE_FLG', '=', '0'],
									['PROCESS_ID', '=', $pProcessId],
									['CUSTOMER_ID', '=', $pCustomerId],
									['INSPECTION_SHEET_NO', '=', $pInspectionSheetNo],
								])
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['REV_NO', 'REV_NO AS REV_NO_NAME']);

		if ($lTblRevNo != null)
		{
			foreach ($lTblRevNo as $lRowRevNo)
			{
				$lRowCountRevNo = $lRowCountRevNo + 1;
			}

			if ($lRowCountRevNo != 1)
			{
				$lArrRevNo = [ "" => "" ];

				foreach ($lTblRevNo as $lRowRevNo)
				{
					$lArrRevNo += [
						$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
					];
				}
			}
			else
			{
				foreach ($lTblRevNo as $lRowRevNo)
				{
					$lArrRevNo = [
						$lRowRevNo["REV_NO"] => $lRowRevNo["REV_NO_NAME"]
					];
				}
			}
		}
		else
		{
			$lArrRevNo = [ "" => "" ];
		}

		return $lArrRevNo;
	}






}

