<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tinsptim extends Model
{
	protected $table = 'tinsptim';

	public function getInspectionTimeList()
	{
		$lTblInspectionTime = self::where('DELETE_FLG', '0')
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['INSPECTION_TIME_ID', 'INSPECTION_TIME_NAME']);

		$lArrDataListInspectionTime = [ "" => "" ];

		foreach ($lTblInspectionTime as $lRowInspectionTime)
		{
			$lArrDataListInspectionTime += [
				$lRowInspectionTime["INSPECTION_TIME_ID"] => $lRowInspectionTime["INSPECTION_TIME_NAME"]
			];
		}

		return $lArrDataListInspectionTime;
	}


	public function getTimeInfo($pTimeId)
	{
		$lTblTimeInfo = [];

        $lTblTimeInfo = DB::table('TINSPTIM')
         ->select('INSPECTION_TIME_ID', 'INSPECTION_TIME_NAME', 'INSPECTION_TIME_NAME_FORM')
         ->where('INSPECTION_TIME_ID', '=', $pTimeId)
         ->where('DELETE_FLG', '=', '0')
         ->get();

		return $lTblTimeInfo;
	}




}

