<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class treshedt extends Model
{
	protected $table = 'treshedt';

	public function getHeaderData($pInspResultNo, $pImportDataRev)
	{
		$lTblData = [];

        $lTblData = DB::table('TRESHEDT')
         ->select('INSPECTION_SHEET_NO', 'REV_NO', 'PROCESS_ID', 'INSPECTION_YMD', 'INSPECTION_TIME_ID', 'CONDITION_CD', 'MOLD_NO')
         ->where('INSPECTION_RESULT_NO', '=', $pInspResultNo)
         ->where('DATA_REV', '=', $pImportDataRev)
         ->get();

		return $lTblData;
	}

}

