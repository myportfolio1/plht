<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ttecusrt extends Model
{
    protected $table = 'ttecusrt';

	public function getTecReqPersonList()
	{
		$lTblTecReqPerson = self::where('DELETE_FLG', '0')
									->orderBy('DISPLAY_ORDER', 'asc')
									->get(['TECHNICIAN_USER_ID', 'TECHNICIAN_USER_NAME']);

		$lArrDataTecReqPersonList = [ "" => "" ];

		foreach ($lTblTecReqPerson as $lRowTecReqPersonList)
		{
			$lArrDataTecReqPersonList += [
				$lRowTecReqPersonList["TECHNICIAN_USER_ID"] => $lRowTecReqPersonList["TECHNICIAN_USER_NAME"]
			];
		}

		return $lArrDataTecReqPersonList;
	}




}
