<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tinspnom extends Model
{
	protected $table = 'tinspnom';

	public function getInspNoData($pInspSheetNo, $pRevNo, $pInspPoint)
	{
		$lTblData = [];

		Log::write('info', 'getInspNoData',
			[
				"pInspSheetNo" => $pInspSheetNo,
				"pRevNo" => $pRevNo,
				"pInspPoint" => $pInspPoint,
			]

		);

        $lTblData = DB::table('TINSPNOM')
         ->select('THRESHOLD_NG_UNDER', 'THRESHOLD_OFFSET_UNDER', 'THRESHOLD_OFFSET_OVER', 'THRESHOLD_NG_OVER', 'PICTURE_URL')
         ->where('INSPECTION_SHEET_NO', '=', $pInspSheetNo)
         ->where('REV_NO', '=', $pRevNo)
         ->where('INSPECTION_POINT', '=', $pInspPoint)
         ->get();

		return $lTblData;
	}

}

