<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class tactionm extends Model
{
    protected $table = 'tactionm';

	public function getOffsetNGActionDetailList()
	{
		$lTblOffsetNGActionDetail = self::where('DELETE_FLG', '0')
										->orderBy('DISPLAY_ORDER', 'asc')
										->get(['ACTION_DETAILS_ID AS ACTION_DETAIL', 'ACTION_DETAILS_NAME AS ACTION_DETAILS_NAME']);

		$lArrDataOffsetNGActionDetailList = [ "" => "" ];

		foreach ($lTblOffsetNGActionDetail as $lRowOffsetNGActionDetailList)
		{
			$lArrDataOffsetNGActionDetailList += [
				$lRowOffsetNGActionDetailList["ACTION_DETAIL"] => $lRowOffsetNGActionDetailList["ACTION_DETAILS_NAME"]
			];
		}

		return $lArrDataOffsetNGActionDetailList;
	}




}
