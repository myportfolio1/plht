<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class ttimedett extends Model
{
    protected $table = 'ttimedett';

	public function getTimedetailData($pCodeNo, $pTimeId)
	{
		$lTblInfo = [];

        $lTblInfo = DB::table('TTIMEDETT')
         ->select('TINSPTIM_CODE_NO', 'INSPECTION_TIME_ID', 'INPUTNUMBER_JUDGE_FLG')
         ->where('TINSPTIM_CODE_NO', '=', $pCodeNo)
         ->where('INSPECTION_TIME_ID', '=', $pTimeId)
         ->where('DELETE_FLG', '=', '0')
         ->get();

		return $lTblInfo;
	}




}

