<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class ImportfileModel
{
	public $InspectionResultValue = "";
	public $InspectionResultNo = "";
	public $InspectionNo = "";
	public $InspectionPoint = "";

	public function ReadLine($pArray)
	{
		$InspectionResultValue = $pArray[6];
		$InspectionPoint       = $pArray[10];
		$InspectionResultNo    = $pArray[11];
		$InspectionNo          = $pArray[12];

Log::write('info', 'ReadLine',
			[
				"InspectionResultValue" => $InspectionResultValue,
				"InspectionPoint" => $InspectionPoint,
				"InspectionResultNo" => $InspectionResultNo,
				"InspectionNo" => $InspectionNo,
			]
		);


		$pArrayData = []; 
		$pArrayData += [
			"InspectionResultValue"  => $InspectionResultValue,
			"InspectionPoint"        => $InspectionPoint,
			"InspectionResultNo"     => $InspectionResultNo,
			"InspectionNo"           => $InspectionNo,
		];

		return $pArrayData;
	}

}

