<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tccientmst extends Model
{
	protected $table = 'tccientmst';

	public function getCoefficientData($pArgument1, $pArgument2)
	{
		$lTblCoefficient = [];

        $lTblCoefficient = DB::table('TCCIENTMST')
         ->select('COEFFICIENT')
         ->where('COEFF_KIND', '=', $pArgument1)
         ->where('COEFF_NUM', '=', $pArgument2)
         ->get();

		return $lTblCoefficient;
	}

}

