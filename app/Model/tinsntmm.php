<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tinsntmm extends Model
{
	protected $table = 'tinsntmm';

	public function getInspectionNoTimeData($pSheetNo, $pTimeId)
	{
		$lTblInspectionNoTimeInfo = [];

        $lTblInspectionNoTimeInfo = DB::table('TINSNTMM')
         ->select('INSPECTION_SHEET_NO', 'INSPECTION_POINT', 'INSPECTION_TIME_ID')
         ->where('INSPECTION_SHEET_NO', '=', $pSheetNo)
         ->where('INSPECTION_TIME_ID', '=', $pTimeId)
         ->where('DELETE_FLG', '=', '0')
         ->get();

		return $lTblInspectionNoTimeInfo;
	}

}

