<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Log;

class tngresnm extends Model
{
    protected $table = 'tngresnm';

	public function getOffsetNGReasonList($pOffsetNGBunrui)
	{
		$lTblOffsetNGReason = self::where([
											['DELETE_FLG', '=', '0'],
											['OFFSET_NG_BUNRUI_ID', '=', $pOffsetNGBunrui],
											])
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['OFFSET_NG_REASON_ID', 'OFFSET_NG_REASON']);

		$lArrOffsetNGReasonList = [ "" => "" ];

		foreach ($lTblOffsetNGReason as $lRowOffsetNGReasonList)
		{
			$lArrOffsetNGReasonList += [
				$lRowOffsetNGReasonList["OFFSET_NG_REASON_ID"] => $lRowOffsetNGReasonList["OFFSET_NG_REASON"]
			];
		}


		return $lArrOffsetNGReasonList;
	}




}

