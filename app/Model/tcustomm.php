<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class tcustomm extends Model
{
	protected $table = 'tcustomm';

	public function getCustomerList()
	{
		$lTblCustomerList = self::where('DELETE_FLG', '0')
								->orderBy('DISPLAY_ORDER', 'asc')
								->get(['CUSTOMER_ID', 'CUSTOMER_NAME']);

		// echo "<pre>";
		// print_r($lTblCustomerList);
		// echo "</pre>";

		$lArrDataCustomerList = [ "" => "" ];

		foreach ($lTblCustomerList as $lRowCustomerList)
		{
			$lArrDataCustomerList += [
				$lRowCustomerList["CUSTOMER_ID"] => $lRowCustomerList["CUSTOMER_NAME"]
			];
		}

		// echo "<pre>";
		// print_r($lArrDataCustomerList);
		// echo "</pre>";

		return $lArrDataCustomerList;
	}







}

