<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class tusermst extends Model
{
	protected $table = 'tusermst';

	public function getInspectorData($pInspectorCode)
	{
		$lTblInspectorInfo = [];

        $lTblInspectorInfo = DB::table('TUSERMST')
         ->select('USER_NAME', 'TEAM_ID')
         ->where('USER_ID', '=', $pInspectorCode)
         ->where('DELETE_FLG', '=', '0')
         ->get();

		return $lTblInspectorInfo;
	}

}

