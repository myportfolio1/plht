<?php

namespace App\Csv;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class InspectionResultCsv
{
	public function getCsvData($pCsvArrayData)
	{
		Log::write('info', 'Start CSV');

		$lOutputCSVData = [];

		//search data for CSV
		$lTblInspectionResultCSV = $this->getInspectionResultCSV($pCsvArrayData);

		if (count($lTblInspectionResultCSV) == 0)
		{
			$lOutputCSVData = null;
		}
		else
		{
			$lTblInspectionResultCSV = (Array)$lTblInspectionResultCSV;
			$lOutputCSVData = $this->createInspectionResultCSV($lTblInspectionResultCSV);
		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process            getInspectionResultCSV
	//**************************************************************************
	private function getInspectionResultCSV($pCsvArrayData)
	{
		$lTblInspectionResultCSV = [];

			$lTblInspectionResultCSV = DB::select('
			         SELECT REHE.INSPECTION_RESULT_NO
							,REHE.INSPECTION_SHEET_NO
							,SHEE.INSPECTION_SHEET_NAME
							,REHE.REV_NO
							,PROCESS.CODE_NAME AS PROCESS_NAME
							,CUST.CUSTOMER_NAME
							,SHEE.MODEL_NAME
							,SHEE.ITEM_NO
							,ITEM.ITEM_NAME
							,SHEE.DRAWING_NO
							,SHEE.ISO_KANRI_NO
							,REHE.MACHINE_NO
							,MACH.MACHINE_NAME
							,REHE.MOLD_NO
							,REHE.MATERIAL_NAME
							,REHE.MATERIAL_SIZE
							,REHE.HEAT_TREATMENT_TYPE
							,REHE.PLATING_KIND
							,COLOR.CODE_NAME AS COLOR_NAME
							,REHE.INSERT_USER_ID
							,USER1.USER_NAME AS INSERT_USER_NAME
							,DATE_FORMAT(REHE.INSPECTION_YMD,"%d-%m-%Y") AS INSPECTION_YMD
							,COND.CODE_NAME AS CONDITION_NAME
							,TIME.INSPECTION_TIME_NAME
							,REHE.MATERIAL_LOT_NO	
							,REHE.CERTIFICATE_NO
							,REHE.PO_NO
							,REHE.INVOICE_NO
							,REHE.QUANTITY_COIL
							,REHE.QUANTITY_WEIGHT
							,REHE.LOT_NO
							,DATE_FORMAT(REHE.PRODUCTION_YMD,"%d-%m-%Y") AS PRODUCTION_YMD
							,REDE.INSPECTION_NO
							,REDE.INSPECTION_POINT
							,REDE.ANALYTICAL_GRP_01
							,REDE.ANALYTICAL_GRP_02
							,REDE.SAMPLE_NO
							,REDE.DISPLAY_ORDER
							,ISNO.INSPECTION_ITEM
							,SOKT.TOOL_CD
							,ISNO.SOKUTEIKI_CD
							,ifnull(ISNO.THRESHOLD_NG_UNDER,"") AS THRESHOLD_NG_UNDER
							,ifnull(ISNO.THRESHOLD_OFFSET_UNDER,"") AS THRESHOLD_OFFSET_UNDER
							,ifnull(ISNO.REFERENCE_VALUE,"") AS REFERENCE_VALUE
							,ifnull(ISNO.THRESHOLD_OFFSET_OVER,"") AS THRESHOLD_OFFSET_OVER
							,ifnull(ISNO.THRESHOLD_NG_OVER,"") AS THRESHOLD_NG_OVER
							,REDE.INSPECTION_RESULT_VALUE
							,CASE
								WHEN REDE.INSPECTION_RESULT_TYPE = "1" THEN "OK"
								WHEN REDE.INSPECTION_RESULT_TYPE = "2" THEN "OFFSET"
								WHEN REDE.INSPECTION_RESULT_TYPE = "3" THEN "NG"
								WHEN REDE.INSPECTION_RESULT_TYPE = "4" THEN "No Check"
								WHEN REDE.INSPECTION_RESULT_TYPE = "5" THEN "Line Stop"
								ELSE ""
							END INSPECTION_RESULT_TYPE
							,NGBUN.OFFSET_NG_BUNRUI_NAME
							,NGRES.OFFSET_NG_REASON AS NGRES_OFFSET_NG_REASON
							,REDE.OFFSET_NG_REASON
							,CASE
								WHEN REDE.OFFSET_NG_ACTION = "1" THEN "Running"
								WHEN REDE.OFFSET_NG_ACTION = "2" THEN "Stop Line"
								ELSE ""
							END OFFSET_NG_ACTION
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_INTERNAL = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_INTERNAL
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_REQUEST = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_REQUEST
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_TSR = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_TSR
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_SORTING = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_SORTING
							,CASE
								WHEN REDE.OFFSET_NG_ACTION_DTL_NOACTION = "1" THEN "YES"
								ELSE ""
							END OFFSET_NG_ACTION_DTL_NOACTION
							,ACTIO.ACTION_DETAILS_NAME
							,REDE.ACTION_DETAILS_TR_NO_TSR_NO
							,REDE.TR_NO
							,TRPER.TECHNICIAN_USER_NAME
							,REDE.TSR_NO
							,ANA.CALC_NUM
							,ANA.MAX_VALUE
							,ANA.MIN_VALUE
							,ANA.AVERAGE_VALUE
							,ANA.RANGE_VALUE
							,ANA.STDEV
							,ANA.CPK
							,ANA.XBAR_UCL
							,ANA.XBAR_LCL
							,ANA.RCHART_UCL
							,ANA.RCHART_LCL
							,REASON.CODE_NAME AS JUDGE_REASON
							,DATE_FORMAT(REDE.INSERT_YMDHMS,"%d-%m-%Y %k:%i:%s") AS INSERT_YMDHMS
							,REDE.LAST_UPDATE_USER_ID
							,USER2.USER_NAME AS UPDATE_USER_NAME
							,DATE_FORMAT(REDE.UPDATE_YMDHMS,"%d-%m-%Y %k:%i:%s") AS UPDATE_YMDHMS
						FROM TRESHEDT AS REHE

					INNER JOIN TRESDETT AS REDE
							ON REDE.INSPECTION_RESULT_NO = REHE.INSPECTION_RESULT_NO
			   LEFT OUTER JOIN TRESANAL AS ANA
							-- ON ANA.INSPECTION_RESULT_NO  = REDE.INSPECTION_RESULT_NO
							ON ANA.INSPECTION_SHEET_NO   = REHE.INSPECTION_SHEET_NO
						   AND ANA.REV_NO                = REHE.REV_NO
						   AND ANA.INSPECTION_POINT      = REDE.INSPECTION_POINT
						   AND ANA.ANALYTICAL_GRP_01     = REDE.ANALYTICAL_GRP_01
						   AND ANA.ANALYTICAL_GRP_02     = REDE.ANALYTICAL_GRP_02
						   AND ANA.INSPECTION_RESULT_NO  = REHE.INSPECTION_RESULT_NO
					INNER JOIN TISHEETM AS SHEE
							ON SHEE.INSPECTION_SHEET_NO  = REHE.INSPECTION_SHEET_NO
						   AND SHEE.REV_NO               = REHE.REV_NO
						   AND SHEE.DELETE_FLG           = "0"
					INNER JOIN TINSPNOM AS ISNO
							ON ISNO.INSPECTION_SHEET_NO  = REHE.INSPECTION_SHEET_NO
						   AND ISNO.REV_NO               = REHE.REV_NO
						   AND ISNO.INSPECTION_POINT     = REDE.INSPECTION_POINT
						   AND ISNO.DELETE_FLG           = "0"
			   LEFT OUTER JOIN TITEMMST AS ITEM
							ON ITEM.ITEM_NO              = SHEE.ITEM_NO
						   AND ITEM.DELETE_FLG           = "0"

					INNER JOIN TUSERMST AS USER1
							ON USER1.USER_ID             = REHE.INSERT_USER_ID
						   AND USER1.DELETE_FLG          = "0"
					INNER JOIN TUSERMST AS USER2
							ON USER2.USER_ID             = REDE.LAST_UPDATE_USER_ID
						   AND USER2.DELETE_FLG          = "0"
					INNER JOIN TINSPTIM AS TIME
							ON TIME.INSPECTION_TIME_ID   = REHE.INSPECTION_TIME_ID
						   AND TIME.DELETE_FLG           = "0"
					INNER JOIN TMACHINM AS MACH
							ON MACH.MACHINE_NO           = REHE.MACHINE_NO
						   AND MACH.DELETE_FLG           = "0"
					INNER JOIN TSOKTEIM AS SOKT
							ON SOKT.SOKUTEIKI_CD         = ISNO.SOKUTEIKI_CD
						   AND SOKT.DELETE_FLG           = "0"
					INNER JOIN TCUSTOMM AS CUST
							ON CUST.CUSTOMER_ID          = SHEE.CUSTOMER_ID
						   AND CUST.DELETE_FLG           = "0"
					INNER JOIN TCODEMST AS COND
							ON COND.CODE_CLASS           = "001"
						   AND COND.CODE_ORDER           = REHE.CONDITION_CD
					INNER JOIN TCODEMST AS PROCESS
							ON PROCESS.CODE_CLASS        = "002"
						   AND PROCESS.CODE_ORDER        = REHE.PROCESS_ID
			   LEFT OUTER JOIN TCODEMST AS REASON
							ON REASON.CODE_CLASS         = "003"
						   AND REASON.CODE_ORDER         = ANA.JUDGE_REASON
			   LEFT OUTER JOIN TCODEMST AS COLOR
							ON COLOR.CODE_CLASS          = "006"
						   AND COLOR.CODE_ORDER          = REHE.COLOR_ID
			   LEFT OUTER JOIN tngrebnm AS NGBUN
							ON NGBUN.OFFSET_NG_BUNRUI_ID = REDE.OFFSET_NG_BUNRUI_ID
			   LEFT OUTER JOIN tngresnm AS NGRES
							ON NGRES.OFFSET_NG_REASON_ID = REDE.OFFSET_NG_REASON_ID
						   AND NGRES.OFFSET_NG_BUNRUI_ID = REDE.OFFSET_NG_BUNRUI_ID
			   LEFT OUTER JOIN tactionm AS ACTIO
							ON ACTIO.ACTION_DETAILS_ID   = REDE.ACTION_DETAILS_ID
			   LEFT OUTER JOIN ttecusrt AS TRPER
							ON TRPER.TECHNICIAN_USER_ID  = REDE.TECHNICIAN_USER_ID

						WHERE REHE.PROCESS_ID        = IF(:ProcessId1 <> "", :ProcessId2, REHE.PROCESS_ID)
						AND SHEE.CUSTOMER_ID         = IF(:CustomerId1 <> "", :CustomerId2, SHEE.CUSTOMER_ID)
						AND REHE.INSPECTION_SHEET_NO LIKE :CheckSheetNo
						AND REHE.REV_NO              = IF(:RevisionNo1 <> "", :RevisionNo2, REHE.REV_NO)
						AND REHE.MACHINE_NO          = IF(:MachineNo1 <> "", :MachineNo2, REHE.MACHINE_NO)
						AND REHE.MOLD_NO             = IF(:MoldNo1 <> "", :MoldNo2, REHE.MOLD_NO)
						AND REHE.INSERT_USER_ID      = IF(:UserID1 <> "", :UserID2, REHE.INSERT_USER_ID)
						AND SHEE.ITEM_NO             LIKE :PartNo
						AND REHE.PRODUCTION_YMD      >= IF((:ProdDateFrom1 is not null) and (:ProdDateFrom2 <> ""), :ProdDateFrom3, REHE.PRODUCTION_YMD)
						AND REHE.PRODUCTION_YMD      <= IF((:ProdDateTo1 is not null) and (:ProdDateTo2 <> ""), :ProdDateTo3, REHE.PRODUCTION_YMD)
						AND REHE.INSPECTION_YMD      >= IF((:InspDateFrom1 is not null) and (:InspDateFrom2 <> ""), :InspDateFrom3, REHE.INSPECTION_YMD)
						AND REHE.INSPECTION_YMD      <= IF((:InspDateTo1 is not null) and (:InspDateTo2 <> ""), :InspDateTo3, REHE.INSPECTION_YMD)
						AND REHE.INSPECTION_TIME_ID  = IF((:InspTime1 is not null) and (:InspTime2 <> ""), :InspTime3, REHE.INSPECTION_TIME_ID)

					ORDER BY REHE.INSPECTION_SHEET_NO
							,REHE.REV_NO
							,REHE.PROCESS_ID
							,REHE.PRODUCTION_YMD
							,REHE.INSPECTION_YMD
							,TIME.DISPLAY_ORDER
							,TIME.INSPECTION_TIME_ID
							,REHE.CONDITION_CD
							,USER1.DISPLAY_ORDER
							,REHE.INSERT_USER_ID
							,REDE.DISPLAY_ORDER
							,REDE.INSPECTION_NO
			',
				[
					"ProcessId1"          => $pCsvArrayData["ProcessId"],
					"ProcessId2"          => $pCsvArrayData["ProcessId"],
					"CustomerId1"         => $pCsvArrayData["CustomerId"],
					"CustomerId2"         => $pCsvArrayData["CustomerId"],
					"CheckSheetNo"        => $pCsvArrayData["CheckSheetNo"],
					"RevisionNo1"         => $pCsvArrayData["RevisionNo"],
					"RevisionNo2"         => $pCsvArrayData["RevisionNo"],
					"MachineNo1"          => $pCsvArrayData["MachineNo"],
					"MachineNo2"          => $pCsvArrayData["MachineNo"],
					"MoldNo1"             => $pCsvArrayData["MoldNo"],
					"MoldNo2"             => $pCsvArrayData["MoldNo"],
					"UserID1"             => $pCsvArrayData["InspectorCode"],
					"UserID2"             => $pCsvArrayData["InspectorCode"],
					"PartNo"              => $pCsvArrayData["PartNo"],
					"ProdDateFrom1"       => $pCsvArrayData["ProductionDateFrom"],
					"ProdDateFrom2"       => $pCsvArrayData["ProductionDateFrom"],
					"ProdDateFrom3"       => $pCsvArrayData["ProductionDateFrom"],
					"ProdDateTo1"         => $pCsvArrayData["ProductionDateTo"],
					"ProdDateTo2"         => $pCsvArrayData["ProductionDateTo"],
					"ProdDateTo3"         => $pCsvArrayData["ProductionDateTo"],
					"InspDateFrom1"       => $pCsvArrayData["InspectionDateFrom"],
					"InspDateFrom2"       => $pCsvArrayData["InspectionDateFrom"],
					"InspDateFrom3"       => $pCsvArrayData["InspectionDateFrom"],
					"InspDateTo1"         => $pCsvArrayData["InspectionDateTo"],
					"InspDateTo2"         => $pCsvArrayData["InspectionDateTo"],
					"InspDateTo3"         => $pCsvArrayData["InspectionDateTo"],
					"InspTime1"           => $pCsvArrayData["InspectionTimeId"],
					"InspTime2"           => $pCsvArrayData["InspectionTimeId"],
					"InspTime3"           => $pCsvArrayData["InspectionTimeId"],
				]
			);

		return $lTblInspectionResultCSV;
	}

	//**************************************************************************
	// process            createInspectionResultCSV
	//**************************************************************************
	private function createInspectionResultCSV($pTblInspectionResultCSV)
	{
		$lOutputCSVData          = "";
		$lRowInspectionResultCSV = [];

		//header
		$lOutputCSVData .= "Inspection Result Seq.No.,";
		$lOutputCSVData .= "Inspection(Check) Sheet No.,";
		$lOutputCSVData .= "Check Sheet Title,";
		$lOutputCSVData .= "Revision No.,";
		$lOutputCSVData .= "Process,";
		$lOutputCSVData .= "Customer Name,";
		$lOutputCSVData .= "Model Name,";
		$lOutputCSVData .= "Parts No.,";
		$lOutputCSVData .= "Parts Name,";
		$lOutputCSVData .= "Drawing No.,";
		$lOutputCSVData .= "ISO No.,";
		$lOutputCSVData .= "Machine No.,";
		$lOutputCSVData .= "Machine Name,";
		$lOutputCSVData .= "Mold No.,";
		$lOutputCSVData .= "Material Name,";
		$lOutputCSVData .= "Material Size,";
		$lOutputCSVData .= "Heat Treatment Type,";
		$lOutputCSVData .= "Plating Kind,";
		$lOutputCSVData .= "Color,";
		$lOutputCSVData .= "Inspecter ID,";
		$lOutputCSVData .= "Inspector Name,";
		$lOutputCSVData .= "Inspection Date,";
		$lOutputCSVData .= "Condition,";
		$lOutputCSVData .= "Inspection Time,";
		$lOutputCSVData .= "Material Lot No.,";
		$lOutputCSVData .= "Certificate No.,";
		$lOutputCSVData .= "PO No.,";
		$lOutputCSVData .= "Invoice No.,";
		$lOutputCSVData .= "Quantity Coil,";
		$lOutputCSVData .= "Quantity Weight,";
		$lOutputCSVData .= "Lot No.,";
		$lOutputCSVData .= "Production Date,";
		$lOutputCSVData .= "Inspection(Check) No.,";
		$lOutputCSVData .= "Inspection Point,";
		$lOutputCSVData .= "Cavity,";
		$lOutputCSVData .= "Where of Cavity,";
		$lOutputCSVData .= "Sample No.,";
		$lOutputCSVData .= "Inspection Order,";
		$lOutputCSVData .= "Inspection Items,";
		$lOutputCSVData .= "Tool Code,";
		$lOutputCSVData .= "Equipment,";
		$lOutputCSVData .= "NG Under,";
		$lOutputCSVData .= "OFFSET Under,";
		$lOutputCSVData .= "Center,";
		$lOutputCSVData .= "OFFSET Over,";
		$lOutputCSVData .= "NG Over,";
		$lOutputCSVData .= "Inspection Result,";
		$lOutputCSVData .= "Result Class,";
		$lOutputCSVData .= "OFFSET / NG Reason 1,";
		$lOutputCSVData .= "OFFSET / NG Reason 2,";
		$lOutputCSVData .= "OFFSET Reason / NG Reason,";
		$lOutputCSVData .= "OFFSET Action / NG Action,";
		$lOutputCSVData .= "Internal Approve (Check Box),";
		$lOutputCSVData .= "Request Offset (Check Box),";
		$lOutputCSVData .= "Doc (Check Box),";
		$lOutputCSVData .= "Sorting (Check Box),";
		$lOutputCSVData .= "No Action (Check Box),";
		$lOutputCSVData .= "Action Details,";
		$lOutputCSVData .= "Action Details / Technician Request No. / Doc No.,";
		$lOutputCSVData .= "TR No.,";
		$lOutputCSVData .= "Technician User Name,";
		$lOutputCSVData .= "Doc No.,";
		$lOutputCSVData .= "Calc Qty,";
		$lOutputCSVData .= "Max Value,";
		$lOutputCSVData .= "Min Value,";
		$lOutputCSVData .= "Average Value,";
		$lOutputCSVData .= "Range Value,";
		$lOutputCSVData .= "STDEV,";
		$lOutputCSVData .= "CPK,";
		$lOutputCSVData .= "UCL(Xbar),";
		$lOutputCSVData .= "LCL(Xbar),";
		$lOutputCSVData .= "UCL(Rchart),";
		$lOutputCSVData .= "LCL(Rchart),";
		$lOutputCSVData .= "Judge Reason,";
		$lOutputCSVData .= "Entry Date/Time,";
		$lOutputCSVData .= "Modify User ID,";
		$lOutputCSVData .= "Modify User Name,";
		$lOutputCSVData .= "Modify Date/Time,";
		$lOutputCSVData .= "\n";

		foreach ($pTblInspectionResultCSV as $lRowInspectionResultCSV)
		{
			$lRowInspectionResultCSV = (Array)$lRowInspectionResultCSV;

			//string concatenation while using treatment function
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_SHEET_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REV_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PROCESS_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CUSTOMER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MODEL_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ITEM_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["DRAWING_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ISO_KANRI_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MACHINE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MACHINE_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MOLD_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_SIZE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["HEAT_TREATMENT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PLATING_KIND"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["COLOR_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CONDITION_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_TIME_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MATERIAL_LOT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CERTIFICATE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PO_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INVOICE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["QUANTITY_COIL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["QUANTITY_WEIGHT"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["LOT_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["PRODUCTION_YMD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_POINT"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ANALYTICAL_GRP_01"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ANALYTICAL_GRP_02"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["SAMPLE_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["DISPLAY_ORDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_ITEM"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TOOL_CD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["SOKUTEIKI_CD"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_UNDER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["REFERENCE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_OFFSET_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["THRESHOLD_NG_OVER"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSPECTION_RESULT_TYPE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_BUNRUI_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["NGRES_OFFSET_NG_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_INTERNAL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_REQUEST"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_TSR"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_SORTING"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["OFFSET_NG_ACTION_DTL_NOACTION"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ACTION_DETAILS_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["ACTION_DETAILS_TR_NO_TSR_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TR_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TECHNICIAN_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["TSR_NO"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CALC_NUM"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MAX_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["MIN_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["AVERAGE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RANGE_VALUE"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["STDEV"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["CPK"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["XBAR_UCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["XBAR_LCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RCHART_UCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["RCHART_LCL"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["JUDGE_REASON"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["INSERT_YMDHMS"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["LAST_UPDATE_USER_ID"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_USER_NAME"]);
			$lOutputCSVData.= $this->createCSVValue($lRowInspectionResultCSV["UPDATE_YMDHMS"]);
			//newline
			$lOutputCSVData.= "\n";
		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process            createCSVValue
	// overview           treat value of CSV（escape value including "）
	// overview           
	// argument           
	// return value       
	// author             s-miyamoto
	// record of updates  No,1 
	//                    No,2 
	// Remarks            
	//**************************************************************************
	private function createCSVValue($pTargetValue)
	{
		//「"」->「""」「",」
		$pTargetValue = "\"".str_replace("\"", "\"\"", $pTargetValue)."\",";

		return $pTargetValue;

	}


}
