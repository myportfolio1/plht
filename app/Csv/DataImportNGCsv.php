<?php

namespace App\Csv;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use DB;
use Log;

class DataImportNGCsv
{
	public function createCSV($pArrayNGData)
	{
		Log::write('info', 'Start Data Import NG CSV');

		$lOutputCSVData = "";
		$lRow = [];

		//header
		$lOutputCSVData .= "InspectionResultNo,";
		$lOutputCSVData .= "InspectionNo,";
		$lOutputCSVData .= "InspectionPoint,";
		$lOutputCSVData .= "InspectionResultValue,";
		$lOutputCSVData .= "ImportNGreason,";
		$lOutputCSVData .= "\n";

		foreach ($pArrayNGData as $lRow)
		{
			$lRow = (Array)$lRow;

			//string concatenation while using treatment function
			$lOutputCSVData.= $this->createCSVValue($lRow["InspectionResultNo"]);
			$lOutputCSVData.= $this->createCSVValue($lRow["InspectionNo"]);
			$lOutputCSVData.= $this->createCSVValue($lRow["InspectionPoint"]);
			$lOutputCSVData.= $this->createCSVValue($lRow["InspectionResultValue"]);
			$lOutputCSVData.= $this->createCSVValue($lRow["ImportNGreason"]);
			//newline
			$lOutputCSVData.= "\n";
		}

		return $lOutputCSVData;
	}

	//**************************************************************************
	// process            createCSVValue
	//**************************************************************************
	private function createCSVValue($pTargetValue)
	{
		//「"」->「""」「",」
		$pTargetValue = "\"".str_replace("\"", "\"\"", $pTargetValue)."\",";

		return $pTargetValue;

	}


}
