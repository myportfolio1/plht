<?php

namespace App\Parts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Model\tccientmst;

use DB;
use Log;

class calculate
{
	//Result Class
	CONST RESULT_CLASS_OK            = "1"; //OK
	CONST RESULT_CLASS_OFFSET        = "2"; //OFFSET
	CONST RESULT_CLASS_NG            = "3"; //NG
	CONST RESULT_CLASS_NO_CHECK      = "4"; //NO CHECK
	CONST RESULT_CLASS_LINE_STOP     = "5"; //LINE STOP

	//Result Input Type
	CONST RESULT_INPUT_TYPE_NUMBER   = "1"; //NUMBER(TEXT)
	CONST RESULT_INPUT_TYPE_OKNG     = "2"; //OK/NG(RadioButton)

	//XBAR (UCL&LCL)
	CONST COEFFICIENT_XBAR           = "A2";
	//RCHAT_UCL
	CONST COEFFICIENT_RCHAT_UCL      = "D4";
	//RCHAT_LCL
	CONST COEFFICIENT_RCHAT_LCL      = "D3";
	

	public function getCalculate($pCalculateArray, $pInspnResultInputType, $pResultClass)
	{
		// Log::write('info', 'Start getCalculate');

		$lOutputData = [];

		$lOutputData = $this->Calculate($pCalculateArray, $pInspnResultInputType, $pResultClass);

		Log::write('info', 'getCalculate',
			[
				"OutputData" => $lOutputData,
			]
		);


		if (count($lOutputData) == 0)
		{
			$lOutputData = null;
		}

		return $lOutputData;
	}

	//**************************************************************************
	// process            Calculate
	// Remark             12-Jun-2018 Hoshina
	//                    number_format is done only on screen display 
	//                    When calculate, DB registration, do nothing
	//**************************************************************************
	private function Calculate($pCalculateArray, $pInspnResultInputType, $pResultClass)
	{
		Log::write('info', 'start Calculate',
			[
				"CalculateArray"       => $pCalculateArray,
				"InspnResultInputType" => $pInspnResultInputType,
				"ResultClass"          => $pResultClass,
			]
		);

		$lCalcFlg                                = false;
		$lBeforeCalcFlg                          = false;
		$lWorkInspectionResult                   = 0; 
		$lWorkMaxValue                           = 0;
		$lWorkMinValue                           = 0;
		$lCalcNam                                = 0;
		$lCodeNum                                = 0;
		$lMaxValue                               = 0;
		$lMinValue                               = 0;
		$lAverageValue                           = 0;
		$lRangeValue                             = 0;
		$lStdev                                  = 0;
		$lWorkCpkStdSpecOver                     = 0;
		$lWorkCpkStdSpecUnder                    = 0;
		$lCpk                                    = 0;
		$lXbarUcl                                = 0;
		$lXbarLcl                                = 0;
		$lRchartUcl                              = 0;
		$lRchartLcl                              = 0;
		// $lJudgeReason                            = "";
		$lJudgeMin                               = 0;
		$lJudgeMax                               = 0;

		//Modify 07-Jun-2018 Hoshina Because number_format is Last only
		// $lWorkInspectionResult = number_format($pCalculateArray["InspectionResultValue"],4);
		$lWorkInspectionResult = $pCalculateArray["InspectionResultValue"];

		$pArray= [];


		$lTblBeforeCommitInspectionResultDetail1 = $this->getBeforeCommitInspectionResultDetailData1($pCalculateArray["InspectionResultNo"], $pCalculateArray["InspectionNo"]);
		$lRowBeforeCommitInspectionResultDetail1 = (Array)$lTblBeforeCommitInspectionResultDetail1[0];

		$lTblBeforeCommitInspectionResultDetail2 = $this->getBeforeCommitInspectionResultDetailData2($pCalculateArray["InspectionResultNo"], $pCalculateArray["InspectionPoint"], $pCalculateArray["AnalGrp01"], $pCalculateArray["AnalGrp02"]);
		$lRowBeforeCommitInspectionResultDetail2 = (Array)$lTblBeforeCommitInspectionResultDetail2[0];

		$lTblBeforeCommitInspectionResultAnalyze1 = $this->getBeforeCommitInspectionResultAnalyzeData1($pCalculateArray["InspectionResultNo"], $pCalculateArray["InspectionPoint"], $pCalculateArray["AnalGrp01"], $pCalculateArray["AnalGrp02"]);




		if (count($lRowBeforeCommitInspectionResultDetail1) != 0)
		{
			if (($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] != null AND $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] != 0) AND ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OK OR $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_OFFSET OR $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_TYPE"] == self::RESULT_CLASS_NG))
			{
				$lBeforeCalcFlg = true;
			}
			else
			{
				$lBeforeCalcFlg = false;
			}
		}
		else
		{
			$lBeforeCalcFlg = false;
		}

		if ($pInspnResultInputType == self::RESULT_INPUT_TYPE_NUMBER)
		{
			if ($pResultClass == self::RESULT_CLASS_OK OR $pResultClass == self::RESULT_CLASS_OFFSET OR $pResultClass == self::RESULT_CLASS_NG)
			{
				$lCalcFlg = true;
			}
			else
			{
				$lCalcFlg = false;
			}
		}
		else
		{
			$lCalcFlg = false;
		}

		// Log::write('info', '1',
		// 	[
		// 		"INSPECTION_RESULT_VALUE" => $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"],
		// 		"BeforeCalcFlg" => $lBeforeCalcFlg,
		// 		"CalcFlg"       => $lCalcFlg,
		// 	]
		// );

		//default
		if ($lCalcFlg == true)
		{
			//Calculated Number (何個の値で計算したか)
			$lCalcNam =  1;
			//Max Value (最大値)
			$lMaxValue = $lWorkInspectionResult;
			//Min Value (最小値)
			$lMinValue = $lWorkInspectionResult;
			//Average Value (平均値)
			$lAverageValue = $lWorkInspectionResult;
			//Range Value (幅)
			$lRangeValue = 0;
			//Stdev (ｼｸﾞﾏσ)
			$lStdev = 0;
			//CPK (CPK)
			$lCpk = 1;
			//Xbar UCL (Xbarの上方管理限界線)
			$lXbarUcl = $lWorkInspectionResult;
			//Xbar LCL (Xbarの下方管理限界線)
			$lXbarLcl = $lWorkInspectionResult;
			//RChart UCL (RChartの上方管理限界線)
			$lRchartUcl = 0;
			//RChart  LCL (RChartの下方管理限界線)
			$lRchartLcl = 0;
			//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
			// $lJudgeReason = "";
		}

		if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
		{
			$lRowBeforeCommitInspectionResultAnalyze1 = (Array)$lTblBeforeCommitInspectionResultAnalyze1[0];

			if ($lBeforeCalcFlg == true and $lCalcFlg == true)
			{
				$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"];
				$lCalcNam = $lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"];
			}
			elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
			{
				$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] - 1;
				$lCalcNam = $lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"] - 1;
			}
			elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
			{
				$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] + 1;
				$lCalcNam = $lRowBeforeCommitInspectionResultAnalyze1["CALC_NUM"] + 1;
			}
			else
			{
				$lCodeNum = 0;
				$lCalcNam = 0;
			}
		}
		else
		{
			if ($lBeforeCalcFlg == true and $lCalcFlg == true)
			{
				//analyTBLは存在する →あり得ない
				// $lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"];
				// $lCalcNam = 1;
			}
			elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
			{
				//analyTBLは存在する →あり得ない
				// $lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] - 1;
				// $lCalcNam = 0;
			}
			elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
			{
				$lCodeNum = $lRowBeforeCommitInspectionResultDetail2["COUNT"] + 1;
				$lCalcNam = 1;
			}
			else
			{
				$lCodeNum = 0;
				$lCalcNam = 0;
			}
		}

		// Log::write('info', '2',
		// 	[
		// 		"CodeNum" => $lCodeNum,
		// 		"CalcNam" => $lCalcNam,
		// 	]
		// );

				
			if ($lBeforeCalcFlg == false and $lCalcFlg == false)
			{
				//Nothing
			}
			//Add 29-May-2018 Hoshina Because this patarn is no calculate
			// else if (($lBeforeCalcFlg == true and $lCalcFlg == false) and ($lCodeNum < 0 and $lCalcNam == 0))
			//Modify 11-Jun-2018 Hoshina
			else if (($lBeforeCalcFlg == true and $lCalcFlg == false) and ($lCodeNum <= 0 and $lCalcNam <= 0))
			{
				//Nothing
			}
			else
			{
				// Log::write('info', 'Start Setteing TRESANAL table item');
							
				//store value of TCCIENTMST Table (Xbar)
				$lTblCoefficientXbar = [];
				$lRowCoefficientXbar = [];
				$objtccientmst = new tccientmst;
				$lTblCoefficientXbar = $objtccientmst->getCoefficientData(self::COEFFICIENT_XBAR, $lCodeNum);
				if (count($lTblCoefficientXbar) != 0)
				{
					$lRowCoefficientXbar = (Array)$lTblCoefficientXbar[0];
				}
				else
				{
					$lRowCoefficientXbar["COEFFICIENT"] = 0;
				}

				//store value of TCCIENTMST Table (RChart UCL)
				$lTblCoefficientRChartUCL = [];
				$lRowCoefficientRChartUCL = [];
				$objtccientmst = new tccientmst;
				$lTblCoefficientRChartUCL = $objtccientmst->getCoefficientData(self::COEFFICIENT_RCHAT_UCL, $lCodeNum);
				if (count($lTblCoefficientRChartUCL) != 0)
				{
					$lRowCoefficientRChartUCL = (Array)$lTblCoefficientRChartUCL[0];
				}
				else
				{
					$lRowCoefficientRChartUCL["COEFFICIENT"] = 0;
				}

				//store value of TCCIENTMST Table (RChart LCL)
				$lTblCoefficientRChartLCL = [];
				$lRowCoefficientRChartLCL = [];
				$objtccientmst = new tccientmst;
				$lTblCoefficientRChartLCL = $objtccientmst->getCoefficientData(self::COEFFICIENT_RCHAT_LCL, $lCodeNum);
				if (count($lTblCoefficientRChartLCL) != 0)
				{
					$lRowCoefficientRChartLCL = (Array)$lTblCoefficientRChartLCL[0];
				}
				else
				{
					$lRowCoefficientRChartLCL["COEFFICIENT"] = 0;
				}

				//Calculate  Max Min Value
				if ($lBeforeCalcFlg == true and $lCalcFlg == true)
				{
					$lWorkMaxValue = $lWorkInspectionResult;
					$lWorkMinValue = $lWorkInspectionResult;
				}
				elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
				{
					$lWorkMaxValue = -9999999999999;
					$lWorkMinValue = 9999999999999;
				}
				elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
				{
					$lWorkMaxValue = $lWorkInspectionResult;
					$lWorkMinValue = $lWorkInspectionResult;
				}
				else
				{
					//入ってこない →No Calculate
				}

				// Log::write('info', '3',
				// 	[
				// 		"WorkMaxValue" => $lWorkMaxValue,
				// 		"WorkMinValue" => $lWorkMinValue,
				// 	]
				// );

				$lTblBeforeCommitInspectionResultDetail3 = $this->getBeforeCommitInspectionResultDetailData3($pCalculateArray["InspectionResultNo"], $pCalculateArray["InspectionPoint"], $pCalculateArray["AnalGrp01"], $pCalculateArray["AnalGrp02"]);

				//Calculate  Max Value and Min Value
				if ($lTblBeforeCommitInspectionResultDetail3 != null)
				{
					foreach ($lTblBeforeCommitInspectionResultDetail3 as $lRowBeforeCommitInspectionResultDetail3)
					{
						$lRowBeforeCommitInspectionResultDetail3 = (Array)$lRowBeforeCommitInspectionResultDetail3;
								
						// Log::write('info', '4',
						// 	[
						// 		"111" => $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"],
						// 		"222" => $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"],
						// 	]
						// );

						if ($lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"] != $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
						{
							if ($lWorkMaxValue < $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"])
							{
								$lWorkMaxValue = $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"];
							}
												
							if ($lWorkMinValue > $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"])
							{
								$lWorkMinValue = $lRowBeforeCommitInspectionResultDetail3["INSPECTION_RESULT_VALUE"];
							}
						}
					}
				}
				$lMaxValue = $lWorkMaxValue;
				$lMinValue = $lWorkMinValue;
	
				// Log::write('info', '5',
				// 	[
				// 		"MaxValue" => $lMaxValue,
				// 		"MinValue" => $lMinValue,
				// 	]
				// );

				//Calculate  Average Value
				if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
				{
					if ($lBeforeCalcFlg == true and $lCalcFlg == true)
					{
						$lAverageValue = ($lWorkInspectionResult - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"];
					}
					elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
					{
							//Modify 30-May-2018 Hoshina Because Calculate mistake 
							// $lAverageValue =  number_format(((0 - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]), 4);
							$lAverageValue = ($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]) / $lCalcNam + $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"];
					}
					elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
					{
						$lAverageValue = ($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] * ($lCalcNam - 1) + $lWorkInspectionResult) / $lCalcNam;
					}
					else
					{
						//入ってこない →No Calculate
					}
				}
				else
				{
					if ($lBeforeCalcFlg == true and $lCalcFlg == true)
					{
						//Nothing to do
					}
					elseif ($lBeforeCalcFlg == true and $lCalcFlg == false)
					{
						//Nothing to do
					}
					elseif ($lBeforeCalcFlg == false and $lCalcFlg == true)
					{
						$lAverageValue =  $lWorkInspectionResult;
					}
					else
					{
						//Nothing to do
					}
				}

				//Calculate  Range Value
				$lRangeValue = $lMaxValue - $lMinValue;
								
				// Log::write('info', '6',
				// 	[
				// 		"AverageValue" => $lAverageValue,
				// 		"RangeValue" => $lRangeValue,
				// 	]
				// );

				//Calculate  Stdev (ｼｸﾞﾏσ)
				if (count($lTblBeforeCommitInspectionResultAnalyze1) != 0)
				{
					//前回の平均値と今回の平均値が同じ場合→同値を入力した場合は、計算せず、前回のσをそのまま入れる
					if (($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]) == 0)
					{
						$lStdev = $lRowBeforeCommitInspectionResultAnalyze1["STDEV"];
					}
					else
					{
							//if ($lRowBeforeCommitInspectionResultDetail1["COUNT"] != 0)
							//{
							//    //既存測定値の修正の場合
							//	$lStdev = number_format(sqrt(
							//	                             (
							//	                              pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
							//	                              +
							//	                              pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)
							//	                              +
							//	                              (
							//	                               (
							//	                                ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
							//	                                *
							//	                                (
							//	                                 ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - Input::get('txtInspectionResult'))
							//	                                 -
							//	                                 (2 * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
							//	                                 +
							//	                                 (2 * $lAverageValue)
							//	                                )
							//	                               )
							//	                               / $lCalcNam
							//	                              )
							//	                             )
							//	                            )
							//	                        ,4);
							//}
							//else
							//{
							//    //新しい測定値の追加の場合
							//	$lStdev = number_format(sqrt(
							//	                              ((pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2) + pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)) * ($lCalcNam - 1) / $lCalcNam)
							//	                              + (pow($lAverageValue - Input::get('txtInspectionResult'), 2) / $lCalcNam)
							//	                             )
							//	                       ,4);
							//}
							
									
							//↑ 上はSTDEV.P(n法)の場合
							//↓ 下はSTDEV(n-1法)の場合
							//■■工場は通常STDEVを使用する■■
									
						if ($lBeforeCalcFlg == true)
						{
							//When modifying existing measurement values 既存測定値を修正する場合
							if ($lCalcNam >= 3)
							{
								$lStdev = sqrt(
								                pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
								                +
								                (
								                 (($lCalcNam * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2))
								                  +
								                  pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult, 2)
								                  -
								                  (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
								                  +
								                  (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult) * $lAverageValue)
								                 ) / ($lCalcNam - 1)
								                )
								              );
							}
							elseif ($lCalcNam == 2)
							{
								$lStdev = sqrt(
								                pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
								                +
								                (2 * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2))
								                +
								                pow($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult, 2)
								                -
								                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult) * $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"])
								                +
								                (2 * ($lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"] - $lWorkInspectionResult) * $lAverageValue)
							    	          );
							}
							else
							{
								$lStdev = 0;
							}
						}
						else
						{
							//When adding new measurement values 新しい測定値を追加する場合
							if ($lCalcNam >= 3)
							{
								$lStdev = sqrt(
								                ( ($lCalcNam - 2) * pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2)
								                  + (($lCalcNam - 1) * pow($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"], 2))
								                  + pow($lAverageValue - $lWorkInspectionResult, 2)
								                ) / ($lCalcNam - 1)
								              );
							}
							elseif ($lCalcNam == 2)
							{
								$lStdev = sqrt(
								                pow(($lAverageValue - $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"]), 2)
								                + pow(($lAverageValue - $lWorkInspectionResult) ,2)
								              );
							}
							else
							{
								$lStdev = 0;
							}
						}

							// Log::write('info', 'After Calculate Stdev',
							// 	[
							// 		"Stdev"            => $lRowBeforeCommitInspectionResultAnalyze1["STDEV"],
							// 		"STDEV2"           => pow($lRowBeforeCommitInspectionResultAnalyze1["STDEV"], 2),
							// 		"n"                => $lCalcNam,
							// 		"S"                => (2 * pow($lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"] - $lAverageValue, 2)),
							// 		"d"                => (Input::get('txtInspectionResult') - $lRowBeforeCommitInspectionResultDetail1["INSPECTION_RESULT_VALUE"]),
							// 		"Result"           => Input::get('txtInspectionResult'              ,''),
							// 		"AverageValueold"  => $lRowBeforeCommitInspectionResultAnalyze1["AVERAGE_VALUE"],
							// 		"AverageValuenow"  => $lAverageValue,
							// 	]
							// );
					}
				}
				else
				{
					$lStdev = 0;
				}

				// Log::write('info', '7',
				// 	[
				// 		"Stdev" => $lStdev,
				// 	]
				// );

				//Calculate  CPK
				if ($lStdev != 0)
				{
					$lWorkCpkStdSpecOver = ($pCalculateArray["StdSpecOver"]- $lAverageValue) / (3 * $lStdev);
					$lWorkCpkStdSpecUnder = ($lAverageValue - $pCalculateArray["StdSpecUnder"]) / (3 * $lStdev);
				}
				else
				{
					$lWorkCpkStdSpecOver = 0;
					$lWorkCpkStdSpecUnder = 0;
				}
								
				if ($lWorkCpkStdSpecOver < $lWorkCpkStdSpecUnder)
				{
					$lCpk = $lWorkCpkStdSpecOver;
				}
				else
				{
					$lCpk = $lWorkCpkStdSpecUnder;
				}
								
				// Log::write('info', '8',
				// 	[
				// 		"WorkCpkStdSpecOver"  => $lWorkCpkStdSpecOver,
				// 		"WorkCpkStdSpecUnder" => $lWorkCpkStdSpecUnder,
				// 		"Cpk"                 => $lCpk,
				// 	]
				// );

				//Calculate  Xbar UCL (Xbarの上方管理限界線)
				$lXbarUcl = $lAverageValue + $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue;
				//Calculate  Xbar LCL (Xbarの下方管理限界線)
				$lXbarLcl = $lAverageValue - $lRowCoefficientXbar["COEFFICIENT"] * $lRangeValue;

				//Calculate  RChart UCL (RChartの上方管理限界線)
				$lRchartUcl = $lRowCoefficientRChartUCL["COEFFICIENT"] * $lRangeValue;
				//Calculate  RChart  LCL (RChartの下方管理限界線)
				$lRchartLcl = $lRowCoefficientRChartLCL["COEFFICIENT"] * $lRangeValue;

				// Log::write('info', '9',
				// 	[
				// 		"XbarUcl"    => $lXbarUcl,
				// 		"XbarLcl"    => $lXbarLcl,
				// 		"RchartUcl"  => $lRchartUcl,
				// 		"RchartLcl"  => $lRchartLcl,
				// 	]
				// );

				//JUDGE_REASON(規格値が平均値±3σに入っていない場合の理由)
				//画面入力値の為、下で画面の値を入れる
				
				//Judgeに使う値を求める
				//Get Value for Judge
				//初回、又は、全てのデータが同じ値
				//in case the first inspection or all inspection value is same
				if ($lStdev == 0)
				{
					$lJudgeMin = 0;
					$lJudgeMax = 0;
				}
				else
				{
					$lJudgeMin = $lAverageValue - $lStdev * 3;
					$lJudgeMax = $lAverageValue + $lStdev * 3;
				}

				// Log::write('info', '10',
				// 	[
				// 		"JudgeMin"    => $lJudgeMin,
				// 		"JudgeMax"    => $lJudgeMax,
				// 	]
				// );
			}

		// Modify 12-Jun-2018 Hoshina Because No number_format
		// $pArray += [
		// 		"BeforeCalcFlg"          => $lBeforeCalcFlg,
		// 		"CalcFlg"                => $lCalcFlg,
		// 		"CalcNam"                => $lCalcNam,
		// 		"CodeNum"                => $lCodeNum,
		// 		"InspectionResultValue"  => number_format($lWorkInspectionResult, 4),
		// 		"MaxValue"               => number_format($lMaxValue, 4),
		// 		"MinValue"               => number_format($lMinValue, 4),
		// 		"AverageValue"           => number_format($lAverageValue, 4),
		// 		"RangeValue"             => number_format($lRangeValue, 4),
		// 		"StdevValue"             => number_format($lStdev, 4),
		// 		"CpkValue"               => number_format($lCpk, 4),
		// 		"XbarUclValue"           => number_format($lXbarUcl, 4),
		// 		"XbarLclValue"           => number_format($lXbarLcl, 4),
		// 		"RchartUclValue"         => number_format($lRchartUcl, 4),
		// 		"RchartLclValue"         => number_format($lRchartLcl, 4),
		// 		// "JudgeReason"            => $lJudgeReason,
		// 		"JudgeMinValue"          => number_format($lJudgeMin, 4),
		// 		"JudgeMaxValue"          => number_format($lJudgeMax, 4),
		// 	];
		$pArray += [
				"BeforeCalcFlg"          => $lBeforeCalcFlg,
				"CalcFlg"                => $lCalcFlg,
				"CalcNam"                => $lCalcNam,
				"CodeNum"                => $lCodeNum,
				"InspectionResultValue"  => $lWorkInspectionResult,
				"MaxValue"               => $lMaxValue,
				"MinValue"               => $lMinValue,
				"AverageValue"           => $lAverageValue,
				"RangeValue"             => $lRangeValue,
				"StdevValue"             => $lStdev,
				"CpkValue"               => $lCpk,
				"XbarUclValue"           => $lXbarUcl,
				"XbarLclValue"           => $lXbarLcl,
				"RchartUclValue"         => $lRchartUcl,
				"RchartLclValue"         => $lRchartLcl,
				// "JudgeReason"            => $lJudgeReason,
				"JudgeMinValue"          => $lJudgeMin,
				"JudgeMaxValue"          => $lJudgeMax,
			];




	// Log::write('info', 'Array',
	// 	[
	// 		"Array"                => $pArray,
	// 	]
	// );

	return $pArray;
	}


	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailData1
	// overview           
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailData1($pInspectionResultNo, $pInspectionNo)
	{
		$lTblInspectionResultDetail1 = [];

			$lTblInspectionResultDetail1 = DB::select('
				SELECT DETL.INSPECTION_RESULT_VALUE
				      ,DETL.INSPECTION_RESULT_TYPE
				  FROM TRESDETT AS DETL
				 WHERE DETL.INSPECTION_RESULT_NO = :InspectionResultNo
				   AND DETL.INSPECTION_NO        = :InspectionNo
			',
				[
					"InspectionResultNo" => TRIM((String)$pInspectionResultNo),
					"InspectionNo"       => TRIM((String)$pInspectionNo),
				]
			);

		return $lTblInspectionResultDetail1;
	}

	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailData2
	// overview           
	// argument           
	// return value       
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailData2($pInspectionResultNo, $pInspectionPoint, $pAnalGrp01, $pAnalGrp02)
	{
		$lTblInspectionResultDetail2 = [];

			$lTblInspectionResultDetail2 = DB::select('
				SELECT Count(*) AS COUNT
				  FROM TRESDETT AS DETL
				 WHERE DETL.INSPECTION_RESULT_NO     = :InspectionResultNo
				   AND DETL.INSPECTION_POINT         = :InspectionPoint
				   AND DETL.ANALYTICAL_GRP_01        = :AnalyticalGroup01
				   AND DETL.ANALYTICAL_GRP_02        = :AnalyticalGroup02
				   -- Modify 07-Jun-2018 Hoshina Because count is OK, OFFSET,NG
				   -- AND DETL.INSPECTION_RESULT_TYPE   = ""
				   -- AND DETL.INSPECTION_RESULT_TYPE   <> ""
				   AND DETL.INSPECTION_RESULT_TYPE   IN ("1","2","3")
			',
				[
					"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
					"InspectionPoint"          => TRIM((String)$pInspectionPoint),
					"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
					"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
				]
			);

		return $lTblInspectionResultDetail2;
	}

	//**************************************************************************
	// process            getBeforeCommitInspectionResultDetailData3
	// overview           
	// argument           
	// return value       Array
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultDetailData3($pInspectionResultNo, $pInspectionPoint, $pAnalGrp01, $pAnalGrp02)
	{
		$lTblInspectionResultDetail3 = [];
		
			$lTblInspectionResultDetail3 = DB::select('
				SELECT DETL.INSPECTION_RESULT_VALUE
				  FROM TRESDETT AS DETL
				 WHERE DETL.INSPECTION_RESULT_NO     = :InspectionResultNo
				   AND DETL.INSPECTION_POINT         = :InspectionPoint
				   AND DETL.ANALYTICAL_GRP_01        = :AnalyticalGroup01
				   AND DETL.ANALYTICAL_GRP_02        = :AnalyticalGroup02
				   AND DETL.INSPECTION_RESULT_TYPE   IN ("1","2","3")
			',
				[
					"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
					"InspectionPoint"          => TRIM((String)$pInspectionPoint),
					"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
					"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
				]
			);

		return $lTblInspectionResultDetail3;
	}

	//**************************************************************************
	// process            getBeforeCommitInspectionResultAnalyzeData1
	// overview           
	// argument           
	// return value       Array
	// record of updates  No,1 
	// Remarks            
	//**************************************************************************
	private function getBeforeCommitInspectionResultAnalyzeData1($pInspectionResultNo, $pInspPoint, $pAnalGrp01, $pAnalGrp02)
	{
		$lTblInspectionResultAnalyze1 = [];

			$lTblInspectionResultAnalyze1 = DB::select('
				SELECT ANALY.CALC_NUM
	 					,ANALY.MAX_VALUE
	 					,ANALY.MIN_VALUE
	 					,ANALY.AVERAGE_VALUE
	 					,ANALY.STDEV
				 FROM TRESANAL AS ANALY
				WHERE ANALY.INSPECTION_POINT      = :InspectionPoint
				  AND ANALY.ANALYTICAL_GRP_01     = :AnalyticalGroup01
				  AND ANALY.ANALYTICAL_GRP_02     = :AnalyticalGroup02
				  AND ANALY.INSPECTION_RESULT_NO  = :InspectionResultNo
			',
				[
					"InspectionResultNo"       => TRIM((String)$pInspectionResultNo),
					"InspectionPoint"          => TRIM((String)$pInspPoint),
					"AnalyticalGroup01"        => TRIM((String)$pAnalGrp01),
					"AnalyticalGroup02"        => TRIM((String)$pAnalGrp02),
				]
			);

		return $lTblInspectionResultAnalyze1;
	}















}
