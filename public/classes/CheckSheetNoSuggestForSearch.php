<?php
	//**************************************************************************
	// 処理名    CheckSheetNoSuggestForSearch
	// 概要      検査シートマスタの内容を検索し、Suggest表示に用いる
	//           JavaScriptからAjaxの処理で呼び出される
	// 引数      無し
	// 戻り値    無し
	// 作成者    s-miyamoto
	// 作成日    2014.07.29
	// 更新履歴  2014.07.29 v0.01 初回作成
	//**************************************************************************
	
	//★注★　本来、DB関連の処理はLaravelの設定ファイルに記載した内容で、
	//Laravelで用意されている「DB::select」を用いて処理したいが、
	//当該処理はAjaxでJavaScriptから呼ばれているため、ファイル配置の都合上
	//phpを直に実行するしか対応が取れなかった。
	//JavaScriptからLaravelのRouteファイルにアクセスするパスが通れば、通常の方法で対処出来る可能性はある。
	
	$lSelectOptionValues = ""; //SelectBoxに出力するためのOptionがn件連なったテキスト
	$lDataCount          = 0 ; //データカウント
	$lEscapedValue       = ""; //エスケープ後の画面入力値
	
	//画面入力値が送られてきた場合
	if($_POST["input_value_for_search"] != ""){
		
		//DB接続 ★app/config/database.phpの内容と合わせること★
		$lDBCon = mysql_connect('localhost', 'root', 'FactoryZQC');
		
		//DB接続に失敗する場合はエラー処理
		if (!$lDBCon) {
		    die('DB Connection Error! '.mysql_error());
		}
		
		//DB接続後、DBを選択する
		$lDBSelectedFlg = mysql_select_db('factoryzdb', $lDBCon);
		
		//DB選択に失敗する場合はエラー処理
		if ($lDBSelectedFlg == false){
		    die('DB Select Error! '.mysql_error());
		}
		
		//画面入力値をエスケープする
		$lEscapedValue = mysql_escape_string($_POST["input_value_for_search"]);
		
		//検索処理
		$lResult = mysql_query('SELECT DISTINCT INSPECTION_SHEET_NO FROM TISHEETM WHERE DELETE_FLG = "0" AND INSPECTION_SHEET_NO LIKE "%'.$lEscapedValue.'%"ORDER BY INSPECTION_SHEET_NO');
		if ($lResult == false) {
		    die('DB Query Error! '.mysql_error());
		}
		
		//DB切断
		mysql_close($lDBCon);
		
		//返却値　HTMLのSELECTタグ　\マークのエスケープをしているので見づらい…　登録用との違いはここだけ。まとめたい。
		$lSelectOptionValues .= "<select id=\"select_val_for_search\" size=\"5\" onClick=\"SetSelectValueForSearch()\">";
		
		//取得したデータが無くなるまでSELECTタグのOPTIONタグを作り続ける
		while ($row = mysql_fetch_assoc($lResult)) {
			
			$lSelectOptionValues .= "<option>{$row['INSPECTION_SHEET_NO']}</option>";
			
			//データカウント加算
			$lDataCount += 1;
			
		}
		
		//返却値　HTMLのSELECTタグ（閉じる）
		$lSelectOptionValues .= "</select>";
		
	}
	
	//データが0件の場合はタグ不要なので消し去る
	if($lDataCount == 0) {
		$lSelectOptionValues = "";
	}
	
	echo $lSelectOptionValues;
	
?>