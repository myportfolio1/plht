/* FACTORYZEROQC */

/* 検索結果一覧で、Modifyボタン押下時に検査実績No（PK）をHiddenに格納する処理 */
function setHiddenValue(inspectionResultNo, dataRev) {
	document.listform.hidInspectionResultNo.value = inspectionResultNo;
	document.listform.hidDataRev.value = dataRev;
}

/* 検索結果一覧で、LineStopボタン押下時に検査実績No（PK）をHiddenに格納する処理 */
function setHiddenValueForLineStop(inspectionResultNo, dataRev) {
	if (window.confirm("Q003 : Will you make the rest of data Line Stop?")) {
		document.listform.hidInspectionResultNo.value = inspectionResultNo;
		document.listform.hidDataRev.value = dataRev;
	}
}


/* 検索結果一覧で、Deleteボタン押下時に検査実績No（PK）をHiddenに格納する処理 */
function setHiddenValueForDelete(inspectionResultNo, dataRev) {
	if (window.confirm("Q002 : Will you delete Inspection Result?")) {
		document.listform.hidInspectionResultNo.value = inspectionResultNo;
		document.listform.hidDataRev.value = dataRev;
	}
}

/* 検索結果一覧で、InspectionSheetボタン押下時に検査実績No（PK）をHiddenに格納する処理 */
function setHiddenValueFoInspectionSheet(inspectionResultNo, dataRev) {
	if (window.confirm("Q008 : Will you print Inspection Sheet?")) {
		document.listform.hidInspectionResultNo.value = inspectionResultNo;
		document.listform.hidDataRev.value = dataRev;
	}
}


/* 検索結果一覧で、Importボタン押下時に検査実績No（PK）をHiddenに格納する処理 */
function setHiddenValueForDataImport(inspectionResultNo, dataRev) {
	if (window.confirm("Q009 : Will you Data Import?")) {
		document.listform.hidInspectionResultNo.value = inspectionResultNo;
		document.listform.hidDataRev.value = dataRev;
	}
}



/* 登録画面で、ロード時にフォーカスする処理 */
function setDefaultForcus() {
	
	/* テキスト入力欄があるならフォーカス処理 */
	if (document.EntryForm.txtInspectionResult !== undefined) {
		document.EntryForm.txtInspectionResult.focus();
	}
	
	/* ラジオ選択があるならフォーカス処理 */
	if (document.EntryForm.rdiInspectionResult !== undefined) {
		document.EntryForm.rdiInspectionResult[0].focus();
	}
	
	/* NGやOFFSETの文言表示のため呼び出している */
	checkAndSetResultInformation('0');
	
}

/* 顧客マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForCustomerMaster(hidPrimaryKey1)
{
	document.CustomerMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* 測定器マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForEquipmentMaster(hidPrimaryKey1)
{
	document.EquipmentMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* ユーザマスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForUserMaster(hidPrimaryKey1)
{
	document.UserMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* 機械マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForMachineMaster(hidPrimaryKey1) 
{
	document.MachineMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* OFFSET/NG理由マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForngreasonmaster(hidPrimaryKey1) 
{
	document.NgReasonMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* OFFSET/NG分類マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForngreasonBunruimaster(hidPrimaryKey1) 
{
	document.NgReasonBunruiMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* アクション詳細マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForActionmDetailMaster(hidPrimaryKey1) 
{
	document.ActionmDetailMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* アクション詳細マスタ登録画面で、Modifyボタン押下時にマスタのPKをHiddenに格納する処理 */
function setHiddenValueForTrPersonMaster(hidPrimaryKey1) 
{
	document.TRPersonMasterForm.hidPrimaryKey1.value = hidPrimaryKey1;
}

/* 登録画面で、ReCheckボタン押下時に検査結果入力欄をクリアし、フォーカスする処理 */
function cleartxtInspectionResult() {
	
	/* 各オブジェクトが存在する場合だけ処理する */
	if (document.EntryForm.rdiInspectionResult !== undefined) {
		document.EntryForm.rdiInspectionResult[0].checked = false;
		document.EntryForm.rdiInspectionResult[0].focus();
	}
	
	if (document.EntryForm.rdiInspectionResult !== undefined) {
		document.EntryForm.rdiInspectionResult[1].checked = false;
	}
	
	if (document.EntryForm.txtInspectionResult !== undefined) {
		document.EntryForm.txtInspectionResult.value = "";
		document.EntryForm.txtInspectionResult.focus();
	}
	
	if (document.EntryForm.cmbJudgeReason !== undefined) {
		var cmbJudgeReason = document.getElementById("JudgeReason").getElementsByTagName('option');
		for(i=0; i<cmbJudgeReason.length;i++){
			if(cmbJudgeReason[i].value == ""){
				cmbJudgeReason[i].selected = true;
				break;
			}
		}
	}
	
	if (document.EntryForm.rdiResultClass !== undefined) {
		document.EntryForm.rdiResultClass[0].checked = false;
	}
	
	if (document.EntryForm.rdiResultClass !== undefined) {
		document.EntryForm.rdiResultClass[1].checked = false;
	}
	
	if (document.EntryForm.rdiResultClass !== undefined) {
		document.EntryForm.rdiResultClass[2].checked = false;
	}
	
	if (document.EntryForm.rdiResultClass !== undefined) {
		document.EntryForm.rdiResultClass[3].checked = false;
	}
	
	if (document.EntryForm.rdiResultClass !== undefined) {
		document.EntryForm.rdiResultClass[4].checked = false;
	}
	
	if (document.EntryForm.cmbOffsetNGBunrui !== undefined) {
		var cmbOffsetNGBunrui = document.getElementById("OffsetNGBunrui").getElementsByTagName('option');
		for(i=0; i<cmbOffsetNGBunrui.length;i++){
			if(cmbOffsetNGBunrui[i].value == 0){
				cmbOffsetNGBunrui[i].selected = true;
				break;
			}
		}
	}
	
	if (document.EntryForm.cmbOffsetNGReason !== undefined) {
		var cmbOffsetNGReason = document.getElementById("OffsetNGReason").getElementsByTagName('option');
		for(i=0; i<cmbOffsetNGReason.length;i++){
			if(cmbOffsetNGReason[i].value == ""){
				cmbOffsetNGReason[i].selected = true;
				break;
			}
		}
	}
	
	if (document.EntryForm.rdiOffsetNGAction !== undefined) {
		document.EntryForm.rdiOffsetNGAction[0].checked = false;
	}
	
	if (document.EntryForm.rdiOffsetNGAction !== undefined) {
		document.EntryForm.rdiOffsetNGAction[1].checked = false;
	}
	
	if (document.EntryForm.chkOffsetNGActionDetailInternal !== undefined) {
		document.EntryForm.chkOffsetNGActionDetailInternal.checked = false;
	}
	if (document.EntryForm.chkOffsetNGActionDetailRequest !== undefined) {
		document.EntryForm.chkOffsetNGActionDetailRequest.checked = false;
	}
	if (document.EntryForm.chkOffsetNGActionDetailTSR !== undefined) {
		document.EntryForm.chkOffsetNGActionDetailTSR.checked = false;
	}
	if (document.EntryForm.chkOffsetNGActionDetailSorting !== undefined) {
		document.EntryForm.chkOffsetNGActionDetailSorting.checked = false;
	}
	if (document.EntryForm.chkOffsetNGActionDetailNoAction !== undefined) {
		document.EntryForm.chkOffsetNGActionDetailNoAction.checked = false;
	}
	
	if (document.EntryForm.cmbActionDetails !== undefined) {
		var cmbActionDetails = document.getElementById("ActionDetails").getElementsByTagName('option');
		for(i=0; i<cmbActionDetails.length;i++){
			if(cmbActionDetails[i].value == ""){
				cmbActionDetails[i].selected = true;
				break;
			}
		}
	}
	
	if (document.EntryForm.txtTecReqNo !== undefined) {
		document.EntryForm.txtTecReqNo.value = "";
	}
	
	if (document.EntryForm.cmbTecReqPerson !== undefined) {
		var cmbTecReqPerson = document.getElementById("TecReqPerson").getElementsByTagName('option');
		for(i=0; i<cmbTecReqPerson.length;i++){
			if(cmbTecReqPerson[i].value == ""){
				cmbTecReqPerson[i].selected = true;
				break;
			}
		}
	}
	
	if (document.EntryForm.txtTSRNo !== undefined) {
		document.EntryForm.txtTSRNo.value = "";
	}
}

/* 登録画面で、検査結果の範囲チェックを行い、ラジオボタンの選択制御を行う処理 */
function checkAndSetResultInformation(mode) {

	/* 検査結果の入力区分　1が数字、2がOK/NG */
	var InspectionResultInputType = document.EntryForm.hidInspectionResultInputType.value;
	
	/* Entry押下時のみ、2度押し制御スクリプト実行　setDefaultForcus()経由なら0なので動かない */
	if (mode == '1')
	{
		dontDoubleClick();
	}
	
	/* 数値入力パターンの場合だけチェックする */
	if (InspectionResultInputType == "1")
	{
		/* この処理は画面ロード時もsetDefaultForcus()経由で呼び出されているが、登録ボタン押下時もしくは変更のLoad時だけ動くようにしている。 */
		/* 検査結果の入力が前提（No CheckとLine Stopは未入力がありえるので、その場合は無視。ゼロ入力がありえるので!=""では比較出来ない。） */
		/*Motooka modified for Kohbyo*/
		if (document.EntryForm.txtInspectionResult.value.length > 0)
		{
			/* 数値入力パターンの場合だけ値が取得出来るので、ここで取得する。1000倍しておかないと、基準値32.5の時に32.5と32.50と32.500と…試していくと合わなくなることがある。*/
			/* InspectionResultValue(検査結果値)は検査器具を使用した場合にマイナスの値がセットされる場合があるが許容するため絶対値でチェックする*/
			/* 値が1000以上の場合、正常判定されない*/
			var InspectionResultValue     = Math.abs(document.EntryForm.txtInspectionResult.value*1000);
			var NGUnder                   = document.EntryForm.hidNGUnder.value*1000;
			var OFFSETUnder               = document.EntryForm.hidOFFSETUnder.value*1000;
			var OFFSETOver                = document.EntryForm.hidOFFSETOver.value*1000;
			var NGOver                    = document.EntryForm.hidNGOver.value*1000;
			var OriginalOFFSETOver        = document.EntryForm.hidOFFSETOver.value;
			var OriginalNGOver            = document.EntryForm.hidNGOver.value;
			
			/* console.log(NGUnder);*/
			/* console.log(OFFSETUnder);*/
			/* console.log(OFFSETOver);*/
			/* console.log(NGOver);*/
			/* console.log(OriginalOFFSETOver);*/
			/* console.log(OriginalNGOver);*/
			
			/* Status表示用 */
			var StatusNode = document.getElementById('Status');

			/* ここで1度Statusをクリア（クリアしないと文字列が結合されて一瞬見えるのが不恰好 */
			StatusNode.innerHTML = "";
			
			/*NG Over か NG Underで判定したいとき*/
			if ((NGUnder == OFFSETUnder) && (OFFSETOver == NGOver))
			{
				/* NG Under（入力値 < NG Under）の場合 */
				if (InspectionResultValue < NGUnder)
				{
					/* alert("NGUnder");*/
					document.EntryForm.rdiResultClass[2].checked = true;
					
					var textNode = document.createTextNode('NG Under');
				}
				/* 基準値内（NG Under <= 入力値　かつ　入力値 <= NG Over）の場合 */
				else if ((NGUnder <= InspectionResultValue) && (InspectionResultValue <= NGOver))
				{
					/* alert("OK");*/
					document.EntryForm.rdiResultClass[0].checked = true;
					
					document.EntryForm.rdiOffsetNGAction[0].checked = false;
					document.EntryForm.rdiOffsetNGAction[1].checked = false;
					
					var textNode = document.createTextNode('OK');
				}
				/* NG Over（NG Over < 入力値）の場合 */
				else if (NGOver < InspectionResultValue)
				{
					/* alert("NGOver");*/
					document.EntryForm.rdiResultClass[2].checked = true;
					
					var textNode = document.createTextNode('NG Over');
				}
				else
				{
					/* このパターンは無い */
				}
			}
			else
			{
				/*NG Over, OFFSET Over, OFFSET Under, NG Underで判定したいとき*/
				/* NG Under（入力値 <= NG Under）の場合 */
				if (InspectionResultValue <= NGUnder)
				{
					document.EntryForm.rdiResultClass[2].checked = true;
					
					var textNode = document.createTextNode('NG Under');
				}
				/* OFFSET Under（NG Under < 入力値　かつ　入力値 <= OFFSET Under）の場合 */
				else if ((NGUnder < InspectionResultValue) && (InspectionResultValue <= OFFSETUnder))
				{
					document.EntryForm.rdiResultClass[1].checked = true;
					
					var textNode = document.createTextNode('OFFSET Under');
				}
				/* 基準値内（OFFSET Under < 入力値　かつ　入力値 < OFFSET Over）の場合 */
				else if ((OFFSETUnder < InspectionResultValue) && (InspectionResultValue < OFFSETOver))
				{
					document.EntryForm.rdiResultClass[0].checked = true;
					
					document.EntryForm.rdiOffsetNGAction[0].checked = false;
					document.EntryForm.rdiOffsetNGAction[1].checked = false;
					
					var textNode = document.createTextNode('OK');
				}
				/* OFFSET Over（OFFSET Over <= 入力値　かつ　入力値 < NG Over）の場合 */
				else if ((OFFSETOver <= InspectionResultValue) && (InspectionResultValue < NGOver))
				{
					document.EntryForm.rdiResultClass[1].checked = true;
					
					var textNode = document.createTextNode('OFFSET Over');
				}
				/* NG Over（NG Over <= 入力値）の場合 */
				else if (NGOver <= InspectionResultValue)
				{
					document.EntryForm.rdiResultClass[2].checked = true;
					
					var textNode = document.createTextNode('NG Over');
				}
				else
				{
					/* このパターンは無い */
				}
			}
			
			//Status表示
			StatusNode.appendChild(textNode);
		}
		/* この処理は画面ロード時もsetDefaultForcus()経由で呼び出されているが、登録ボタン押下時もしくは変更のLoad時だけ動くようにしている。 */
		/* 検査結果の入力が前提（No CheckとLine Stopは未入力がありえるので、その場合は無視。） */
		if (document.EntryForm.txtInspectionResult.value.length > 0)
		{
			/* 数値入力パターンの場合だけ値が取得出来るので、ここで取得する。1000倍しておかないと、基準値32.5の時に32.5と32.50と32.500と…試していくと合わなくなることがある。*/
			var JudgeMin               = document.EntryForm.hidJudgeMin.value*1000;
			var JudgeMax               = document.EntryForm.hidJudgeMax.value*1000;
			var StandardNGUnder        = document.EntryForm.hidNGUnder.value*1000;
			var StandardNGOver         = document.EntryForm.hidNGOver.value*1000;
			
			 console.log(JudgeMin);
			 console.log(JudgeMax);
			 console.log(StandardNGUnder);
			 console.log(StandardNGOver);
			
			/* Judge表示用 */
			var JudgeNode = document.getElementById('Judge');

			/* ここで1度Judgeをクリア（クリアしないと文字列が結合されて一瞬見えるのが不恰好 */
			JudgeNode.innerHTML = "";
			
			if (JudgeMin == 0 && JudgeMax == 0)
			{
				//alert("here01");
				var textNode = document.createTextNode('No Check');
			}
			else
			{
				if ((StandardNGUnder > JudgeMin) || (StandardNGUnder > JudgeMax) || (StandardNGOver < JudgeMin) || (StandardNGOver < JudgeMax))
				{
					//alert("here11");
					var textNode = document.createTextNode('No Good');
				}
				else
				{
					//alert("here21");
					var textNode = document.createTextNode('Good');
				}
			}
			
			//Judge表示
			JudgeNode.appendChild(textNode);
		}
	}
	else
	/* OK/NGパターンの場合は、Inspection ResultのOK/NGを、Result ClassのOK/NGに連動させる */
	{
		/* 検査結果がOK */
		if (document.EntryForm.rdiInspectionResult[0].checked == true)
		{
			/* 結果区分もOK */
			document.EntryForm.rdiResultClass[0].checked = true;
		}
		/* 検査結果がNG */
		else if (document.EntryForm.rdiInspectionResult[1].checked == true)
		{
			/* 結果区分もNG */
			document.EntryForm.rdiResultClass[2].checked = true;
		}
		else
		{
			/* No CheckやLine Stopの場合は未選択がありえる。その場合は何もしない。 */
		}
	}
}

/* 登録画面の2度押し禁止スクリプト */
var set = 0;
function dontDoubleClick() {
	if(set==0){
		
		set=1;
		
	}else{
		
		alert("E995 : Double-click is not allowed. Please wait.");
		return false;
		
	}
}

/* PictureDL＆UL画面で、Deleteボタン押下時に確認ダイアログを表示して、OKならダウンロード処理 */
function deletePictureData() {
	if (window.confirm("Q006 : Will you delete the selected Picture Data?")) {
		
		/* 処理OKを押下したことをhiddenに格納し、PHP側に伝える手段とする */
		document.PictureForm.hidProcOKFlg.value = "OK";
		
	}
}

/* PictureDL＆UL画面で、Uploadボタン押下時に確認ダイアログを表示して、OKならダウンロード処理 */
function uploadPictureData() {
	if (window.confirm("Q007 : Will you upload the Picture Data?")) {
		
		/* 処理OKを押下したことをhiddenに格納し、PHP側に伝える手段とする */
		document.PictureForm.hidProcOKFlg.value = "OK";
		
	}
}

/* マスタDL＆UL画面で、DownLoadボタン押下時に確認ダイアログを表示して、OKならダウンロード処理 */
function downloadMasterData() {
	if (window.confirm("Q004 : Will you download the selected Master Data?")) {
		
		/* 処理OKを押下したことをhiddenに格納し、PHP側に伝える手段とする */
		document.MasterForm.hidProcOKFlg.value = "OK";
		
	}
	else
	{
		document.MasterForm.hidProcOKFlg.value = "";
	}
}

/* マスタDL＆UL画面で、Uploadボタン押下時に確認ダイアログを表示して、OKならダウンロード処理 */
function uploadMasterData() {
	if (window.confirm("Q005 : Will you upload the selected Master Data?")) {
		
		/* 処理OKを押下したことをhiddenに格納し、PHP側に伝える手段とする */
		document.MasterForm.hidProcOKFlg.value = "OK";
		
	}
}

/* Ajaxによる入力補完（Check Sheet No.（登録用）） */
function SuggestSearch(input){
	
	/* AjaxでPHPに対してPOSTしている。画面の入力値を「input_value」という名称で渡す。 */
	$.ajax({
		type: 'post',
		url: '../../classes/CheckSheetNoSuggest.php',
		cache: false,
		data: {
			'input_value':	input.value
		},
		success: function(data){
			$("#SuggestDispForEntry").html(data);
		}
	});
	
}

/* Ajax入力補完を選択した際、選択値を入力欄に反映させる処理（Check Sheet No.（登録用）） */
function SetSelectValue(){

	$("#txtCheckSheetNoForEntry").val($("#select_val option:selected").val());
	$("#SuggestDispForEntry").html("");
	
}

/* Ajaxによる入力補完（Check Sheet No.（検索用）） */
function SuggestSearchForSearch(input){
	
	/* AjaxでPHPに対してPOSTしている。画面の入力値を「input_value_for_search」という名称で渡す。呼び出しているPHPが異なる点に注意。 */
	$.ajax({
		type: 'post',
		url: '../../classes/CheckSheetNoSuggestForSearch.php',
		cache: false,
		data: {
			'input_value_for_search':	input.value
		},
		success: function(data){
			$("#SuggestDispForSearch").html(data);
		}
	});
	
}

/* Ajax入力補完を選択した際、選択値を入力欄に反映させる処理（Check Sheet No.（検索用）） */
function SetSelectValueForSearch(){
	
	/* ここもIDを指定しているので多少記述が異なる */
	$("#txtCheckSheetNoForSearch").val($("#select_val_for_search option:selected").val());
	$("#SuggestDispForSearch").html("");
	
}
/*Motooka added for Kohbyo's graph*/
/* transport data from controller to blade to js */
	
	/*get value of X,R from view*/
	/*select table in blade*/
	var domX = document.getElementById("javascript_table_X_tbody");
	
	
	/*enter data of lines to parameter*/
	var collectionX = domX.rows;
	
	
	/*enter the data of line 1 in table to parameter*/
	var trX = collectionX.item(0);
	
	
	/*enter data to parameter from left cell*/
	/*change data of cells to numeric*/
	var sumX = 0;
	
	
	for ( var i = 0; i<=4; i++ )
	{
		 eval("var tdX" + i + "= trX.cells.item(" + i + ");");
		 
		 eval("var DataX" + i + "= parseFloat(tdX" + i + ".firstChild.nodeValue);");
		 
		 eval("sumX = sumX + DataX" + i + ";");
		 
	}
	
	
	/*For Center line(CLx,CLr)*/
	var CLX = parseFloat((sumX/5).toFixed(4));
	
	
$('body').data('foo1', 6.6835); /* データ( 6.6685 )を格納*/
$('body').data('foo2', 6.6581); /* データ( 6.6581 )を格納*/


/*Graph1*/
var dataPlot11 = [
  { label: "1", y: DataX0 },
  { label: "2", y: DataX1 },
  { label: "3", y: DataX2 },
  { label: "4", y: DataX3 },
  { label: "5", y: DataX4 , indexLabel: "x average" }
];

$UCLx = $('body').data('foo1');
var dataPlot12 = [
  { label: "1", y: $UCLx },
  { label: "2", y: $UCLx },
  { label: "3", y: $UCLx },
  { label: "4", y: $UCLx },
  { label: "5", y: $UCLx , indexLabel: "UCLx"}
];
$LCLx = $('body').data('foo2');
var dataPlot13 = [
  { label: "1", y: $LCLx },
  { label: "2", y: $LCLx },
  { label: "3", y: $LCLx },
  { label: "4", y: $LCLx },
  { label: "5", y: $LCLx , indexLabel: "LCLx"}
];

var dataPlot14 = [
  { label: "1", y: CLX },
  { label: "2", y: CLX },
  { label: "3", y: CLX },
  { label: "4", y: CLX },
  { label: "5", y: CLX , indexLabel: "CLx"}
];

//Make Chart
$(function () {
          var chart1 = new CanvasJS.Chart(chartContainer11, {
              title:{text: 'X'},
              axisY: {
					includeZero:false,
					title: 'x',
					margin: 0.0001,
				},
		      data: [{
		        	type: 'line',
	        		dataPoints: dataPlot11
	        		},
	        		{type: 'line',
	        		dataPoints: dataPlot12
	        		},
	        		{type: 'line',
	        		dataPoints: dataPlot13
	        		},
	        		{type: 'line',
	        		dataPoints: dataPlot14
	        		}]
          });
 
          chart1.render();
});
