<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link type="text/css" rel="stylesheet" media="screen" href="../../css/bootstrap.css" />
		<link type="text/css" rel="stylesheet" media="screen" href="../../css/bootstrap-responsive.css" />
		<link type="text/css" rel="stylesheet" media="screen" href="../../css/factoryz.css" />
		<!-- faviconはimg直下に設置 -->
		<link rel="shortcut icon" href="../../img/favicon_graph.ico" />
		<title>FactoryZeroQC</title>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script type="text/javascript" charset="UTF-8" src="../../js/bootstrap.js"></script>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script type="text/javascript" charset="UTF-8" src="../../js/jquery-2.1.0.js"></script>
		<!-- JavaScript -->
		<script type="text/javascript" charset="UTF-8" src="../../js/factoryz.js"></script>
	</head>
	<body class="BodyWidth">
		@include("header")
			@yield("content")
		@include("footer")
	</body>
</html>