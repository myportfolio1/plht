	@section("header")
		<div class="header">
			<h4>Factory Zero QC</h4>
			<meta http-equiv="Pragma" content="no-cache">
			<meta http-equiv="Cache-Control" content="no-cache">
			<meta http-equiv="Expires" content="0">
			@if (Auth::check())
				<a href="{{ URL::route("user/list") }}">"Inspection List"</a>
				<!-- 管理者だけ表示 -->
				@if ((String)$AdminFlg == "1")
					<a href="{{ URL::route("user/graph") }}">"Graph"</a>
					<a href="{{ URL::route("user/master") }}">"Master Data Download & Upload"</a>
				@endif
				<a href="{{ URL::route("user/logout") }}">"Logout"</a>
					@if ((String)$AdminFlg == "1")
						<Br>
							<a href="{{ URL::route("user/picture") }}">"Picture Upload"</a>
							<a href="{{ URL::route("user/usermaster") }}">"User Master"</a>
							<a href="{{ URL::route("user/machinemaster") }}">"Machine Mastere"</a>
							<a href="{{ URL::route("user/customermaster") }}">"Customer Master"</a>
							<a href="{{ URL::route("user/equipmentmaster") }}">"Equipment Master"</a>
							<a href="{{ URL::route("user/ngreasonmaster") }}">"OFFSET/NG Reason Master"</a>
							<a href="{{ URL::route("user/ngreasonbunruimaster") }}">"OFFSET/NG Reason Class Master"</a>
							<a href="{{ URL::route("user/actionmdetailmaster") }}">"Action Detail Master"</a>
							<a href="{{ URL::route("user/trpersonmaster") }}">"TR Person Master"</a>
							<a href="{{ URL::route("user/monitoring") }}">"Monitoring"</a>
					@endif
			@endif
		</div>
	@show