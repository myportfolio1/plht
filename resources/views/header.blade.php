	@section("header")
		<div class="header">
			<h4>Factory Zero-QC</h4>
			<meta http-equiv="Pragma" content="no-cache">
			<meta http-equiv="Cache-Control" content="no-cache">
			<meta http-equiv="Expires" content="0">
			<style type="text/css">
    			.bu {	
    				padding-top: 0px;
				    padding-left: 0px;
				    padding-right: 0px;
				    padding-bottom: 0px;
				}
			</style>
			<?php 
				$directoryURI = $_SERVER['REQUEST_URI'];
				$path = parse_url($directoryURI, PHP_URL_PATH);
				$components = explode('/', $path);
                $first_part = end($components);
			?>

			@if (Auth::check())
					<nav class="navbar navbar-default">
  						<div class="navbar-inner">
		  					<ul class="nav">
								<li class="<?php if ($first_part=="list" or $first_part=="preentry" or $first_part=="entry") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" href="{!! URL::route("user.list") !!}">Inspection List</a></li>
								<li class="<?php if ($first_part=="graph") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" href="{!! URL::route("user.graph") !!}">Graph</a></li>

								@if ((String)$AdminFlg == "1")
									<li class="<?php if ($first_part=="monitoring") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.monitoring") !!}">Monitoring</a></li>
									<li class="<?php if ($first_part=="master") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.master") !!}">Master Data Download & Upload</a></li>
									<li class="<?php if ($first_part=="picture") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.picture") !!}">Picture Upload</a></li>
									<li class="<?php if ($first_part=="usermaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.usermaster") !!}">User</a></li>
									<li class="<?php if ($first_part=="machinemaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.machinemaster") !!}">Machine</a></li>
									<li class="<?php if ($first_part=="customermaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.customermaster") !!}">Customer</a></li>
									<li class="<?php if ($first_part=="equipmentmaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.equipmentmaster") !!}">Equipment</a></li>
									<li class="<?php if ($first_part=="ngreasonmaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.ngreasonmaster") !!}">OFFSET/NG Reason</a></li>
									<li class="<?php if ($first_part=="ngreasonbunruimaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.ngreasonbunruimaster") !!}">OFFSET/NG Reason Class</a></li>
									<li class="<?php if ($first_part=="actionmdetailmaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.actionmdetailmaster") !!}">Action Detail</a></li>
									<li class="<?php if ($first_part=="trpersonmaster") {echo "active"; } else  {echo "noactive";}?>"><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.trpersonmaster") !!}">TR Person</a></li>

								@endif
								<li><a style="padding-top: 5px; padding-left: 5px; padding-right: 5px;padding-bottom: 15px;" class="bu" href="{!! URL::route("user.logout") !!}">Logout</a></li>
							</ul>
						</div>
					</nav>
			@endif
		</div>
	@show