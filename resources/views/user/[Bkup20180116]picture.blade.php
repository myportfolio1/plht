@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Picture Maintenance</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif
		
		<!-- ファイルアップロードがある場合は、files=trueの設定が必要 -->
		{{ Form::open([
			"route"         => "user/picture",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "PictureForm",
			"files"         => true,
		])}}
			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Picture Upload</legend>
					<table>
						<tr>
							<td>
								{{ Form::file("filUploadFile") }}
								{{ Form::submit("Data Upload",
									[ 
										"class"   => "RoundControls",
										"name"    => "btnUpload",
										"onClick" => "uploadPictureData()",
									]
								) }}
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Picture Delete</legend>
					<table>
						<tr>
							<td>
								{{ Form::select("cmbPictureName"
									,$arrDataListPictureData
									,$PictureName
									,[
										"class"      => "input-xlarge",
										"name"       => "cmbPictureData",
										"onChange"   => "this.form.submit()",
									])
								}}
								{{ Form::submit("Delete",
									[ 
										"class"   => "RoundControls",
										"name"    => "btnDelete",
										"onClick" => "deletePictureData()",
									])
								}}
							</td>
						</tr>
						<tr>
							<td class="TDMarginPaddingSetting VAlignTop">
								<!-- 画像をホイールで拡大縮小するScriptをここで読み込んでいる -->
								<script src="../../js/jquery.wheelzoom.js"></script>
								<img src="../../{{$PictureURL}}" alt="zumen" width="500px" height="500px" id="wheelzoom">
								<script>
									$('#wheelzoom').wheelzoom();
								</script>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
		{{ Form::hidden("hidProcOKFlg", "") }}
		{{ Form::close() }}
		
@stop
