@extends("layout")
@section("content")
		<div class="TitleBar">Login</div>
		@if ($lErrorMessage = $Errors->first("error"))
			<div class="ErrorMessage">
				{{ $lErrorMessage }}
			</div>
		@endif
		
		<div class="InputFormGroup">
			{!! Form::open([
				"route"         => "user.login",
				"autocomplete"  => "off"
			]) !!}
			
				<div>
					{!! Form::label("lblUserID","User ID") !!}
					
					<div>
						{{--{!! Form::text("txtUserID", Input::old("txtUserID"), [
							"maxlength"     => 10,
						]) !!}--}}
						<input type="text" name="txtUserID" value="{{ old('txtUserID') }}" maxlength = 10>
					</div>
				</div>
				<div>
					{!! Form::label("lblPassword","Password") !!}
					
					<div>
						{!! Form::password("txtPassword", [
							"maxlength"     => 64,
						]) !!}
						
					</div>
				</div>
				<div>
					{!! Form::submit("Login", [ 
						"class" => "RoundControls"
					]) !!}
					
				</div>
			{!! Form::close() !!}
			
		</div>
@stop