@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Inspection List</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{!! $error !!}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{!! $NormalMessage !!}
			</div>
		@endif


		{{-- ファイルアップロードがある場合は、files=trueの設定が必要 --}}
		{!! Form::open([
			"route"         => "user.list",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "listform",
			"files"         => true,
		]) !!}

			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Inspection Result Data Entry</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblProcessForEntry","Process") !!}
									{!! Form::select('cmbProcessForEntry', $arrDataListProcessList, $ProcessForEntry, [
											"class"      => "input-small",
											"onChange"   => "this.form.submit()",
									]) !!}
									{!! Form::hidden("hidProcessForEntry", $ProcessForEntry) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblCustomerForEntry","Customer/Supplier") !!}
									{!! Form::select('cmbCustomerForEntry', $arrDataListCustomerList, $CustomerForEntry, [
											"class"      => "input-large",
											"onChange"   => "this.form.submit()",
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblCheckSheetNoForEntry","Check Sheet") !!}
									{!! Form::select('cmbCheckSheetNoForEntry', $arrCheckSheetNoForEntryList, $CheckSheetNoForEntry, [
											"class"      => "input-large",
											"onChange"   => "this.form.submit()",
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblMachineNoForEntry","M/C No.") !!}
									{!! Form::text("txtMachineNoForEntry", $MachineNoForEntry, [
										"class"         => "input-mini",
										"maxlength"     => 10,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblMoldNoForEntry","Mold No.") !!}
									{!! Form::text("txtMoldNoForEntry", $MoldNoForEntry, [
										"class"         => "input-mini",
										"maxlength"     => 4,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{{-- AdminFlg == "0" is readonly --}}
									{!! Form::label("lblInspectorCodeForEntry","Inspector") !!}
									@if ((String)$AdminFlg == "0")
										{!! Form::text("txtInspectorCodeForEntry", $UserID, [
											"class"         => "input-mini",
											"maxlength"     => 10,
											"readonly"      => "readonly",
										]) !!}
									@else
										{!! Form::text("txtInspectorCodeForEntry", "$InspectorCodeForEntry", [
											"class"         => "input-mini",
											"maxlength"     => 10,
										]) !!}
									
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblInspectionDateForEntry","Inspection Date") !!}
									{{--{{ Form::text("txtInspectionDateForEntry", $InspectionDateForEntry, [
										"class"         => "input-small",
										"maxlength"     => 10,
									]) }}--}}
									<input size="10" class="input-small form_datetime" id="InspectionDate" name="txtInspectionDateForEntry" type="text" value="{!! $InspectionDateForEntry !!}" >
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblConditionForEntry","Condition") !!}
									{!! Form::select('cmbConditionForEntry', $arrDataListCondition, $ConditionForEntry, [
											"class"      => "input-small"
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblInspectionTimeForEntry","Insp. Time") !!}
									{!! Form::select('cmbInspectionTimeForEntry', $arrDataListInspectionTime, $InspectionTimeForEntry, [
											"class"      => "input-small"
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::submit("Entry", [ 
										"class" => "RoundControls",
										"name"  => "btnEntry"
									]) !!}
								</td>

							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
			<div class="SearchFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Search & Data Output</legend>
						<table>
							<tr>
								{{-- (Search) --}}
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblProcessForSearch","Process") !!}
									{!! Form::select('cmbProcessForSearch', $arrDataListProcessList, $ProcessForSearch, [
											"class"      => "input-small",
											"onChange"   => "this.form.submit()",
									]) !!}
									{!! Form::hidden("hidProcessForSearch", $ProcessForSearch) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblCustomerForSearch","Customer/Supplier") !!}
									{!! Form::select('cmbCustomerForSearch', $arrDataListCustomerForSearchList, $CustomerForSearch, [
											"class"      => "input-large",
											"onChange"   => "this.form.submit()",
									]) !!}
									{!! Form::hidden("hidCustomerForSearch", $CustomerForSearch) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblCheckSheetNoForSearch","Check Sheet") !!}
									{!! Form::select('cmbCheckSheetNoForSearch', $arrCheckSheetNoForSearchList, $CheckSheetNoForSearch, [
											"class"      => "input-large",
											"onChange"   => "this.form.submit()",
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblRevisionNoForSearch","Revision No.") !!}
									{!! Form::select('cmbRevisionNoForSearch', $arrRevNoForSearchList, $RevisionNoForSearch, [
											"class"      => "input-small",
											"onChange"   => "this.form.submit()",
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblMachineNoForSearch","M/C No.") !!}
									{!! Form::text("txtMachineNoForSearch", $MachineNoForSearch, [
										"class"         => "input-mini",
										"maxlength"     => 10,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblMoldNoForSearch","Mold No.") !!}
									{!! Form::text("txtMoldNoForSearch", $MoldNoForSearch, [
										"class"         => "input-mini",
										"maxlength"     => 10,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{{-- AdminFlg == "0" is readonly --}}
									{!! Form::label("lblInspectorCodeForSearch","Inspector") !!}
									@if ((String)$AdminFlg == "0")
										{!! Form::text("txtInspectorCodeForSearch", $UserID, [
											"class"         => "input-mini",
											"maxlength"     => 10,
											"readonly"      => "readonly",
										]) !!}
									@else
										{!! Form::text("txtInspectorCodeForSearch", $InspectorCodeForSearch, [
											"class"         => "input-mini",
											"maxlength"     => 10,
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblPartNoForSearch","Part No.") !!}
									{!! Form::text("txtPartNoForSearch", $PartNoForSearch, [
										"class"         => "input-small",
										"maxlength"     => 30,
									]) !!}
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblProductionDateFromForSearch","Production Date") !!}
									<input size="10" class="input-small" id="startdateproduction" name="txtProductionDateFromForSearch" type="text" value="{!! $ProductionDateFromForSearch !!}" >
									-
									<input size="10" class="input-small" id="enddateproduction" name="txtProductionDateToForSearch" type="text"
									 value="{!! $ProductionDateToForSearch !!}" >
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblInspectionDateFromForSearch","Inspection Date") !!}
									{{--{{ Form::text("txtInspectionDateFromForSearch", $InspectionDateFromForSearch, [
										"class"         => "input-small",
										"maxlength"     => 10,
									]) }}--}}
									<input size="10" class="input-small" id="startdateinspection" name="txtInspectionDateFromForSearch" type="text" value="{!! $InspectionDateFromForSearch !!}" >
									-
									{{--{{ Form::text("txtInspectionDateToForSearch", $InspectionDateToForSearch, [
										"class"         => "input-small",
										"maxlength"     => 10,
									]) }}--}}
									<input size="10" class="input-small" id="enddateinspection" name="txtInspectionDateToForSearch" type="text" value="{!! $InspectionDateToForSearch !!}" >
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblInspectionTimeForSearch","Insp. Time") !!}
									{!! Form::select('cmbInspectionTimeForSearch', $arrDataListInspectionTime, $InspectionTimeForSearch, [
											"class"      => "input-small"
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::submit("Search", [ 
										"class" => "RoundControls",
										"name"  => "btnSearch"
									]) !!}
									{{-- CSV is AdminFlg = "1" only --}}
									@if ((String)$AdminFlg == "1")
										{!! Form::submit("CSV Download", [ 
											"class" => "RoundControls",
											"name"  => "btnCsv"
										]) !!}
									@endif
									{{--{{ Form::submit("CSV (For Approval)", [ 
										"class" => "RoundControls",
										"name"  => "btnCsv2"
									]) }}--}}
									{!! Form::submit("Excel", [ 
										"class" => "RoundControls",
										"name"  => "btnShoninData"
									]) !!}
									{{-- NG Result is AdminFlg = "1" only --}}
									@if ((String)$AdminFlg == "1")
									{!! Form::submit("NGResult", [ 
										"class" => "RoundControls",
										"name"  => "btnShoninData2"
									]) !!}
									@endif
								</td>
								<td>
									{!! Form::label("file","Choice Import File") !!}
									{!! Form::file("filUploadFile") !!}
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
			
		@if ($Pagenator != null)
			<table class="SearchResultTable">
				<thead>
					<tr class="SearchResultTable whitetableframe">
						<th class="SearchResultHeader tablecolumn00">Modify</th>
						<th class="SearchResultHeader tablecolumn01">Pro<br>cess</th>
						<th class="SearchResultHeader tablecolumn02">Customer/Supplier</th>
						<th class="SearchResultHeader tablecolumn03">CheckSheet<br>No.</th>
						<th class="SearchResultHeader tablecolumn04">Rev.<br>No.</th>
						<th class="SearchResultHeader tablecolumn05">Part<br>No.</th>
						<th class="SearchResultHeader tablecolumn06">Part<br>Name</th>
						<th class="SearchResultHeader tablecolumn07">Material<br>Name</th>
						{{--Write comment--}}
						{{-- <th class="SearchResultHeader tablecolumn08">Material<br>Size</th> --}}
						<th class="SearchResultHeader tablecolumn09">Mold<br>No.</th>
						<th class="SearchResultHeader tablecolumn10">M/C<br>No.</th>
					 	<th class="SearchResultHeader tablecolumn11">Inspector<br>Code</th>
						<th class="SearchResultHeader tablecolumn12">Inspector<br>Name</th>
						<th class="SearchResultHeader tablecolumn13">Lot<br>No.</th>
						<th class="SearchResultHeader tablecolumn14">Production<br>Date</th>
						<th class="SearchResultHeader tablecolumn15">Inspection<br>Date</th>
						<th class="SearchResultHeader tablecolumn16">Con<br>dition</th>
						<th class="SearchResultHeader tablecolumn17">Insp.<br>Time</th>
						<th class="SearchResultHeader tablecolumn18">Progress</th>
						<th class="SearchResultHeader tablecolumn19">Rate</th>
						<th class="SearchResultHeader tablecolumn20">Insp.<br>Sheet</th>
						<th class="SearchResultHeader tablecolumn23">Data<br>Import</th>
						<th class="SearchResultHeader tablecolumn21">Data<br>Del</th>
						<th class="SearchResultHeader tablecolumn22">Line<br>Stop</th>
					</tr>
				</thead>
				<tbody>
				
				<?php $cntRow = 0 ?>
				{{-- //Laravel VerUp From 4.2 To 5.5 --}}
				{{-- $intCurrentPage = $Pagenator->getCurrentPage() --}}
				<?php $intCurrentPage = $Pagenator->CurrentPage() ?>

				{{-- //Laravel VerUp From 4.2 To 5.5 --}}
				{{-- $intPerPage = $Pagenator->getPerPage() --}}
				<?php $intPerPage = $Pagenator->PerPage() ?>

				@foreach($Pagenator as $arrDataRow)
					
					<?php $cntRow += 1 ?>
					
					@if (
							(($intPerPage * ($intCurrentPage - 1) + 1) <= $cntRow)
							 and ($cntRow <= $intPerPage * ($intCurrentPage))
						)
							<tr class="SearchResultTable">
								<td class="tablecolumn00 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Modify", [ 
										"class"   => "RoundControls",
										"name"    => "btnModify",
										"onClick" => "setHiddenValue(" . $arrDataRow->INSPECTION_RESULT_NO . "," . $arrDataRow->DATA_REV . ")",
									]) !!}
								</td>
								<td class="tablecolumn01 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->PROCESS_NAME !!}
								</td>
								<td class="tablecolumn02 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->CUSTOMER_NAME !!}
								</td>
								<td class="tablecolumn03 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->INSPECTION_SHEET_NO !!}
								</td>
								<td class="tablecolumn04 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->REV_NO !!}
								</td>
								<td class="tablecolumn05 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
								 	{!! $arrDataRow->ITEM_NO !!}
								</td>
								<td class="tablecolumn06 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->ITEM_NAME !!}
								</td>
								<td class="tablecolumn07 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->MATERIAL_NAME !!}
								</td>
								{{--Write comment--}}
								{{-- <td class="tablecolumn08 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->MATERIAL_SIZE !!}
								</td> --}}
								<td class="tablecolumn09 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->MOLD_NO !!}
								</td>
								<td class="tablecolumn10 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->MACHINE_NAME !!}
								</td>
								<td class="tablecolumn11 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->INSERT_USER_ID !!}
								</td>
								<td class="tablecolumn12 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->USER_NAME !!}
								</td>
								<td class="tablecolumn13 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->LOT_NO !!}
								</td>
								<td class="tablecolumn14 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->PRODUCTION_YMD !!}
								</td>
								<td class="tablecolumn15 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->INSPECTION_YMD !!}
								</td>
								<td class="tablecolumn16 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->CONDITION_NAME !!}
								</td>
								<td class="tablecolumn17 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->INSPECTION_TIME_NAME !!}
								</td>
								{{--Write comment--}}
								{{-- <td class="tablecolumn16 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext " @if(isset($NotExistNoData[$arrDataRow->INSPECTION_RESULT_NO])) title=" {{ $NotExistNoData[$arrDataRow->INSPECTION_RESULT_NO] }} "  @endif >
								 	{{ $arrDataRow->RESULT_NO_COUNT }} / {{ $arrDataRow->TOTAL_NO_COUNT }}
								 </td> --}}
								<td class="tablecolumn18 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
									{!! $arrDataRow->RESULT_COUNT !!} / {!! $arrDataRow->TOTAL_COUNT !!}
								</td>
								<td class="tablecolumn19 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext ">
									{!! $arrDataRow->RESULT_RATE."%" !!}
								</td>
								<td class="tablecolumn20 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Print", [ 
										"class"   => "RoundControls",
										"name"    => "btnPrintInspectionSheet",
										"onClick" => "setHiddenValueFoInspectionSheet(" . $arrDataRow->INSPECTION_RESULT_NO . "," . $arrDataRow->DATA_REV . ")",
									]) !!}
								</td>
								<td class="tablecolumn21 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Import", [ 
										"class"   => "RoundControls",
										"name"    => "btnDataImport",
										"onClick" => "setHiddenValueForDataImport(" . $arrDataRow->INSPECTION_RESULT_NO . "," . $arrDataRow->DATA_REV . ")",
									]) !!}
								</td>								
								<td class="tablecolumn22 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Del", [ 
										"class"   => "RoundControls",
										"name"    => "btnDelete",
										"onClick" => "setHiddenValueForDelete(" . $arrDataRow->INSPECTION_RESULT_NO . "," . $arrDataRow->DATA_REV . ")",
									]) !!}
								</td>
								<td class="tablecolumn23 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Stop", [ 
										"class"   => "RoundControls",
										"name"    => "btnLineStop",
										"onClick" => "setHiddenValueForLineStop(" . $arrDataRow->INSPECTION_RESULT_NO . "," . $arrDataRow->DATA_REV . ")",
									]) !!}
								</td>

							</tr>
					@endif
				
				@endforeach
				
				{!! Form::hidden("hidInspectionResultNo") !!}
				{!! Form::hidden("hidDataRev") !!}
				
				</tbody>
			</table>
			
			<div class="pagination">
				{{-- //Laravel VerUp From 4.2 To 5.5 --}}
				{{-- {{ $Pagenator->links(); }} --}}
				{!! $Pagenator->render(); !!}
			</div>
			
		@endif
		
		{!! Form::close() !!}


		<style type="text/css">
			/* Pagination links */
			.pagination a {
			    color: black;
			    float: left;
			    padding: 8px 16px;
			    text-decoration: none;
			    transition: background-color .3s;
			}

			/* Style the active/current link */
			.pagination a.active {
			    background-color: dodgerblue;
			    color: white;
			}

			/* Add a grey background color on mouse-over */
			.pagination a:hover:not(.active) {background-color: #ddd;}
		</style>




<script src="../../js/datepicker/bootstrap-datetimepicker.js" charset="utf-8"></script>
<script src="../../js/datepicker/bootstrap-datetimepicker.min.js" charset="utf-8"></script>
<script src="../../js/datepicker/moment.js" charset="utf-8"></script>
<script type="text/javascript">

	// var today = new Date();
    $(".form_datetime").datetimepicker({
    	format: 'dd-mm-yyyy',
    	startView: 'month',
        minView: 'month',
        autoclose: true ,
        todayBtn: true,
	});
    // $('.form_datetime').datetimepicker('setEndDate', today);


	window.onload = function ()
	{
  //   	var oneweekago = moment().subtract(7, 'days').format('DD-MM-YYYY');
	 //    $("#startdateproduction").datetimepicker({
	 //    	format: 'dd-mm-yyyy',
	 //    	startView: 'month',
	 //        minView: 'month',
	 //        autoclose: true ,
	 //        todayBtn: true,
		// }).val(oneweekago);
		$("#startdateproduction").datetimepicker({
	    	format: 'dd-mm-yyyy',
	    	startView: 'month',
	        minView: 'month',
	        autoclose: true ,
	        todayBtn: true,
		});

	    $("#enddateproduction").datetimepicker({
	    	format: 'dd-mm-yyyy',
	    	startView: 'month',
	        minView: 'month',
	        autoclose: true ,
	        todayBtn: true,
		});

		$("#startdateinspection").datetimepicker({
	    	format: 'dd-mm-yyyy',
	    	startView: 'month',
	        minView: 'month',
	        autoclose: true ,
	        todayBtn: true,
		});

	    $("#enddateinspection").datetimepicker({
	    	format: 'dd-mm-yyyy',
	    	startView: 'month',
	        minView: 'month',
	        autoclose: true ,
	        todayBtn: true,
		});

	 //    $("#InspectionDate").datetimepicker({
	 //    	format: 'dd-mm-yyyy',
	 //    	startView: 'month',
	 //        minView: 'month',
	 //        autoclose: true ,
	 //        todayBtn: true,
		// });

	    $("#InspectionDate").datetimepicker({
	    	format: 'dd-mm-yyyy',
	    	startView: 'month',
	        minView: 'month',
	        autoclose: true ,
	        todayBtn: true,
		}).val(moment().format('DD-MM-YYYY'));
    }

    
</script> 
@stop