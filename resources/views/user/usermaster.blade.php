@extends("layout")
@section("content") 
		<div class="header">
			<div class="TitleBar">User Master Maintenance</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif
		
		{!! Form::open([
			"route"         => "user.usermaster",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "UserMasterForm",
		])!!}
			<div class="SearchFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Search Condition</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblUserForSearch","User ID") !!}
									{!! Form::text("txtUserIDForSearch", $UserNoForSearch, [
										"class"         => "input-small",
										"maxlength"     => 10,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblUserNameForSearch","User Name") !!}
									
									{!! Form::text("txtUserNameForSearch", $UserNameForSearch, [
										"class"         => "input-large",
										"maxlength"     => 100,
									]) !!}
								</td>
								
								<td></td>
								<td></td>
								<td>Count</td>
								<td class="TDMarginPaddingSetting">{!! $PersonCount !!}</td>
							</tr>
						</table>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									@if($SearchLock == "Lock")
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnSearch"
										]) !!}
									@else
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"name"  => "btnSearch"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($NewAddLock == "Lock")
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnNewAdd"
										]) !!}
									@else
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"name"  => "btnNewAdd"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($RegistLock == "Lock")
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnResistUpload"
										]) !!}
									@else
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"name"  => "btnResistUpload"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($DeleteLock == "Lock")
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnDelete"
										]) !!}
									@else
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"name"  => "btnDelete"
										]) !!}
									@endif
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@if($EditVisible == "True")
			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Edit Field</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblUserIDForEntry","User ID") !!}
									@if($MachineNoEditLock == "Lock")
										{!! Form::text("txtUserIDForEntry", $UserIDForEntry, [
											"class"         => "input-small",
											"ReadOnly"      => "True",
											"maxlength"     => 10,
										]) !!}
									@else
										{!! Form::text("txtUserIDForEntry", $UserIDForEntry, [
											"class"         => "input-small",
											"maxlength"     => 10,
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblUserNameForEntry","User Name") !!}
									{!! Form::text("txtUserNameForEntry", $UserNameForEntry, [
										"class"         => "input-large",
										"maxlength"     => 100,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblPasswordForEntry","Password") !!}
									{!! Form::text("txtPasswordForEntry", $PasswordForEntry, [
										"class"         => "input-large",
										"maxlength"     => 64,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblAdminFlg","Administrator") !!}
									{!! Form::select("cmbAdminFlgForEntry", 
										[
											"" => "",
											"0" => "Not Administrator",
											"1" => "Administrator",
										]
										,$AdminFlgForEntry
										,[
											"class"      => "input-large"
										])
									!!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblTeam","Team") !!}
									{!! Form::select("cmbTeamIDForEntry",  
										$arrDataTeam
										,$TeamIDForEntry
										,[
											"class"      => "input-large"
										])
									!!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblDisplayOrderForEntry","Display Order") !!}
									
									{!! Form::text("txtDisplayOrderForEntry", $DisplayOrderForEntry, [
										"class"         => "input-small",
										"maxlength"     => 11,
									]) !!}
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@endif
		@if ($Pagenator != null)
			<table class="SearchResultTable">
				<thead>
					<tr class="SearchResultTable whitetableframe">
						<th class="SearchResultHeader tablecolumn01">Modify</th>
						<th class="SearchResultHeader tablecolumnUserId">User<br>ID</th>
						<th class="SearchResultHeader tablecolumnUserName">User<br>Name</th>
						<th class="SearchResultHeader tablecolumnAdminiFlg">Administrator</th>
						<th class="SearchResultHeader tablecolumnTeamId">Team<br>ID</th>
						<th class="SearchResultHeader tablecolumnDisplayOrder">Display<br>Order</th>
					</tr>
				</thead>
				<tbody>
				
				<?php $cntRow = 0 ?>
				<?php $intCurrentPage = $Pagenator->CurrentPage() ?>
				<?php $intPerPage = $Pagenator->PerPage() ?>
				
				@foreach($Pagenator as $arrDataRow) 

					<?php $cntRow += 1 ?>
					
					<!-- 1ページの件数×（現在ページ－1）＋1　～　1ページの件数×現在ページの範囲に該当するデータのみを出力 -->
					@if (
							(($intPerPage * ($intCurrentPage - 1) + 1) <= $cntRow)
							 and ($cntRow <= $intPerPage * ($intCurrentPage))
						)
							<tr class="SearchResultTable">
								<td class="tablecolumn01 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Modify", [ 
										"class"   => "RoundControls",
										"name"    => "btnModify",
										"onClick" => "setHiddenValueForUserMaster('" . $arrDataRow->USER_ID . "')",
									]) !!} 
								</td>
								<td class="tablecolumnUserId SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->USER_ID !!}
								</td>
								<td class="tablecolumnUserName SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->USER_NAME !!}
								</td>
								<td class="tablecolumnAdminiFlg SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->ADMIN_FLG_NAME !!}
								</td>
								<td class="tablecolumnAdminiFlg SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->CODE_NAME !!}
								</td>
								<td class="tablecolumnDisplayOrder SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->DISPLAY_ORDER !!}
								</td>
							</tr>
					@endif
				
				@endforeach
				<!-- hdnは共通部品でJavaScriptでセットする定義が必要 -->
				{!! Form::hidden("hidPrimaryKey1") !!}
				</tbody>
			</table>
			<div class="pagination">
				{!! $Pagenator->render(); !!}
			</div>
			
		@endif
		
		{!! Form::close() !!}

		<style type="text/css">
			/* Pagination links */
			.pagination a {
			    color: black;
			    float: left;
			    padding: 8px 16px;
			    text-decoration: none;
			    transition: background-color .3s;
			}

			/* Style the active/current link */
			.pagination a.active {
			    background-color: dodgerblue;
			    color: white;
			}

			/* Add a grey background color on mouse-over */
			.pagination a:hover:not(.active) {background-color: #ddd;}
		</style>


@stop