@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Inspection Result Entry & Modify</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		{{ Form::open([
			"route"         => "user/entry",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "EntryForm",
		])}}

			<div class="InputFormGroup">
				<table>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Master No.</th>
									<th class="SearchResultHeader">Process</th>
									<th class="SearchResultHeader">Customer/Supplier</th>
									<th class="SearchResultHeader">Check Sheet No.</th>
									<th class="SearchResultHeader">Check Sheet Title</th>
									<th class="SearchResultHeader">Revision No.</th>
									<th class="SearchResultHeader">Part No.</th>
									<th class="SearchResultHeader">Part Name</th>
									<th class="SearchResultHeader">Material Name</th>
									<th class="SearchResultHeader">Material Size</th>
									<th class="SearchResultHeader">M/C No.</th>
									<th class="SearchResultHeader">Inspector Code</th>
									<th class="SearchResultHeader">Inspector Name</th>
									<th class="SearchResultHeader">Inspection Date</th>
									<th class="SearchResultHeader">Condition</th>
									<th class="SearchResultHeader">Inspection Time</th>
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{{ $MasterNo }}</td>
									<td class="SearchResultTable">{{ $ProcessName }}</td>
									<td class="SearchResultTable">{{ $CustomerName }}</td>
									<td class="SearchResultTable">{{ $CheckSheetNo }}</td>
									<td class="SearchResultTable">{{ $CheckSheetName }}</td>
									<td class="SearchResultTable">{{ $RevisionNo }}</td>
									<td class="SearchResultTable">{{ $ItemNo }}</td>
									<td class="SearchResultTable">{{ $ItemName }}</td>
									<td class="SearchResultTable">{{ $MaterialName }}</td>
									<td class="SearchResultTable">{{ $MaterialSize }}</td>
									<td class="SearchResultTable">{{ $MachineNoName }}</td>
									<td class="SearchResultTable">{{ $InspectorCode }}</td>
									<td class="SearchResultTable">{{ $InspectorName }}</td>
									<td class="SearchResultTable">{{ $InspectionDate }}</td>
									<td class="SearchResultTable">{{ $ConditionName }}</td>
									<td class="SearchResultTable">{{ $InspectionTimeName }}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Model Name</th>
									<!-- <th class="SearchResultHeader">Form No.</th> -->
									<!-- <th class="SearchResultHeader">Dar No.</th> -->
									<th class="SearchResultHeader">Item</th>
									<th class="SearchResultHeader">Tool Code</th>
									<!-- <th class="SearchResultHeader">EFF Date</th> -->
									<th class="SearchResultHeader">Drawing No.</th>
									<th class="SearchResultHeader">ISO Control No.</th>
									<th class="SearchResultHeader">Product Weight</th>
									<th class="SearchResultHeader">Inspection Items</th>
									<th class="SearchResultHeader">X-R Chart No./Pokayoke No.</th>
									<th class="SearchResultHeader">Heat Treatment Type</th>
									<th class="SearchResultHeader">Plating Kind</th>
									<th class="SearchResultHeader">Team Name</th>
									<th class="SearchResultHeader">Mold Name</th>
									<th class="SearchResultHeader">Color</th>
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{{ $ModelName }}</td>
									<!-- <td class="SearchResultTable">{{ $ModelName }}</td> -->
									<!-- <td class="SearchResultTable">{{ $ModelName }}</td> -->
									<td class="SearchResultTable">{{ $ModelName }}</td>
									<td class="SearchResultTable">{{ $ToolCode }}</td>
									<!-- <td class="SearchResultTable">{{ $ModelName }}</td> -->
									<td class="SearchResultTable">{{ $DrawingNo }}</td>
									<td class="SearchResultTable">{{ $ISOKanriNo }}</td>
									<td class="SearchResultTable">{{ $ProductWeight }}</td>
									<td class="SearchResultTable">{{ $InspectionItem }}</td>
									<td class="SearchResultTable">{{ $XRChartNoPokayokeNo }}</td>
									<td class="SearchResultTable">{{ $HeatTreatmentType }}</td>
									<td class="SearchResultTable">{{ $PlatingKind }}</td>
									<td class="SearchResultTable">{{ $PlatingKind }}</td>
									<td class="SearchResultTable">{{ $PlatingKind }}</td>
									<td class="SearchResultTable">{{ $PlatingKind }}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Material Lot No.</th>
									<th class="SearchResultHeader">Certificate No.</th>
									<th class="SearchResultHeader">P/O No.</th>
									<th class="SearchResultHeader">Invoice No.</th>
									<th class="SearchResultHeader">Quantity Coil</th>
									<th class="SearchResultHeader">Quantity Weight</th>
									<th class="SearchResultHeader">Lot No.</th>
									<th class="SearchResultHeader">Production Date</th>
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{{ $MaterialLotNoForEntry }}</td>
									<td class="SearchResultTable">{{ $CertificateNoForEntry }}</td>
									<td class="SearchResultTable">{{ $PONoForEntry }}</td>
									<td class="SearchResultTable">{{ $InvoiceNoForEntry }}</td>
									<td class="SearchResultTable">{{ $QuantityCoilForEntry }}</td>
									<td class="SearchResultTable">{{ $QuantityWeightForEntry }}</td>
									<td class="SearchResultTable">{{ $LotNoForEntry }}</td>
									<td class="SearchResultTable">{{ $PlatingKind }}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Refer to document / Frequency</th>
									<th class="SearchResultHeader">Equipment</th>
									<th class="SearchResultHeader">NG Under</th>
									<th class="SearchResultHeader">OFFSET Under</th>
									<!--2015/04/23　保科修正　Reference Value⇒Center-->
									<!--<th class="SearchResultHeader">Reference Value</th>-->
									<th class="SearchResultHeader">Center</th>
									<th class="SearchResultHeader">OFFSET Over</th>
									<th class="SearchResultHeader">NG Over</th>
									<th class="SearchResultHeader">Inspection No.</th>
									<!-- <th class="SearchResultHeader">Shot No.</th> -->
									<!-- <th class="SearchResultHeader">Inspection Point</th> -->
									<!-- <th class="SearchResultHeader">Cavity No.</th> -->
									<th class="SearchResultHeader">Status</th>
									<!-- ■■2017/03/10 Motooka added■■ -->
									<!-- 検査結果入力区分が1：テキスト入力ならテキスト、2：OK/NGならラジオボタン選択式にする -->
									@if ($InspectionResultInputType == "1")
										<th class="SearchResultHeader">X bar</th>
										<th class="SearchResultHeader">Max</th>
										<th class="SearchResultHeader">Min</th>
										<th class="SearchResultHeader">Range</th>
										<th class="SearchResultHeader">Stdev</th>
										<th class="SearchResultHeader"><label for="OKNGNoCheck">Judge</label></th>
										<th class="SearchResultHeader"><label for="OKNGNoCheck">Reason</label></th>
									@endif
								</tr>
								<!-- OK/NGパターンの場合は空白表示。 -->
								<tr class="SearchResultTable">
									<td class="SearchResultTable" rowspan="2">{{ $ReferToDocumentFrequency }}</td>
									<td class="SearchResultTable" rowspan="2">{{ $Equipment }}</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $NGUnder }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $OFFSETUnder }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $ReferenceValue }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $OFFSETOver }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $NGOver }} @else &nbsp; @endif</td>
									<td class="NowNumber" rowspan="2">{{ $InspectionNo }}</td>
									<!-- <td class="NowNumber" rowspan="2">{{ $ShotNo }}</td> -->
									<!-- <td class="NowNumber" rowspan="2">{{ $InspectionPoint }}</td> -->
									<!-- <td class="NowNumber" rowspan="2">{{ $CavityNo }}</td> -->
									<!-- 通常は白、OFFSETは黄、NGはピンクに変更している。StatusはJavaScriptで処置。 -->
									<td class="{{$StatusColorClass}}" rowspan="2"><span id="Status"></span></td>
									<!-- ■■2017/03/10 Motooka added■■ -->

									{{ Form::hidden("hidJudgeMin", $JudgeMin) }}
									{{ Form::hidden("hidJudgeMax", $JudgeMax) }}

									@if ($InspectionResultInputType == "1")
										<td class="SearchResultTable" rowspan="2">{{ $Xbar }}</td>
										<td class="SearchResultTable" rowspan="2">{{ $Max }}</td>
										<td class="SearchResultTable" rowspan="2">{{ $Min }}</td>
										<td class="SearchResultTable" rowspan="2">{{ $Range }}</td>
										<td class="SearchResultTable" rowspan="2">{{ $Stdev }}</td>
										<!-- 通常は白、No GoodはNG Colorに変更している。JudgeはJavaScriptで処置。 -->
										<td class="{{$JudgeColorClass}}" rowspan="2"><span id="Judge"></span></td>

										<td class="SearchResultTable" rowspan="2"><span id="JudgeReason">
										{{ Form::select('cmbJudgeReason', $arrJudgeReasonList, $JudgeReason, [
												"class"      => "input-medium  MarginReset"
											])
										}}</span>
										</td>
									@endif
									
									{{ Form::hidden("hidNGUnder", $NGUnder) }}
									{{ Form::hidden("hidOFFSETUnder", $OFFSETUnder) }}
									{{ Form::hidden("hidReferenceValue", $ReferenceValue) }}
									{{ Form::hidden("hidOFFSETOver", $OFFSETOver) }}
									{{ Form::hidden("hidNGOver", $NGOver) }}
									
									
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $NGUnderDiff }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $OFFSETUnderDiff }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $ReferenceValueDiff }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $OFFSETOverDiff }} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {{ $NGOverDiff }} @else &nbsp; @endif</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="TDMarginPaddingSetting VAlignTop">
							<label for="zumen">Cutting Check Points</label>
							<!-- 画像をホイールで拡大縮小するScriptをここで読み込んでいる -->
							<script src="../../js/jquery.wheelzoom.js"></script>
							<img src="../../{{ $PictureURL }}" alt="zumen" width="500px" height="500px" id="wheelzoom">
							<script>
								$('#wheelzoom').wheelzoom();
							</script>
						</td>
						<td class="TDMarginPaddingSetting VAlignTop">
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblInspectionResult","Inspection Result",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable">
										<!-- 検査結果入力区分が1：テキスト入力ならテキスト、2：OK/NGならラジオボタン選択式にする -->
										@if ($InspectionResultInputType == "1")
										
											{{ Form::text("txtInspectionResult", $InspectionResult, [
												"maxlength"     => 30,
												"class"         => "MarginReset",
											]) }}
										
										@else
										
										{{ Form::radio('rdiInspectionResult', '1', $InspectionResultRadio1) }}OK　
										{{ Form::radio('rdiInspectionResult', '2', $InspectionResultRadio2) }}NG　
										
										@endif
										
										{{ Form::hidden("hidInspectionResultInputType", $InspectionResultInputType) }}
									</td>
									<td class="InputFormTable">
										{{ Form::submit("Entry", [ 
											"class"   => "RoundControls",
											"name"    => "btnEntry",
											"onClick" => "checkAndSetResultInformation('1')",
										]) }}
									</td>
									<td class="InputFormTable">
										<input type="button" value="Input Clear" onClick="cleartxtInspectionResult()">
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblResultClass","Result Class",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3">
										{{ Form::radio('rdiResultClass', '1', $ResultClass1) }}OK　
										{{ Form::radio('rdiResultClass', '2', $ResultClass2) }}OFFSET　
										{{ Form::radio('rdiResultClass', '3', $ResultClass3) }}NG　
										{{ Form::radio('rdiResultClass', '4', $ResultClass4) }}No Check　
										{{ Form::radio('rdiResultClass', '5', $ResultClass5) }}Line Stop
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader"  Rowspan="2">
										{{ Form::label("lblOffsetNGReason","OFFSET Reason / NG Reason",["class"=>"MarginReset"]) }}
									</th>
									
									<td class="InputFormTable" colspan="3"><span id="OffsetNGBunrui">
										{{ Form::select('cmbOffsetNGBunrui', $arrOffsetNGBunruiList, $OffsetNGBunrui, [
												"class"      => "input-xlarge  MarginReset",
												"onChange"   => "this.form.submit()",
											])
										}}</span>
									</td>
									{{ Form::hidden("hidOffsetNGBunrui", $OffsetNGBunrui) }}
								</tr>
								<tr class="InputFormTable">
									<td class="InputFormTable" colspan="3"><span id="OffsetNGReason">
										{{ Form::select('cmbOffsetNGReason', $arrOffsetNGReasonList, $OffsetNGReason, [
												"class"      => "input-xlarge  MarginReset"
											])
										}}</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblOffsetNGAction","OFFSET Action / NG Action",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3">
										{{ Form::radio('rdiOffsetNGAction', '1', $OffsetNGAction1) }}Running　
										{{ Form::radio('rdiOffsetNGAction', '2', $OffsetNGAction2) }}Stop Line
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblOffsetNGActionDetail","OFFSET Action Detail / NG Action Detail",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3">
										{{ Form::checkbox('chkOffsetNGActionDetailInternal' ,'1' , $OffsetNGActionDetailInternal) }}Internal Approve　
										{{ Form::checkbox('chkOffsetNGActionDetailRequest'  ,'1' , $OffsetNGActionDetailRequest) }}Request Offset　
										{{ Form::checkbox('chkOffsetNGActionDetailTSR'      ,'1' , $OffsetNGActionDetailTSR) }}TSR　
										{{ Form::checkbox('chkOffsetNGActionDetailSorting'  ,'1' , $OffsetNGActionDetailSorting) }}Sorting　
										{{ Form::checkbox('chkOffsetNGActionDetailNoAction' ,'1' , $OffsetNGActionDetailNoAction) }}No Action
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblActionDetails","Action Details",["class"=>"MarginReset"]) }}
									</th>

									<td class="InputFormTable" colspan="3"><span id="ActionDetails">
										{{ Form::select('cmbActionDetails', $arrActionDetailsList, $ActionDetails, [
												"class"      => "input-xlarge  MarginReset"
											])
										}}</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblTecReqNo","Technician Request No.",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3">
										{{ Form::textarea("txtTecReqNo", $TecReqNo, [
											"maxlength"     => 50,
											"rows"          => 1,
											"class"         => "input-xlarge MarginReset",
										]) }}
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblTecReqPerson","Technician Request Person",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3"><span id="TecReqPerson">
										{{ Form::select('cmbTecReqPerson', $arrTecReqPersonList, $TecReqPerson, [
												"class"      => "input-xlarge  MarginReset"
											])
										}}</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblTSRNo","TSR No.",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable" colspan="3">
										{{ Form::textarea("txtTSRNo", $TSRNo, [
											"maxlength"     => 50,
											"rows"          => 1,
											"class"         => "input-xlarge MarginReset",
										]) }}
									</td>
								</tr>
							</table>
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblInspectionNo","Inspection No.",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable">
										{{ Form::text("txtInspectionNo", $InspectionNo, [
											"class" => "input-medium MarginReset",
											"maxlength"     => 10,
										]) }}
										{{ Form::hidden("hidInspectionNo", $hidInspectionNo) }}
										{{ Form::hidden("hidDataRev", $hidDataRev) }}
									</td>
									<td class="InputFormTable">
										{{ Form::submit("Page Move", [ 
											"class" => "RoundControls",
											"name"  => "btnPageMove"
										]) }}
									</td>
									<td class="InputFormTable">
										@if($prevButtonLock == "lock")
											{{ Form::submit("<<", [ 
												"class"    => "RoundControls",
												"name"     => "btnPrevPage",
												"disabled" => "disabled"
											]) }}
										@else
											{{ Form::submit("<<", [ 
												"class"    => "RoundControls",
												"name"     => "btnPrevPage",
											]) }}
										@endif
									</td>
									<td class="InputFormTable">
										@if($nextButtonLock == "lock")
											{{ Form::submit(">>", [ 
												"class"    => "RoundControls",
												"name"     => "btnNextPage",
												"disabled" => "disabled"
											]) }}
										@else
											{{ Form::submit(">>", [ 
												"class"    => "RoundControls",
												"name"     => "btnNextPage",
											]) }}
										@endif
									</td>
									<td class="InputFormTable">
										{{ Form::submit("Return", [ 
											"class" => "RoundControls",
											"name"  => "btnReturn"
										]) }}
									</td>
								</tr>
							</table>
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{{ Form::label("lblInspectionToolClass","Equipment",["class"=>"MarginReset"]) }}
									</th>
									<td class="InputFormTable">
										{{ Form::select('cmbInspectonToolClass', $arrDataListInspectionToolClass, $InspectionToolClass, [
												"class"      => "input-medium MarginReset"
											])
										}}
										{{ Form::hidden("hidEquipment", $InspectionToolClass) }}
									</td>
									<td class="InputFormTable">
										{{ Form::submit("Filter", [ 
											"class" => "RoundControls",
											"name"  => "btnFilter"
										]) }}
									</td>
								</tr>
							</table>
							<!-- SUNLIT用追加 -->
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<td class="InputFormTable" style="display:none">
										{{ Form::submit("FileLoad", [ 
											"class" => "RoundControls",
											"name"  => "btnExcelLoad"
										]) }}
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		{{ Form::close() }}
		<!-- 画面のロード時に、検査結果入力欄にフォーカスするための記述 -->
		<script>
			setDefaultForcus();
		</script>
@stop
