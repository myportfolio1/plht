@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">OFFSET/NG Reason Master Maintenance</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif
		
		{!! Form::open([
			"route"         => "user.ngreasonmaster",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "NgReasonMasterForm",
		])!!}
			<div class="SearchFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Search Condition</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblNGRaasonNameForSearch","OFFSET/NG Reason") !!}
									
									{!! Form::text("txtNGRaasonNameForSearch", $NGRaasonNameForSearch, [
										"class"         => "input-xlarge",
										"maxlength"     => 50,
									]) !!}
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									@if($SearchLock == "Lock")
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnSearch"
										]) !!}
									@else
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"name"  => "btnSearch"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($NewAddLock == "Lock")
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnNewAdd"
										]) !!}
									@else
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"name"  => "btnNewAdd"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($RegistLock == "Lock")
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnResistUpload"
										]) !!}
									@else
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"name"  => "btnResistUpload"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($DeleteLock == "Lock")
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnDelete"
										]) !!}
									@else
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"name"  => "btnDelete"
										]) !!}
									@endif
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@if($EditVisible == "True")
			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Edit Field</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblNGRaasonCDForEntry","ID") !!}
									@if($KeyEditLock == "Lock")
										{!! Form::text("txtNGRaasonIDForEntry", $NGRaasonCDForEntry, [
											"class"         => "input-small",
											"ReadOnly"      => "True",
											"maxlength"     => 3,
										]) !!}
									@else
										{!! Form::text("txtNGRaasonIDForEntry", $NGRaasonCDForEntry, [
											"class"         => "input-small",
											"maxlength"     => 3,
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblNGRaasonNameForEntry","OFFSET/NG Reason") !!}
									{!! Form::text("txtNGRaasonNameForEntry", $NGRaasonNameForEntry, [
										"class"         => "input-xlarge",
										"maxlength"     => 50,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblNGRaasonBunruiForEntry","OFFSET/NG Class") !!}
									{!! Form::select('cmbOffsetNGBunruiForEntry', $arrOffsetNGBunruiList, $OffsetNGBunruiForEntry, [
											"class"      => "input-xlarge"
										])
									!!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblDisplayOrderForEntry","Display Order") !!}
									
									{!! Form::text("txtDisplayOrderForEntry", $DisplayOrderForEntry, [
										"class"         => "input-small",
										"maxlength"     => 11,
									]) !!}
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@endif
		@if ($Pagenator != null)
			<table class="SearchResultTable">
				<thead>
					<tr class="SearchResultTable whitetableframe">
						<th class="SearchResultHeader tablecolumn01">Modify</th>
						<th class="SearchResultHeader tablecolumnNGRaasonCD">ID</th>
						<th class="SearchResultHeader tablecolumnNGRaasonName">OFFSET/NG Reason</th>
						<th class="SearchResultHeader tablecolumnNGRaasonName">OFFSET/NG Class</th>
						<th class="SearchResultHeader tablecolumnDisplayOrder">Display<br>Order</th>
					</tr>
				</thead>
				<tbody>
				
				<?php $cntRow = 0 ?>
				<?php $intCurrentPage = $Pagenator->CurrentPage() ?>
				<?php $intPerPage = $Pagenator->PerPage() ?>
				
				@foreach($Pagenator as $arrDataRow)

					<?php $cntRow += 1 ?>
					
					<!-- 1ページの件数×（現在ページ－1）＋1　～　1ページの件数×現在ページの範囲に該当するデータのみを出力 -->
					@if (
							(($intPerPage * ($intCurrentPage - 1) + 1) <= $cntRow)
							 and ($cntRow <= $intPerPage * ($intCurrentPage))
						)
							<tr class="SearchResultTable">
								<td class="tablecolumn01 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Modify", [ 
										"class"   => "RoundControls",
										"name"    => "btnModify",
										"onClick" => "setHiddenValueForngreasonmaster('" . $arrDataRow->OFFSET_NG_REASON_ID . "')",
									]) !!} 
								</td>
								<td class="tablecolumnNGRaasonCD SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->OFFSET_NG_REASON_ID !!}
								</td>
								<td class="tablecolumnNGRaasonName SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->OFFSET_NG_REASON !!}
								</td>
								<td class="tablecolumnNGRaasonClass SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->OFFSET_NG_BUNRUI_NAME !!}
								</td>
								<td class="tablecolumnDisplayOrder SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->DISPLAY_ORDER !!}
								</td>
							</tr>
					@endif
				
				@endforeach
				<!-- hdnは共通部品でJavaScriptでセットする定義が必要 -->
				{!! Form::hidden("hidPrimaryKey1") !!}

				</tbody>
			</table>
			
			<div class="pagination">
				{!! $Pagenator->render(); !!}
			</div>
			
		@endif
		
		{!! Form::close() !!}
		
		<style type="text/css">
			/* Pagination links */
			.pagination a {
			    color: black;
			    float: left;
			    padding: 8px 16px;
			    text-decoration: none;
			    transition: background-color .3s;
			}

			/* Style the active/current link */
			.pagination a.active {
			    background-color: dodgerblue;
			    color: white;
			}

			/* Add a grey background color on mouse-over */
			.pagination a:hover:not(.active) {background-color: #ddd;}
		</style>

@stop