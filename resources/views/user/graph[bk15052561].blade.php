@extends("layout")
@section("content") 
<div class="header">
	<div class="TitleBar">Graph</div>
</div>
<meta http-equiv="refresh" content="1800">
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif

		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif

		{{ Form::open([
			"route"         => "user.graph",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting form__graph row-fluid",
			"name"          => "GraphgForm",
		])}}
			<div class="container-fluid">
				<div class="SearchFormGroup__graph span8">
					<form class="FormMarginPaddingSetting">
						<fieldset>
							<legend class="FieldsetLegend">Search & Data Output</legend>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblProcessForGraph">Process</label>
									{{ Form::select('cmbProcessForGraph', $arrDataListProcessList, $ProcessForGraph, [
											"class"      => "input-small input-block-level",
											
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblCustomerForGraph">Customer/Supplier</label>
									{{ Form::select('cmbCustomerForGraph', $arrDataListCustomerList, $CustomerForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}						
								</div>
								<div class="control-group span2">
									<label for="lblCheckSheetNoForGraph">Product No.</label>
									{{ Form::select('cmbCheckSheetNoForGraph', $arrDataListProductNoList, $ProductNoForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}	
								</div>
								<div class="control-group span2">
									<label for="lblRevisionNoForGraph">Rev No.</label>
									{{ Form::select('cmbRevisionNoForGraph', $arrDataListRevNoList, $RevNoForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Mold No.</label>
									{{ Form::select('cmbMoldNoForGraph', $arrDataListMoldNoList, $MoldNoForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Team Name</label>
									{{ Form::select('cmbTeamNameForGraph', $arrDataListTeamNoList, $TeamNameForGraph, [
										"class"	=>	"input-small input-block-level"
									]) }}
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblMaterialNameForGraph">Material Name</label>
									{{ Form::select('cmbMaterialNameForGraph', $arrDataListMaterialNameList, $MaterialNameForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMaterialSizeForGraph">Material Size</label>
									{{ Form::select('cmbMaterialSizeForGraph', $arrDataListMaterialSizeList, $MaterialSizeForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span4" id="InspectionDateForm"  style="text-align:center;">
									{{ Form::label("lblInspectionDateFromForGraph","Inspection Date") }}
									<div class="row-fluid">
										<div class="span5">
											{{ Form::text("txtInspectionDateFromForGraph", $InspectionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "startdate",
											]) }}
										</div>
										<div class="span1">
											-
										</div>
										<div class="span5">
											{{ Form::text("txtInspectionDateToForGraph", $InspectionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "enddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span3">
									<label for="lblInspectionDateFromForGraph">Inspection Date</label>
									{{ Form::checkbox('cmbRadioChoosePeriod', 'Old', true, [
										"id" => "cmbRadioChoosePeriod",
										"onchange"	=> "",
										"class" 	=> " input-prepend"
									]) }}
									{{ Form::label("lblRadioChoosePeriod", " Old", array(
										"style"	=> "display: inline-block; vertical-align: sub;",
									)) }}
									{{ Form::text("txtTypeTimeForGraph", '30', [
										"class"         => "input-mini input-prepend",
										"maxlength"     => 3,
										"style"			=> "margin-bottom: 0;"
									]) }}
									{{ Form::label("lblTypeTimeForGraph", " days", array(
										"style"	=> "display: inline-block; vertical-align: sub;",
									)) }}
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span4" id="InspectionDateForm"  style="text-align:center;">
									{{ Form::label("lblInspectionDateFromForGraph","Production Date") }}
									<div class="row-fluid">
										<div class="span5">
											{{ Form::text("txtProductionDateFromForGraph", $ProductionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "startdate",
											]) }}
										</div>
										<div class="span1">
										-
										</div>
										<div class="span5">
											{{ Form::text("txtProductionDateToForGraph", $ProductionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "enddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span2">
									<label for="lblConditionForGraph">Condition</label>
									{{ Form::select('cmbConditionForGraph', $arrDataListConditionList, $ConditionForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									{{ Form::label("lblSelectTypeForFraph", "Select") }}
									{{ Form::select('cmbSelectTypeForGraph', 
											array(
												""					=> "",
												"01" 				=> "Xbar-Min-Max",
												"02" 				=> "Range",
												"03" 				=> "Stdev",
												"04" 				=> "Cpk",
											),$SelectTypeData,[
												"class" => "input-small",
												"id"	=> "cmbSelectTypeForGraph"
											]
									) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Please") }}
									{{ Form::submit("Graph", [ 
										"class" => "RoundControls",
										"name"  => "btnGraph",
										"onclick"	=> "add()",
									]) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Select") }}
									{{ Form::submit("Excel",[ 
										"class" => "RoundControls",
										"style" => "margin-bottom: 10px;",
										"name"  => "btnExcel",
									]) }}
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>				
		</form>
<script src="../../js/datepicker/bootstrap-datetimepicker.js" charset="utf-8"></script>
<script src="../../js/datepicker/bootstrap-datetimepicker.min.js" charset="utf-8"></script>
<script src="../../js/datepicker/moment.js" charset="utf-8"></script>
<script type="text/javascript">
	        var today = new Date();
		    $(".form_datetime").datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			});

		 	$('#startdate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(start > end){
			    	$('#enddate').val(startdate);
			    }
			});

			$('#enddate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(end < start){
			    	$('#startdate').val(enddate);
			    }
			});

			$(document).ready(function() {
				var radioButtons = $("input[type='radio'][name='cmbRadioChoosePeriod']");
				var radioStates = {};
				
				$.each(radioButtons, function(index, rd) {
				    radioStates[rd.value] = $(rd).is(':checked');
				});
				
				radioButtons.click(function() {
				    var val = $(this).val();  
				    $(this).attr('checked', (radioStates[val] = !radioStates[val]));    
				    
				    $.each(radioButtons, function(index, rd) {
				        if(rd.value !== val) {
				            radioStates[rd.value] = false; 
				        }
				        if(radioStates[rd.value] == true){
				        	document.getElementById('InspectionDateForm').style.display = 'none';
				        }
				    });
				});
			});
</script>	
<script src="../../js/charts/chart.min.js" charset="utf-8"></script>
<script src="../../js/charts/chartjs-plugin-annotation.js" charset="utf-8"></script>
<style type="text/css">
	#border{
	  border: solid 0px white;
	}

	.canvas{
	  border: solid 0px blue;
	  margin-left: 20%; 
	  height: 70%;
	  width: 80%;
	}

	table {
		border-collapse: collapse;
		border-spacing: 0;
		width: 80%;
	}

    .dropdown{
    	margin-bottom: 100%;
    }

</style>
<script type="text/javascript">

 window.onload = function () {
 	var SelectTypeData = document.getElementById('cmbSelectTypeForGraph').value;
 	console.log(SelectTypeData);
    if(SelectTypeData.length > 0){
    	if(SelectTypeData == 01){
		 <?php if(isset($arrAverage)){ 
		 	foreach ($arrAverage as $key => $value){ ?>
		 		var ctx = document.getElementById('<?php echo "average".$key; ?>').getContext('2d');
		 	    	new Chart(ctx, {
				    type: 'line',
				    data: {
				    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
				        datasets: [
				        {
				            label: 'Xbar',
				            data: <?php echo (!empty($arrAverage))?json_encode($arrAverage[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: "rgba(148, 184, 255, 1)",
							lineTension: 0, 
							fill: false,  
							borderColor: "rgba(148, 184, 255, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(148, 184, 255, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(148, 184, 255, 1)",
								font: {
									weight: 'bold'
								},
							}
						}
						,
						{
							label: 'Max',
							data: <?php echo (!empty($arrMax))?json_encode($arrMax[$key],JSON_NUMERIC_CHECK):null; ?>,
							backgroundColor: "rgba(218, 165, 32, 0.8)",
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(218, 165, 32, 0.8)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(218, 165, 32, 0.8)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(218, 165, 32, 0.8)",
								font: {
									weight: 'bold'
								},
							}
				        },
						{
							label: 'Min',
							data: <?php echo (!empty($arrMin))?json_encode($arrMin[$key],JSON_NUMERIC_CHECK):null; ?>,
							backgroundColor: "rgba(50, 205, 50, 1)",
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(50, 205, 50, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(50, 205, 50, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(50, 205, 50, 1)",
								font: {
									weight: 'bold'
								},
							}
				        }
				        ,
						{
							label: 'UCL',
							data: <?php echo (!empty($UCLX))?json_encode($UCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
				        }
				        ,
						{
							label: 'LCL',
							type: 'line',
							data: <?php echo (!empty($LCLX))?json_encode($LCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
						}
						,
						{
							label: 'SU',
							data: <?php echo (!empty($UCLS))?json_encode($UCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(255, 0, 0, 0.8)",
							borderDash: [15,2],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(255, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255, 0, 0, 0.8)",
								}
				        },
						{
							label: 'CL',
							type: 'line',
							data: <?php echo (!empty($CLX))?json_encode($CLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
							}
				        ,
						{
							label: 'SL',
							type: 'line',
							data: <?php echo (!empty($LCLS))?json_encode($LCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(255, 0, 0, 0.8)",
							borderDash: [15,2],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(255, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255, 0, 0, 0.8)",
								}
						}	
				        ]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				               		display: true,
				               		max: <?php echo (!empty($arrMaxXbarChart))?json_encode($arrMaxXbarChart[$key],JSON_NUMERIC_CHECK):null; ?>,
				               		min: <?php echo (!empty($arrMinXbarChart))?json_encode($arrMinXbarChart[$key],JSON_NUMERIC_CHECK):null; ?>,
				                }
				            }]
				        }
				    }
				});		
		 <?php }
			}?>
		}else if(SelectTypeData == 02){
			<?php if(isset($arrRange)){ 
		 		foreach ($arrRange as $key => $value){ ?>
		 	    	var ctx = document.getElementById('<?php echo "range".$key; ?>').getContext('2d');
		 	    	new Chart(ctx, {
				    type: 'line',
				    data: {
				    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
				        datasets: [
				        {
				            label: 'Range',
				            data: <?php echo (!empty($arrRange))?json_encode($arrRange[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: "rgba(165, 42, 42, 1)",
							lineTension: 0, 
							fill: false,  
							borderColor: "rgba(165, 42, 42, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(165, 42, 42, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(165, 42, 42, 1)",
								font: {
									weight: 'bold'
								},
							}
						},
						{
							label: 'UCL',
							data: <?php echo (!empty($UCLR))?json_encode($UCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
				        }
				        ,
						{
							label: 'LCL',
							type: 'line',
							data: <?php echo (!empty($LCLR))?json_encode($LCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
						}
						  ,
						{
							label: 'CL',
							type: 'line',
							data: <?php echo (!empty($CLR))?json_encode($CLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
							}	
				        ]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				               		display: true,
				               		max: <?php echo (!empty($arrMaxRangeChart))?json_encode($arrMaxRangeChart[$key],JSON_NUMERIC_CHECK):null; ?>,
				               		min: <?php echo (!empty($arrMinRangeChart))?json_encode($arrMinRangeChart[$key],JSON_NUMERIC_CHECK):null; ?>,
				                }
				            }]
				        }
				    }
				});		
		 <?php 	}
			}?>
		}else if(SelectTypeData == 03){
			<?php if(isset($arrStandard)){ 
		 		foreach ($arrStandard as $key => $value){ ?>
		 			var ctx = document.getElementById('<?php echo "xbar".$key; ?>').getContext('2d');
		 			new Chart(ctx, {
				    type: 'line',
				    data: {
				    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
				        datasets: [
				        {
				            label: 'Stdev',
				            data: <?php echo (!empty($arrStandard))?json_encode($arrStandard[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: "rgba(255, 182, 193, 1)",
							lineTension: 0, 
							fill: false,  
							borderColor: "rgba(255, 182, 193, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(255, 182, 193, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(255, 182, 193, 1)",
								font: {
									weight: 'bold'
								},
							}
						}
						,
						{
							label: 'UCL',
							data: <?php echo (!empty($UCLStdev))?json_encode($UCLStdev[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
				        }
				        ,
						{
							label: 'LCL',
							type: 'line',
							data: <?php echo (!empty($LCLStdev))?json_encode($LCLStdev[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
							}
						  ,
						{
							label: 'CL',
							type: 'line',
							data: <?php echo (!empty($CLS))?json_encode($CLS[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
							}	
				        ]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				               		display: true,
				                }
				            }]
				        }
				    }
				});		
		 	<?php }
		 		}
		 	?>		
		}else{
			<?php if(isset($arrBarCPK)){ 
		    foreach ($arrBarCPK as $key => $value){ 
		    	if(count($value) != "1" and $key[0] != "0"){?>
		    		var ctx = document.getElementById('<?php echo "cpk".$key; ?>').getContext('2d');
		    		var labels = <?php echo (!empty($arrCurveValue))?json_encode($arrCurveValue[$key],JSON_NUMERIC_CHECK):null; ?>;
				new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels:labels,
			        datasets: [{
			            label: 'Histogram',
			            data: <?php echo (!empty($arrBarCPK))?json_encode($arrBarCPK[$key],JSON_NUMERIC_CHECK):null; ?>,
			            backgroundColor: 'rgba(0,191,255,0.2)',
			            borderColor: '	rgba(70,130,180,1)',
			            borderWidth: 0.8,
			            datalabels: {
								align: 'end',
								borderRadius: -0.4,
								color: "rgba(255,255,255,0.0)",
								}
			        }
			        ,
			        {

			          type: 'line',
			          label: 'Bell Curve',
			          data: <?php echo (!empty($arrCurve))?json_encode($arrCurve[$key],JSON_NUMERIC_CHECK):null; ?>,
			          lineTension: 0,  
										fill: false,
										borderColor: "rgba(0, 0, 0, 1)",
										radius: 1,
										borderWidth: 1,
										pointRadius: 4,
										pointBorderColor: "rgba(0, 0, 0, 0)",
										datalabels: {
											align: 'end',
											borderRadius: 0,
											color: "rgba(255,255,255,0.0)",
										}
			        }
			        ]
			    },
			    options: {
			       scales: {
	        			xAxes: [{
	         				stacked : true,
	     					barPercentage: 1.2,
	          				position:'center',
	          				gridLines: {
			                    display:false
			                }
	      				}
	      				,{
	      					stacked : true,
	     					barPercentage: 1.2,
	          				id : 'x-axis-label',
	          				position:'center',
	          				type: 'linear',
			          		gridLines: {
					            display:false
			                },
			                display:false,    
	      				}
	      				],
	      				yAxes: [
	         			{
	            			stacked : false,
	 						ticks: {
	                                max: <?php echo (!empty($indexMax))?json_encode($indexMax[$key],JSON_NUMERIC_CHECK):null; ?>,
	                                min: <?php echo (!empty($indexMin))?json_encode($indexMin[$key],JSON_NUMERIC_CHECK):null; ?>,
	                            }
	         			}]
	    			},
	    			annotation: {
	                    drawTime: "afterDraw",
	                    annotations: [
	                    {
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label', 
							value:  <?php echo (!empty($indexAvg))?json_encode($indexAvg[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content:<?php 
				                    if(!empty($arrAvg)){
				                    	if(($USL[$key] == $arrAvg[$key]) and ($USL[$key] == $LSL[$key]) and ($arrAvg[$key] == $LSL[$key])){
				                    		echo json_encode("USL : Average : LSL : ".$arrAvg[$key],JSON_NUMERIC_CHECK); 
				                    	}elseif(($USL[$key] == $arrAvg[$key]) and ($arrAvg[$key] != $LSL[$key])){
				                    		echo json_encode("USL : Average : ".$arrAvg[$key],JSON_NUMERIC_CHECK);
				                    	}elseif(($LSL[$key] == $arrAvg[$key]) and ($arrAvg[$key] != $USL[$key])){
				                    		echo json_encode("Average : LSL : ".$arrAvg[$key],JSON_NUMERIC_CHECK);
				                    	}else{
											echo json_encode("Average : ".$arrAvg[$key],JSON_NUMERIC_CHECK);
										}
				                    } else{  
				                        echo "";
				                    }  ?>,
				                enabled: true,
				                position: "top",
				                },
				        },{
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label', 
							value:  <?php echo (!empty($indexLCL))?json_encode($indexLCL[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content:<?php 
				                    if(!empty($arrAvg)){
				                    	if(($LSL[$key] == $USL[$key]) and ($arrAvg[$key] != $LSL[$key])){
				                    		echo json_encode("USL : LSL : ".$LSL[$key],JSON_NUMERIC_CHECK);
				                    	}else{
											echo json_encode("LSL : ".$LSL[$key],JSON_NUMERIC_CHECK);
										}
				                    } else{  
				                        echo "";
				                    }  ?>,
				                enabled: true,
				                position: "center",
				                },
				          },{
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label', 
							value:  <?php echo (!empty($indexUCL))?json_encode($indexUCL[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content:<?php 
				                    if(!empty($arrAvg)){
											echo json_encode("USL : ".$USL[$key],JSON_NUMERIC_CHECK);
				                    } else{  
				                        echo "";
				                    }  ?>,
				                enabled: true,
				                position: "bottom",
				                },        
						},{
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexMinutOneSigma))?json_encode($indexMinutOneSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "-1σ",
				            //         content: <?php 
								        // if(!empty($arrMinutOneSigma)){
								        //     if($arrMinutOneSigma[$key] != $arrMinutOneSigma[$key]){ 
								        //        	echo json_encode("-1σ : ".$arrMinutOneSigma[$key],JSON_NUMERIC_CHECK); 
								        //     }else{ 
								        //         echo json_encode("-1σ : ".$arrMinutOneSigma[$key],JSON_NUMERIC_CHECK); 
								        //     }
								        // } else{  
								        //     echo null;
								        // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexMinutTwoSigma))?json_encode($indexMinutTwoSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "-2σ",
				            //         content: <?php 
								        // if(!empty($arrMinutTwoSigma)){
								        //     if($arrMinutTwoSigma[$key] != $arrMinutTwoSigma[$key]){ 
								        //        	echo json_encode("-2σ : ".$arrMinutTwoSigma[$key],JSON_NUMERIC_CHECK); 
								        //     }else{ 
								        //         echo json_encode("-2σ : ".$arrMinutTwoSigma[$key],JSON_NUMERIC_CHECK); 
								        //     }
								        // } else{  
								        //     echo null;
								        // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexMinutThreeSigma))?json_encode($indexMinutThreeSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "-3σ",
				                    // content: <?php 
					                   //  if(!empty($arrMinutThreeSigma)){
					                   //      if($arrMinutThreeSigma[$key] != $arrMinutThreeSigma[$key]){ 
					                   //          echo json_encode("-3σ : ".$arrMinutThreeSigma[$key],JSON_NUMERIC_CHECK); 
					                   //      }else{ 
					                   //          echo json_encode("-3σ : ".$arrMinutThreeSigma[$key],JSON_NUMERIC_CHECK); 
					                   //      }
					                   //  } else{  
					                   //      echo null;
					                   //  }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexMinutFourSigma))?json_encode($indexMinutFourSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "-4σ",
				                    // content: <?php 
				                    // if(!empty($arrMinutFourSigma)){
				                    //     if($arrMinutFourSigma[$key] != $arrMinutFourSigma[$key]){ 
				                    //         echo json_encode("-4σ : ".$arrMinutFourSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }else{ 
				                    //         echo json_encode("-4σ : ".$arrMinutFourSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }
				                    // } else{  
				                    //     echo null;
				                    // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexPlusFourSigma))?json_encode($indexPlusFourSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "+4σ",
				                    // content: <?php 
				                    // if(!empty($arrPlusFourSigma)){
				                    //     if($arrPlusFourSigma[$key] != $arrPlusFourSigma[$key]){ 
				                    //         echo json_encode("+4σ : ".$arrPlusFourSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }else{ 
				                    //         echo json_encode("+4σ : ".$arrPlusFourSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }
				                    // } else{  
				                    //     echo null;
				                    // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexPlusThreeSigma))?json_encode($indexPlusThreeSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "+3σ",
				                    // content: <?php 
				                    // if(!empty($arrPlusThreeSigma)){
				                    //     if($arrPlusThreeSigma[$key] != $arrPlusThreeSigma[$key]){ 
				                    //         echo json_encode("+3σ : ".$arrPlusThreeSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }else{ 
				                    //         echo json_encode("+3σ : ".$arrPlusThreeSigma[$key],JSON_NUMERIC_CHECK); 
				                    //     }
				                    // } else{  
				                    //     echo null;
				                    // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexPlusTwoSigma))?json_encode($indexPlusTwoSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "+2σ",
				                    // content: <?php 
				                    // if(!empty($arrPlusTwoSigma)){
				                        // if($arrPlusTwoSigma[$key] != $arrPlusTwoSigma[$key]){ 
				                        //     // echo json_encode("+2σ : ".$arrPlusTwoSigma[$key],JSON_NUMERIC_CHECK); 
				                        // }else{ 
				                        //     // echo json_encode("+2σ : ".$arrPlusTwoSigma[$key],JSON_NUMERIC_CHECK); 
				                        // }
				                    // }else{  
				                        // echo null;
				                    // }  
				                    ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}, {
								type: 'line',
								mode: 'vertical' ,
								scaleID: 'x-axis-label', 
								value:  <?php echo (!empty($indexPlusOneSigma))?json_encode($indexPlusOneSigma[$key],JSON_NUMERIC_CHECK):null; ?>,
								borderColor: '#A9A9A9',
								borderWidth: 2,
								label: {
				                    backgroundColor: '#A9A9A9',
				                    content: "+1σ",
				                    // content: <?php 
				                    // if(!empty($arrPlusOneSigma)){
				                        // if($arrPlusOneSigma[$key] != $arrPlusOneSigma[$key]){ 
				                        //     echo json_encode("+1σ : ".$arrPlusOneSigma[$key],JSON_NUMERIC_CHECK); 
				                        // }else{ 
				                        //     echo json_encode("+1σ : ".$arrPlusOneSigma[$key],JSON_NUMERIC_CHECK); 
				                        // }
				                    // } else{  
				                        // echo null;
				                    // }  ?>,
				                    enabled: true,
				                    position: "bottom",
				               	},
				                borderDash: [5,2]
						}
	                    ]
	                },    
	                legend : {
	      				position: 'right',
	          			labels:{
	        				boxWidth:20,
	        			}
	      			},
	      			maintainAspectRatio: false,
	      			scaleBeginAtZero: true,
			    }
			});
		    <?php } } 
        	} ?>
		}
    }
}

</script>	
<div class="container-fluid" id="widget">
   @if($SelectTypeData != null)
	@if($SelectTypeData == 01)
		@if($arrAverage != null)
		<div class="row"><div><center><h4><b>Xbar-Max-Min Chart</b></h4></center></div></div>
		<?php foreach ($arrAverage as $key => $value){ ?>
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="border">
					<div><center><h5><b>INSPECTION POINT <?php echo $arrPoint1Dimention[$key]; ?> CAVITY <?php echo $arrCavity1Dimention[$key]; ?> POSITION <?php echo $arrWhere1Dimention[$key]; ?> </b></h5></center></div>
					<table>
						<tr>
							<th width="50%">
								<canvas class="canvas" id="<?php echo "average".$key; ?>"></canvas>
							</th>
							<th width="10%"  >
							</th>
						</tr>
					</table><br>
				</div>	
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
			</div>			    
		<?php } ?>
		@endif
	@elseif($SelectTypeData == 02)
		@if($arrRange != null)
		<div class="row"><div><center><h4><b>Range Chart</b></h4></center></div></div>
		<?php foreach ($arrRange as $key => $value){ ?>
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="border">
					<div><center><h5><b>INSPECTION POINT <?php echo $arrPoint1Dimention[$key]; ?> CAVITY <?php echo $arrCavity1Dimention[$key]; ?> POSITION <?php echo $arrWhere1Dimention[$key]; ?> </b></h5></center></div>
					<table>
						<tr>
							<th width="50%">
								<canvas class="canvas" id="<?php echo "range".$key; ?>"></canvas>
							</th>
							<th width="10%"  >
							</th>
						</tr>
					</table><br>
				</div>	
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
			</div>			    
		<?php } ?>
		@endif
	@elseif($SelectTypeData == 03)
		@if($arrStandard != null)
		<div class="row"><div><center><h4><b>Stdev Chart</b></h4></center></div></div>
		<?php foreach ($arrStandard as $key => $value){ ?>
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="border">
					<div><center><h5><b>INSPECTION POINT <?php echo $arrPoint1Dimention[$key]; ?> CAVITY <?php echo $arrCavity1Dimention[$key]; ?> POSITION <?php echo $arrWhere1Dimention[$key]; ?> </b></h5></center></div>
					<table>
						<tr>
							<th width="50%">
								<canvas class="canvas" id="<?php echo "xbar".$key; ?>"></canvas>
							</th>
							<th width="10%"  >
							</th>
						</tr>
					</table><br>
				</div>	
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
			</div>			    
		<?php } ?>
		@endif
	@else
		@if($arrBarCPK != null)
			<?php foreach ($arrBarCPK as $key => $value){
				if(count($value) != "1" and $key[0] != "0"){
			 ?>
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
					<div class="col-xs-4 col-sm-4 col-md-4" id="border">
						<div><center><h5><b>INSPECTION POINT {{$arrPointCPK[$key]}} CAVITY {{$arrCavityCPK[$key]}} POSITION {{$arrWhereCPK[$key]}}</b></h5></center></div>
						<div><center><h6><b>LSL {{ $arrAverageLCLCPK[$key]}}  USL {{ $arrAverageUCLCPK[$key]}}  Average {{$arrAvg[$key]}} Standard Deviation {{$arrStdev[$key]}}</b></h6></center></div>
						<table>
							<tr>
							<th width="100%">
								<canvas class="canvas " id="<?php echo "cpk".$key;?>"></canvas>
							</th>
							</tr>
						</table><br>
					</div>	
					<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				</div>
		    <?php } } ?>
		@endif	
	@endif	
@endif 
</div>	
@stop
