@extends("layout")
@section("content") 
<div class="header">
	<div class="TitleBar">Graph</div>
</div>
<meta http-equiv="refresh" content="1800">
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif

		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif

		{{ Form::open([
			"route"         => "user.graph",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting form__graph row-fluid",
			"name"          => "GraphgForm",
		])}}
			<div class="container-fluid">
				<div class="SearchFormGroup__graph span8">
					<form class="FormMarginPaddingSetting">
						<fieldset>
							<legend class="FieldsetLegend">Search & Data Output</legend>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblProcessForGraph">Process</label>
									{{ Form::select('cmbProcessForGraph', $arrProcessList, $ProcessForGraph, [
											"class"      => "input-small input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblCustomerForGraph">Customer/Supplier</label>
									{{ Form::select('cmbCustomerForGraph', $arrCustomerList, $CustomerForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}						
								</div>
								<div class="control-group span2">
									<label for="lblCheckSheetNoForGraph">Product No.</label>
									{{ Form::select('cmbCheckSheetNoForGraph', $arrProductNoList, $ProductNoForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}	
								</div>
								<div class="control-group span2">
									<label for="lblRevisionNoForGraph">Rev No.</label>
									{{ Form::select('cmbRevisionNoForGraph', $arrRevNoList, $RevNoForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Mold No.</label>
									{{ Form::select('cmbMoldNoForGraph', $arrMoldNoList, $MoldNoForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Team Name</label>
									{{ Form::select('cmbTeamNameForGraph', $arrTeamNameList, $TeamNameForGraph,[
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblMaterialNameForGraph">Inspection Point</label>
									{{ Form::select('cmbInspectionPointForGraph', $arrInspectionPointList, $InspectionPointForGraph,[
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMaterialSizeForGraph">Cavity</label>
									{{ Form::select('cmbCavityForGraph', $arrCavityNoList,$CavityNoForGraph,[
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span4" id="InspectionDateForm"  style="text-align:center;">
									{{ Form::label("lblInspectionDateFromForGraph","Inspection Date") }}
									<div class="row-fluid">
										<div class="span2">
											{{ Form::checkbox('cmbRadioChooseInspectionDate', 'Old', true, [
												"id" => "cmbRadioChooseInspectionDate",
												"onchange"	=> "",
												"class" 	=> " input-prepend"
											]) }}
										</div>
										<div class="span4">
											{{ Form::text("txtInspectionDateFromForGraph", $InspectionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "startdate",
											]) }}
										</div>
										<div class="span1">
											-
										</div>
										<div class="span4">
											{{ Form::text("txtInspectionDateToForGraph", $InspectionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "enddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span2">
									<label for="lblInspectionDateFromForGraph">Inspection Date</label>
									{{ Form::checkbox('cmbRadioChoosePeriod', 'Old', "", [
											"id" => "cmbRadioChoosePeriod",
											"onchange"	=> "",
											"class" 	=> " input-prepend"
									]) }}
									{{ Form::label("lblRadioChoosePeriod", " Old", array(
										"style"	=> "display: inline-block; vertical-align: sub;",
									)) }}
									{{ Form::text("txtTypeTimeForGraph", '30', [
										"class"         => "input-mini input-prepend",
										"maxlength"     => 3,
										"style"			=> "margin-bottom: 0;",
										"placeholder"   => "Days",
										"id"            => "txtTypeTimeForGraph",
									]) }}
								</div>
								<div class="control-group span2">
									{{ Form::label("lblInspectionDateFromForGraph","Inspection Time") }}
									{{ Form::select('cmbTimeIDForGraph', $arrTimeIDList, $TimeIDForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span4" id="InspectionDateForm"  style="text-align:center;">
								
									{{ Form::label("lblInspectionDateFromForGraph","Production Date") }}
									<div class="row-fluid">
										<div class="span2">
											{{ Form::checkbox('cmbRadioChooseProductionDate', 'Old', true, [
												"id" => "cmbRadioChooseProductionDate",
												"onchange"	=> "",
												"class" 	=> " input-prepend"
											]) }}
										</div>
										<div class="span4">
											{{ Form::text("txtProductionDateFromForGraph", $ProductionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "productstartdate",
											]) }}
										</div>
										<div class="span1">
										-
										</div>
										<div class="span4">
											{{ Form::text("txtProductionDateToForGraph", $ProductionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "productenddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span2">
									<label for="lblConditionForGraph">Condition</label>
									{{ Form::select('cmbConditionForGraph', $arrConditionList, $ConditionForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									{{ Form::label("lblSelectTypeForFraph", "Select") }}
									{{ Form::select('cmbSelectTypeForGraph', 
											array(
												""					=> "",
												"01" 				=> "Xbar-R",
												"02" 				=> "Range",
												"03" 				=> "Stdev",
												"04" 				=> "Cpk",
											),$SelectTypeData,[
												"class" => "input-small",
												"id"	=> "cmbSelectTypeForGraph"
											]
									) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Please") }}
									{{ Form::submit("Graph", [ 
										"class" => "RoundControls",
										"name"  => "btnGraph",
										"onclick"	=> "add()",
									]) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Select") }}
									{{ Form::submit("Excel",[ 
										"class" => "RoundControls",
										"style" => "margin-bottom: 10px;",
										"name"  => "btnExcel",
									]) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Buttons") }}
									<button id="save">Image</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>				
		</form>
<script src="../../js/datepicker/bootstrap-datetimepicker.js" charset="utf-8"></script>
<script src="../../js/datepicker/bootstrap-datetimepicker.min.js" charset="utf-8"></script>
<script src="../../js/datepicker/moment.js" charset="utf-8"></script>
<script type="text/javascript">
	      
		    $(".form_datetime").datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			});

		 	$('#startdate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(start > end){
			    	$('#enddate').val(startdate);
			    }
			});

			$('#enddate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(end < start){
			    	$('#startdate').val(enddate);
			    }
			});

			$(document).ready(function() {
				var radioButtons = $("input[type='radio'][name='cmbRadioChoosePeriod']");
				var radioStates = {};
				
				$.each(radioButtons, function(index, rd) {
				    radioStates[rd.value] = $(rd).is(':checked');
				});
				
				radioButtons.click(function() {
				    var val = $(this).val();  
				    $(this).attr('checked', (radioStates[val] = !radioStates[val]));    
				    
				    $.each(radioButtons, function(index, rd) {
				        if(rd.value !== val) {
				            radioStates[rd.value] = false; 
				        }
				        if(radioStates[rd.value] == true){
				        	document.getElementById('InspectionDateForm').style.display = 'none';
				        }
				    });
				});
			});

			$(document).ready(function () {
				$("#cmbRadioChoosePeriod").prop('checked', false);
		  		$("#txtTypeTimeForGraph").prop("disabled", true);
		  		$("#cmbRadioChooseInspectionDate").prop('checked', false);
	  			$("#enddate").prop("disabled", true);
	  			$("#startdate").prop("disabled", true);
		  		$("#cmbRadioChooseProductionDate").prop('checked', true);
		  		$("#productstartdate").prop("disabled", false);
	  			$("#productenddate").prop("disabled", false);
	  			$("#startdate").val("");
				$("#enddate").val("");
	  			$("#txtTypeTimeForGraph").val("");

				var day = new Date();
				var today = moment(day).format("DD-MM-YYYY");

	        $("#cmbRadioChoosePeriod").click(
	            function () {
	            	if ($(this).is(":checked"))
					{
						$(this).prop('checked', true);
						$("#startdate").prop("disabled", true);
						$("#enddate").prop("disabled", true);
						$("#productstartdate").prop("disabled", true);
						$("#productenddate").prop("disabled", true);
						$("#txtTypeTimeForGraph").prop("disabled", false);
						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");
						$("#cmbRadioChooseProductionDate").prop('checked', false);
						$("#productstartdate").val("");
						$("#productenddate").val("");
					}else{
						$(this).prop('checked', false);
						$("#startdate").prop("disabled", true);
						$("#enddate").prop("disabled", true);
						$("#productstartdate").prop("disabled", false);
						$("#productenddate").prop("disabled", false);
						$("#txtTypeTimeForGraph").prop("disabled", true);
						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");
						$("#cmbRadioChooseProductionDate").prop('checked', true);
						
						$("#productstartdate").val(today);
						$("#productenddate").val(today);
					}
	            }            
	        );

	        $("#cmbRadioChooseInspectionDate").click(
	        	function () {
					if ($(this).is(":checked"))
					{
						$(this).prop('checked', true);
						$("#startdate").prop("disabled", false);
						$("#enddate").prop("disabled", false);
						$("#productstartdate").prop("disabled", true);
						$("#productenddate").prop("disabled", true);
						$("#txtTypeTimeForGraph").prop("disabled", false);
						$("#cmbRadioChooseInspectionDate").prop('checked', true);
						$("#startdate").val(today);
						$("#enddate").val(today);
						$("#cmbRadioChooseProductionDate").prop('checked', false);
						$("#productstartdate").val("");
						$("#productenddate").val("");
					}else{
						$(this).prop('checked', false);
						$("#startdate").prop("disabled", true);
						$("#enddate").prop("disabled", true);
						$("#productstartdate").prop("disabled", true);
						$("#productenddate").prop("disabled", true);
						$("#txtTypeTimeForGraph").prop("disabled", true);
						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");
						$("#cmbRadioChooseProductionDate").prop('checked', false);
						// $("#productstartdate").val(today);
						// $("#productenddate").val(today);
						$("#productstartdate").val("");
						$("#productenddate").val("");
					}
	        	}
	        );

	        $("#cmbRadioChooseProductionDate").click(
	        	function () {
					if ($(this).is(":checked"))
					{
						// $(this).prop('checked', true);
						// $("#productstartdate").prop("disabled", false);
		  		// 	 	$("#productenddate").prop("disabled", false);
					}else{
						// $(this).prop('checked', false);
						// $("#productstartdate").prop("disabled", true);
		  		// 	 	$("#productenddate").prop("disabled", true);
		  		// 	 	$("#productstartdate").val("");
						// $("#productenddate").val("");
					}
	        	}
	        );
	    });
</script>	
<script src="../../js/charts/chart.min.js" charset="utf-8"></script>
<script src="../../js/charts/chartjs-plugin-annotation.js" charset="utf-8"></script>
<style type="text/css">
	#border{
	  border: solid 0px white;
	}

	.canvas{
	  border: solid 0px blue;
	  margin-left: 20%; 
	  height: 70%;
	  width: 80%;
	}

	table {
		border-collapse: collapse;
		border-spacing: 0;
		width: 80%;
	}

    .dropdown{
    	margin-bottom: 100%;
    }

</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

 @if($SelectTypeData != null)
	@if($SelectTypeData == 01)
		@if($arrAverage != null)
		<div class="row"><div><center><h4><b>Xbar-R Chart</b></h4></center></div></div>
		<?php foreach ($arrAverage as $key => $value){ ?>
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="border">
					<div><center><h5><b>INSPECTION POINT <?php echo $arrPoint1Dimention[$key]; ?> CAVITY <?php echo $arrCavity1Dimention[$key]; ?> @if($arrWhere1Dimention[$key] != "0") POSITION <?php echo $arrWhere1Dimention[$key];   ?> @endif</b></h5></center></div>
					<table>
						<tr>
							<th width="50%">
								<canvas class="canvas" id="<?php echo "average".$key; ?>"></canvas>
								<!-- <canvas class="canvas" id="<?php //echo "NoBidsChart"; ?>"></canvas> -->
								
							</th>
							<th width="10%"  >
							</th>
						</tr>
					</table><br>
				</div>	
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
			</div>	
		<!-- <div class="row"><div><center><h4><b>Range Chart</b></h4></center></div></div> -->
		
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
				<div class="col-xs-4 col-sm-4 col-md-4" id="border">
					<!-- <div><center><h5><b>INSPECTION POINT <?php //echo $arrPoint1Dimention[$key]; ?> CAVITY <?php //echo $arrCavity1Dimention[$key]; ?> POSITION <?php //echo $arrWhere1Dimention[$key]; ?> </b></h5></center></div> -->
					<table>
						<tr>
							<th width="50%">
								<canvas class="canvas" id="<?php echo "range".$key; ?>"></canvas>
							</th>
							<th width="10%"  >
							</th>
						</tr>
					</table><br>
				</div>	
				<div class="col-xs-4 col-sm-4 col-md-4" id="border"></div>
			</div>			    
		<?php } ?>
		@endif
	@endif
@endif		

<!-- <canvas class="canvas" id="NoBidsChart"></canvas> -->
<script type="text/javascript">
	window.onload = function () {
		// draw background
		var backgroundColor = 'white';
		Chart.plugins.register({
		    beforeDraw: function(c) {
		        var ctx = c.chart.ctx;
		        ctx.fillStyle = backgroundColor;
		        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
		    }
		});

        var SelectTypeData = document.getElementById('cmbSelectTypeForGraph').value;
        if(SelectTypeData.length > 0){
    		if(SelectTypeData == 01){
    			<?php if(isset($arrAverage)){ 
		 			foreach ($arrAverage as $key => $value){ ?>
				// chart
				// var canvas = $('#NoBidsChart').get(0);
				var canvas = $('#<?php echo "average".$key; ?>').get(0);
				// var canvas = document.getElementById('<?php //echo "average".$key; ?>').getContext('2d');
				var myChart = new Chart(canvas, {
				    type: 'line',
				    data: {
				    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
				        datasets: [
				        {
				            label: 'Xbar',
				            data: <?php echo (!empty($arrAverage))?json_encode($arrAverage[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: "rgba(148, 184, 255, 1)",
							lineTension: 0, 
							fill: false,  
							borderColor: "rgba(148, 184, 255, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(148, 184, 255, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(148, 184, 255, 1)",
								font: {
									weight: 'bold'
								},
							}
						},
						{
							label: 'Max',
							data: <?php echo (!empty($arrMax))?json_encode($arrMax[$key],JSON_NUMERIC_CHECK):null; ?>,
							backgroundColor: "rgba(218, 165, 32, 0.8)",
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(218, 165, 32, 0.8)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(218, 165, 32, 0.8)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(218, 165, 32, 0.8)",
								font: {
									weight: 'bold'
								},
							}
				        },
						{
							label: 'Min',
							data: <?php echo (!empty($arrMin))?json_encode($arrMin[$key],JSON_NUMERIC_CHECK):null; ?>,
							backgroundColor: "rgba(50, 205, 50, 1)",
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(50, 205, 50, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(50, 205, 50, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(50, 205, 50, 1)",
								font: {
									weight: 'bold'
								},
							}
				        }  
				        ,
						{
							label: 'UCL',
							data: <?php echo (!empty($UCLX))?json_encode($UCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
				        }
				         ,
						{
							label: 'LCL',
							type: 'line',
							data: <?php echo (!empty($LCLX))?json_encode($LCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
						},
						{
							label: 'SU',
							data: <?php echo (!empty($UCLS))?json_encode($UCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(255, 0, 0, 0.8)",
							borderDash: [15,2],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(255, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255, 0, 0, 0.8)",
								}
				        },
				        {
							label: 'XAve',
							type: 'line',
							data: <?php echo (!empty($CLX))?json_encode($CLX[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
						},
						{
							label: 'SL',
							type: 'line',
							data: <?php echo (!empty($LCLS))?json_encode($LCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(255, 0, 0, 0.8)",
							borderDash: [15,2],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(255, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255, 0, 0, 0.8)",
								}
						}		
				        ]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				               		display: true,
				                }
				            }]
				        }
				    }
				});		
				<?php }
			}?>

			<?php if(isset($arrRange)){ 
		 		foreach ($arrRange as $key => $value){ ?>
		 	    	var ctx = document.getElementById('<?php echo "range".$key; ?>').getContext('2d');
		 	    	new Chart(ctx, {
				    type: 'line',
				    data: {
				    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
				        datasets: [
				        {
				            label: 'Range',
				            data: <?php echo (!empty($arrRange))?json_encode($arrRange[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: "rgba(165, 42, 42, 1)",
							lineTension: 0, 
							fill: false,  
							borderColor: "rgba(165, 42, 42, 1)",
							borderWidth: 2,
							pointRadius: 3,
							pointBorderColor: "rgba(165, 42, 42, 1)",
							datalabels: {
								align: 'end',
								borderRadius: 1,
								color: "rgba(165, 42, 42, 1)",
								font: {
									weight: 'bold'
								},
							}
						}
						,
						{
							label: 'UCL',
							data: <?php echo (!empty($UCLR))?json_encode($UCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
				        }
				        ,
						{
							label: 'LCL',
							type: 'line',
							data: <?php echo (!empty($LCLR))?json_encode($LCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(48, 48, 48, 0.8)",
							borderDash: [6,4],
							radius: 1,
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(48, 48, 48, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
						}
						  ,
						{
							label: 'CL',
							type: 'line',
							data: <?php echo (!empty($CLR))?json_encode($CLR[$key],JSON_NUMERIC_CHECK):null; ?>,
							lineTension: 0,  
							fill: false,
							borderColor: "rgba(0, 0, 0, 1)",
							borderWidth: 1,
							pointStyle: 'line',
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
								}
							}	
				        ]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				               		display: true,
				               		min: 0,
				                }
				            }]
				        }
				    }
				});		
		 <?php 	}
			}?>

			}
		}		

		// save as image
		$('#save').click(function() {
		    canvas.toBlob(function(blob) {
		        saveAs(blob, "graph.jpg");
		    });
		});
	}


</script>	
@stop
