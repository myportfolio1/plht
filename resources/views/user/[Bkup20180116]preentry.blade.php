@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Inspection Result Pre-Entry</div>
		</div>

		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif

		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif

		{{ Form::open([
			"route"         => "user/preentry",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "PreEntryForm",
		])}}

        <div class="InputFormGroup">
			<table>
				<tr>
					<td class="TDMarginPaddingSetting">
						<table class="SearchResultTable">
							<tr class="SearchResultTable">
								<th class="SearchResultHeader">Master No.</th>
								<th class="SearchResultHeader">Process</th>
								<th class="SearchResultHeader">Customer/Supplier</th>
								<th class="SearchResultHeader">Check Sheet No.</th>
								<th class="SearchResultHeader">Check Sheet Title</th>
								<th class="SearchResultHeader">Revision No.</th>
								<th class="SearchResultHeader">Part No.</th>
								<th class="SearchResultHeader">Part Name</th>
								<th class="SearchResultHeader">M/C No.</th>
							</tr>
							<tr class="SearchResultTable">
								<td class="SearchResultTable">{{ $MasterNo }}</td>
								<td class="SearchResultTable">{{ $ProcessName }}</td>
								<td class="SearchResultTable">{{ $CustomerName }}</td>
								<td class="SearchResultTable">{{ $CheckSheetNo }}</td>
								<td class="SearchResultTable">{{ $CheckSheetName }}</td>
								<td class="SearchResultTable">{{ $RevisionNo }}</td>
								<td class="SearchResultTable">{{ $ItemNo }}</td>
								<td class="SearchResultTable">{{ $ItemName }}</td>
								<td class="SearchResultTable">{{ $MachineNoName }}</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td class="TDMarginPaddingSetting">
						<table class="SearchResultTable">
							<tr class="SearchResultTable">
								<th class="SearchResultHeader">Inspector Code</th>
								<th class="SearchResultHeader">Inspector Name</th>
								<th class="SearchResultHeader">Inspection Date</th>
								<th class="SearchResultHeader">Condition</th>
								<th class="SearchResultHeader">Inspection Time</th>
								<th class="SearchResultHeader">Model Name</th>
								<th class="SearchResultHeader">Drawing No.</th>
								<th class="SearchResultHeader">ISO Control No.</th>
							</tr>
							<tr class="SearchResultTable">
								<td class="SearchResultTable">{{ $InspectorCode }}</td>
								<td class="SearchResultTable">{{ $InspectorName }}</td>
								<td class="SearchResultTable">{{ $InspectionDate }}</td>
								<td class="SearchResultTable">{{ $ConditionName }}</td>
								<td class="SearchResultTable">{{ $InspectionTimeName }}</td>
								<td class="SearchResultTable">{{ $ModelName }}</td>
								<td class="SearchResultTable">{{ $DrawingNo }}</td>
								<td class="SearchResultTable">{{ $ISOKanriNo }}</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="TDMarginPaddingSetting">
						<table class="SearchResultTable">
							<tr class="SearchResultTable">
								<th class="SearchResultHeader">Material Name</th>
								<th class="SearchResultHeader">Material Size</th>
								<th class="SearchResultHeader">Product Weight</th>
								<th class="SearchResultHeader">Heat Treatment Type</th>
								<th class="SearchResultHeader">Plating Kind</th>
							</tr>
							<tr class="SearchResultTable">
								<td class="SearchResultTable">{{ $MaterialName }}</td>
								<td class="SearchResultTable">{{ $MaterialSize }}</td>
								<td class="SearchResultTable">{{ $ProductWeight }}</td>
								<td class="SearchResultTable">{{ $HeatTreatmentType }}</td>
								<td class="SearchResultTable">{{ $PlatingKind }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<fieldset>
				<table>
					<tr>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblMaterialLotNoForEntry","Material Lot No.") }}
							{{ Form::text("txtMaterialLotNoForEntry", $MaterialLotNoForEntry, [
								"class"         => "input-large",
								"maxlength"     => 30,
							]) }}
						</td>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblCertificateNoForEntry","Certificate No.") }}
							{{ Form::text("txtCertificateNoForEntry", $CertificateNoForEntry, [
								"class"         => "input-large",
								"maxlength"     => 30,
							]) }}
						</td>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblPONoForEntry","P/O No.") }}
							{{ Form::text("txtPONoForEntry", $PONoForEntry, [
								"class"         => "input-large",
								"maxlength"     => 30,
							]) }}
						</td>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblInvoiceNoForEntry","Invoice No.") }}
							{{ Form::text("txtInvoiceNoForEntry", $InvoiceNoForEntry, [
								"class"         => "input-large",
								"maxlength"     => 30,
							]) }}
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblQuantityCoilForEntry","Quantity Coil") }}
							{{ Form::text("txtQuantityCoilForEntry", $QuantityCoilForEntry, [
								"class"         => "input-small",
								"maxlength"     => 7,
							]) }}
						</td>
						<td class="TDMarginPaddingSetting">
							{{ Form::label("lblQuantityWeightForEntry","Quantity Weight") }}
							{{ Form::text("txtQuantityWeightForEntry", $QuantityWeightForEntry, [
								"class"         => "input-small",
								"maxlength"     => 7,
							]) }}
						</td>
						<td class="TDMarginPaddingSetting">
								{{ Form::label("lblLotNoForEntry","Lot No.") }}
								{{ Form::text("txtLotNoForEntry", $LotNoForEntry, [
									"class"         => "input-large",
									"maxlength"     => 30,
								]) }}
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="TDMarginPaddingSetting">
								{{ Form::submit("Next", [ 
									"class" => "RoundControls",
									"name"  => "btnNext"
								]) }}
						</td>
						<td class="TDMarginPaddingSetting">
								{{ Form::submit("Return", [ 
									"class" => "RoundControls",
									"name"  => "btnReturn"
								]) }}
						</td>
					</tr>
				</table>
			</fieldset>
		</div>

		{{ Form::close() }}

@stop
