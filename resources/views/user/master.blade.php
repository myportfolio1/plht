@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Master Data Download & Upload</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif
		
		<!-- ファイルアップロードがある場合は、files=trueの設定が必要 -->
		{!! Form::open([
			"route"         => "user.master",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "MasterForm",
			"files"         => true,
		])!!}
			<div class="InputFormGroup">
				<table>
					<tr>
						<td>
							{!! Form::label("lblMasterName","Master Name") !!}
							{!! Form::select("cmbMasterName", 
								[
									"" => "",
									"001" => "001:User Master(Inspector Master)",
									"002" => "002:Machine Master",
									"003" => "003:Inspection Time Master",
									"004" => "004:Customer Master",
									"005" => "005:Inspection(Check) Sheet Master",
									"006" => "006:Inspection(Check) No. Master",
									"007" => "007:Inspection(Check) No. Time Master",
									"008" => "008:Item Master",
									"009" => "009:Data Link Master",
								]
								,$MasterName
								,[
									"class"      => "input-xlarge"
								])
							!!}
						</td>
						
						<td class="TDMarginPaddingSetting">
							{!! Form::label("lblCustomerForSearch","Customer/Supplier") !!}
							{!! Form::select('cmbCustomerForSearch', $arrDataListCustomerList, $CustomerForSearch, [
									"class"      => "input-xlarge",
									"onChange"   => "this.form.submit()",
								])
							!!}
						</td>
						
						<td class="TDMarginPaddingSetting">
							{!! Form::label("lblModelForSearch","Model") !!}
							{!! Form::select('cmbModelForSearch', $arrDataListModelList, $ModelForSearch, [
									"class"      => "input-xlarge",
									"onChange"   => "this.form.submit()",
								])
							!!}
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td>
							{!! Form::submit("Data Download(Add)",
								[ 
									"class"   => "RoundControls",
									"name"    => "btnDownloadIns",
									"onClick" => "downloadMasterData()",
								])
							!!}
							{!! Form::submit("Data Download(Modify)",
								[ 
									"class"   => "RoundControls",
									"name"    => "btnDownloadUpd",
									"onClick" => "downloadMasterData()",
								])
							!!}
							{!! Form::submit("Data Download(Delete)",
								[ 
									"class"   => "RoundControls",
									"name"    => "btnDownloadDel",
									"onClick" => "downloadMasterData()",
								])
							!!}
							{!! Form::submit("Data Download(All)",
								[ 
									"class"   => "RoundControls",
									"name"    => "btnDownload",
									"onClick" => "downloadMasterData()",
								])
							!!}
						</td>
					</tr>
				</table>
			</div>
			<div class="InputFormGroup">
				<table>
					<tr>
						<td>
							{!! Form::label("file","Upload File") !!}
							{!! Form::file("filUploadFile") !!}
							{!! Form::submit("Data Upload",
								[ 
									"class"   => "RoundControls",
									"name"    => "btnUpload",
									"onClick" => "uploadMasterData()",
								]
							) !!}
						</td>
					</tr>
				</table>
			</div>
		{!! Form::hidden("hidProcOKFlg", "") !!}
		{!! Form::close() !!}
		
@stop
