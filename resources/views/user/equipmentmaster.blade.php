@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Equipment Master Maintenance</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif
		
		{!! Form::open([
			"route"         => "user.equipmentmaster",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "EquipmentMasterForm",
		]) !!}
			<div class="SearchFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Search Condition</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentForSearch","Equipment Code") !!}
									{!! Form::text("txtEquipmentCDForSearch", $EquipmentCDForSearch, [
										"class"         => "input-small",
										"maxlength"     => 5,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentNameForSearch","Equipment Name") !!}
									{!! Form::text("txtEquipmentNameForSearch", $EquipmentNameForSearch, [
										"class"         => "input-large",
										"maxlength"     => 50,
									]) !!}
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									@if($SearchLock == "Lock")
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnSearch"
										]) !!}
									@else
										{!! Form::submit("Search", [ 
											"class" => "RoundControls",
											"name"  => "btnSearch"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($NewAddLock == "Lock")
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnNewAdd"
										]) !!}
									@else
										{!! Form::submit("New Add", [ 
											"class" => "RoundControls",
											"name"  => "btnNewAdd"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($RegistLock == "Lock")
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnResistUpload"
										]) !!}
									@else
										{!! Form::submit("Resist/Upload", [ 
											"class" => "RoundControls",
											"name"  => "btnResistUpload"
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									@if($DeleteLock == "Lock")
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"disabled" => "disabled",
											"name"  => "btnDelete"
										]) !!}
									@else
										{!! Form::submit("Delete", [ 
											"class" => "RoundControls",
											"name"  => "btnDelete"
										]) !!}
									@endif
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@if($EditVisible == "True")
			<div class="InputFormGroup">
				<fieldset>
					<legend class="FieldsetLegend">Edit Field</legend>
						<table>
							<tr>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentCDForEntry","Equipment Code") !!}
									@if($MachineNoEditLock == "Lock")
										{!! Form::text("txtEquipmentCDForEntry", $EquipmentCDForEntry, [
											"class"         => "input-small",
											"ReadOnly"      => "True",
											"maxlength"     => 5,
										]) !!}
									@else
										{!! Form::text("txtEquipmentCDForEntry", $EquipmentCDForEntry, [
											"class"         => "input-small",
											"maxlength"     => 5,
										]) !!}
									@endif
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentResultInputTypeForEntry","Equipment Result InputType") !!}
									{!! Form::select("cmbEquipmentInputTypeForEntry", 
										[
											"" => "",
											"1" => "Input Value",
											"2" => "Input OKNG",
										]
										,$EquipmentInputTypeForEntry
										,[
											"class"      => "input-large"
										])
									!!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentNameForEntry","Equipment Name") !!}
									{!! Form::text("txtEquipmentNameForEntry", $EquipmentNameForEntry, [
										"class"         => "input-large",
										"maxlength"     => 50,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentShortNameForEntry","Equipment Short Name") !!}
									{!! Form::text("txtEquipmentShortNameForEntry", $EquipmentShortNameForEntry, [
										"class"         => "input-small",
										"maxlength"     => 10,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblEquipmentToolCdForEntry","Equipment ToolCode Name") !!}
									{!! Form::text("txtEquipmentToolCdForEntry", $EquipmentToolCdForEntry, [
										"class"         => "input-large",
										"maxlength"     => 50,
									]) !!}
								</td>
								<td class="TDMarginPaddingSetting">
									{!! Form::label("lblDisplayOrderForEntry","Display Order") !!}
									{!! Form::text("txtDisplayOrderForEntry", $DisplayOrderForEntry, [
										"class"         => "input-small",
										"maxlength"     => 11,
									]) !!}
								</td>
							</tr>
						</table>
					</legend>
				</fieldset>
			</div>
		@endif
		@if ($Pagenator != null)
			<table class="SearchResultTable">
				<thead>
					<tr class="SearchResultTable whitetableframe">
						<th class="SearchResultHeader tablecolumn01">Modify</th>
						<th class="SearchResultHeader tablecolumnEquipmentCD">Equipment<br>Code</th>
						<th class="SearchResultHeader tablecolumnEquipmentResultInputType">Equipment<br>ResultInput<br>Type</th>
						<th class="SearchResultHeader tablecolumnEquipmentName">Equipment Name</th>
						<th class="SearchResultHeader tablecolumnEquipmentShortName">Equipment<br>ShortName</th>
						<th class="SearchResultHeader tablecolumnEquipmentToolCd">Equipment<br>ToolName</th>
						<th class="SearchResultHeader tablecolumnDisplayOrder">Display<br>Order</th>
					</tr>
				</thead>
				<tbody>
				
				<?php $cntRow = 0 ?>
				<?php $intCurrentPage = $Pagenator->CurrentPage() ?>
				<?php $intPerPage = $Pagenator->PerPage() ?>
				
				@foreach($Pagenator as $arrDataRow)

					<?php $cntRow += 1 ?>

					@if (
							(($intPerPage * ($intCurrentPage - 1) + 1) <= $cntRow)
							 and ($cntRow <= $intPerPage * ($intCurrentPage))
						)
							<tr class="SearchResultTable">
								<td class="tablecolumn01 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! Form::submit("Modify", [ 
										"class"   => "RoundControls",
										"name"    => "btnModify",
										"onClick" => "setHiddenValueForEquipmentMaster('" . $arrDataRow->SOKUTEIKI_CD . "')",
									]) !!} 
								</td>
								<td class="tablecolumnEquipmentCD SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->SOKUTEIKI_CD !!}
								</td>
								<td class="tablecolumnEquipmentResultInputType SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->INSPECTION_RESULT_INPUT_TYPE_NAME !!}
								</td>
								<td class="tablecolumnEquipmentName SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->SOKUTEIKI_NAME !!}
								</td>
								<td class="tablecolumnEquipmentShortName SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->SOKUTEIKI_SHORT_NAME !!}
								</td>
								<td class="tablecolumnEquipmentToolCd SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->TOOL_CD !!}
								</td>
								<td class="tablecolumnDisplayOrder SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
									{!! $arrDataRow->DISPLAY_ORDER !!}
								</td>
							</tr>
					@endif
				
				@endforeach
				<!-- hdnは共通部品でJavaScriptでセットする定義が必要 -->
				{!! Form::hidden("hidPrimaryKey1") !!}

				</tbody>
			</table>
			
			<div class="pagination">
				{!! $Pagenator->render(); !!}
			</div>
		@endif
		
		{!! Form::close() !!}
		<style type="text/css">
			/* Pagination links */
			.pagination a {
			    color: black;
			    float: left;
			    padding: 8px 16px;
			    text-decoration: none;
			    transition: background-color .3s;
			}

			/* Style the active/current link */
			.pagination a.active {
			    background-color: dodgerblue;
			    color: white;
			}

			/* Add a grey background color on mouse-over */
			.pagination a:hover:not(.active) {background-color: #ddd;}
		</style>

@stop
