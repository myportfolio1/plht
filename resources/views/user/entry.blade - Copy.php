@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Inspection Result Entry & Modify</div>
		</div>
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif
		
		{!! Form::open([
			"route"         => "user.entry",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "EntryForm",
		]) !!}

			<div class="InputFormGroup">
				<table>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Process</th>
									<th class="SearchResultHeader">Customer/Supplier</th>
									<th class="SearchResultHeader">Check Sheet No.</th>
									<th class="SearchResultHeader">Check Sheet Title</th>
									<th class="SearchResultHeader">Revision No.</th>
									<th class="SearchResultHeader">Part No.</th>
									<th class="SearchResultHeader">Part Name</th>
									<th class="SearchResultHeader">Model Name</th>
									<th class="SearchResultHeader">M/C No.</th>
									<th class="SearchResultHeader">Mold No.</th>
									<th class="SearchResultHeader">Inspector Code</th>
									<th class="SearchResultHeader">Inspector Name</th>
									<th class="SearchResultHeader">Team Name</th>
									<th class="SearchResultHeader">Inspection Date</th>
									<th class="SearchResultHeader">Condition</th>
									<th class="SearchResultHeader">Inspection Time</th>
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{!! $ProcessName !!}</td>
									<td class="SearchResultTable">{!! $CustomerName !!}</td>
									<td class="SearchResultTable">{!! $CheckSheetNo !!}</td>
									<td class="SearchResultTable">{!! $CheckSheetName !!}</td>
									<td class="SearchResultTable">{!! $RevisionNo !!}</td>
									<td class="SearchResultTable">{!! $ItemNo !!}</td>
									<td class="SearchResultTable">{!! $ItemName !!}</td>
									<td class="SearchResultTable">{!! $ModelName !!}</td>
									<td class="SearchResultTable">{!! $MachineName !!}</td>
									<td class="SearchResultTable">{!! $MoldNo !!}</td>
									<td class="SearchResultTable">{!! $InspectorCode !!}</td>
									<td class="SearchResultTable">{!! $InspectorName !!}</td>
									<td class="SearchResultTable">{!! $TeamName !!}</td>
									<td class="SearchResultTable">{!! $InspectionDate !!}</td>
									<td class="SearchResultTable">{!! $ConditionName !!}</td>
									<td class="SearchResultTable">{!! $InspectionTimeName !!}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Material Name</th>
									<th class="SearchResultHeader">Material Size</th>
									<th class="SearchResultHeader">Product Weight</th>
									<th class="SearchResultHeader">Heat Treatment Type</th>
									<th class="SearchResultHeader">Plating Kind</th>
									<th class="SearchResultHeader">Color</th>
									<th class="SearchResultHeader">Item</th>
									<th class="SearchResultHeader">Tool Code</th>
									<th class="SearchResultHeader">Drawing No.</th>
									<th class="SearchResultHeader">ISO Control No.</th>
									<th class="SearchResultHeader">Inspection Items</th>
									<th class="SearchResultHeader">X-R Chart No./Pokayoke No.</th>
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{!! $MaterialName !!}</td>
									<td class="SearchResultTable">{!! $MaterialSize !!}</td>
									<td class="SearchResultTable">{!! $ProductWeight !!}</td>
									<td class="SearchResultTable">{!! $HeatTreatmentType !!}</td>
									<td class="SearchResultTable">{!! $PlatingKind !!}</td>
									<td class="SearchResultTable">{!! $ColorName !!}</td>
									<td class="SearchResultTable">{!! $ModelName !!}</td>
									<td class="SearchResultTable">{!! $ToolCode !!}</td>
									<td class="SearchResultTable">{!! $DrawingNo !!}</td>
									<td class="SearchResultTable">{!! $ISOKanriNo !!}</td>
									<td class="SearchResultTable">{!! $InspectionItem !!}</td>
									<td class="SearchResultTable">{!! $XRChartNoPokayokeNo !!}</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Material Lot No.</th>
									<th class="SearchResultHeader">Certificate No.</th>
									<th class="SearchResultHeader">P/O No.</th>
									<th class="SearchResultHeader">Invoice No.</th>
									<th class="SearchResultHeader">Quantity Coil</th>
									<th class="SearchResultHeader">Quantity Weight</th>
									<th class="SearchResultHeader">Lot No.</th>
									<th class="SearchResultHeader">Production Date</th>
									<!-- <th class="SearchResultHeader">hidden</th> -->
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable">{!! $MaterialLotNo !!}</td>
									<td class="SearchResultTable">{!! $CertificateNo !!}</td>
									<td class="SearchResultTable">{!! $PoNo !!}</td>
									<td class="SearchResultTable">{!! $InvoiceNo !!}</td>
									<td class="SearchResultTable">{!! $QuantityCoil !!}</td>
									<td class="SearchResultTable">{!! $QuantityWeight !!}</td>
									<td class="SearchResultTable">{!! $LotNo !!}</td>
									<td class="SearchResultTable">{!! $ProductionDate !!}</td>
									<!-- <td class="SearchResultTable">{!! $hidInspectionNo !!}</td> -->
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td class="TDMarginPaddingSetting">
							<table class="SearchResultTable">
								<tr class="SearchResultTable">
									<th class="SearchResultHeader">Refer to document / Frequency</th>
									<th class="SearchResultHeader">Equipment</th>
									<th class="SearchResultHeader">NG Under</th>
									<th class="SearchResultHeader">OFFSET Under</th>
									<th class="SearchResultHeader">Reference Value</th>
									<th class="SearchResultHeader">OFFSET Over</th>
									<th class="SearchResultHeader">NG Over</th>
									<th class="SearchResultHeader">Inspection No.</th>
									<th class="SearchResultHeader">Inspection Point</th>
									<th class="SearchResultHeader">Cavity</th>
									<th class="SearchResultHeader">Point Of Cavity</th>
									<th class="SearchResultHeader">Sample No.</th>
									<th class="SearchResultHeader">Status</th>
									{{--INSPECTION_RESULT_INPUT_TYPE=1 → Show--}}
									@if ($InspectionResultInputType == "1")
										<th class="SearchResultHeader">X bar</th>
										<th class="SearchResultHeader">Max</th>
										<th class="SearchResultHeader">Min</th>
										<th class="SearchResultHeader">Range</th>
										<th class="SearchResultHeader">Stdev</th>
										<th class="SearchResultHeader"><label for="OKNGNoCheck">Judge</label></th>
										<th class="SearchResultHeader"><label for="OKNGNoCheck">Reason</label></th>
									@endif
								</tr>
								{{--OK/NG → Show blank--}}
								<tr class="SearchResultTable">
									<td class="SearchResultTable" rowspan="2">{!! $ReferToDocumentFrequency !!}</td>
									<td class="SearchResultTable" rowspan="2">{!! $Equipment !!}</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $NGUnder !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $OFFSETUnder !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $ReferenceValue !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $OFFSETOver !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $NGOver !!} @else &nbsp; @endif</td>
									<td class="NowNumber" rowspan="2">{!! $InspectionNo !!}</td>
									<td class="NowNumber" rowspan="2">{!! $InspectionPoint !!}</td>
									<td class="NowNumber" rowspan="2">{!! $AnalyticalGroup01 !!}</td>
									<td class="NowNumber" rowspan="2">{!! $AnalyticalGroup02 !!}</td>
									<td class="NowNumber" rowspan="2">{!! $SampleNo !!}</td>
									
									{{--White is usually, Yellow is OFFSET, Red is NG, Status is JavaScript--}}
									<td class="{!! $StatusColorClass !!}" rowspan="2"><span id="Status"></span></td>


									<input 	class="input-medium" 
											name="hidJudgeMin" 
											type="hidden" 
											value="{!! $JudgeMin !!}"
											maxlength="30",
									>
									<!-- {!! Form::hidden("hidJudgeMax", $JudgeMax) !!} -->
									<input 	class="input-medium" 
											name="hidJudgeMax" 
											type="hidden" 
											value="{!! $JudgeMax !!}"
											maxlength="30",
									>

									@if ($InspectionResultInputType == "1")
										<td class="SearchResultTable" rowspan="2">{!! $Xbar !!}</td>
										<td class="SearchResultTable" rowspan="2">{!! $Max !!}</td>
										<td class="SearchResultTable" rowspan="2">{!! $Min !!}</td>
										<td class="SearchResultTable" rowspan="2">{!! $Range !!}</td>
										<td class="SearchResultTable" rowspan="2">{!! $Stdev !!}</td>
										<td class="{{$JudgeColorClass}}" rowspan="2"><span id="Judge"></span></td>
										<td class="SearchResultTable" rowspan="2">
											<span id="JudgeReason">
												<!-- {!! Form::select('cmbJudgeReason', $arrJudgeReasonList, $JudgeReason, [
														"class"      => "input-medium  MarginReset"
												]) !!} -->
												<select name="cmbJudgeReason" class="input-medium  MarginReset">
													@foreach($arrJudgeReasonList as $key0=>$value0)
														@if($key0==$JudgeReason)
															<option value="{!! $key0 !!}" selected="selected">{!! $value0 !!}</option>
														@else
															<option value="{!! $key0 !!}">{!! $value0 !!}</option>
														@endif
													@endforeach
												</select>
											</span>
										</td>
									@endif
									
									<!-- {!! Form::hidden("hidNGUnder", $NGUnder) !!} -->
									<input 	class="input-medium" 
											name="hidNGUnder" 
											type="hidden" 
											value="{!! $NGUnder !!}"
											maxlength="30",
									>
									<!-- {!! Form::hidden("hidOFFSETUnder", $OFFSETUnder) !!} -->
									<input 	class="input-medium" 
											name="hidOFFSETUnder" 
											type="hidden" 
											value="{!! $OFFSETUnder !!}"
											maxlength="30",
									>
									<!-- {!! Form::hidden("hidReferenceValue", $ReferenceValue) !!} -->
									<input 	class="input-medium" 
											name="hidReferenceValue" 
											type="hidden" 
											value="{!! $ReferenceValue !!}"
											maxlength="30",
									>
									<!-- {!! Form::hidden("hidOFFSETOver", $OFFSETOver) !!} -->
									<input 	class="input-medium" 
											name="hidOFFSETOver" 
											type="hidden" 
											value="{!! $OFFSETOver !!}"
											maxlength="30",
									>
									<!-- {!! Form::hidden("hidNGOver", $NGOver) !!} -->
									<input 	class="input-medium" 
											name="hidNGOver" 
											type="hidden" 
											value="{!! $NGOver !!}"
											maxlength="30",
									>
									
									
								</tr>
								<tr class="SearchResultTable">
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $NGUnderDiff !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $OFFSETUnderDiff !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $ReferenceValueDiff !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $OFFSETOverDiff !!} @else &nbsp; @endif</td>
									<td class="SearchResultTable righttext">@if ($InspectionResultInputType != "2") {!! $NGOverDiff !!} @else &nbsp; @endif</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				<table>
					<tr>
						<td class="TDMarginPaddingSetting VAlignTop">
							<label for="zumen">Cutting Check Points</label>
							{{--画像をホイールで拡大縮小するScriptをここで読み込んでいる--}}
							<script src="../../js/jquery.wheelzoom.js"></script>
							<img src="../../{!! $PictureURL !!}" alt="zumen" width="500px" height="500px" id="wheelzoom">
							<script>
								$('#wheelzoom').wheelzoom();
							</script>
						</td>
						<td class="TDMarginPaddingSetting VAlignTop">
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblInspectionResult","Inspection Result",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable">
										@if ($InspectionResultInputType == "1")
											<!-- {!! Form::text("txtInspectionResult", $InspectionResult, [
												"maxlength"     => 30,
												"class"         => "MarginReset",
											]) !!} -->
											
											<input 	class="MarginReset"
													name="txtInspectionResult" 
													type="text" 
													value="{!! $InspectionResult !!}"
													maxlength="30",
											>
										
										@else
											<!-- {!! Form::radio('rdiInspectionResult', '1', $InspectionResultRadio1) !!}OK　
											{!! Form::radio('rdiInspectionResult', '2', $InspectionResultRadio2) !!}NG -->
											<input 	name="rdiInspectionResult" 
													type="radio" 
													value="1"
													@if($InspectionResultRadio1 == "true") checked @endif
											>OK	
											<input 	name="rdiInspectionResult" 
													type="radio" 
													value="2"
													@if($InspectionResultRadio2 == "true") checked @endif
											>NG
											
										@endif
										
										<!-- {!! Form::hidden("hidInspectionResultInputType", $InspectionResultInputType) !!} -->
										<input 	class="input-medium" 
												name="hidInspectionResultInputType" 
												type="hidden" 
												value="{!! $InspectionResultInputType !!}"
												maxlength="2",
										>
									</td>
									<td class="InputFormTable">
										{!! Form::submit("Entry", [ 
											"class"   => "RoundControls",
											"name"    => "btnEntry",
											"onClick" => "checkAndSetResultInformation('1')",
										]) !!}
									</td>
									<td class="InputFormTable">
										<input type="button" value="Input Clear" onClick="cleartxtInspectionResult()">
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblCavityMemo","Cavity Memo",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable">
										<input 	class="MarginReset"
												name="txtCavityMemo" 
												type="text" 
												value="{!! $CavityMemo !!}"
												maxlength="3",
										>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblResultClass","Result Class",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<!-- {!! Form::radio('rdiResultClass', '1', $ResultClass1) !!}OK　
										{!! Form::radio('rdiResultClass', '2', $ResultClass2) !!}OFFSET　
										{!! Form::radio('rdiResultClass', '3', $ResultClass3) !!}NG　
										{!! Form::radio('rdiResultClass', '4', $ResultClass4) !!}No Check　
										{!! Form::radio('rdiResultClass', '5', $ResultClass5) !!}Line Stop -->
										<input 	name="rdiResultClass" 
												type="radio" 
												value="1"
												@if($ResultClass1 == "true") checked @endif
										>OK&nbsp;&nbsp;
										<input 	name="rdiResultClass" 
												type="radio" 
												value="2"
												@if($ResultClass2 == "true") checked @endif
										>OFFSET&nbsp;&nbsp;
										<input 	name="rdiResultClass" 
												type="radio" 
												value="3"
												@if($ResultClass3 == "true") checked @endif
										>NG&nbsp;&nbsp;
										<input 	name="rdiResultClass" 
												type="radio" 
												value="4"
												@if($ResultClass4 == "true") checked @endif
										>No Check&nbsp;&nbsp;
										<input 	name="rdiResultClass" 
												type="radio" 
												value="5"
												@if($ResultClass5 == "true") checked @endif
										>Line Stop
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader"  Rowspan="2">
										{!! Form::label("lblOffsetNGReason","OFFSET Reason / NG Reason",["class"=>"MarginReset"]) !!}
									</th>
									
									<td class="InputFormTable" colspan="3">
										<span id="OffsetNGBunrui">
											<!-- {!! Form::select('cmbOffsetNGBunrui', $arrOffsetNGBunruiList, $OffsetNGBunrui, [
													"class"      => "input-xlarge  MarginReset",
													"onChange"   => "this.form.submit()",
											]) !!} -->
											<select name="cmbOffsetNGBunrui" class="input-xlarge  MarginReset" onchange="this.form.submit()">
												@foreach($arrOffsetNGBunruiList as $key1=>$value1)
													@if($key1==$OffsetNGBunrui)
														<option value="{!! $key1 !!}" selected="selected">{!! $value1 !!}</option>
													@else
														<option value="{!! $key1 !!}">{!! $value1 !!}</option>
													@endif
												@endforeach
											</select>
										</span>
									</td>
									<!-- {!! Form::hidden("hidOffsetNGBunrui", $OffsetNGBunrui) !!} -->
										<input 	class="input-medium" 
												name="hidOffsetNGBunrui" 
												type="hidden" 
												value="{!! $OffsetNGBunrui !!}"
												maxlength="10",
										>
								</tr>
								<tr class="InputFormTable">
									<td class="InputFormTable" colspan="3">
										<span id="OffsetNGReason">
											<!-- {!! Form::select('cmbOffsetNGReason', $arrOffsetNGReasonList, $OffsetNGReason, [
													"class"      => "input-xlarge  MarginReset"
											]) !!} -->
											<select name="cmbOffsetNGReason" class="input-xlarge  MarginReset">
												@foreach($arrOffsetNGReasonList as $key2=>$value2)
													@if($key2==$OffsetNGReason)
														<option value="{!! $key2 !!}" selected="selected">{!! $value2 !!}</option>
													@else
														<option value="{!! $key2 !!}">{!! $value2 !!}</option>
													@endif
												@endforeach
											</select>
										</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblOffsetNGAction","OFFSET Action / NG Action",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<!-- {!! Form::radio('rdiOffsetNGAction', '1', $OffsetNGAction1) !!}Running　
										{!! Form::radio('rdiOffsetNGAction', '2', $OffsetNGAction2) !!}Stop Line -->
										<input 	name="rdiOffsetNGAction" 
												type="radio" 
												value="1"
												@if($OffsetNGAction1 == "true") checked @endif
										>Running&nbsp;&nbsp;
										<input 	name="rdiOffsetNGAction" 
												type="radio" 
												value="2"
												@if($OffsetNGAction2 == "true") checked @endif
										>Stop Line
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblOffsetNGActionDetail","OFFSET Action Detail / NG Action Detail",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<!-- {!! Form::checkbox('chkOffsetNGActionDetailInternal' ,'1' , $OffsetNGActionDetailInternal) !!}Internal Approve　
										{!! Form::checkbox('chkOffsetNGActionDetailRequest'  ,'1' , $OffsetNGActionDetailRequest) !!}Request Offset　
										{!! Form::checkbox('chkOffsetNGActionDetailTSR'      ,'1' , $OffsetNGActionDetailTSR) !!}TSR　
										{!! Form::checkbox('chkOffsetNGActionDetailSorting'  ,'1' , $OffsetNGActionDetailSorting) !!}Sorting　
										{!! Form::checkbox('chkOffsetNGActionDetailNoAction' ,'1' , $OffsetNGActionDetailNoAction) !!}No Action -->
										<input 	name="chkOffsetNGActionDetailInternal" 
												type="checkbox"
												value="1"
												@if($OffsetNGActionDetailInternal == "true") checked @endif
										>Internal Approve&nbsp;&nbsp;
										<input 	name="chkOffsetNGActionDetailRequest" 
												type="checkbox"
												value="1"
												@if($OffsetNGActionDetailRequest == "true") checked @endif
										>Request Offset&nbsp;&nbsp;
										<input 	name="chkOffsetNGActionDetailTSR" 
												type="checkbox"
												value="1"
												@if($OffsetNGActionDetailTSR == "true") checked @endif
										>TSR&nbsp;&nbsp;
										<input 	name="chkOffsetNGActionDetailSorting" 
												type="checkbox"
												value="1"
												@if($OffsetNGActionDetailSorting == "true") checked @endif
										>Sorting&nbsp;&nbsp;
										<input 	name="chkOffsetNGActionDetailNoAction" 
												type="checkbox"
												value="1"
												@if($OffsetNGActionDetailNoAction == "true") checked @endif
										>No Action
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblActionDetails","Action Details",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<span id="ActionDetails">
											<!-- {!! Form::select('cmbActionDetails', $arrActionDetailsList, $ActionDetails, [
													"class"      => "input-xlarge  MarginReset"
											]) !!} -->
											<select name="cmbActionDetails" class="input-xlarge  MarginReset">
												@foreach($arrActionDetailsList as $key3=>$value3)
													@if($key3==$ActionDetails)
														<option value="{!! $key3 !!}" selected="selected">{!! $value3 !!}</option>
													@else
														<option value="{!! $key3 !!}">{!! $value3 !!}</option>
													@endif
												@endforeach
											</select>
										</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblTecReqNo","Technician Request No.",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<!-- {!! Form::textarea("txtTecReqNo", $TecReqNo, [
											"maxlength"     => 50,
											"rows"          => 1,
											"class"         => "input-xlarge MarginReset",
										]) !!} -->
										<input 	class="input-xlarge MarginReset" 
												name="txtTecReqNo" 
												type="textarea" 
												value="{!! $TecReqNo !!}"
												maxlength="50",
										>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblTecReqPerson","Technician Request Person",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<span id="TecReqPerson">
											<!-- {!! Form::select('cmbTecReqPerson', $arrTecReqPersonList, $TecReqPerson, [
													"class"      => "input-xlarge  MarginReset"
											]) !!} -->
											<select name="cmbTecReqPerson" class="input-xlarge  MarginReset">
												@foreach($arrTecReqPersonList as $key4=>$value4)
													@if($key4==$TecReqPerson)
														<option value="{!! $key4 !!}" selected="selected">{!! $value4 !!}</option>
													@else
														<option value="{!! $key4 !!}">{!! $value4 !!}</option>
													@endif
												@endforeach
											</select>
										</span>
									</td>
								</tr>
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblTSRNo","TSR No.",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable" colspan="3">
										<!-- {!! Form::textarea("txtTSRNo", $TSRNo, [
											"maxlength"     => 50,
											"rows"          => 1,
											"class"         => "input-xlarge MarginReset",
										]) !!} -->
										<input 	class="input-xlarge MarginReset" 
												name="txtTSRNo" 
												type="textarea" 
												value="{!! $TSRNo !!}"
												maxlength="50",
										>
									</td>
								</tr>
							</table>
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblInspectionNo","Inspection No.",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable">
									<!-- 	{!! Form::text("txtInspectionNo", $InspectionNo, [
											"class" => "input-medium MarginReset",
											"maxlength"     => 10,
										]) !!} -->
										<input 	class="input-medium MarginReset" 
												name="txtInspectionNo" 
												type="text" 
												value="{!!$InspectionNo!!}"
												maxlength="10",
										>
										<!-- {!! Form::hidden("hidInspectionNo", $hidInspectionNo) !!} -->
										<input 	class="input-medium" 
												name="hidInspectionNo" 
												type="hidden" 
												value="{!! $hidInspectionNo !!}"
												maxlength="10",
										>
										<!-- {!! Form::hidden("hidDataRev", $hidDataRev) !!} -->
										<input 	class="input-medium" 
												name="hidDataRev" 
												type="hidden" 
												value="{!! $hidDataRev !!}"
												maxlength="11",
										>
									</td>
									<td class="InputFormTable">
										{!! Form::submit("Page Move", [ 
											"class" => "RoundControls",
											"name"  => "btnPageMove"
										]) !!}
									</td>
									<td class="InputFormTable">
										@if($prevButtonLock == "lock")
											{!! Form::submit("<<", [ 
												"class"    => "RoundControls",
												"name"     => "btnPrevPage",
												"disabled" => "disabled"
											]) !!}
										@else
											{!! Form::submit("<<", [ 
												"class"    => "RoundControls",
												"name"     => "btnPrevPage",
											]) !!}
										@endif
									</td>
									<td class="InputFormTable">
										@if($nextButtonLock == "lock")
											{!! Form::submit(">>", [ 
												"class"    => "RoundControls",
												"name"     => "btnNextPage",
												"disabled" => "disabled"
											]) !!}
										@else
											{!! Form::submit(">>", [ 
												"class"    => "RoundControls",
												"name"     => "btnNextPage",
											]) !!}
										@endif
									</td>
									<td class="InputFormTable">
										{!! Form::submit("Return", [ 
											"class" => "RoundControls",
											"name"  => "btnReturn"
										]) !!}
									</td>
								</tr>
							</table>
							<table class="InputFormTable">
								<tr class="InputFormTable">
									<th class="SearchResultHeader">
										{!! Form::label("lblInspectionToolClass","Equipment",["class"=>"MarginReset"]) !!}
									</th>
									<td class="InputFormTable">
										<!-- {!! Form::select('cmbInspectonToolClass', $arrDataListInspectionToolClass, $InspectionToolClass, [
												"class"      => "input-medium MarginReset"
										]) !!} -->
										<select name="cmbInspectonToolClass" class="input-medium  MarginReset">
											@foreach($arrDataListInspectionToolClass as $key5=>$value5)
												@if($key5==$InspectionToolClass)
													<option value="{!! $key5 !!}" selected="selected">{!! $value5 !!}</option>
												@else
													<option value="{!! $key5 !!}">{!! $value5 !!}</option>
												@endif
											@endforeach
										</select>

										<input 	class="input-medium" 
												name="hidEquipment" 
												type="hidden" 
												value="{!! $InspectionToolClass !!}"
												maxlength="5",
										>
									</td>
									<td class="InputFormTable">
										{!! Form::submit("Filter", [ 
											"class" => "RoundControls",
											"name"  => "btnFilter"
										]) !!}
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		{!! Form::close() !!}
		{{--When Screen load, set forcus input result--}}
		<script>
			setDefaultForcus();
		</script>
@stop
