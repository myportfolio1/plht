@extends("layout")
@section("content") 
<div class="header">
	<div class="TitleBar">Graph</div>
</div>
<meta http-equiv="refresh" content="1800">
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif

		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif

		{{ Form::open([
			"route"         => "user.graph",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting form__graph row-fluid",
			"name"          => "GraphgForm",
		])}}
			<div class="container-fluid">
				<div class="SearchFormGroup__graph span8">
					<form class="FormMarginPaddingSetting">
						<fieldset>
							<legend class="FieldsetLegend">Search & Data Output</legend>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblProcessForGraph">Process</label>
									{{ Form::select('cmbProcessForGraph', $arrProcessList, $ProcessForGraph, [
											"class"      => "input-small input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblCustomerForGraph">Customer/Supplier</label>
									{{ Form::select('cmbCustomerForGraph', $arrCustomerList, $CustomerForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}						
								</div>
								<div class="control-group span2">
									<label for="lblCheckSheetNoForGraph">Product No.</label>
									{{ Form::select('cmbCheckSheetNoForGraph', $arrProductNoList, $ProductNoForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}	
								</div>
								<div class="control-group span2">
									<label for="lblRevisionNoForGraph">Rev No.</label>
									{{ Form::select('cmbRevisionNoForGraph', $arrRevNoList, $RevNoForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Mold No.</label>
									{{ Form::select('cmbMoldNoForGraph', $arrMoldNoList, $MoldNoForGraph, [
											"class"      => "input-medium input-block-level",
											"onChange"   => "this.form.submit()",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblInspectionDateFromForGraph">Inspection Date</label>
									{{ Form::checkbox('cmbRadioChoosePeriod', 'Old', $RadioChoosePeriod, [
											"id" => "cmbRadioChoosePeriod",
											"onchange"	=> "",
											"class" 	=> " input-prepend"
									]) }}
									{{ Form::label("lblRadioChoosePeriod", " Old", array(
										"style"	=> "display: inline-block; vertical-align: sub;",
									)) }}
									{{ Form::text("txtTypeTimeForGraph", $TypeTimeForGraph, [
										"class"         => "input-mini input-prepend",
										"maxlength"     => 3,
										"style"			=> "margin-bottom: 0;",
										"placeholder"   => "Days",
										"id"            => "txtTypeTimeForGraph",
									]) }}
								</div>
							</div>
							<div class="row-fluid">
								<!-- <div class="control-group span4" style="text-align:center;"> -->
								<div class="control-group span4">
									{{ Form::label("lblInspectionDateFromForGraph","Production Date") }}
									<div class="row-fluid">
										<div class="span1">
											{{ Form::checkbox('cmbRadioChooseProductionDate', 'Old', $RadioChooseProductionDate, [
												"id" => "cmbRadioChooseProductionDate",
												"class" 	=> " input-prepend"
											]) }}
										</div>
										<div class="span5">
											{{ Form::text("txtProductionDateFromForGraph", $ProductionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "productstartdate"
											]) }}
										</div>
										<div class="span1">
										 -
										</div>
										<div class="span5">
											{{ Form::text("txtProductionDateToForGraph", $ProductionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "productenddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span4">
									{{ Form::label("lblInspectionDateFromForGraph","Inspection Date") }}
									<div class="row-fluid">
										<div class="span1">
											{{ Form::checkbox('cmbRadioChooseInspectionDate', 'Old', $RadioChooseInspectionDate, [
												"id" => "cmbRadioChooseInspectionDate",
												"class" 	=> " input-prepend"
											]) }}
										</div>
										<div class="span5">
											{{ Form::text("txtInspectionDateFromForGraph", $InspectionDateFromForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"style"			=> "margin-bottom: 0;",
												"id"            => "startdate",
											]) }}
										</div>
										<div class="span1">
											-
										</div>
										<div class="span5">
											{{ Form::text("txtInspectionDateToForGraph", $InspectionDateToForGraph, [
												"class"         => "input-small input-block-level form_datetime",
												"maxlength"     => 10,
												"id"            => "enddate",
											]) }}
										</div>
									</div>
								</div>
								<div class="control-group span2">
									{{ Form::label("lblSelectTypeForFraph", "Type Graph") }}
									{{ Form::select('cmbSelectTypeForGraph', 
											array(
												""					=> "",
												"01" 				=> "XAve - R",
											    "02" 				=> "XAve - S",
												"03" 				=> "Cpk", 
											),$SelectTypeData,[
												"class" => "input-medium input-block-level",
												"id"	=> "cmbSelectTypeForGraph"
											]
									) }}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Please") }}
									<button type="submit" class="btn btn-sm btn-outline-secondary input-small input-block-level cd-admin-create-modal-btn" name="btnGraph" onclick="add()">Graph</button>
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Select") }}
									<button type="submit" class="btn btn-sm btn-outline-secondary input-small input-block-level cd-admin-create-modal-btn" name="btnExcel" onclick="add()">Excel</button>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span2">
									<label for="lblMaterialNameForGraph">Inspection Point</label>
									{{ Form::select('cmbInspectionPointForGraph', $arrInspectionPointList, $InspectionPointForGraph,[
											"class"      => "input-medium input-block-level",
											{{-- "onChange"   => "this.form.submit()", --}}
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMaterialSizeForGraph">Cavity</label>
									{{ Form::select('cmbCavityForGraph', $arrCavityNoList,$CavityNoForGraph,[
											"class"      => "input-medium input-block-level"
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblMoldNoForGraph">Team Name</label>
									{{ Form::select('cmbTeamNameForGraph', $arrTeamNameList, $TeamNameForGraph,[
											"class"      => "input-medium input-block-level"
										])
									}}
								</div>
								<div class="control-group span2">
									{{ Form::label("lblInspectionDateFromForGraph","Inspection Time") }}
									{{ Form::select('cmbTimeIDForGraph', $arrTimeIDList, $TimeIDForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span2">
									<label for="lblConditionForGraph">Condition</label>
									{{ Form::select('cmbConditionForGraph', $arrConditionList, $ConditionForGraph, [
											"class"      => "input-medium input-block-level",
										])
									}}
								</div>
								<div class="control-group span1">
									{{ Form::label("lblSubmitForGraph", "Buttons") }}
									<text type="button" id="<?php if(!empty($arrAverage)){
		if($arrAverage != null){ if(count($arrAverage)=="1"){ echo "save"; }}}?>" class="btn btn-sm btn-outline-secondary input-small input-block-level cd-admin-create-modal-btn" name="btnImage" <?php if(!empty($arrAverage)){
		if($arrAverage != null){ if(count($arrAverage) != "1"){ echo "disabled"; }}}?>>Image</text>
								    <input type="hidden" name="UserID" id="UserID" value="<?php echo $UserID; ?>"/>
								</div>
								<div class="control-group span1">
									<label for="lblConditionForGraph">Graph Totals</label>
									<input type="text" name="countGraph" id="countGraph"class="input-medium input-block-level" value="<?php if(!empty($arrAverage)){
		if($arrAverage != null){ echo count($arrAverage); }}else{ echo "0"; }?>" disabled/>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="image__graph span4">
					<table class="SearchResultTable">
						<tbody>
							<tr class="SearchResultTable">
								@if(!empty($arrPicture[0]))
									@foreach($arrPicture as $value)
										<td class="TDMarginPaddingSetting">
											<div class="img__tresanal" >
												<img src="../../{{ $value }}" class="zoom"/>
											</div>
										</td>
									@endforeach
								@endif	
							</tr>
						</tbody>
					</table>	
				</div>	
			</div>				
		</form>
		<style type="text/css">
			.zoom {
			    padding: 0px;
			    transition: transform .6s;
			    margin: 0 auto;
			}

			.zoom:hover {
				margin: 0 auto;
			    transform: scale(1.5);
			}

			.cd-admin-create-modal-btn {
			  /*min-width: 10vw;*/
			 padding: 2px;
			 font-size : 0.7vw;
			}
		</style>
<script src="../../js/datepicker/bootstrap-datetimepicker.js" charset="utf-8"></script>
<script src="../../js/datepicker/bootstrap-datetimepicker.min.js" charset="utf-8"></script>
<script src="../../js/datepicker/moment.js" charset="utf-8"></script>
<script src="../../js/charts/html2canvas.js" charset="utf-8"></script>
<script src="../../js/charts/FileSaver.min.js" charset="utf-8"></script>
<script type="text/javascript">
	      
		    $(".form_datetime").datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			});

		 	$('#startdate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(start > end){
			    	$('#enddate').val(startdate);
			    }
			});

			$('#enddate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#startdate').val();
				var enddate = $('#enddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(end < start){
			    	$('#startdate').val(enddate);
			    }
			});

			$('#productstartdate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#productstartdate').val();
				var enddate = $('#productenddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(start > end){
			    	$('#productenddate').val(startdate);
			    }
			});

			$('#productenddate').datetimepicker({
		    	format: 'dd-mm-yyyy',
		    	startView: 'month',
		        minView: 'month',
		        autoclose: true ,
		        todayBtn: true,
			}).on("change.dp", function(selectedDate) {
				var startdate = $('#productstartdate').val();
				var enddate = $('#productenddate').val();
				var datetimestart = moment( startdate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var start = new Date(datetimestart);
				var datetimeend = moment( enddate, 'DD-MM-YYYY',true).format("YYYY-MM-DD");
				var end = new Date(datetimeend);
			     
			    if(end < start){
			    	$('#productstartdate').val(enddate);
			    }
			});

			$(document).ready(function () {
				var day = new Date();
				var today = moment(day).format("DD-MM-YYYY");

				$("#cmbRadioChoosePeriod").click(function () {
	            	if ($(this).is(":checked"))
					{
						$(this).prop('checked', true);
						$("#txtTypeTimeForGraph").val("30");

						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");

						$("#cmbRadioChooseProductionDate").prop('checked', false);
						$("#productstartdate").val("");
						$("#productenddate").val("");

					}else{

						$(this).prop('checked', false);
						$("#txtTypeTimeForGraph").val("");

						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");

						$("#cmbRadioChooseProductionDate").prop('checked', true);
						$("#productstartdate").val(today);
						$("#productenddate").val(today);
					}      
	        	});

				$("#cmbRadioChooseInspectionDate").click(function () {
					if ($(this).is(":checked"))
					{
						$(this).prop('checked', true);
						$("#startdate").val(today);
						$("#enddate").val(today);

						$("#cmbRadioChooseProductionDate").prop('checked', false);
						$("#productstartdate").val("");
						$("#productenddate").val("");

						$("#cmbRadioChoosePeriod").prop('checked', false);
						$("#txtTypeTimeForGraph").val("");

					}else{

						$(this).prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");

						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#productstartdate").val(today);
						$("#productenddate").val(today);

						$("#cmbRadioChoosePeriod").prop('checked', true);
						$("#txtTypeTimeForGraph").val("");
						
					}
	        	});

	        	$("#cmbRadioChooseProductionDate").click(function () {
					if ($(this).is(":checked"))
					{
						$(this).prop('checked', true);
						$("#productstartdate").val(today);
						$("#productenddate").val(today);

						$("#cmbRadioChooseInspectionDate").prop('checked', false);
						$("#startdate").val("");
						$("#enddate").val("");

						$("#cmbRadioChoosePeriod").prop('checked', false);
						$("#txtTypeTimeForGraph").val("");

					}else{

						$(this).prop('checked', false);
						$("#productstartdate").val("");
						$("#productenddate").val("");

						$("#cmbRadioChooseInspectionDate").prop('checked', true);
						$("#startdate").val(today);
						$("#enddate").val(today);

						$("#cmbRadioChoosePeriod").prop('checked', false);
						$("#txtTypeTimeForGraph").val("");

					}
	        	});

	        	$("#txtTypeTimeForGraph").click(function () {
	        		$("#cmbRadioChoosePeriod").prop('checked', true);
	        		$("#cmbRadioChooseProductionDate").prop('checked', false);
	        		$("#cmbRadioChooseInspectionDate").prop('checked', false);
	        		$("#startdate").val("");
					$("#enddate").val("");
	        	    $("#productstartdate").val("");
				    $("#productenddate").val("");
	        	});

	        	$("#productstartdate").click(function () {
	        		$("#cmbRadioChooseProductionDate").prop('checked', true);
	        		$("#cmbRadioChoosePeriod").prop('checked', false);
	        		$("#cmbRadioChooseInspectionDate").prop('checked', false);
	        		$("#txtTypeTimeForGraph").val("");
	        		$("#startdate").val("");
					$("#enddate").val("");
	        	});
	        	$("#productenddate").click(function () {
	        		$("#cmbRadioChooseProductionDate").prop('checked', true);
	        		$("#cmbRadioChoosePeriod").prop('checked', false);
	        		$("#cmbRadioChooseInspectionDate").prop('checked', false);
	        		$("#txtTypeTimeForGraph").val("");
	        		$("#startdate").val("");
					$("#enddate").val("");
	        	});

	        	$("#startdate").click(function () {
	        		$("#cmbRadioChooseInspectionDate").prop('checked', true);
	        		$("#cmbRadioChoosePeriod").prop('checked', false);
	        		$("#cmbRadioChooseProductionDate").prop('checked', false);
	        		$("#txtTypeTimeForGraph").val("");
	        		$("#productstartdate").val("");
					$("#productenddate").val("");
	        	});
	        	$("#enddate").click(function () {
	        		$("#cmbRadioChooseInspectionDate").prop('checked', true);
	        		$("#cmbRadioChoosePeriod").prop('checked', false);
	        		$("#cmbRadioChooseProductionDate").prop('checked', false);
	        		$("#txtTypeTimeForGraph").val("");
	        		$("#productstartdate").val("");
					$("#productenddate").val("");
	        	});
	    });
</script>	
<script src="../../js/charts/chart.min.js" charset="utf-8"></script>
<script src="../../js/charts/chartjs-plugin-annotation.js" charset="utf-8"></script>
<style type="text/css">
	#border{
	  border: solid 0px white;
	}

	.canvas{
	  border: solid 0px blue;
	  margin-left: 10%; 
	  height: 70%;
	  width: 80%;
	}

	table {
		border-collapse: collapse;
		border-spacing: 0;
		width: 80%;
	}

    .dropdown{
    	margin-bottom: 100%;
    }

</style>

 @if($SelectTypeData != null)
	@if($SelectTypeData == "01")
	  	@if(!empty($arrAverage))
			@if($arrAverage != null)
				<div id="capture">
					<?php foreach ($arrAverage as $key => $value){ ?>
							<table class="table table-bordered">
							  <thead>
							  	<tr>
							  	  <th score="col" colspan="6"><center><h3><b><u>XAve - R Control</u></b></h3></center></th>
							  	</tr>	
							    <tr>
							      <th scope="col" colspan="1">Customer ID : <?php echo $arrCustomerID[$key];  ?></th>
							      <th scope="col" colspan="3">Customer Name : <?php echo $arrCustomerName[$key]; ?></th>
							      <th scope="col" colspan="1">User ID : <?php echo $UserID;?></th>
							      <th scope="col" colspan="1">Print Date : <?php echo date("Y-m-d"); ?> </th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="col" colspan="1">Product No : <?php echo $arrProductNo[$key]; ?></th>
							      <th scope="col" colspan="3">Product Name : <?php echo $arrProductName[$key]; ?></th>
							      <th scope="col" colspan="1">Revision No : <?php echo $arrRevNo1Dimention[$key]; ?></th>
							      <th scope="col" colspan="1"></th>
							    </tr>
							    <tr>
							     <th scope="col" colspan="6">Inspection Date : <?php echo $DateFrom; ?> - <?php echo $DateTo; ?></th>
							    </tr>
							    <tr>
							      <th scope="col">Mold No : <?php echo $arrMoldNo1Dimention[$key]; ?></th>
							      <th scope="col">Machine No : <?php echo $arrMachineNo[$key]; ?></th>
							      <th scope="col"><?php if($arrPoint1Dimention[$key] != "0") { echo "Inspection Point : ".$arrPoint1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrCavity1Dimention[$key] != "0") { echo "Cavity No : ".$arrCavity1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrWhere1Dimention[$key] != "0") { echo "Cavity Where : ".$arrWhere1Dimention[$key]; } ?></th>
							      <th scope="col">Q'ty : <?php echo $totalsValue[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col" colspan="4">Item Name :  <?php echo $arrItemName[$key]; ?></th>
							     <th scope="col" colspan="1">XAve Chart</th>
							     <th scope="col" colspan="1">R Chart</th>
							    </tr>
							     <tr>
							     <th scope="col">Standard Spec : <?php echo $arrCenterSpec[$key]; ?></th>
							     <th>Average : <?php echo $averageCpk[$key]; ?></th>
							     <th scope="col" colspan="2">SD : <?php echo $stdevCpk[$key]; ?></th>
							     <th>UCL : <?php echo $UCLX[$key][0]; ?></th>
							     <th>UCLR : <?php echo $UCLR[$key][0]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Upper Spec (SU) :  <?php echo $arrUpperSpec[$key]; ?></th>
							     <th>Maximum : <?php echo $arrMaxHeader[$key]; ?></th>
							     <th scope="col" colspan="2">Cp : <?php echo $Cp[$key]; ?></th>
							     <th>XAve : <?php echo $CLX[$key][0]; ?></th>
							     <th>RAve : <?php echo $CLR[$key][0]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Lower Spec (SL) :  <?php echo $arrLowerSpec[$key]; ?></th>
							     <th>Minimum : <?php echo $arrMinHeader[$key]; ?></th>
							     <th scope="col" colspan="2">Cpk : <?php echo $Cpk[$key]; ?></th>
							     <th>LCL : <?php echo $LCLX[$key][0]; ?></th>
							     <th>LCLR : <?php echo $LCLR[$key][0]; ?></th>
							    </tr>
							    <tr>
							    	<th colspan="6">
							    		<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4">
												<table border="0">
													<tr>
														<th>
															<canvas class="canvas" id="<?php echo "average".$key; ?>"></canvas>
														</th>
													</tr>
												</table>
											</div>	
										</div>	
										<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4">
												<table border="0">
													<tr>
														<th>
															<canvas class="canvas" id="<?php echo "range".$key; ?>"></canvas>
														</th>
													</tr>
												</table>
											</div>	
										</div>	
									</th>
							    </tr>
							    <tr>
							    	<th colspan="6"><center><img src="../../img/CompanyLogo.png"/>PLASESS HI-TECH CO.,LTD.</center></th>
							    </tr>	
							  </tbody>
							</table>
								    
					<?php } ?>
				</div>
			@endif
	 	@endif
	@elseif($SelectTypeData == "02")
		@if(!empty($arrAverage))
			@if($arrAverage != null)
				<div id="capture">
					<?php foreach ($arrAverage as $key => $value){ ?>
							<table class="table table-bordered">
							  <thead>
							  	<tr>
							  	  <th score="col" colspan="6"><center><h3><b><u>XAve - S Control</u></b></h3></center></th>
							  	</tr>	
							    <tr>
							      <th scope="col" colspan="1">Customer ID : <?php echo $arrCustomerID[$key];  ?></th>
							      <th scope="col" colspan="3">Customer Name : <?php echo $arrCustomerName[$key]; ?></th>
							      <th scope="col" colspan="1">User ID : <?php echo $UserID;?></th>
							      <th scope="col" colspan="1">Print Date : <?php echo date("Y-m-d"); ?> </th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="col" colspan="1">Product No : <?php echo $arrProductNo[$key]; ?></th>
							      <th scope="col" colspan="3">Product Name : <?php echo $arrProductName[$key]; ?></th>
							      <th scope="col" colspan="1">Revision No : <?php echo $arrRevNo1Dimention[$key]; ?></th>
							      <th scope="col" colspan="1"></th>
							    </tr>
							    <tr>
							     <th scope="col" colspan="6">Inspection Date : <?php echo $DateFrom; ?> - <?php echo $DateTo; ?></th>
							    </tr>
							    <tr>
							      <th scope="col">Mold No : <?php echo $arrMoldNo1Dimention[$key]; ?></th>
							      <th scope="col">Machine No : <?php echo $arrMachineNo[$key]; ?></th>
							      <th scope="col"><?php if($arrPoint1Dimention[$key] != "0") { echo "Inspection Point : ".$arrPoint1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrCavity1Dimention[$key] != "0") { echo "Cavity No : ".$arrCavity1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrWhere1Dimention[$key] != "0") { echo "Cavity Where : ".$arrWhere1Dimention[$key]; } ?></th>
							      <th scope="col">Q'ty : <?php echo $totalsValue[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col" colspan="4">Item Name :  <?php echo $arrItemName[$key]; ?></th>
							     <th scope="col" colspan="1">XAve Chart</th>
							     <th scope="col" colspan="1">S Chart</th>
							    </tr>
							     <tr>
							     <th scope="col">Standard Spec : <?php echo $arrCenterSpec[$key]; ?></th>
							     <th>Average : <?php echo $averageCpk[$key]; ?></th>
							     <th scope="col" colspan="2">SD : <?php echo $stdevCpk[$key]; ?></th>
							     <th>UCL : <?php echo $UCLXbarStd[$key][0]; ?></th>
							     <th>UCLS : <?php echo $UCLStdev[$key][0]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Upper Spec (SU) :  <?php echo $arrUpperSpec[$key]; ?></th>
							     <th>Maximum : <?php echo $arrMaxHeader[$key]; ?></th>
							     <th scope="col" colspan="2">Cp : <?php echo $Cp[$key]; ?></th>
							     <th>XAve : <?php echo $CLX[$key][0]; ?></th>
							     <th>SAve : <?php echo $CLS[$key][0]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Lower Spec (SL):  <?php echo $arrLowerSpec[$key]; ?></th>
							     <th>Minimum : <?php echo $arrMinHeader[$key]; ?></th>
							     <th scope="col" colspan="2">Cpk : <?php echo $Cpk[$key]; ?></th>
							     <th>LCL : <?php echo $LCLXbarStd[$key][0]; ?></th>
							     <th>LCLS : <?php echo $LCLStdev[$key][0]; ?></th>
							    </tr>
							    <tr>
							    	<th colspan="6">
							    		<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4">
												<table border="0">
													<tr>
														<th>
															<canvas class="canvas" id="<?php echo "average".$key; ?>"></canvas>
														</th>
													</tr>
												</table>
											</div>	
										</div>	
										<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4">
												<table border="0">
													<tr>
														<th>
															<canvas class="canvas" id="<?php echo "stdev".$key; ?>"></canvas>
														</th>
													</tr>
												</table>
											</div>	
										</div>	
									</th>
							    </tr>
							    <tr>
							    	<th colspan="6"><center><img src="../../img/CompanyLogo.png"/>PLASESS HI-TECH CO.,LTD.</center></th>
							    </tr>	
							  </tbody>
							</table>
								    
					<?php } ?>
				</div>
			@endif
	 	@endif
	@elseif($SelectTypeData == "03")
		@if(!empty($arrAverage))
			@if($arrAverage != null) 	
			<div id="capture">
					<?php foreach ($arrAverage as $key => $value){ ?>
							<table class="table table-bordered">
							  <thead>
							  	<tr>
							  	  <th score="col" colspan="6"><center><h3><b><u>Cpk Control</u></b></h3></center></th>
							  	</tr>	
							    <tr>
							      <th scope="col" colspan="1">Customer ID : <?php echo $arrCustomerID[$key];  ?></th>
							      <th scope="col" colspan="3">Customer Name : <?php echo $arrCustomerName[$key]; ?></th>
							      <th scope="col" colspan="1">User ID : <?php echo $UserID;?></th>
							      <th scope="col" colspan="1">Print Date : <?php echo date("Y-m-d"); ?> </th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="col" colspan="1">Product No : <?php echo $arrProductNo[$key]; ?></th>
							      <th scope="col" colspan="3">Product Name : <?php echo $arrProductName[$key]; ?></th>
							      <th scope="col" colspan="1">Revision No : <?php echo $arrRevNo1Dimention[$key]; ?></th>
							      <th scope="col" colspan="1"></th>
							    </tr>
							    <tr>
							     <th scope="col" colspan="6">Inspection Date : <?php echo $DateFrom; ?> - <?php echo $DateTo; ?></th>
							    </tr>
							    <tr>
							      <th scope="col">Mold No : <?php echo $arrMoldNo1Dimention[$key]; ?></th>
							      <th scope="col">Machine No : <?php echo $arrMachineNo[$key]; ?></th>
							      <th scope="col"><?php if($arrPoint1Dimention[$key] != "0") { echo "Inspection Point : ".$arrPoint1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrCavity1Dimention[$key] != "0") { echo "Cavity No : ".$arrCavity1Dimention[$key]; } ?></th>
							      <th scope="col"><?php if($arrWhere1Dimention[$key] != "0") { echo "Cavity Where : ".$arrWhere1Dimention[$key]; } ?></th>
							      <th scope="col">Q'ty : <?php echo $totalsValue[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col" colspan="6">Item Name : <?php echo $arrItemName[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Standard Spec : <?php echo $arrCenterSpec[$key]; ?></th>
							     <th>Average : <?php echo $averageCpk[$key]; ?></th>
							     <th scope="col" colspan="2">SD : <?php echo $stdevCpk[$key]; ?></th>
							     <th>Cp : <?php echo $Cp[$key]; ?></th>
							     <th>Pp : <?php echo $Pp[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Upper Spec (SU) :  <?php echo $specUpperCPK[$key]; ?></th>
							     <th>Maximum : <?php echo $maxCpk[$key]; ?></th>
							     <th scope="col" colspan="2">S : <?php echo $estimateStdev[$key]; ?></th>
							     <th>Cpu : <?php echo $Cpu[$key]; ?></th>
							     <th>Ppu : <?php echo $Ppu[$key]; ?></th>
							    </tr>
							     <tr>
							     <th scope="col">Lower Spec (SL):  <?php echo $specLowerCPK[$key]; ?></th>
							     <th scope="col" colspan="3">Minimum : <?php echo $minCpk[$key]; ?></th>
							     <th>Cpl : <?php echo $Cpl[$key]; ?></th>
							     <th>Ppl : <?php echo $Ppl[$key]; ?></th>
							     
							    </tr>
							     <tr>
							     <th scope="col" colspan="4"></th>
							     <th>Cpk : <?php echo $Cpk[$key]; ?></th>
							     <th>Ppk : <?php echo $Ppk[$key]; ?></th>
							    <tr>
							    	<th colspan="6">
							    		<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4">
												<table border="0">
													<tr>
														<td >
															<canvas class="canvas" id="<?php echo "cpk".$key; ?>"></canvas>
														</td>
													</tr>
												</table>
											</div>	
										</div>	
									</th>
							    </tr>
							    <tr>
							    	<th colspan="6"><center><img src="../../img/CompanyLogo.png"/>PLASESS HI-TECH CO.,LTD.</center></th>
							    </tr>	
							  </tbody>
							</table>
					<?php } ?>
				</div>
			@endif
		@endif		
	@endif
@endif		

<script type="text/javascript">
	window.onload = function () {
		// draw background
		var backgroundColor = 'white';
		Chart.plugins.register({
		    beforeDraw: function(c) {
		        var ctx = c.chart.ctx;
		        ctx.fillStyle = backgroundColor;
		        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
		    }
		});

        var SelectTypeData = document.getElementById('cmbSelectTypeForGraph').value;
        if(SelectTypeData.length > 0){
    		if(SelectTypeData == "01"){
    			<?php if(isset($arrAverage)){ 
		 			foreach ($arrAverage as $key => $value){ ?>
						var canvas = $('#<?php echo "average".$key; ?>').get(0);
						var myChart = new Chart(canvas, {
					    type: 'line',
					    data: {
					    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
					        datasets: [
					        {
					            label: 'Xbar',
					            data: <?php echo (!empty($arrAverage))?json_encode($arrAverage[$key],JSON_NUMERIC_CHECK):null; ?>,
					            backgroundColor: "rgba(148, 184, 255, 1)",
								lineTension: 0, 
								fill: false,  
								borderColor: "rgba(148, 184, 255, 1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(148, 184, 255, 1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(148, 184, 255, 1)",
									font: {
										weight: 'bold'
									},
								}
							},
							{
								label: 'Max',
								data: <?php echo (!empty($arrMax))?json_encode($arrMax[$key],JSON_NUMERIC_CHECK):null; ?>,
								backgroundColor: "rgba(218, 165, 32, 0.8)",
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(218, 165, 32, 0.8)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(218, 165, 32, 0.8)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(218, 165, 32, 0.8)",
									font: {
										weight: 'bold'
									},
								}
					        },
							{
								label: 'Min',
								data: <?php echo (!empty($arrMin))?json_encode($arrMin[$key],JSON_NUMERIC_CHECK):null; ?>,
								backgroundColor: "rgba(50, 205, 50, 1)",
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(50, 205, 50, 1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(50, 205, 50, 1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(50, 205, 50, 1)",
									font: {
										weight: 'bold'
									},
								}
					        },
					        {
								label: 'XAve',
								type: 'line',
								data: <?php echo (!empty($CLX))?json_encode($CLX[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(0, 0, 0, 1)",
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(0, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
							},
							{
								label: 'UCL',
								data: <?php echo (!empty($UCLX))?json_encode($UCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
					        }
					         ,
							{
								label: 'LCL',
								type: 'line',
								data: <?php echo (!empty($LCLX))?json_encode($LCLX[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
							},
							{
								label: 'SU',
								data: <?php echo (!empty($UCLS))?json_encode($UCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(255, 0, 0, 0.8)",
								borderDash: [15,2],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(255, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255, 0, 0, 0.8)",
									}
					        },
							{
								label: 'SL',
								type: 'line',
								data: <?php echo (!empty($LCLS))?json_encode($LCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(255, 0, 0, 0.8)",
								borderDash: [15,2],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(255, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255, 0, 0, 0.8)",
									}
							}		
					        ]
					    },
					    options: {
					        scales: {
					            yAxes: [{
					                ticks: {
					               		display: true,
					                }
					            }]
					        }
					    }
					});		
					<?php }
				}?>

				<?php if(isset($arrRange)){ 
		 			foreach ($arrRange as $key => $value){ ?>
		 				var canvasRange = $('#<?php echo "range".$key; ?>').get(0);
			 	    	var myChart = new Chart(canvasRange, {
					    type: 'line',
					    data: {
					    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
					        datasets: [
					        {
					            label: 'Range',
					            data: <?php echo (!empty($arrRange))?json_encode($arrRange[$key],JSON_NUMERIC_CHECK):null; ?>,
					            backgroundColor: "rgba(165, 42, 42, 1)",
								lineTension: 0, 
								fill: false,  
								borderColor: "rgba(165, 42, 42, 1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(165, 42, 42, 1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(165, 42, 42, 1)",
									font: {
										weight: 'bold'
									},
								}
							},
							{
								label: 'CL',
								type: 'line',
								data: <?php echo (!empty($CLR))?json_encode($CLR[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(0, 0, 0, 1)",
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(0, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
							},
							{
								label: 'UCL',
								data: <?php echo (!empty($UCLR))?json_encode($UCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
					        },
							{
								label: 'LCL',
								type: 'line',
								data: <?php echo (!empty($LCLR))?json_encode($LCLR[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
							}
					        ]
					    },
					    options: {
					        scales: {
					            yAxes: [{
					                ticks: {
					               		display: true,
					               		min: 0,
					                }
					            }]
					        }
					    }
					});		
				 <?php 	}
				}?>
			}else if(SelectTypeData == "02"){
				<?php if(isset($arrAverage)){ 
		 			foreach ($arrAverage as $key => $value){ ?>
						var canvasXbarStd = $('#<?php echo "average".$key; ?>').get(0);
						var myChart = new Chart(canvasXbarStd, {
					    type: 'line',
					    data: {
					    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
					        datasets: [
					        {
					            label: 'Xbar',
					            data: <?php echo (!empty($arrAverage))?json_encode($arrAverage[$key],JSON_NUMERIC_CHECK):null; ?>,
					            backgroundColor: "rgba(148, 184, 255, 1)",
								lineTension: 0, 
								fill: false,  
								borderColor: "rgba(148, 184, 255, 1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(148, 184, 255, 1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(148, 184, 255, 1)",
									font: {
										weight: 'bold'
									},
								}
							},
							{
								label: 'Max',
								data: <?php echo (!empty($arrMax))?json_encode($arrMax[$key],JSON_NUMERIC_CHECK):null; ?>,
								backgroundColor: "rgba(218, 165, 32, 0.8)",
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(218, 165, 32, 0.8)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(218, 165, 32, 0.8)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(218, 165, 32, 0.8)",
									font: {
										weight: 'bold'
									},
								}
					        },
							{
								label: 'Min',
								data: <?php echo (!empty($arrMin))?json_encode($arrMin[$key],JSON_NUMERIC_CHECK):null; ?>,
								backgroundColor: "rgba(50, 205, 50, 1)",
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(50, 205, 50, 1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(50, 205, 50, 1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(50, 205, 50, 1)",
									font: {
										weight: 'bold'
									},
								}
					        },
					        {
								label: 'XAve',
								type: 'line',
								data: <?php echo (!empty($CLX))?json_encode($CLX[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(0, 0, 0, 1)",
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(0, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
							},
							{
								label: 'UCL',
								data: <?php echo (!empty($UCLXbarStd))?json_encode($UCLXbarStd[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
					        }
					         ,
							{
								label: 'LCL',
								type: 'line',
								data: <?php echo (!empty($LCLXbarStd))?json_encode($LCLXbarStd[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
									}
							},
							{
								label: 'SU',
								data: <?php echo (!empty($UCLS))?json_encode($UCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(255, 0, 0, 0.8)",
								borderDash: [15,2],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(255, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255, 0, 0, 0.8)",
									}
					        },
							{
								label: 'SL',
								type: 'line',
								data: <?php echo (!empty($LCLS))?json_encode($LCLS[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(255, 0, 0, 0.8)",
								borderDash: [15,2],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(255, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255, 0, 0, 0.8)",
									}
							}		
					        ]
					    },
					    options: {
					        scales: {
					            yAxes: [{
					                ticks: {
					               		display: true,
					                }
					            }]
					        }
					    }
					});		
					<?php }
				}?>

				<?php if(isset($arrStandard)){ 
		 			foreach ($arrStandard as $key => $value){ ?>
		 				var canvasStdev = $('#<?php echo "stdev".$key; ?>').get(0);
			 	    	var myChart = new Chart(canvasStdev, {
					    type: 'line',
					    data: {
					    	 labels:<?php echo (!empty($arrLabel))?json_encode($arrLabel[$key],JSON_NUMERIC_CHECK):null; ?>,
					        datasets: [
					        {
					            label: 'Stdev',
					            data: <?php echo (!empty($arrStandard))?json_encode($arrStandard[$key],JSON_NUMERIC_CHECK):null; ?>,
					            backgroundColor: "rgba(219,112,147 ,1)",
								lineTension: 0, 
								fill: false,  
								borderColor: "rgba(219,112,147 ,1)",
								borderWidth: 2,
								pointRadius: 3,
								pointBorderColor: "rgba(219,112,147 ,1)",
								datalabels: {
									align: 'end',
									borderRadius: 1,
									color: "rgba(219,112,147 ,1)",
									font: {
										weight: 'bold'
									},
								}
							},
							{
								label: 'CL',
								type: 'line',
								data: <?php echo (!empty($CLS))?json_encode($CLS[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(0, 0, 0, 1)",
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(0, 0, 0, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
							},
							{
								label: 'UCL',
								data: <?php echo (!empty($UCLStdev))?json_encode($UCLStdev[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
					        },
							{
								label: 'LCL',
								type: 'line',
								data: <?php echo (!empty($LCLStdev))?json_encode($LCLStdev[$key],JSON_NUMERIC_CHECK):null; ?>,
								lineTension: 0,  
								fill: false,
								borderColor: "rgba(48, 48, 48, 0.8)",
								borderDash: [6,4],
								radius: 1,
								borderWidth: 1,
								pointStyle: 'line',
								pointRadius: 4,
								pointBorderColor: "rgba(48, 48, 48, 0)",
								datalabels: {
									align: 'end',
									borderRadius: 0,
									color: "rgba(255,255,255,0.0)",
								}
							}
					        ]
					    },
					    options: {
					        scales: {
					            yAxes: [{
					                ticks: {
					               		display: true,
					               		min: 0,
					                }
					            }]
					        }
					    }
					});		
				 <?php 	}
				}?>
			}else if(SelectTypeData == "03"){
				<?php if(isset($arrRange)){ 
		 			foreach ($arrRange as $key => $value){ ?>
		 			var labels = <?php echo (!empty($arrLabelCPK))?json_encode($arrLabelCPK[$key],JSON_NUMERIC_CHECK):null; ?>;
					var canvas = $('#<?php echo "cpk".$key; ?>').get(0);
					var myChart = new Chart(canvas, {
			    	type: 'bar',
				    data: {
				        labels: labels,
				        datasets: [
				        {
				            label: 'Histogram',
				            data: <?php echo (!empty($arrBar))?json_encode($arrBar[$key],JSON_NUMERIC_CHECK):null; ?>,
				            backgroundColor: 'rgba(0,191,255,0.2)',
					        borderColor: '	rgba(70,130,180,1)',
					        borderWidth: 0.8,
					        datalabels: {
								align: 'end',
								borderRadius: -0.4,
								color: "rgba(255,255,255,0.0)",
							},
							xAxisID: 'axis-bar',
						}
						,
						{
				          	type: 'line',
				            label: 'Bell Curve',
				            data: <?php echo  (!empty($arrBarCPK))?json_encode($arrBarCPK[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: "rgba(0, 0, 0, 1)",
							radius: 1,
							borderWidth: 1,
							pointRadius: 4,
							pointBorderColor: "rgba(0, 0, 0, 0)",
							datalabels: {
								align: 'end',
								borderRadius: 0,
								color: "rgba(255,255,255,0.0)",
							}
				        }
				        ]
				    },
				    options: {
				        scales: {
				           xAxes: [{
		         				stacked : true,
		     					barPercentage: 2.5,
		          				position:'center',
		          				type: 'linear',
		          				gridLines: {
				                    display:false
				                },
				                id: 'axis-bar',
				                type: 'category',
		      				}
		      				,{
		      					stacked : true,
		     					barPercentage: 1.2,
		          				id : 'x-axis-label',
		          				position:'center',
		          				type: 'linear',
				          		gridLines: {
						            display:false
				                },
				                display:false,    
		      				}
		      				]
		      				,
		      				yAxes: [
		         			{
		            			stacked : false
	         				}]
				        },
				        annotation: {
	                    drawTime: "afterDraw",
	                    annotations: [
	                    {
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label',
							value: 0.5,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content: "Average : "+<?php echo json_encode($averageCpk[$key],JSON_NUMERIC_CHECK); ?>,
				                enabled: true,
				                position: "top",
				                },
				        }
				        ,{
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label', 
							value:  <?php echo (!empty($arrIndexUSL))?json_encode($arrIndexUSL[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content:<?php 
				                    if(!empty($specUpperCPK)){
											echo json_encode("USL : ".$specUpperCPK[$key],JSON_NUMERIC_CHECK);
				                    } else{  
				                        echo "";
				                    }  ?>,
				                enabled: true,
				                position: "top",
				                },        
						}
				  		,{
							type: 'line',
							mode: 'vertical' ,
							scaleID: 'x-axis-label', 
							value:  <?php echo (!empty($arrIndexLSL))?json_encode($arrIndexLSL[$key],JSON_NUMERIC_CHECK):null; ?>,
							borderColor: '#E35500',
							borderWidth: 2,
							label: {
				                backgroundColor: "red",
				                content:<?php 
				                    if(!empty($specLowerCPK)){
											echo json_encode("LSL : ".$specLowerCPK[$key],JSON_NUMERIC_CHECK);
				                    } else{  
				                        echo "";
				                    }  ?>,
				                enabled: true,
				                position: "top",
				                },
				          }
	                    ]
	                },
	                legend : {
	      				position: 'right',
	          			labels:{
	        				boxWidth:20,
	        			}
	      			},
	      			maintainAspectRatio: false,
	      			scaleBeginAtZero: true,    
				    }
				});
				<?php }
				} ?> 
			}
		}		

		// save as image
		$('#save').click(function() {
			var UserID = $('#UserID').val();
			var day = new Date();
			var today = moment(day).format("YYYYMMDD");
		   	html2canvas(document.querySelector("#capture")).then(canvas => {
		    	canvas.toBlob(function(blob) {
		        	saveAs(blob,"Graph."+UserID+"."+today+".jpg");
		    	});
			});
		    
		});
	}


</script>
@stop
