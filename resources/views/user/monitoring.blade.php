@extends("layout")
@section("content")
		<div class="header">
			<div class="TitleBar">Monitoring</div>
		</div>
		<body>
		<p><strong><big>●:</big></strong>100% Checked　　　<strong><big>※:</big></strong>Now Checking　　　<strong><big>△:</big></strong>No Check</p>
		</body>
		<meta http-equiv="refresh" content="300">
		@if ($error = $errors->first("error"))
			<div class="ErrorMessage">
				{{ $error }}
			</div>
		@endif

		@if (isset($NormalMessage) == true)
			<div class="NormalMessage">
				{{ $NormalMessage }}
			</div>
		@endif

		{{ Form::open([
			"route"         => "user.monitoring",
			"autocomplete"  => "off",
			"class"         => "FormMarginPaddingSetting",
			"name"          => "MonitoringForm",
		])}}

			<table class="SearchResultTable">
				<thead class="scrollHead">
					<tr class="SearchResultTable whitetableframe">
						<th class="SearchResultHeader MtablecolumnCustomer">Customer<br>Name</th>
						<th class="SearchResultHeader Mtablecolumn02">Lot No.</th>
						<th class="SearchResultHeader Mtablecolumn02">Product<br>No.</th>
						<th class="SearchResultHeader Mtablecolumn02">Product<br>Name</th>
						<th class="SearchResultHeader Mtablecolumn03">M/C<br>Name</th>
						{{-- <th class="SearchResultHeader MtablecolumnTE">Now<br>Error</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnTE">Last<br>Error</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name24)){{$Name24}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name23)){{$Name23}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name22)){{$Name22}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name21)){{$Name21}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name20)){{$Name20}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name19)){{$Name19}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name18)){{$Name18}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name17)){{$Name17}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name16)){{$Name16}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name15)){{$Name15}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name14)){{$Name14}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name13)){{$Name13}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name12)){{$Name12}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name11)){{$Name11}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name10)){{$Name10}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name9)){{$Name9}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name8)){{$Name8}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name7)){{$Name7}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name6)){{$Name6}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name5)){{$Name5}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name4)){{$Name4}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name3)){{$Name3}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name2)){{$Name2}}@endif</th> --}}
						{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name1)){{$Name1}}@endif</th> --}}
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name1)){{$Name1}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name2)){{$Name2}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name3)){{$Name3}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name4)){{$Name4}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name5)){{$Name5}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name6)){{$Name6}}@endif</th>
					<!-- 	<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name7)){{$Name7}}@endif</th>
						<th class="SearchResultHeader MtablecolumnJE1" colspan="2">@if(isset($Name8)){{$Name8}}@endif</th> -->
					</tr>
				</thead>

				
				@if ($Pagenator != null)
					<tbody>
						<?php $cntRow = 0 ?>
						<?php $intCurrentPage = $Pagenator->CurrentPage() ?>
						<?php $intPerPage = $Pagenator->PerPage() ?>

						@foreach($Pagenator as $arrDataRow)
							<?php $cntRow += 1 ?>
							@if (
									(($intPerPage * ($intCurrentPage - 1) + 1) <= $cntRow)
									 and ($cntRow <= $intPerPage * ($intCurrentPage))
								)
								<tr class="SearchResultTable">
									<td class="MtablecolumnCustomer SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->CUSTOMER_NAME)) {!! $arrDataRow->CUSTOMER_NAME !!}  @endif
									</td>
									<td class="Mtablecolumn02 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->INSPECTION_SHEET_NO)) {!! $arrDataRow->INSPECTION_SHEET_NO !!}  @endif
									</td>
									<td class="Mtablecolumn02 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->INSPECTION_SHEET_NAME)) {!! $arrDataRow->INSPECTION_SHEET_NAME !!}  @endif
									</td>
									<td class="Mtablecolumn02 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->LOT_NO)) {!! $arrDataRow->LOT_NO !!}  @endif
									</td>
									<td class="Mtablecolumn04 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->MACHINE_NAME)) {!! $arrDataRow->MACHINE_NAME !!}  @endif
									</td>
									{{-- <td class="MtablecolumnTE SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->NOW_ERROR_SU)) {!! $arrDataRow->NOW_ERROR_SU !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnTE SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->LAST_ERROR_SU)) {!! $arrDataRow->LAST_ERROR_SU !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI24)) {!! $arrDataRow->JOTAI24 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU24)) {!! $arrDataRow->ERROR_SU24 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI23)) {!! $arrDataRow->JOTAI23 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU23)) {!! $arrDataRow->ERROR_SU23 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI22)) {!! $arrDataRow->JOTAI22 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU22)) {!! $arrDataRow->ERROR_SU22 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI21)) {!! $arrDataRow->JOTAI21 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU21)) {!! $arrDataRow->ERROR_SU21 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI20)) {!! $arrDataRow->JOTAI20 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU20)) {!! $arrDataRow->ERROR_SU20 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI19)) {!! $arrDataRow->JOTAI19 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU19)){{ $arrDataRow->ERROR_SU19 }}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI18)) {!! $arrDataRow->JOTAI18 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU18)) {!! $arrDataRow->ERROR_SU18 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI17)) {!! $arrDataRow->JOTAI17 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU17)) {!! $arrDataRow->ERROR_SU17 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI16)) {!! $arrDataRow->JOTAI16 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
									  	@if(isset($arrDataRow->ERROR_SU16)) {!! $arrDataRow->ERROR_SU16 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI15)) {!! $arrDataRow->JOTAI15 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU15)) {!! $arrDataRow->ERROR_SU15 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">    
										@if(isset($arrDataRow->JOTAI14)) {!! $arrDataRow->JOTAI14 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU14)) {!! $arrDataRow->ERROR_SU14 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI13)) {!! $arrDataRow->JOTAI13 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU13)) {!! $arrDataRow->ERROR_SU13 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI12)) {!! $arrDataRow->JOTAI12 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU12)) {!! $arrDataRow->ERROR_SU12 !!}
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI11)) {!! $arrDataRow->JOTAI11 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext"> 
										@if(isset($arrDataRow->ERROR_SU11)) {!! $arrDataRow->ERROR_SU11 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI10)) {!! $arrDataRow->JOTAI10 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU10)) {!! $arrDataRow->ERROR_SU10 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI9)) {!! $arrDataRow->JOTAI9 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU9)) {!! $arrDataRow->ERROR_SU9 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI8)) {!! $arrDataRow->JOTAI8 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU8)) {!! $arrDataRow->ERROR_SU8 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI7)) {!! $arrDataRow->JOTAI7 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU7)) {!! $arrDataRow->ERROR_SU7 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI6)) {!! $arrDataRow->JOTAI6 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU6)) {!! $arrDataRow->ERROR_SU6 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI5)) {!! $arrDataRow->JOTAI5 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU5)) {!! $arrDataRow->ERROR_SU5 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI4)) {!! $arrDataRow->JOTAI4 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU4)) {!! $arrDataRow->ERROR_SU4 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI3)) {!! $arrDataRow->JOTAI3 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU3)) {!! $arrDataRow->ERROR_SU3 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI2)) {!! $arrDataRow->JOTAI2 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU2)) {!! $arrDataRow->ERROR_SU2 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI1)) {!! $arrDataRow->JOTAI1 !!}  @endif
									</td> --}}
									{{-- <td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU1)) {!! $arrDataRow->ERROR_SU1 !!}  @endif
									</td> --}}
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI1)) {!! $arrDataRow->JOTAI1 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU1)) {!! $arrDataRow->ERROR_SU1 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI2)) {!! $arrDataRow->JOTAI2 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU2)) {!! $arrDataRow->ERROR_SU2 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI3)) {!! $arrDataRow->JOTAI3 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU3)) {!! $arrDataRow->ERROR_SU3 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI4)) {!! $arrDataRow->JOTAI4 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU4)) {!! $arrDataRow->ERROR_SU4 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI5)) {!! $arrDataRow->JOTAI5 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU5)) {!! $arrDataRow->ERROR_SU5 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI6)) {!! $arrDataRow->JOTAI6 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU6)) {!! $arrDataRow->ERROR_SU6 !!}  @endif
									</td>
								{{--	<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI7)) {!! $arrDataRow->JOTAI7 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU7)) {!! $arrDataRow->ERROR_SU7 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif">
										@if(isset($arrDataRow->JOTAI8)) {!! $arrDataRow->JOTAI8 !!}  @endif
									</td>
									<td class="MtablecolumnJE1 SearchResultTable @if(($cntRow % 2) == 0) EvenNumRowColor @endif righttext">
										@if(isset($arrDataRow->ERROR_SU8)) {!! $arrDataRow->ERROR_SU8 !!}  @endif --}}
									</td>
								</tr>

								@if(($cntRow % 5) == 0)
									<tr class="SearchResultTable whitetableframe">
										<th class="SearchResultHeader MtablecolumnCustomer">Customer<br>Name</th>
										<th class="SearchResultHeader Mtablecolumn02">Product<br>No.</th>
										<th class="SearchResultHeader Mtablecolumn02">Product<br>Name</th>
										<th class="SearchResultHeader MtablecolumnCustomer">Lot No.</th>
										<th class="SearchResultHeader Mtablecolumn03">M/C<br>Name</th>
										{{-- <th class="SearchResultHeader MtablecolumnTE">Now Error</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnTE">Last Error</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name24}}</th> --}} 
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name23}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name22}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name21}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name20}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name19}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name18}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name17}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name16}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name15}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name14}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name13}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name12}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name11}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name10}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name9}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name8}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name7}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name6}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name5}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name4}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name3}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name2}}</th> --}}
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name1}}</th> --}}
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name1}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name2}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name3}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name4}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name5}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name6}}</th>
										{{-- <th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name7}}</th>
										<th class="SearchResultHeader MtablecolumnJE1" colspan="2">{{$Name8}}</th> --}}
									</tr>
								@endif
							@endif
						@endforeach

						<!-- hdnは共通部品で汎用性を持たせているため常に4つ定義しておく) -->
						{{ Form::hidden("hidPrimaryKey1") }}
					</tbody>
				@endif
			</table>

			<div class="pagination">
				{!! $Pagenator->render(); !!}
			</div>

			{{ Form::close() }}


			<style type="text/css">
				/* Pagination links */
				.pagination a {
				    color: black;
				    float: left;
				    padding: 8px 16px;
				    text-decoration: none;
				    transition: background-color .3s;
				}

				/* Style the active/current link */
				.pagination a.active {
				    background-color: dodgerblue;
				    color: white;
				}

				/* Add a grey background color on mouse-over */
				.pagination a:hover:not(.active) {background-color: #ddd;}
			</style>


    <script type="text/javascript">
    	 setInterval(function() {
                  window.location.reload();
                }, 300000); 
    </script>

@stop
